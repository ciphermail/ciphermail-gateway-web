/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.rest.client.MTAClient;
import com.ciphermail.rest.core.MTADTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@Component
@Command(command = {"mta"}, group = "MTA")
@SuppressWarnings({"java:S6813"})
public class MTACommands
{
    @Autowired
    private MTAClient mtaClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "list"}, description = "List MTA mail queue")
    public String getQueueList(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(mtaClient.getQueueList(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "size"}, description = "Get MTA queue size")
    public Long getQueueSize()
    {
        return mtaClient.getQueueSize();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "flush"}, description = "Flush MTA queue")
    public void flushQueue()
    {
        mtaClient.flushQueue();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "reschedule"}, description = "Reschedule deferred Mail")
    public void rescheduleDeferredMail(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.rescheduleDeferredMail(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "delete"}, description = "Delete Mail")
    public void deleteMailFromQueue(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.deleteMailFromQueue(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "hold"}, description = "Hold Mail")
    public void holdMailFromQueue(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.holdMailFromQueue(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "release"}, description = "Release Mail")
    public void releaseMailFromQueue(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.releaseMailFromQueue(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "bounce"}, description = "Bounce Mail")
    public void bounceMailFromQueue(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.bounceMailFromQueue(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "requeue"}, description = "Requeue Mail")
    public void requeueMailFromQueue(@Option(required = true, longNames = "queue-ids") List<String> queueIDs)
    {
        mtaClient.requeueMailFromQueue(queueIDs);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"queue", "get"}, description = "Get a queued mail")
    public String getMail(
            @Option(required = true, longNames = "queue-id") String queueID,
            @Option(required = false) Long maxSize,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File mimeOutputFile)
    throws IOException
    {
        String mail = mtaClient.getMail(queueID, maxSize);

        if (mail == null) {
            throw new IllegalArgumentException(String.format("There is no email with id %s", queueID));
        }

        if (mimeOutputFile != null && ShellUtils.validateForWriting(mimeOutputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(mimeOutputFile,false))
        {
            throw new CancelCommandException();
        }

        if (mimeOutputFile != null) {
            FileUtils.write(mimeOutputFile, mail, StandardCharsets.UTF_8);
        }

        return mimeOutputFile == null ? mail : null;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "get"}, description = "Get Postfix main config")
    public String getMainConfig() {
        return mtaClient.getMainConfig();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "set"}, description = "Set Postfix main config")
    public void setMainConfig(
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File configFile)
    throws IOException
    {
        mtaClient.setMainConfig(FileUtils.readFileToString(configFile, StandardCharsets.UTF_8));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"control", "start"}, description = "Start Postfix")
    public void startPostfix()
    {
        mtaClient.startPostfix();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"control", "stop"}, description = "Stop Postfix")
    public void stopPostfix()
    {
        mtaClient.stopPostfix();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"control", "restart"}, description = "Restart Postfix")
    public void restartPostfix()
    {
        mtaClient.restartPostfix();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"control", "status"}, description = "Get Postfix status")
    public String getPostfixStatus()
    {
        return mtaClient.getPostfixStatus();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"control", "check", "running"}, description = "Check if Postfix is running")
    public boolean isPostfixRunning()
    {
        return mtaClient.isPostfixRunning();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "myhostname", "set"}, description = "Set Postfix myhostname")
    public void setPostfixMyHostname(@Option(required = true) String myHostname)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setMyHostname(myHostname));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "myhostname", "get"}, description = "Get Postfix myhostname")
    public String getPostfixMyHostname()
    {
        return mtaClient.getStandardMainSettings().getMyHostname();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "mynetworks", "set"}, description = "Set Postfix mynetworks")
    public void setPostfixMyNetworks(@Option(required = true) List<String> myNetworks)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setMyNetworks(
                myNetworks));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "mynetworks", "get"}, description = "Get Postfix mynetworks")
    public List<String> getPostfixMyNetworks()
    {
        return mtaClient.getStandardMainSettings().getMyNetworks();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host", "set"}, description = "Set Postfix external relay host")
    public void setPostfixExternalRelayHost(@Option(required = true) String relayHost)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setExternalRelayHost(relayHost));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host", "get"}, description = "Get Postfix external relay host")
    public String getPostfixExternalRelayHost()
    {
        return mtaClient.getStandardMainSettings().getExternalRelayHost();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host-mx-lookup", "set"}, description = "Set Postfix external relay host mx lookup")
    public void setPostfixExternalRelayHostMxLookup(@Option(required = true) boolean relayHostMxLookup)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setExternalRelayHostMxLookup(relayHostMxLookup));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host-mx-lookup", "get"}, description = "Get Postfix external relay host mx lookup")
    public Boolean getPostfixExternalRelayHostMxLookup()
    {
        return mtaClient.getStandardMainSettings().getExternalRelayHostMxLookup();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host-port", "set"}, description = "Set Postfix external relay host port")
    public void setPostfixExternalRelayHostPort(@Option(required = true) int relayHostPort)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setExternalRelayHostPort(relayHostPort));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "external-relay-host-port", "get"}, description = "Get Postfix external relay host port")
    public Integer getPostfixExternalRelayHostPort()
    {
        return mtaClient.getStandardMainSettings().getExternalRelayHostPort();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "relay-domains", "set"}, description = "Set Postfix relay domains")
    public void setPostfixRelayDomains(@Option(required = true) List<String> relayDomains)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setRelayDomains(
                relayDomains));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "relay-domains", "get"}, description = "Get Postfix relay domains")
    public List<String> getPostfixRelayDomains()
    {
        return mtaClient.getStandardMainSettings().getRelayDomains();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "message-size-limit", "set"}, description = "Set Postfix message size limit")
    public void setPostfixMessageSizeLimit(@Option(required = true) long messageSizeLimit)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setMessageSizeLimit(messageSizeLimit));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "message-size-limit", "get"}, description = "Get Postfix message size limit")
    public Long getPostfixMessageSizeLimit()
    {
        return mtaClient.getStandardMainSettings().getMessageSizeLimit();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "smtp-helo-name", "set"}, description = "Set Postfix SMTP HELO/EHLO name")
    public void setPostfixSmtpHeloName(@Option(required = true) String smtpHeloName)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setSmtpHeloName(smtpHeloName));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "smtp-helo-name", "get"}, description = "Get Postfix SMTP HELO/EHLO name")
    public String getPostfixSmtpHeloName()
    {
        return mtaClient.getStandardMainSettings().getSmtpHeloName();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host", "set"}, description = "Set Postfix internal relay host")
    public void setPostfixInternalRelayHost(@Option(required = true) String relayHost)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setInternalRelayHost(relayHost));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host", "get"}, description = "Get Postfix internal relay host")
    public String getPostfixInternalRelayHost()
    {
        return mtaClient.getStandardMainSettings().getInternalRelayHost();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host-mx-lookup", "set"}, description = "Set Postfix internal relay host mx lookup")
    public void setPostfixInternalRelayHostMxLookup(@Option(required = true) boolean relayHostMxLookup)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setInternalRelayHostMxLookup(relayHostMxLookup));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host-mx-lookup", "get"}, description = "Get Postfix internal relay host mx lookup")
    public Boolean getPostfixInternalRelayHostMxLookup()
    {
        return mtaClient.getStandardMainSettings().getInternalRelayHostMxLookup();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host-port", "set"}, description = "Set Postfix internal relay host port")
    public void setPostfixInternalRelayHostPort(@Option(required = true) int relayHostPort)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setInternalRelayHostPort(relayHostPort));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "internal-relay-host-port", "get"}, description = "Get Postfix internal relay host port")
    public Integer getPostfixInternalRelayHostPort()
    {
        return mtaClient.getStandardMainSettings().getInternalRelayHostPort();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "reject-unverified-recipient", "set"}, description = "Set Postfix reject unverified recipient")
    public void setPostfixRejectUnverifiedRecipient(@Option(required = true) boolean rejectUnverifiedRecipient)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setRejectUnverifiedRecipient(rejectUnverifiedRecipient));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "reject-unverified-recipient", "get"}, description = "Get Postfix reject unverified recipient")
    public Boolean getPostfixRejectUnverifiedRecipient()
    {
        return mtaClient.getStandardMainSettings().getRejectUnverifiedRecipient();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "unverified-recipient-reject-code", "set"}, description = "Set Postfix unverified recipient reject code")
    public void setPostfixUnverifiedRecipientRejectCode(@Option(required = true) int unverifiedRecipientRejectCode)
    {
        mtaClient.setStandardMainSettings(MTADTO.StandardMainSettings.createInstance().setUnverifiedRecipientRejectCode(unverifiedRecipientRejectCode));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"config", "main", "unverified-recipient-reject-code", "get"}, description = "Set Postfix unverified recipient reject code")
    public Integer getPostfixUnverifiedRecipientRejectCode()
    {
        return mtaClient.getStandardMainSettings().getUnverifiedRecipientRejectCode();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"map", "list"}, description = "List all Postfix maps")
    public String listMaps()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(mtaClient.getMaps());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"map", "get", "content"}, description = "Get the Postfix map content")
    public String getMapContent(
            @Option(required = true, label = ShellConst.POSTFIX_MAP_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.PostfixMapType type,
            @Option(required = true) String name,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (outputFile != null && ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        String content = mtaClient.getMapContent(type, name);

        if (outputFile != null) {
            FileUtils.write(outputFile, content, StandardCharsets.UTF_8);
        }

        return outputFile == null ? content : null;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"map", "set", "content"}, description = "Set the Postfix map content")
    public void setMapContent(
            @Option(required = true, label = ShellConst.POSTFIX_MAP_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.PostfixMapType type,
            @Option(required = true) String name,
            @Option(required = false) String content,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File file)
    throws IOException
    {
        if (content == null && file == null) {
            throw new IllegalArgumentException("Either content or file must be specified");
        }

        if (content != null && file != null) {
            throw new IllegalArgumentException("Either content or file must be specified");
        }

        String mapContent;

        if (content != null) {
            mapContent = StringUtils.replace(content, "\\n", "\n");
        }
        else {
            mapContent = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        }

        mtaClient.setMapContent(type, name, mapContent);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"map", "create"}, description = "Create a Postfix map file")
    public String createMap(
            @Option(required = true, label = ShellConst.POSTFIX_MAP_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.PostfixMapType type,
            @Option(required = true) String name)
    throws IOException
    {
        return jsonPrettyPrinter.prettyPrintJSON(mtaClient.createMap(type, name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"map", "delete"}, description = "Delete a Postfix map file")
    public void deleteMap(
            @Option(required = true, label = ShellConst.POSTFIX_MAP_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.PostfixMapType type,
            @Option(required = true) String name)
    {
        mtaClient.deleteMap(type, name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"send", "mime"}, description = "Send a raw MIME email")
    public void sendEmailMime(
            @Option(required = false) String sender,
            @Option(required = true) List<String> recipients,
            @Option(required = true, label = ShellConst.MTA_PORT_LABEL, defaultValue = "mpa")
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.MTAPort mtaPort,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File mimeFile)
    {
        mtaClient.sendEmailMime(
                sender,
                recipients,
                mtaPort,
                new FileSystemResource(ShellUtils.validateForReading(mimeFile)));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"send", "mail"}, description = "Send an email")
    public void sendEmail(
            @Option(required = true) String subject,
            @Option(required = true) String from,
            @Option(required = false) String textBodyBase64,
            @Option(required = false) String htmlBodyBase64,
            @Option(required = false) String sender,
            @Option(required = true) List<String> recipients,
            @Option(required = false) List<String> to,
            @Option(required = false) List<String> cc,
            @Option(required = true, label = ShellConst.MTA_PORT_LABEL, defaultValue = "mpa")
                    @OptionValues(provider = "enumCompletionProvider") MTADTO.MTAPort mtaPort,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File attachment)
    {
        mtaClient.sendEmail(
                subject,
                from,
                Optional.ofNullable(textBodyBase64).map(v -> new String(Base64Utils.decode(v),
                        StandardCharsets.UTF_8)).orElse(null),
                Optional.ofNullable(htmlBodyBase64).map(v -> new String(Base64Utils.decode(v),
                        StandardCharsets.UTF_8)).orElse(null),
                sender,
                recipients,
                to,
                cc,
                mtaPort,
                attachment != null ? new FileSystemResource(ShellUtils.validateForReading(attachment)) : null);
    }
}
