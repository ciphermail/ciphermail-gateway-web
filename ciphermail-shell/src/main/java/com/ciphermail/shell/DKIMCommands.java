/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.DKIMClient;
import com.ciphermail.rest.client.util.FileNameAwareByteArrayResource;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.StandardCharsets;

@Component
@Command(command = "dkim", group = "DKIM")
@SuppressWarnings({"java:S6813"})
public class DKIMCommands
{
    @Autowired
    private DKIMClient dkimClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"generate", "key"}, description = "Generate a new DKIM key")
    public void generateKey(
            @Option(required = true) String keyId,
            @Option(required = true, label = "[1024, 2048, 3072, 4096]", defaultValue = "2048") int keyLength)
    {
        dkimClient.generateKey(keyId, keyLength);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "key-ids"}, description = "List all DKIM key ids")
    public String getCTLs(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dkimClient.getKeyIds(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"set", "key"}, description = "Set DKIM key")
    public void setKey(
            @Option(required = true) String keyId,
            @Option(required = false) String pemEncodedKeyPair,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File file)
    {
        if ((pemEncodedKeyPair == null && file == null) || (pemEncodedKeyPair != null && file != null)) {
            throw new IllegalArgumentException("Either pem-encoded-key-pair or file should be specified");
        }
        dkimClient.setKey(keyId, file != null ?
                new FileSystemResource(ShellUtils.validateForReading(file)) :
                new FileNameAwareByteArrayResource("dkim.key",
                        pemEncodedKeyPair.getBytes(StandardCharsets.UTF_8)));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "public-key"}, description = "Get DKIM public key")
    public String getPublicKey(@Option(required = true) String keyId)
    {
        return dkimClient.getPublicKey(keyId);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "key-pair"}, description = "Get DKIM key pair")
    public String getKeyPair(@Option(required = true) String keyId)
    {
        return dkimClient.getKeyPair(keyId);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "available"}, description = "Returns true if a key with the given id is available")
    public String isKeyAvailable(@Option(required = true) String keyId)
    {
        return dkimClient.isKeyAvailable(keyId);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete", "key"}, description = "Delete DKIM key")
    public String deleteKey(@Option(required = true) String keyId)
    {
        return dkimClient.deleteKey(keyId);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"parse-signature-template"}, description = "Parse a DKIM signature template")
    public String parseSignatureTemplate(@Option(required = true) String signatureTemplate)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dkimClient.parseSignatureTemplate(
                new FileNameAwareByteArrayResource("signatureTemplate",
                        signatureTemplate.getBytes(StandardCharsets.UTF_8))));
    }
}
