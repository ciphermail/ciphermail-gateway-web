/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.RestClientProvider;
import com.ciphermail.rest.client.SystemClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.stereotype.Component;

@Component
@Command(command = "system", group = "System")
@SuppressWarnings({"java:S6813"})
public class SystemCommands
{
    @Autowired
    private SystemClient systemClient;

    @Autowired
    private RestClientProvider restClientProvider;

    @Autowired
    private ShellRestClientSettingsProvider restClientSettingsProvider;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "ping", description = "Send a ping command to the server and expect a pong back")
    public String ping()
    {
        return systemClient.ping();
    }

    @Command(command = {"set", "base-url"}, description = "Set the back-end base URL")
    public void setBaseURL(@Option(required = true) String url) {
        restClientSettingsProvider.setBaseUrl(url);
    }

    @Command(command = {"get", "base-url"}, description = "Get the back-end base URL")
    public String getBaseURL() {
        return restClientSettingsProvider.getBaseUrl();
    }

    @Command(command = {"set", "username"}, description = "Set the back-end username")
    public void setUsername(@Option(required = true) String username) {
        restClientSettingsProvider.setUsername(username);
    }

    @Command(command = {"get", "username"}, description = "Get the back-end username")
    public String getUsername() {
        return restClientSettingsProvider.getUsername();
    }

    @Command(command = {"set", "password"}, description = "Set the back-end password")
    public void setPassword(@Option(required = true) String password) {
        restClientSettingsProvider.setPassword(password);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "reset", description = "Reset the cached http client connection")
    public void reset() {
        restClientProvider.reset();
    }
}
