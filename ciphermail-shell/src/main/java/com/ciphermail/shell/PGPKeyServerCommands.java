/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.PGPKeyServerClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Command(command = {"pgp", "keyserver"}, group = "PGP")
@SuppressWarnings({"java:S6813"})
public class PGPKeyServerCommands
{
    @Autowired
    private PGPKeyServerClient pgpKeyServerClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search"}, description = "Search for keys on a remote key server")
    public String search(
            @Option(required = true) String query,
            @Option(required = true, defaultValue = "true") boolean exactMatch,
            @Option(required = true, defaultValue = "10") int maxKeys)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyServerClient.searchKeys(query, exactMatch, maxKeys));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"download"}, description = "Download keys from a remote key server")
    public String downloadKeys(
            @Option(required = true) List<String> keyIds,
            @Option(required = true, defaultValue = "false") boolean autoTrust)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyServerClient.downloadKeys(keyIds, autoTrust));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"refresh"}, description = "Refresh keys from a remote key server")
    public String refreshKeys(@Option(required = true) List<UUID> ids)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyServerClient.refreshKeys(ids));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"submit"}, description = "Submit keys to a remote key server")
    public String submitKeys(@Option(required = true) List<UUID> ids)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyServerClient.submitKeys(ids));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "config"}, description = "Get remote key server config")
    public String getPGPKeyServers()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpKeyServerClient.getPGPKeyServers());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"set", "config"}, description = "Set remote key server config")
    public void setPGPKeyServers(List<String> keyServerUrls)
    {
        pgpKeyServerClient.setPGPKeyServers(keyServerUrls);
    }
}
