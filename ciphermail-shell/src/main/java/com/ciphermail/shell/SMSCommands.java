/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.rest.client.SMSClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Command(command = {"sms"}, group = "SMS")
@SuppressWarnings({"java:S6813"})
public class SMSCommands
{
    @Autowired
    private SMSClient smsClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all"}, description = "Get SMS messages")
    public String getSMSMessages(
            @Option(required = true, defaultValue = "created", label = ShellConst.SMS_SORT_COLUMN_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") SortColumn sortColumn,
            @Option(required = true, defaultValue = "asc", label = ShellConst.SORT_DIRECTION_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") SortDirection sortDirection,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(smsClient.getSMSMessages(
                sortColumn,
                sortDirection,
                firstResult,
                maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all", "count"}, description = "Get number of SMS messages")
    public Long getSMSMessagesCount()
    {
        return smsClient.getSMSMessagesCount();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"send"}, description = "Send SMS message")
    public String sendSMSMessage(
            @Option(required = true) String phoneNumber,
            @Option(required = true) String message)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(smsClient.sendSMSMessage(
                phoneNumber,
                message));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete"}, description = "Delete SMS message")
    public String deleteSMSMessage(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(smsClient.deleteSMSMessage(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"transports"}, description = "List all SMS transports")
    public String getSMSTransports()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(smsClient.getSMSTransports());
    }
}
