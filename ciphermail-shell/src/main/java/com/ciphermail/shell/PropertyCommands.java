/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.PropertyClient;
import com.ciphermail.rest.core.PropertyDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

@Component
@Command(command = {"property"}, group = "Property")
@SuppressWarnings({"java:S6813"})
public class PropertyCommands
{
    @Autowired
    private PropertyClient propertyClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get"}, description = "Get user property")
    public String getUserProperty(
            @Option(required = true) String email,
            @Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getUserProperty(email, name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "value"}, description = "Get user property value")
    public String getUserPropertyValue(
            @Option(required = true) String email,
            @Option(required = true) String name)
    {
        return propertyClient.getUserProperty(email, name).value();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "multiple"}, description = "Get user properties")
    public String getUserProperties(
            @Option(required = true) String email,
            @Option(required = true) List<String> names)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getUserProperties(email, names));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set"}, description = "Set user property")
    public void setUserProperty(
            @Option(required = true) String email,
            @Option(required = true) String name,
            @Option(required = true) String value)
    {
        propertyClient.setUserProperty(email, name, value);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "file"}, description = "Set user property from a file")
    public void setUserProperty(
            @Option(required = true) String email,
            @Option(required = true) String name,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    throws IOException
    {
        propertyClient.setUserProperty(email, name, FileUtils.readFileToString(
                ShellUtils.validateForReading(file), StandardCharsets.UTF_8));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "reset"}, description = "Reset user property to its inherited value")
    public void resetUserProperty(
            @Option(required = true) String email,
            @Option(required = true) String name)
    {
        propertyClient.resetUserProperty(email, name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get"}, description = "Get domain property")
    public String getDomainProperty(
            @Option(required = true) String domain,
            @Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getDomainProperty(domain, name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "value"}, description = "Get domain property value")
    public String getDomainPropertyValue(
            @Option(required = true) String domain,
            @Option(required = true) String name)
    {
        return propertyClient.getDomainProperty(domain, name).value();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "multiple"}, description = "Get domain properties")
    public String getDomainProperties(
            @Option(required = true) String domain,
            @Option(required = true) List<String> names)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getDomainProperties(domain, names));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set"}, description = "Set domain property")
    public void setDomainProperty(
            @Option(required = true) String domain,
            @Option(required = true) String name,
            @Option(required = true) String value)
    {
        propertyClient.setDomainProperty(domain, name, value);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "file"}, description = "Set domain property from a file")
    public void setDomainProperty(
            @Option(required = true) String domain,
            @Option(required = true) String name,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    throws IOException
    {
        propertyClient.setDomainProperty(domain, name, FileUtils.readFileToString(
                ShellUtils.validateForReading(file), StandardCharsets.UTF_8));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "reset"}, description = "Reset domain property to its inherited value")
    public void resetDomainProperty(
            @Option(required = true) String domain,
            @Option(required = true) String name)
    {
        propertyClient.resetDomainProperty(domain, name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get"}, description = "Get global property")
    public String getGlobalProperty(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getGlobalProperty(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get", "value"}, description = "Get global property value")
    public String getGlobalPropertyValue(@Option(required = true) String name)
    {
        return propertyClient.getGlobalProperty(name).value();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get", "multiple"}, description = "Get global properties")
    public String getGlobalProperties(@Option(required = true) List<String> names)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getGlobalProperties(names));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "set"}, description = "Set global property")
    public void setGlobalProperty(
            @Option(required = true) String name,
            @Option(required = true) String value)
    {
        propertyClient.setGlobalProperty(name, value);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "set", "file"}, description = "Set global property from a file")
    public void setGlobalProperty(
            @Option(required = true) String name,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    throws IOException
    {
        propertyClient.setGlobalProperty(name, FileUtils.readFileToString(
                ShellUtils.validateForReading(file), StandardCharsets.UTF_8));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "reset"}, description = "Reset global property to its inherited value")
    public void resetGlobalProperty(@Option(required = true) String name)
    {
        propertyClient.resetGlobalProperty(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"available"}, description = "Get all supported properties")
    public String getAvailableProperties(
            @Option(required = false) String category,
            @Option(required = false, defaultValue = "true") boolean skipInvisible,
            @Option(required = false, defaultValue = "true") boolean skipUnauthorized)
    throws JsonProcessingException
    {
        List<PropertyDTO.UserPropertiesDetails> result = propertyClient.getAvailableProperties(skipInvisible,
                skipUnauthorized);

        if (category != null)
        {
            boolean found = false;

            for (PropertyDTO.UserPropertiesDetails userPropertiesDetails : result)
            {
                if (category.equalsIgnoreCase(userPropertiesDetails.category()))
                {
                    result = List.of(userPropertiesDetails);

                    found = true;

                    break;
                }
            }

            if (!found) {
                result = Collections.emptyList();
            }
        }

        return jsonPrettyPrinter.prettyPrintJSON(result);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"available", "categories"}, description = "List all property categories")
    public String getAvailablePropertyCategories(
            @Option(required = false, defaultValue = "true") boolean skipInvisible)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getAvailableProperties(
                skipInvisible, false).stream().map(
                PropertyDTO.UserPropertiesDetails::category).sorted().toList());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"descriptor"}, description = "Get property descriptor for a property")
    public String getUserPropertyDescriptor(
            @Option String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(propertyClient.getUserPropertyDescriptor(name));
    }
}
