/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.TLSClient;
import com.ciphermail.rest.core.TLSDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
@Command(command = {"tls"}, group = "TLS")
@SuppressWarnings({"java:S6813"})
public class TLSCommands
{
    @Autowired
    private TLSClient csrClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "create"}, description = "Create a CSR")
    public String createCSR(
            @Option(required = true) List<String> domains,
            @Option(required = false) String email,
            @Option(required = false) String organisation,
            @Option(required = false) String organisationalUnit,
            @Option(required = false) String countryCode,
            @Option(required = false) String state,
            @Option(required = false) String locality,
            @Option(required = true) String commonName,
            @Option(required = false) String givenName,
            @Option(required = false) String surname,
            @Option(required = true, label = ShellConst.KEY_ALGORITHM_LABEL, defaultValue = "secp256r1")
                    @OptionValues(provider = "enumCompletionProvider") TLSDTO.KeyAlgorithm keyAlgorithm
    )
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(csrClient.createCSR(
                domains,
                email,
                organisation,
                organisationalUnit,
                countryCode,
                state,
                locality,
                commonName,
                givenName,
                surname,
                keyAlgorithm
        ));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "list"}, description = "List all CSRs")
    public String getCSRs()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(csrClient.getCSRs());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "get"}, description = "Get CSR with the given id")
    public String getCSR(@Option(required = true) String id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(csrClient.getCSR(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "delete"}, description = "Delete CSR with the given id")
    public void deleteCSR(@Option(required = true) String id)
    {
        csrClient.deleteCSR(id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "export", "pkcs10"}, description = "Export PKCS#10 request with the given id")
    public String getPKCS10Request(
            @Option(required = true) String id,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (outputFile != null && ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        String pkcs10 = csrClient.exportPKCS10Request(id);

        String result = null;

        if (outputFile != null) {
            FileUtils.write(outputFile, pkcs10, StandardCharsets.UTF_8);
        }
        else {
            result = pkcs10;
        }

        return result;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "import", "certificate-chain"}, description = "Import certificate chain from a file")
    public String importCertificateChain(
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File certificatesFile)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(csrClient.importCertificateChain(
                new FileSystemResource(ShellUtils.validateForReading(certificatesFile))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "export", "certificate-chain"}, description = "Export certificate chain and private key to a " +
                                                                      "password protected PEM")
    public String exportCertificateChain(
            @Option(required = true) String id,
            @Option(required = true) String exportPassword,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (outputFile != null && ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        String pem = csrClient.exportCertificateChain(id, exportPassword);

        String result = null;

        if (outputFile != null) {
            FileUtils.write(outputFile, pem, StandardCharsets.UTF_8);
        }
        else {
            result = pem;
        }

        return result;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"csr", "install", "certificate"}, description = "Make the certificate from the CSR the active TLS " +
                                                                  "certificate")
    public void installCertificate(@Option(required = true) String id) {
        csrClient.installCertificate(id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pkcs12", "import"}, description = "Import a TLS certificate from a file")
    public void importPKCS12(
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File pkcs12File,
            @Option(required = false) String keyStorePassword)
    {
        csrClient.importPKCS12(new FileSystemResource(ShellUtils.validateForReading(pkcs12File)),
                keyStorePassword);
    }
}
