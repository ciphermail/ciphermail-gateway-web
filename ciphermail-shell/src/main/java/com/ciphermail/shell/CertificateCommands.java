/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.client.CertificateClient;
import com.ciphermail.rest.core.CertificateImportAction;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.KeyExportFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
@Command(command = "certificate", group = "Certificate")
@SuppressWarnings({"java:S6813"})
public class CertificateCommands
{
    @Autowired
    private CertificateClient certificateClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "external"}, description = "Get certificate details without importing the certificate")
    public String getCertificateDetails(@Option(required = true)
            @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.getCertificateDetails(
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all"}, description = "List all certificates")
    public String getCertificates(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.getCertificates(store, Expired.MATCH_ALL,
                MissingKeyAlias.ALLOWED, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all", "count"}, description = "Get the number of all certificates")
    public Long getCertificatesCount(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store)
    {
        return certificateClient.getCertificatesCount(store, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "matching"}, description = "List all matching certificates")
    public String getCertificates(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.getCertificates(store, expired, missingKeyAlias,
                firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "matching", "count"}, description = "Get the number of matching certificates")
    public Long getCertificateCount(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias)
    {
        return certificateClient.getCertificatesCount(store, expired, missingKeyAlias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "email"}, description = "List all certificates matching the email address")
    public String searchForCertificatesByEmail(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "exact", label = ShellConst.MATCH_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Match match,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.searchForCertificatesByEmail(store, email, match,
                expired, missingKeyAlias, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "email", "count"}, description = "Get the number of certificates " +
                                                                   "matching the email address")
    public Long searchForCertificatesByEmailCount(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String email,
            @Option(required = true, defaultValue = "exact", label = ShellConst.MATCH_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Match match,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias)
    {
        return certificateClient.searchForCertificatesByEmailCount(store, email, match, expired, missingKeyAlias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "subject"}, description = "List all certificates matching the subject")
    public String searchForCertificatesBySubject(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String subject,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.searchForCertificatesBySubject(store, subject,
                expired, missingKeyAlias, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "subject", "count"}, description = "Get the number of certificates " +
                                                                     "matching the subject")
    public Long searchForCertificatesBySubjectCount(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String subject,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias)
    {
        return certificateClient.searchForCertificatesBySubjectCount(store, subject, expired, missingKeyAlias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "issuer"}, description = "List all certificates matching the issuer")
    public String searchForCertificatesByIssuer(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String issuer,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.searchForCertificatesByIssuer(store, issuer,
                expired, missingKeyAlias, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "issuer", "count"}, description = "Get the number of certificates " +
                                                                    "matching the issuer")
    public Long searchForCertificatesByIssuerCount(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String issuer,
            @Option(required = true, defaultValue = "match-all", label = ShellConst.EXPIRED_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") Expired expired,
            @Option(required = true, defaultValue = "allowed", label = ShellConst.MISSING_KEYALIAS_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAlias missingKeyAlias)
    {
        return certificateClient.searchForCertificatesByIssuerCount(store, issuer, expired, missingKeyAlias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "get", description = "Get the certificate with the given thumbprint")
    public String getCertificate(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.getCertificate(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "import", description = "Import certificates from a file")
    public String importCertificates(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true, defaultValue = "import-all", label = ShellConst.CERT_IMPORT_ACTION_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateImportAction certificateImportAction,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.importCertificates(store, certificateImportAction,
                new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = "delete", description = "Delete a certificate")
    public void deleteCertificate(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    {
        certificateClient.deleteCertificate(store, thumbprint);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"export", "certificates"}, description = "Export certificates to a DER or PEM encoded file")
    public void exportCertificates(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true, defaultValue = "pem", label = ShellConst.OBJECT_ENCODING_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") ObjectEncoding encoding,
            @Option(required = true) List<String> thumbprints,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, certificateClient.exportCertificates(store, encoding, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"export", "chain"}, description = "Export the certificate chain of a certificate to a " +
                                                          "DER or PEM encoded file")
    public void exportCertificateChain(
            @Option(required = true, defaultValue = "pem", label = ShellConst.OBJECT_ENCODING_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") ObjectEncoding encoding,
            @Option(required = true) String thumbprint,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, certificateClient.exportCertificateChain(encoding, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"import", "keys"}, description = "Import private keys from a PKCS#12 or PEM file")
    public String importPKCS12(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String keystorePassword,
            @Option(required = true, defaultValue = "add-certificate-only-entry",
                    label = ShellConst.MISSING_KEY_ACTION_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") MissingKeyAction missingKeyAction,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.importPrivateKeys(store, keystorePassword, missingKeyAction,
                 new FileSystemResource(ShellUtils.validateForReading(file))));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"export", "keys"}, description = "Export private keys to PKCS#12 or PEM encoded file")
    public void exportPrivateKeys(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String keystorePassword,
            @Option(required = true, defaultValue = "pkcs12", label = ShellConst.KEY_EXPORT_FORMAT_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyExportFormat keyExportFormat,
            @Option(required = true, defaultValue = "true") boolean includeRoot,
            @Option(required = true) List<String> thumbprints,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, certificateClient.exportPrivateKeys(store, keystorePassword,
                keyExportFormat, includeRoot, thumbprints));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"referenced", "details"},
            description = "List all the references to a certificate")
    public String getCertificateReferences(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.getCertificateReferences(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"referenced"},
            description = "Checks if the certificate is referenced")
    public String istCertificateReferenced(
            @Option(required = true, defaultValue = "certificates", label = ShellConst.STORE_LABEL)
            @OptionValues(provider = "enumCompletionProvider") CertificateStore store,
            @Option(required = true) String thumbprint)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.isCertificateReferenced(store, thumbprint));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"import", "system-roots"},
            description = "Import default system root certificates")
    public String importSystemRoots()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(certificateClient.importSystemRoots());
    }
}
