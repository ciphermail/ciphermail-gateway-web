/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import jakarta.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class ShellUtils
{
    private ShellUtils() {
        // empty on purpose
    }

    /**
     * Checks if the file exists and is readable. If not, throw IllegalArgumentException
     */
    public static File validateForReading(@Nonnull File file)
    {
        if (!file.exists()) {
            throw new IllegalArgumentException(String.format("File %s does not exist", file.getAbsolutePath()));
        }

        if (!file.isFile()) {
            throw new IllegalArgumentException(String.format("%s is not a file", file.getAbsolutePath()));
        }

        if (!file.canRead()) {
            throw new IllegalArgumentException(String.format("File %s is not readable", file.getAbsolutePath()));
        }

        return file;
    }

    /**
     * Checks if the file is writeable. If not, throw IllegalArgumentException
     */
    public static File validateForWriting(@Nonnull File file)
    {
        if (file.exists() && !file.canWrite()) {
            throw new IllegalArgumentException(String.format("File %s is not writeable", file.getAbsolutePath()));
        }

        return file;
    }

    /*
     * To make entering enum values easier, enum values can be entered with mixing caps and using _ and - as
     * separator. To convert back to an enum, we need to normalize the enum value.
     */
    public static String normalizeEnumValue(String value) {
        return StringUtils.replaceChars(StringUtils.upperCase(value), "-", "_");
    }

    /*
     * To make entering enum values easier, enum values can be entered with mixing caps and using _ and - as
     * separator. To convert back to an enum, we need to normalize the enum value.
     */
    public static String userFriendlyEnumValue(String value) {
        return StringUtils.replaceChars(StringUtils.lowerCase(value), "_", "-");
    }
}
