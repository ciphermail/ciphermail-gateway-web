/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.QuarantineClient;
import com.ciphermail.rest.core.QuarantineDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

@Component
@Command(command = "quarantine", group = "Quarantine")
@SuppressWarnings({"java:S6813"})
public class QuarantineCommands
{
    @Autowired
    private QuarantineClient quarantineClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "all"}, description = "List quarantined email")
    public String getQuarantinedMail(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.getQuarantinedMail(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "count"}, description = "Get the number of quarantined emails")
    public String getQuarantinedMailCount()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.getQuarantinedMailCount());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get"}, description = "Get quarantined email")
    public String getQuarantinedMailById(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.getQuarantinedMailById(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete"}, description = "Delete quarantined email")
    public String deleteQuarantinedMail(@Option(required = true) UUID id)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.deleteQuarantinedMail(id));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "mime"}, description = "Get quarantined email content")
    public String getMail(
            @Option(required = true) UUID id,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File mimeOutputFile)
    throws IOException
    {
        String mime = quarantineClient.getQuarantinedMailContent(id);

        if (mimeOutputFile != null && ShellUtils.validateForWriting(mimeOutputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(mimeOutputFile,false))
        {
            throw new CancelCommandException();
        }

        if (mimeOutputFile != null) {
            FileUtils.write(mimeOutputFile, mime, StandardCharsets.UTF_8);
        }

        return mimeOutputFile == null ? mime : null;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search"}, description = "Search quarantined email")
    public String searchQuarantinedMail(
            @Option(required = true, label = ShellConst.QUARANTINE_SEARCH_FIELD_LABEL)
                @OptionValues(provider = "enumCompletionProvider") QuarantineDTO.SearchField searchField,
            @Option(required = true) String searchKey,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.searchQuarantinedMail(
                searchField,
                searchKey,
                firstResult,
                maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"search", "count"}, description = "Search quarantined email count")
    public String searchQuarantinedMailCount(
            @Option(required = true, label = ShellConst.QUARANTINE_SEARCH_FIELD_LABEL)
            @OptionValues(provider = "enumCompletionProvider") QuarantineDTO.SearchField searchField,
            @Option(required = true) String searchKey)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.searchQuarantinedMailCount(
                searchField,
                searchKey));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"release"}, description = "Release quarantined email")
    public String releaseQuarantinedMail(
            @Option(required = true) UUID id,
            @Option(required = true, label = ShellConst.QUARANTINE_RELEASE_PROCESSOR_LABEL)
                @OptionValues(provider = "enumCompletionProvider") QuarantineDTO.ReleaseProcessor releaseProcessor)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(quarantineClient.releaseQuarantinedMail(id, releaseProcessor));
    }
}
