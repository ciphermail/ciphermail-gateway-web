/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.rest.client.DLPClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

@Component
@Command(command = "dlp", group = "DLP")
@SuppressWarnings({"java:S6813"})
public class DLPCommands
{
    @Autowired
    private DLPClient dlpClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "add"}, description = "Add a DLP policy pattern")
    public String addPolicyPattern(
            @Option(required = true) String name,
            @Option(required = false) String description,
            @Option(required = false) String notes,
            @Option(required = true) String regEx,
            @Option(required = false) String validator,
            @Option(defaultValue = "1") int threshold,
            @Option(required = true, description = ShellConst.POLICY_VIOLATION_ACTION_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PolicyViolationAction action,
            @Option(defaultValue = "false") boolean delayEvaluation,
            @Option(required = false) String matchFilter)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.addPolicyPattern(
                name,
                description,
                notes,
                regEx,
                validator,
                threshold,
                action,
                delayEvaluation,
                matchFilter));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"group", "add"}, description = "Add a DLP policy group")
    public String addPolicyGroup(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.addPolicyGroup(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "update"}, description = "Update a DLP policy pattern")
    public String updatePolicyPattern(
            @Option(required = true) String name,
            @Option(required = false) String description,
            @Option(required = false) String notes,
            @Option(required = true) String regEx,
            @Option(required = false) String validator,
            @Option(defaultValue = "1") int threshold,
            @Option(required = true, description = ShellConst.POLICY_VIOLATION_ACTION_LABEL)
            @OptionValues(provider = "enumCompletionProvider") PolicyViolationAction action,
            @Option(defaultValue = "false") boolean delayEvaluation,
            @Option(required = false) String matchFilter)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.updatePolicyPattern(
                name,
                description,
                notes,
                regEx,
                validator,
                threshold,
                action,
                delayEvaluation,
                matchFilter));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "get"}, description = "Get a DLP policy pattern")
    public String getPolicyPattern(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getPolicyPattern(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "get", "all"}, description = "Get all DLP policy patterns")
    public String getPolicyPatterns(
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getPolicyPatterns(firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "get", "all", "count"}, description = "Get the number of DLP policy patterns")
    public Long getPolicyPatternsCount()
    {
        return dlpClient.getPolicyPatternsCount();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "delete"}, description = "Delete a DLP policy pattern")
    public String deletePolicyPattern(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.deletePolicyPattern(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "rename"}, description = "Delete a DLP policy pattern")
    public String renamePolicyPattern(
            @Option(required = true) String oldName,
            @Option(required = true) String newName)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.renamePolicyPattern(oldName, newName));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "referenced"}, description = "Checks if the DLP policy pattern is referenced")
    public Boolean isPolicyPatternReferenced(@Option(required = true) String name)
    {
        return dlpClient.isPolicyPatternReferenced(name);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"pattern", "referenced", "details"}, description = "Lists all references to a DLP " +
                                                                           "policy pattern")
    public String getPolicyPatternReferencedDetails(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getPolicyPatternReferencedDetails(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"child", "add"}, description = "Add a child pattern")
    public Boolean addChildNode(
            @Option(required = true) String parentNode,
            @Option(required = true) String childNode)
    {
        return dlpClient.addChildNode(parentNode, childNode);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"child", "remove"}, description = "Remove a child pattern")
    public Boolean removeChildNode(
            @Option(required = true) String parentNode,
            @Option(required = true) String childNode)
    {
        return dlpClient.removeChildNode(parentNode, childNode);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"extract", "text"}, description = "Extract text from a MIME message")
    public String extractTextFromMimeMessage(
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file,
            @Option(required = false) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (outputFile != null && ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        String extractedText = dlpClient.extractTextFromMimeMessage(
                new FileSystemResource(ShellUtils.validateForReading(file)));

        if (outputFile != null)
        {
            FileUtils.write(outputFile, extractedText, StandardCharsets.UTF_8);

            return null;
        }

        return extractedText;
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"match-filter", "get", "all"}, description = "Lists all match filters")
    public String getMatchFilters()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getMatchFilters());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"match-filter", "get"}, description = "Get match filter")
    public String getMatchFilter(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getMatchFilter(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validator", "get", "all"}, description = "Lists all validators")
    public String getValidators()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getValidators());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"validator", "get"}, description = "Get validator")
    public String getValidator(@Option(required = true) String name)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getValidator(name));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"skip-list", "get"}, description = "Get skip list")
    public String getSkipList()
    {
        return dlpClient.getSkipList();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"skip-list", "set"}, description = "Set skip list")
    public void setSkipList(@Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    {
        dlpClient.setSkipList(new FileSystemResource(ShellUtils.validateForReading(file)));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "patterns"}, description = "Assigns policy patterns to a user")
    public void setUserPolicyPatterns(
            @Option(required = true) String email,
            @Option(required = true) List<String> patternNames)
    {
        dlpClient.setUserPolicyPatterns(email, patternNames);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "reset", "patterns"}, description = "Removes policy patterns assigned to a user")
    public void resetUserPolicyPatterns(@Option(required = true) String email)
    {
        dlpClient.setUserPolicyPatterns(email, Collections.emptyList());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "patterns"}, description = "Lists policy patterns assigned to a user")
    public String getUserPolicyPatterns(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getUserPolicyPatterns(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "patterns"}, description = "Assigns policy patterns to a domain")
    public void setDomainPolicyPatterns(
            @Option(required = true) String domain,
            @Option(required = true) List<String> patternNames)
    {
        dlpClient.setDomainPolicyPatterns(domain, patternNames);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "reset", "patterns"}, description = "Removes policy patterns assigned to a domain")
    public void resetDomainPolicyPatterns(@Option(required = true) String domain)
    {
        dlpClient.setDomainPolicyPatterns(domain, Collections.emptyList());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "patterns"}, description = "Lists policy patterns assigned to a domain")
    public String getDomainPolicyPatterns(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getDomainPolicyPatterns(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "set", "patterns"}, description = "Assigns policy patterns to the global settings")
    public void setGlobalPolicyPatterns(@Option(required = true) List<String> patternNames)
    {
        dlpClient.setGlobalPolicyPatterns(patternNames);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "reset", "patterns"}, description = "Removes policy patterns assigned to the global settings")
    public void resetGlobalPolicyPatterns()
    {
        dlpClient.setGlobalPolicyPatterns(Collections.emptyList());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"global", "get", "patterns"}, description = "Lists policy patterns assigned to the global settings")
    public String getGlobalPolicyPatterns()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(dlpClient.getGlobalPolicyPatterns());
    }
}
