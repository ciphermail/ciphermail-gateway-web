/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.KeyStoreClient;
import com.ciphermail.rest.core.KeyStoreName;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
@Command(command = "keystore", group = "KeyStore")
@SuppressWarnings({"java:S6813"})
public class KeyStoreCommands
{
    @Autowired
    private KeyStoreClient keyStoreClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"aliases"}, description = "Lists all key aliases")
    public String getAliases(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true, defaultValue = "0") int firstResult,
            @Option(required = true, defaultValue = CipherMailShellServices.DEFAULT_MAX_RESULTS_STRING) int maxResults)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(keyStoreClient.getAliases(keystoreName, firstResult, maxResults));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"aliases", "count"}, description = "Return the number of keystore entries")
    public Long getKeyStoreSize(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName)
    {
        return keyStoreClient.getKeyStoreSize(keystoreName);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "certificate"}, description = "Get the certificate with the given alias")
    public String getCertificate(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(keyStoreClient.getCertificate(keystoreName, alias));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get", "chain"}, description = "Get the certificate chain of the certificate with the given alias")
    public String getChain(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
            @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(keyStoreClient.getChain(keystoreName, alias));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"contains", "alias"}, description = "Checks whether an entry with the given alias exists")
    public Boolean containsAlias(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    {
        return keyStoreClient.containsAlias(keystoreName, alias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"is", "certificate"}, description = "Checks whether the entry with the given alias is " +
                                                            "a certificate")
    public Boolean isCertificateEntry(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    {
        return keyStoreClient.isCertificateEntry(keystoreName, alias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"is", "key"}, description = "Checks whether the entry with the given alias is a key")
    public Boolean isKeyEntry(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    {
        return keyStoreClient.isKeyEntry(keystoreName, alias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"delete"}, description = "Delete the entry with the given alias")
    public void deleteEntry(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String alias)
    {
        keyStoreClient.deleteEntry(keystoreName, alias);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"rename"}, description = "Rename the entry with the given alias")
    public void renameEntry(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String oldAlias,
            @Option(required = true) String newAlias,
            @Option(required = true) String keystorePassword)
    {
        keyStoreClient.renameEntry(keystoreName, oldAlias, newAlias, keystorePassword);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"export", "pkcs12"}, description = "Export private keys to PKCS#12")
    public void exportPrivateKeys(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = false) String keystorePassword,
            @Option(required = true) String exportPassword,
            @Option(required = true) List<String> aliases,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, keyStoreClient.exportToPKCS12(keystoreName, keystorePassword,
                exportPassword, aliases));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"import", "pkcs12"}, description = "Import private keys from a PKCS#12 file")
    public Integer importPKCS12(
            @Option(required = true, label = ShellConst.KEYSTORE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") KeyStoreName keystoreName,
            @Option(required = true) String keystorePassword,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    {
        return keyStoreClient.importPKCS12(keystoreName, keystorePassword,
                new FileSystemResource(ShellUtils.validateForReading(file)));
    }
}
