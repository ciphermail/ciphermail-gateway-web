/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.rest.client.AcmeClient;
import com.ciphermail.rest.core.AcmeDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.List;

@Component
@Command(command = {"acme"}, group = "ACME")
@SuppressWarnings({"java:S6813"})
public class AcmeCommands
{
    @Autowired
    private AcmeClient acmeClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"account", "create-keypair"}, description = "Create an ACME account keypair")
    public void createAccountKeyPair(
            @Option(required = true, label = ShellConst.KEY_ALGORITHM_LABEL, defaultValue = "secp256r1")
                @OptionValues(provider = "enumCompletionProvider") AcmeDTO.KeyAlgorithm keyAlgorithm,
            @Option(required = false, defaultValue = "false") boolean replaceExistingKey)
    {
        acmeClient.createAccountKeyPair(keyAlgorithm, replaceExistingKey);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"account", "keypair-exists"}, description = "Checks if an ACME account keypair exists")
    public Boolean accountKeyPairExists()
    {
        return acmeClient.accountKeyPairExists();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get-tos"}, description = "Gets the Terms Of Service URI")
    public URI getTermsOfService()
    {
        return acmeClient.getTermsOfService();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"set-tos-accepted"}, description = "Accept the Terms Of Service")
    public void setTosAccepted(@Option(required = true, defaultValue = "true") boolean accepted)
    {
        acmeClient.setTosAccepted(accepted);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"get-tos-accepted"}, description = "Accept the Terms Of Service")
    public boolean getTosAccepted()
    {
        return acmeClient.isTosAccepted();
    }
    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"account", "find-or-register"}, description = "Return or register an account")
    public String findOrRegisterAccount()
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(acmeClient.findOrRegisterAccount());
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"account", "deactivate"}, description = "Deactivate account")
    public void deactivateAccount()
    {
        acmeClient.deactivateAccount();
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"order", "renew"}, description = "Renew certificate")
    public void renewCertificate(@Option(required = false, defaultValue = "false") boolean forceRenewal)
    {
        acmeClient.renewCertificate(forceRenewal);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"order", "new"}, description = "Order a new certificate")
    public void orderCertificate(
            @Option(required = true, label = ShellConst.KEY_ALGORITHM_LABEL, defaultValue = "secp256r1")
                @OptionValues(provider = "enumCompletionProvider") AcmeDTO.KeyAlgorithm keyAlgorithm,
            @Option(required = true) List<String> domains)
    {
        acmeClient.orderCertificate(keyAlgorithm, domains);
    }
}
