/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

public class ShellConst
{
    private ShellConst() {
        // empty in purpose
    }

    // value property names
    public static final String BASE_PROPERTY = "ciphermail.shell";
    public static final String SILENT_PROPERTY = BASE_PROPERTY + ".silent";

    // command labels
    public static final String STORE_LABEL = "[roots, certificates]";
    public static final String EXPIRED_LABEL = "[match-all, match-unexpired-only, match-expired-only]";
    public static final String MISSING_KEYALIAS_LABEL = "[allowed, not-allowed]";
    public static final String MATCH_LABEL = "[exact, like]";
    public static final String CERT_IMPORT_ACTION_LABEL = "[import-all,skip-self-signed,must-be-self-signed]";
    public static final String AUTHENTICATION_TYPE_LABEL = "[username-password, oidc]";
    public static final String CA_CERT_SIG_ALG_LABEL = "[sha256-with-rsa-encryption, " +
                                                       "sha384-with-rsa-encryption, " +
                                                       "sha512-with-rsa-encryption]";
    public static final String CA_KEYUSAGE_FILTER_LABEL = "[all, crl-sign]";
    public static final String OBJECT_ENCODING_LABEL = "[der, pem]";
    public static final String MISSING_KEY_ACTION_LABEL = "[skip-certificate-only-entry, add-certificate-only-entry]";
    public static final String KEY_EXPORT_FORMAT_LABEL = "[pkcs12, pem]";
    public static final String SORT_DIRECTION_LABEL = "[asc, desc]";
    public static final String NAMED_CERTIFICATE_CATEGORY_LABEL = "[additional]";
    public static final String CTL_ENTRY_STATUS_LABEL = "[blacklisted, whitelisted]";
    public static final String POLICY_VIOLATION_ACTION_LABEL = "[warn, must-encrypt, quarantine, block]";
    public static final String KEYSTORE_LABEL = "[cert, pgp, dkim]";
    public static final String MAIL_QUEUE_FIELD_LABEL = "[sender, recipient, name]";
    public static final String PGP_KEY_TYPE_LABEL = "[master-key, sub-key, all]";
    public static final String PGP_SEARCH_FIELD_LABEL = "[email, user-id, key-id, fingerprint]";
    public static final String PGP_KEY_GENERATION_TYPE_LABEL = "[rsa-1024, rsa-2048, rsa-3072, rsa-4096, " +
                                                               "ecc-nist-p-256, ecc-nist-p-384, ecc-nist-p-521, " +
                                                               "ecc-brainpool-p-256, ecc-brainpool-p-384, " +
                                                               "ecc-brainpool-p-512, ecc-secp256k1]";
    public static final String PGP_TRUST_LIST_STATUS_LABEL = "[trusted, untrusted]";
    public static final String SMS_SORT_COLUMN_LABEL = "[phonenumber, created, last-try]";
    public static final String POSTFIX_MAP_TYPE_LABEL = "[btree, cidr, hash, pcre, regexp]";
    public static final String QUARANTINE_SEARCH_FIELD_LABEL = "[id, message-id, subject, recipients, sender, from]";
    public static final String QUARANTINE_RELEASE_PROCESSOR_LABEL = "[default, encrypt, as-is]";
    public static final String KEY_ALGORITHM_LABEL = "[rsa-2048, rsa-3072, rsa-4096, secp521r1, secp384r1, secp256r1]";
    public static final String MTA_PORT_LABEL = "[direct-delivery, mpa]";
}
