/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.core.common.security.openpgp.PGPKeyType;
import com.ciphermail.core.common.security.openpgp.PGPSearchField;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.AcmeDTO;
import com.ciphermail.rest.core.CAKeyUsageFilter;
import com.ciphermail.rest.core.CertificateImportAction;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.KeyExportFormat;
import com.ciphermail.rest.core.KeyStoreName;
import com.ciphermail.rest.core.LogDTO;
import com.ciphermail.rest.core.MTADTO;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.QuarantineDTO;
import com.ciphermail.rest.core.TLSDTO;
import jakarta.annotation.Nonnull;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.command.CommandOption;
import org.springframework.shell.completion.CompletionProvider;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
public class EnumConverters
{
    private EnumConverters() {
        // empty on purpose
    }

    /*
     * Case-insensitive converter from String to AuthenticationType Enum
     */
    @Component
    public static class AuthenticationConverter implements Converter<String, AuthenticationType>
    {
        @Override
        public AuthenticationType convert(@Nonnull String value) {
            return AuthenticationType.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to CACertificateSignatureAlgorithm Enum
     */
    @Component
    public static class CACertificateSignatureAlgorithmConverter implements Converter<String, CACertificateSignatureAlgorithm>
    {
        @Override
        public CACertificateSignatureAlgorithm convert(@Nonnull String value) {
            return CACertificateSignatureAlgorithm.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to CAKeyUsageFilter Enum
     */
    @Component
    public static class CAKeyUsageFilterConverter implements Converter<String, CAKeyUsageFilter>
    {
        @Override
        public CAKeyUsageFilter convert(@Nonnull String value) {
            return CAKeyUsageFilter.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to Match Enum
     */
    @Component
    public static class MatchConverter implements Converter<String, Match>
    {
        @Override
        public Match convert(@Nonnull String value) {
            return Match.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to CertificateStore Enum
     */
    @Component
    public static class CertificateStoreConverter implements Converter<String, CertificateStore>
    {
        @Override
        public CertificateStore convert(@Nonnull String value) {
            return CertificateStore.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to Expired Enum
     */
    @Component
    public static class ExpiredConverter implements Converter<String, Expired>
    {
        @Override
        public Expired convert(@Nonnull String value) {
            return Expired.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MissingKeyAlias Enum
     */
    @Component
    public static class MissingKeyAliasConverter implements Converter<String, MissingKeyAlias>
    {
        @Override
        public MissingKeyAlias convert(@Nonnull String value) {
            return MissingKeyAlias.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MissingKeyAlias Enum
     */
    @Component
    public static class CertificateImportActionConverter implements Converter<String, CertificateImportAction>
    {
        @Override
        public CertificateImportAction convert(@Nonnull String value) {
            return CertificateImportAction.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MissingKeyAlias Enum
     */
    @Component
    public static class ObjectEncodingConverter implements Converter<String, ObjectEncoding>
    {
        @Override
        public ObjectEncoding convert(@Nonnull String value) {
            return ObjectEncoding.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MissingKeyAction Enum
     */
    @Component
    public static class MissingKeyActionConverter implements Converter<String, MissingKeyAction>
    {
        @Override
        public MissingKeyAction convert(@Nonnull String value) {
            return MissingKeyAction.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MissingKeyAction Enum
     */
    @Component
    public static class KeyExportFormatTypeConverter implements Converter<String, KeyExportFormat>
    {
        @Override
        public KeyExportFormat convert(@Nonnull String value) {
            return KeyExportFormat.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to SortDirection Enum
     */
    @Component
    public static class SortDirectionConverter implements Converter<String, SortDirection>
    {
        @Override
        public SortDirection convert(@Nonnull String value) {
            return SortDirection.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to NamedCertificateCategory Enum
     */
    @Component
    public static class NamedCertificateCategoryConverter implements Converter<String, NamedCertificateCategory>
    {
        @Override
        public NamedCertificateCategory convert(@Nonnull String value) {
            return NamedCertificateCategory.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to CTLEntryStatus Enum
     */
    @Component
    public static class CTLEntryStatusConverter implements Converter<String, CTLEntryStatus>
    {
        @Override
        public CTLEntryStatus convert(@Nonnull String value) {
            return CTLEntryStatus.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PolicyViolationAction Enum
     */
    @Component
    public static class PolicyViolationActionConverter implements Converter<String, PolicyViolationAction>
    {
        @Override
        public PolicyViolationAction convert(@Nonnull String value) {
            return PolicyViolationAction.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to KeyStoreName Enum
     */
    @Component
    public static class KeyStoreNameConverter implements Converter<String, KeyStoreName>
    {
        @Override
        public KeyStoreName convert(@Nonnull String value) {
            return KeyStoreName.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MailDTO.MailQueueField Enum
     */
    @Component
    public static class MailQueueFieldConverter implements Converter<String, MailDTO.MailQueueField>
    {
        @Override
        public MailDTO.MailQueueField convert(@Nonnull String value) {
            return MailDTO.MailQueueField.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PGPKeyType Enum
     */
    @Component
    public static class PGPKeyTypeConverter implements Converter<String, PGPKeyType>
    {
        @Override
        public PGPKeyType convert(@Nonnull String value) {
            return PGPKeyType.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PGPSearchField Enum
     */
    @Component
    public static class PGPSearchFieldConverter implements Converter<String, PGPSearchField>
    {
        @Override
        public PGPSearchField convert(@Nonnull String value) {
            return PGPSearchField.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PGPKeyGenerationType Enum
     */
    @Component
    public static class PGPKeyGenerationTypeConverter implements Converter<String, PGPKeyGenerationType>
    {
        @Override
        public PGPKeyGenerationType convert(@Nonnull String value) {
            return PGPKeyGenerationType.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PGPTrustListStatus Enum
     */
    @Component
    public static class PGPTrustListStatusConverter implements Converter<String, PGPTrustListStatus>
    {
        @Override
        public PGPTrustListStatus convert(@Nonnull String value) {
            return PGPTrustListStatus.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to SortColumn Enum
     */
    @Component
    public static class SortColumnConverter implements Converter<String, SortColumn>
    {
        @Override
        public SortColumn convert(@Nonnull String value) {
            return SortColumn.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to PostfixMapType Enum
     */
    @Component
    public static class PostfixMapTypeConverter implements Converter<String, MTADTO.PostfixMapType>
    {
        @Override
        public MTADTO.PostfixMapType convert(@Nonnull String value) {
            return MTADTO.PostfixMapType.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to MTAPort Enum
     */
    @Component
    public static class MTAPortConverter implements Converter<String, MTADTO.MTAPort>
    {
        @Override
        public MTADTO.MTAPort convert(@Nonnull String value) {
            return MTADTO.MTAPort.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to LogUnit Enum
     */
    @Component
    public static class LogUnitConverter implements Converter<String, LogDTO.LogTarget>
    {
        @Override
        public LogDTO.LogTarget convert(@Nonnull String value) {
            return LogDTO.LogTarget.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to SearchField Enum
     */
    @Component
    public static class QuarantineSearchFieldConverter implements Converter<String, QuarantineDTO.SearchField>
    {
        @Override
        public QuarantineDTO.SearchField convert(@Nonnull String value) {
            return QuarantineDTO.SearchField.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to ReleaseProcessorEnum
     */
    @Component
    public static class QuarantineReleaseProcessorConverter implements Converter<String, QuarantineDTO.ReleaseProcessor>
    {
        @Override
        public QuarantineDTO.ReleaseProcessor convert(@Nonnull String value) {
            return QuarantineDTO.ReleaseProcessor.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to KeyAlgorithm Enum
     */
    @Component
    public static class TLSKeyAlgorithmConverter implements Converter<String, TLSDTO.KeyAlgorithm>
    {
        @Override
        public TLSDTO.KeyAlgorithm convert(@Nonnull String value) {
            return TLSDTO.KeyAlgorithm.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /*
     * Case-insensitive converter from String to KeyAlgorithm Enum
     */
    @Component
    public static class AcmeKeyAlgorithmConverter implements Converter<String, AcmeDTO.KeyAlgorithm>
    {
        @Override
        public AcmeDTO.KeyAlgorithm convert(@Nonnull String value) {
            return AcmeDTO.KeyAlgorithm.valueOf(ShellUtils.normalizeEnumValue(value));
        }
    }

    /**
     * Completion provider for Enum options
     */
    @Bean
    public CompletionProvider enumCompletionProvider()
    {
        return completionContext ->
        {
            List<CompletionProposal> proposals = new LinkedList<>();

            CommandOption commandOption = completionContext.getCommandOption();

            if (commandOption != null)
            {
                ResolvableType resolvableType = completionContext.getCommandOption().getType();

                if (resolvableType != null)
                {
                    Class<?> rawType = resolvableType.getRawClass();

                    if (rawType != null && rawType.isEnum())
                    {
                        for (Enum<?> enumConstant : (Enum<?>[]) rawType.getEnumConstants()) {
                            proposals.add(new CompletionProposal(ShellUtils.userFriendlyEnumValue(enumConstant.name())));
                        }

                    }
                }
            }

            return proposals;
        };
    }
}
