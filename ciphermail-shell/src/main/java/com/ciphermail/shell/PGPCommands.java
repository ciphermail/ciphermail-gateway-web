/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.shell;

import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.rest.client.PGPClient;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.CommandAvailability;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.command.annotation.OptionValues;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Component
@Command(command = {"pgp"}, group = "PGP")
@SuppressWarnings({"java:S6813"})
public class PGPCommands
{
    @Autowired
    private PGPClient pgpClient;

    @Autowired
    private JSONPrettyPrinter jsonPrettyPrinter;

    @Autowired
    private StandardShellComponents standardShellComponents;

    @Value("${" + ShellConst.SILENT_PROPERTY + "}")
    private boolean silent;

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"keyring", "import"}, description = "Import keys from a PGP keyring")
    public Integer importPKCS12(
            @Option(required = false) String password,
            @Option(required = false, defaultValue = "false") boolean ignoreParsingErrors,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File file)
    {
        return pgpClient.importKeyring(password, ignoreParsingErrors,
                new FileSystemResource(ShellUtils.validateForReading(file)));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"keyring", "export", "public-keys"}, description = "Export public keys to a keyring")
    public void exportPublicKeys(
            @Option(required = true) List<UUID> ids,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, pgpClient.exportPublicKeys(ids));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"keyring", "export", "secret-keys"}, description = "Export secret keys to a password " +
                                                                           "protected keyring")
    public void exportSecretKeys(
            @Option(required = true) List<UUID> ids,
            @Option(required = true) String password,
            @Option(required = true) @OptionValues(provider = "fileCompletionProvider") File outputFile)
    throws IOException
    {
        if (ShellUtils.validateForWriting(outputFile).exists() && !silent &&
            !standardShellComponents.overwriteFileConfirmation(outputFile,false))
        {
            throw new CancelCommandException();
        }

        FileUtils.writeByteArrayToFile(outputFile, pgpClient.exportSecretKeys(ids, password));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"set", "email-and-domains"}, description = "Configure email and domains for a key")
    public void setPGPKeyEmailAndDomains(
            @Option(required = true) UUID id,
            @Option List<String> emailAndDomains)
    {
        pgpClient.setPGPKeyEmailAndDomains(id, emailAndDomains);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "all", "encryption", "keys"}, description = "List available encryption keys " +
                                                                                   "for a user")
    public String getEncryptionKeysForEmail(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getEncryptionKeysForEmail(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "all", "encryption", "keys"}, description = "List available encryption keys " +
                                                                                     "for a domain")
    public String getEncryptionKeysForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getEncryptionKeysForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "all", "signing", "keys"}, description = "List signing keys for a user")
    public String getAvailableSigningKeysForEmail(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getAvailableSigningKeysForEmail(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "all", "signing", "keys"}, description = "List signing keys for a domain")
    public String getAvailableSigningKeysForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getAvailableSigningKeysForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "get", "signing", "key"}, description = "Get signing key for a user")
    public String getSigningKeyForUser(@Option(required = true) String email)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getSigningKeyForUser(email));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "get", "signing", "key"}, description = "Get signing key for a domain")
    public String getSigningKeyForDomain(@Option(required = true) String domain)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.getSigningKeyForDomain(domain));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"user", "set", "signing", "key"}, description = "Set signing key for a user")
    public void setSigningKeyForUser(@Option(required = true) String email, @Option(required = true) UUID id)
    throws JsonProcessingException
    {
        pgpClient.setSigningKeyForUser(email, id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"domain", "set", "signing", "key"}, description = "Set signing key for a domain")
    public void setSigningKeyForDomain(@Option(required = true) String domain, @Option(required = true) UUID id)
    throws JsonProcessingException
    {
        pgpClient.setSigningKeyForDomain(domain, id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"keyring", "generate", "secret-key",}, description = "Generate a secret key")
    public String generateSecretKeyRing(
            @Option(required = true) String email,
            @Option(required = true) String name,
            @Option(required = true, defaultValue = "rsa-3072", label = ShellConst.PGP_KEY_GENERATION_TYPE_LABEL)
                    @OptionValues(provider = "enumCompletionProvider") PGPKeyGenerationType keyType,
            @Option(required = false, defaultValue = "false") boolean publishKeys)
    throws JsonProcessingException
    {
        return jsonPrettyPrinter.prettyPrintJSON(pgpClient.generateSecretKeyRing(email, name, keyType, publishKeys));
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"revoke", "key"}, description = "Revoke key with the given id")
    public void revokeKey(@Option(required = true) UUID id)
    {
        pgpClient.revokeKey(id);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"is", "userid", "valid"}, description = "Checks if a userID is valid")
    public Boolean isUserIDValid(
            @Option(required = true) UUID id,
            @Option(required = true) String userId)
    {
        return pgpClient.isUserIDValid(id, userId);
    }

    @CommandAvailability(provider = CipherMailShellServices.BACKEND_AVAILABILITY_PROVIDER)
    @Command(command = {"is", "userid", "revoked"}, description = "Checks if a userID is revoked")
    public Boolean isUserIDRevoked(
            @Option(required = true) UUID id,
            @Option(required = true) String userId)
    {
        return pgpClient.isUserIDRevoked(id, userId);
    }
}
