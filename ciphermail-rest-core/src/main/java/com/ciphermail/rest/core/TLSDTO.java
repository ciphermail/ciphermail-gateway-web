/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public class TLSDTO
{
    private TLSDTO() {
        // empty on purpose
    }

    public enum KeyAlgorithm {
        RSA_2048,
        RSA_3072,
        RSA_4096,
        SECP521R1,
        SECP384R1,
        SECP256R1}

    public record CSRStoreEntry(
            @NotNull String id,
            @NotNull long date,
            @NotNull String subject,
            @NotNull String keyAlgorithm,
            @NotNull int keyLength,
            @NotNull boolean certificateAvailable){}

    public static class Request
    {
        @JsonProperty
        private String email;

        @JsonProperty
        private String organisation;

        @JsonProperty
        private String organisationalUnit;

        @JsonProperty
        private String countryCode;

        @JsonProperty
        private String state;

        @JsonProperty
        private String locality;

        @JsonProperty
        private String commonName;

        @JsonProperty
        private String givenName;

        @JsonProperty
        private String surname;

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonProperty
        private List<String> domains;

        public String getEmail() {
            return email;
        }

        public Request setEmail(String email)
        {
            this.email = email;
            return this;
        }

        public String getOrganisation() {
            return organisation;
        }

        public Request setOrganisation(String organisation)
        {
            this.organisation = organisation;
            return this;
        }

        public String getOrganisationalUnit() {
            return organisationalUnit;
        }

        public Request setOrganisationalUnit(String organisationalUnit) {
            this.organisationalUnit = organisationalUnit;
            return this;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public Request setCountryCode(String countryCode)
        {
            this.countryCode = countryCode;
            return this;
        }

        public String getState() {
            return state;
        }

        public Request setState(String state)
        {
            this.state = state;
            return this;
        }

        public String getLocality() {
            return locality;
        }

        public Request setLocality(String locality)
        {
            this.locality = locality;
            return this;
        }

        public String getCommonName() {
            return commonName;
        }

        public Request setCommonName(String commonName)
        {
            this.commonName = commonName;
            return this;
        }

        public String getGivenName() {
            return givenName;
        }

        public Request setGivenName(String givenName)
        {
            this.givenName = givenName;
            return this;
        }

        public String getSurname() {
            return surname;
        }

        public Request setSurname(String surname)
        {
            this.surname = surname;
            return this;
        }

        public List<String> getDomains() {
            return domains;
        }

        public @Nonnull Request setDomains(List<String> domains)
        {
            this.domains = domains;
            return this;
        }
    }
}
