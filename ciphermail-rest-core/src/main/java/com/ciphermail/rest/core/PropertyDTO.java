/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.core;

import jakarta.validation.constraints.NotNull;

import java.util.List;
import java.util.Map;

public class PropertyDTO
{
    private PropertyDTO() {
        // empty on purpose
    }

    /*
     * Property value
     */
    public record PropertyRequest(
            @NotNull String name,
            String value) {}

    /*
     * property return value
     */
    public record PropertyValue(
            @NotNull String name,
            String value,
            String parentValue,
            @NotNull boolean inherited) {}

    /*
     * Details for the type of the property. For an enum type, provide the enum options
     */
    public record TypeDetails(
            @NotNull String type,
            @NotNull Map<String, Object> options){}

    /*
     * Details for a setter or getter property
     */
    @SuppressWarnings("java:S6218")
    public record UserPropertyDetails(
            @NotNull String name,
            @NotNull TypeDetails type,
            @NotNull String editor,
            @NotNull String displayNameKey,
            @NotNull String descriptionKey,
            @NotNull String helpKey,
            @NotNull boolean user,
            @NotNull boolean domain,
            @NotNull boolean global,
            @NotNull int order){}

    /*
     * Setter and getter details for a property
     */
    public record UserPropertyDescriptorDetails(
            @NotNull UserPropertyDetails getter,
            @NotNull UserPropertyDetails setter){}

    /*
     * Details for a User Properties object. This is a class which contains multiple properties
     */
    public record UserPropertiesDetails(
            @NotNull String category,
            @NotNull String displayName,
            @NotNull String displayNameKey,
            @NotNull String descriptionKey,
            @NotNull int order,
            @NotNull List<UserPropertyDescriptorDetails> properties,
            @NotNull List<UserPropertiesDetails> children){}
}
