# -*- mode: ruby -*-
# vi: set ft=ruby :

# VMs are not started by default. To start a VM, use the up command.
# Example: vagrant up debian

Vagrant.configure(2) do |config|
    # set default sync folder to build dir
    config.vm.synced_folder 'build', '/vagrant', disabled: false, create: true

    # do not generate a new SSH key
    # this box is only for testing
    config.ssh.insert_key = false

    config.vm.provider "libvirt" do |libvirt|
        libvirt.cpus = 1
        libvirt.memory = 2048
    end

    config.vm.define "build", autostart: false do |node|
        node.vm.box = "debian/bookworm64"
        # sync project
        node.vm.synced_folder '.', '/vagrant', disabled: false, create: true

        node.vm.provider "libvirt" do |nodeoverride|
            nodeoverride.cpus = 4
            nodeoverride.memory = 8192
        end
    end

    config.vm.define "debian-postgres", autostart: false do |node|
        node.vm.box = "debian/bookworm64"

        node.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/ciphermail-backend-postgres.yml"

            ansible.groups = {
              "ciphermail_backend" => ["debian-postgres"],
            }

            ansible.extra_vars = {
              ciphermail_base__cli_user: 'vagrant'
            }
        end
    end

    config.vm.define "debian-mariadb", autostart: false do |node|
        node.vm.box = "debian/bookworm64"

        node.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/ciphermail-backend-mariadb.yml"

            ansible.groups = {
              "ciphermail_backend" => ["debian-mariadb"],
            }

            ansible.extra_vars = {
              ciphermail_base__cli_user: 'vagrant'
            }
        end
    end

    config.vm.define "alma-postgres", autostart: false do |node|
        node.vm.box = "almalinux/9"

        node.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/ciphermail-backend-postgres.yml"

            ansible.groups = {
              "ciphermail_backend" => ["alma-postgres"],
            }

            ansible.extra_vars = {
              ciphermail_base__cli_user: 'vagrant'
            }
        end
    end

    config.vm.define "alma-mariadb", autostart: false do |node|
        node.vm.box = "almalinux/9"

        node.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/ciphermail-backend-mariadb.yml"

            ansible.groups = {
              "ciphermail_backend" => ["alma-mariadb"],
            }

            ansible.extra_vars = {
              ciphermail_base__cli_user: 'vagrant'
            }
        end
    end
end
