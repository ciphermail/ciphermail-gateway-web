/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import java.io.IOException;
import java.util.List;

/**
 * Interface to the Postfix postqueue command.
 */
public interface PostQueue
{
    /**
     * Flush the queue: attempt to deliver all queued mail.
     */
    void flushQueue()
    throws IOException;

    /**
     * Schedule immediate delivery of deferred mail with the specified queue ID.
     */
    void rescheduleDeferredMail(String queueID)
    throws IOException;

    /**
     * From: <a href="http://www.postfix.org/postqueue.1.html">...</a>
     * <p>
     * Produce a queue listing in JSON format, based on output from the showq(8) daemon.ine character to support
     * simple streaming parsers. See "JSON OBJECT FORMAT" below for details.
     * <p>
     *
     * JSON OBJECT FORMAT
     *        Each  JSON  object represents one queue file; it is emitted as a single
     *        text line followed by a newline character.
     * <p>
     *        Object members have string values unless indicated otherwise.  Programs
     *        should ignore object members that are not listed here; the list of mem-
     *        bers is expected to grow over time.
     * <p>
     *        queue_name
     *               The name of the queue where the message was  found.   Note  that
     *               the  contents  of  the  mail  queue may change while it is being
     *               listed; some messages may appear more than once, and  some  mes-
     *               sages may be missed.
     * <p>
     *        queue_id
     *               The queue file name. The queue_id may be reused within a Postfix
     *               instance unless "enable_long_queue_ids = true" and time is mono-
     *               tonic.   Even  then,  the  queue_id is not expected to be unique
     *               between different  Postfix  instances.   Management  tools  that
     *               require  a  unique  name  should  combine  the queue_id with the
     *               myhostname setting of the Postfix instance.
     * <p>
     *        arrival_time
     *               The number of seconds since the start of the UNIX epoch.
     *
     *        message_size
     *               The number of bytes in the message header and body. This  number
     *               does  not  include  message envelope information. It is approxi-
     *               mately equal to the number of bytes that  would  be  transmitted
     *               via SMTP including the <CR><LF> line endings.
     * <p>
     *        forced_expire
     *               The  message is forced to expire (true or false).  See the post-
     *               super(1) options -e or -f.
     * <p>
     *               This feature is available in Postfix 3.5 and later.
     * <p>
     *        sender The envelope sender address.
     * <p>
     *        recipients
     *               An array containing zero or more objects with members:
     * <p>
     *               address
     *                      One recipient address.
     * <p>
     *               delay_reason
     *                      If present, the reason  for  delayed  delivery.   Delayed
     *                      recipients  may  have no delay reason, for example, while
     *                      delivery is in progress, or after the system was  stopped
     *                      before it could record the reason.
     * This feature is available in Postfix 3.1 and later.
     */
    List<String> listQueue(Integer firstResult, Integer maxResults)
    throws IOException;

    /**
     * Returns the number of items in the mail queue
     */
    long getQueueSize()
    throws IOException;
}
