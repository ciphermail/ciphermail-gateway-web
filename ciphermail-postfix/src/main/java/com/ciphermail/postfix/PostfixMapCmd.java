/*
 * Copyright (c) 2018-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.postfix;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.List;

public interface PostfixMapCmd
{
    String readMap(@Nonnull String filename)
    throws IOException;

    void writeMap(@Nonnull String filename, @Nonnull String content)
    throws IOException;

    @Nonnull List<String> listMaps()
    throws IOException;

    void deleteMap(@Nonnull String filename)
    throws IOException;
}
