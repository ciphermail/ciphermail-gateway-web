/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import java.io.IOException;
import java.util.List;

/**
 * Manages Postfix map files
 *
 */
public interface PostfixMapManager
{
    /**
     * Returns all the available maps
     */
    List<PostfixMap> getMaps()
    throws IOException;

    /**
     * Returns the map with the given map type and name. If the map does not exist, null will be returned.
     * <p>
     * Note: if a new map is created but the content is not yet set, null will be returned.
     *
     */
    PostfixMap getMap(MapType mapType, String name)
    throws IOException;

    /**
     * Gets a PostfixMap. If the type and name combination already exist, it will overwrite the existing mapping.
     * <p>
     * Note: the map file is not created until {@link PostfixMap#setContent(String)} is called. If
     * {@link PostfixMap#getContent()} is called on a new map for which {@link PostfixMap#setContent(String)} is not
     * yet called, an {@link IOException} will be raised.
     */
    PostfixMap createMap(MapType mapType, String name);

    /**
     * Deletes the map with the given name and type
     */
    void deleteMap(MapType mapType, String name )
    throws IOException;
}
