/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;

/**
 * Default implementation of PostfixMap
 *
 */
public class PostfixMapImpl implements PostfixMap
{
    /*
     * Map type (hash, cidr etc.)
     */
    private final MapType mapType;

    /*
     * Name of the map
     */
    private final String name;

    private final PostfixMapCmd postfixMapCmd;

    PostfixMapImpl(@Nonnull MapType mapType, @Nonnull String name, @Nonnull PostfixMapCmd postfixMapCmd)
    {
        this.mapType = Objects.requireNonNull(mapType);
        this.name = Objects.requireNonNull(name);
        this.postfixMapCmd = Objects.requireNonNull(postfixMapCmd);
    }

    @Override
    public @Nonnull MapType getMapType() {
        return mapType;
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    @Override
    public @Nonnull String getFilename() {
        return PostfixMapUtils.getFilename(mapType, name);
    }

    @Override
    public String getContent()
    throws IOException
    {
        return postfixMapCmd.readMap(getFilename());
    }

    @Override
    public void setContent(@Nonnull String content)
    throws IOException
    {
        postfixMapCmd.writeMap(getFilename(), content);
    }

    @Override
    public String toString() {
        return getFilename();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }

        if (obj.getClass() != getClass()) {
          return false;
        }

        PostfixMapImpl rhs = (PostfixMapImpl) obj;

        return new EqualsBuilder()
            .append(mapType, rhs.mapType)
            .append(name, rhs.name)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().
            append(mapType).
            append(name).
            toHashCode();
    }
}
