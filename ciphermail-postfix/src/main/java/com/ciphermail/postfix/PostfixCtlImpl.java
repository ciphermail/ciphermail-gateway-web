/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.postfix;

import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

public class PostfixCtlImpl extends PostfixBaseCommand implements PostfixCtl
{
    /*
     * The command to run
     */
    private final String command;

    public PostfixCtlImpl(@Nonnull String command) {
        this.command = Objects.requireNonNull(command);
    }

    @Override
    public void startPostfix()
    throws IOException
    {
        executeCommand(List.of("start"));
    }

    @Override
    public void stopPostfix()
    throws IOException
    {
        executeCommand(List.of("stop"));
    }

    @Override
    public void restartPostfix()
    throws IOException
    {
        executeCommand(List.of("restart"));
    }

    @Override
    public String getPostfixStatus()
    throws IOException
    {
        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = new SizeLimitedOutputStream(WriterOutputStream.builder().setWriter(outputWriter)
                .setCharset(StandardCharsets.UTF_8).get(), getMaxErrorSize(), false);

        try {
            // if postfix is not running, exit code will be > 0
            executeCommand(List.of("status"), null, outputStream, false);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
        }

        return outputWriter.toString();
    }

    @Override
    public boolean isPostfixRunning()
    throws IOException
    {
        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = new SizeLimitedOutputStream(WriterOutputStream.builder().setWriter(outputWriter)
                .setCharset(StandardCharsets.UTF_8).get(), getMaxErrorSize(), false);

        try {
            executeCommand(List.of("running"), null, outputStream);
        }
        finally {
            IOUtils.closeQuietly(outputStream);
        }

        return BooleanUtils.toBoolean(StringUtils.trim(outputWriter.toString()));
    }

    @Override
    public String getBaseCommand() {
        return command;
    }
}
