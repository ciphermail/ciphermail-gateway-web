#!/bin/bash

set -e
set -o pipefail

# Description: Script which copies the standard input to the postfix main.cf config file.
#
# Copyright 2008-2024, CipherMail B.V.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

postfix_main_config_file="/etc/postfix/main.cf"

# all postfix main config settings that can be modified by the admin
# note: because config_directory will be modified by the call to postconf
# we need to add config_directory (we should validate this setting differently)
allowed_settings=$(cat <<'EOF'
2bounce_notice_recipient
address_verify_cache_cleanup_interval
address_verify_default_transport
address_verify_negative_cache
address_verify_negative_expire_time
address_verify_negative_refresh_time
address_verify_positive_expire_time
address_verify_positive_refresh_time
address_verify_relay_transport
address_verify_relayhost
address_verify_sender
address_verify_sender_dependent_default_transport_maps
address_verify_sender_dependent_relayhost_maps
address_verify_sender_ttl
address_verify_transport_maps
address_verify_virtual_transport
always_add_missing_headers
always_bcc
append_at_myorigin
append_dot_mydomain
authorized_verp_clients
best_mx_transport
body_checks
body_checks_size_limit
bounce_notice_recipient
bounce_queue_lifetime
bounce_size_limit
broken_sasl_auth_clients
canonical_classes
canonical_maps
compatibility_level
config_directory
confirm_delay_cleared
content_filter
debug_peer_level
debug_peer_list
default_delivery_status_filter
default_destination_concurrency_failed_cohort_limit
default_destination_concurrency_limit
default_destination_concurrency_negative_feedback
default_destination_concurrency_positive_feedback
default_destination_rate_delay
default_destination_recipient_limit
default_extra_recipient_limit
default_filter_nexthop
default_rbl_reply
default_recipient_limit
default_transport
default_transport_rate_delay
defer_transports
delay_logging_resolution_limit
delay_notice_recipient
delay_warning_time
destination_concurrency_feedback_debug
detect_8bit_encoding_header
disable_dns_lookups
disable_mime_input_processing
disable_mime_output_conversion
disable_verp_bounces
disable_vrfy_command
dns_ncache_ttl_fix_enable
dont_remove
double_bounce_sender
empty_address_default_transport_maps_lookup_key
empty_address_recipient
empty_address_relayhost_maps_lookup_key
enable_idna2003_compatibility
enable_long_queue_ids
enable_original_recipient
error_notice_recipient
expand_owner_alias
fallback_relay
fallback_transport
fallback_transport_maps
header_checks
header_from_format
header_size_limit
helpful_warnings
hopcount_limit
ignore_mx_lookup_error
in_flow_delay
inet_interfaces
inet_protocols
info_log_address_format
initial_destination_concurrency
internal_mail_filter_classes
line_length_limit
mail_name
masquerade_classes
masquerade_domains
masquerade_exceptions
maximal_backoff_time
maximal_queue_lifetime
message_drop_headers
message_reject_characters
message_size_limit
message_strip_characters
milter_command_timeout
milter_connect_macros
milter_connect_timeout
milter_content_timeout
milter_data_macros
milter_default_action
milter_end_of_data_macros
milter_end_of_header_macros
milter_header_checks
milter_helo_macros
milter_macro_daemon_name
milter_macro_defaults
milter_macro_v
milter_mail_macros
milter_protocol
milter_rcpt_macros
milter_unknown_command_macros
mime_boundary_length_limit
mime_header_checks
mime_nesting_limit
minimal_backoff_time
mydestination
mydomain
myhostname
mynetworks
mynetworks_style
myorigin
nested_header_checks
non_fqdn_reject_code
non_smtpd_milters
notify_classes
nullmx_reject_code
owner_request_special
parent_domain_matches_subdomains
permit_mx_backup_networks
plaintext_reject_code
prepend_delivered_header
proxy_interfaces
queue_minfree
queue_run_delay
rbl_reply_maps
receive_override_options
recipient_bcc_maps
recipient_canonical_classes
recipient_canonical_maps
reject_tempfail_action
relay_clientcerts
relay_destination_concurrency_limit
relay_destination_recipient_limit
relay_domains
relay_recipient_maps
relay_transport
relayhost
remote_header_rewrite_domain
resolve_dequoted_address
resolve_null_domain
resolve_numeric_domain
sender_bcc_maps
sender_canonical_classes
sender_canonical_maps
sender_dependent_default_transport_maps
sender_dependent_relayhost_maps
smtp_address_preference
smtp_address_verify_target
smtp_always_send_ehlo
smtp_balance_inet_protocols
smtp_bind_address
smtp_bind_address6
smtp_body_checks
smtp_cname_overrides_servername
smtp_connect_timeout
smtp_defer_if_no_mx_address_found
smtp_delivery_status_filter
smtp_destination_concurrency_limit
smtp_destination_recipient_limit
smtp_discard_ehlo_keyword_address_maps
smtp_discard_ehlo_keywords
smtp_dns_reply_filter
smtp_dns_resolver_options
smtp_dns_support_level
smtp_enforce_tls
smtp_fallback_relay
smtp_generic_maps
smtp_header_checks
smtp_helo_name
smtp_helo_timeout
smtp_host_lookup
smtp_line_length_limit
smtp_mail_timeout
smtp_mime_header_checks
smtp_mx_address_limit
smtp_mx_session_limit
smtp_nested_header_checks
smtp_never_send_ehlo
smtp_per_record_deadline
smtp_pix_workaround_delay_time
smtp_pix_workaround_maps
smtp_pix_workaround_threshold_time
smtp_pix_workarounds
smtp_quit_timeout
smtp_quote_rfc821_envelope
smtp_randomize_addresses
smtp_rcpt_timeout
smtp_reply_filter
smtp_rset_timeout
smtp_sasl_auth_enable
smtp_sasl_auth_soft_bounce
smtp_sasl_mechanism_filter
smtp_sasl_password_maps
smtp_sasl_security_options
smtp_sasl_tls_security_options
smtp_sasl_tls_verified_security_options
smtp_sasl_type
smtp_send_dummy_mail_auth
smtp_send_xforward_command
smtp_sender_dependent_authentication
smtp_skip_5xx_greeting
smtp_skip_quit_response
smtp_starttls_timeout
smtp_tcp_port
smtp_tls_block_early_mail_reply
smtp_tls_ciphers
smtp_tls_connection_reuse
smtp_tls_dane_insecure_mx_policy
smtp_tls_enforce_peername
smtp_tls_exclude_ciphers
smtp_tls_fingerprint_cert_match
smtp_tls_fingerprint_digest
smtp_tls_force_insecure_host_tlsa_lookup
smtp_tls_loglevel
smtp_tls_mandatory_ciphers
smtp_tls_mandatory_exclude_ciphers
smtp_tls_mandatory_protocols
smtp_tls_note_starttls_offer
smtp_tls_per_site
smtp_tls_policy_maps
smtp_tls_protocols
smtp_tls_scert_verifydepth
smtp_tls_secure_cert_match
smtp_tls_security_level
smtp_tls_servername
smtp_tls_verify_cert_match
smtp_tls_wrappermode
smtp_use_tls
smtp_xforward_timeout
smtpd_authorized_verp_clients
smtpd_authorized_xclient_hosts
smtpd_authorized_xforward_hosts
smtpd_banner
smtpd_client_auth_rate_limit
smtpd_client_connection_count_limit
smtpd_client_connection_rate_limit
smtpd_client_event_limit_exceptions
smtpd_client_message_rate_limit
smtpd_client_new_tls_session_rate_limit
smtpd_client_port_logging
smtpd_client_recipient_rate_limit
smtpd_client_restrictions
smtpd_command_filter
smtpd_data_restrictions
smtpd_delay_open_until_valid_rcpt
smtpd_delay_reject
smtpd_discard_ehlo_keyword_address_maps
smtpd_discard_ehlo_keywords
smtpd_dns_reply_filter
smtpd_end_of_data_restrictions
smtpd_enforce_tls
smtpd_error_sleep_time
smtpd_etrn_restrictions
smtpd_forbidden_commands
smtpd_hard_error_limit
smtpd_helo_required
smtpd_helo_restrictions
smtpd_log_access_permit_actions
smtpd_milter_maps
smtpd_milters
smtpd_noop_commands
smtpd_null_access_lookup_key
smtpd_peername_lookup
smtpd_per_record_deadline
smtpd_recipient_limit
smtpd_recipient_overshoot_limit
smtpd_recipient_restrictions
smtpd_reject_footer
smtpd_reject_footer_maps
smtpd_reject_unlisted_recipient
smtpd_reject_unlisted_sender
smtpd_relay_restrictions
smtpd_restriction_classes
smtpd_sasl_auth_enable
smtpd_sasl_authenticated_header
smtpd_sasl_exceptions_networks
smtpd_sasl_local_domain
smtpd_sasl_response_limit
smtpd_sasl_security_options
smtpd_sasl_tls_security_options
smtpd_sender_login_maps
smtpd_sender_restrictions
smtpd_soft_error_limit
smtpd_starttls_timeout
smtpd_timeout
smtpd_tls_always_issue_session_ids
smtpd_tls_ask_ccert
smtpd_tls_auth_only
smtpd_tls_ccert_verifydepth
smtpd_tls_ciphers
smtpd_tls_eecdh_grade
smtpd_tls_exclude_ciphers
smtpd_tls_fingerprint_digest
smtpd_tls_loglevel
smtpd_tls_mandatory_ciphers
smtpd_tls_mandatory_exclude_ciphers
smtpd_tls_mandatory_protocols
smtpd_tls_protocols
smtpd_tls_received_header
smtpd_tls_req_ccert
smtpd_tls_security_level
smtpd_tls_wrappermode
smtpd_use_tls
smtputf8_autodetect_classes
smtputf8_enable
soft_bounce
strict_7bit_headers
strict_8bitmime
strict_8bitmime_body
strict_mime_encoding_domain
strict_rfc821_envelopes
strict_smtputf8
tcp_windowsize
tls_append_default_CA
tls_daemon_random_bytes
tls_dane_digest_agility
tls_dane_digests
tls_dane_trust_anchor_digest_enable
tls_disable_workarounds
tls_eecdh_auto_curves
tls_eecdh_strong_curve
tls_eecdh_ultra_curve
tls_export_cipherlist
tls_fast_shutdown_enable
tls_high_cipherlist
tls_legacy_public_key_fingerprints
tls_low_cipherlist
tls_medium_cipherlist
tls_null_cipherlist
tls_preempt_cipherlist
tls_random_bytes
tls_server_sni_maps
tls_session_ticket_cipher
tls_ssl_options
tls_wildcard_matches_multiple_labels
transport_maps
undisclosed_recipients_header
unknown_address_tempfail_action
unknown_helo_hostname_tempfail_action
unknown_local_recipient_reject_code
unknown_relay_recipient_reject_code
unknown_virtual_alias_reject_code
unknown_virtual_mailbox_reject_code
unverified_recipient_reject_code
unverified_recipient_reject_reason
unverified_sender_reject_code
unverified_sender_reject_reason
virtual_alias_domains
virtual_alias_maps
virtual_delivery_status_filter
virtual_destination_concurrency_limit
virtual_destination_recipient_limit
virtual_mailbox_domains
virtual_mailbox_limit
virtual_mailbox_maps
virtual_maps
virtual_transport
EOF
)

debug_enabled=false

# include defaults if available
if [ -f /etc/default/cm-postfix-main-config ] ; then
    . /etc/default/cm-postfix-main-config
fi

usage()
{
    echo "usage: cm-postfix-main-config -s | -g" >&2

    exit 1
}

cleanup()
{
    if [[ -n $tmp_dir ]]; then
        rm -rf $tmp_dir
    fi
}

exit_with_error()
{
    echo "$1" >&2

    local exit_code="$2"

    if [ -z "$exit_code" ]; then
        exit_code=100
    fi

    exit "$exit_code"
}

debug()
{
    if [[ $debug_enabled == true ]]; then
        echo "[DEBUG] $1"
    fi
}

contains()
{
    local list="$1"
    local input="$2"

    # input should not contain spaces
    [[ "$input" =~ [[:space:]] ]] && return 1

    [[ "$list" =~ (^|[[:space:]])"$input"($|[[:space:]]) ]] && return 0 || return 1
}

# checks if a postfix main config setting is modified
is_setting_modified()
{
    local setting="$1"

    if [[ ! -f "$tmp_dir/main.cf" ]]; then
        exit_with_error "new config file does not exist"
    fi

    local current_value
    local new_value

    current_value=$(postconf "$setting" 2> /dev/null)
    new_value=$(postconf -c $tmp_dir "$setting" 2> /dev/null)

    debug "setting: $setting, current value: $current_value, new value: $new_value"

    if [[ "$current_value" != "$new_value" ]]; then
        return 0
    fi

    return 1
}

# checks if a settings is modified and if so exits with error
validate_unauthorized_setting()
{
    local setting="$1"

    if is_setting_modified "$setting"; then
        exit_with_error "You are not authorized to change $setting"
    fi
}

set_main_config()
{
    # create a temp dir for the new config file so we can validate the new config file (check for dangerous commands)
    tmp_dir=$(mktemp -d) || exit_with_error "tmp dir could not be created"

    new_config="$tmp_dir/main.cf"

    cat > $new_config

    # read all the setting names of the new config into a var
    new_settings=$(postconf -n -H -C builtin -c $tmp_dir 2> /dev/null)

    debug "new_settings: $new_settings"

    # step through all settings and check if they are allowed and if not whether they are modified
    while IFS= read -r setting; do
        if ! contains "$allowed_settings" $setting ; then
            debug "Setting not allowed: $setting"
            # not allowed so check if the value is modified
            validate_unauthorized_setting $setting
        fi
    done <<< "$new_settings"

    # we now need to check if unauthorized settings are removed from the new config
    current_settings=$(postconf -n -C builtin 2> /dev/null | sed -r 's/(.*)\s=.*/\1/g')

    # step through all current settings and check if it contains a setting which is not in the
    # new settings and if so, check if this is a white listed setting
    while IFS= read -r setting; do
        if ! contains "$new_settings" $setting; then
            debug "Setting removed: $setting"
            # setting was removed. now check if this was a white listed setting
            if ! contains "$allowed_settings" $setting ; then
                exit_with_error "You are not authorized to remove $setting"
            fi
        fi
    done <<< "$current_settings"

    # because we had to whitelist config_directory (because of the postconf -c param) we need to
    # check whether config_directory is modified in a different way

    # get current config_directory value
    local current_config_directory
    local default_config_directory

    current_config_directory=$(postconf -n "config_directory" 2> /dev/null)
    default_config_directory=$(postconf -d "config_directory" 2> /dev/null)

    # check if config_directory was explicitly configured or is set to the default value
    # note: postconf -n will always return a value for config_directory even if it's not
    # explicitly set in main.cf
    if [[ -z $current_config_directory || $current_config_directory == "$default_config_directory" ]]; then
        postconf -c $tmp_dir -X "config_directory"
    else
        # config_directory explicitly configured. new config should keep this value
        postconf -c $tmp_dir -e "$current_config_directory"
    fi

    cp $new_config $postfix_main_config_file
}

get_main_config()
{
    cat $postfix_main_config_file
}

if [ $# -lt 1 ]; then
    usage
fi

# create a trap that will cleanup all temporary resources
trap cleanup ABRT EXIT HUP INT QUIT

while getopts 'sg' OPTION
do
    case $OPTION in
    s)  action="set"
        ;;
    g)  action="get"
        ;;
    ?)  usage
        ;;
    esac
done

shift $(($OPTIND - 1))

case $action in
    set) set_main_config
        ;;
    get) get_main_config
        ;;
    *)  usage
        ;;
esac

exit 0
