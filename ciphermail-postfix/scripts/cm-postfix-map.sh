#!/bin/bash

set -e
set -o pipefail

# Description: Script for maintaining a postfix map file
#
# Copyright CipherMail 2018

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# the error exit codes
GENERAL_EXIT_CODE=100
RUN_AS_ROOT=101

script_name=$(basename "$0")

map_extension="map"
db_map_types="btree hash"

maps_dir="/etc/postfix/maps.d"

if [[ $(id -u) -ne 0 ]] ; then echo "This command should be executed as 'sudo $script_name' or run under the root account" ; exit $RUN_AS_ROOT ; fi

usage()
{
    echo "Usage: $script_name [option...] [filename]" >&2
    echo "" >&2
    echo "    -h                    show usage" >&2
    echo "    --help                show usage" >&2
    echo "    --write-map           reads from std in and writes to the specified map file" >&2
    echo "    --read-map            reads the map file content and writes it to std out" >&2
    echo "    --list-maps           prints the filenames of the map files to std out" >&2
    echo "    --delete-map          delete the map file and associated files" >&2

    exit 1
}

exit_with_error()
{
    echo "$1" >&2

    local exit_code="$2"

    if [ -z "$exit_code" ]; then
        exit_code=$GENERAL_EXIT_CODE;
    fi

    exit "$exit_code"
}

contains()
{
    local list="$1"
    local input="$2"

    # input should not contain spaces
    [[ "$input" =~ [[:space:]] ]] && return 1

    [[ "$list" =~ (^|[[:space:]])"$input"($|[[:space:]]) ]] && return 0 || return 1
}

reload_postfix()
{
    systemctl reload postfix
}

# the input filename should be the concatenation of map type and filename separated by - (example hash-somemap.map)
parse_map_type()
{
    if [[ -z $filename ]]; then
       exit_with_error "filename is not set"
    fi

    # validate filename to check for illegal characters
    if [[ ! "$filename" =~ ^[A-Za-z_0-9.-]+$ ]]; then
        exit_with_error "filename contains illegal characters"
    fi

    # the name should be maptype-
    map_type=$(echo "$filename" | sed -r 's/(^[a-z]+)-.*$/\1/')

    # check supported map types
    if ! contains "btree cidr hash pcre regexp" "$map_type" ; then
       exit_with_error "unsupported map type"
    fi
}

write_map()
{
    parse_map_type

    cat > "$maps_dir/$filename.$map_extension"

    # only some map type need to be postmapped
    if contains "$db_map_types" "$map_type" ; then
        postmap "$map_type:/$maps_dir/$filename.$map_extension"
    fi

    reload_postfix
}

read_map()
{
    parse_map_type

    cat "$maps_dir/$filename.$map_extension"
}

list_maps()
{
    # find all .map file names with valid chars and valid names (type-name)
    find "$maps_dir" -type f -name "*.$map_extension" -exec basename -s ".$map_extension" {} \; | sed -r -n '/^[A-Za-z_0-9.-]+$/p'  | sed -n '/^.*-.*$/p'
}

delete_map()
{
    parse_map_type

    rm "$maps_dir/$filename.$map_extension" || true

    # delete .db file for some map types
    if contains "$db_map_types" "$map_type" ; then
        rm "$maps_dir/$filename.$map_extension.db" || true
    fi
}

[ $# -ne 0 ] || usage

# Note that we use `"$@"' to let each command-line parameter expand to a
# separate word. The quotes around `$@' are essential!
# We need GETOPT_TEMP as the `eval set --' would nuke the return value of getopt.
GETOPT_TEMP=$(getopt -o h --long "help, map-type:, write-map, read-map, list-maps, delete-map" \
    -n "$script_name" -- "$@")

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around $GETOPT_TEMP: they are essential!
eval set -- "$GETOPT_TEMP"

while true ; do
    case "$1" in
        -h|--help)      usage ;;
        --write-map)    action="write-map"  ; shift 1 ;;
        --read-map)     action="read-map"   ; shift 1 ;;
        --list-maps)    action="list-maps"  ; shift 1 ;;
        --delete-map)   action="delete-map" ; shift 1 ;;
        --)             shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

filename="$1"

case "$action" in
    write-map)  write_map ;;
    read-map)   read_map ;;
    list-maps)  list_maps ;;
    delete-map) delete_map ;;
esac
