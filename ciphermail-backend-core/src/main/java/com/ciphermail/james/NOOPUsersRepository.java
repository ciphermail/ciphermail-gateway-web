/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.james;

import org.apache.james.core.MailAddress;
import org.apache.james.core.Username;
import org.apache.james.user.api.UsersRepository;
import org.apache.james.user.api.model.User;
import org.reactivestreams.Publisher;

import java.util.Iterator;

/**
 * Implementation of {@link NOOPUsersRepository} which does nothing. This is needed because James requires an
 * implementation of {@link NOOPUsersRepository} even if we do not use it
 */
public class NOOPUsersRepository implements UsersRepository
{
    @Override
    public void addUser(Username username, String password) {
        // empty on purpose
    }

    @Override
    public User getUserByName(Username name) {
        return null;
    }

    @Override
    public void updateUser(User user) {
        // empty on purpose
    }

    @Override
    public void removeUser(Username name) {
        // empty on purpose
    }

    @Override
    public boolean contains(Username name) {
        return false;
    }

    @Override
    public Publisher<Boolean> containsReactive(Username name) {
        return null;
    }

    @Override
    public boolean test(Username name, String password) {
        return false;
    }

    @Override
    public int countUsers() {
        return 0;
    }

    @Override
    public Iterator<Username> list() {
        return null;
    }

    @Override
    public Publisher<Username> listReactive() {
        return null;
    }

    @Override
    public boolean supportVirtualHosting() {
        return false;
    }

    @Override
    public MailAddress getMailAddressFor(Username username) {
        return null;
    }

    @Override
    public boolean isAdministrator(Username username) {
        return false;
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }
}
