/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.james;

import com.ciphermail.core.app.admin.Admin;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.core.app.james.MailQueueFactoryProvider;
import com.ciphermail.core.app.james.MailRepositoryStoreProvider;
import com.ciphermail.core.common.util.RequiredByJames;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.util.Modules;
import org.apache.commons.configuration2.BaseHierarchicalConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.james.GuiceJamesServer;
import org.apache.james.JamesServerMain;
import org.apache.james.NaiveDelegationStoreModule;
import org.apache.james.domainlist.api.DomainList;
import org.apache.james.mailbox.Authorizator;
import org.apache.james.mailrepository.api.MailRepositoryFactory;
import org.apache.james.mailrepository.api.MailRepositoryUrlStore;
import org.apache.james.mailrepository.api.Protocol;
import org.apache.james.mailrepository.memory.MailRepositoryStoreConfiguration;
import org.apache.james.mailrepository.memory.MemoryMailRepository;
import org.apache.james.mailrepository.memory.MemoryMailRepositoryFactory;
import org.apache.james.mailrepository.memory.MemoryMailRepositoryUrlStore;
import org.apache.james.modules.MailetProcessingModule;
import org.apache.james.modules.protocols.ProtocolHandlerModule;
import org.apache.james.modules.protocols.SMTPServerModule;
import org.apache.james.modules.queue.activemq.ActiveMQQueueModule;
import org.apache.james.modules.server.DKIMMailetModule;
import org.apache.james.modules.server.MailetContainerModule;
import org.apache.james.modules.server.RawPostDequeueDecoratorModule;
import org.apache.james.rrt.api.CanSendFrom;
import org.apache.james.rrt.api.RecipientRewriteTable;
import org.apache.james.user.api.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionOperations;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;


/**
 * Class represents the main entry point of the CipherMail backend system that
 * integrates with an Apache James server.
 *
 * Main class of the CipherMailJames project integrates Apache James Server into
 * CipherMail system and is responsible for loading all necessary components
 * and ensuring their proper initialization.
 *
 * It also includes a nested module providing definitions of some services.
 * These services are required for the James server to function properly. Some of these
 * services are unused and are described as "non-functional implementations" to satisfy dependency requirements.
 *
 * During initialization, default roles and configurations are set for the administration role.
 * If the command line specifies the password of an admin, it will be encoded and set accordingly.
 *
 */
// suppress warning because some bean are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CipherMailBackendMain implements JamesServerMain
{
    /*
     * This nested class binds the necessary services to their appropriate implementations
     * in order to satisfy the dependency requirements of Apache James server.
     * Some services, referred as "Non-functional implementations" or "NOOP" are
     * provided to fulfil requirements of James server but they do not have effective functionality
     * due to unused nature of these services in the current application.
     */
    private static class LocalModule extends AbstractModule
    {
        @Override
        protected void configure()
        {
            // Some services are required by James which we do not need. For those services we will provide
            // non-functional implementations (NOOP)
            bind(UsersRepository.class).to(NOOPUsersRepository.class);
            bind(Authorizator.class).to(ForbiddenAuthorizator.class);
            bind(DomainList.class).to(NOOPDomainList.class);
            bind(MailRepositoryUrlStore.class).to(MemoryMailRepositoryUrlStore.class);
            bind(RecipientRewriteTable.class).to(NOOPRecipientRewriteTable.class);
            bind(CanSendFrom.class).to(NOOPCanSendFrom.class);
            bind(MailRepositoryStoreConfiguration.Item.class)
                    .toProvider(() -> new MailRepositoryStoreConfiguration.Item(
                            List.of(new Protocol("memory")),
                            MemoryMailRepository.class.getName(),
                            new BaseHierarchicalConfiguration()));
            Multibinder.newSetBinder(binder(), MailRepositoryFactory.class)
                    .addBinding().to(MemoryMailRepositoryFactory.class);

            // A DefaultProcessorsConfigurationSupplier instance is required. We will return null config.
            bind(MailetContainerModule.DefaultProcessorsConfigurationSupplier.class)
                    .toInstance(() -> null);
        }
    }

    @Component
    static class PostInit
    {
        @Value("${ciphermail.commandline.init-admin-cmd}")
        private String initAdminPasswordCmdOption;

        @Value("${ciphermail.rest.authentication.admin.default.name}")
        private String adminName;

        @Value("${ciphermail.rest.authentication.admin.default.role}")
        private String adminRole;

        @Autowired
        private ApplicationArguments applicationArguments;

        @Autowired
        private TransactionOperations transactionOperations;

        @Autowired
        private RoleManager roleManager;

        @Autowired
        private AdminManager adminManager;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @EventListener(ApplicationReadyEvent.class)
        // the default roles should be created prior to adding a default admin
        @Order(Ordered.LOWEST_PRECEDENCE)
        public void handleCommandLineArgs()
        {
            // check if the init admin command line option is provided
            if (applicationArguments.containsOption(initAdminPasswordCmdOption))
            {
                transactionOperations.executeWithoutResult(tx ->
                {
                    if (adminManager.getAdmin(adminName) == null)
                    {
                        LOGGER.info("Add default admin");

                        Admin admin = adminManager.persistAdmin(adminManager.createAdmin(adminName));

                        admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);

                        // check if the command line specifies a password
                        String password = StringUtils.trimToNull(applicationArguments.getOptionValues(
                                initAdminPasswordCmdOption).stream().findFirst().orElse(null));

                        if (password != null)
                        {
                            LOGGER.info("Set default admin password");

                            admin.setPassword(passwordEncoder.encode(password));
                        }

                        admin.getRoles().clear();
                        admin.getRoles().add(Optional.ofNullable(roleManager.getRole(adminRole)).orElseThrow(
                                () -> new IllegalArgumentException(String.format("Role %s does not exist", adminRole))));
                    }
                });
            }
        }
    }

    /**
     * The main method for the CipherMailBackendMain class.
     * Initiates the Spring boot application before starting the Apache James server,
     * and builds the JamesConfiguration.
     * Logs of the loading configuration are generated for tracking.
     * <p>
     * The SpringApplication has to be started before JamesServer for
     * proper functionality of application.
     */
    public static void main(String[] args)
    throws Exception
    {
        // we must start spring before james
        SpringApplication springApplication = new SpringApplication(CipherMailSpringBootApplication.class);

        ConfigurableApplicationContext applicationContext = springApplication.run(args);

        JamesConfiguration configuration = JamesConfiguration.builder()
            .build();

        LOGGER.info("Loading configuration {}", configuration);

        // James by default uses Guice whereas CipherMail uses Spring.
        //
        // Even though it possible to configure James with Spring (or CipherMail with Guice), we figured it is better
        // not to do so. The main reason is that Guice developers seem to prefer Guice and CipherMail seems to prefer
        // Spring. One reason we prefer Spring is that spring supports configuring the system with XML. This allows
        // the system to be configured at runtime and therefore supporting customer specific setups. Another benefit
        // of not using the same IOC container is that it makes it less likely that we run into upgrade issues where
        // James requires a different Spring version than CipherMail.
        // The downside of using Guice and Spring is that it can be somewhat problematic if we need to use beans
        // created by Guice from Spring and vice versa. To make Guice beans available in Spring and vice versa, we
        // will use a Guice module which will inject some spring beans into Guice and vice versa.
        Module springToGuiceModule = new AbstractModule()
        {
            @Override
            public void configure()
            {
                // Spring beans which are used by mailets/matchers should be available from Guice
                // find all Spring beans which we need to "inject" into Guice

                // inject the spring application context because some mailets/matchers need it
                createBeanInstance(ApplicationContext.class, applicationContext);

                Map<String, Object> bridgeableBeans = applicationContext.getBeansWithAnnotation(RequiredByJames.class);

                for (Map.Entry<String, Object> beanEntry : bridgeableBeans.entrySet())
                {
                    Class<?> baseType = (Class<?>) MergedAnnotations.from(beanEntry.getValue().getClass(),
                            MergedAnnotations.SearchStrategy.TYPE_HIERARCHY).get(
                                    RequiredByJames.class, MergedAnnotation::isDirectlyPresent).getSource();

                    Object instance = applicationContext.getBean(Objects.requireNonNull(baseType));

                    RequiredByJames annotation = baseType.getAnnotation(RequiredByJames.class);

                    Class<?> baseClass = annotation.baseClass();

                    if (!Void.class.equals(baseClass)) {
                        baseType = baseClass;
                    }

                    // make the Spring bean instance available in Guice
                    createBeanInstance(baseType, instance);
                }

                // We need some James instances which are created by Guice to be available in classes instantiated by
                // spring. We therefore need to manually inject Guice created instanced into spring instances.
                // For now, we only need to support a few classes. If we need to support more, we might add a special
                // annotation and auto scan all available spring beans whether further injection is required.
                requestInjection(applicationContext.getBean(MailQueueFactoryProvider.class));
                requestInjection(applicationContext.getBean(MailRepositoryStoreProvider.class));
            }

            @SuppressWarnings({"rawtypes", "unchecked"})
            void createBeanInstance(Class<?> baseType, Object instance)
            {
                bind((Class) baseType).toInstance(instance);
            }
        };

        Module jamesModules = Modules.combine(
                new NaiveDelegationStoreModule(),
                new MailetProcessingModule(),
                new ActiveMQQueueModule(),
                new RawPostDequeueDecoratorModule(),
                new ProtocolHandlerModule(),
                new SMTPServerModule(),
                new DKIMMailetModule(),
                new LocalModule()
        );

        JamesServerMain.main(GuiceJamesServer.forConfiguration(configuration).combineWith(jamesModules,
                springToGuiceModule));
    }
}
