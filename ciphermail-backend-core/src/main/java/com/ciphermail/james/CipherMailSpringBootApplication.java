/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.james;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.DefaultFileComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jms.JmsAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication(
    exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        JmsAutoConfiguration.class,
    },
    scanBasePackages = {"com.ciphermail"}
)
@EnableScheduling
@ConfigurationPropertiesScan(basePackages = "com.ciphermail")
@SuppressWarnings("java:S1118") // spring boot application must have a non-private default constructor
public class CipherMailSpringBootApplication
{
    private static final Logger logger = LoggerFactory.getLogger(CipherMailSpringBootApplication.class);

    private static final String SPRING_CONFIG_PATH_SYSTEM_PROPERTY = "com.ciphermail.spring.config.path";

    @Component
    static class SpringModule implements BeanDefinitionRegistryPostProcessor
    {
        @Override
        public void postProcessBeanDefinitionRegistry(@Nonnull BeanDefinitionRegistry beanDefinitionRegistry)
        throws BeansException
        {
            XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanDefinitionRegistry);

            // if the spring config path is explicitly set, use it, if not, make it a sub dir of working dir
            String springConfigProperty = System.getProperty(SPRING_CONFIG_PATH_SYSTEM_PROPERTY);

            File springConfigPath = springConfigProperty != null ? new File(springConfigProperty) :
                    new File(CipherMailBackendConfiguration.getBackendConfigHome(), "spring.d");

            List<File> configFiles = new ArrayList<>(FileUtils.listFiles(springConfigPath,
                    new String[]{"xml"}, true));

            configFiles.sort(DefaultFileComparator.DEFAULT_COMPARATOR);

            // load additional spring configuration files
            for (File configFile : configFiles)
            {
                logger.info("Loading spring configuration file: {}", configFile.getAbsolutePath());

                Resource beanFile = new FileSystemResource(configFile);
                xmlBeanDefinitionReader.loadBeanDefinitions(beanFile);
            }
        }

        @Override
        public void postProcessBeanFactory(@Nonnull ConfigurableListableBeanFactory configurableListableBeanFactory)
        throws BeansException
        {
            // empty on purpose
        }

        /*
         * This is used by actuator/httpexchanges to log the last 100 requests
         */
        @Bean
        public InMemoryHttpExchangeRepository createTraceRepository() {
            return new InMemoryHttpExchangeRepository();
        }
    }
}
