/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */

package com.ciphermail.james;

import com.google.common.base.MoreObjects;
import org.apache.james.filesystem.api.FileSystem;
import org.apache.james.filesystem.api.JamesDirectoriesProvider;
import org.apache.james.server.core.JamesServerResourceLoader;
import org.apache.james.server.core.configuration.Configuration;

import java.io.File;

public class JamesConfiguration implements Configuration
{
    public static class Builder
    {
        private Builder() {
            // empty on purpose
        }

        public JamesConfiguration build()
        {
            ConfigurationPath configurationPath = new ConfigurationPath(FileSystem.FILE_PROTOCOL_AND_CONF);

            File rootDirectory = new File(CipherMailBackendConfiguration.getBackendConfigHome(), "james");

            if (!rootDirectory.exists()) {
                throw new IllegalArgumentException(rootDirectory + " does not exist");
            }
            if (!rootDirectory.isDirectory()) {
                throw new IllegalArgumentException(rootDirectory + " is not a directory");
            }

            JamesServerResourceLoader directoriesProvider = new JamesServerResourceLoader(rootDirectory.getAbsolutePath())
            {
                @Override
                public String getVarDirectory() {
                    return new File(CipherMailBackendConfiguration.getVarHome(), "james").getAbsolutePath();
                }
            };

            return new JamesConfiguration(
                    configurationPath,
                    directoriesProvider);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private final ConfigurationPath configurationPath;
    private final JamesDirectoriesProvider directoriesProvider;

    public JamesConfiguration(ConfigurationPath configurationPath, JamesDirectoriesProvider directoriesProvider)
    {
        this.configurationPath = configurationPath;
        this.directoriesProvider = directoriesProvider;
    }

    @Override
    public ConfigurationPath configurationPath() {
        return configurationPath;
    }

    @Override
    public JamesDirectoriesProvider directories() {
        return directoriesProvider;
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("configurationPath", configurationPath)
                .add("directoriesProvider", directoriesProvider)
                .toString();
    }
}
