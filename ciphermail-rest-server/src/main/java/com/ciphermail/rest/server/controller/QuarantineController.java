/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.mail.MailSerializer;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryEventListener;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.mail.repository.MailRepositorySearchField;
import com.ciphermail.core.common.util.Functional;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.rest.core.DLPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.QuarantineDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "Quarantine")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class QuarantineController
{
    private static final Logger logger = LoggerFactory.getLogger(QuarantineController.class);

    /*
     * String return result when a parameter value results in an error
     */
    static final String ERROR = "<error>";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private MailRepository quarantineMailRepository;

    @Autowired
    private MailEnqueuer mailEnqueuer;

    /*
     * Notifies listeners that something has happened for on the mailRepository.
     */
    @Autowired
    private MailRepositoryEventListener mailRepositoryEventListener;

    @Autowired
    private RestValidator restValidator;

    /*
     * The next processor for normal released message
     */
    @Value("${ciphermail.quarantine.release.default-processor}")
    private String defaultProcessor;

    /*
     * The next processor for released message that must be encrypted
     */
    @Value("${ciphermail.quarantine.release.force-encrypt-processor}")
    private String forceEncryptProcessor;

    /*
     * The next processor for released message that should be sent without further processing
     */
    @Value("${ciphermail.quarantine.release.send-as-is-processor}")
    private String sendAsIsProcessor;

    private enum Action
    {
        GET_QUARANTINED_MAIL,
        GET_QUARANTINED_MAIL_COUNT,
        GET_QUARANTINED_MAIL_BY_ID,
        DELETE_QUARANTINED_MAIL,
        DELETE_QUARANTINED_MAILS,
        GET_QUARANTINED_MAIL_CONTENT,
        SEARCH_QUARANTINED_MAIL,
        SEARCH_QUARANTINED_MAIL_COUNT,
        RELEASE_QUARANTINED_MAIL,
        RELEASE_QUARANTINED_MAILS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.QUARANTINE,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.QUARANTINE, action.toPermissionName());
    }

    public QuarantineController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(path= RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_PATH)
    public List<QuarantineDTO.QuarantinedMail> getQuarantinedMail(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_QUARANTINED_MAIL);

            return quarantineMailRepository.getItems(firstResult, maxResults).stream()
                    .map(QuarantineController::toQuarantinedMail).toList();
        });
    }

    @GetMapping(path= RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_COUNT_PATH)
    public long getQuarantinedMailCount()
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_QUARANTINED_MAIL_COUNT);

            return quarantineMailRepository.getItemCount();
        })).orElse(-1L);
    }

    @GetMapping(path= RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_BY_ID_PATH)
    public QuarantineDTO.QuarantinedMail getQuarantinedMailById(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_QUARANTINED_MAIL_BY_ID);

            MailRepositoryItem mailRepositoryItem = getMailRepositoryItem(id);

            return toQuarantinedMail(mailRepositoryItem);
        });
    }

    @PostMapping(path= RestPaths.QUARANTINE_DELETE_QUARANTINED_MAIL_PATH)
    public QuarantineDTO.QuarantinedMail deleteQuarantinedMail(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.DELETE_QUARANTINED_MAIL);

            MailRepositoryItem mailRepositoryItem = getMailRepositoryItem(id);

            quarantineMailRepository.deleteItem(id);

            return toQuarantinedMail(mailRepositoryItem);
        });
    }

    @PostMapping(path= RestPaths.QUARANTINE_DELETE_QUARANTINED_MAILS_PATH)
    public void deleteQuarantinedMails(@RequestBody List<UUID> ids)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_QUARANTINED_MAILS);

            restValidator.limitMaxResults(ids.size(), "number of ids exceed the upper limit %s",
                    logger);

            for (UUID id : ids)
            {
                MailRepositoryItem mailRepositoryItem = quarantineMailRepository.getItem(id);

                if (mailRepositoryItem != null) {
                    quarantineMailRepository.deleteItem(id);
                }
            }
        });
    }

    @GetMapping(path= RestPaths.QUARANTINE_GET_QUARANTINED_MAIL_CONTENT_PATH)
    public ResponseEntity<StreamingResponseBody> getQuarantinedMailContent(@RequestParam(required = false) UUID id,
            @RequestParam(required = false) Long maxSize)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_QUARANTINED_MAIL_CONTENT));

        StreamingResponseBody responseBody = outputStream ->
                transactionOperations.executeWithoutResult(tx ->
                {
                    try {
                        getMailRepositoryItem(id).writeMessage(maxSize != null ?
                                new SizeLimitedOutputStream(outputStream, maxSize, false) : outputStream);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Error getting mail with id %s. Message: %s", id, e.getMessage()), logger);
                    }
                });

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + id + ".eml")
                .header(HttpHeaders.CONTENT_TYPE, "plain/txt")
                .body(responseBody);
    }

    @GetMapping(path= RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_PATH)
    public List<QuarantineDTO.QuarantinedMail> searchQuarantinedMail(
            @RequestParam QuarantineDTO.SearchField searchField,
            @RequestParam String searchKey,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SEARCH_QUARANTINED_MAIL);

            return quarantineMailRepository.searchItems(
                    MailRepositorySearchField.valueOf(searchField.name()),
                            searchKey,
                            firstResult,
                            maxResults).stream()
                    .map(QuarantineController::toQuarantinedMail).toList();
        });
    }

    @GetMapping(path= RestPaths.QUARANTINE_SEARCH_QUARANTINED_MAIL_COUNT_PATH)
    public long searchQuarantinedMailCount(
            @RequestParam QuarantineDTO.SearchField searchField,
            @RequestParam String searchKey)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.SEARCH_QUARANTINED_MAIL_COUNT);

            return quarantineMailRepository.getSearchCount(
                            MailRepositorySearchField.valueOf(searchField.name()),
                            searchKey);
        })).orElse(-1L);
    }

    @PostMapping(path= RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAIL_PATH)
    public QuarantineDTO.QuarantinedMail releaseQuarantinedMail(
            @RequestParam UUID id,
            @RequestParam QuarantineDTO.ReleaseProcessor releaseProcessor)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.RELEASE_QUARANTINED_MAIL);

            try {
                return toQuarantinedMail(release(getMailRepositoryItem(id), releaseProcessor));
            }
            catch (MessagingException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Error releasing mail with id %s. Message: %s", id, e.getMessage()), logger);
            }
        });
    }

    @PostMapping(path= RestPaths.QUARANTINE_RELEASE_QUARANTINED_MAILS_PATH)
    public void releaseQuarantinedMails(
            @RequestBody List<UUID> ids,
            @RequestParam(required = false) QuarantineDTO.ReleaseProcessor releaseProcessor)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.RELEASE_QUARANTINED_MAILS);

            try {
                for (UUID id : ids)
                {
                    MailRepositoryItem mailRepositoryItem = quarantineMailRepository.getItem(id);

                    if (mailRepositoryItem != null) {
                        release(mailRepositoryItem, releaseProcessor);
                    }
                }
            }
            catch (MessagingException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Error releasing mail. Message: %s", e.getMessage()), logger);
            }
        });
    }

    private MailRepositoryItem release(@Nonnull MailRepositoryItem mailRepositoryItem, QuarantineDTO.ReleaseProcessor releaseProcessor)
    throws MessagingException
    {
        Mail mail = new MailSerializer().deserialize(mailRepositoryItem.getAdditionalData());

        mail.setMessage(mailRepositoryItem.getMimeMessage());

        if (releaseProcessor == null) {
            releaseProcessor = QuarantineDTO.ReleaseProcessor.DEFAULT;
        }

        switch(releaseProcessor)
        {
            case DEFAULT -> mail.setState(defaultProcessor);
            case ENCRYPT -> mail.setState(forceEncryptProcessor);
            case AS_IS   -> mail.setState(sendAsIsProcessor);
        }

        try {
            mailEnqueuer.enqueue(mail);
        }
        finally {
            JamestUtils.dispose(mail);
        }

        mailRepositoryEventListener.onReleased(quarantineMailRepository.getName(), mailRepositoryItem);

        // Remove the message from repository because it has just been re-spooled
        quarantineMailRepository.deleteItem(mailRepositoryItem.getID());

        return mailRepositoryItem;
    }

    private MailRepositoryItem getMailRepositoryItem(@Nonnull UUID id)
    {
        MailRepositoryItem mailRepositoryItem = quarantineMailRepository.getItem(id);

        if (mailRepositoryItem == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Quarantined mail with id %s not found", id), logger);
        }

        return mailRepositoryItem;
    }

    private static QuarantineDTO.QuarantinedMail toQuarantinedMail(@Nonnull MailRepositoryItem mailRepositoryItem)
    {
        List<QuarantineDTO.PolicyViolation> policyViolations = new LinkedList<>();
        String policyViolationErrorMessage = null;

        // MailRepositoryMailStorer stores the Mail object in serialized form in the additional data
        Mail mail = new MailSerializer().deserialize(mailRepositoryItem.getAdditionalData());

        if (mail != null)
        {
            for (PolicyViolation violation : Optional.ofNullable(CoreApplicationMailAttributes
                    .getPolicyViolations(mail)).orElse(Collections.emptyList()))
            {
                policyViolations.add(new QuarantineDTO.PolicyViolation(
                        violation.getPolicy(),
                        violation.getRule(),
                        violation.getMatch(),
                        Optional.ofNullable(violation.getPriority()).map(p ->
                                DLPDTO.PolicyViolationAction.valueOf(p.name())).orElse(null)));
            }

            policyViolationErrorMessage = CoreApplicationMailAttributes.getPolicyViolationErrorMessage(mail);
        }

        return new QuarantineDTO.QuarantinedMail(
                mailRepositoryItem.getID(),
                mailRepositoryItem.getMessageID(),
                mailRepositoryItem.getSubject(),
                Functional.catchAllGet(() ->  Optional.ofNullable(mailRepositoryItem.getOriginator())
                        .map(InternetAddress::getAddress).orElse(null), ERROR, logger),
                Functional.catchAllGet(() ->  Optional.ofNullable(mailRepositoryItem.getSender())
                        .map(InternetAddress::getAddress).orElse(null), ERROR, logger),
                Functional.catchAllGet(() ->  mailRepositoryItem.getRecipients().stream()
                        .map(InternetAddress::getAddress).toList(), Collections.emptyList(), logger),
                mailRepositoryItem.getRemoteAddress(),
                mailRepositoryItem.getCreated().getTime(),
                policyViolations,
                policyViolationErrorMessage);
    }
}
