/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.app.admin.Admin;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AuthenticationType;
import inet.ipaddr.IPAddressString;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.util.List;

// suppress warning because some bean are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
@Component
public class AuthenticationEventHandler
{
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationEventHandler.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private AuthenticationFailureCache authenticationFailureCache;

    @Autowired
    private ClientAddressProvider clientAddressProvider;

    @Autowired
    private AdminManager adminManager;

    private boolean isConnectedIpAuthorized(@Nonnull List<String> ipAddresses)
    {
        // if no ip addresses are specified, accept all
        if (ipAddresses.isEmpty()) {
            return true;
        }

        for (String ipAddress : ipAddresses)
        {
            if (new IPAddressString(ipAddress).contains(new IPAddressString(clientAddressProvider.getRemoteAddress()))) {
                return true;
            }
        }

        return false;
    }

    @EventListener
    public void authenticationEvent(AbstractAuthenticationEvent authenticationEvent)
    {
        logger.debug("AuthenticationEvent: {}", authenticationEvent);

        if (authenticationEvent instanceof AuthenticationSuccessEvent)
        {
            // Note: we cannot use the injected AuthenticatedUserProvider because at this stage, the authenticated
            // user is not yet stored in the SecurityContextHolder
            String username = AuthenticatedUserProvider.getPrincipalName(authenticationEvent.getAuthentication());

            // check if the remote ip address is allowed for the admin
            // ip addresses should be returned because they can only be accesses within a transaction
            ImmutablePair<Admin, List<String>> adminDetails = transactionOperations.execute(tx ->
            {
                Admin admin = adminManager.getAdmin(username);

                // admin can be null if the admin was not added (can happen with OIDC)
                return admin != null ? new ImmutablePair<>(admin, admin.getIpAddresses()) : null;
            });

            if (adminDetails == null || adminDetails.left == null)
            {
                // can happen with OIDC login when the admin was not added
                logger.warn("There is no admin account for {}", username);

                throw new InsufficientAuthenticationException(String.format(
                        "There is no admin account for %s", username));
            }

            Admin admin = adminDetails.left;
            List<String> ipAddresses = adminDetails.right;

            if (!isConnectedIpAuthorized(ipAddresses))
            {
                logger.warn("IP address {} is not authorized for admin account {}",
                        clientAddressProvider.getRemoteAddress(), username);

                throw new InsufficientAuthenticationException(String.format(
                        "IP address %s is not authorized for admin account %s",
                        clientAddressProvider.getRemoteAddress(), username));
            }

            // if the user was authenticated with OIDC, we need to check whether the logged-in user is an admin
            // and is an OIDC admin
            if (authenticationEvent.getAuthentication().getPrincipal() instanceof DefaultOidcUser &&
                (!AuthenticationType.OIDC.equals(admin.getAuthenticationType())))
            {
                logger.warn("The admin account {} is not an OIDC account", username);

                throw new InsufficientAuthenticationException(String.format(
                        "The admin account %s is not an OIDC account", username));
            }

            logger.info("Authentication success. User: {}, Remote IP: {}, Details: {}", username,
                    clientAddressProvider.getRemoteAddress(), authenticationEvent.getAuthentication());
        }
        else if (authenticationEvent instanceof AuthenticationFailureBadCredentialsEvent)
        {
            // Note: we cannot use the injected AuthenticatedUserProvider because at this stage, the authenticated
            // user is not yet stored in the SecurityContextHolder
            String username = AuthenticatedUserProvider.getPrincipalName(authenticationEvent.getAuthentication());

            logger.warn("Authentication failure. User: {}, Remote IP: {}, Details: {}", username,
                    clientAddressProvider.getRemoteAddress(), authenticationEvent.getAuthentication());

            authenticationFailureCache.registerFailedLogin(username);
        }
    }
}
