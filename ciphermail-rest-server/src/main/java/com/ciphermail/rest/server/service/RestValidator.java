/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.util.AddressUtils;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.postfix.PostfixUtils;
import inet.ipaddr.IPAddressString;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

@SuppressWarnings({"java:S6813"})
@Component
public class RestValidator
{
    /*
     * Regex for a SHA512 hash
     */
    private static final Pattern sha512RegEx = Pattern.compile("(?i)[a-f0-9]{128}");

    /*
     * Regex for a SHA256 hash
     */
    private static final Pattern sha256RegEx = Pattern.compile("(?i)[a-f0-9]{64}");

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    /**
     * Default value for maxResultsUpperLimit. This is used by REST controllers for the default maxResults param
     */
    public static final String DEFAULT_MAX_RESULTS_STRING = "250";

    @Value("${ciphermail.rest.max-results:" + DEFAULT_MAX_RESULTS_STRING + "}")
    private int maxResultsUpperLimit;

    /**
     * Checks if value is not null
     */
    public <T> T validateNotNull(T value, String name, Logger logger)
    {
        if (value == null) {
            throw RestException.createInstance(String.format("%s cannot be null", name))
                    .setKey("validation.value-cannot-be-null").log(logger);
        }

        return value;
    }

    /**
     * Checks if value is not empty
     */
    public String validateNotEmpty(String value, String name, Logger logger)
    {
        if (StringUtils.isEmpty(value)) {
            throw RestException.createInstance(String.format("%s cannot be empty", name))
                    .setKey("validation.value-cannot-be-empty").log(logger);
        }

        return value;
    }

    /**
     * The upper limit of the maximum number of results returned with a REST call.
     */
    public int getMaxResultsUpperLimit() {
        return maxResultsUpperLimit;
    }

    /**
     * Checks if value is within the lower and upper limit
     */
    public int validateMinMax(int value, Integer min, Integer max, Logger logger)
    {
        return (int) validateMinMax(
                value,
                min != null ? min.longValue() : null,
                max != null ? max.longValue() : null, logger);
    }

    /**
     * Checks if value is within the lower and upper limit
     */
    public long validateMinMax(long value, Long min, Long max, Logger logger)
    {
        if (min != null && value < min) {
            throw RestException.createInstance(String.format("Value %d is less than %d", value, min))
                    .setKey("validation.value-too-low").log(logger);
        }

        if (max != null && value > max) {
            throw RestException.createInstance(String.format("Value %d is greater than %d", value, max))
                    .setKey("validation.value-too-high").log(logger);
        }

        return value;
    }

    /**
     * Checks if email is a valid email address
     */
    public String validateEmail(@Nonnull String email, Logger logger)
    {
        String validated = EmailAddressUtils.canonicalizeAndValidate(email, true);

        if (validated == null)
        {
            throw RestException.createInstance(String.format("%s is not a valid email address", email))
                    .setKey("validation.invalid-email-address").log(logger);
        }

        return validated;
    }

    /**
     * Checks if domain is a domain
     */
    public String validateDomain(@Nonnull String domain, @Nonnull DomainUtils.DomainType domainType, Logger logger)
    {
        String validated = DomainUtils.canonicalizeAndValidate(domain, domainType);

        if (validated == null)
        {
            throw RestException.createInstance(String.format("%s is not a valid domain", domain))
                    .setKey("validation.invalid-domain").log(logger);
        }

        return validated;
    }

    /**
     * Checks if hostname is valid
     */
    public String validateHostname(@Nonnull String hostname, @Nonnull DomainUtils.DomainType domainType, Logger logger)
    {
        String validated = DomainUtils.canonicalizeAndValidate(hostname, domainType);

        if (validated == null)
        {
            throw RestException.createInstance(String.format("%s is not a valid hostname", hostname))
                    .setKey("validation.invalid-hostname").log(logger);
        }

        return validated;
    }

    /**
     * Checks if thumbprint is a valid SHA512 thumbprint
     */
    public String validateThumbprint(@Nonnull String thumbprint, Logger logger)
    {
        if (!sha512RegEx.matcher(thumbprint).matches()) {
            throw RestException.createInstance(String.format("%s is not a valid postfix thumbprint", thumbprint))
                    .setKey("validation.invalid-thumbprint").log(logger);
        }

        return thumbprint;
    }

    /**
     * Checks if thumbprint is a valid SHA256 thumbprint
     */
    public String validatePGPSha256Fingerprint(@Nonnull String sha256Fingerprint, Logger logger)
    {
        if (!sha256RegEx.matcher(sha256Fingerprint).matches()) {
            throw RestException.createInstance(String.format("%s is not a valid sha256 fingerprint", sha256Fingerprint))
                    .setKey("validation.invalid-sha256-fingerprint").log(logger);
        }

        return sha256Fingerprint;
    }

    /**
     * Checks if values exceeds maxResultsUpperlimit. Use this to maximizing the number of results returned from a REST
     * call.
     */
    public int limitMaxResults(int value, String message, Logger logger)
    {
        final int finalMaxResults;

        if (value > getMaxResultsUpperLimit()) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format(message, getMaxResultsUpperLimit()), logger);
        }
        else {
            finalMaxResults = value;
        }

        return finalMaxResults;
    }

    /**
     * Checks if values exceeds maxResultsUpperlimit. Use this to maximizing the number of results returned from a REST
     * call.
     */
    public int limitMaxResults(int value, Logger logger) {
        return limitMaxResults(value, "maxResults exceed the upper limit %s", logger);
    }

    /**
     * Checks if keyEntry is a PGP master key
     */
    public PGPKeyRingEntry validateIsMasterKey(PGPKeyRingEntry keyEntry, Logger logger)
    {
        // email addresses or domains can only be set on a master key
        if (!keyEntry.isMasterKey()) {
            throw RestException.createInstance(String.format("Key with id %s is not a master key", keyEntry.getID()))
                    .setKey("validation.key-is-not-a-masterkey").log(logger);
        }

        return keyEntry;
    }

    /**
     * Checks if queueID is a valid Postfix queue ID
     */
    public String validatePostfixQueueID(String queueID, Logger logger)
    {
        if (queueID == null || !PostfixUtils.isValidQueueID(queueID)) {
            throw RestException.createInstance(String.format("Queue id %s is invalid", queueID))
                    .setKey("validation.invalid-queue-id").log(logger);
        }

        return queueID;
    }

    /**
     * Checks if hostname is valid for Postfix
     * <p>
     * Note: Postfix has strict requirements for a valid hostname
     */
    public String validatePostfixHostname(String hostname, Logger logger)
    {
        if (!PostfixUtils.isValidHostname(hostname)) {
            throw RestException.createInstance(String.format("%s is not a valid postfix hostname", hostname))
                    .setKey("validation.invalid-postfix-hostname").log(logger);
        }

        return hostname;
    }

    /**
     * Checks if hostname is valid for Postfix
     */
    public String validatePostfixNetwork(String network, Logger logger)
    {
        if (!AddressUtils.isValidNetwork(network, true)) {
            throw RestException.createInstance(String.format("Network %s is invalid", network))
                    .setKey("validation.invalid-postfix-network").log(logger);
        }

        return network;
    }

    /**
     * Checks if ip address is a valid ip address range (see {@link IPAddressString} for supported formats)
     */
    public String validateIpAddressRange(String ipAddressRange, Logger logger)
    {
        if (ipAddressRange == null) {
            return null;
        }

        if (!new IPAddressString(ipAddressRange).isValid()) {
            throw RestException.createInstance(String.format("IP address range %s is invalid", ipAddressRange))
                    .setKey("validation.invalid-ip-address-range").log(logger);
        }

        return ipAddressRange;
    }
}
