/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.authorization;

import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
// suppress warning because some bean are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PermissionCheckerImpl implements PermissionChecker
{
    private static final Logger logger = LoggerFactory.getLogger(PermissionCheckerImpl.class);

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    @Autowired
    private AdminManager adminManager;

    @Autowired
    private Permissions permissions;

    private String getAuthenticatedUser()
    {
        String authenticatedUser = authenticatedUserProvider.getAuthenticatedUser();

        if (authenticatedUser == null) {
            throw new UsernameNotFoundException("User is not authenticated");
        }

        return authenticatedUser;
    }

    @Override
    public void checkPermission(
            @Nonnull PermissionCategory category,
            @Nonnull String action,
            String... additionalParameters)
    throws PermissionDeniedException
    {
        String permission = PermissionUtils.toPermission(category, action, additionalParameters);

        if (!hasPermission(permission))
        {
            throw new PermissionDeniedException(String.format("Authenticated user does not have the required " +
                                                              "permission: %s", permission));
        }
    }

    @Override
    public boolean hasPermission(
            @Nonnull PermissionCategory category,
            @Nonnull String action,
            String... additionalParameters)
    {
        return hasPermission(PermissionUtils.toPermission(category, action, additionalParameters));
    }

    private boolean hasPermission(@Nonnull String permission) {

        if (!permissions.isPermission(permission)) {
            logger.warn("{} is an unknown permission", permission);
        }

        return adminManager.getEffectivePermissions(getAuthenticatedUser()).contains(permission);
    }
}
