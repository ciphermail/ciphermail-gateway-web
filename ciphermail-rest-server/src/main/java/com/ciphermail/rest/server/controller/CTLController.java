/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.rest.core.CTLDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "CTL")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CTLController
{
    private static final Logger logger = LoggerFactory.getLogger(CTLController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private CTLManager ctlManager;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_CTLS,
        GET_CTLS_COUNT,
        GET_CTL,
        SET_CTL,
        SET_CTLS,
        DELETE_CTL,
        DELETE_CTLS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CTL,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.CTL, action.toPermissionName());
    }

    public CTLController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @GetMapping(RestPaths.CTL_GET_CTLS_PATH)
    public List<CTLDTO.CTLResult> getCTLs(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CTLS);

            try {
                return getCTL().getEntries(firstResult,restValidator.limitMaxResults(
                        maxResults, logger)).stream().map(CTLController::toCTLResult).toList();
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CTL_GET_CTLS_COUNT_PATH)
    public long getCTLsCount()
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CTLS_COUNT);

            try {
                return getCTL().size();
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @GetMapping(RestPaths.CTL_GET_CTL_PATH)
    public CTLDTO.CTLResult getCTL(@RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CTL);

            try {
                return toCTLResult(getCTL().getEntry(restValidator.validateThumbprint(thumbprint, logger)));
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "CTL not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CTL_SET_CTL_PATH)
    public CTLDTO.CTLResult setCTL(@RequestParam String thumbprint, @RequestParam CTLEntryStatus status,
            @RequestParam boolean allowExpired)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SET_CTL);

            try {
                CTL ctl = getCTL();

                CTLEntry entry = ctl.getEntry(restValidator.validateThumbprint(thumbprint, logger));

                if (entry == null)
                {
                    entry = toCTLEntry(ctl, thumbprint, status, allowExpired);
                    ctl.addEntry(entry);
                }
                else {
                    entry.setStatus(status);
                    entry.setAllowExpired(allowExpired);
                }

                return toCTLResult(entry);
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE)
    })
    @PostMapping(RestPaths.CTL_SET_CTLS_PATH)
    public void setCTLs(@RequestBody List<String> thumbprints, @RequestParam CTLEntryStatus status,
            @RequestParam boolean allowExpired)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.SET_CTLS);

            CTL ctl = getCTL();

            try {
                for (String thumbprint : thumbprints)
                {
                    CTLEntry entry = ctl.getEntry(restValidator.validateThumbprint(thumbprint, logger));

                    if (entry == null) {
                        entry = toCTLEntry(ctl, thumbprint, status, allowExpired);
                        ctl.addEntry(entry);
                    }
                    else {
                        entry.setStatus(status);
                        entry.setAllowExpired(allowExpired);
                    }
                }
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "CTL not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CTL_DELETE_CTL_PATH)
    public CTLDTO.CTLResult deleteCTL(@RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_CTL);

            try {
                CTL ctl = getCTL();

                CTLEntry entry = ctl.getEntry(restValidator.validateThumbprint(thumbprint, logger));

                if (entry == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("CTL with thumbprint %s not found", thumbprint), logger);
                }

                ctl.deleteEntry(entry);

                return toCTLResult(entry);
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE)
    })
    @PostMapping(RestPaths.CTL_DELETE_CTLS_PATH)
    public void deleteCTLs(@RequestBody List<String> thumbprints)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.DELETE_CTLS);

            try {
                CTL ctl = getCTL();

                for (String thumbprint : thumbprints)
                {
                    CTLEntry entry = ctl.getEntry(restValidator.validateThumbprint(thumbprint, logger));

                    if (entry != null) {
                        ctl.deleteEntry(entry);
                    }
                }
            }
            catch (CTLException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private CTL getCTL() {
        // for now, only the default CTL is used
        return ctlManager.getCTL(CTLManager.DEFAULT_CTL);
    }

    private static CTLDTO.CTLResult toCTLResult(CTLEntry entry) {
        return entry != null ? new CTLDTO.CTLResult(entry.getThumbprint(), entry.getStatus(), entry.isAllowExpired())
                : null;
    }

    private static CTLEntry toCTLEntry(CTL ctl, String thumbprint, CTLEntryStatus status,
            boolean allowExpired)
    throws CTLException
    {
        CTLEntry entity = ctl.createEntry(thumbprint);

        entity.setStatus(status);
        entity.setAllowExpired(allowExpired);

        return entity;
    }
}
