/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.security.crl.CRLPathBuilder;
import com.ciphermail.core.common.security.crl.CRLUtils;
import com.ciphermail.core.common.security.crl.ThreadedCRLStoreUpdater;
import com.ciphermail.core.common.security.crl.X509CRLInspector;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreEntry;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CRLDetailsFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "CRL")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CRLController
{
    private static final Logger logger = LoggerFactory.getLogger(CRLController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private ThreadedCRLStoreUpdater crlStoreUpdater;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private X509CRLDetailsFactory x509CRLDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        IMPORT_CRLS,
        GET_CRLS,
        GET_CRLS_COUNT,
        GET_CRL,
        DELETE_CRL,
        DELETE_CRLS,
        EXPORT_CRLS,
        REFRESH_CRL_STORE,
        VALIDATE_CRL;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CRL,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.CRL, action.toPermissionName());
    }

    public CRLController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "CRL could not be read",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path= RestPaths.CRL_IMPORT_CRLS_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public List<CRLDTO.X509CRLDetails> importCRLs(@RequestBody MultipartFile derEncodedCRLs,
            boolean skipExpiredCRL)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_CRLS));

        try(InputStream crlInputStream = derEncodedCRLs.getInputStream())
        {
            Collection<X509CRL> crls = CRLUtils.readX509CRLs(crlInputStream);

            List<CRLDTO.X509CRLDetails> imported = new LinkedList<>();

            for (X509CRL crl : crls)
            {
                if (X509CRLInspector.isExpired(crl) && skipExpiredCRL) {
                    continue;
                }

                transactionOperations.executeWithoutResult(ctx ->
                {
                    try {
                        if (!crlStore.contains(crl))
                        {
                            crlStore.addCRL(crl);
                            imported.add(x509CRLDetailsFactory.createX509CRLDetails(crl));
                        }
                    }
                    catch (CRLStoreException e) {
                        logger.error("Error adding CRL.", e);
                    }
                });
            }

            return imported;
        }
        catch (CertificateException | NoSuchProviderException | CRLException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    "CRL could not be read. Message: " + e.getMessage(), e, logger);
        }
    }

    @GetMapping(RestPaths.CRL_GET_CRLS_PATH)
    public List<CRLDTO.X509CRLDetails> getCRLs(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CRLS);

            List<CRLDTO.X509CRLDetails> details = new LinkedList<>();

            try (CloseableIterator<? extends X509CRLStoreEntry> crlIterator = crlStore.getCRLStoreIterator(
                    null, firstResult, restValidator.limitMaxResults(maxResults, logger)))
            {
                while (crlIterator.hasNext())
                {
                    X509CRLStoreEntry crlEntry = crlIterator.next();

                    if (crlEntry.getCRL() != null) {
                        details.add(x509CRLDetailsFactory.createX509CRLDetails(crlEntry.getCRL()));
                    }

                    // clear first-level cache to save memory
                    sessionManager.getSession().clear();
                }

                return details;
            }
            catch (CRLStoreException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CRL_GET_CRLS_COUNT_PATH)
    public long getCRLsCount()
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CRLS_COUNT);

            return crlStore.size();
        })).orElse(0L);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "CRL not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.CRL_GET_CRL_PATH)
    public CRLDTO.X509CRLDetails getCRL(@RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CRL);

            try {
                return x509CRLDetailsFactory.createX509CRLDetails(getX509CRL(thumbprint));
            }
            catch (CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "CRL not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CRL_DELETE_CRL_PATH)
    public CRLDTO.X509CRLDetails deleteCRL(@RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_CRL);

            try {
                X509CRL crl = getX509CRL(thumbprint);

                crlStore.remove(crl);

                return x509CRLDetailsFactory.createX509CRLDetails(crl);
            }
            catch (CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE)
    })
    @PostMapping(RestPaths.CRL_DELETE_CRLS_PATH)
    public void deleteCRLs(@RequestBody List<String> thumbprints)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.DELETE_CRLS);

            try {
                for (String thumbprint : thumbprints)
                {
                    X509CRL crl = crlStore.getCRL(restValidator.validateThumbprint(thumbprint, logger));

                    if (crl != null) {
                        crlStore.remove(crl);
                    }
                }
            }
            catch (CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Missing thumbprints",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CRL_EXPORT_CRLS_PATH)
    public byte[] exportCRLs(@RequestParam ObjectEncoding encoding,
            @RequestBody List<String> thumbprints)
    {
        List<X509CRL> crls = new LinkedList<>();

        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.EXPORT_CRLS);

            if (thumbprints.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "thumbprints are missing", logger);
            }

            restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                    logger);

            try {
                for (String thumbprint : thumbprints)
                {
                    X509CRL crl = getX509CRL(thumbprint);

                    // clear first-level cache to save memory
                    sessionManager.getSession().clear();

                    crls.add(crl);
                }
            }
            catch (CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            CRLUtils.writeX509CRLs(crls, bos, encoding);
        }
        catch (CRLException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }

        return bos.toByteArray();
    }

    @PostMapping(RestPaths.CRL_REFRESH_CRL_STORE_PATH)
    public void refreshCRLStore()
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.REFRESH_CRL_STORE));

        crlStoreUpdater.update();
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "CRL not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CRL_VALIDATE_CRL_PATH)
    public CRLDTO.CRLValidationResult validateCRL(@RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_CRL);

            try {
                CRLDTO.CRLValidationResult result;

                X509CRL crl = getX509CRL(thumbprint);

                try {
                    CRLPathBuilder pathBuilder = pKISecurityServices.getCRLPathBuilderFactory().
                            createCRLPathBuilder();

                    pathBuilder.buildPath(crl);

                    result = new CRLDTO.CRLValidationResult(true, null);
                }
                catch (CertPathBuilderException e) {
                    // CertPathBuilderException is thrown for a lot of reasons. Try to extract the reason.
                    Throwable rootCause = ExceptionUtils.getRootCause(e);

                    Throwable cause = (rootCause != null ? rootCause : e);

                    String errorMessage;

                    if (cause instanceof CertificateExpiredException) {
                        errorMessage = "Certificate in the CRL path is expired. CRL: " +
                                       X509CRLInspector.toString(crl) + ". Message: " + cause.getMessage();
                    }
                    else {
                        errorMessage = "Error while building path for CRL. CRL: " +
                                       X509CRLInspector.toString(crl);

                        if (logger.isDebugEnabled()) {
                            logger.error(errorMessage, cause);
                        }
                        else {
                            logger.error("{}. Message: {}", errorMessage, cause.getMessage());
                        }
                    }

                    result = new CRLDTO.CRLValidationResult(false, errorMessage);
                }

                return result;
            }
            catch (CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private @Nonnull X509CRL getX509CRL(@Nonnull String thumbprint)
    throws CRLStoreException
    {
        X509CRL crl = crlStore.getCRL(restValidator.validateThumbprint(thumbprint, logger));

        if (crl == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("CRL with thumbprint %s not found", thumbprint), logger);
        }

        return crl;
    }
}
