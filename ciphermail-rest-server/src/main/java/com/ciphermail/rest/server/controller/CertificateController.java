/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SSLUtils;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.security.certificate.CertificateEncoder;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.keystore.KeyStoreLoader;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateImportAction;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.KeyExportFormat;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@Tag(name = "Certificate")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CertificateController
{
    private static final Logger logger = LoggerFactory.getLogger(CertificateController.class);

    private static final String NO_CERTIFICATES_IN_REQUEST_DESCRIPTION =
            "No certificates in request";

    private static final String CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION =
            "Certificate with the given thumbprint was not found";

    /*
     * The number of certificates imported within one database transaction
     */
    @Value("${ciphermail.rest.controller.certificate.certificate-import-batch-size:10}")
    private int certificateImportBatchSize;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    @Qualifier("certStore")
    private X509CertStoreExt certificateStore;

    @Autowired
    @Qualifier("rootStore")
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier("keyAndCertStore")
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier("keyAndRootCertStore")
    private KeyAndCertStore keyAndRootStore;

    @Autowired
    @Qualifier("keyAndCertificateWorkflow")
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier("keyAndRootCertificateWorkflow")
    private KeyAndCertificateWorkflow keyAndRootCertificateWorkflow;

    @Autowired
    private UserPreferencesWorkflow userPreferencesWorkflow;

    @Autowired
    private CertificatePathBuilderFactory certificatePathBuilderFactory;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private X509CertificateDetailsFactory x509CertificateDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_CERTIFICATE_DETAILS,
        GET_CERTIFICATES,
        GET_CERTIFICATES_COUNT,
        SEARCH_FOR_CERTIFICATES_BY_EMAIL,
        SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT,
        SEARCH_FOR_CERTIFICATES_BY_SUBJECT,
        SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT,
        SEARCH_FOR_CERTIFICATES_BY_ISSUER,
        SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT,
        GET_CERTIFICATE,
        IMPORT_CERTIFICATES,
        IMPORT_CERTIFICATES_PEM,
        IMPORT_CERTIFICATES_BASE64,
        DELETE_CERTIFICATE,
        DELETE_CERTIFICATES,
        EXPORT_CERTIFICATE,
        EXPORT_CERTIFICATE_CHAIN,
        EXPORT_CERTIFICATES,
        IMPORT_PRIVATE_KEYS,
        EXPORT_PRIVATE_KEYS,
        GET_CERTIFICATE_REFERENCED_DETAILS,
        IS_CERTIFICATE_REFERENCED,
        IMPORT_SYSTEM_ROOTS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values()) {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CERTIFICATE, action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.CERTIFICATE, action.toPermissionName());
    }

    public CertificateController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    /**
     * Returns the certificate details for the certificates from the request. This can be used to get certificate
     * details without having to import the certificates first.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path= RestPaths.CERTIFICATE_GET_CERTIFICATE_DETAILS_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public @Nonnull List<CertificateDTO.X509CertificateDetails> getCertificateDetails(
            @RequestBody MultipartFile chain)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.GET_CERTIFICATE_DETAILS));

        try (InputStream chainInputStream = chain.getInputStream())
        {
            List<X509Certificate> certificates = CertificateUtils.readX509Certificates(chainInputStream);

            if (certificates.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        NO_CERTIFICATES_IN_REQUEST_DESCRIPTION, logger);
            }

            return certificates.stream().map(c ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(c);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList();
        }
        catch (NoSuchProviderException | CertificateException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @GetMapping(RestPaths.CERTIFICATE_GET_CERTIFICATES_PATH)
    public List<CertificateDTO.X509CertificateDetails> getCertificates(
            @RequestParam CertificateStore store,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CERTIFICATES);

            try (CloseableIterator<? extends X509CertStoreEntry> certIterator = getCertStore(store).getCertStoreIterator(
                    expired, missingKeyAlias, firstResult,
                    restValidator.limitMaxResults(maxResults, logger)))
            {
                return getCertificates(certIterator);
            }
            catch (CertStoreException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CERTIFICATE_GET_CERTIFICATES_COUNT_PATH)
    public long getCertificatesCount(
            @RequestParam CertificateStore store,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias)
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CERTIFICATES_COUNT);

            return getCertStore(store).size(expired, missingKeyAlias);
        })).orElse(0L);
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_PATH)
    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesByEmail(
            @RequestParam CertificateStore store,
            @RequestParam String email,
            @RequestParam Match match,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_EMAIL);

            try (CloseableIterator<? extends X509CertStoreEntry> certIterator = getCertStore(store).getByEmail(
                    email, match, expired, missingKeyAlias, firstResult,
                    restValidator.limitMaxResults(maxResults, logger)))
            {
                return getCertificates(certIterator);
            }
            catch (CertStoreException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT_PATH)
    public long searchForCertificatesByEmailCount(
            @RequestParam CertificateStore store,
            @RequestParam String email,
            @RequestParam Match match,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias)
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT);

            try {
                return getCertStore(store).getByEmailCount(email, match, expired, missingKeyAlias);
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_PATH)
    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesBySubject(
            @RequestParam CertificateStore store,
            @RequestParam String subject,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_SUBJECT);

            try (CloseableIterator<? extends X509CertStoreEntry> certIterator = getCertStore(store).searchBySubject(
                    subject, expired, missingKeyAlias, firstResult,
                    restValidator.limitMaxResults(maxResults, logger)))
            {
                return getCertificates(certIterator);
            }
            catch (CertStoreException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT_PATH)
    public long searchForCertificatesBySubjectCount(
            @RequestParam CertificateStore store,
            @RequestParam String subject,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias)
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT);

            try {
                return getCertStore(store).getSearchBySubjectCount(subject, expired, missingKeyAlias);
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_PATH)
    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesByIssuer(
            @RequestParam CertificateStore store,
            @RequestParam String issuer,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_ISSUER);

            try (CloseableIterator<? extends X509CertStoreEntry> certIterator = getCertStore(store).searchByIssuer(
                    issuer, expired, missingKeyAlias, firstResult,
                    restValidator.limitMaxResults(maxResults, logger)))
            {
                return getCertificates(certIterator);
            }
            catch (CertStoreException | CloseableIteratorException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT_PATH)
    public long searchForCertificatesByIssuerCount(
            @RequestParam CertificateStore store,
            @RequestParam String issuer,
            @RequestParam Expired expired,
            @RequestParam MissingKeyAlias missingKeyAlias)
    {
        return Optional.ofNullable(transactionOperations.execute(ctx ->
        {
            checkPermission(Action.SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT);

            try {
                return getCertStore(store).getSearchByIssuerCount(issuer, expired, missingKeyAlias);
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @GetMapping(RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
    public CertificateDTO.X509CertificateDetails getCertificate(@RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_CERTIFICATE);

            try {
                return x509CertificateDetailsFactory.createX509CertificateDetails(getCertStore(store).getByThumbprint(
                        restValidator.validateThumbprint(thumbprint, logger)));
            }
            catch (CertStoreException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Import certificates from a multipart upload. DER (cer/p7b) and PEM encoded formats are supported.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public List<CertificateDTO.X509CertificateDetails> importCertificates(
            @RequestParam CertificateStore store,
            @RequestParam CertificateImportAction importAction,
            @RequestBody MultipartFile certificates)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_CERTIFICATES));

        try(InputStream certificatesInputStream = certificates.getInputStream())
        {
            List<X509CertStoreEntry> certStoreEntries = importCertificates(store, importAction,
                    CertificateUtils.readX509Certificates(certificatesInputStream));

            return transactionOperations.execute(tx -> certStoreEntries.stream().map(certStoreEntry ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList());
        }
        catch (CertificateException | NoSuchProviderException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    /**
     * Import certificates from a PEM encoded format.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PEM_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public List<CertificateDTO.X509CertificateDetails> importCertificatesPEM(
            @RequestParam CertificateStore store,
            @RequestParam CertificateImportAction importAction,
            @RequestBody String pemEncodedCertificates)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_CERTIFICATES_PEM));

        try {
            List<X509CertStoreEntry> certStoreEntries = importCertificates(store, importAction,
                    CertificateUtils.readX509Certificates(
                            IOUtils.toInputStream(pemEncodedCertificates, StandardCharsets.UTF_8)));

            return transactionOperations.execute(tx -> certStoreEntries.stream().map(certStoreEntry ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList());
        }
        catch (CertificateException | NoSuchProviderException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    /**
     * Import certificates from a base64 encoded value. DER (cer/p7b) and PEM encoded formats are supported.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_BASE64_PATH, consumes = { MediaType.TEXT_PLAIN_VALUE })
    public List<CertificateDTO.X509CertificateDetails> importCertificatesBase64(
            @RequestParam CertificateStore store,
            @RequestParam CertificateImportAction importAction,
            @RequestBody String base64EncodedCertificates)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_CERTIFICATES_BASE64));

        try {
            List<X509CertStoreEntry> certStoreEntries = importCertificates(store, importAction,
                    CertificateUtils.readX509Certificates(
                            new ByteArrayInputStream(Base64Utils.decode(base64EncodedCertificates))));

            return transactionOperations.execute(tx -> certStoreEntries.stream().map(certStoreEntry ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).toList());
        }
        catch (CertificateException | NoSuchProviderException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_DELETE_CERTIFICATE_PATH)
    public void deleteCertificate(
            @RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.DELETE_CERTIFICATE);

            try {
                getCertStore(store).removeCertificate(getCertStoreEntry(store, thumbprint).getCertificate());
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_DELETE_CERTIFICATES_PATH)
    public List<String> deleteCertificates(
            @RequestParam CertificateStore store,
            @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.DELETE_CERTIFICATES);

            List<String> notDeleted = new LinkedList<>();

            try {
                for (String thumbprint : thumbprints)
                {
                    // check if a certificate is "in-use" because it cannot be deleted
                    if (getCertificateReferenced(store, thumbprint).isEmpty()) {
                        getCertStore(store).removeCertificate(getCertStoreEntry(store, thumbprint).getCertificate());
                    }
                    else {
                        notDeleted.add(thumbprint);
                    }
                }

                return notDeleted;
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_PATH)
    public byte[] exportCertificate(
            @RequestParam CertificateStore store,
            @RequestParam String thumbprint,
            @RequestParam ObjectEncoding encoding)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.EXPORT_CERTIFICATE);

            try {
                return CertificateEncoder.encode(getCertStoreEntry(store, thumbprint).getCertificate(), encoding);
            }
            catch (CertStoreException | CertificateEncodingException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_CHAIN_PATH)
    public byte[] exportCertificateChain(
            @RequestParam ObjectEncoding encoding,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.EXPORT_CERTIFICATE_CHAIN);

            try {
                X509Certificate certificate = getCertStoreEntry(CertificateStore.CERTIFICATES, thumbprint)
                        .getCertificate();

                List<? extends Certificate> path = null;

                try {
                    CertPathBuilderResult pathResult = certificatePathBuilderFactory.createCertificatePathBuilder()
                            .buildPath(certificate);

                    CertPath certPath = pathResult.getCertPath();

                    if (certPath != null && CollectionUtils.isNotEmpty(certPath.getCertificates())) {
                        path = certPath.getCertificates();
                    }
                }
                catch (CertPathBuilderException e) {
                    logger.warn("Error building path", e);
                }

                if (path == null) {
                    path = List.of(certificate);
                }

                return CertificateEncoder.encode(path, encoding);
            }
            catch (IOException | CertStoreException | CertificateEncodingException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Missing thumbprints",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
    public byte[] exportCertificates(
            @RequestParam CertificateStore store,
            @RequestParam ObjectEncoding encoding,
            @RequestBody List<String> thumbprints)
    {
        List<X509Certificate> certificates = new LinkedList<>();

        transactionOperations.executeWithoutResult(ctx ->
        {
            checkPermission(Action.EXPORT_CERTIFICATES);

            if (thumbprints.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "thumbprints are missing", logger);
            }

            restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                    logger);

            try {
                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(store, thumbprint).getCertificate());
                }
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });

        // encode certificates outside of transaction to keep transaction short
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            // if there are multiple certificates, encode to PKCS7 or PEM
            if (certificates.size() > 1) {
                CertificateUtils.writeCertificates(certificates, bos, encoding);
            }
            else  {
                bos.write(CertificateEncoder.encode(certificates.get(0), encoding));
            }

            return bos.toByteArray();
        }
        catch (CertificateEncodingException | IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "PKCS#12 is invalid or the password is incorrect",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public List<CertificateDTO.X509CertificateDetails> importPrivateKeys(
            @RequestParam CertificateStore store,
            @RequestPart(required = false) String keyStorePassword,
            @RequestParam MissingKeyAction missingKeyAction,
            @RequestBody MultipartFile file)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.IMPORT_PRIVATE_KEYS));

        try {
            KeyStoreLoader keyStoreLoader = new KeyStoreLoader();

            KeyStore keyStore;

            try(InputStream inputStream = file.getInputStream()) {
                keyStore = keyStoreLoader.loadKeyStore(inputStream, StringUtils.defaultString(keyStorePassword));
            }
            catch (GeneralSecurityException | IOException e)
            {
                String errorMessage = e.getMessage();

                if (errorMessage == null) {
                    errorMessage = "Error loading private keys file";
                }

                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        errorMessage, e, logger);
            }

            boolean hasPrivateKeys = false;

            Enumeration<String> aliases = keyStore.aliases();

            while (aliases.hasMoreElements() && !hasPrivateKeys) {
                hasPrivateKeys = keyStore.isKeyEntry(aliases.nextElement());
            }

            if (!hasPrivateKeys)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "File does not contain any private keys", logger);
            }

            List<X509CertStoreEntry> importedEntries = getKeyAndCertificateWorkflow(store)
                    .importKeyStoreTransacted(keyStore, missingKeyAction);

            return transactionOperations.execute(tx ->
                importedEntries.stream().map(certStoreEntry ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).toList());
        }
        catch (KeyStoreException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Missing thumbprints",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "No entry with the given thumbprint was found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
    public byte[] exportPrivateKeys(
            @RequestParam CertificateStore store,
            @RequestParam KeyExportFormat keyExportFormat,
            @RequestParam(defaultValue = "true") boolean includeRoot,
            @RequestBody CertificateDTO.ExportPrivateKeysRequestBody body)
    {
        try {
            List<X509Certificate> certificates = new LinkedList<>();

            transactionOperations.executeWithoutResult(ctx ->
            {
                checkPermission(Action.EXPORT_PRIVATE_KEYS);

                if (body.thumbprints().isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Thumbprints are missing", logger);
                }

                restValidator.limitMaxResults(body.thumbprints().size(),
                        "number of thumbprints exceed the upper limit %s",
                        logger);

                try {
                    for (String thumbprint : body.thumbprints())
                    {
                        X509CertStoreEntry entry = getKeyAndCertStore(store).getByThumbprint(
                                restValidator.validateThumbprint(thumbprint, logger));

                        if (entry == null) {
                            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                    String.format("No entry with thumbprint %s found", thumbprint), logger);
                        }

                        if (entry.getCertificate() == null) {
                            // should never happen
                            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                                    String.format("Certificate is null for entry with thumbprint %s", thumbprint),
                                    logger);
                        }

                        certificates.add(entry.getCertificate());
                    }
                }
                catch (CertStoreException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            });

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            if (keyExportFormat == KeyExportFormat.PKCS12) {
                getKeyAndCertificateWorkflow(store).copyKeysToPKCS12OutputstreamTransacted(certificates,
                        StringUtils.defaultString(body.keyStorePassword()).toCharArray(), includeRoot, bos);
            }
            else if (keyExportFormat == KeyExportFormat.PEM) {
                getKeyAndCertificateWorkflow(store).copyKeysToPEMOutputstreamTransacted(certificates,
                        StringUtils.defaultString(body.keyStorePassword()).toCharArray(), includeRoot, bos);
            }
            else {
                throw new IllegalStateException("Unsupported format: " + keyExportFormat);
            }

            return bos.toByteArray();
        }
        catch (KeyStoreException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    private X509CertStoreExt getCertStore(@Nonnull CertificateStore store)
    {
        return switch (store)
        {
            case ROOTS        -> rootStore;
            case CERTIFICATES -> certificateStore;
        };
    }

    private KeyAndCertStore getKeyAndCertStore(@Nonnull CertificateStore store)
    {
        return switch (store)
        {
            case ROOTS        -> keyAndRootStore;
            case CERTIFICATES -> keyAndCertStore;
        };
    }

    private KeyAndCertificateWorkflow getKeyAndCertificateWorkflow(@Nonnull CertificateStore store)
    {
        return switch (store)
        {
            case ROOTS        -> keyAndRootCertificateWorkflow;
            case CERTIFICATES -> keyAndCertificateWorkflow;
        };
    }

    private @Nonnull List<CertificateDTO.X509CertificateDetails> getCertificates(
            @Nonnull CloseableIterator<? extends X509CertStoreEntry> certIterator)
    throws CloseableIteratorException
    {
        List<CertificateDTO.X509CertificateDetails> certificates = new LinkedList<>();

        while (certIterator.hasNext())
        {
            X509CertStoreEntry certStoreEntry = certIterator.next();

            try {
                certificates.add(x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry));
            }
            catch (IOException e) {
                logger.error("Error reading certificate", e);
            }

            // clear first-level cache to save memory
            sessionManager.getSession().clear();
        }

        return certificates;
    }

    private @Nonnull X509CertStoreEntry getCertStoreEntry(@Nonnull CertificateStore store, @Nonnull String thumbprint)
    throws CertStoreException
    {
        X509CertStoreEntry certStoreEntry = getCertStore(store).getByThumbprint(
                restValidator.validateThumbprint(thumbprint, logger));

        if (certStoreEntry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Certificate with thumbprint %s not found", thumbprint), logger);
        }

        return certStoreEntry;
    }

    private @Nonnull List<X509CertStoreEntry> importCertificates(
            @Nonnull CertificateStore store,
            @Nonnull CertificateImportAction importAction,
            @Nonnull List<X509Certificate> allCertificates)
    {
        // importing a large number of certificates within a transaction requires a lot of memory and a long
        // transaction. It is therefore better to split the import into batches.
        List<List<X509Certificate>> batches = Lists.partition(allCertificates, certificateImportBatchSize);

        if (batches.isEmpty()) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    NO_CERTIFICATES_IN_REQUEST_DESCRIPTION, logger);
        }

        List<X509CertStoreEntry> imported = new LinkedList<>();

        for (List<X509Certificate> certificates : batches)
        {
            transactionOperations.executeWithoutResult(tx ->
            {
                try {
                    X509CertStoreExt certStore = getCertStore(store);

                    for (X509Certificate certificate : certificates)
                    {
                        if (!certStore.contains(certificate))
                        {
                            boolean selfSigned = X509CertificateInspector.isSelfSigned(certificate);

                            if (importAction == CertificateImportAction.SKIP_SELF_SIGNED && selfSigned) {
                                continue;
                            }

                            if (importAction == CertificateImportAction.MUST_BE_SELF_SIGNED && !selfSigned) {
                                continue;
                            }

                            imported.add(certStore.addCertificate(certificate));
                        }
                    }
                }
                catch (CertStoreException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            });
        }

        return imported;
    }

    private List<UserPreferences> getCertificateReferenced(@Nonnull CertificateStore store,
            @RequestParam String thumbprint)
    {
        try {
            X509Certificate certificate = getCertStoreEntry(store, thumbprint).getCertificate();

            List<UserPreferences> userPreferences = new LinkedList<>();

            userPreferences.addAll(userPreferencesWorkflow.getReferencingFromCertificates(
                    certificate, 0, restValidator.getMaxResultsUpperLimit()));

            userPreferences.addAll(userPreferencesWorkflow.getReferencingFromNamedCertificates(
                    certificate, 0, restValidator.getMaxResultsUpperLimit()));

            userPreferences.addAll(userPreferencesWorkflow.getReferencingFromKeyAndCertificate(
                    certificate, 0, restValidator.getMaxResultsUpperLimit()));

            return userPreferences;
        }
        catch (CertStoreException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
        }
    }

    @Operation(summary = "Get all the references to a certificate",
            description =
                    """
                    If a certificate is referenced, for example because the certificate is selected as a signing
                    certificate for a user, the certificate cannot be deleted because that would result in a
                    database constraint violation. This operation will return a list of objects that references the
                    provided certificate.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_GET_CERTIFICATE_REFERENCED_DETAILS_PATH)
    public List<CertificateDTO.CertificateReference> getCertificateReferencedDetails(@Nonnull CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CERTIFICATE_REFERENCED_DETAILS);

            return getCertificateReferenced(store, thumbprint).stream().map(pref -> new CertificateDTO.CertificateReference(
                        pref.getCategory(), pref.getName())).limit(restValidator.getMaxResultsUpperLimit()).toList();
        });
    }

    @Operation(summary = "Checks if the certificate is referenced",
            description =
                    """
                    If a certificate is referenced, for example because the certificate is selected as a signing
                    certificate for a user, the certificate cannot be deleted because that would result in a
                    database constraint violation. This operation will return true if the certificate is referenced.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_IS_CERTIFICATE_REFERENCED_PATH)
    public boolean isCertificateReferenced(@Nonnull CertificateStore store, @RequestParam String thumbprint)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_CERTIFICATE_REFERENCED);

            return !getCertificateReferenced(store, thumbprint).isEmpty();
        }));
    }

    @PostMapping(RestPaths.CERTIFICATE_IMPORT_SYSTEM_ROOTS_PATH)
    public List<CertificateDTO.X509CertificateDetails> importSystemRoots()
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.IMPORT_SYSTEM_ROOTS);

            List<CertificateDTO.X509CertificateDetails> imported = new LinkedList<>();

            try {
                for (X509Certificate certificate : SSLUtils.getSystemTrustedRoots())
                {
                    try {
                        if (certificate == null) {
                            continue;
                        }

                        if (!X509CertificateInspector.isSelfSigned(certificate))
                        {
                            logger.debug("Certificate is not self signed: {}", certificate);

                            continue;
                        }

                        // Skip non CA certificates
                        if (!X509CertificateInspector.isCA(certificate))
                        {
                            logger.debug("Certificate is not a CA certificate: {}", certificate);

                            continue;
                        }

                        if (X509CertificateInspector.isExpired(certificate))
                        {
                            logger.debug("Certificate expired: {}", certificate);

                            continue;
                        }

                        if (!rootStore.contains(certificate))
                        {
                            rootStore.addCertificate(certificate);

                            imported.add(x509CertificateDetailsFactory.createX509CertificateDetails(certificate));
                        }
                    }
                    catch (Exception e) {
                        logger.error("Error adding certificate ", e);
                    }
                }

                return imported;
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }
}
