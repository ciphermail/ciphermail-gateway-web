/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.dkim.DKIMPrivateKeyManager;
import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.rest.core.DKIMDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.apache.commons.io.IOUtils;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.tagvalue.SignatureRecordImpl;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyStoreException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@Tag(name = "DKIM")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class DKIMController
{
    private static final Logger logger = LoggerFactory.getLogger(DKIMController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private DKIMPrivateKeyManager dKIMPrivateKeyManager;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GENERATE_KEY,
        GET_KEY_IDS,
        SET_KEY,
        GET_PUBLIC_KEY,
        GET_KEY_PAIR,
        IS_KEY_AVAILABLE,
        DELETE_KEY,
        PARSE_SIGNATURE_TEMPLATE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.DKIM,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.DKIM, action.toPermissionName());
    }

    public DKIMController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @PostMapping(path= RestPaths.DKIM_GENERATE_KEY_PATH)
    public void generateKey(
            @RequestParam String keyId,
            @RequestParam @Schema(allowableValues = {"1024", "2048", "3072", "4096"},
                    defaultValue = "2048") @Min(value = 1024) @Max(value = 4096) int keyLength)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.GENERATE_KEY);

            try {
                dKIMPrivateKeyManager.generateKey(keyId, keyLength);
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.DKIM_GET_KEY_IDS_PATH)
    public List<String> getKeyIds(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEY_IDS);

            try {
                return dKIMPrivateKeyManager.getKeyIds().stream()
                        .skip(firstResult)
                        .limit(restValidator.limitMaxResults(maxResults, logger))
                        .toList();
            }
            catch (KeyStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path=RestPaths.DKIM_SET_KEY_PATH)
    public void setKey(@RequestParam String keyId, @RequestBody MultipartFile pemEncodedKeyPair)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_KEY);

            try(InputStream pemEncodedKeyPairInputStream = pemEncodedKeyPair.getInputStream())
            {
                Object pem;

                try (PEMParser parser = new PEMParser(new InputStreamReader(pemEncodedKeyPairInputStream)))
                {
                    do {
                        pem = parser.readObject();

                        if (pem instanceof PEMKeyPair) {
                            break;
                        }
                    }
                    while (pem != null);
                }

                if (pem == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Request does not contain a PEM encoded KeyPair", logger);
                }

                JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();

                // Since we are importing PEM, we will use the non-sensitive provider because any private material
                // will not be stored on HSM
                keyConverter.setProvider(SecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

                dKIMPrivateKeyManager.setKey(keyId, keyConverter.getKeyPair((PEMKeyPair) pem));
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DKIM_GET_PUBLIC_KEY_PATH)
    public String getPublicKey(@RequestParam String keyId)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PUBLIC_KEY);

            try {
                String base64EncodedPublicKey = null;

                KeyPair keypair = dKIMPrivateKeyManager.getKey(keyId);

                if (keypair != null) {
                    base64EncodedPublicKey = Base64Utils.encode(keypair.getPublic().getEncoded());
                }

                return base64EncodedPublicKey;
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DKIM_GET_KEY_PAIR_PATH)
    public String getKeyPair(@RequestParam String keyId)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEY_PAIR);

            try {
                String pemEncodedKeyPair = null;

                KeyPair keypair = dKIMPrivateKeyManager.getKey(keyId);

                if (keypair != null)
                {
                    StringWriter pemWriter = new StringWriter();

                    JcaPEMWriter writer = new JcaPEMWriter(pemWriter);

                    try {
                        writer.writeObject(keypair);
                    }
                    finally {
                        IOUtils.closeQuietly(writer);
                        IOUtils.closeQuietly(pemWriter);
                    }

                    pemEncodedKeyPair = pemWriter.toString();
                }

                return pemEncodedKeyPair;
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DKIM_IS_KEY_AVAILABLE_PATH)
    public boolean isKeyAvailable(@RequestParam String keyId)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_KEY_AVAILABLE);

            try {
                return dKIMPrivateKeyManager.isValidKey(keyId);
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    @PostMapping(path=RestPaths.DKIM_DELETE_KEY_PATH)
    public boolean deleteKey(@RequestParam String keyId)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.DELETE_KEY);

            try {
                boolean deleted = false;

                if (dKIMPrivateKeyManager.isValidKey(keyId))
                {
                    dKIMPrivateKeyManager.deleteKey(keyId);

                    deleted = true;
                }

                return deleted;
            }
            catch (IOException | GeneralSecurityException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    @PostMapping(path=RestPaths.DKIM_PARSE_SIGNATURE_TEMPLATE_PATH)
    public DKIMDTO.SignatureTemplate parseSignatureTemplate(@RequestBody MultipartFile signatureTemplate)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.PARSE_SIGNATURE_TEMPLATE);

            try(InputStream signatureTemplateInputStream = signatureTemplate.getInputStream())
            {
                SignatureRecord signatureRecord = new SignatureRecordImpl(DKIMUtils.replaceSignatureTemplateTokens(
                        IOUtils.toString(signatureTemplateInputStream, StandardCharsets.UTF_8),
                        // use test email address
                        "test@example.com"));

                signatureRecord.validate();

                return new DKIMDTO.SignatureTemplate(
                        signatureRecord.getHeaders(),
                        signatureRecord.getIdentityLocalPart(),
                        signatureRecord.getIdentity(),
                        signatureRecord.getHashKeyType(),
                        signatureRecord.getHashMethod(),
                        signatureRecord.getHashAlgo(),
                        signatureRecord.getSelector(),
                        signatureRecord.getDToken(),
                        signatureRecord.getBodyHash(),
                        signatureRecord.getBodyHashLimit(),
                        signatureRecord.getHeaderCanonicalisationMethod(),
                        signatureRecord.getBodyCanonicalisationMethod(),
                        signatureRecord.getRecordLookupMethods(),
                        signatureRecord.getSignature());
            }
            catch (IOException | IllegalStateException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Error parsing DKIM template [%s]", e.getMessage()), logger);
            }
        });
    }
}
