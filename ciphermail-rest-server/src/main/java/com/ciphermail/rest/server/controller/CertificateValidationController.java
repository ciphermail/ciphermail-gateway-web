/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.validator.CertificateValidator;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMEEncryption;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMESigning;
import com.ciphermail.core.common.security.certificate.validator.PKITrustCheckCertificateValidator;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.SingleCertificateCertPath;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@Tag(name = "CertificateValidation")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CertificateValidationController
{
    private static final Logger logger = LoggerFactory.getLogger(CertificateValidationController.class);

    private static final String CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION =
            "A certificate with the given thumbprint was not found";

    private static final String NO_CERTIFICATES_IN_REQUEST_DESCRIPTION =
            "No certificates in request";

    @Autowired
    private TransactionOperations transactionOperations;

    /*
     * Gives access to all the PKI services
     */
    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private X509CertificateDetailsFactory x509CertificateDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        VALIDATE_CERTIFICATE_FOR_SIGNING,
        VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING,
        VALIDATE_CERTIFICATE_FOR_ENCRYPTION,
        VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION,
        VALIDATE_CERTIFICATE,
        VALIDATE_EXTERNAL_CERTIFICATE,
        GET_ISSUER_CERTIFICATE;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CERTIFICATE_VALIDATION,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.CERTIFICATE_VALIDATION, action.toPermissionName());
    }

    public CertificateValidationController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH)
    public CertificateDTO.CertificateValidationResult validateCertificateForSigning(@RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_CERTIFICATE_FOR_SIGNING);

            try {
                return validateCertificateForSigning(getCertificate(store, thumbprint), null);
            }
            catch (CertStoreException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException |
                   IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Checks whether an uploaded certificate is valid for signing. The main difference between this function and
     * validateCertificateForSigning is that the certificate does not have to be imported before validation. This can
     * be used if the REST api user would like to know whether a certificate is valid prior to importing it into the
     * store
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING_PATH,
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public CertificateDTO.CertificateValidationResult validateExternalCertificateForSigning(
            @RequestParam(required = false) String thumbprint, @RequestBody MultipartFile chain)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING);

            try(InputStream chainInputStream = chain.getInputStream())
            {
                List<X509Certificate> certificates = CertificateUtils.readX509Certificates(chainInputStream);

                if (certificates.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            NO_CERTIFICATES_IN_REQUEST_DESCRIPTION, logger);
                }

                List<X509Certificate> chainCertificates = new LinkedList<>(certificates);

                return validateCertificateForSigning(
                        getCertificateWithThumbprint(thumbprint, certificates),
                        chainCertificates);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException | CertificateException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
    public CertificateDTO.CertificateValidationResult validateCertificateForEncryption(@RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_CERTIFICATE_FOR_ENCRYPTION);

            try {
                return validateCertificateForEncryption(getCertificate(store, thumbprint), null);
            }
            catch (CertStoreException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException |
                   IOException e)

            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Checks whether an uploaded certificate is valid for encryption. The main difference between this function and
     * validateCertificateForEncryption is that the certificate does not have to be imported before validation. This can
     * be used if the REST api user would like to know whether a certificate is valid prior to importing it into the
     * store
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION_PATH,
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public CertificateDTO.CertificateValidationResult validateExternalCertificateForEncryption(
            @RequestParam(required = false) String thumbprint, @RequestBody MultipartFile chain)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION);

            try(InputStream chainInputStream = chain.getInputStream())
            {
                List<X509Certificate> certificates = CertificateUtils.readX509Certificates(chainInputStream);

                if (certificates.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            NO_CERTIFICATES_IN_REQUEST_DESCRIPTION, logger);
                }

                List<X509Certificate> chainCertificates = new LinkedList<>(certificates);

                return validateCertificateForEncryption(
                        getCertificateWithThumbprint(thumbprint, certificates),
                        chainCertificates);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException | CertificateException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
    public CertificateDTO.CertificateValidationResult validateCertificate(@RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_CERTIFICATE);

            try {
                return checkValidity(getCertificate(store, thumbprint), null);
            }
            catch (CertStoreException | NoSuchAlgorithmException | NoSuchProviderException | CertificateException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Checks whether an uploaded certificate is valid. The main difference between this function and
     * validateCertificate is that the certificate does not have to be imported before validation. This can
     * be used if the REST api user would like to know whether a certificate is valid prior to importing it into the
     * store
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = NO_CERTIFICATES_IN_REQUEST_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_PATH,
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public CertificateDTO.CertificateValidationResult validateExternalCertificate(
            @RequestParam(required = false) String thumbprint, @RequestBody MultipartFile chain)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.VALIDATE_EXTERNAL_CERTIFICATE);

            try(InputStream chainInputStream = chain.getInputStream())
            {
                List<X509Certificate> certificates = CertificateUtils.readX509Certificates(chainInputStream);

                if (certificates.isEmpty()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            NO_CERTIFICATES_IN_REQUEST_DESCRIPTION, logger);
                }

                List<X509Certificate> chainCertificates = new LinkedList<>(certificates);

                return checkValidity(
                        getCertificateWithThumbprint(thumbprint, certificates),
                        chainCertificates);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException | CertificateException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH)
    public CertificateDTO.IssuerWithStore getIssuerCertificate(@RequestParam CertificateStore store,
            @RequestParam String thumbprint)
    {
        return transactionOperations.execute(ctx ->
        {
            checkPermission(Action.GET_ISSUER_CERTIFICATE);

            try {
                X509Certificate certificate = getCertificate(store, thumbprint);

                CertPath certPath = null;
                X509Certificate issuer = null;
                CertificateStore issuerStore = null;
                TrustAnchor trustAnchor = null;

                try {
                    CertificatePathBuilder pathBuilder = pKISecurityServices.getCertificatePathBuilderFactory().
                            createCertificatePathBuilder();

                    CertPathBuilderResult pathBuilderResult = pathBuilder.buildPath(certificate);

                    certPath = pathBuilderResult.getCertPath();

                    if (pathBuilderResult instanceof PKIXCertPathBuilderResult pkixCertPathBuilderResult) {
                        trustAnchor = pkixCertPathBuilderResult.getTrustAnchor();
                    }
                }
                catch (Exception e) {
                    // Log on debug level because CertPathBuilderException is thrown when the path cannot be built
                    // which can happen if the chain is not available
                    logger.debug("buildPath failed.", e);
                }

                if (certPath == null) {
                    // If a complete path cannot be built because for example a root certificate is missing, the
                    // certificate path will be null, i.e., an incomplete path will not be returned. We will therefore
                    // try to find the issuer certificate by looking for a certificate which signed this certificate.
                    // Note that this does not imply that the incomplete chain is a valid chain.
                    certPath = findIssuer(certificate, store);
                }

                if (certPath != null)
                {
                    List<? extends Certificate> path = certPath.getCertificates();

                    if (CollectionUtils.isNotEmpty(path))
                    {
                        if (CollectionUtils.getSize(path) == 1)
                        {
                            // Since there is only one certificate (the certificate itself) we need
                            // to check whether there is a root in the path
                            if (trustAnchor != null)
                            {
                                issuer = trustAnchor.getTrustedCert();
                                issuerStore = CertificateStore.ROOTS;
                            }
                            else {
                                // The path was not complete but the issuer was found with findIssuer
                                issuer = (X509Certificate) path.get(0);
                                issuerStore = CertificateStore.CERTIFICATES;
                            }
                        }
                        else
                        {
                            issuer = (X509Certificate) path.get(1);
                            issuerStore = CertificateStore.CERTIFICATES;
                        }
                    }
                }

                if (issuerStore == null) {
                    issuerStore = CertificateStore.CERTIFICATES;
                }

                return issuer != null ? new CertificateDTO.IssuerWithStore(x509CertificateDetailsFactory
                        .createX509CertificateDetails(issuer), issuerStore) : null;
            }
            catch (CertStoreException | IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private X509Certificate getCertificateWithThumbprint(String thumbprint, @Nonnull List<X509Certificate> certificates)
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        thumbprint = StringUtils.trimToNull(thumbprint);

        if (thumbprint == null && certificates.size() == 1) {
            thumbprint = X509CertificateInspector.getThumbprint(certificates.get(0));
        }

        if (thumbprint == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Thumbprint is required because there are multiple certificates in the request", logger);
        }

        for (X509Certificate certificate : certificates)
        {
            if (thumbprint.equals(X509CertificateInspector.getThumbprint(certificate))) {
                return certificate;
            }
        }

        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("Certificate with thumbprint %s not found", thumbprint), logger);
    }

    private CertificateDTO.CertificateValidationResult validateCertificateForSigning(@Nonnull X509Certificate certificate,
            Collection<? extends Certificate> additionalCertificates)
    throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        CertificateDTO.CertificateValidationResult validatorResult = checkValidity(certificate, additionalCertificates);

        if (validatorResult.valid()) {
            // Certificate is trusted etc. now check if it can be used for
            // S/MIME signatures
            CertificateValidator validator = new IsValidForSMIMESigning();

            if (!validator.isValid(certificate)) {
                validatorResult = new CertificateDTO.CertificateValidationResult(false /* not valid */,
                        validatorResult.trusted(), validatorResult.revoked(),
                        validatorResult.blackListed(), validatorResult.whiteListed(),
                        "Certificate cannot be used for S/MIME signatures. " +
                        ObjectUtils.toString(validator.getFailureMessage()),
                        x509CertificateDetailsFactory.createX509CertificateDetails(certificate));
            }
        }

        return validatorResult;
    }

    private CertificateDTO.CertificateValidationResult validateCertificateForEncryption(@Nonnull X509Certificate certificate,
            Collection<? extends Certificate> additionalCertificates)
    throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException, IOException
    {
        CertificateDTO.CertificateValidationResult validatorResult = checkValidity(certificate, additionalCertificates);

        if (validatorResult.valid()) {
            // Certificate is trusted etc. now check if it can be used for
            // S/MIME encryption
            CertificateValidator validator = new IsValidForSMIMEEncryption();

            if (!validator.isValid(certificate)) {
                validatorResult = new CertificateDTO.CertificateValidationResult(false /* not valid */,
                        validatorResult.trusted(), validatorResult.revoked(),
                        validatorResult.blackListed(), validatorResult.whiteListed(),
                        "Certificate cannot be used for S/MIME encryption. " +
                        ObjectUtils.toString(validator.getFailureMessage()),
                        x509CertificateDetailsFactory.createX509CertificateDetails(certificate));
            }
        }

        return validatorResult;
    }

    private X509Certificate getCertificate(CertificateStore store, String thumbprint)
    throws CertStoreException
    {
        X509CertStoreExt certStore = switch (store) {
            case CERTIFICATES -> pKISecurityServices.getKeyAndCertStore();
            case ROOTS        -> pKISecurityServices.getRootStore();
            default -> throw new IllegalArgumentException("Unsupported CertificateStore: " + store);
        };

        X509CertStoreEntry entry = certStore.getByThumbprint(restValidator.validateThumbprint(thumbprint, logger));

        if (entry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Certificate with thumbprint %s not found", thumbprint), logger);
        }

        return entry.getCertificate();
    }

    private CertificateDTO.CertificateValidationResult checkValidity(@Nonnull X509Certificate certificate,
            Collection<? extends Certificate> additionalCertificates)
    throws CertificateEncodingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        PKITrustCheckCertificateValidator validator = pKISecurityServices.
                getPKITrustCheckCertificateValidatorFactory().createValidator(additionalCertificates);

        CertificateDTO.CertificateValidationResult result;

        try {
            validator.isValid(certificate);

            result = new CertificateDTO.CertificateValidationResult(
                    validator.isValid(),
                    validator.isTrusted(),
                    validator.isRevoked(),
                    validator.isBlackListed(),
                    validator.isWhiteListed(),
                    StringUtils.trimToNull(validator.getFailureMessage()),
                    x509CertificateDetailsFactory.createX509CertificateDetails(certificate));
        }
        catch (CertificateException | IOException e)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error validating the certificate with thumbprint %s. Message: %s",
                            X509CertificateInspector.getThumbprint(certificate),
                            ExceptionUtils.getRootCauseMessage(e)), e, logger);
        }

        return result;
    }

    /*
     * Find issuer by searching for a certificate that signed the certificate
     */
    private CertPath findIssuer(@Nonnull X509Certificate certificate, @Nonnull CertificateStore store)
    {
        try {
            X509CertSelector selector = new X509CertSelector();

            selector.setSubject(certificate.getIssuerX500Principal());

            X509CertStoreExt certStore = switch (store) {
                case CERTIFICATES -> pKISecurityServices.getKeyAndCertStore();
                case ROOTS        -> pKISecurityServices.getRootStore();
                default -> throw new IllegalArgumentException("Unknown CertificateStore.");
            };

            CloseableIterator<X509Certificate> iterator = certStore.getCertificateIterator(selector);

            while (iterator.hasNext())
            {
                X509Certificate issuer = iterator.next();

                // Skip if the issuer is the same as the certificate
                if (certificate.equals(issuer)) {
                    continue;
                }

                try {
                    certificate.verify(issuer.getPublicKey());

                    // Signature was correct. Issuer found.
                    return new SingleCertificateCertPath(issuer);
                }
                catch (Exception e) {
                    // Signature was not correct
                }
            }
        }
        catch (Exception e) {
            logger.error("Error in #findIssuer", e);
        }

        // No issuer found
        return null;
    }
}
