/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.app.admin.Role;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.authorization.Permissions;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionOperations;

@Service
// suppress warning because some bean are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
@Configuration
@SecurityScheme(
        name = "basicAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
@OpenAPIDefinition(
        info = @Info(title = "CipherMail back-end API", version = "v1"),
        security = @SecurityRequirement(name = "basicAuth")
)
public class RestService
{
    @Value("${ciphermail.rest.authentication.admin.default.role}")
    private String adminRole;

    @Autowired
    private Permissions permissions;

    @Autowired
    private RoleManager roleManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PermissionProviderRegistry permissionProviderRegistry;

    @EventListener(ApplicationReadyEvent.class)
    @Order(0)
    private void init()
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            // initialize the permissions registry
            permissionProviderRegistry.getPermissionProviders().forEach(p ->
                    permissions.addPermissions(p.getPermissions()));

            // add admin role with all permissions
            Role role = roleManager.getRole(adminRole);

            if (role == null) {
                role = roleManager.persistRole(roleManager.createRole(adminRole));
            }

            // the admin role should have all permissions
            role.getPermissions().clear();
            role.getPermissions().addAll(permissions.getPermissions());
        });
    }
}
