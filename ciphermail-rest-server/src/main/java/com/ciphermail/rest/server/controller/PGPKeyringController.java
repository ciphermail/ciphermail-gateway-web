/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyType;
import com.ciphermail.core.common.security.openpgp.PGPSearchField;
import com.ciphermail.core.common.security.openpgp.PGPSearchParameters;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.PGPKeyDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "PGPKeyring")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PGPKeyringController
{
    private static final Logger logger = LoggerFactory.getLogger(PGPKeyringController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    /*
     * The Keyring which stores the PGP keys
     */
    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPKeyDetailsFactory detailsFactory;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_KEY,
        GET_KEY_BY_SHA256_FINGERPRINT,
        GET_SUBKEYS,
        GET_KEYS,
        GET_KEYS_COUNT,
        SEARCH_KEYS,
        SEARCH_KEYS_COUNT,
        DELETE_KEY,
        DELETE_KEYS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.PGP_KEYRING,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.PGP_KEYRING, action.toPermissionName());
    }

    public PGPKeyringController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.PGP_KEYRING_GET_KEY_PATH)
    public PGPDTO.PGPKeyDetails getKey(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEY);

            try {
                return detailsFactory.createPGPKeyDetails(getPGPKeyRingEntryById(id));
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH)
    public PGPDTO.PGPKeyDetails getKeyBySha256Fingerprint(@RequestParam String sha256Fingerprint)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEY_BY_SHA256_FINGERPRINT);

            try {
                return detailsFactory.createPGPKeyDetails(keyRing.getBySha256Fingerprint(
                        restValidator.validatePGPSha256Fingerprint(sha256Fingerprint, logger)));
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
    public List<PGPDTO.PGPKeyDetails> getSubkeys(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SUBKEYS);

            try {
                return Optional.ofNullable(restValidator.validateIsMasterKey(getPGPKeyRingEntryById(id), logger)
                                .getSubkeys()).orElse(Collections.emptySet())
                        .stream().map(detailsFactory::createPGPKeyDetails).toList();
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(RestPaths.PGP_KEYRING_GET_KEYS_PATH)
    public List<PGPDTO.PGPKeyDetails> getKeys(
            @RequestParam(defaultValue = "MATCH_ALL") Expired expired,
            @RequestParam(defaultValue = "ALLOWED") MissingKeyAlias missingKeyAlias,
            @RequestParam(defaultValue = "MASTER_KEY") PGPKeyType keyType,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEYS);

            List<PGPDTO.PGPKeyDetails> keys = new LinkedList<>();

            try (CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getIterator(
                    new PGPSearchParameters(null, expired, missingKeyAlias, keyType),
                    firstResult, restValidator.limitMaxResults(maxResults, logger)))

            {
                while(iterator.hasNext()) {
                    keys.add(detailsFactory.createPGPKeyDetails(iterator.next()));
                }
            }
            catch (PGPException | CloseableIteratorException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }

            return keys;
        });
    }

    @GetMapping(RestPaths.PGP_KEYRING_GET_KEYS_COUNT_PATH)
    public long getKeysCount(
            @RequestParam(defaultValue = "MATCH_ALL") Expired expired,
            @RequestParam(defaultValue = "ALLOWED") MissingKeyAlias missingKeyAlias,
            @RequestParam(defaultValue = "MASTER_KEY") PGPKeyType keyType)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_KEYS_COUNT);

            try {
                return keyRing.getCount(new PGPSearchParameters(null, expired, missingKeyAlias, keyType));
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @GetMapping(RestPaths.PGP_KEYRING_SEARCH_KEYS_PATH)
    public List<PGPDTO.PGPKeyDetails> searchKeys(
            @RequestParam(defaultValue = "EMAIL") PGPSearchField searchField,
            @RequestParam String searchValue,
            @RequestParam(defaultValue = "EXACT") Match match,
            @RequestParam(defaultValue = "MATCH_ALL") Expired expired,
            @RequestParam(defaultValue = "ALLOWED") MissingKeyAlias missingKeyAlias,
            @RequestParam(defaultValue = "MASTER_KEY") PGPKeyType keyType,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SEARCH_KEYS);

            List<PGPDTO.PGPKeyDetails> keys = new LinkedList<>();

            try (CloseableIterator<PGPKeyRingEntry> iterator = keyRing.search(searchField, searchValue,
                    new PGPSearchParameters(match, expired, missingKeyAlias, keyType),
                    firstResult, restValidator.limitMaxResults(maxResults, logger)))

            {
                while(iterator.hasNext()) {
                    keys.add(detailsFactory.createPGPKeyDetails(iterator.next()));
                }
            }
            catch (PGPException | CloseableIteratorException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }

            return keys;
        });
    }

    @GetMapping(RestPaths.PGP_KEYRING_SEARCH_KEYS_COUNT_PATH)
    public long searchKeysCount(
            @RequestParam(defaultValue = "EMAIL") PGPSearchField searchField,
            @RequestParam String searchValue,
            @RequestParam(defaultValue = "EXACT") Match match,
            @RequestParam(defaultValue = "MATCH_ALL") Expired expired,
            @RequestParam(defaultValue = "ALLOWED") MissingKeyAlias missingKeyAlias,
            @RequestParam(defaultValue = "MASTER_KEY") PGPKeyType keyType)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.SEARCH_KEYS_COUNT);

            try {
                return keyRing.searchCount(searchField, searchValue,
                        new PGPSearchParameters(match, expired, missingKeyAlias, keyType));
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        })).orElse(0L);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.PGP_KEYRING_DELETE_KEY_PATH)
    public void deleteKey(@RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_KEY);

            try {
                keyRing.delete(getPGPKeyRingEntryById(id).getID());
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(RestPaths.PGP_KEYRING_DELETE_KEYS_PATH)
    public void deleteKeys(@RequestBody List<UUID> ids)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_KEYS);

            try {
                for(UUID id : ids) {
                    keyRing.delete(getPGPKeyRingEntryById(id).getID());
                }
            }
            catch (PGPException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private @Nonnull PGPKeyRingEntry getPGPKeyRingEntryById(@Nonnull UUID id)
    throws PGPException, IOException
    {
        PGPKeyRingEntry keyRingEntry = keyRing.getByID(id);

        if (keyRingEntry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Key with id %s not found", id), logger);
        }

        return keyRingEntry;
    }
}
