/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.dlp.PolicyPatternManager;
import com.ciphermail.core.app.dlp.PolicyPatternNode;
import com.ciphermail.core.app.dlp.UpdateableWordSkipper;
import com.ciphermail.core.app.dlp.UserPolicyPatternNode;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManager;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.MimeMessageTextExtractor;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.dlp.TextNormalizer;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.dlp.impl.PolicyPatternImpl;
import com.ciphermail.core.common.extractor.ExtractedPart;
import com.ciphermail.core.common.extractor.impl.TextExtractorUtils;
import com.ciphermail.core.common.locale.CharacterEncoding;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.common.util.StringIterator;
import com.ciphermail.rest.core.DLPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@RestController
@Tag(name = "DLP")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class DLPController
{
    private static final Logger logger = LoggerFactory.getLogger(DLPController.class);

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @Autowired
    private ValidatorRegistry validatorRegistry;

    @Autowired
    private MatchFilterRegistry matchFilterRegistry;

    @Autowired
    private MimeMessageTextExtractor textExtractor;

    @Autowired
    private TextNormalizer textNormalizer;

    @Autowired
    private UpdateableWordSkipper wordSkipper;

    @Autowired
    private UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private RestValidator restValidator;

    private enum Action
    {
        ADD_POLICY_PATTERN,
        ADD_POLICY_GROUP,
        UPDATE_POLICY_PATTERN,
        GET_POLICY_PATTERN,
        GET_POLICY_PATTERNS_BY_NAME,
        DELETE_POLICY_PATTERN,
        DELETE_POLICY_PATTERNS,
        GET_POLICY_PATTERNS,
        GET_POLICY_PATTERNS_COUNT,
        RENAME_POLICY_PATTERN,
        IS_REFERENCED,
        GET_REFERENCED_DETAILS,
        SET_CHILD_NODES,
        ADD_CHILD_NODE,
        REMOVE_CHILD_NODE,
        EXTRACT_TEXT_FROM_EMAIL,
        GET_MATCH_FILTERS,
        GET_MATCH_FILTER,
        GET_VALIDATORS,
        GET_VALIDATOR,
        GET_SKIP_LIST,
        SET_SKIP_LIST,
        SET_USER_POLICY_PATTERNS,
        GET_USER_POLICY_PATTERNS,
        SET_DOMAIN_POLICY_PATTERNS,
        GET_DOMAIN_POLICY_PATTERNS,
        SET_GLOBAL_POLICY_PATTERNS,
        GET_GLOBAL_POLICY_PATTERNS;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.DLP,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.DLP, action.toPermissionName());
    }

    public DLPController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Match filter with name does not exist, Validator with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Regular expression is invalid",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.CONFLICT_RESPONSE_CODE,
                    description = "Policy pattern with name already exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path= RestPaths.DLP_ADD_POLICY_PATTERN_PATH)
    public DLPDTO.DLPPolicyPattern addPolicyPattern(
            @RequestParam String name,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) String notes,
            @RequestParam String regEx,
            @RequestParam(required = false) String validator,
            @RequestParam(defaultValue = "1") int threshold,
            @RequestParam PolicyViolationAction action,
            @RequestParam boolean delayEvaluation,
            @RequestParam(required = false) String matchFilter)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.ADD_POLICY_PATTERN);

            if (policyPatternManager.getPattern(name) != null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.CONFLICT,
                        String.format("Policy pattern with name %s already exist", name), logger);
            }

            PolicyPatternNode node = policyPatternManager.createPattern(name);

            PolicyPatternImpl pattern;

            try {
                pattern = new PolicyPatternImpl(name, Pattern.compile(regEx));
            }
            catch (PatternSyntaxException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                         String.format("Regular expression is invalid [%s]", e.getMessage()), logger);
            }

            if (StringUtils.isNotBlank(validator) && validatorRegistry.getValidator(validator) == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Validator with name %s does not exist", validator), logger);
            }

            if (StringUtils.isNotBlank(matchFilter) && matchFilterRegistry.getMatchFilter(matchFilter) == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Match filter with name %s does not exist", matchFilter), logger);
            }

            pattern.setDescription(StringUtils.trimToNull(description));
            pattern.setNotes(StringUtils.trimToNull(notes));
            pattern.setValidatorName(StringUtils.trimToNull(validator));
            pattern.setThreshold(threshold);
            pattern.setAction(action);
            pattern.setDelayEvaluation(delayEvaluation);
            pattern.setMatchFilterName(StringUtils.trimToNull(matchFilter));

            node.setPolicyPattern(pattern);

            return toDLPPolicyPattern(node);
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.CONFLICT_RESPONSE_CODE,
                    description = "Policy pattern with name already exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path= RestPaths.DLP_ADD_POLICY_GROUP_PATH)
    public DLPDTO.DLPPolicyPattern addPolicyGroup(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.ADD_POLICY_GROUP);

            if (policyPatternManager.getPattern(name) != null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.CONFLICT,
                        String.format("Policy pattern with name %s already exist", name), logger);
            }

            return toDLPPolicyPattern(policyPatternManager.createPattern(name));
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Match filter with name does not exist, Validator with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Regular expression is invalid",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_UPDATE_POLICY_PATTERN_PATH)
    public DLPDTO.DLPPolicyPattern updatePolicyPattern(
            @RequestParam String name,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) String notes,
            @RequestParam String regEx,
            @RequestParam(required = false) String validator,
            @RequestParam(defaultValue = "1") int threshold,
            @RequestParam PolicyViolationAction action,
            @RequestParam boolean delayEvaluation,
            @RequestParam(required = false) String matchFilter)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.UPDATE_POLICY_PATTERN);

            PolicyPatternNode node = getPolicyPatternNode(name);

            PolicyPatternImpl pattern;

            try {
                pattern = new PolicyPatternImpl(name, Pattern.compile(regEx));
            }
            catch (PatternSyntaxException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Regular expression is invalid [%s]", e.getMessage()), logger);
            }

            if (StringUtils.isNotEmpty(validator) && validatorRegistry.getValidator(validator) == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Validator with name %s does not exist", validator), logger);
            }

            if (StringUtils.isNotEmpty(matchFilter) && matchFilterRegistry.getMatchFilter(matchFilter) == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Match filter with name %s does not exist", matchFilter), logger);
            }

            pattern.setDescription(StringUtils.trimToNull(description));
            pattern.setNotes(StringUtils.trimToNull(notes));
            pattern.setValidatorName(StringUtils.trimToNull(validator));
            pattern.setThreshold(threshold);
            pattern.setAction(action);
            pattern.setDelayEvaluation(delayEvaluation);
            pattern.setMatchFilterName(StringUtils.trimToNull(matchFilter));

            node.setPolicyPattern(pattern);

            return toDLPPolicyPattern(node);
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_POLICY_PATTERN_PATH)
    public DLPDTO.DLPPolicyPattern getPolicyPattern(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_POLICY_PATTERN);

            return toDLPPolicyPattern(policyPatternManager.getPattern(name));
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_GET_POLICY_PATTERNS_BY_NAME_PATH)
    public List<DLPDTO.DLPPolicyPattern> getPolicyPatternsByName(@RequestBody List<String> names)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_POLICY_PATTERNS_BY_NAME);

            List<DLPDTO.DLPPolicyPattern> result = new LinkedList<>();

            for (String name : names) {
                result.add(toDLPPolicyPattern(getPolicyPatternNode(name)));
            }

            return result;
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_DELETE_POLICY_PATTERN_PATH)
    public DLPDTO.DLPPolicyPattern deletePolicyPattern(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.DELETE_POLICY_PATTERN);

            PolicyPatternNode node = getPolicyPatternNode(name);

            policyPatternManager.deletePattern(node.getName());

            return toDLPPolicyPattern(node);
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
    })
    @PostMapping(path=RestPaths.DLP_DELETE_POLICY_PATTERNS_PATH)
    public void deletePolicyPatterns(@RequestBody List<String> names)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_POLICY_PATTERNS);

            for (String name : names)
            {
                PolicyPatternNode patternNode = policyPatternManager.getPattern(name);

                if (patternNode != null && !policyPatternManager.isInUse(name)) {
                    policyPatternManager.deletePattern(name);
                }
            }
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_POLICY_PATTERNS_PATH)
    public List<DLPDTO.DLPPolicyPattern> getPolicyPatterns(@RequestParam(required = false) Integer firstResult,
            @RequestParam(required = false) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_POLICY_PATTERNS);

            List<DLPDTO.DLPPolicyPattern> result = new LinkedList<>();

            List<PolicyPatternNode> patternNodes = policyPatternManager.getPatterns(firstResult, maxResults);

            for (PolicyPatternNode patternNode : patternNodes) {
                result.add(toDLPPolicyPattern(patternNode));
            }

            return result;
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_POLICY_PATTERNS_COUNT_PATH)
    public long getPolicyPatternCount()
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_POLICY_PATTERNS_COUNT);

            return policyPatternManager.getNumberOfPatterns();
        })).orElse(0L);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.CONFLICT_RESPONSE_CODE,
                    description = "Policy pattern with name name already exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_RENAME_POLICY_PATTERN_PATH)
    public void renamePolicyPattern(@RequestParam String oldName, @RequestParam String newName)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.RENAME_POLICY_PATTERN);

            PolicyPatternNode patternNode = getPolicyPatternNode(oldName);

            // check if there is already a node with the new name
            if (policyPatternManager.getPattern(newName) != null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.CONFLICT,
                        String.format("Policy pattern with name %s already exist", newName), logger);
            }

            patternNode.setName(newName);

            // also rename the pattern not just the node. But only if the pattern is set
            PolicyPattern pattern = patternNode.getPolicyPattern();

            if (pattern != null) {
                patternNode.setPolicyPattern(PolicyPatternImpl.clone(pattern, newName));
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(path=RestPaths.DLP_IS_REFERENCED_PATH)
    public boolean isReferenced(@RequestParam String name)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_REFERENCED);

            return policyPatternManager.isInUse(getPolicyPatternNode(name).getName());
        }
        )).orElse(false);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(path=RestPaths.DLP_GET_REFERENCED_DETAILS_PATH)
    public List<String> getReferencedDetails(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_REFERENCED_DETAILS);

            return policyPatternManager.getInUseInfo(getPolicyPatternNode(name).getName());
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_SET_CHILD_NODES_PATH)
    public void setChildNodes(@RequestParam String parentNode, @RequestBody List<String> childNodes)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_CHILD_NODES);

            // do not allow the node to add itself as a child
            for (String childNode : childNodes)
            {
                if (StringUtils.equalsIgnoreCase(StringUtils.trimToNull(parentNode), StringUtils.trimToNull(childNode)))
                {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            "The child node cannot be the same as the parent node", logger);
                }
            }

            Set<PolicyPatternNode> children = getPolicyPatternNode(parentNode).getChilds();
            children.clear();
            children.addAll(childNodes.stream().map(this::getPolicyPatternNode).toList());
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_ADD_CHILD_NODE_PATH)
    public boolean addChildNode(@RequestParam String parentNode, @RequestParam String childNode)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.ADD_CHILD_NODE);

            // do not allow the node to add itself as a child
            if (StringUtils.equalsIgnoreCase(StringUtils.trimToNull(parentNode), StringUtils.trimToNull(childNode)))
            {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "The child node cannot be the same as the parent node", logger);
            }

            return getPolicyPatternNode(parentNode).getChilds().add(getPolicyPatternNode(childNode));
        }
        )).orElse(false);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Policy pattern with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_REMOVE_CHILD_NODE_PATH)
    public boolean removeChildNode(@RequestParam String parentNode, @RequestParam String childNode)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.REMOVE_CHILD_NODE);

            return getPolicyPatternNode(parentNode).getChilds().remove(getPolicyPatternNode(childNode));
        }
        )).orElse(false);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Input is not a valid MIME message",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.DLP_EXTRACT_TEXT_FROM_EMAIL_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public String extractTextFromMimeMessage(@RequestBody MultipartFile mime)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXTRACT_TEXT_FROM_EMAIL);

            MimeMessage message;

            try(InputStream mimeInputStream = mime.getInputStream()) {
                message = MailUtils.loadMessage(mimeInputStream);
            }
            catch (MessagingException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Input is not a valid MIME message", logger);
            }

            try {
                List<ExtractedPart> parts = null;

                ByteArrayOutputStream extractedText = new ByteArrayOutputStream();

                try {
                    parts = textExtractor.extractText(message);

                    PrintWriter outputWriter = new PrintWriter(extractedText);

                    for (ExtractedPart part : parts)
                    {
                        StringIterator si = new StringIterator(part.getContent(), SizeUtils.KB * 512,
                                CharacterEncoding.UTF_8);

                        String text;

                        while ((text = si.getNext()) != null) {
                            textNormalizer.normalize(new StringReader(text), outputWriter);
                        }
                    }

                    outputWriter.flush();
                }
                finally {
                    TextExtractorUtils.closeQuitely(parts);
                }

                return extractedText.toString(StandardCharsets.UTF_8);
            }
            catch (IOException | MessagingException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_MATCH_FILTERS_PATH)
    public List<DLPDTO.DLPMatchFilter> getMatchFilters()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_MATCH_FILTERS);

            return matchFilterRegistry.getMatchFilters().stream().map(this::toDLPMatchFilter).toList();
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Match filter with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(path=RestPaths.DLP_GET_MATCH_FILTER_PATH)
    public DLPDTO.DLPMatchFilter getMatchFilter(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_MATCH_FILTER);

            DLPDTO.DLPMatchFilter filter = Optional.ofNullable(matchFilterRegistry.getMatchFilter(name))
                    .map(this::toDLPMatchFilter).orElse(null);

            if (filter == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Match filter with name %s does not exist", name), logger);
            }

            return filter;
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_VALIDATORS_PATH)
    public List<DLPDTO.DLPValidator> getValidators()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_VALIDATORS);

            return validatorRegistry.getValidators().stream().map(this::toDLPValidator).toList();
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Validator with name does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @GetMapping(path=RestPaths.DLP_GET_VALIDATOR_PATH)
    public DLPDTO.DLPValidator getValidator(@RequestParam String name)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_VALIDATOR);

            DLPDTO.DLPValidator validator = Optional.ofNullable(validatorRegistry.getValidator(name))
                    .map(this::toDLPValidator).orElse(null);

            if (validator == null) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Validator with name %s does not exist", name), logger);
            }

            return validator;
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_SKIP_LIST_PATH)
    public String getSkipList()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SKIP_LIST);

            return wordSkipper.getSkipList();
        });
    }

    @PostMapping(path=RestPaths.DLP_SET_SKIP_LIST_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public void setSkipList(@RequestBody MultipartFile skipList)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_SKIP_LIST);

            try(InputStream skipListInputStream = skipList.getInputStream()) {
                wordSkipper.setSkipList(IOUtils.toString(skipListInputStream, StandardCharsets.UTF_8));
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Error reading skip list", logger);
            }
        });
    }

    @PostMapping(path=RestPaths.DLP_SET_USER_POLICY_PATTERNS_PATH)
    public void setUserPolicyPatterns(@RequestParam String email, @RequestBody List<String> patternNames)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_USER_POLICY_PATTERNS);

            try {
                 userPreferencesPolicyPatternManager.setPatterns(getUserPreferences(email,
                                 UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST),
                         patternNames.stream().map(this::getPolicyPatternNode).toList());
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Error setting user policy patterns", logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_USER_POLICY_PATTERNS_PATH)
    public List<DLPDTO.DLPPolicyPattern> getUserPolicyPatterns(@RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_USER_POLICY_PATTERNS);

            try {
                return userPreferencesPolicyPatternManager.getPatterns(getUserPreferences(email,
                                UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)).stream()
                                    .map(this::toDLPPolicyPattern).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Error getting user policy patterns", logger);
            }
        });
    }

    @PostMapping(path=RestPaths.DLP_SET_DOMAIN_POLICY_PATTERNS_PATH)
    public void setDomainPolicyPatterns(@RequestParam String domain, @RequestBody List<String> patternNames)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_DOMAIN_POLICY_PATTERNS);

            userPreferencesPolicyPatternManager.setPatterns(getDomainPreferences(domain),
                    patternNames.stream().map(this::getPolicyPatternNode).toList());
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_DOMAIN_POLICY_PATTERNS_PATH)
    public List<DLPDTO.DLPPolicyPattern> getDomainPolicyPatterns(@RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_DOMAIN_POLICY_PATTERNS);

            return userPreferencesPolicyPatternManager.getPatterns(getDomainPreferences(domain)).stream()
                    .map(this::toDLPPolicyPattern).toList();
        });
    }

    @PostMapping(path=RestPaths.DLP_SET_GLOBAL_POLICY_PATTERNS_PATH)
    public void setGlobalPolicyPatterns(@RequestBody List<String> patternNames)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_GLOBAL_POLICY_PATTERNS);

            try {
                userPreferencesPolicyPatternManager.setPatterns(globalPreferencesManager.getGlobalUserPreferences(),
                        patternNames.stream().map(this::getPolicyPatternNode).toList());
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Error getting global policy patterns", logger);
            }
        });
    }

    @GetMapping(path=RestPaths.DLP_GET_GLOBAL_POLICY_PATTERNS_PATH)
    public List<DLPDTO.DLPPolicyPattern> getGlobalPolicyPatterns()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_GLOBAL_POLICY_PATTERNS);

            try {
                return userPreferencesPolicyPatternManager.getPatterns(
                        globalPreferencesManager.getGlobalUserPreferences()).stream()
                                .map(this::toDLPPolicyPattern).toList();
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Error getting global policy patterns", logger);
            }
        });
    }

    private @Nonnull DLPDTO.DLPMatchFilter toDLPMatchFilter(@Nonnull MatchFilter matchFilter) {
        return new DLPDTO.DLPMatchFilter(matchFilter.getName(), matchFilter.getDescription());
    }

    private @Nonnull DLPDTO.DLPValidator toDLPValidator(@Nonnull Validator validator) {
        return new DLPDTO.DLPValidator(validator.getName(), StringUtils.defaultString(validator.getDescription()));
    }

    private DLPDTO.DLPPolicyPattern toDLPPolicyPattern(PolicyPatternNode patternNode)
    {
        if (patternNode == null) {
            return null;
        }

        PolicyPattern policyPattern = patternNode.getPolicyPattern();

        return policyPattern != null ?
                new DLPDTO.DLPPolicyPattern(
                    StringUtils.defaultString(patternNode.getName()),
                    StringUtils.defaultString(policyPattern.getDescription()),
                    StringUtils.defaultString(policyPattern.getNotes()),
                    StringUtils.defaultString(policyPattern.getPattern().pattern()),
                    Optional.ofNullable(policyPattern.getValidator()).map(Validator::getName).orElse(""),
                    policyPattern.getThreshold(),
                    Optional.ofNullable(policyPattern.getAction()).map(p ->
                            DLPDTO.PolicyViolationAction.valueOf(p.name())).orElse(null),
                    policyPattern.isDelayEvaluation(),
                    Optional.ofNullable(policyPattern.getMatchFilter()).map(MatchFilter::getName).orElse(""),
                    patternNode.getChilds().stream().map(PolicyPatternNode::getName).toList(),
                    false,
                    patternNode instanceof UserPolicyPatternNode userPolicyPatternNode && userPolicyPatternNode.isInherited(),
                    policyPatternManager.isInUse(patternNode.getName()))
                :
                new DLPDTO.DLPPolicyPattern(
                        patternNode.getName(),
                        "",
                        "",
                        "",
                        "",
                        0,
                        null,
                        false,
                        "",
                        patternNode.getChilds().stream().map(PolicyPatternNode::getName).toList(),
                        true,
                        patternNode instanceof UserPolicyPatternNode userPolicyPatternNode && userPolicyPatternNode.isInherited(),
                        policyPatternManager.isInUse(patternNode.getName()));
    }

    private PolicyPatternNode getPolicyPatternNode(@Nonnull String name)
    {
        PolicyPatternNode patternNode = policyPatternManager.getPattern(name);

        if (patternNode == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Policy pattern with name %s does not exist", name), logger);
        }

        return patternNode;
    }

    private UserPreferences getUserPreferences(
            @Nonnull String email,
            @Nonnull UserWorkflow.UserNotExistResult userNotExistResult)
    throws HierarchicalPropertiesException, AddressException
    {
        User user = userWorkflow.getUser(restValidator.validateEmail(email, logger),
                userNotExistResult);

        if (user == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with email address %s not found", email), logger);
        }

        return user.getUserPreferences();
    }

    private UserPreferences getDomainPreferences(@Nonnull String domain)
    {
        UserPreferences userPreferences = domainManager.getDomainPreferences(restValidator.validateDomain(domain,
                DomainUtils.DomainType.FULLY_QUALIFIED, logger));

        if (userPreferences == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Domain with name  %s not found", domain), logger);
        }

        return userPreferences;
    }
}
