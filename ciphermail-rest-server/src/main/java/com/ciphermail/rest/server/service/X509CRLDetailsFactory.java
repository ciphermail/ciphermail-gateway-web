/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.security.AlgorithmUtils;
import com.ciphermail.core.common.security.crl.X509CRLInspector;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.DateTimeUtils;
import com.ciphermail.core.common.util.Functional;
import com.ciphermail.rest.core.CRLDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.security.cert.X509CRL;

@Component
public class X509CRLDetailsFactory
{
    private static final Logger logger = LoggerFactory.getLogger(X509CRLDetailsFactory.class);

    /*
     * String return result when a parameter value results in an error
     */
    static final String ERROR = "<error>";

    /**
     * Creates a X509CRLDetails from an X509CRL.
     */
    public CRLDTO.X509CRLDetails createX509CRLDetails(@Nonnull X509CRL crl)
    {
        X509CRLInspector inspector = new X509CRLInspector(crl);

        return new CRLDTO.X509CRLDetails(
                Functional.catchAllGet(inspector::getIssuerFriendly, ERROR, logger),
                Functional.catchAllGet(() -> DateTimeUtils.getTimestamp(crl.getNextUpdate()), null, logger),
                Functional.catchAllGet(() -> DateTimeUtils.getTimestamp(crl.getThisUpdate()), null, logger),
                Functional.catchAllGet(crl::getVersion, null, logger),
                Functional.catchAllGet(() -> BigIntegerUtils.hexEncode(inspector.getCRLNumber()), ERROR, logger),
                Functional.catchAllGet(() -> BigIntegerUtils.hexEncode(inspector.getDeltaIndicator()), ERROR, logger),
                Functional.catchAllGet(inspector::isDeltaCRL, null, logger),
                Functional.catchAllGet(inspector::getThumbprint, ERROR, logger),
                Functional.catchAllGet(() -> inspector.getThumbprint(Digest.SHA1), ERROR, logger),
                Functional.catchAllGet(() -> AlgorithmUtils.getAlgorithmName(crl.getSigAlgOID()), ERROR, logger));
    }
}
