/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.logging.JournalCtl;
import com.ciphermail.rest.core.LogDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Tag(name = "Log")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class LogController
{
    private static final Logger logger = LoggerFactory.getLogger(LogController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private JournalCtl journalCtl;

    /*
     * Comma seperated units for MPA logging
     */
    @Value("${ciphermail.rest.controller.log.mpa-units:}")
    private String mpaUnits;

    /*
     * Comma seperated identifiers for MPA logging
     */
    @Value("${ciphermail.rest.controller.log.mpa-identifiers:ciphermail-backend}")
    private String mpaIdentifiers;

    /*
     * Comma seperated units for postfix logging
     */
    @Value("${ciphermail.rest.controller.log.postfix-units:postfix*}")
    private String postfixUnits;

    /*
     * Comma seperated identifiers for postfix logging
     */
    @Value("${ciphermail.rest.controller.log.postfix-identifiers:}")
    private String postfixIdentifiers;

    /*
     * Comma seperated units for dovecot logging
     */
    @Value("${ciphermail.rest.controller.log.dovecot-units:dovecot}")
    private String dovecotUnits;

    /*
     * Comma seperated identifiers for dovecot logging
     */
    @Value("${ciphermail.rest.controller.log.dovecot-identifiers:}")
    private String dovecotIdentifiers;

    /*
     * Number of lines journalctl will max return
     */
    @Value("${ciphermail.rest.controller.log.lines:100000}")
    private int lines;

    private enum Action
    {
        READ_LOG_COUNT,
        READ_LOG;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.LOG,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.LOG, action.toPermissionName());
    }

    public LogController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    private List<String> getJournalUnits(@Nonnull LogDTO.LogTarget logUnit)
    {
        return switch (logUnit) {
            case MPA -> List.of(StringUtils.split(mpaUnits, ","));
            case MTA -> List.of(StringUtils.split(postfixUnits, ","));
            case IMAP -> List.of(StringUtils.split(dovecotUnits, ","));
        };
    }

    private List<String> getJournalIdentifiers(@Nonnull LogDTO.LogTarget logUnit)
    {
        return switch (logUnit) {
            case MPA -> List.of(StringUtils.split(mpaIdentifiers, ","));
            case MTA -> List.of(StringUtils.split(postfixIdentifiers, ","));
            case IMAP -> List.of(StringUtils.split(dovecotIdentifiers, ","));
        };
    }

    @GetMapping(path= RestPaths.LOG_GET_LOG_LINES_COUNT_PATH)
    public long getLogLinesCount(
            @RequestParam @Nonnull LogDTO.LogTarget logTarget,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String since,
            @RequestParam(required = false) String until)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.READ_LOG_COUNT));

        try {
            return journalCtl.getLog(
                    getJournalUnits(logTarget),
                    getJournalIdentifiers(logTarget),
                    false,
                    StringUtils.trimToEmpty(filter),
                    lines,
                    StringUtils.trimToNull(since),
                    StringUtils.trimToNull(until))
                   .lines().count();
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error getting logs. Log target: %s, Reverse: %s, Filter: %s, Lines: %d, " +
                                  "Since: %s, Until: %s, Message: %s",
                            logTarget, false, filter, lines, since, until, e.getMessage()), logger);
        }
    }

    @GetMapping(path= RestPaths.LOG_GET_LOG_LINES_PATH)
    public String getLogLines(
            @RequestParam @Nonnull LogDTO.LogTarget logTarget,
            @RequestParam(required = false, defaultValue = "false") boolean reverse,
            @RequestParam(required = false) Integer firstResult,
            @RequestParam(required = false) Integer maxResults,
            @RequestParam(required = false) String filter,
            @RequestParam(required = false) String since,
            @RequestParam(required = false) String until)
    {
        transactionOperations.executeWithoutResult(tx -> checkPermission(Action.READ_LOG));

        try {
            return "[\n" +
                   journalCtl.getLog(
                                   getJournalUnits(logTarget),
                                   getJournalIdentifiers(logTarget),
                                   reverse,
                                   StringUtils.trimToEmpty(filter),
                                   lines,
                                   StringUtils.trimToNull(since),
                                   StringUtils.trimToNull(until))
                           .lines()
                           .skip(Optional.ofNullable(firstResult).orElse(0))
                           .limit(Optional.ofNullable(maxResults).orElse(lines)).collect(Collectors.joining(",")) +
                   "\n]";
        }
        catch (IOException e) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("Error getting logs. Log target: %s, Reverse: %s, Filter: %s, Lines: %d, " +
                                  "Since: %s, Until: %s, Message: %s",
                            logTarget, reverse, filter, lines, since, until, e.getMessage()), logger);
        }
    }
}
