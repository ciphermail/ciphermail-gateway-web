/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestor;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestorResult;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntryRevoker;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingExporter;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPPrivateKeySelector;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeySelector;
import com.ciphermail.core.common.security.openpgp.PGPUserIDRevocationChecker;
import com.ciphermail.core.common.security.openpgp.PGPUserIDRevokedException;
import com.ciphermail.core.common.security.openpgp.PGPUserIDValidator;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.PGPKeyDetailsFactory;
import com.ciphermail.rest.server.service.PGPKeyServerSubmitDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "PGP")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PGPController
{
    private static final Logger logger = LoggerFactory.getLogger(PGPController.class);

    /*
     * The Keyring which stores the PGP keys
     */
    @Autowired
    private PGPKeyRing keyRing;

    /*
     * For importing public and secret keyrings
     */
    @Autowired
    private PGPKeyRingImporter keyRingImporter;

    /*
     * For exporting public and secret keyrings
     */
    @Autowired
    private PGPKeyRingExporter keyRingExporter;

    /*
     * Returns the encryption keys for an email address
     */
    @Autowired
    private PGPPublicKeySelector publicKeySelector;

    /*
     * Returns the signing keys for an email address
     */
    @Autowired
    private PGPPrivateKeySelector privateKeySelector;

    /*
     * For generating a secret key
     */
    @Autowired
    private PGPSecretKeyRequestor secretKeyRequestor;

    /*
     * For revoking a key from the key ring
     */
    @Autowired
    private PGPKeyRingEntryRevoker keyRevoker;

    /*
     * For checking whether the User ID is valid
     */
    @Autowired
    private PGPUserIDValidator userIDValidator;

    /*
     * For checking whether the User ID is revoked
     */
    @Autowired
    private PGPUserIDRevocationChecker userIDRevocationChecker;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyDetailsFactory keyDetailsFactory;

    @Autowired
    private UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelector;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private PGPKeyServerSubmitDetailsFactory pgpKeyServerSubmitDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    private enum Action
    {
        IMPORT_KEYRING,
        EXPORT_PUBLIC_KEYS,
        EXPORT_SECRET_KEYS,
        SET_PGP_KEY_EMAIL_AND_DOMAINS,
        GET_ENCRYPTION_KEYS_FOR_EMAIL,
        GET_ENCRYPTION_KEYS_FOR_DOMAIN,
        AVAILABLE_SIGNING_KEYS_FOR_EMAIL,
        GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN,
        GET_SIGNING_KEY_FOR_USER,
        GET_SIGNING_KEY_FOR_DOMAIN,
        SET_SIGNING_KEY_FOR_USER,
        SET_SIGNING_KEY_FOR_DOMAIN,
        GENERATE_SECRET_KEY_RING,
        REVOKE_KEY,
        REVOKE_KEYS,
        IS_USER_ID_VALID,
        IS_USER_ID_REVOKED;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.PGP,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.PGP, action.toPermissionName());
    }

    public PGPController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Keyring is invalid or the password is incorrect",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path= RestPaths.PGP_IMPORT_KEYRING_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public int importKeyring(
            @RequestPart(required = false) String password,
            @RequestParam(defaultValue = "false") boolean ignoreParsingErrors,
            @RequestBody MultipartFile keyringFile)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IMPORT_KEYRING);

            try(InputStream keyringFileInputStream = keyringFile.getInputStream())
            {
                return CollectionUtils.getSize(keyRingImporter.importKeyRing(keyringFileInputStream,
                        new StaticPasswordProvider(password), ignoreParsingErrors));
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Keyring is invalid or the password is incorrect", logger);
            }
        })).orElse(0);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Missing id's",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Key with the given id is not a master key",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
    public byte[] exportPublicKeys(@RequestBody List<UUID> ids)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXPORT_PUBLIC_KEYS);

            if (ids.isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "id's are missing", logger);
            }

            try {
                List<PGPKeyRingEntry> keyRingEntries = new LinkedList<>();

                for (UUID id : ids) {
                    // only master keys can be exported
                    keyRingEntries.add(restValidator.validateIsMasterKey(getPGPKeyRingEntryById(id), logger));
                }

                ByteArrayOutputStream encodedRing = new ByteArrayOutputStream();

                if (keyRingExporter.exportPublicKeys(keyRingEntries, encodedRing)) {
                    return encodedRing.toByteArray();
                }
                else {
                    // should not happen
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                            "One or more keys could not be exported", logger);
                }
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Missing id's",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Key with the given id is not a master key",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
    public byte[] exportSecretKeys(@RequestBody PGPDTO.ExportSecretKeysRequestBody body)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.EXPORT_SECRET_KEYS);

            if (body.ids().isEmpty()) {
                throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                        "id's are missing", logger);
            }

            try {
                List<PGPKeyRingEntry> keyRingEntries = new LinkedList<>();

                for (UUID id : body.ids()) {
                    // only master keys can be exported
                    keyRingEntries.add(restValidator.validateIsMasterKey(getPGPKeyRingEntryById(id), logger));
                }

                ByteArrayOutputStream encodedRing = new ByteArrayOutputStream();

                if (keyRingExporter.exportSecretKeys(keyRingEntries, encodedRing,
                        new StaticPasswordProvider(body.password())))
                {
                    return encodedRing.toByteArray();
                }
                else {
                    // should not happen
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                            "One or more keys could not be exported", logger);
                }
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Key with the given id is not a master key",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
    public void setPGPKeyEmailAndDomains(
            @RequestParam UUID id,
            @RequestBody List<String> emailAndDomains)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_PGP_KEY_EMAIL_AND_DOMAINS);

            try {
                for (String emailOrDomain : emailAndDomains)
                {
                    if (!EmailAddressUtils.isValid(emailOrDomain) && !DomainUtils.isValid(emailOrDomain, DomainUtils.DomainType.FULLY_QUALIFIED)) {
                        // should not happen
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                "%s is not a valid email address or domain".formatted(emailOrDomain), logger);
                    }
                }

                restValidator.validateIsMasterKey(getPGPKeyRingEntryById(id), logger).setEmail(new HashSet<>(emailAndDomains));
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
    public List<PGPDTO.PGPKeyDetails> getEncryptionKeysForEmail(@RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ENCRYPTION_KEYS_FOR_EMAIL);

            try {
                return Optional.ofNullable(publicKeySelector.select(restValidator.validateEmail(email, logger)))
                        .orElse(Collections.emptySet())
                        .stream().map(keyDetailsFactory::createPGPKeyDetails).toList();
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
    public List<PGPDTO.PGPKeyDetails> getEncryptionKeysForDomain(@RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ENCRYPTION_KEYS_FOR_DOMAIN);

            try {
                return Optional.ofNullable(publicKeySelector.select(restValidator.validateDomain(domain,
                                DomainUtils.DomainType.WILD_CARD, logger)))
                        .orElse(Collections.emptySet())
                        .stream().map(keyDetailsFactory::createPGPKeyDetails).toList();
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
    public List<PGPDTO.PGPKeyDetails> getAvailableSigningKeysForEmail(@RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.AVAILABLE_SIGNING_KEYS_FOR_EMAIL);

            try {
                return Optional.ofNullable(privateKeySelector.select(restValidator.validateEmail(email, logger)))
                        .orElse(Collections.emptySet())
                        .stream().map(keyDetailsFactory::createPGPKeyDetails).toList();
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
    public List<PGPDTO.PGPKeyDetails> getAvailableSigningKeysForDomain(@RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN);

            try {
                return Optional.ofNullable(privateKeySelector.select(restValidator.validateDomain(domain,
                                DomainUtils.DomainType.FULLY_QUALIFIED, logger)))
                        .orElse(Collections.emptySet())
                        .stream().map(keyDetailsFactory::createPGPKeyDetails).toList();
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
    public PGPDTO.PGPKeyDetails getSigningKeyForUser(@RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SIGNING_KEY_FOR_USER);

            try {
                PGPKeyRingEntry keyRingEntry = userPreferencesPGPSigningKeySelector.select(getUserPreferences(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST));

                return keyRingEntry != null ? keyDetailsFactory.createPGPKeyDetails(keyRingEntry) : null;
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
    public PGPDTO.PGPKeyDetails getSigningKeyForDomain(@RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SIGNING_KEY_FOR_DOMAIN);

            try {
                PGPKeyRingEntry keyRingEntry = userPreferencesPGPSigningKeySelector.select(getDomainPreferences(domain));

                return keyRingEntry != null ? keyDetailsFactory.createPGPKeyDetails(keyRingEntry) : null;
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path=RestPaths.PGP_SET_SIGNING_KEY_FOR_USER_PATH)
    public void setSigningKeyForUser(@RequestParam String email, @RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_SIGNING_KEY_FOR_USER);

            try {
                PGPProperties pgpProperties = userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                        .createInstance(getUserPreferences(email,
                                UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getProperties());

                pgpProperties.setPGPSigningKey(getPGPKeyRingEntryById(id).getSHA256Fingerprint());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path=RestPaths.PGP_SET_SIGNING_KEY_FOR_DOMAIN_PATH)
    public void setSigningKeyForDomain(@RequestParam String domain, @RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.SET_SIGNING_KEY_FOR_DOMAIN);

            try {
                PGPProperties pgpProperties = userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                        .createInstance(getDomainPreferences(domain).getProperties());

                pgpProperties.setPGPSigningKey(getPGPKeyRingEntryById(id).getSHA256Fingerprint());
            }
            catch (IOException | PGPException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path=RestPaths.PGP_GENERATE_SECRET_KEY_RING_PATH)
    public PGPDTO.GenerateSecretKeyRingDetails generateSecretKeyRing(
            @RequestParam String email,
            @RequestParam String name,
            @RequestParam(defaultValue = "RSA_3072") PGPKeyGenerationType keyType,
            @RequestParam(defaultValue = "false") boolean publishKeys)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GENERATE_SECRET_KEY_RING);

            try {
                PGPSecretKeyRequestorResult requestorResult = secretKeyRequestor.requestSecretKey(
                        restValidator.validateEmail(email, logger), name, keyType, publishKeys);

                return requestorResult != null ? new PGPDTO.GenerateSecretKeyRingDetails(
                        keyDetailsFactory.createPGPKeyDetails(requestorResult.getMasterKeyEntry()),
                        pgpKeyServerSubmitDetailsFactory.createKeyServerSubmitDetails(
                                requestorResult.getKeyServerClientSubmitResult())) : null;
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Revokes the key with the given id from the key store. If the entry is a master key, but the key has no secret
     * key, 404 will be returned. If the key is a sub-key, but the master key does not have a secret key, 404 will be
     * returned.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Master key does not have a private key",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Key does not have a master key",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.PGP_REVOKE_KEY_PATH)
    public void revokeKey(@RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.REVOKE_KEY);

            try {
                PGPKeyRingEntry entry = getPGPKeyRingEntryById(id);

                if (entry.isMasterKey())
                {
                    if (entry.getPrivateKey() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Master key with id %s does not have a private key", id), logger);
                    }
                }
                else {
                    PGPKeyRingEntry masterKey = entry.getParentKey();

                    if (masterKey == null) {
                        // should normally not happen because a sub key should always have a master key
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Key with id %s does not have a master key", id), logger);
                    }

                    if (masterKey.getPrivateKey() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Master key with id %s does not have a private key",
                                        masterKey.getID()), logger);
                    }
                }

                keyRevoker.revokeKeyRingEntry(entry.getID());
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    /**
     * Revokes the keys with the given ids from the key store. If the entry is a master key, but the key has no secret
     * key, the key wil be skipped. If the key is a sub-key, but the master key does not have a secret key, the key will
     * be skipped.
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE)
    })
    @PostMapping(path=RestPaths.PGP_REVOKE_KEYS_PATH)
    public void revokeKeys(@RequestBody List<UUID> ids)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.REVOKE_KEYS);

            try {
                for (UUID id : ids)
                {
                    final PGPKeyRingEntry entry = keyRing.getByID(id);

                    // skip if key with id does not exist
                    if (entry == null) {
                        continue;
                    }

                    if (entry.isMasterKey()) {
                        // skip if there is no private key
                        if (entry.getPrivateKey() == null) {
                            continue;
                        }
                    }
                    else {
                        // sub key, get the master key
                        PGPKeyRingEntry masterKey = entry.getParentKey();

                        if (masterKey == null) {
                            // should normally not happen because a sub key should always have a master key
                            continue;
                        }

                        if (masterKey.getPrivateKey() == null) {
                            // skip if the master key has no private key
                            continue;
                        }
                    }

                    keyRevoker.revokeKeyRingEntry(entry.getID());
                }
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(path=RestPaths.PGP_IS_USER_ID_VALID_PATH)
    public boolean isUserIDValid(
            @RequestParam UUID id,
            @RequestParam String userID)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_USER_ID_VALID);

            try {
                PGPKeyRingEntry entry = getPGPKeyRingEntryById(id);

                boolean valid = false;

                try {
                    valid = userIDValidator.validateUserID(PGPUtils.userIDToByteArray(userID), entry.getPublicKey());
                }
                catch (PGPUserIDRevokedException e) {
                    logger.warn("User ID {} is revoked. Message: {}", userID, e.getMessage());
                }

                return valid;
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Key with the given id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @GetMapping(path=RestPaths.PGP_IS_USER_ID_REVOKED_PATH)
    public boolean isUserIDRevoked(
            @RequestParam UUID id,
            @RequestParam String userID)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(tx ->
        {
            checkPermission(Action.IS_USER_ID_REVOKED);

            try {
                PGPKeyRingEntry entry = getPGPKeyRingEntryById(id);

                // revoked until proven otherwise
                boolean revoked = true;

                try {
                    revoked = userIDRevocationChecker.isRevoked(PGPUtils.userIDToByteArray(userID),
                            entry.getPublicKey());
                }
                catch (Exception e) {
                    logger.error("Error checking User ID revocation", e);
                }

                return revoked;
            }
            catch (IOException | PGPException  e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        }));
    }

    private @Nonnull PGPKeyRingEntry getPGPKeyRingEntryById(@Nonnull UUID id)
    throws PGPException, IOException
    {
        PGPKeyRingEntry keyRingEntry = keyRing.getByID(id);

        if (keyRingEntry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Key with id %s not found", id), logger);
        }

        return keyRingEntry;
    }

    private @Nonnull UserPreferences getUserPreferences(@Nonnull String email, @Nonnull UserWorkflow.UserNotExistResult mode)
    throws HierarchicalPropertiesException, AddressException
    {
        String validatedEmail = restValidator.validateEmail(email, logger);

        User user = userWorkflow.getUser(validatedEmail, mode);

        if (user == null)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with email address %s not found", validatedEmail), logger);
        }

        return user.getUserPreferences();
    }

    private @Nonnull UserPreferences getDomainPreferences(@Nonnull String domain)
    {
        String validatedDomain = restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger);

        UserPreferences domainPreferences = domainManager.getDomainPreferences(validatedDomain);

        if (domainPreferences == null)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Domain %s not found", validatedDomain), logger);
        }

        return domainPreferences;
    }
}
