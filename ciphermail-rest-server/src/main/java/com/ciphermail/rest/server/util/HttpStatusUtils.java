/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.util;

/**
 * Http status codes for the REST API
 */
public class HttpStatusUtils
{
    private HttpStatusUtils() {
        // empty on purpose
    }

    // we need static string values because we need to use them with @ApiResponse annotations
    // Note: the default exception handler only supports status codes which are supported by HttpStatus
    // if an unsupported code is used, spring boot will throw an IllegalArgumentException which will
    // then result in a

    public static final String OK_RESPONSE_CODE = "200";
    public static final String BAD_REQUEST_RESPONSE_CODE = "400";
    public static final String NOT_FOUND_RESPONSE_CODE = "404";
    public static final String METHOD_NOT_ALLOWED_RESPONSE_CODE = "405";
    public static final String CONFLICT_RESPONSE_CODE = "409";

    public static final String INVALID_EMAIL_ADDRESS_DESCRIPTION = "Email address is invalid";
    public static final String INVALID_DOMAIN_DESCRIPTION = "Domain is invalid";
}
