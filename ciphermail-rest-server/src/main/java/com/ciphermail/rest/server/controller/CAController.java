/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.CA;
import com.ciphermail.core.common.security.ca.CABuilder;
import com.ciphermail.core.common.security.ca.CABuilderParameters;
import com.ciphermail.core.common.security.ca.CABuilderParametersImpl;
import com.ciphermail.core.common.security.ca.CABuilderResult;
import com.ciphermail.core.common.security.ca.CACertificateSignatureAlgorithm;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.ca.EncodedRequestProvider;
import com.ciphermail.core.common.security.ca.RequestParameters;
import com.ciphermail.core.common.security.ca.RequestParametersImpl;
import com.ciphermail.core.common.security.ca.SelfSignedSMIMEKeyAndCertificateIssuer;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.KeyUsageType;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X500PrincipalInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.crl.CRLStoreMaintainer;
import com.ciphermail.core.common.security.crl.X509CRLBuilder;
import com.ciphermail.core.common.security.crl.X509CRLBuilderHelper;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.CADTO;
import com.ciphermail.rest.core.CAKeyUsageFilter;
import com.ciphermail.rest.core.CRLDTO;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestException;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CRLDetailsFactory;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@RestController
@Tag(name = "CA")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CAController
{
    private static final Logger logger = LoggerFactory.getLogger(CAController.class);

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private CABuilder caBuilder;

    @Autowired
    private CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    @Autowired
    @Qualifier("keyAndCertStore")
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier("keyAndRootCertStore")
    private KeyAndCertStore keyAndRootStore;

    @Autowired
    private CA ca;

    @Autowired
    private CertificateRequestStore certificateRequestStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private CRLStoreMaintainer crlStoreMaintainer;

    @Autowired
    private SelfSignedSMIMEKeyAndCertificateIssuer selfSignedKeyAndCertificateIssuer;

    @Autowired
    private CTLManager ctlManager;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private X509CertificateDetailsFactory x509CertificateDetailsFactory;

    @Autowired
    private X509CRLDetailsFactory x509CRLDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        CREATE_CA,
        GET_AVAILABLE_CAS,
        GET_CERTIFICATE_REQUEST_HANDLERS,
        REQUEST_NEW_CERTIFICATE,
        GET_PENDING_REQUESTS,
        GET_PENDING_REQUESTS_COUNT,
        GET_PENDING_REQUESTS_BY_EMAIL,
        GET_PENDING_REQUESTS_BY_EMAIL_COUNT,
        GET_PENDING_REQUEST,
        DELETE_PENDING_REQUEST,
        DELETE_PENDING_REQUESTS,
        RESCHEDULE_REQUEST,
        RESCHEDULE_REQUESTS,
        FINALIZE_PENDING_REQUEST,
        CREATE_SELF_SIGNED_CERTIFICATE,
        CREATE_CRL;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values()) {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CA, action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull Action action) {
        permissionChecker.checkPermission(PermissionCategory.CA, action.toPermissionName());
    }

    public CAController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @PostMapping(path= RestPaths.CA_CREATE_CA_PATH)
    public CADTO.NewCAResponse createCA(
            @RequestBody CADTO.NewCASubjects subjects,
            @RequestParam @Schema(minimum = "1") @Min(value = 1) int rootDaysValid,
            @RequestParam @Schema(minimum = "1") @Min(value = 1) int intermediateDaysValid,
            @RequestParam @Schema(allowableValues = {"2048", "3072", "4096"}, defaultValue = "3072")
                    @Min(value = 2048) @Max(value = 4096) int rootKeyLength,
            @RequestParam @Schema(allowableValues = {"2048", "3072", "4096"}, defaultValue = "3072")
                    @Min(value = 2048) @Max(value = 4096) int intermediateKeyLength,
            @RequestParam CACertificateSignatureAlgorithm signatureAlgorithm,
            @RequestParam(required = false) String crlDistributionPoint)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.CREATE_CA);

            try {
                CABuilderParameters builderParameters = new CABuilderParametersImpl();

                builderParameters.setRootSubject(toX500Principal(subjects.rootSubject()));
                builderParameters.setIntermediateSubject(toX500Principal(subjects.intermediateSubject()));
                builderParameters.setRootKeyLength(rootKeyLength);
                builderParameters.setIntermediateKeyLength(intermediateKeyLength);
                builderParameters.setRootValidity(rootDaysValid);
                builderParameters.setIntermediateValidity(intermediateDaysValid);
                builderParameters.setSignatureAlgorithm(signatureAlgorithm.getAlgorithmName());
                builderParameters.setCRLDistributionPoints(Optional.ofNullable(
                        StringUtils.trimToNull(crlDistributionPoint)).stream().toList());

                CABuilderResult result = caBuilder.buildCA(builderParameters);

                keyAndRootStore.addKeyAndCertificate(result.getRoot());
                keyAndCertStore.addKeyAndCertificate(result.getIntermediate());

                return new CADTO.NewCAResponse(
                        x509CertificateDetailsFactory.createX509CertificateDetails(
                                result.getIntermediate().getCertificate()),
                        x509CertificateDetailsFactory.createX509CertificateDetails(result.getRoot().getCertificate()));
            }
            catch (IOException | CAException | CertStoreException | KeyStoreException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.CA_GET_AVAILABLE_CAS_PATH)
    public List<CertificateDTO.X509CertificateDetails> getAvailableCAs(CAKeyUsageFilter keyUsageFilter)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_AVAILABLE_CAS);

            try {
                List<CertificateDTO.X509CertificateDetails> result = new LinkedList<>();

                X509CertSelector selector = new X509CertSelector();

                // only select certificates which can issue other certificates
                selector.setBasicConstraints(0);

                Set<KeyUsageType> keyUsage = new HashSet<>();

                keyUsage.add(KeyUsageType.KEYCERTSIGN);

                if (keyUsageFilter == CAKeyUsageFilter.CRL_SIGN) {
                    keyUsage.add(KeyUsageType.CRLSIGN);
                }

                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(keyUsage));

                // maxResults cannot be used because we need to step through all certificates to find a certificate that
                // matches the selector
                // Note: this can be a slow process if there is large number of certificates
                try (CloseableIterator<? extends X509CertStoreEntry> iterator = keyAndCertStore.getCertStoreIterator(
                        selector, MissingKeyAlias.NOT_ALLOWED, null, null))
                {
                    while (iterator.hasNext())
                    {
                        result.add(x509CertificateDetailsFactory.createX509CertificateDetails(
                                iterator.next().getCertificate()));

                        if (result.size() > restValidator.getMaxResultsUpperLimit())
                        {
                            // should never happen in practise since we should not have that many CAs
                            logger.warn("max results exceeded");
                            break;
                        }
                    }
                }

                return result;
            }
            catch (CertStoreException | CloseableIteratorException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.CA_GET_CERTIFICATE_REQUEST_HANDLERS_PATH)
    public List<CADTO.CertificateRequestHandlerDetails> getCertificateRequestHandlers()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_CERTIFICATE_REQUEST_HANDLERS);

            return certificateRequestHandlerRegistry.getHandlers().stream().map(
                        CAController::toCertificateRequestHandlerDetails).toList();
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Certificate request handler does not exist",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Email address is not valid, Certificate could not be requested",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CA_REQUEST_NEW_CERTIFICATE_PATH)
    public CADTO.NewRequest requestNewCertificate(
            @RequestParam String email,
            @RequestBody CADTO.X500PrincipalRequest subject,
            @RequestParam @Schema(minimum = "1") @Min(value = 1) int daysValid,
            @RequestParam @Schema(allowableValues = {"2048", "3072", "4096"}, defaultValue = "3072")
                    @Min(value = 2048) @Max(value = 4096) int keyLength,
            @RequestParam CACertificateSignatureAlgorithm signatureAlgorithm,
            @RequestParam(required = false) String crlDistributionPoint,
            @RequestParam String certificateRequestHandler)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.REQUEST_NEW_CERTIFICATE);

            try {
                CertificateRequestHandler handler = certificateRequestHandlerRegistry.getHandler(
                        certificateRequestHandler);

                if (handler == null) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                            String.format("Certificate request handler %s does not exist",
                                    certificateRequestHandler), logger);
                }

                if (!handler.isEnabled()) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("Certificate request handler %s is not enabled",
                                    certificateRequestHandler), logger);
                }

                RequestParametersImpl requestParameters = new RequestParametersImpl();

                requestParameters.setEmail(restValidator.validateEmail(email, logger));
                requestParameters.setSubject(toX500Principal(subject));
                requestParameters.setValidity(daysValid);
                requestParameters.setKeyLength(keyLength);
                requestParameters.setSignatureAlgorithm(signatureAlgorithm.getAlgorithmName());
                requestParameters.setCRLDistributionPoint(StringUtils.trimToNull(crlDistributionPoint));
                requestParameters.setCertificateRequestHandler(handler.getCertificateHandlerName());

                CA.RequestResponse requestResponse;

                try {
                    requestResponse = ca.requestCertificate(requestParameters);
                }
                catch (CAException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("Certificate could not be requested [%s]", e.getMessage()),
                            logger);
                }

                // issued can be null when the certificate issuer does not immediately issue a certificate (the
                // certificate request will be stored in the request store and handled asynchronous)
                return new CADTO.NewRequest(toPendingRequest(requestResponse.request()),
                        requestResponse.certStoreEntry() != null ?
                                x509CertificateDetailsFactory.createX509CertificateDetails(
                                        requestResponse.certStoreEntry()) : null);
            }
            catch (IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @GetMapping(path=RestPaths.CA_GET_PENDING_REQUESTS_PATH)
    public List<CADTO.PendingRequest> getPendingRequests(
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PENDING_REQUESTS);

            return certificateRequestStore.getAllRequests(firstResult,
                            restValidator.limitMaxResults(maxResults, logger))
                    .stream().map(this::toPendingRequest).toList();
        });
    }

    @GetMapping(path=RestPaths.CA_GET_PENDING_REQUESTS_COUNT_PATH)
    public long getPendingRequestsCount()
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PENDING_REQUESTS_COUNT);

            return certificateRequestStore.getSize();

        })).orElse(0L);
    }

    @GetMapping(path=RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_PATH)
    public List<CADTO.PendingRequest> getPendingRequestsByEmail(
            @RequestParam String email,
            @RequestParam Match match,
            @RequestParam(required = false, defaultValue = "0") Integer firstResult,
            @RequestParam(required = false, defaultValue = RestValidator.DEFAULT_MAX_RESULTS_STRING) Integer maxResults)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PENDING_REQUESTS_BY_EMAIL);

            return certificateRequestStore.getRequestsByEmail(
                            email,
                            match,
                            firstResult,
                            restValidator.limitMaxResults(maxResults, logger))
                    .stream().map(this::toPendingRequest).toList();
        });
    }

    @GetMapping(path=RestPaths.CA_GET_PENDING_REQUESTS_BY_EMAIL_COUNT_PATH)
    public long getPendingRequestsByEmailCount(@RequestParam String email, @RequestParam Match match)
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PENDING_REQUESTS_BY_EMAIL_COUNT);

            return certificateRequestStore.getSizeByEmail(email, match);
        })).orElse(0L);
    }

    @GetMapping(path=RestPaths.CA_GET_PENDING_REQUEST_PATH)
    public CADTO.PendingRequest getPendingRequest(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_PENDING_REQUEST);

            return toPendingRequest(getCertificateRequestByID(id));
        });
    }

    @PostMapping(path=RestPaths.CA_DELETE_PENDING_REQUEST_PATH)
    public void deletePendingRequest(@RequestParam UUID id)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_PENDING_REQUEST);

            CertificateRequest request = getCertificateRequestByID(id);

            certificateRequestStore.deleteRequest(request.getID());

        });
    }

    @PostMapping(path=RestPaths.CA_DELETE_PENDING_REQUESTS_PATH)
    public void deletePendingRequests(@RequestBody List<UUID> ids)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.DELETE_PENDING_REQUESTS);

            for (UUID id : ids)
            {
                CertificateRequest certificateRequest = certificateRequestStore.getRequest(id);

                if (certificateRequest != null) {
                    certificateRequestStore.deleteRequest(id);
                }
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Pending request with id not found",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CA_RESCHEDULE_REQUEST_PATH)
    public CADTO.PendingRequest rescheduleRequest(@RequestParam UUID id)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.RESCHEDULE_REQUEST);

            CertificateRequest certificateRequest = getCertificateRequestByID(id);

            certificateRequest.setNextUpdate(new Date());

            logger.info("Certificate request with id {} was rescheduled.", id);

            return toPendingRequest(certificateRequest);
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
    })
    @PostMapping(path=RestPaths.CA_RESCHEDULE_REQUESTS_PATH)
    public void rescheduleRequests(@RequestBody List<UUID> ids)
    {
        transactionOperations.executeWithoutResult(tx ->
        {
            checkPermission(Action.RESCHEDULE_REQUESTS);

            for (UUID id : ids)
            {
                CertificateRequest certificateRequest = certificateRequestStore.getRequest(id);

                if (certificateRequest != null)
                {
                    certificateRequest.setNextUpdate(new Date());

                    logger.info("Certificate request with id {} was rescheduled.", id);
                }
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "No matching pending request found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "No certificates in request, Error handling request",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CA_FINALIZE_PENDING_REQUEST_PATH, consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public List<CertificateDTO.X509CertificateDetails> finalizePendingRequest(
            @RequestBody MultipartFile certificates)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.FINALIZE_PENDING_REQUEST);

            try(InputStream certificatesInputStream = certificates.getInputStream())
            {
                final List<CertificateDTO.X509CertificateDetails> handled = new LinkedList<>();

                Collection<X509Certificate> certificateCollection = CertificateUtils.readX509Certificates(
                        certificatesInputStream);

                if (certificateCollection.isEmpty()) {
                    throw RestException.createInstance("No certificates in request")
                            .setKey("validation.no-certificates-in-request").log(logger);
                }

                certificateCollection.forEach(c -> {
                    try {
                        KeyAndCertificate keyAndCertificate = ca.handleRequest(c);

                        if (keyAndCertificate != null)
                        {
                            handled.add(x509CertificateDetailsFactory.createX509CertificateDetails(
                                    keyAndCertificate.getCertificate()));
                        }
                    }
                    catch (CAException | IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                                "Error handling request", logger);
                    }
                });

                if (handled.isEmpty()) {
                    throw RestException.createInstance("No pending request found that matches the uploaded certificate")
                            .setKey("validation.no-matching-pending-request").log(logger);
                }

                return handled;
            }
            catch (IOException | CertificateException | NoSuchProviderException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @PostMapping(path=RestPaths.CA_CREATE_SELF_SIGNED_CERTIFICATE_PATH)
    public CertificateDTO.X509CertificateDetails createSelfSignedCertificate(
            @RequestParam String email,
            @RequestBody CADTO.X500PrincipalRequest subject,
            @RequestParam @Schema(minimum = "1") @Min(value = 1) int daysValid,
            @RequestParam @Schema(allowableValues = {"2048", "3072", "4096"}, defaultValue = "3072")
                    @Min(value = 2048) @Max(value = 4096) int keyLength,
            @RequestParam CACertificateSignatureAlgorithm signatureAlgorithm,
            @RequestParam(defaultValue = "true") boolean whitelist)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.CREATE_SELF_SIGNED_CERTIFICATE);

            try {
                RequestParameters parameters = new RequestParametersImpl();

                parameters.setSubject(toX500Principal(subject));
                parameters.setEmail(email);
                parameters.setValidity(daysValid);
                parameters.setKeyLength(keyLength);
                parameters.setSignatureAlgorithm(signatureAlgorithm.getAlgorithmName());

                KeyAndCertificate keyAndCertificate = selfSignedKeyAndCertificateIssuer.issueKeyAndCertificate(
                        parameters, null /* self signed */);

                X509CertStoreEntry certStoreEntry = keyAndCertStore.addKeyAndCertificate(keyAndCertificate);

                if (whitelist)
                {
                    CTL ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);

                    CTLEntry entry = ctl.createEntry(keyAndCertificate.getCertificate());

                    entry.setStatus(CTLEntryStatus.WHITELISTED);
                    entry.setAllowExpired(false);

                    ctl.addEntry(entry);
                }

                return x509CertificateDetailsFactory.createX509CertificateDetails(certStoreEntry.getCertificate(),
                        certStoreEntry.getKeyAlias());
            }
            catch (IOException | CAException | CertStoreException | KeyStoreException | CTLException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Certificate with thumbprint not found",
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.BAD_REQUEST_RESPONSE_CODE,
                    description = "Certificate does not have a private key",
                    content = @Content(schema = @Schema(hidden = true))),
    })
    @PostMapping(path=RestPaths.CA_CREATE_CRL_PATH)
    public CRLDTO.X509CRLDetails createCRL(
            @RequestParam String issuerThumbprint,
            @RequestParam long nextUpdate,
            @RequestParam boolean updateExistingCRL,
            @RequestParam CACertificateSignatureAlgorithm signatureAlgorithm,
            @RequestBody List<String> serialNumbersHex)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.CREATE_CRL);

            try {
                KeyAndCertificate issuer = keyAndCertStore.getKeyAndCertificate(getCertStoreEntry(issuerThumbprint));

                if (issuer == null || issuer.getPrivateKey() == null)
                {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("Certificate with thumbprint %s does not have a private key",
                                    issuerThumbprint), logger);
                }

                // check if certificate can be used to sign CRLs
                X509CertSelector selector = new X509CertSelector();
                selector.setKeyUsage(KeyUsageType.getKeyUsageArray(Set.of(KeyUsageType.CRLSIGN)));

                if (!selector.match(issuer.getCertificate())) {
                    throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.BAD_REQUEST,
                            String.format("Certificate with thumbprint %s cannot sign CRLs",
                                    issuerThumbprint), logger);
                }

                X509CRLBuilder crlBuilder = SecurityFactoryFactory.getSecurityFactory().createX509CRLBuilder();

                crlBuilder.setSignatureAlgorithm(signatureAlgorithm.getAlgorithmName());
                crlBuilder.setThisUpdate(new Date());
                crlBuilder.setNextUpdate(new Date(nextUpdate));

                X509CRLBuilderHelper builderHelper = new X509CRLBuilderHelper(crlStore, crlBuilder);

                builderHelper.addEntries(serialNumbersHex);

                X509CRL crl = builderHelper.generateCRL(issuer, updateExistingCRL);

                // Add the new CRL to the CRL store using the crlStoreMaintainer
                // (replaces any existing CRL with the new CRL)
                crlStoreMaintainer.addCRL(crl);

                return x509CRLDetailsFactory.createX509CRLDetails(crl);
            }
            catch (CertStoreException | KeyStoreException | CRLException | CRLStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private @Nonnull X509CertStoreEntry getCertStoreEntry(@Nonnull String thumbprint)
    throws CertStoreException
    {
        X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(
                restValidator.validateThumbprint(thumbprint, logger));

        if (certStoreEntry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Certificate with thumbprint %s not found", thumbprint), logger);
        }

        return certStoreEntry;
    }

    private static X500Principal toX500Principal(CADTO.X500PrincipalRequest principal)
    throws IOException
    {
        if (principal == null) {
            return null;
        }

        X500PrincipalBuilder principalBuilder = X500PrincipalBuilder.getInstance();

        principalBuilder.setEmail(principal.email());
        principalBuilder.setGivenName(principal.givenName());
        principalBuilder.setSurname(principal.surname());
        principalBuilder.setCommonName(principal.commonName());
        principalBuilder.setCountryCode(principal.countryCode());
        principalBuilder.setLocality(principal.locality());
        principalBuilder.setOrganisation(principal.organisation());
        principalBuilder.setOrganisationalUnit(principal.organisationalUnit());
        principalBuilder.setState(principal.state());

        return principalBuilder.buildPrincipal();
    }

    private static CADTO.CertificateRequestHandlerDetails toCertificateRequestHandlerDetails(
            CertificateRequestHandler certificateRequestHandler)
    {
        return new CADTO.CertificateRequestHandlerDetails(
                certificateRequestHandler.getCertificateHandlerName(),
                certificateRequestHandler.isEnabled(),
                certificateRequestHandler.isInstantlyIssued());
    }

    private @Nonnull CADTO.PendingRequest toPendingRequest(CertificateRequest certificateRequest)
    {
        CertificateRequestHandler handler = certificateRequestHandlerRegistry.getHandler(
                certificateRequest.getCertificateHandlerName());

        String pemEncodedRequest = null;

        try {
            if (handler != null)
            {
                // if the request handler implements EncodedRequestProvider, get the encoded request
                pemEncodedRequest = handler instanceof EncodedRequestProvider encodedRequestProvider ?
                        encodedRequestProvider.getPemEncodedRequest(certificateRequest) : null;
            }
            else {
                // can happen if the certificate request handler was removed after the request was created
                // should not happen in a normal setup though
                logger.warn("Certificate request handler with name {} not found",
                        certificateRequest.getCertificateHandlerName());
            }
        }
        catch (CAException e) {
            logger.error("Error getting encoded request", e);
        }

        return new CADTO.PendingRequest(
                certificateRequest.getID(),
                certificateRequest.getCreated().getTime(),
                StringUtils.defaultString(X500PrincipalInspector.getFriendly(certificateRequest.getSubject())),
                StringUtils.defaultString(certificateRequest.getEmail()),
                certificateRequest.getValidity(),
                StringUtils.defaultString(certificateRequest.getSignatureAlgorithm()),
                certificateRequest.getKeyLength(),
                StringUtils.defaultString(certificateRequest.getCRLDistributionPoint()),
                StringUtils.defaultString(certificateRequest.getCertificateHandlerName()),
                StringUtils.defaultString(certificateRequest.getInfo()),
                certificateRequest.getIteration(),
                Optional.ofNullable(certificateRequest.getLastUpdated()).map(Date::getTime).orElse(-1L),
                Optional.ofNullable(certificateRequest.getNextUpdate()).map(Date::getTime).orElse(-1L),
                StringUtils.defaultString(certificateRequest.getLastMessage()),
                StringUtils.defaultString(pemEncodedRequest));
    }

    private CertificateRequest getCertificateRequestByID(UUID id)
    {
        CertificateRequest certificateRequest = certificateRequestStore.getRequest(id);

        if (certificateRequest == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Pending request with id %s not found", id), logger);
        }

        return certificateRequest;
    }
}
