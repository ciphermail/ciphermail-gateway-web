/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.EncryptionRecipientSelector;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.NamedCertificateUtils;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PermissionUtils;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionProvider;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.util.HttpStatusUtils;
import com.google.common.base.CaseFormat;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@Tag(name = "CertificateSelection")
// suppress warning because most beans are created in XML
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class CertificateSelectionController
{
    private static final Logger logger = LoggerFactory.getLogger(CertificateSelectionController.class);

    private static final String CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION =
            "A certificate with the given thumbprint was not found";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private EncryptionRecipientSelector encryptionRecipientSelector;

    @Autowired
    private ResponseStatusExceptionFactory responseStatusExceptionFactory;

    @Autowired
    private RestValidator restValidator;

    @Autowired
    private X509CertificateDetailsFactory x509CertificateDetailsFactory;

    @Autowired
    private PermissionChecker permissionChecker;

    private enum Action
    {
        GET_AUTO_SELECTED_ENCRYPTION_CERTIFICATES_FOR_USER,
        GET_EXPLICIT_CERTIFICATES_FOR_USER,
        SET_EXPLICIT_CERTIFICATES_FOR_USER,
        GET_INHERITED_CERTIFICATES_FOR_USER,
        GET_ENCRYPTION_CERTIFICATES_FOR_USER,
        GET_NAMED_CERTIFICATES_FOR_USER,
        SET_NAMED_CERTIFICATES_FOR_USER,
        GET_INHERITED_NAMED_CERTIFICATES_FOR_USER,
        GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN,
        SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN,
        GET_INHERITED_CERTIFICATES_FOR_DOMAIN,
        GET_NAMED_CERTIFICATES_FOR_DOMAIN,
        SET_NAMED_CERTIFICATES_FOR_DOMAIN,
        GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN,
        GET_EXPLICIT_CERTIFICATES_GLOBAL,
        SET_EXPLICIT_CERTIFICATES_GLOBAL,
        GET_NAMED_CERTIFICATES_GLOBAL,
        SET_NAMED_CERTIFICATES_GLOBAL,
        GET_SIGNING_CERTIFICATE_FOR_USER,
        SET_SIGNING_CERTIFICATE_FOR_USER,
        GET_SIGNING_CERTIFICATE_FOR_DOMAIN,
        SET_SIGNING_CERTIFICATE_FOR_DOMAIN,
        GET_SIGNING_CERTIFICATE_FOR_GLOBAL,
        SET_SIGNING_CERTIFICATE_FOR_GLOBAL;

        public String toPermissionName() {
            return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
        }
    }

    private static class PermissionProviderImpl implements PermissionProvider
    {
        @Override
        public Set<String> getPermissions()
        {
            Set<String> permissions = new HashSet<>();

            for (Action action : Action.values())
            {
                permissions.add(PermissionUtils.toPermission(PermissionCategory.CERTIFICATE_SELECTION,
                        action.toPermissionName()));
            }

            return permissions;
        }
    }

    private void checkPermission(@Nonnull CertificateSelectionController.Action action) {
        permissionChecker.checkPermission(PermissionCategory.CERTIFICATE_SELECTION, action.toPermissionName());
    }

    public CertificateSelectionController(@Nonnull PermissionProviderRegistry permissionProviderRegistry) {
        permissionProviderRegistry.addPermissionProvider(new PermissionProviderImpl());
    }

    @Operation(summary = "Get all automatically selected encryption certificates for a user",
            description =
                    """
                    When the property "autoSelectEncryptionCerts" is activated for a user, certificates that meet
                    the following criteria will be returned: they must be valid for encryption, trusted,
                    not revoked, and have a matching email address to that of the user.
                    
                    Certificates that are inherited, manually selected, or those that lack PKI validity will not be 
                    included in the list of automatically selected certificates.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_AUTOSELECTED_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getAutoSelectedEncryptionCertificatesForUser(
            @RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_AUTO_SELECTED_ENCRYPTION_CERTIFICATES_FOR_USER);

            try {
                return getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                        .getAutoSelectEncryptionCertificates().stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all explicitly set certificates for a user",
            description =
                    """
                    Returns all the explicitly set certificate for a user.
                    
                    Whether the certificates are used for encryption depends on a number of factors like whether
                    the certificate is valid for email encryption, is trusted and not revoked.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getExplicitCertificatesForUser(
            @RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_EXPLICIT_CERTIFICATES_FOR_USER);

            try {
                return getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                            .getUserPreferences().getCertificates().stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set certificates for a user",
            description =
                    """
                    Set certificates for a user. Whether the certificates are used for encryption depends on a number
                    of factors like whether the certificate is valid for email encryption, is trusted and not revoked.
                    
                    If the list if thumbprints is empty, the explicitly list of certificates for the user will be
                    cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> setExplicitCertificatesForUser(
            @RequestParam String email, @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_EXPLICIT_CERTIFICATES_FOR_USER);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                UserPreferences userPreferences = getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)
                        .getUserPreferences();

                // Replace the certificates with the new certificates
                userPreferences.getCertificates().clear();
                userPreferences.getCertificates().addAll(certificates);

                return certificates.stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).toList();
            }
            catch (HierarchicalPropertiesException | AddressException | CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all inherited certificates for a user",
            description =
                    """
                    Returns all inherited certificate for a user.
                    
                    Certificates are inherited from the domain of the user.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getInheritedCertificatesForUser(
            @RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_INHERITED_CERTIFICATES_FOR_USER);

            try {
                return getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                            .getUserPreferences().getInheritedCertificates().stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all encryption certificates for a user",
            description =
                    """
                    Returns all valid and trusted encryption certificate for a user.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_ENCRYPTION_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getEncryptionCertificatesForUser(
            @RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_ENCRYPTION_CERTIFICATES_FOR_USER);

            try {
                List<X509Certificate> certificates = new LinkedList<>();

                encryptionRecipientSelector.select(List.of(getUser(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)), certificates);

                return certificates.stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all named certificates for a user",
            description =
                    """
                    Returns all certificate for a user from the certificate category.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForUser(
            @RequestParam String email, @RequestParam NamedCertificateCategory category)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_NAMED_CERTIFICATES_FOR_USER);

            try {
                return NamedCertificateUtils.getByName(category.name(), getUser(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getUserPreferences()
                            .getNamedCertificates()).stream().map(certificate ->
                            {
                                try {
                                    return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                                }
                                catch (IOException e) {
                                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                                }
                            }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set named certificates for a user",
            description =
                    """
                    If the list if thumbprints is empty, the named list of certificates for the user will be
                    cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesForUser(
            @RequestParam String email, @RequestParam NamedCertificateCategory category,
            @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_NAMED_CERTIFICATES_FOR_USER);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                // Replace the certificates with the new certificates
                NamedCertificateUtils.replaceNamedCertificates(getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getUserPreferences()
                            .getNamedCertificates(), category.name(), certificates);

                return certificates.stream().map(certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).toList();
            }
            catch (HierarchicalPropertiesException | AddressException | CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all inherited named certificates for a user",
            description =
                    """
                    Returns all inherited certificate for a user from the certificate category.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_USER_PATH)
    public List<CertificateDTO.X509CertificateDetails> getInheritedNamedCertificatesForUser(
            @RequestParam String email, @RequestParam NamedCertificateCategory category)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_INHERITED_NAMED_CERTIFICATES_FOR_USER);

            try {
                return NamedCertificateUtils.getByName(category.name(), getUser(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getUserPreferences()
                            .getInheritedNamedCertificates()).stream().map(certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all explicitly set certificates for a domain",
            description =
                    """
                    Returns all the explicitly set certificate for a domain.
                    
                    Whether the certificates are used for encryption depends on a number of factors like whether
                    the certificate is valid for email encryption, is trusted and not revoked.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> getExplicitCertificatesForDomain(
            @RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_EXPLICIT_CERTIFICATES_FOR_DOMAIN);

            return getDomainPreferences(domain).getCertificates().stream().map(
                certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).limit(restValidator.getMaxResultsUpperLimit()).toList();
        });
    }

    @Operation(summary = "Set certificates for a domain",
            description =
                    """
                    Set certificates for a domain. Whether the certificates are used for encryption depends on a number
                    of factors like whether the certificate is valid for email encryption, is trusted and not revoked.
                    
                    If the list if thumbprints is empty, the explicitly list of certificates for the domain will be
                    cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> setExplicitCertificatesForDomain(
            @RequestParam String domain, @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_EXPLICIT_CERTIFICATES_FOR_DOMAIN);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                UserPreferences domainPreferences = getDomainPreferences(domain);

                // Replace the certificates with the new certificates
                domainPreferences.getCertificates().clear();
                domainPreferences.getCertificates().addAll(certificates);

                return certificates.stream().map(certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).toList();
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all inherited certificates for a domain",
            description =
                    """
                    Returns all inherited certificate for a domain.
                    
                    Certificates are inherited from the global settings.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> getInheritedCertificatesForDomain(
            @RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_INHERITED_CERTIFICATES_FOR_DOMAIN);

            return getDomainPreferences(domain).getInheritedCertificates().stream().map(
                        certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
        });
    }

    @Operation(summary = "Get all named certificates for a domain",
            description =
                    """
                    Returns all certificate for a domain from the certificate category.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForDomain(
            @RequestParam String domain, @RequestParam NamedCertificateCategory category)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_NAMED_CERTIFICATES_FOR_DOMAIN);

            return NamedCertificateUtils.getByName(category.name(), getDomainPreferences(domain)
                    .getNamedCertificates()).stream().map(certificate ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).limit(restValidator.getMaxResultsUpperLimit()).toList();
        });
    }

    @Operation(summary = "Set named certificates for a domain",
            description =
                    """
                    If the list if thumbprints is empty, the named list of certificates for the domain will be
                    cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesForDomain(
            @RequestParam String domain, @RequestParam NamedCertificateCategory category,
            @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_NAMED_CERTIFICATES_FOR_DOMAIN);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                // Replace the certificates with the new certificates
                NamedCertificateUtils.replaceNamedCertificates(getDomainPreferences(domain)
                        .getNamedCertificates(), category.name(), certificates);

                return certificates.stream().map(
                        certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).toList();
            }
            catch (CertStoreException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all inherited named certificates for a domain",
            description =
                    """
                    Returns all inherited certificate for a domain from the certificate category.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN_PATH)
    public List<CertificateDTO.X509CertificateDetails> getInheritedNamedCertificatesForDomain(
            @RequestParam String domain, @RequestParam NamedCertificateCategory category)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_INHERITED_NAMED_CERTIFICATES_FOR_DOMAIN);

            return NamedCertificateUtils.getByName(category.name(), getDomainPreferences(domain)
                    .getInheritedNamedCertificates()).stream().map(certificate ->
            {
                try {
                    return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                }
                catch (IOException e) {
                    throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                }
            }).limit(restValidator.getMaxResultsUpperLimit()).toList();
        });
    }


    @Operation(summary = "Get all explicitly set certificates for the global settings",
            description =
                    """
                    Returns all the explicitly set certificate for the global settings.
                    
                    Whether the certificates are used for encryption depends on a number of factors like whether
                    the certificate is valid for email encryption, is trusted and not revoked.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
    public List<CertificateDTO.X509CertificateDetails> getExplicitCertificatesGlobal()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_EXPLICIT_CERTIFICATES_GLOBAL);

            try {
                return globalPreferencesManager.getGlobalUserPreferences().getCertificates().stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set certificates for the global settings",
            description =
                    """
                    Set certificates for the global settings. Whether the certificates are used for encryption depends
                    on a number of factors like whether the certificate is valid for email encryption, is trusted and
                    not revoked.
                    
                    If the list if thumbprints is empty, the explicitly list of certificates for the global settings
                    will be cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_EXPLICIT_CERTIFICATES_GLOBAL_PATH)
    public List<CertificateDTO.X509CertificateDetails> setExplicitCertificatesGlobal(@RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_EXPLICIT_CERTIFICATES_GLOBAL);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                UserPreferences globalPreferences = globalPreferencesManager.getGlobalUserPreferences();

                // Replace the certificates with the new certificates
                globalPreferences.getCertificates().clear();
                globalPreferences.getCertificates().addAll(certificates);

                return certificates.stream().map(certificate ->
                        {
                            try {
                                return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                            }
                            catch (IOException e) {
                                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                            }
                        }).toList();
            }
            catch (CertStoreException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get all named certificates for the global settings",
            description =
                    """
                    Returns all certificate for a domain from the global settings.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_NAMED_CERTIFICATES_GLOBAL_PATH)
    public List<CertificateDTO.X509CertificateDetails> getNamedCertificatesForGlobal(
            @RequestParam NamedCertificateCategory category)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_NAMED_CERTIFICATES_GLOBAL);

            try {
                return NamedCertificateUtils.getByName(category.name(), globalPreferencesManager.getGlobalUserPreferences()
                        .getNamedCertificates()).stream().map(certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).limit(restValidator.getMaxResultsUpperLimit()).toList();
            }
            catch (HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set named certificates for the global settings",
            description =
                    """
                    If the list if thumbprints is empty, the named list of certificates for the global settings will be
                    cleared.
                    """
    )
    @PostMapping(RestPaths.CERTIFICATE_SELECTION_SET_NAMED_CERTIFICATES_GLOBAL_PATH)
    public List<CertificateDTO.X509CertificateDetails> setNamedCertificatesGlobal(
            @RequestParam NamedCertificateCategory category,
            @RequestBody List<String> thumbprints)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.SET_NAMED_CERTIFICATES_GLOBAL);

            try {
                restValidator.limitMaxResults(thumbprints.size(), "number of thumbprints exceed the upper limit %s",
                        logger);

                Set<X509Certificate> certificates = new HashSet<>();

                for (String thumbprint : thumbprints) {
                    certificates.add(getCertStoreEntry(thumbprint).getCertificate());
                }

                // Replace the certificates with the new certificates
                NamedCertificateUtils.replaceNamedCertificates(globalPreferencesManager.getGlobalUserPreferences()
                        .getNamedCertificates(), category.name(), certificates);

                return certificates.stream().map(certificate ->
                {
                    try {
                        return x509CertificateDetailsFactory.createX509CertificateDetails(certificate);
                    }
                    catch (IOException e) {
                        throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
                    }
                }).toList();
            }
            catch (CertStoreException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get the signing certificate for a user",
            description =
                    """
                    Returns the user's signing certificate.
                    
                    A signing certificate always includes a private key linked to it, typically indicated by a key
                    alias being set. If the user has enabled the option "alwaysUseFreshestSigningCert",
                    the most recent signing certificate will be automatically chosen and provided.
                    If "alwaysUseFreshestSigningCert" is not set and a signing certificate has been
                    manually selected and remains valid, the manually selected certificate will be returned.
                    
                    If there is no manually selected certificate or if the selected certificate is no longer
                    valid, the system will automatically select and return the latest signing certificate.
                    If no valid signing certificate is available, an empty result will be returned.
                    """
    )
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_USER_PATH)
    public CertificateDTO.X509CertificateDetails getSigningCertificateForUser(
            @RequestParam String email)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SIGNING_CERTIFICATE_FOR_USER);

            try {
                KeyAndCertificate keyAndCertificate = getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                        .getSigningKeyAndCertificate();

                // if there is no signing certificate, return
                if (keyAndCertificate == null) {
                    return null;
                }

                // lookup the entry to get the key alias for the certificate
                X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(keyAndCertificate.getCertificate());

                String keyAlias = null;

                if (certStoreEntry != null) {
                    keyAlias = certStoreEntry.getKeyAlias();
                }
                else {
                    // should never happen
                    logger.warn("certStoreEntry is null");
                }

                return x509CertificateDetailsFactory.createX509CertificateDetails(keyAndCertificate.getCertificate(),
                        keyAlias);
            }
            catch (HierarchicalPropertiesException | AddressException | CertStoreException | KeyStoreException |
                   IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set the signing certificate for a user",
            description =
                    """
                    Explicitly sets the signing certificate for a user. It's required that the signing certificate
                    exists and is associated with a private key.
                    
                    Whether this signing certificate is actually used depends on various conditions. The signing
                    certificate will not be used if it is not suitable for signing, lacks trust, is revoked,
                    not valid for email purposes, or if the user has the "alwaysUseFreshestSigningCert" option
                    enabled, and a more recent signing certificate is accessible.
                    
                    if the thumbprint is not set, the selected signing certificate will be cleared.
                    """
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Certificate does not have an associated private key",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_USER_PATH)
    public CertificateDTO.X509CertificateDetails setSigningCertificateForUser(@RequestParam String email,
            @RequestParam(required = false) String thumbprint)
    {
        return transactionOperations.execute( tx ->
        {
            checkPermission(Action.SET_SIGNING_CERTIFICATE_FOR_USER);

            try {
                User user = getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                KeyAndCertificate keyAndCertificate = null;

                X509CertStoreEntry certStoreEntry = null;

                if (StringUtils.isNotBlank(thumbprint))
                {
                    certStoreEntry = keyAndCertStore.getByThumbprint(restValidator.validateThumbprint(thumbprint, logger));

                    if (certStoreEntry == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s not found", thumbprint), logger);
                    }

                    keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);

                    if (keyAndCertificate == null || keyAndCertificate.getPrivateKey() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s does not have an associated private key",
                                        thumbprint), logger);
                    }
                }

                user.setSigningKeyAndCertificate(keyAndCertificate);

                return certStoreEntry != null ? x509CertificateDetailsFactory.createX509CertificateDetails(
                        certStoreEntry.getCertificate(), certStoreEntry.getKeyAlias()) : null;
            }
            catch (HierarchicalPropertiesException | AddressException | CertStoreException | KeyStoreException |
                   IOException e)
            {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get the signing certificate for a domain")
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
    public CertificateDTO.X509CertificateDetails getSigningCertificateForDomain(
            @RequestParam String domain)
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SIGNING_CERTIFICATE_FOR_DOMAIN);

            try {
                KeyAndCertificate keyAndCertificate = getDomainPreferences(domain).getKeyAndCertificate();

                // if there is no signing certificate, return
                if (keyAndCertificate == null) {
                    return null;
                }

                // lookup the entry to get the key alias for the certificate
                X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(keyAndCertificate.getCertificate());

                String keyAlias = null;

                if (certStoreEntry != null) {
                    keyAlias = certStoreEntry.getKeyAlias();
                }
                else {
                    // should never happen
                    logger.warn("certStoreEntry is null");
                }

                return x509CertificateDetailsFactory.createX509CertificateDetails(keyAndCertificate.getCertificate(),
                        keyAlias);
            }
            catch (CertStoreException | KeyStoreException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set the signing certificate for a domain")
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Certificate does not have an associated private key",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_DOMAIN_PATH)
    public CertificateDTO.X509CertificateDetails setSigningCertificateForDomain(
            @RequestParam String domain, @RequestParam(required = false) String thumbprint)
    {
        return transactionOperations.execute( tx ->
        {
            checkPermission(Action.SET_SIGNING_CERTIFICATE_FOR_DOMAIN);

            try {
                UserPreferences domainPreferences = getDomainPreferences(domain);

                KeyAndCertificate keyAndCertificate = null;

                X509CertStoreEntry certStoreEntry = null;

                if (StringUtils.isNotBlank(thumbprint))
                {
                    certStoreEntry = keyAndCertStore.getByThumbprint(restValidator.validateThumbprint(thumbprint, logger));

                    if (certStoreEntry == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s not found", thumbprint), logger);
                    }

                    keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);

                    if (keyAndCertificate == null || keyAndCertificate.getPrivateKey() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s does not have an associated private key",
                                        thumbprint), logger);
                    }
                }

                domainPreferences.setKeyAndCertificate(keyAndCertificate);

                return certStoreEntry != null ? x509CertificateDetailsFactory.createX509CertificateDetails(
                        certStoreEntry.getCertificate(), certStoreEntry.getKeyAlias()) : null;
            }
            catch (CertStoreException | KeyStoreException | IOException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Get the signing certificate for global")
    @GetMapping(RestPaths.CERTIFICATE_SELECTION_GET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
    public CertificateDTO.X509CertificateDetails getSigningCertificateForGlobal()
    {
        return transactionOperations.execute(tx ->
        {
            checkPermission(Action.GET_SIGNING_CERTIFICATE_FOR_GLOBAL);

            try {
                KeyAndCertificate keyAndCertificate = globalPreferencesManager.getGlobalUserPreferences()
                        .getKeyAndCertificate();

                // if there is no signing certificate, return
                if (keyAndCertificate == null) {
                    return null;
                }

                // lookup the entry to get the key alias for the certificate
                X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(keyAndCertificate.getCertificate());

                String keyAlias = null;

                if (certStoreEntry != null) {
                    keyAlias = certStoreEntry.getKeyAlias();
                }
                else {
                    // should never happen
                    logger.warn("certStoreEntry is null");
                }

                return x509CertificateDetailsFactory.createX509CertificateDetails(keyAndCertificate.getCertificate(),
                        keyAlias);
            }
            catch (CertStoreException | KeyStoreException | IOException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    @Operation(summary = "Set the signing certificate for global")
    @ApiResponses(value = {
            @ApiResponse(responseCode = HttpStatusUtils.OK_RESPONSE_CODE),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = CERTIFICATE_WITH_THUMBPRINT_NOT_FOUND_DESCRIPTION,
                    content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = HttpStatusUtils.NOT_FOUND_RESPONSE_CODE,
                    description = "Certificate does not have an associated private key",
                    content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(path=RestPaths.CERTIFICATE_SELECTION_SET_SIGNING_CERTIFICATE_FOR_GLOBAL_PATH)
    public CertificateDTO.X509CertificateDetails setSigningCertificateForGlobal(
            @RequestParam(required = false) String thumbprint)
    {
        return transactionOperations.execute( tx ->
        {
            checkPermission(Action.SET_SIGNING_CERTIFICATE_FOR_GLOBAL);

            try {
                UserPreferences globalPreferences = globalPreferencesManager.getGlobalUserPreferences();

                KeyAndCertificate keyAndCertificate = null;

                X509CertStoreEntry certStoreEntry = null;

                if (StringUtils.isNotBlank(thumbprint))
                {
                    certStoreEntry = keyAndCertStore.getByThumbprint(restValidator.validateThumbprint(thumbprint, logger));

                    if (certStoreEntry == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s not found", thumbprint), logger);
                    }

                    keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);

                    if (keyAndCertificate == null || keyAndCertificate.getPrivateKey() == null) {
                        throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("Certificate with thumbprint %s does not have an associated private key",
                                        thumbprint), logger);
                    }
                }

                globalPreferences.setKeyAndCertificate(keyAndCertificate);

                return certStoreEntry != null ? x509CertificateDetailsFactory.createX509CertificateDetails(
                        certStoreEntry.getCertificate(), certStoreEntry.getKeyAlias()) : null;
            }
            catch (CertStoreException | KeyStoreException | IOException | HierarchicalPropertiesException e) {
                throw responseStatusExceptionFactory.createResponseStatusException(e, logger);
            }
        });
    }

    private @Nonnull User getUser(@Nonnull String email, @Nonnull UserWorkflow.UserNotExistResult userNotExistResult)
    throws HierarchicalPropertiesException, AddressException
    {
        String validatedEmail = restValidator.validateEmail(email, logger);

        User user = userWorkflow.getUser(validatedEmail, userNotExistResult);

        if (user == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with email address %s not found", validatedEmail), logger);
        }

        return user;
    }

    private @Nonnull UserPreferences getDomainPreferences(@Nonnull String domain)
    {
        String validatedDomain = restValidator.validateDomain(domain, DomainUtils.DomainType.WILD_CARD, logger);

        UserPreferences domainPreferences = domainManager.getDomainPreferences(validatedDomain);

        if (domainPreferences == null)
        {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Domain %s not found", validatedDomain), logger);
        }

        return domainPreferences;
    }

    private @Nonnull X509CertStoreEntry getCertStoreEntry(@Nonnull String thumbprint)
    throws CertStoreException
    {
        X509CertStoreEntry certStoreEntry = keyAndCertStore.getByThumbprint(
                restValidator.validateThumbprint(thumbprint, logger));

        if (certStoreEntry == null) {
            throw responseStatusExceptionFactory.createResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Certificate with thumbprint %s not found", thumbprint), logger);
        }

        return certStoreEntry;
    }
}
