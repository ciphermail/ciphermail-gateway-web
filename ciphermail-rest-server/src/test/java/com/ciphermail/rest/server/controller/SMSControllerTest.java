/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.core.common.sms.impl.SMSImpl;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.SMSDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SMSController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class SMSControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private SMSGateway smsGateway;

    @MockBean
    private SMSTransportFactoryRegistry transportFactoryRegistry;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void getSMSMessages()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-messages");

        Date now = new Date();

        when(smsGateway.getAll(any(), any(), any(), any())).thenReturn(List.of(
                new SMSImpl(
                        UUID.randomUUID(),
                        "123",
                        "test1",
                        now,
                        DateUtils.addDays(now, 1),
                        null),
                new SMSImpl(
                        UUID.randomUUID(),
                        "456",
                        "test2",
                        DateUtils.addDays(now, 2),
                        DateUtils.addDays(now, 3),
                        "some error")
        ));

        MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_MESSAGES_PATH)
                        .param("sortColumn", SortColumn.CREATED.name())
                        .param("sortDirection", SortDirection.ASC.name())
                        .param("firstResult", "0")
                        .param("maxResults", "100")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<SMSDTO.SMSMessage> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        assertNotNull(response.get(0).id());
        assertEquals("123", response.get(0).phoneNumber());
        assertEquals("test1", response.get(0).message());
        assertEquals(now.getTime(), response.get(0).created());
        assertEquals(DateUtils.addDays(now, 1).getTime(), response.get(0).lastTry());
        assertNotNull(response.get(0).lastError());

        assertNotNull(response.get(1).id());
        assertEquals("456", response.get(1).phoneNumber());
        assertEquals("test2", response.get(1).message());
        assertEquals(DateUtils.addDays(now, 2).getTime(), response.get(1).created());
        assertEquals(DateUtils.addDays(now, 3).getTime(), response.get(1).lastTry());
        assertEquals("some error", response.get(1).lastError());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-messages");
    }

    @Test
    @WithMockUser
    public void getSMSMessagesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_MESSAGES_PATH)
                        .param("sortColumn", SortColumn.CREATED.name())
                        .param("sortDirection", SortDirection.ASC.name())
                        .param("firstResult", "0")
                        .param("maxResults", "100")
                        .contentType("application/json"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(smsGateway, never()).getAll(any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-messages");
    }

    @Test
    @WithMockUser
    public void sendSMSMessage()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.SMS,
                "send-sms");

        Date now = new Date();

        when(smsGateway.sendSMS(any(), any())).thenAnswer(i ->
                new SMSImpl(
                        UUID.randomUUID(),
                        i.getArgument(0),
                        i.getArgument(1),
                        now,
                        DateUtils.addDays(now, 1),
                        null));

        MvcResult result = mockMvc.perform(post(RestPaths.SMS_SEND_SMS_PATH)
                        .param("phoneNumber", "123")
                        .content("test message")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        SMSDTO.SMSMessage response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("123", response.phoneNumber());
        assertEquals("test message", response.message());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "send-sms");
    }

    @Test
    @WithMockUser
    public void sendSMSMessageNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.SMS_SEND_SMS_PATH)
                        .param("phoneNumber", "123")
                        .content("test message")
                        .contentType(MediaType.TEXT_PLAIN)
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(smsGateway, never()).sendSMS(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "send-sms");
    }

    @Test
    @WithMockUser
    public void deleteSMS()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.SMS,
                "delete-sms");

        Date now = new Date();

        when(smsGateway.delete(any())).thenReturn(
                new SMSImpl(
                        UUID.randomUUID(),
                        "123",
                        "test",
                        now,
                        DateUtils.addDays(now, 1),
                        null));

        MvcResult result = mockMvc.perform(post(RestPaths.SMS_DELETE_SMS_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        SMSDTO.SMSMessage response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("123", response.phoneNumber());
        assertEquals("test", response.message());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "delete-sms");
    }

    @Test
    @WithMockUser
    public void deleteSMSNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.SMS_DELETE_SMS_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(smsGateway, never()).delete(any());

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "delete-sms");
    }

    @Test
    @WithMockUser
    public void getSMSQueueSize()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-queue-size");

        when(smsGateway.getCount()).thenReturn(10L);

        MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_QUEUE_SIZE_PATH))
                .andExpect(status().isOk())
                .andReturn();

        Long response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(10L, (long) response);

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-queue-size");
    }

    @Test
    @WithMockUser
    public void getSMSQueueSizeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_QUEUE_SIZE_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(smsGateway, never()).getCount();

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-queue-size");
    }

    @Test
    @WithMockUser
    public void getSMSTransports()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-transports");

        when(transportFactoryRegistry.getFactoryNames()).thenReturn(Set.of("t1", "t2"));

        MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_TRANSPORTS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());
        assertTrue(response.contains("t1"));
        assertTrue(response.contains("t2"));

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-transports");
    }

    @Test
    @WithMockUser
    public void getSMSTransportsNoPermission()
    throws Exception
    {
       MvcResult result = mockMvc.perform(get(RestPaths.SMS_GET_SMS_TRANSPORTS_PATH))
               .andExpect(status().isForbidden())
               .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(transportFactoryRegistry, never()).getFactoryNames();

        verify(permissionChecker).checkPermission(PermissionCategory.SMS,
                "get-sms-transports");
    }
}
