/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PGPTrustListController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class
})
public class PGPTrustListControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private PGPTrustList trustList;

    private static class MockPGPTrustListEntry implements PGPTrustListEntry
    {
        private final String sha256Fingerprint;
        PGPTrustListStatus status;

        MockPGPTrustListEntry(@Nonnull String sha256Fingerprint, PGPTrustListStatus status)
        {
            this.sha256Fingerprint = Objects.requireNonNull(sha256Fingerprint);
            this.status = status;
        }

        @Override
        public @Nonnull String getSHA256Fingerprint() {
            return sha256Fingerprint;
        }

        @Override
        public @Nonnull PGPTrustListStatus getStatus() {
            return status != null ? status : PGPTrustListStatus.UNTRUSTED;
        }

        @Override
        public void setStatus(PGPTrustListStatus status) {
            this.status = status;
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntries()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entries");

        List<? extends PGPTrustListEntry> entries = List.of(new MockPGPTrustListEntry(
                        "ADE29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                        PGPTrustListStatus.TRUSTED),
                new MockPGPTrustListEntry(
                        "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                        PGPTrustListStatus.UNTRUSTED));

        doReturn(entries).when(trustList).getEntries(anyInt(), anyInt());

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPTrustListResult[] resultEntries = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPTrustListResult[].class);

        assertEquals(2, resultEntries.length);
        assertEquals(entries.get(0).getSHA256Fingerprint(), resultEntries[0].sha256Fingerprint());
        assertEquals(entries.get(0).getStatus(), resultEntries[0].status());
        assertEquals(entries.get(1).getSHA256Fingerprint(), resultEntries[1].sha256Fingerprint());
        assertEquals(entries.get(1).getStatus(), resultEntries[1].status());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entries");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntriesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).getEntries(anyInt(), anyInt());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entries");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntriesMaxResultsTooLarge()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entries");

        mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRIES_PATH)
                .param("maxResults", "251"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entries");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");

        MockPGPTrustListEntry entry = new MockPGPTrustListEntry(
                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPTrustListStatus.UNTRUSTED);

        doReturn(entry).when(trustList).getEntry(entry.sha256Fingerprint);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint", entry.sha256Fingerprint))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPTrustListResult resultEntry = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPTrustListResult.class);

        assertEquals(entry.getSHA256Fingerprint(), resultEntry.sha256Fingerprint());
        assertEquals(entry.getStatus(), resultEntry.status());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint", "invalid"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).getEntry((String) any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntryFingerprintNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListEntryInvalidFingerprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        Assert.assertTrue(result.getResponse().getContentAsString().endsWith(
                ",\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"," +
                "\"error\":\"Bad Request\",\"message\":\"invalid is not a valid sha256 fingerprint\"," +
                "\"key\":\"backend.validation.invalid-sha256-fingerprint\",\"params\":null,\"path\":\"\"}"
        ));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-count");

        when(trustList.size()).thenReturn(10L);

        mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-count");
    }

    @Test
    @WithMockUser
    public void getPGPTrustListCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_TRUST_LIST_GET_PGP_TRUST_LIST_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).size();

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "get-pgp-trust-list-count");
    }

    @Test
    @WithMockUser
    public void setPGPTrustListEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entry");

        MockPGPTrustListEntry entry = new MockPGPTrustListEntry(
                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPTrustListStatus.UNTRUSTED);

        when(trustList.createEntry("FFF29F0A314B" +
                                   "1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")).thenReturn(entry);

        mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")
                        .param("status", PGPTrustListStatus.TRUSTED.name())
                        .param("includeSubkeys", "true")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(trustList).setEntry(any(), eq(true));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void setPGPTrustListEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")
                        .param("status", PGPTrustListStatus.TRUSTED.name())
                        .param("includeSubkeys", "true")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).setEntry(any(), anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void setPGPTrustListEntries()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entries");

        MockPGPTrustListEntry entry = new MockPGPTrustListEntry(
                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPTrustListStatus.UNTRUSTED);

        when(trustList.createEntry("FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44" +
                                   "DE07C3B20995")).thenReturn(entry);
        when(trustList.createEntry("AAF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44" +
                                   "DE07C3B20995")).thenReturn(entry);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                "AAF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995"));

        mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRIES_PATH)
                        .content(jsonWriter.toString())
                        .param("status", PGPTrustListStatus.TRUSTED.name())
                        .param("includeSubkeys", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(trustList, times(2)).setEntry(any(), eq(true));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entries");
    }

    @Test
    @WithMockUser
    public void setPGPTrustListEntriesNoPermission()
    throws Exception
    {
        MockPGPTrustListEntry entry = new MockPGPTrustListEntry(
                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPTrustListStatus.UNTRUSTED);

        when(trustList.createEntry("FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44" +
                                   "DE07C3B20995")).thenReturn(entry);
        when(trustList.createEntry("AAF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44" +
                                   "DE07C3B20995")).thenReturn(entry);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                "AAF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_SET_PGP_TRUST_LIST_ENTRIES_PATH)
                        .content(jsonWriter.toString())
                        .param("status", PGPTrustListStatus.TRUSTED.name())
                        .param("includeSubkeys", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).setEntry(any(), eq(true));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "set-pgp-trust-list-entries");
    }

    @Test
    @WithMockUser
    public void deletePGPTrustListEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "delete-pgp-trust-list-entry");

        MockPGPTrustListEntry entry = new MockPGPTrustListEntry(
                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPTrustListStatus.UNTRUSTED);

        doReturn(entry).when(trustList).getEntry(entry.sha256Fingerprint);

        mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")
                        .param("includeSubkeys", "true")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(trustList).deleteEntry(any(), eq(true));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "delete-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void deletePGPTrustListEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")
                        .param("includeSubkeys", "true")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(trustList, never()).deleteEntry(any(), anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "delete-pgp-trust-list-entry");
    }

    @Test
    @WithMockUser
    public void deletePGPTrustListEntryFingerprintNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "delete-pgp-trust-list-entry");

        mockMvc.perform(post(RestPaths.PGP_TRUST_LIST_DELETE_PGP_TRUST_LIST_ENTRY_PATH)
                        .param("sha256Fingerprint",
                                "FFF29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995")
                        .param("includeSubkeys", "true")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("Entry with sha256 fingerprint FFF29F0A314B1760C151946F4E6F8E97" +
                                           "B251B464DDE17003DA44DE07C3B20995 not found"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_TRUST_LIST,
                "delete-pgp-trust-list-entry");
    }
}