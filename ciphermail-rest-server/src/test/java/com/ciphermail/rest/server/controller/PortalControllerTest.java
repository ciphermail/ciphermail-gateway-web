/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.PortalEmailSigner;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.impl.MockupUser;
import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.app.james.MailEnqueuer;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.PortalProperties;
import com.ciphermail.core.app.properties.PortalPropertiesImpl;
import com.ciphermail.core.app.properties.TemplateProperties;
import com.ciphermail.core.app.properties.TemplatePropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.properties.DefaultPropertyProviderRegistry;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidator;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidatorImpl;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.ImageUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.common.util.URIUtils;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestExceptionHandler;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.passay.EnglishSequenceData;
import org.passay.IllegalSequenceRule;
import org.passay.PasswordValidator;
import org.passay.RepeatCharacterRegexRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PortalController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class
})
public class PortalControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private DefaultPropertyProviderRegistry defaultPropertyProviderRegistry;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private UserWorkflow userWorkflow;

    @MockBean
    private MailEnqueuer mailEnqueuer;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices extends RestSystemServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }

        // override portalPasswordStrengthValidator because MockupUserPreferences and therefore some system properties
        // are not set
        @Bean("portalPasswordStrengthValidator")
        public PasswordStrengthValidator passwordStrengthValidatorService()
        {
            return new PasswordStrengthValidatorImpl(new PasswordValidator(
                    new RepeatCharacterRegexRule(5),
                    new IllegalSequenceRule(EnglishSequenceData.USQwerty, 5, true)));
        }
    }

    @Test
    @WithMockUser
    public void getPortalProperties()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-portal-properties");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setEnabled(true);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PORTAL_PROPERTIES_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PortalProperties resultProperties = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PortalProperties.class);

        assertTrue(resultProperties.loginEnabled());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-portal-properties");
    }

    @Test
    @WithMockUser
    public void getPortalPropertiesNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setEnabled(true);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PORTAL_PROPERTIES_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-portal-properties");
    }

    @Test
    @WithMockUser
    public void set2FAEnabled()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-enabled");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        assertFalse(portalProperties.get2FAEnabled());

        mockMvc.perform(post(RestPaths.PORTAL_SET_2FA_ENABLED_PATH)
                        .param("email", "test@example.com")
                        .param("enable", "true")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(portalProperties.get2FAEnabled());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-enabled");
    }

    @Test
    @WithMockUser
    public void set2FAEnabledNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        assertFalse(portalProperties.get2FAEnabled());

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_SET_2FA_ENABLED_PATH)
                        .param("email", "test@example.com")
                        .param("enable", "true")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertFalse(portalProperties.get2FAEnabled());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-enabled");
    }

    @Test
    @WithMockUser
    public void set2FASecret()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-secret");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        assertNull(portalProperties.get2FASecret());

        mockMvc.perform(post(RestPaths.PORTAL_SET_2FA_SECRET_PATH)
                        .param("email", "test@example.com")
                        .content("notarealsecret")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals("notarealsecret", portalProperties.get2FASecret());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-secret");
    }

    @Test
    @WithMockUser
    public void set2FASecretNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        assertNull(portalProperties.get2FASecret());

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_SET_2FA_SECRET_PATH)
                        .param("email", "test@example.com")
                        .content("notarealsecret")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertNull(portalProperties.get2FASecret());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-2fa-secret");
    }

    @Test
    @WithMockUser
    public void getPdfReplyURL()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-url");

        MockupUserPreferences userPreferences = new MockupUserPreferences("sender@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        pdfProperties.setPdfReplyURL("http://pdf.example.com");
        pdfProperties.setPdfReplySecretKey("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
                        .param("sender", "sender@example.com")
                        .param("replyRecipient", "replyRecipient@example.com")
                        .param("replySender", "replySender@example.com")
                        .param("subject", "some subject")
                        .param("messageID", "some-message-id"))
                .andExpect(status().isOk())
                .andReturn();

        URI pdfReplyURL = new URI(result.getResponse().getContentAsString());

        Map<String, List<String>> params = URIUtils.parseQuery(pdfReplyURL.getQuery());

        assertEquals(1, params.get("env").size());
        assertEquals(1, params.get("hmac").size());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-url");
    }

    @Test
    @WithMockUser
    public void getPdfReplyURLNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
                        .param("sender", "sender@example.com")
                        .param("replyRecipient", "replyRecipient@example.com")
                        .param("replySender", "replySender@example.com")
                        .param("subject", "some subject")
                        .param("messageID", "some-message-id"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-url");
    }

    @Test
    @WithMockUser
    public void getPdfReplyParameters()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-parameters");

        // generating a valid URL requires this permission
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-url");

        // generate a valid URL first

        MockupUserPreferences senderPreferences = new MockupUserPreferences("sender@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(senderPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(senderPreferences.getName(), senderPreferences, null));

        PDFProperties senderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(senderPreferences.getProperties());

        senderPdfProperties.setPdfReplyURL("http://pdf.example.com");
        senderPdfProperties.setPdfReplyEnabled(true);
        senderPdfProperties.setPdfReplySubjectFormatString("Re: %s");
        senderPdfProperties.setPdfReplyFromFormatString("in name of %s");
        senderPdfProperties.setPdfReplySecretKey("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
                        .param("sender", "sender@example.com")
                        .param("replyRecipient", "replyRecipient@example.com")
                        .param("replySender", "replySender@example.com")
                        .param("subject", "some subject")
                        .param("messageID", "some-message-id"))
                .andExpect(status().isOk())
                .andReturn();

        URI pdfReplyURL = new URI(result.getResponse().getContentAsString());

        Map<String, List<String>> params = URIUtils.parseQuery(pdfReplyURL.getQuery());

        String env = params.get("env").get(0);
        String hmac = params.get("hmac").get(0);

        // enable PDF reply allowed for the reply sender
        MockupUserPreferences replySenderPreferences = new MockupUserPreferences("replysender@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(replySenderPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(replySenderPreferences.getName(), replySenderPreferences, null));

        PDFProperties replySenderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(replySenderPreferences.getProperties());

        replySenderPdfProperties.setPdfReplyEnabled(true);
        replySenderPdfProperties.setPdfReplyBodyMaxSize(100000L);
        replySenderPdfProperties.setPdfReplyAttachmentsMaxSize(100000L);
        replySenderPdfProperties.setPdfReplyValidityInterval(60L);

        result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
                        .param("env", env)
                        .param("hmac", hmac))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PdfReplyParameters pdfReplyParameters = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PdfReplyParameters.class);

        assertEquals("replysender@example.com", pdfReplyParameters.from());
        assertEquals("replysender@example.com", pdfReplyParameters.sender());
        assertEquals("replyrecipient@example.com", pdfReplyParameters.to());
        assertNull(pdfReplyParameters.cc());
        assertEquals("Re: some subject", pdfReplyParameters.subject());
        assertEquals("replysender@example.com", pdfReplyParameters.replyTo());
        assertEquals("some-message-id", pdfReplyParameters.messageID());
        assertFalse(pdfReplyParameters.expired());
        assertTrue(pdfReplyParameters.replyAllowed());
        assertEquals(100000L, pdfReplyParameters.bodyMaxSize());
        assertEquals(100000L, pdfReplyParameters.attachmentsMaxSize());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-parameters");

        // test with replySender and cc
        senderPdfProperties.setPdfReplySenderEnabled(true);
        senderPdfProperties.setPdfReplySender("pdf-reply@example.com");
        senderPdfProperties.setPdfReplyCC(true);
        replySenderPdfProperties.setPdfReplyCC(true);

        result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
                        .param("env", env)
                        .param("hmac", hmac))
                .andExpect(status().isOk())
                .andReturn();

        pdfReplyParameters = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PdfReplyParameters.class);

        assertEquals("\"in name of replysender@example.com\" <pdf-reply@example.com>", pdfReplyParameters.from());
        assertEquals("pdf-reply@example.com", pdfReplyParameters.sender());
        assertEquals("replyrecipient@example.com", pdfReplyParameters.to());
        assertEquals("replysender@example.com", pdfReplyParameters.cc());
        assertEquals("Re: some subject", pdfReplyParameters.subject());
        assertEquals("replysender@example.com", pdfReplyParameters.replyTo());
        assertEquals("some-message-id", pdfReplyParameters.messageID());
        assertTrue(pdfReplyParameters.replyAllowed());
        assertEquals(100000L, pdfReplyParameters.bodyMaxSize());
        assertEquals(100000L, pdfReplyParameters.attachmentsMaxSize());
    }

    @Test
    @WithMockUser
    public void getPdfReplyParametersExpired()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-parameters");

        // generating a valid URL requires this permission
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-url");

        // generate a valid URL first

        MockupUserPreferences senderPreferences = new MockupUserPreferences("sender@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(senderPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(senderPreferences.getName(), senderPreferences, null));

        PDFProperties senderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(senderPreferences.getProperties());

        senderPdfProperties.setPdfReplyURL("http://pdf.example.com");
        senderPdfProperties.setPdfReplyEnabled(true);
        senderPdfProperties.setPdfReplySubjectFormatString("Re: %s");
        senderPdfProperties.setPdfReplyFromFormatString("in name of %s");
        senderPdfProperties.setPdfReplySecretKey("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
                        .param("sender", "sender@example.com")
                        .param("replyRecipient", "replyRecipient@example.com")
                        .param("replySender", "replySender@example.com")
                        .param("subject", "some subject")
                        .param("messageID", "some-message-id"))
                .andExpect(status().isOk())
                .andReturn();

        URI pdfReplyURL = new URI(result.getResponse().getContentAsString());

        Map<String, List<String>> params = URIUtils.parseQuery(pdfReplyURL.getQuery());

        String env = params.get("env").get(0);
        String hmac = params.get("hmac").get(0);

        // enable PDF reply allowed for the reply sender
        MockupUserPreferences replySenderPreferences = new MockupUserPreferences("replysender@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(replySenderPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(replySenderPreferences.getName(), replySenderPreferences, null));

        PDFProperties replySenderPdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(replySenderPreferences.getProperties());

        replySenderPdfProperties.setPdfReplyEnabled(true);
        replySenderPdfProperties.setPdfReplyBodyMaxSize(100000L);
        replySenderPdfProperties.setPdfReplyAttachmentsMaxSize(100000L);
        replySenderPdfProperties.setPdfReplyValidityInterval(1L);

        ThreadUtils.sleepQuietly(1001);

        result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
                        .param("env", env)
                        .param("hmac", hmac))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PdfReplyParameters pdfReplyParameters = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PdfReplyParameters.class);

        assertEquals("replysender@example.com", pdfReplyParameters.from());
        assertEquals("replysender@example.com", pdfReplyParameters.sender());
        assertEquals("replyrecipient@example.com", pdfReplyParameters.to());
        assertNull(pdfReplyParameters.cc());
        assertEquals("Re: some subject", pdfReplyParameters.subject());
        assertEquals("replysender@example.com", pdfReplyParameters.replyTo());
        assertEquals("some-message-id", pdfReplyParameters.messageID());
        assertTrue(pdfReplyParameters.expired());
        assertTrue(pdfReplyParameters.replyAllowed());
        assertEquals(100000L, pdfReplyParameters.bodyMaxSize());
        assertEquals(100000L, pdfReplyParameters.attachmentsMaxSize());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-parameters");
    }

    @Test
    @WithMockUser
    public void getPdfReplyParametersNoPermissions()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
                        .param("env", "invalid")
                        .param("hmac", "invalid"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-reply-parameters");
    }

    @Test
    @WithMockUser
    public void getPdfPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .param("code", " eyJhIjoib3RwIiwiayI6IjN5ZXplY2kiLCJwIjoiMjA0NzgwMTcwNiIsICJsIjoxNn0"))
                .andExpect(status().isOk())
                .andReturn();

        String password = result.getResponse().getContentAsString();

        assertEquals("wt7a656d7xj75oj6t26pob3nrm", password);

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-password");
    }

    @Test
    @WithMockUser
    public void getPdfPasswordNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .param("code", " eyJhIjoib3RwIiwiayI6IjN5ZXplY2kiLCJwIjoiMjA0NzgwMTcwNiIsICJsIjoxNn0"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-password");
    }

    @Test
    @WithMockUser
    public void getPdfPasswordInvalidCode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_PDF_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .param("code", " invalid"))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("OTP code is invalid", error.message());
        assertEquals("backend.portal.otp-code-invalid", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-pdf-password");
    }

    @Test
    @WithMockUser
    public void getOTPSecretKeyQRCode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-secret-key-qr-code");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_SECRET_KEY_QR_CODE_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        String qrCode = result.getResponse().getContentAsString();

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(Base64Utils.decode(qrCode)));

        Assertions.assertEquals(200, image.getWidth(null));
        Assertions.assertEquals(200, image.getHeight(null));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new RGBLuminanceSource(image.getWidth(null), image.getHeight(null),
                        ImageUtils.getPixels(image))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        JSONObject json = new JSONObject(qrCodeResult.getText());

        Assertions.assertEquals("cs", json.getString("a"));
        Assertions.assertEquals("", json.getString("h"));
        Assertions.assertEquals("efpdmfq", json.getString("k"));
        Assertions.assertEquals("ajJ4d2t3eTI2NWl3YXJzcnJ3Y2d2bmRxcG80ejdpNGc1c3JlYXZ1a2dmZGpkYXh6N3ZxcQ==",
                json.getString("s"));

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-secret-key-qr-code");
    }

    @Test
    @WithMockUser
    public void getOTPSecretKeyQRCodeNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_SECRET_KEY_QR_CODE_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-secret-key-qr-code");
    }

    @Test
    @WithMockUser
    public void getOTPJSONCode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-json-code");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_JSON_CODE_PATH)
                        .param("email", "test@example.com")
                        .param("passwordID", "some-id")
                        .param("passwordLength", "6"))
                .andExpect(status().isOk())
                .andReturn();

        String jsonCode = MiscStringUtils.toStringFromUTF8Bytes(Base64Utils.decode(
                result.getResponse().getContentAsString()));

        System.out.println(jsonCode);

        JSONObject json = new JSONObject(jsonCode);

        Assertions.assertEquals("otp", json.getString("a"));
        Assertions.assertEquals("some-id", json.getString("p"));
        Assertions.assertEquals("efpdmfq", json.getString("k"));
        Assertions.assertEquals(6, json.getInt("l"));

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-json-code");
    }

    @Test
    @WithMockUser
    public void getOTPJSONCodeNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_JSON_CODE_PATH)
                        .param("email", "test@example.com")
                        .param("passwordID", "some-id")
                        .param("passwordLength", "6"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-json-code");
    }

    @Test
    @WithMockUser
    public void getOTPQRCode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-qr-code");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_QR_CODE_PATH)
                        .param("email", "test@example.com")
                        .param("passwordID", "some-id")
                        .param("passwordLength", "6"))
                .andExpect(status().isOk())
                .andReturn();

        String qrCode = result.getResponse().getContentAsString();

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(Base64Utils.decode(qrCode)));

        Assertions.assertEquals(200, image.getWidth(null));
        Assertions.assertEquals(200, image.getHeight(null));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new RGBLuminanceSource(image.getWidth(null), image.getHeight(null),
                        ImageUtils.getPixels(image))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        JSONObject json = new JSONObject(qrCodeResult.getText());

        Assertions.assertEquals("otp", json.getString("a"));
        Assertions.assertEquals("some-id", json.getString("p"));
        Assertions.assertEquals("efpdmfq", json.getString("k"));
        Assertions.assertEquals(6, json.getInt("l"));

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-qr-code");
    }

    @Test
    @WithMockUser
    public void getOTPQRCodeNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        userPreferences.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_GET_OTP_QR_CODE_PATH)
                        .param("email", "test@example.com")
                        .param("passwordID", "some-id")
                        .param("passwordLength", "6"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-get-otp-qr-code");
    }

    @Test
    @WithMockUser
    public void validatePortalSignup()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setSignupValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest))))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PortalSignupValidationResult validationResult  = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PortalSignupValidationResult.class);

        assertEquals("recipient@example.com", validationResult.email());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");
    }

    @Test
    @WithMockUser
    public void validatePortalSignupNoPermission()
    throws Exception
    {
        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest))))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");
    }

    @Test
    @WithMockUser
    public void validatePortalSignupExpired()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setServerSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setSignupValidityIntervalSeconds(1L);

        long ts = System.currentTimeMillis() - 2000;

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest))))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Portal signup link has expired", error.message());
        assertEquals("backend.portal.signup-link-expired", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");
    }

    @Test
    @WithMockUser
    public void validatePortalSignupInvalidChecksum()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setSignupValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts + 123,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest))))
                .andExpect(status().isBadRequest())
                .andReturn();


        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Checksum is not valid", error.message());
        assertEquals("backend.portal.checksum-invalid", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");
    }

    @Test
    @WithMockUser
    public void validatePortalSignupPortalPasswordSet()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("somelongpassword");
        portalProperties.setSignupValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest))))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                 result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Password is already set", error.message());
        assertEquals("backend.portal.password-already-set", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-signup");
    }

    @Test
    @WithMockUser
    public void setPortalSignupPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-portal-signup-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setSignupValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest)))
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PortalSignupValidationResult validationResult  = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PortalSignupValidationResult.class);

        assertEquals("recipient@example.com", validationResult.email());
        assertTrue(Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8().matches("test-password",
                portalProperties.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-portal-signup-password");
    }

    @Test
    @WithMockUser
    public void setPortalSignupPasswordNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setSignupValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_SIGNUP);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail signupRequest = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH)
                        .param("signupCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(signupRequest)))
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-set-portal-signup-password");
    }

    @Test
    @WithMockUser
    public void forgotPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        // because of MockupUserPreferences, some system properties must be explicitly set
        userPreferences.getProperties().setClientSecret("not-a-secret");
        userPreferences.getProperties().setSystemFrom("no-reply@example.com");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        // because of MockupUserPreferences, some system properties must be explicitly set
        portalProperties.setPasswordResetURL("http://localhost:8080/reset-password");
        portalProperties.setSignupValidityIntervalSeconds(100L);

        TemplateProperties templateProperties = userPropertiesFactoryRegistry.getFactoryForClass(TemplatePropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        // because of MockupUserPreferences, some system properties must be explicitly set
        templateProperties.setPortalPasswordResetTemplate("dummy template");

        mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(mailEnqueuer, times(1)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void forgotPasswordNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(mailEnqueuer, times(0)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void forgotPasswordMissingUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("User with email address recipient@example.com not found", error.message());
        assertEquals("backend.portal.user-not-registered", error.key());

        verify(mailEnqueuer, times(0)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void forgotPasswordResetURLNotSet()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        // because of MockupUserPreferences, some system properties must be explicitly set
        portalProperties.setPasswordResetInterval(10000L);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Password reset URL is not set", error.message());
        assertEquals("backend.portal.password-reset-url-not-set", error.key());

        verify(mailEnqueuer, times(0)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void forgotPasswordTryAgainTooFast()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        // because of MockupUserPreferences, some system properties must be explicitly set
        portalProperties.setPasswordResetTimestamp(System.currentTimeMillis());
        portalProperties.setPasswordResetInterval(10000L);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("It's not allowed to start password resets in rapid succession (try again later)", error.message());
        assertEquals("backend.portal.password-reset-too-fast", error.key());

        verify(mailEnqueuer, times(0)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void forgotPasswordSystemFromNotSet()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        // because of MockupUserPreferences, some system properties must be explicitly set
        portalProperties.setPasswordResetURL("http://localhost:8080/reset-password");
        portalProperties.setPasswordResetInterval(10000L);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .param("email", "recipient@example.com")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("System From is not set", error.message());
        assertEquals("backend.portal.system-from-not-set", error.key());

        verify(mailEnqueuer, times(0)).enqueue(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-forgot-password");
    }

    @Test
    @WithMockUser
    public void validatePortalPasswordReset()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPasswordResetTimestamp(System.currentTimeMillis());
        portalProperties.setPasswordResetValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request))))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PortalPasswordResetValidationResult validationResult  = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PortalPasswordResetValidationResult.class);

        assertEquals("recipient@example.com", validationResult.email());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");
    }

    @Test
    @WithMockUser
    public void validatePortalPasswordResetNoPendingReset()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPasswordResetValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request))))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("There is no pending password reset", error.message());
        assertEquals("backend.portal.no-pending-password-reset", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");
    }

    @Test
    @WithMockUser
    public void validatePortalPasswordResetExpired()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPasswordResetTimestamp(System.currentTimeMillis());
        portalProperties.setPasswordResetValidityIntervalSeconds(1L);

        long ts = System.currentTimeMillis() - 2000;

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request))))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Portal password reset link has expired", error.message());
        assertEquals("backend.portal.password-reset-link-expired", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");
    }

    @Test
    @WithMockUser
    public void validatePortalPasswordResetInvalidChecksum()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPasswordResetTimestamp(System.currentTimeMillis());
        portalProperties.setPasswordResetValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts + 123,
                calculatedMAC);

        MvcResult result = mockMvc.perform(get(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request))))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Checksum is not valid", error.message());
        assertEquals("backend.portal.checksum-invalid", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-validate-portal-password-reset");
    }

    @Test
    @WithMockUser
    public void resetPortalPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-reset-portal-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("recipient@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        userPreferences.getProperties().setClientSecret("not-a-secret");

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPasswordResetTimestamp(System.currentTimeMillis());
        portalProperties.setPasswordResetValidityIntervalSeconds(100L);

        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request)))
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PortalDTO.PortalPasswordResetValidationResult validationResult  = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PortalDTO.PortalPasswordResetValidationResult.class);

        assertEquals("recipient@example.com", validationResult.email());

        assertTrue(Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8().matches("test-password",
                portalProperties.getPassword()));
        assertNull(portalProperties.getPasswordResetTimestamp());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-reset-portal-password");
    }

    @Test
    @WithMockUser
    public void resetPortalPasswordNoPermission()
    throws Exception
    {
        long ts = System.currentTimeMillis();

        PortalEmailSigner emailSigner = new PortalEmailSigner("recipient@example.com", ts,
                PortalEmailSigner.PORTAL_RESET_PASSWORD);

        String calculatedMAC = emailSigner.calculateMAC("not-a-secret");

        PortalDTO.MACProtectedEmail request = new PortalDTO.MACProtectedEmail("recipient@example.com", ts,
                calculatedMAC);

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH)
                        .param("resetCode", Base64Utils.encode(JacksonUtil.getObjectMapper().writeValueAsBytes(request)))
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-reset-portal-password");
    }

    @Test
    @WithMockUser
    public void testChangePassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("test");

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "new");

        mockMvc.perform(post(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(passwordEncoder.matches("new", portalProperties.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");
    }

    @Test
    @WithMockUser
    public void testChangePasswordNoPermission()
    throws Exception
    {
        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("test");

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "new");

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");
    }

    @Test
    @WithMockUser
    public void testChangePasswordNewPasswordEmpty()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("test");

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "", "new");

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("New password is empty", error.message());
        assertEquals("backend.portal.new-password-empty", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");
    }

    @Test
    @WithMockUser
    public void testChangePasswordPasswordMismatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("test");

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "other");

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("New password and repeated password do not match", error.message());
        assertEquals("backend.portal.password-mismatch", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");
    }

    @Test
    @WithMockUser
    public void testChangePasswordPasswordIncorrect()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");

        MockupUserPreferences userPreferences = new MockupUserPreferences("test@example.com",
                UserPreferencesCategory.USER.name());

        when(userWorkflow.getUser(userPreferences.getName(), UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser(userPreferences.getName(), userPreferences, null));

        PortalProperties portalProperties = userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(userPreferences.getProperties());

        portalProperties.setPassword("test");

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("wrong",
                "new", "new");

        MvcResult result = mockMvc.perform(post(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .param("email", "test@example.com")
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("Current password is incorrect", error.message());
        assertEquals("backend.portal.current-password-incorrect", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PORTAL,
                "portal-change-portal-password");
    }
}
