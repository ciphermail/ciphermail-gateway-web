/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.acme.AcmeManager;
import com.ciphermail.core.app.acme.AcmeTokenCache;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilder;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.AcmeDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.TLSDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.Login;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.util.KeyPairUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyPair;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = AcmeController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class AcmeControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private AcmeManager acmeManager;

    static class MockAccount extends Account
    {
        protected MockAccount()
        throws MalformedURLException, URISyntaxException
        {
            super(new Login(
                    new URI("https://example.com").toURL(),
                    KeyPairUtils.createKeyPair(2048),
                    new Session("https://example.com")));
        }

        @Override
        public Status getStatus() {
            return Status.VALID;
        }

        @Override
        public List<URI> getContacts() {
            try {
                return List.of(new URI("http://contact.example.com"));
            }
            catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Optional<Boolean> getTermsOfServiceAgreed() {
            return Optional.of(true);
        }
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void createAccountKeyPair()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");

        when(acmeManager.getAccountKeyPair()).thenReturn(null);

        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        mockMvc.perform(post(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .param("replaceExistingKey", "false")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).createAccountKeyPair(TLSKeyPairBuilder.Algorithm.SECP256R1);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");
    }

    @Test
    @WithMockUser
    public void createAccountKeyPairNoReplace()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");

        when(acmeManager.getAccountKeyPair()).thenReturn(new KeyPair(null, null));

        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        mockMvc.perform(post(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .param("replaceExistingKey", "false")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Account Key already exist"));

        verify(acmeManager, never()).createAccountKeyPair(TLSKeyPairBuilder.Algorithm.SECP256R1);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");
    }

    @Test
    @WithMockUser
    public void createAccountKeyPairReplace()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");

        when(acmeManager.getAccountKeyPair()).thenReturn(new KeyPair(null, null));

        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        mockMvc.perform(post(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .param("replaceExistingKey", "true")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).createAccountKeyPair(TLSKeyPairBuilder.Algorithm.SECP256R1);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");
    }

    @Test
    @WithMockUser
    public void createAccountKeyPairNoPermission()
    throws Exception
    {
        TLSDTO.Request request = new TLSDTO.Request().setCommonName("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        MvcResult result = mockMvc.perform(post(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .param("replaceExistingKey", "false")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).createAccountKeyPair(TLSKeyPairBuilder.Algorithm.SECP256R1);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "create-account-keypair");
    }

    @Test
    @WithMockUser
    public void accountKeyPairExistsPositive()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "account-keypair-exists");

        when(acmeManager.getAccountKeyPair()).thenReturn(new KeyPair(null, null));

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_ACCOUNT_KEYPAIR_EXISTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(acmeManager).getAccountKeyPair();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "account-keypair-exists");
    }

    @Test
    @WithMockUser
    public void accountKeyPairExistsNegative()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "account-keypair-exists");

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_ACCOUNT_KEYPAIR_EXISTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("false", result.getResponse().getContentAsString());

        verify(acmeManager).getAccountKeyPair();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "account-keypair-exists");
    }

    @Test
    @WithMockUser
    public void accountKeyPairExistsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.ACME_ACCOUNT_KEYPAIR_EXISTS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).getAccountKeyPair();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "account-keypair-exists");
    }

    @Test
    @WithMockUser
    public void getTermsOfService()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-terms-of-service");

        when(acmeManager.getTermsOfService()).thenReturn(Optional.of(new URI("http://example.com")));

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_TERMS_OF_SERVICE_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        URI uri = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("http://example.com", uri.toString());

        verify(acmeManager).getTermsOfService();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-terms-of-service");
    }

    @Test
    @WithMockUser
    public void getTermsOfServiceNullResult()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-terms-of-service");

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_TERMS_OF_SERVICE_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(acmeManager).getTermsOfService();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-terms-of-service");
    }

    @Test
    @WithMockUser
    public void getTermsOfServiceNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_TERMS_OF_SERVICE_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).getTermsOfService();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-terms-of-service");
    }

    @Test
    @WithMockUser
    public void setTosAccepted()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "set-tos-accepted");

        mockMvc.perform(post(RestPaths.ACME_SET_TOS_ACCEPTED_PATH)
                        .param("accepted", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).setTosAccepted(true);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "set-tos-accepted");
    }

    @Test
    @WithMockUser
    public void setTosAcceptedNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.ACME_SET_TOS_ACCEPTED_PATH)
                        .param("accepted", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).setTosAccepted(anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "set-tos-accepted");
    }

    @Test
    @WithMockUser
    public void getTosAccepted()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-tos-accepted");

        when(acmeManager.isTosAccepted()).thenReturn(true);

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_TOS_ACCEPTED_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(acmeManager).isTosAccepted();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-tos-accepted");
    }

    @Test
    @WithMockUser
    public void getTosAcceptedNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_TOS_ACCEPTED_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).isTosAccepted();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "get-tos-accepted");
    }

    @Test
    @WithMockUser
    public void findOrRegisterAccount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "find-or-register-account");

        when(acmeManager.findOrRegisterAccount()).thenReturn(new MockAccount());

        MvcResult result = mockMvc.perform(post(RestPaths.ACME_FIND_OR_REGISTER_ACCOUNT_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        AcmeDTO.Account account = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(AcmeDTO.AccountStatus.VALID, account.status());
        assertEquals(new URL("https://example.com"), account.location());
        assertEquals(new URI("http://contact.example.com"), account.contacts().get(0));
        assertTrue(account.tosAccepted());

        verify(acmeManager).findOrRegisterAccount();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "find-or-register-account");
    }

    @Test
    @WithMockUser
    public void findOrRegisterAccountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.ACME_FIND_OR_REGISTER_ACCOUNT_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).findOrRegisterAccount();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "find-or-register-account");
    }

    @Test
    @WithMockUser
    public void deactivateAccount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "deactivate-account");

        mockMvc.perform(post(RestPaths.ACME_DEACTIVATE_ACCOUNT_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).deactivateAccount();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "deactivate-account");
    }

    @Test
    @WithMockUser
    public void deactivateAccountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.ACME_DEACTIVATE_ACCOUNT_PATH)
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).deactivateAccount();

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "deactivate-account");
    }

    @Test
    @WithMockUser
    public void orderCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "order-certificate");

        List<String> request = List.of("test1@example.com", "test2@example.com");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        mockMvc.perform(post(RestPaths.ACME_ORDER_CERTIFICATE_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).orderCertificate(TLSKeyPairBuilder.Algorithm.SECP256R1, request);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "order-certificate");
    }

    @Test
    @WithMockUser
    public void orderCertificateNoPermission()
    throws Exception
    {
        List<String> request = List.of("test1@example.com", "test2@example.com");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, request);

        MvcResult result = mockMvc.perform(post(RestPaths.ACME_ORDER_CERTIFICATE_PATH)
                        .param("keyAlgorithm", TLSDTO.KeyAlgorithm.SECP256R1.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(acmeManager, never()).orderCertificate(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "order-certificate");
    }

    @Test
    @WithMockUser
    public void renewCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.ACME,
                "renew-certificate");

        mockMvc.perform(post(RestPaths.ACME_RENEW_CERTIFICATE_PATH)
                        .param("forceRenewal", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(acmeManager).renewCertificate(true);

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "renew-certificate");
    }

    @Test
    @WithMockUser
    public void renewCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.ACME_RENEW_CERTIFICATE_PATH)
                        .param("forceRenewal", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.ACME,
                "renew-certificate");
    }

    /*
     * Note1: getAuthorization should be accessible without authentication and without requiring any permission
     * Note2: unit testing with spring boot requires a mock user
     */
    @Test
    @WithMockUser
    public void getAuthorization()
    throws Exception
    {
        when(acmeManager.getChallenge("test-token")).thenReturn(new AcmeTokenCache.Challenge ("test-auth",
                System.currentTimeMillis()));

        MvcResult result = mockMvc.perform(get(RestPaths.ACME_GET_AUTHORIZATION_PATH, "test-token")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("test-auth", result.getResponse().getContentAsString());

        verify(acmeManager).getChallenge("test-token");

        verify(permissionChecker, never()).checkPermission(any(), any());
    }
}
