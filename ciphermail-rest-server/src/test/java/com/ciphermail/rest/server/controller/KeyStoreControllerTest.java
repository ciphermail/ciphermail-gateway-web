/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.dlp.PolicyPatternManager;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.KeyStoreDTO;
import com.ciphermail.rest.core.KeyStoreName;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.service.X509CertificateDetailsFactory;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.function.Failable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockPart;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.Enumeration;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = KeyStoreController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        X509CertificateDetailsFactory.class})
public class KeyStoreControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @Autowired
    private ValidatorRegistry validatorRegistry;

    @Autowired
    @Qualifier("keyStoreProvider")
    private KeyStoreProvider keyStoreProvider;

    @Autowired
    @Qualifier("pgpKeyStoreProvider")
    private KeyStoreProvider pgpKeyStoreProvider;

    @Autowired
    @Qualifier("dkimKeyStoreProvider")
    private KeyStoreProvider dkimKeyStoreProvider;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // clean key stores
            keyStoreProvider.getKeyStore().aliases().asIterator().forEachRemaining(
                    a -> Failable.run(() -> keyStoreProvider.getKeyStore().deleteEntry(a)));

            pgpKeyStoreProvider.getKeyStore().aliases().asIterator().forEachRemaining(
                    a -> Failable.run(() -> pgpKeyStoreProvider.getKeyStore().deleteEntry(a)));

            dkimKeyStoreProvider.getKeyStore().aliases().asIterator().forEachRemaining(
                    a -> Failable.run(() -> dkimKeyStoreProvider.getKeyStore().deleteEntry(a)));
        }));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private void importPKCS12(KeyStore keyStore, File p12File, String password)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Failable.run(() ->
            {
                KeyStore p12KeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                p12KeyStore.load(new FileInputStream(p12File), password.toCharArray());

                Enumeration<String> aliases = p12KeyStore.aliases();

                while (aliases.hasMoreElements())
                {
                    String alias = aliases.nextElement();

                    if (p12KeyStore.isCertificateEntry(alias)) {
                        keyStore.setCertificateEntry(alias, p12KeyStore.getCertificate(alias));
                    }
                    else if (p12KeyStore.isKeyEntry(alias))
                    {
                        keyStore.setKeyEntry(alias, p12KeyStore.getKey(alias, password.toCharArray()),
                                password.toCharArray(), p12KeyStore.getCertificateChain(alias));
                    }
                }
            });
        });
    }

    @Test
    @WithMockUser
    public void getAliases()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-aliases");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_ALIASES_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(22, response.size());

        importPKCS12(dkimKeyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_ALIASES_PATH)
                        .param("keyStoreName", KeyStoreName.DKIM.name())
                        .param("firstResult", "10")
                        .param("maxResults", "100"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(12, response.size());

        importPKCS12(pgpKeyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_ALIASES_PATH)
                        .param("keyStoreName", KeyStoreName.PGP.name()))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(22, response.size());

        verify(permissionChecker, times(3)).checkPermission(PermissionCategory.KEYSTORE,
                "get-aliases");
    }

    @Test
    @WithMockUser
    public void getAliasesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_ALIASES_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("firstResult", "0")
                        .param("maxResults", "100"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-aliases");
    }

    @Test
    @WithMockUser
    public void getKeyStoreSize()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-keystore-size");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name()))
                .andExpect(status().isOk())
                .andReturn();

        Integer response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(22, (int) response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-keystore-size");
    }

    @Test
    @WithMockUser
    public void getKeyStoreSizeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-keystore-size");
    }

    @Test
    @WithMockUser
    public void getCertificate()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_CERTIFICATE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        KeyStoreDTO.CertificateDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("X.509", response.type());
        assertEquals("ValidCertificate", response.x509CertificateDetails().keyAlias());
        assertTrue(response.x509CertificateDetails().privateKeyAccessible());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void getCertificateNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_CERTIFICATE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate");
    }

    @Test
    @WithMockUser
    public void getCertificateChain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate-chain");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_CERTIFICATE_CHAIN_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        List<KeyStoreDTO.CertificateDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate-chain");
    }

    @Test
    @WithMockUser
    public void getCertificateChainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_CERTIFICATE_CHAIN_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-certificate-chain");
    }

    @Test
    @WithMockUser
    public void containsAlias()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "contains-alias");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_CONTAINS_ALIAS_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        Boolean response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "contains-alias");
    }

    @Test
    @WithMockUser
    public void containsAliasNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_CONTAINS_ALIAS_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "contains-alias");
    }

    @Test
    @WithMockUser
    public void isCertificateEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-certificate-entry");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_CERTIFICATE_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "root"))
                .andExpect(status().isOk())
                .andReturn();

        Boolean response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-certificate-entry");
    }

    @Test
    @WithMockUser
    public void isCertificateEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_CERTIFICATE_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "root"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-certificate-entry");
    }

    @Test
    @WithMockUser
    public void isKeyEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-key-entry");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        Boolean response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-key-entry");
    }

    @Test
    @WithMockUser
    public void isKeyEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-key-entry");
    }

    @Test
    @WithMockUser
    public void deleteEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-key-entry");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "delete-entry");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        Boolean response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        mockMvc.perform(post(RestPaths.KEYSTORE_DELETE_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate")
                        .with(csrf()))
                .andExpect(status().isOk());

        result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertFalse(response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "delete-entry");
    }

    @Test
    @WithMockUser
    public void deleteEntryNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.KEYSTORE_DELETE_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "delete-entry");
    }

    @Test
    @WithMockUser
    public void renameAlias()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "is-key-entry");
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "rename-alias");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        Boolean response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        KeyStoreDTO.RenameEntryRequestBody requestBody = new KeyStoreDTO.RenameEntryRequestBody("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, requestBody);

        mockMvc.perform(post(RestPaths.KEYSTORE_RENAME_ALIAS_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("oldAlias", "ValidCertificate")
                        .param("newAlias", "NewAlias")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "ValidCertificate"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertFalse(response);

        result = mockMvc.perform(get(RestPaths.KEYSTORE_IS_KEY_ENTRY_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("alias", "NewAlias"))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertTrue(response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "rename-alias");
    }

    @Test
    @WithMockUser
    public void renameAliasNoPermission()
    throws Exception
    {
        KeyStoreDTO.RenameEntryRequestBody requestBody = new KeyStoreDTO.RenameEntryRequestBody("test");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, requestBody);

        MvcResult result = mockMvc.perform(post(RestPaths.KEYSTORE_RENAME_ALIAS_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .param("oldAlias", "ValidCertificate")
                        .param("newAlias", "NewAlias")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "rename-alias");
    }

    @Test
    @WithMockUser
    public void exportToPKCS12()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "export-to-pkcs12");

        importPKCS12(keyStoreProvider.getKeyStore(), new File(TEST_BASE_CORE, "keys/testCertificates.p12"),
                "test");

        KeyStoreDTO.ExportToPKCS12RequestBody requestBody = new KeyStoreDTO.ExportToPKCS12RequestBody(
                "test", "export-password", List.of("ValidCertificate", "ca"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, requestBody);

        MvcResult result = mockMvc.perform(post(RestPaths.KEYSTORE_EXPORT_TO_PKCS12_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] response = result.getResponse().getContentAsByteArray();

        KeyStore exportedKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        exportedKeyStore.load(new ByteArrayInputStream(response), "export-password".toCharArray());

        assertEquals(2, exportedKeyStore.size());
        assertTrue(exportedKeyStore.isKeyEntry("ValidCertificate"));
        assertTrue(exportedKeyStore.isCertificateEntry("ca"));

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "export-to-pkcs12");
    }

    @Test
    @WithMockUser
    public void exportToPKCS12NoPermission()
    throws Exception
    {
        KeyStoreDTO.ExportToPKCS12RequestBody requestBody = new KeyStoreDTO.ExportToPKCS12RequestBody(
                "test", "export-password", List.of("ValidCertificate", "ca"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, requestBody);

        MvcResult result = mockMvc.perform(post(RestPaths.KEYSTORE_EXPORT_TO_PKCS12_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "export-to-pkcs12");
    }

    @Test
    @WithMockUser
    public void importPKCS12()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "import-pkcs12");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "get-keystore-size");

        MvcResult result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name()))
                .andExpect(status().isOk())
                .andReturn();

        Integer response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, (int) response);

        result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.KEYSTORE_IMPORT_PKCS12_PATH)
                        .file("pkcs12", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(22, (int) response);

        result = mockMvc.perform(get(RestPaths.KEYSTORE_GET_KEYSTORE_SIZE_PATH)
                        .param("keyStoreName", KeyStoreName.CERT.name()))
                .andExpect(status().isOk())
                .andReturn();

        response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(22, (int) response);

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "import-pkcs12");
    }

    @Test
    @WithMockUser
    public void importPKCS12NoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.KEYSTORE_IMPORT_PKCS12_PATH)
                        .file("pkcs12", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "keys/testCertificates.p12")))
                        .part(new MockPart("keyStorePassword", "test".getBytes(StandardCharsets.UTF_8)))
                        .param("keyStoreName", KeyStoreName.CERT.name())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.KEYSTORE,
                "import-pkcs12");
    }
}