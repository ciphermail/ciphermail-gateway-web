/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.logging.JournalCtl;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.LogDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = LogController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class LogControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private JournalCtl journalCtl;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void getLogLinesCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log-count");

        when(journalCtl.getLog(any(), any(), anyBoolean(), any(), any(), any(), any())).thenReturn(
                """
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test1"}
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test2"}
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test3"}
                      """);

        MvcResult result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_COUNT_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(3, Long.parseLong(result.getResponse().getContentAsString()));

        verify(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log-count");
    }

    @Test
    @WithMockUser
    public void getLogLinesCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_COUNT_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log-count");
    }

    @Test
    @WithMockUser
    public void getLogLines()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log");

        when(journalCtl.getLog(any(), any(), anyBoolean(), any(), any(), any(), any())).thenReturn(
                """
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test1"}
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test2"}
                      {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test3"}
                      """);

        MvcResult result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(
                """
                [
                {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test1"},{"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test2"},{"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test3"}
                ]""",
                result.getResponse().getContentAsString());

        verify(journalCtl).getLog(any(), any(), anyBoolean(), any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log");

        result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name())
                        .param("firstResult", "1"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(
                """
                [
                {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test2"},{"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test3"}
                ]""",
                result.getResponse().getContentAsString());

        result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name())
                        .param("firstResult", "1")
                        .param("maxResults", "1"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(
                """
                [
                {"SYSLOG_IDENTIFIER":"systemd","MESSAGE":"test2"}
                ]""",
                result.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void getLogLinesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.LOG_GET_LOG_LINES_PATH)
                        .param("logTarget", LogDTO.LogTarget.MTA.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(journalCtl, never()).getLog(any(), any(), anyBoolean(), any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.LOG,
                "read-log");
    }
}