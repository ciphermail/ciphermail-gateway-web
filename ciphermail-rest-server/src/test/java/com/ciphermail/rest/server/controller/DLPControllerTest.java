/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.dlp.PolicyPatternManager;
import com.ciphermail.core.app.dlp.PolicyPatternNode;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManager;
import com.ciphermail.core.app.impl.MockupUser;
import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.DLPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.function.Failable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext
@WebMvcTest(value = DLPController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class DLPControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @MockBean
    private UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;

    @MockBean
    private UserWorkflow userWorkflow;

    @MockBean
    private DomainManager domainManager;

    @MockBean
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private ValidatorRegistry validatorRegistry;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    private static class MockPolicyPatternNode implements PolicyPatternNode
    {
        private String name;
        private PolicyPattern policyPattern;

        MockPolicyPatternNode(String name) {
            this.name = name;
        }

        @Override
        public void setName(@Nonnull String name) {
            this.name = name;
        }

        @Nonnull
        @Override
        public String getName() {
            return name;
        }

        @Override
        public PolicyPattern getPolicyPattern() {
            return policyPattern;
        }

        @Override
        public void setPolicyPattern(@Nonnull PolicyPattern policyPattern) {
            this.policyPattern = policyPattern;
        }

        @Nonnull
        @Override
        public Set<PolicyPatternNode> getChilds() {
            return Collections.emptySet();
        }
    }

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices {
        // empty on purpose
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status -> Failable.run(() ->
        {
            // remove children before we can delete DLP patterns
            policyPatternManager.getPatterns(null, null).forEach(
                    pn -> Failable.run(() -> policyPatternManager.getPattern(pn.getName()).getChilds().clear()));
            // delete DLP patterns
            policyPatternManager.getPatterns(null, null).forEach(
                    pn -> Failable.run(() -> policyPatternManager.deletePattern(pn.getName())));
        }));

        validatorRegistry.addValidator(new Validator() {
            @Override
            public String getName() {
                return "dummy";
            }

            @Override
            public String getDescription() {
                return null;
            }

            @Override
            public boolean isValid(String input) {
                return true;
            }
        });

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    private MvcResult addPolicyPattern(String name)
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-pattern");

        return mockMvc.perform(post(RestPaths.DLP_ADD_POLICY_PATTERN_PATH)
                        .param("name", name)
                        .param("description", "test description")
                        .param("notes", "test notes")
                        .param("regEx", ".*")
                        .param("validator", "dummy")
                        .param("threshold", "2")
                        .param("action", PolicyViolationAction.QUARANTINE.name())
                        .param("delayEvaluation", "true")
                        .param("matchFilter", "Mask")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();
    }

    private int getPatternCount()
    {
        return Optional.ofNullable(transactionOperations.execute(tx ->
                policyPatternManager.getPatterns(null, null).size())).orElse(0);
    }

    @Test
    @WithMockUser
    public void testAddPolicyPattern()
    throws Exception
    {
        MvcResult result = addPolicyPattern("test");

        DLPDTO.DLPPolicyPattern response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", response.name());
        assertEquals("test description", response.description());
        assertEquals("test notes", response.notes());
        assertEquals(".*", response.regEx());
        assertEquals(2, response.threshold());
        assertEquals(DLPDTO.PolicyViolationAction.QUARANTINE, response.action());
        assertTrue(response.delayEvaluation());
        assertEquals("Mask", response.matchFilter());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testAddPolicyPatternNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_ADD_POLICY_PATTERN_PATH)
                        .param("name", "test")
                        .param("description", "test description")
                        .param("notes", "test notes")
                        .param("regEx", ".*")
                        .param("threshold", "2")
                        .param("action", PolicyViolationAction.QUARANTINE.name())
                        .param("delayEvaluation", "true")
                        .param("matchFilter", "Mask")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testAddPolicyGroup()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-group");

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_ADD_POLICY_GROUP_PATH)
                        .param("name", "test-group")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPPolicyPattern response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test-group", response.name());
        assertEquals("", response.description());
        assertEquals("", response.notes());
        assertEquals("", response.regEx());
        assertEquals(0, response.threshold());
        assertNull(response.action());
        assertFalse(response.delayEvaluation());
        assertEquals("", response.matchFilter());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-group");
    }

    @Test
    @WithMockUser
    public void testAddPolicyGroupNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_ADD_POLICY_GROUP_PATH)
                        .param("name", "test-group")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-policy-group");
    }

    @Test
    @WithMockUser
    public void testUpdatePolicyPattern()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "update-policy-pattern");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_UPDATE_POLICY_PATTERN_PATH)
                        .param("name", "test")
                        .param("description", "test2 description")
                        .param("notes", "test2 notes")
                        .param("regEx", "test")
                        .param("threshold", "3")
                        .param("action", PolicyViolationAction.WARN.name())
                        .param("delayEvaluation", "false")
                        .param("matchFilter", "")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPPolicyPattern response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", response.name());
        assertEquals("test2 description", response.description());
        assertEquals("test2 notes", response.notes());
        assertEquals("test", response.regEx());
        assertEquals(3, response.threshold());
        assertEquals(DLPDTO.PolicyViolationAction.WARN, response.action());
        assertFalse(response.delayEvaluation());
        assertEquals("", response.matchFilter());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "update-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testUpdatePolicyPatternNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_UPDATE_POLICY_PATTERN_PATH)
                        .param("name", "test")
                        .param("description", "test2 description")
                        .param("notes", "test2 notes")
                        .param("regEx", "test")
                        .param("threshold", "3")
                        .param("action", PolicyViolationAction.WARN.name())
                        .param("delayEvaluation", "false")
                        .param("matchFilter", "")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "update-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPattern()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-pattern");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERN_PATH)
                        .param("name", "test"))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPPolicyPattern response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", response.name());
        assertEquals("test description", response.description());
        assertEquals("test notes", response.notes());
        assertEquals(".*", response.regEx());
        assertEquals(2, response.threshold());
        assertEquals(DLPDTO.PolicyViolationAction.QUARANTINE, response.action());
        assertTrue(response.delayEvaluation());
        assertEquals("Mask", response.matchFilter());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERN_PATH)
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternsByName()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-by-name");

        addPolicyPattern("test1");
        addPolicyPattern("test2");

        assertEquals(2, getPatternCount());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1", "test2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_GET_POLICY_PATTERNS_BY_NAME_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPPolicyPattern> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-by-name");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternsByNameNoPermission()
    throws Exception
    {
        addPolicyPattern("test1");
        addPolicyPattern("test2");

        assertEquals(2, getPatternCount());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1", "test2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_GET_POLICY_PATTERNS_BY_NAME_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-by-name");
    }

    @Test
    @WithMockUser
    public void testDeletePolicyPattern()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-pattern");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_DELETE_POLICY_PATTERN_PATH)
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPPolicyPattern response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testDeletePolicyPatternNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_DELETE_POLICY_PATTERN_PATH)
                        .param("name", "test")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testDeletePolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-patterns");

        addPolicyPattern("test1");
        addPolicyPattern("test2");

        assertEquals(2, getPatternCount());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1", "test2", "test3"));

        mockMvc.perform(post(RestPaths.DLP_DELETE_POLICY_PATTERNS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(0, getPatternCount());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-patterns");
    }

    @Test
    @WithMockUser
    public void testDeletePolicyPatternsNoPermission()
    throws Exception
    {
        addPolicyPattern("test1");
        addPolicyPattern("test2");

        assertEquals(2, getPatternCount());

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1", "test2", "test3"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_DELETE_POLICY_PATTERNS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertEquals(2, getPatternCount());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "delete-policy-patterns");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERNS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10"))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPPolicyPattern> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERNS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternsCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-count");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERNS_COUNT_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("1", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-count");
    }

    @Test
    @WithMockUser
    public void testGetPolicyPatternsCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_POLICY_PATTERNS_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-policy-patterns-count");
    }

    @Test
    @WithMockUser
    public void testRenamePolicyPattern()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "rename-policy-pattern");

        addPolicyPattern("test");

        mockMvc.perform(post(RestPaths.DLP_RENAME_POLICY_PATTERN_PATH)
                        .param("oldName", "test")
                        .param("newName", "test2")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "rename-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testRenamePolicyPatternNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_RENAME_POLICY_PATTERN_PATH)
                        .param("oldName", "test")
                        .param("newName", "test2")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "rename-policy-pattern");
    }

    @Test
    @WithMockUser
    public void testIsReferenced()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "is-referenced");

        addPolicyPattern("test");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_IS_REFERENCED_PATH)
                        .param("name", "test"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("false", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "is-referenced");
    }

    @Test
    @WithMockUser
    public void testIsReferencedNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_IS_REFERENCED_PATH)
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "is-referenced");
    }

    @Test
    @WithMockUser
    public void testGetReferencedDetails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-referenced-details");

        testAddChildNode();

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_REFERENCED_DETAILS_PATH)
                        .param("name", "child"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());
        assertEquals("pattern,group,parent", response.get(0));

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-referenced-details");
    }

    @Test
    @WithMockUser
    public void testGetReferencedDetailsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_REFERENCED_DETAILS_PATH)
                        .param("name", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-referenced-details");
    }

    @Test
    @WithMockUser
    public void testSetChildNodes()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-child-nodes");

        addPolicyPattern("parent");
        addPolicyPattern("child1");
        addPolicyPattern("child2");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("child1", "child2"));

        mockMvc.perform(post(RestPaths.DLP_SET_CHILD_NODES_PATH)
                        .param("parentNode", "parent")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-child-nodes");
    }

    @Test
    @WithMockUser
    public void testSetChildNodesDoNotAddSelf()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-child-nodes");

        addPolicyPattern("parent");
        addPolicyPattern("child1");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("child1", "parent"));

        mockMvc.perform(post(RestPaths.DLP_SET_CHILD_NODES_PATH)
                        .param("parentNode", "parent")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("The child node cannot be the same as the parent node"));

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-child-nodes");
    }

    @Test
    @WithMockUser
    public void testSetChildNodesNoPermission()
    throws Exception
    {
        addPolicyPattern("parent");
        addPolicyPattern("child1");
        addPolicyPattern("child2");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("child1", "child2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_SET_CHILD_NODES_PATH)
                        .param("parentNode", "parent")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-child-nodes");
    }

    @Test
    @WithMockUser
    public void testAddChildNode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-child-node");

        addPolicyPattern("parent");
        addPolicyPattern("child");

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_ADD_CHILD_NODE_PATH)
                        .param("parentNode", "parent")
                        .param("childNode", "child")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-child-node");
    }

    @Test
    @WithMockUser
    public void testAddChildNodeDoNotAddSelf()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-child-node");

        addPolicyPattern("parent");

        mockMvc.perform(post(RestPaths.DLP_ADD_CHILD_NODE_PATH)
                        .param("parentNode", "parent")
                        .param("childNode", "parent")
                        .with(csrf()))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("The child node cannot be the same as the parent node"));

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-child-node");
    }

    @Test
    @WithMockUser
    public void testAddChildNodeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_ADD_CHILD_NODE_PATH)
                        .param("parentNode", "parent")
                        .param("childNode", "child")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "add-child-node");
    }

    @Test
    @WithMockUser
    public void testRemoveChildNode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "remove-child-node");

        testAddChildNode();

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_REMOVE_CHILD_NODE_PATH)
                        .param("parentNode", "parent")
                        .param("childNode", "child")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "remove-child-node");
    }

    @Test
    @WithMockUser
    public void testRemoveChildNodeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.DLP_REMOVE_CHILD_NODE_PATH)
                        .param("parentNode", "parent")
                        .param("childNode", "child")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "remove-child-node");
    }

    @Test
    @WithMockUser
    public void testExtractTextFromMimeMessage()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "extract-text-from-email");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DLP_EXTRACT_TEXT_FROM_EMAIL_PATH)
                        .file("mime", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "mail/simple-text-message.eml")))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("content-type: text/plain subject: test simple message to: test@example.com " +
                     "test detected-mime-type: text/plain detected-mime-type: application/octet-stream ",
                result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "extract-text-from-email");
    }

    @Test
    @WithMockUser
    public void testExtractTextFromMimeMessageNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DLP_EXTRACT_TEXT_FROM_EMAIL_PATH)
                        .file("mime", FileUtils.readFileToByteArray(new File(TEST_BASE_CORE,
                                "mail/simple-text-message.eml")))
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "extract-text-from-email");
    }

    @Test
    @WithMockUser
    public void testGetMatchFilters()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filters");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_MATCH_FILTERS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPMatchFilter> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filters");
    }

    @Test
    @WithMockUser
    public void testGetMatchFiltersNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_MATCH_FILTERS_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filters");
    }

    @Test
    @WithMockUser
    public void testGetMatchFilter()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filter");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_MATCH_FILTER_PATH)
                        .param("name", "Mask"))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPMatchFilter response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("Mask", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filter");
    }

    @Test
    @WithMockUser
    public void testGetMatchFilterNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_MATCH_FILTER_PATH)
                        .param("name", "Mask"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-match-filter");
    }

    @Test
    @WithMockUser
    public void testGetValidators()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validators");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_VALIDATORS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPValidator> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validators");
    }

    @Test
    @WithMockUser
    public void testGetValidatorsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_VALIDATORS_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validators");
    }

    @Test
    @WithMockUser
    public void testGetValidator()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validator");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_VALIDATOR_PATH)
                        .param("name", "dummy"))
                .andExpect(status().isOk())
                .andReturn();

        DLPDTO.DLPValidator response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("dummy", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validator");
    }

    @Test
    @WithMockUser
    public void testGetValidatorNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_VALIDATOR_PATH)
                        .param("name", "dummy"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-validator");
    }

    @Test
    @WithMockUser
    public void testGetSkipList()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-skip-list");

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_SKIP_LIST_PATH))
                .andExpect(status().isOk())
                .andReturn();

        assertNotNull(result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-skip-list");
    }

    @Test
    @WithMockUser
    public void testGetSkipListNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_SKIP_LIST_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-skip-list");
    }

    @Test
    @WithMockUser
    public void testSetSkipList()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-skip-list");

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DLP_SET_SKIP_LIST_PATH)
                        .file("skipList", "!@#$%^&*(".getBytes(StandardCharsets.UTF_8))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-skip-list");
    }

    @Test
    @WithMockUser
    public void testSetSkipListNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.DLP_SET_SKIP_LIST_PATH)
                        .file("skipList", "test 123".getBytes(StandardCharsets.UTF_8))
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-skip-list");
    }

    @Test
    @WithMockUser
    public void setUserPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-user-policy-patterns");

        when(userWorkflow.getUser("test@example.com", UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST))
                .thenReturn(new MockupUser("test@example.com", null, null));

        addPolicyPattern("p1");
        addPolicyPattern("p2");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        mockMvc.perform(post(RestPaths.DLP_SET_USER_POLICY_PATTERNS_PATH)
                        .param("email", "test@example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(userPreferencesPolicyPatternManager).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-user-policy-patterns");
    }

    @Test
    @WithMockUser
    public void setUserPolicyPatternsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_SET_USER_POLICY_PATTERNS_PATH)
                        .param("email", "test@example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-user-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getUserPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-user-policy-patterns");

        when(userWorkflow.getUser("test@example.com", UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                .thenReturn(new MockupUser("test@example.com", null, null));

        when(userPreferencesPolicyPatternManager.getPatterns(any())).thenReturn(Set.of(
                new MockPolicyPatternNode("p1"),
                new MockPolicyPatternNode("p2")));

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_USER_POLICY_PATTERNS_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPPolicyPattern> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        response.sort(Comparator.comparing(DLPDTO.DLPPolicyPattern::name));

        assertEquals(2, response.size());
        assertEquals("p1", response.get(0).name());
        assertEquals("p2", response.get(1).name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-user-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getUserPolicyPatternsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_USER_POLICY_PATTERNS_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).getPatterns(any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-user-policy-patterns");
    }

    @Test
    @WithMockUser
    public void setDomainPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-domain-policy-patterns");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences(
                "example.com",
                UserPreferencesCategory.DOMAIN.toString()));

        addPolicyPattern("p1");
        addPolicyPattern("p2");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        mockMvc.perform(post(RestPaths.DLP_SET_DOMAIN_POLICY_PATTERNS_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(userPreferencesPolicyPatternManager).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-domain-policy-patterns");
    }

    @Test
    @WithMockUser
    public void setDomainPolicyPatternsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_SET_DOMAIN_POLICY_PATTERNS_PATH)
                        .param("domain", "example.com")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-domain-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getDomainPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-domain-policy-patterns");

        when(domainManager.getDomainPreferences("example.com")).thenReturn(new MockupUserPreferences(
                "example.com",
                UserPreferencesCategory.DOMAIN.toString()));

        when(userPreferencesPolicyPatternManager.getPatterns(any())).thenReturn(Set.of(
                new MockPolicyPatternNode("p1"),
                new MockPolicyPatternNode("p2")));

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_DOMAIN_POLICY_PATTERNS_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPPolicyPattern> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        response.sort(Comparator.comparing(DLPDTO.DLPPolicyPattern::name));

        assertEquals(2, response.size());
        assertEquals("p1", response.get(0).name());
        assertEquals("p2", response.get(1).name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-domain-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getDomainPolicyPatternsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_DOMAIN_POLICY_PATTERNS_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).getPatterns(any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-domain-policy-patterns");
    }

    @Test
    @WithMockUser
    public void setGlobalPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-global-policy-patterns");

        when(globalPreferencesManager.getGlobalUserPreferences()).thenReturn(new MockupUserPreferences(
                "test",
                UserPreferencesCategory.GLOBAL.toString()));

        addPolicyPattern("p1");
        addPolicyPattern("p2");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        mockMvc.perform(post(RestPaths.DLP_SET_GLOBAL_POLICY_PATTERNS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(userPreferencesPolicyPatternManager).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-global-policy-patterns");
    }

    @Test
    @WithMockUser
    public void setGlobalPolicyPatternsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("p1", "p2"));

        MvcResult result = mockMvc.perform(post(RestPaths.DLP_SET_GLOBAL_POLICY_PATTERNS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).setPatterns(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "set-global-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getGlobalPolicyPatterns()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-global-policy-patterns");

        when(globalPreferencesManager.getGlobalUserPreferences()).thenReturn(new MockupUserPreferences(
                "test",
                UserPreferencesCategory.GLOBAL.toString()));

        when(userPreferencesPolicyPatternManager.getPatterns(any())).thenReturn(Set.of(
                new MockPolicyPatternNode("p1"),
                new MockPolicyPatternNode("p2")));

        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_GLOBAL_POLICY_PATTERNS_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<DLPDTO.DLPPolicyPattern> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        response.sort(Comparator.comparing(DLPDTO.DLPPolicyPattern::name));

        assertEquals(2, response.size());
        assertEquals("p1", response.get(0).name());
        assertEquals("p2", response.get(1).name());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-global-policy-patterns");
    }

    @Test
    @WithMockUser
    public void getGlobalPolicyPatternsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.DLP_GET_GLOBAL_POLICY_PATTERNS_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userPreferencesPolicyPatternManager, never()).getPatterns(any());

        verify(permissionChecker).checkPermission(PermissionCategory.DLP,
                "get-global-policy-patterns");
    }
}