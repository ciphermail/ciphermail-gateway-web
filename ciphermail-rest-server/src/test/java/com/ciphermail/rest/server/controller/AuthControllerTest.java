/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.admin.Admin;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.core.app.admin.Role;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.otp.TOTP;
import com.ciphermail.core.common.security.otp.TOTPImpl;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.ImageUtils;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.authorization.Permissions;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestSystemServices;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class})
public class AuthControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @MockBean
    private Permissions permissions;

    @MockBean
    private RoleManager roleManager;

    @MockBean
    private AdminManager adminManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    /*
     * This is required to make test beans available
     */
    @TestConfiguration
    public static class LocalServices extends RestSystemServices
    {
        @Bean
        public PasswordEncoder createPasswordEncoder() {
            return Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8();
        }

        @Bean
        public TOTP createTOTPService() {
            return new TOTPImpl();
        }
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @Test
    @WithMockUser
    public void testGetAvailablePermissions()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-available-permissions");

        when(permissions.getPermissions()).thenReturn(Set.of("ca:create-ca", "property:set:user:sms-send-enabled"));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_AVAILABLE_PERMISSIONS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());
        assertTrue(response.contains("ca:create-ca"));
        assertTrue(response.contains("property:set:user:sms-send-enabled"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-available-permissions");
    }

    @Test
    @WithMockUser
    public void testGetAvailablePermissionsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_AVAILABLE_PERMISSIONS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissions, never()).getPermissions();

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-available-permissions");
    }

    @Test
    @WithMockUser
    public void testGetRoles()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-roles");

        when(roleManager.getRoles(any(), any(), any())).thenReturn(List.of(new Role("test1"),
                new Role("test2")));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ROLES_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<AuthDTO.RoleDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-roles");
    }

    @Test
    @WithMockUser
    public void testGetRolesNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ROLES_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRoles(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-roles");
    }

    @Test
    @WithMockUser
    public void testGetRole()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-role");

        when(roleManager.getRole("test")).thenReturn(new Role("test"));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ROLE_PATH)
                        .param("role", "test")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        AuthDTO.RoleDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-role");
    }

    @Test
    @WithMockUser
    public void testGetRoleNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ROLE_PATH)
                        .param("role", "test")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-role");
    }

    @Test
    @WithMockUser
    public void testGetAdmins()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-admins");

        when(adminManager.getAdmins(any(), any(), any())).thenReturn(List.of(new Admin("admin1"),
                new Admin("admin2")));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ADMINS_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<AuthDTO.AdminDetails> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());

       verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-admins");
    }

    @Test
    @WithMockUser
    public void testGetAdminsNoPermissions()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ADMINS_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).getAdmins(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-admins");
    }

    @Test
    @WithMockUser
    public void testGetAdmin()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-admin");

        when(adminManager.getAdmin("admin")).thenReturn(new Admin("admin"));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ADMIN_PATH)
                        .param("admin", "admin")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        AuthDTO.AdminDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("admin", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-admin");
    }

    @Test
    @WithMockUser
    public void testGetAdminNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_ADMIN_PATH)
                        .param("admin", "admin")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).getAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-admin");
    }

    @Test
    @WithMockUser
    public void testAddAdmin()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "add-admin");

        Admin admin = new Admin("admin");

        when(adminManager.createAdmin("admin")).thenReturn(admin);
        when(adminManager.persistAdmin(any())).thenAnswer(i -> i.getArgument(0));

        mockMvc.perform(post(RestPaths.AUTH_ADD_ADMIN_PATH)
                        .param("admin", "admin")
                        .param("authenticationType", AuthenticationType.OIDC.name())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals(AuthenticationType.OIDC, admin.getAuthenticationType());
        verify(adminManager).createAdmin("admin");
        verify(adminManager).persistAdmin(admin);

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "add-admin");
    }

    @Test
    @WithMockUser
    public void testAddAdminNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_ADD_ADMIN_PATH)
                        .param("admin", "admin")
                        .param("authenticationType", AuthenticationType.OIDC.name())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).createAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "add-admin");
    }

    @Test
    @WithMockUser
    public void testDeleteAdmin()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "delete-admin");

        when(adminManager.getAdmin("admin")).thenReturn(new Admin("admin"));
        when(adminManager.deleteAdmin("admin")).thenReturn(true);

        mockMvc.perform(post(RestPaths.AUTH_DELETE_ADMIN_PATH)
                        .param("admin", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(adminManager).deleteAdmin("admin");

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "delete-admin");
    }

    @Test
    @WithMockUser
    public void testDeleteAdminNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_DELETE_ADMIN_PATH)
                        .param("admin", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).deleteAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "delete-admin");
    }

    @Test
    @WithMockUser
    public void testCreateRole()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "create-role");

        when(roleManager.createRole("admin")).thenReturn(new Role("admin"));
        when(roleManager.persistRole(any())).thenAnswer(i -> i.getArgument(0, Role.class));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_CREATE_ROLE_PATH)
                        .param("role", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        AuthDTO.RoleDetails response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("admin", response.name());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "create-role");
    }

    @Test
    @WithMockUser
    public void testCreateRoleNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_CREATE_ROLE_PATH)
                        .param("role", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).createRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "create-role");
    }

    @Test
    @WithMockUser
    public void testDeleteRole()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "delete-role");

        Role role = new Role("admin");

        when(roleManager.getRole("admin")).thenReturn(role);

        mockMvc.perform(post(RestPaths.AUTH_DELETE_ROLE_PATH)
                        .param("role", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(roleManager).deleteRole(role.getName());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "delete-role");
    }

    @Test
    @WithMockUser
    public void testDeleteRoleNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_DELETE_ROLE_PATH)
                        .param("role", "admin")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "delete-role");
    }

    @Test
    @WithMockUser
    public void testAddRolePermissions()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "add-role-permissions");

        Role role = new Role("admin");

        when(roleManager.getRole("admin")).thenReturn(role);
        when(permissions.isPermission(any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        mockMvc.perform(post(RestPaths.AUTH_ADD_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(role.getPermissions().contains("perm1"));
        assertTrue(role.getPermissions().contains("perm2"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "add-role-permissions");
    }

    @Test
    @WithMockUser
    public void testAddRolePermissionsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_ADD_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "add-role-permissions");
    }

    @Test
    @WithMockUser
    public void testSetRolePermissions()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-role-permissions");

        Role role = new Role("admin");

        when(roleManager.getRole("admin")).thenReturn(role);
        when(permissions.isPermission(any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        mockMvc.perform(post(RestPaths.AUTH_SET_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(role.getPermissions().contains("perm1"));
        assertTrue(role.getPermissions().contains("perm2"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-role-permissions");
    }

    @Test
    @WithMockUser
    public void testSetRolePermissionsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-role-permissions");
    }

    @Test
    @WithMockUser
    public void testRemoveRolePermissions()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "remove-role-permissions");

        Role role = new Role("admin");

        role.getPermissions().add("perm1");
        role.getPermissions().add("perm2");
        role.getPermissions().add("perm3");

        when(roleManager.getRole("admin")).thenReturn(role);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        mockMvc.perform(post(RestPaths.AUTH_REMOVE_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals(1, role.getPermissions().size());
        assertTrue(role.getPermissions().contains("perm3"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "remove-role-permissions");
    }

    @Test
    @WithMockUser
    public void testRemoveRolePermissionsNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("perm1", "perm2"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_REMOVE_ROLE_PERMISSIONS_PATH)
                        .param("role", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "remove-role-permissions");
    }

    @Test
    @WithMockUser
    public void testSetPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-password");

        Admin entity = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenAnswer(a ->
        {
            entity.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);

            return entity;
        });

        mockMvc.perform(post(RestPaths.AUTH_SET_PASSWORD_PATH)
                        .param("admin", "admin")
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(entity.getPassword().startsWith("$argon2id$"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-password");
    }

    @Test
    @WithMockUser
    public void testSetPasswordNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_PASSWORD_PATH)
                        .param("admin", "admin")
                        .content("test-password")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).getAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-password");
    }

    @Test
    @WithMockUser("admin")
    public void testChangePassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "change-password");

        Admin admin = new Admin("admin");

        admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);
        admin.setPassword(passwordEncoder.encode("test"));

        when(adminManager.getAdmin("admin")).thenAnswer(a -> admin);

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "new");

        mockMvc.perform(post(RestPaths.AUTH_CHANGE_PASSWORD_PATH)
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(passwordEncoder.matches("new", admin.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "change-password");
    }

    @Test
    @WithMockUser("admin")
    public void testChangePasswordNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);
        admin.setPassword(passwordEncoder.encode("test"));

        when(adminManager.getAdmin("admin")).thenAnswer(a -> admin);

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "new");

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_CHANGE_PASSWORD_PATH)
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertTrue(passwordEncoder.matches("test", admin.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "change-password");
    }

    @Test
    @WithMockUser("admin")
    public void testChangePasswordCurrentPasswordWrong()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "change-password");

        Admin admin = new Admin("admin");

        admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);
        admin.setPassword(passwordEncoder.encode("test"));

        when(adminManager.getAdmin("admin")).thenAnswer(a -> admin);

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("wrong",
                "new", "new");

        mockMvc.perform(post(RestPaths.AUTH_CHANGE_PASSWORD_PATH)
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Current password is incorrect"));

        assertTrue(passwordEncoder.matches("test", admin.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "change-password");
    }

    @Test
    @WithMockUser("admin")
    public void testChangePasswordMismatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "change-password");

        Admin admin = new Admin("admin");

        admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);
        admin.setPassword(passwordEncoder.encode("test"));

        when(adminManager.getAdmin("admin")).thenAnswer(a -> admin);

        AuthDTO.ChangePassword changePassword = new AuthDTO.ChangePassword("test",
                "new", "mismatch");

        mockMvc.perform(post(RestPaths.AUTH_CHANGE_PASSWORD_PATH)
                        .content(new ObjectMapper().writeValueAsString(changePassword))
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("New password and repeated password do not match"));

        assertTrue(passwordEncoder.matches("test", admin.getPassword()));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "change-password");
    }

    @Test
    @WithMockUser
    public void testSetIpAddresses()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-ip-addresses");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenAnswer(a ->
        {
            admin.setAuthenticationType(AuthenticationType.USERNAME_PASSWORD);

            return admin;
        });

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("127.0.0.1/32", "10.0.0.0/24"));

        mockMvc.perform(post(RestPaths.AUTH_SET_IP_ADDRESSES_PATH)
                        .param("admin", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertThat(admin.getIpAddresses(), allOf(hasItem("127.0.0.1/32"), hasItem("10.0.0.0/24")));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-ip-addresses");
    }

    @Test
    @WithMockUser
    public void testSetIpAddressesNoPermissions()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("127.0.0.1/32", "10.0.0.0/24"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_IP_ADDRESSES_PATH)
                        .param("admin", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-ip-addresses");
    }

    @Test
    @WithMockUser
    public void testSetRoles()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-roles");

        Admin admin = new Admin("admin");

        Role role1 = new Role("role1");
        Role role2 = new Role("role2");

        when(adminManager.getAdmin("admin")).thenReturn(admin);
        when(roleManager.getRole("role1")).thenReturn(role1);
        when(roleManager.getRole("role2")).thenReturn(role2);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("role1", "role2"));

        mockMvc.perform(post(RestPaths.AUTH_SET_ROLES_PATH)
                        .param("admin", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals(2, admin.getRoles().size());
        assertTrue(admin.getRoles().contains(role1));
        assertTrue(admin.getRoles().contains(role2));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-roles");
    }

    @Test
    @WithMockUser
    public void testSetRolesNoPermission()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("role1", "role2"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_ROLES_PATH)
                        .param("admin", "admin")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).getAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-roles");
    }

    @Test
    @WithMockUser
    public void testSetInheritedRoles()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-inherited-roles");

        Role role = new Role("role");

        when(roleManager.getRole("role")).thenReturn(role);
        when(roleManager.getRole("inherit1")).thenReturn(new Role("inherit1"));
        when(roleManager.getRole("inherit2")).thenReturn(new Role("inherit2"));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("inherit1", "inherit2"));

        mockMvc.perform(post(RestPaths.AUTH_SET_INHERITED_ROLES_PATH)
                        .param("role", "role")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals(2, role.getRoles().size());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-inherited-roles");
    }

    @Test
    @WithMockUser
    public void testSetInheritedRolesNoPermissions()
    throws Exception
    {
        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("inherit1", "inherit2"));

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_INHERITED_ROLES_PATH)
                        .param("role", "role")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(roleManager, never()).getRole(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-inherited-roles");
    }

    @Test
    @WithMockUser
    public void testGetEffectivePermissions()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-effective-permissions");

        when(adminManager.getAdmin("admin")).thenReturn(new Admin("admin"));
        when(adminManager.getEffectivePermissions("admin")).thenReturn(Set.of("perm1", "perm2"));

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_EFFECTIVE_PERMISSIONS_PATH)
                        .param("admin", "admin")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        List<String> response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, response.size());
        assertTrue(response.contains("perm1"));
        assertTrue(response.contains("perm2"));

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-effective-permissions");
    }

    @Test
    @WithMockUser
    public void testGetEffectivePermissionsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_EFFECTIVE_PERMISSIONS_PATH)
                        .param("admin", "admin")
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(adminManager, never()).getAdmin(any());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-effective-permissions");
    }

    @Test
    @WithMockUser
    public void testGetAuthenticatedUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-authenticated-user");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_AUTHENTICATED_USER_PATH)
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("user", result.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void testGetAuthenticatedUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_AUTHENTICATED_USER_PATH)
                        .contentType("application/json"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void testSet2FAEnabled()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-2fa-enabled");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertFalse(admin.isEnable2FA());

        mockMvc.perform(post(RestPaths.AUTH_SET_2FA_ENABLED_PATH)
                        .param("admin", "admin")
                        .param("enable", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(admin.isEnable2FA());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-2fa-enabled");
    }

    @Test
    @WithMockUser
    public void testSet2FAEnabledPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertFalse(admin.isEnable2FA());

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_2FA_ENABLED_PATH)
                        .param("admin", "admin")
                        .param("enable", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertFalse(admin.isEnable2FA());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-2fa-enabled");
    }

    @Test
    @WithMockUser
    public void testSet2FASecret()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-2fa-secret");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertNull(admin.get2FASecret());

        mockMvc.perform(post(RestPaths.AUTH_SET_2FA_SECRET_PATH)
                        .param("admin", "admin")
                        .content("notarealsecret")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertEquals("notarealsecret", admin.get2FASecret());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-2fa-secret");
    }

    @Test
    @WithMockUser
    public void testSet2FASecretNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertNull(admin.get2FASecret());

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_2FA_SECRET_PATH)
                        .param("admin", "admin")
                        .content("notarealsecret")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        assertNull(admin.get2FASecret());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCallerIs2FASecretSet()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-caller-is-2fa-secret-set");

        Admin admin = new Admin("admin");
        admin.set2FASecret("not-a-real-secret");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_IS_2FA_SECRET_SET_PATH)
                        .contentType("text/plain"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("true", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-is-2fa-secret-set");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCallerIs2FASecretSetNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");
        admin.set2FASecret("not-a-real-secret");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_IS_2FA_SECRET_SET_PATH)
                        .contentType("text/plain"))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-is-2fa-secret-set");
    }

    @Test
    @WithMockUser("admin")
    public void testGet2FASecret()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-2fa-secret");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        admin.set2FASecret("notarealsecret");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_2FA_SECRET_PATH)
                        .param("admin", "admin")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("notarealsecret", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testGet2FASecretNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        admin.set2FASecret("notarealsecret");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_2FA_SECRET_PATH)
                        .param("admin", "admin")
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCaller2FASecret()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-caller-2fa-secret");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        admin.set2FASecret("notarealsecret");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_2FA_SECRET_PATH)
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("notarealsecret", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCaller2FASecretNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        admin.set2FASecret("notarealsecret");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_2FA_SECRET_PATH)
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCaller2FASecretQRCode()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-caller-2fa-secret");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        admin.set2FASecret("notarealsecret");

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_2FA_SECRET_QR_CODE_PATH)
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        String qrCode = result.getResponse().getContentAsString();

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(Base64Utils.decode(qrCode)));

        Assertions.assertEquals(200, image.getWidth(null));
        Assertions.assertEquals(200, image.getHeight(null));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new RGBLuminanceSource(image.getWidth(null), image.getHeight(null),
                        ImageUtils.getPixels(image))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        assertEquals("otpauth://totp/:admin?secret=notarealsecret&issuer=", qrCodeResult.getText());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-2fa-secret");
    }

    @Test
    @WithMockUser("admin")
    public void testSetCaller2FAEnabled()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "set-caller-2fa-enabled");

        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertFalse(admin.isEnable2FA());

        mockMvc.perform(post(RestPaths.AUTH_SET_CALLER_2FA_ENABLED_PATH)
                        .param("enable", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        assertTrue(admin.isEnable2FA());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-caller-2fa-enabled");
    }

    @Test
    @WithMockUser("admin")
    public void testSetCaller2FAEnabledNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        assertFalse(admin.isEnable2FA());

        MvcResult result = mockMvc.perform(post(RestPaths.AUTH_SET_CALLER_2FA_ENABLED_PATH)
                        .param("enable", "true")
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "set-caller-2fa-enabled");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCallerAuthenticationType()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.AUTH, "get-caller-authentication-type");

        Admin admin = new Admin("admin");
        admin.setAuthenticationType(AuthenticationType.OIDC);

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_AUTHENTICATION_TYPE_PATH)
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        AuthenticationType response = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(AuthenticationType.OIDC, response);

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-authentication-type");
    }

    @Test
    @WithMockUser("admin")
    public void testGetCallerAuthenticationTypeNoPermission()
    throws Exception
    {
        Admin admin = new Admin("admin");
        admin.setAuthenticationType(AuthenticationType.OIDC);

        when(adminManager.getAdmin("admin")).thenReturn(admin);

        MvcResult result = mockMvc.perform(get(RestPaths.AUTH_GET_CALLER_AUTHENTICATION_TYPE_PATH)
                        .contentType("text/plain")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.FORBIDDEN.value()))
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.AUTH,
                "get-caller-authentication-type");
    }
}
