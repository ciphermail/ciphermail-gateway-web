/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.james.MailQueueFactoryProvider;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.MailDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.MailDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.function.Failable;
import org.apache.james.core.MailAddress;
import org.apache.james.core.MaybeSender;
import org.apache.james.queue.api.MailQueue;
import org.apache.james.queue.api.MailQueueName;
import org.apache.james.queue.api.ManageableMailQueue;
import org.apache.mailet.Mail;
import org.apache.mailet.base.test.FakeMail;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.stubbing.OngoingStubbing;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = MailQueueController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        MailDetailsFactory.class})
public class MailQueueControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean(answer = Answers.RETURNS_DEEP_STUBS)
    private MailQueueFactoryProvider mailQueueFactoryProvider;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    static class MockMailQueue implements ManageableMailQueue
    {
        private final MailQueueName mailQueueName;
        private final List<ManageableMailQueue.MailQueueItemView> items;

        MockMailQueue(@Nonnull String name, @Nonnull List<ManageableMailQueue.MailQueueItemView> items)
        {
            this.mailQueueName = MailQueueName.of(name);
            this.items = items;
        }

        @Override
        public long getSize() {
            return items.size();
        }

        @Override
        public long flush() {
            return 0;
        }

        @Override
        public long clear()
        {
            int size = items.size();
            items.clear();
            return size;
        }

        @Override
        public long remove(Type type, String value)
        {
            int current = items.size();

            switch (type)
            {
                case Name -> items.removeIf(m -> m.getMail().getName().equals(value));
                case Recipient -> items.removeIf(m -> Failable.get(()
                        -> m.getMail().getRecipients().contains(new MailAddress(value))));
                case Sender -> items.removeIf(m -> Failable.get(()
                        -> m.getMail().getMaybeSender().equals(MaybeSender.of(new MailAddress(value)))));
            }

            return current - items.size();
        }

        @Override
        public MailQueueIterator browse()
        {
            Iterator<ManageableMailQueue.MailQueueItemView> itemViewIterator = items.iterator();

            return new MailQueueIterator() {
                @Override
                public void close() {
                    // ignore
                }

                @Override
                public boolean hasNext() {
                    return itemViewIterator.hasNext();
                }

                @Override
                public MailQueueItemView next() {
                    return itemViewIterator.next();
                }
            };
        }

        @Override
        public MailQueueName getName() {
            return mailQueueName;
        }

        @Override
        public void enQueue(Mail mail, Duration delay) {
            // empty on purpose
        }

        @Override
        public void enQueue(Mail mail) {
            // empty on purpose
        }

        @Override
        public Publisher<Void> enqueueReactive(Mail mail) {
            return null;
        }

        @Override
        public Publisher<MailQueueItem> deQueue() {
            return null;
        }

        @Override
        public void close() {
            // empty on purpose
        }
    }

    private static class MailQueueItemViewImpl implements ManageableMailQueue.MailQueueItemView
    {
        private final Mail mail;

        MailQueueItemViewImpl(@Nonnull Mail mail) {
            this.mail = mail;
        }
        @Override
        public Mail getMail() {
            return mail;
        }

        @Override
        public Optional<ZonedDateTime> getNextDelivery() {
            return Optional.empty();
        }
    }

    private Mail createMail(@Nonnull String subject, @Nonnull String recipient, @Nonnull String mailName)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(mailName)
                .recipients(recipient)
                .sender("sender@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();
    }

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @Test
    @WithMockUser
    public void getQueueNames()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-names");

        when(mailQueueFactoryProvider.getMailQueueFactory().listCreatedMailQueues()).thenReturn(
                Set.of(MailQueueName.of("outgoing"), MailQueueName.of("spool")));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUE_NAMES_PATH))
                .andExpect(status().isOk())
                .andReturn();

        List<String> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, results.size());
        assertTrue(results.contains("outgoing"));
        assertTrue(results.contains("spool"));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-names");
    }

    @Test
    @WithMockUser
    public void getQueueNamesNoPermissions()
    throws Exception
    {
        when(mailQueueFactoryProvider.getMailQueueFactory().listCreatedMailQueues()).thenReturn(
                Set.of(MailQueueName.of("outgoing"), MailQueueName.of("spool")));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUE_NAMES_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-names");
    }

    @Test
    @WithMockUser
    public void getQueuedMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");

        List<ManageableMailQueue.MailQueueItemView> items = List.of(
                new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")),
                new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")),
                new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("firstResult", "0")
                        .param("maxResults", "10"))
                .andExpect(status().isOk())
                .andReturn();

        List<MailDTO.MailDetails> results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3, results.size());
        assertEquals("test1@example.com", String.join(",", results.get(0).recipients()));
        assertEquals("test2@example.com", String.join(",", results.get(1).recipients()));
        assertEquals("test3@example.com", String.join(",", results.get(2).recipients()));

        result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("firstResult", "1")
                        .param("maxResults", "1"))
                .andExpect(status().isOk())
                .andReturn();

        results = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, results.size());
        assertEquals("test2@example.com", String.join(",", results.get(0).recipients()));

        verify(permissionChecker, times(2)).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");
    }

    @Test
    @WithMockUser
    public void getQueuedMailsNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");
    }

    @Test
    @WithMockUser
    public void getQueuedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mail");

        List<ManageableMailQueue.MailQueueItemView> items = List.of(
                new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")),
                new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")),
                new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("mailName", "mail2")
                        .param("includeMIME", "true"))
                .andExpect(status().isOk())
                .andReturn();

        MailDTO.MailDetailsWithMIME details = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals("test2@example.com", String.join(",", details.mailDetails().recipients()));

        assertTrue(details.mime().contains("Subject: test2"));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mail");
    }

    @Test
    @WithMockUser
    public void getQueuedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("mailName", "mail2")
                        .param("includeMIME", "true"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mail");
    }

    @Test
    @WithMockUser
    public void deleteQueuedMails()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mails");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");

        List<ManageableMailQueue.MailQueueItemView> items = new LinkedList<>();
        items.add(new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")));
        items.add(new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")));
        items.add(new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("mail1", "mail2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        Long deleted = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2L, (long) deleted);

        result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isOk())
                .andReturn();

        List<MailDTO.MailDetails> mailItems = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1, mailItems.size());
        assertEquals("test3@example.com", String.join(",", mailItems.get(0).recipients()));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mails");
    }

    @Test
    @WithMockUser
    public void deleteQueuedMailsNoPermission()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");

        List<ManageableMailQueue.MailQueueItemView> items = new LinkedList<>();
        items.add(new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")));
        items.add(new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")));
        items.add(new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("mail1", "mail2"));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mails");
    }

    @Test
    @WithMockUser
    public void deleteQueuedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mail");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");

        List<ManageableMailQueue.MailQueueItemView> items = new LinkedList<>();
        items.add(new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")));
        items.add(new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")));
        items.add(new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("field", MailDTO.MailQueueField.NAME.name())
                        .param("value", "mail2")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        Long deleted = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(1L, (long) deleted);

        result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isOk())
                .andReturn();

        List<MailDTO.MailDetails> mailItems = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(2, mailItems.size());
        assertEquals("test1@example.com", String.join(",", mailItems.get(0).recipients()));
        assertEquals("test3@example.com", String.join(",", mailItems.get(1).recipients()));

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mail");
    }

    @Test
    @WithMockUser
    public void deleteQueuedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .param("field", MailDTO.MailQueueField.NAME.name())
                        .param("value", "mail2")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-queued-mail");
    }

    @Test
    @WithMockUser
    public void deleteAllQueuedMail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-all-queued-mail");

        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queued-mails");

        List<ManageableMailQueue.MailQueueItemView> items = new LinkedList<>();
        items.add(new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")));
        items.add(new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")));
        items.add(new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_ALL_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        Long deleted = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3L, (long) deleted);

        result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUED_MAILS_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isOk())
                .andReturn();

        List<MailDTO.MailDetails> mailItems = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(0, mailItems.size());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-all-queued-mail");
    }

    @Test
    @WithMockUser
    public void deleteAllQueuedMailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.MAIL_QUEUE_DELETE_ALL_QUEUED_MAIL_PATH)
                        .param("mailQueueName", "outgoing")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "delete-all-queued-mail");
    }

    @Test
    @WithMockUser
    public void getQueueSize()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-size");

        List<ManageableMailQueue.MailQueueItemView> items = new LinkedList<>();
        items.add(new MailQueueItemViewImpl(createMail("test1","test1@example.com", "mail1")));
        items.add(new MailQueueItemViewImpl(createMail("test2","test2@example.com", "mail2")));
        items.add(new MailQueueItemViewImpl(createMail("test3","test3@example.com", "mail3")));

        MockMailQueue mailQueue = new MockMailQueue("outgoing", items);

        OngoingStubbing<Optional<? extends MailQueue>> stubbing = when(mailQueueFactoryProvider.getMailQueueFactory()
                .getQueue(any()));

        stubbing.thenReturn(Optional.of(mailQueue));

        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUE_SIZE_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isOk())
                .andReturn();

        Long size = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), new TypeReference<>() {});

        assertEquals(3L, (long) size);

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-size");
    }

    @Test
    @WithMockUser
    public void getQueueSizeNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.MAIL_QUEUE_GET_QUEUE_SIZE_PATH)
                        .param("mailQueueName", "outgoing"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.MAIL_QUEUE,
                "get-queue-size");
    }
}