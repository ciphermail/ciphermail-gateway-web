/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.impl.MockupUser;
import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.util.CloseableIteratorAdapter;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.core.UserDTO;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestExceptionHandler;
import com.ciphermail.rest.server.service.RestValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.StringWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class
})
public class UserControllerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private UserWorkflow userWorkflow;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    @Before
    public void setup()
    {
        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    @Test
    @WithMockUser
    public void addUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "add-user");

        String email = "test@example.com";

        // mock UserWorkflow
        when(userWorkflow.addUser(any(String.class))).thenAnswer(i -> new MockupUser(i.getArgument(0),
                new MockupUserPreferences(i.getArgument(0), UserPreferencesCategory.USER.name()),
                null));
        when(userWorkflow.makePersistent(any(User.class))).thenAnswer(i -> i.getArgument(0));

        String json = mockMvc.perform(post(RestPaths.USER_ADD_USER_PATH)
                        .param("email", email)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserDTO.UserValue userValue = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue.class);

        assertEquals(email, userValue.email());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "add-user");

        // now pretend the user is already added
        when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                        null));

        json = mockMvc.perform(post(RestPaths.USER_ADD_USER_PATH)
                        .param("email", email)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        userValue = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue.class);

        assertEquals(email, userValue.email());
    }

    @Test
    @WithMockUser
    public void addUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.USER_ADD_USER_PATH)
                        .param("email", "test@example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).addUser(any(String.class));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "add-user");
    }

    @Test
    @WithMockUser
    public void addUserInvalidEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "add-user");

        MvcResult result = mockMvc.perform(post(RestPaths.USER_ADD_USER_PATH)
                        .param("email", "!@#$%^/\"")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("!@#$%^/\" is not a valid email address", error.message());
        assertEquals("backend.validation.invalid-email-address", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "add-user");
    }

    @Test
    @WithMockUser
    public void getUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-user");

        String email = "test@example.com";

        // pretend the user is already added
        when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                        null));

        String json = mockMvc.perform(get(RestPaths.USER_GET_USER_PATH)
                        .param("email", email))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserDTO.UserValue userValue = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue.class);

        assertEquals(email, userValue.email());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-user");

        // check user which does not exist
        MvcResult result = mockMvc.perform(get(RestPaths.USER_GET_USER_PATH)
                        .param("email", "other@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());
    }

    @Test
    @WithMockUser
    public void getUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_GET_USER_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
            .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).getUser(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-user");
    }

    @Test
    @WithMockUser
    public void isUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "is-user");

        String email = "test@example.com";

        mockMvc.perform(get(RestPaths.USER_IS_USER_PATH)
                        .param("email", email))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "is-user");

        // now pretend the user exists
        when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                        null));

        mockMvc.perform(get(RestPaths.USER_IS_USER_PATH)
                        .param("email", email))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    @WithMockUser
    public void isUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_IS_USER_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).getUser(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "is-user");
    }

    @Test
    @WithMockUser
    public void deleteUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-user");

        String email = "test@example.com";

        when(userWorkflow.deleteUser(any(User.class))).thenReturn(true);

        mockMvc.perform(post(RestPaths.USER_DELETE_USER_PATH)
                        .param("email", email)
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("User test@example.com does not exist"));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-user");

        // now pretend the user exists
        when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                        null));

        String json = mockMvc.perform(post(RestPaths.USER_DELETE_USER_PATH)
                        .param("email", email)
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserDTO.UserValue userValue = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue.class);

        assertEquals(email, userValue.email());
    }

    @Test
    @WithMockUser
    public void deleteUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.USER_DELETE_USER_PATH)
                        .param("email", "test@example.com")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).deleteUser(any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-user");
    }

    @Test
    @WithMockUser
    public void deleteUsers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-users");

        List<String> emailAddresses = List.of("test1@example.com", "test2@example.com");

        for (String email : emailAddresses)
        {
            // add users
            when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                    new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                            null));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1@example.com", "test2@example.com"));

        mockMvc.perform(post(RestPaths.USER_DELETE_USERS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(userWorkflow, times(emailAddresses.size())).deleteUser(any(User.class));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-users");
    }

    @Test
    @WithMockUser
    public void deleteUsersNoPermission()
    throws Exception
    {
        List<String> emailAddresses = List.of("test1@example.com", "test2@example.com");

        for (String email : emailAddresses)
        {
            // add users
            when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                    new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                            null));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1@example.com", "test2@example.com"));

        MvcResult result = mockMvc.perform(post(RestPaths.USER_DELETE_USERS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).deleteUser(any(User.class));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-users");
    }

    @Test
    @WithMockUser
    public void deleteUsersUserNotExist()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-users");

        List<String> emailAddresses = List.of("test1@example.com");

        for (String email : emailAddresses)
        {
            // add users
            when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                    new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                            null));
        }

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of("test1@example.com", "test2@example.com"));

        mockMvc.perform(post(RestPaths.USER_DELETE_USERS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason("User test2@example.com does not exist"));

        // the first user is "deleted" but because the transaction fails, it will not really be deleted
        verify(userWorkflow, times(1)).deleteUser(any(User.class));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "delete-users");
    }

    @Test
    @WithMockUser
    public void getUsers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users");

        List<String> emails = List.of("test1@example.com", "test2@example.com");

        try (CloseableIteratorAdapter<String> iterator = new CloseableIteratorAdapter<>(
                emails.iterator()))
        {
            when(userWorkflow.getEmailIterator(any(), any(), any())).thenReturn(iterator);
        }

        for (String email : emails)
        {
            when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                    new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                            null));
        }

        String json = mockMvc.perform(get(RestPaths.USER_GET_USERS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserDTO.UserValue[] userValues = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue[].class);

        assertEquals(2, userValues.length);

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users");
    }

    @Test
    @WithMockUser
    public void getUsersNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_GET_USERS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).getUsers(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users");
    }

    @Test
    @WithMockUser
    public void getExceedMaxResults()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users");

        mockMvc.perform(get(RestPaths.USER_GET_USERS_PATH)
                        .param("firstResult", "0")
                        .param("maxResults", "251")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));
    }

    @Test
    @WithMockUser
    public void getUsersCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users-count");

        when(userWorkflow.getUserCount()).thenReturn(10L);

        mockMvc.perform(get(RestPaths.USER_GET_USERS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users-count");
    }

    @Test
    @WithMockUser
    public void getUsersCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_GET_USERS_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).getUserCount();

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "get-users-count");
    }

    @Test
    @WithMockUser
    public void searchUsers()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users");

        List<String> emails = List.of("test1@example.com", "test2@example.com");

        try (CloseableIteratorAdapter<String> iterator = new CloseableIteratorAdapter<>(
                emails.iterator()))
        {
            when(userWorkflow.searchEmail(any(), any(), any(), any())).thenReturn(iterator);
        }

        for (String email : emails)
        {
            when(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST)).thenReturn(
                    new MockupUser(email, new MockupUserPreferences(email, UserPreferencesCategory.USER.name()),
                            null));
        }

        String json = mockMvc.perform(get(RestPaths.USER_SEARCH_USERS_PATH)
                        .param("search", "test")
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserDTO.UserValue[] userValues = JacksonUtil.getObjectMapper().readValue(
                json, UserDTO.UserValue[].class);

        assertEquals(2, userValues.length);

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users");
    }

    @Test
    @WithMockUser
    public void searchUsersNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_SEARCH_USERS_PATH)
                        .param("search", "test")
                        .param("firstResult", "0")
                        .param("maxResults", "10")
                        .param("sortDirection", SortDirection.ASC.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).searchEmail(any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users");
    }

    @Test
    @WithMockUser
    public void searchUsersCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users-count");

        when(userWorkflow.getSearchEmailCount(any())).thenReturn(10L);

        mockMvc.perform(get(RestPaths.USER_SEARCH_USERS_COUNT_PATH)
                        .param("search", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users-count");
    }

    @Test
    @WithMockUser
    public void searchUsersCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.USER_SEARCH_USERS_COUNT_PATH)
                        .param("search", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(userWorkflow, never()).getSearchEmailCount(any());

        verify(permissionChecker).checkPermission(PermissionCategory.USER,
                "search-users-count");
    }
}