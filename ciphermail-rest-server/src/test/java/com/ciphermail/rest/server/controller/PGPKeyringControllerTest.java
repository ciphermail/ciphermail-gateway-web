/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyType;
import com.ciphermail.core.common.security.openpgp.PGPSearchField;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.StaticPGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.StaticPGPKeyRingEntryProvider;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorFailureSeverity;
import com.ciphermail.core.common.util.CloseableIteratorAdapter;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.PGPKeyDetailsFactory;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PGPKeyringController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
})
public class PGPKeyringControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @MockBean
    private PGPKeyRing keyRing;

    @Autowired
    private TransactionOperations transactionOperations;

    @MockBean
    private PGPKeyDetailsFactory detailsFactory;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    private StaticPGPKeyRingEntry testKeyRingEntry;

    @Before
    public void setup()
    throws Exception
    {
        StaticPGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE_CORE,
                "pgp/test@example.com.gpg.asc"));

        testKeyRingEntry = keyProvider.getByKeyID(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }
    }

    private PGPDTO.PGPKeyDetails createPGPKeyDetails()
    {
        UUID id = UUID.nameUUIDFromBytes("id".getBytes());

        Date now = new Date();

        return new PGPDTO.PGPKeyDetails(
                id,
                "4EC4E8813E11A9A0",
                "FFFFFFFFFFFFFFF0",
                now.getTime(),
                DateUtils.addHours(now, 1).getTime(),
                DateUtils.addHours(now, 2).getTime(),
                "alias",
                true,
                true,
                true,
                "F372FCF8208776062C69C1854EC4E8813E11A9A0",
                "ADE29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                4096,
                "RSA",
                Set.of("test1 <test1@example.com>", "test2 <test2@example.com>"),
                Set.of("test1@example.com", "test2@example.com", "test3@example.com"),
                true,
                UUID.nameUUIDFromBytes("parent".getBytes()),
                true,
                PGPPublicKeyValidatorFailureSeverity.CRITICAL,
                "This is a test");
    }

    private PGPDTO.PGPKeyDetails createNullsPGPKeyDetails()
    {
        return new PGPDTO.PGPKeyDetails(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                false,
                false,
                null,
                null,
                0,
                null,
                null,
                null,
                false,
                null,
                false,
                null,
                null);
    }

    @Test
    @WithMockUser
    public void getKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();

        when(keyRing.getByID(keyDetails.id())).thenReturn(testKeyRingEntry);

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_PATH)
                        .param("id", keyDetails.id().toString()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails.class);

        assertEquals(keyDetails.id(), keyDetailsResult.id());
        assertEquals(keyDetails.keyID(), keyDetailsResult.keyID());
        assertEquals(keyDetails.parentKeyID(), keyDetailsResult.parentKeyID());
        assertEquals(keyDetails.creationDate(), keyDetailsResult.creationDate());
        assertEquals(keyDetails.expirationDate(), keyDetailsResult.expirationDate());
        assertEquals(keyDetails.insertionDate(), keyDetailsResult.insertionDate());
        assertEquals(keyDetails.privateKeyAlias(), keyDetailsResult.privateKeyAlias());
        assertEquals(keyDetails.privateKeyAvailable(), keyDetailsResult.privateKeyAvailable());
        assertEquals(keyDetails.validForEncryption(), keyDetailsResult.validForEncryption());
        assertEquals(keyDetails.validForSigning(), keyDetailsResult.validForSigning());
        assertEquals(keyDetails.fingerprint(), keyDetailsResult.fingerprint());
        assertEquals(keyDetails.sha256Fingerprint(), keyDetailsResult.sha256Fingerprint());
        assertEquals(keyDetails.keyLength(), keyDetailsResult.keyLength());
        assertEquals(keyDetails.algorithm(), keyDetailsResult.algorithm());
        assertEquals(keyDetails.userIDs(), keyDetailsResult.userIDs());
        assertEquals(keyDetails.email(), keyDetailsResult.email());
        assertEquals(keyDetails.masterKey(), keyDetailsResult.masterKey());
        assertEquals(keyDetails.failureSeverity(), keyDetailsResult.failureSeverity());
        assertEquals(keyDetails.failureMessage(), keyDetailsResult.failureMessage());

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");
    }

    @Test
    @WithMockUser
    public void getKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");
    }

    @Test
    @WithMockUser
    public void getKeyNullValues()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");

        PGPDTO.PGPKeyDetails keyDetails = createNullsPGPKeyDetails();

        when(keyRing.getByID(any())).thenReturn(testKeyRingEntry);

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_PATH)
                        .param("id", UUID.randomUUID().toString()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails.class);

        assertEquals(keyDetails.id(), keyDetailsResult.id());
        assertEquals(keyDetails.keyID(), keyDetailsResult.keyID());
        assertEquals(keyDetails.parentKeyID(), keyDetailsResult.parentKeyID());
        assertEquals(keyDetails.creationDate(), keyDetailsResult.creationDate());
        assertEquals(keyDetails.expirationDate(), keyDetailsResult.expirationDate());
        assertEquals(keyDetails.insertionDate(), keyDetailsResult.insertionDate());
        assertEquals(keyDetails.privateKeyAlias(), keyDetailsResult.privateKeyAlias());
        assertEquals(keyDetails.privateKeyAvailable(), keyDetailsResult.privateKeyAvailable());
        assertEquals(keyDetails.validForEncryption(), keyDetailsResult.validForEncryption());
        assertEquals(keyDetails.validForSigning(), keyDetailsResult.validForSigning());
        assertEquals(keyDetails.fingerprint(), keyDetailsResult.fingerprint());
        assertEquals(keyDetails.sha256Fingerprint(), keyDetailsResult.sha256Fingerprint());
        assertEquals(keyDetails.keyLength(), keyDetailsResult.keyLength());
        assertEquals(keyDetails.algorithm(), keyDetailsResult.algorithm());
        assertEquals(keyDetails.userIDs(), keyDetailsResult.userIDs());
        assertEquals(keyDetails.email(), keyDetailsResult.email());
        assertEquals(keyDetails.masterKey(), keyDetailsResult.masterKey());
        assertEquals(keyDetails.failureSeverity(), keyDetailsResult.failureSeverity());
        assertEquals(keyDetails.failureMessage(), keyDetailsResult.failureMessage());

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");
    }

    @Test
    @WithMockUser
    public void getKeyKeyNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");

        String id = UUID.nameUUIDFromBytes("id".getBytes()).toString();

        mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_PATH)
                        .param("id", id))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("Key with id %s not found", id)));

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key");
    }

    @Test
    @WithMockUser
    public void getKeyBySha256Fingerprint()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key-by-sha256-fingerprint");

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();

        when(keyRing.getBySha256Fingerprint(keyDetails.sha256Fingerprint())).thenReturn(testKeyRingEntry);

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH)
                        .param("sha256Fingerprint", keyDetails.sha256Fingerprint()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails.class);

        assertEquals(keyDetails.id(), keyDetailsResult.id());

        verify(keyRing).getBySha256Fingerprint(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key-by-sha256-fingerprint");
    }

    @Test
    @WithMockUser
    public void getKeyBySha256FingerprintNoPermission()
    throws Exception
    {
        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH)
                        .param("sha256Fingerprint", keyDetails.sha256Fingerprint()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).getBySha256Fingerprint(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key-by-sha256-fingerprint");
    }

    @Test
    @WithMockUser
    public void getKeyBySha256FingerprintNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key-by-sha256-fingerprint");

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEY_BY_SHA256_FINGERPRINT_PATH)
                        .param("sha256Fingerprint", keyDetails.sha256Fingerprint()))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("", result.getResponse().getContentAsString());

        verify(keyRing).getBySha256Fingerprint(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-key-by-sha256-fingerprint");
    }

    @Test
    @WithMockUser
    public void getSubkeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");

        when(keyRing.getByID(any())).thenReturn(testKeyRingEntry);

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();
        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
                        .param("id", keyDetails.id().toString()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(1, keyDetailsResult.length);

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");
    }

    @Test
    @WithMockUser
    public void getSubkeysNoPermission()
    throws Exception
    {
        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
                        .param("id", keyDetails.id().toString()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");
    }

    @Test
    @WithMockUser
    public void getSubkeysByIdNoSubKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");

        testKeyRingEntry.setSubkeys(null);
        when(keyRing.getByID(any())).thenReturn(testKeyRingEntry);

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();
        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
                        .param("id", keyDetails.id().toString()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(0, keyDetailsResult.length);

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");
    }

    @Test
    @WithMockUser
    public void getSubkeysByIdMasterKeyNotFound()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");

        mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_SUBKEYS_PATH)
                        .param("id", UUID.nameUUIDFromBytes("static".getBytes()).toString()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("Key with id %s not found",
                        UUID.nameUUIDFromBytes("static".getBytes()))));

        verify(keyRing).getByID(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-subkeys");
    }

    @Test
    @WithMockUser
    public void getKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys");

        CloseableIteratorAdapter<PGPKeyRingEntry> iterator = new CloseableIteratorAdapter<>(
                List.of(testKeyRingEntry, testKeyRingEntry, testKeyRingEntry).iterator());

        when(keyRing.getIterator(any(), any(), any())).thenReturn(iterator);

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();
        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEYS_PATH)
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(3, keyDetailsResult.length);

        verify(keyRing).getIterator(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys");
    }

    @Test
    @WithMockUser
    public void getKeysNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEYS_PATH)
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).getIterator(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys");
    }

    @Test
    @WithMockUser
    public void getKeysMaxResultTooLarge()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys");

        mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEYS_PATH)
                        .param("expired", Expired.MATCH_ALL.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name())
                        .param("firstResult", "0")
                        .param("maxResults", "251"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(keyRing, never()).getIterator(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys");
    }

    @Test
    @WithMockUser
    public void getKeysCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys-count");

        when(keyRing.getCount(any())).thenReturn(9999L);

        mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEYS_COUNT_PATH))
                .andExpect(status().isOk())
                .andExpect(content().string("9999"));

        verify(keyRing).getCount(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys-count");
    }

    @Test
    @WithMockUser
    public void getKeysCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_GET_KEYS_COUNT_PATH))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).getCount(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "get-keys-count");
    }

    @Test
    @WithMockUser
    public void searchKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys");

        CloseableIteratorAdapter<PGPKeyRingEntry> iterator = new CloseableIteratorAdapter<>(
                List.of(testKeyRingEntry, testKeyRingEntry, testKeyRingEntry).iterator());

        when(keyRing.search(any(), any(), any(), any(), any())).thenReturn(iterator);

        PGPDTO.PGPKeyDetails keyDetails = createPGPKeyDetails();
        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(keyDetails);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_SEARCH_KEYS_PATH)
                        .param("searchValue", "something")
                        .param("searchField", PGPSearchField.EMAIL.name())
                        .param("match", Match.EXACT.name())
                        .param("expired", Expired.MATCH_EXPIRED_ONLY.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(3, keyDetailsResult.length);

        verify(keyRing).search(any(), any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys");
    }

    @Test
    @WithMockUser
    public void searchKeysNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_SEARCH_KEYS_PATH)
                        .param("searchValue", "something")
                        .param("searchField", PGPSearchField.EMAIL.name())
                        .param("match", Match.EXACT.name())
                        .param("expired", Expired.MATCH_EXPIRED_ONLY.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).search(any(), any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys");
    }

    @Test
    @WithMockUser
    public void searchKeysMaxResultTooLarge()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys");

        mockMvc.perform(get(RestPaths.PGP_KEYRING_SEARCH_KEYS_PATH)
                        .param("searchValue", "something")
                        .param("searchField", PGPSearchField.EMAIL.name())
                        .param("match", Match.EXACT.name())
                        .param("expired", Expired.MATCH_EXPIRED_ONLY.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name())
                        .param("firstResult", "0")
                        .param("maxResults", "251"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("maxResults exceed the upper limit 250"));

        verify(keyRing, never()).search(any(), any(), any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys");
    }

    @Test
    @WithMockUser
    public void searchKeysCount()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys-count");

        when(keyRing.searchCount(any(), any(), any())).thenReturn(9999L);

        mockMvc.perform(get(RestPaths.PGP_KEYRING_SEARCH_KEYS_COUNT_PATH)
                        .param("searchValue", "something")
                        .param("searchField", PGPSearchField.EMAIL.name())
                        .param("match", Match.EXACT.name())
                        .param("expired", Expired.MATCH_EXPIRED_ONLY.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isOk())
                .andExpect(content().string("9999"));

        verify(keyRing).searchCount(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys-count");
    }

    @Test
    @WithMockUser
    public void searchKeysCountNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_KEYRING_SEARCH_KEYS_COUNT_PATH)
                        .param("searchValue", "something")
                        .param("searchField", PGPSearchField.EMAIL.name())
                        .param("match", Match.EXACT.name())
                        .param("expired", Expired.MATCH_EXPIRED_ONLY.name())
                        .param("missingKeyAlias", MissingKeyAlias.ALLOWED.name())
                        .param("keyType", PGPKeyType.MASTER_KEY.name()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).searchCount(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "search-keys-count");
    }

    @Test
    @WithMockUser
    public void deleteKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-key");

        when(keyRing.getByID(testKeyRingEntry.getID())).thenReturn(testKeyRingEntry);

        mockMvc.perform(post(RestPaths.PGP_KEYRING_DELETE_KEY_PATH)
                        .param("id", testKeyRingEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(keyRing).delete(testKeyRingEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-key");
    }

    @Test
    @WithMockUser
    public void deleteKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEYRING_DELETE_KEY_PATH)
                        .param("id", testKeyRingEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).delete(testKeyRingEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-key");
    }

    @Test
    @WithMockUser
    public void deleteUnknownId()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-key");

        mockMvc.perform(post(RestPaths.PGP_KEYRING_DELETE_KEY_PATH)
                        .param("id", testKeyRingEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("Key with id %s not found",
                        testKeyRingEntry.getID().toString())));

        verify(keyRing, never()).delete(testKeyRingEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-key");
    }

    @Test
    @WithMockUser
    public void deleteKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-keys");

        when(keyRing.getByID(testKeyRingEntry.getID())).thenReturn(testKeyRingEntry);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(testKeyRingEntry.getID().toString()));

        mockMvc.perform(post(RestPaths.PGP_KEYRING_DELETE_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(keyRing).delete(testKeyRingEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-keys");
    }

    @Test
    @WithMockUser
    public void deleteKeysNoPermission()
    throws Exception
    {
        when(keyRing.getByID(testKeyRingEntry.getID())).thenReturn(testKeyRingEntry);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(testKeyRingEntry.getID().toString()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_KEYRING_DELETE_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRing, never()).delete(testKeyRingEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP_KEYRING,
                "delete-keys");
    }
}