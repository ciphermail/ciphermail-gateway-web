/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.controller;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.impl.MockupUser;
import com.ciphermail.core.app.impl.MockupUserPreferences;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestor;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestorResult;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSubmitResult;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSubmitResultImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistryImpl;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntryRevoker;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingExporter;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPPrivateKeySelector;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeySelector;
import com.ciphermail.core.common.security.openpgp.PGPUserIDRevocationChecker;
import com.ciphermail.core.common.security.openpgp.PGPUserIDRevokedException;
import com.ciphermail.core.common.security.openpgp.PGPUserIDValidator;
import com.ciphermail.core.common.security.openpgp.StaticPGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.StaticPGPKeyRingEntryProvider;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorFailureSeverity;
import com.ciphermail.core.test.MockTransactionOperations;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.PermissionCategory;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.authorization.PermissionChecker;
import com.ciphermail.rest.server.authorization.PermissionDeniedException;
import com.ciphermail.rest.server.authorization.PermissionProviderRegistry;
import com.ciphermail.rest.server.service.AuthenticatedUserProvider;
import com.ciphermail.rest.server.service.ClientAddressProvider;
import com.ciphermail.rest.server.service.PGPKeyDetailsFactory;
import com.ciphermail.rest.server.service.PGPKeyServerSubmitDetailsFactory;
import com.ciphermail.rest.server.service.PropertyValidationFailedExceptionHandler;
import com.ciphermail.rest.server.service.ResponseStatusExceptionFactory;
import com.ciphermail.rest.server.service.RestExceptionHandler;
import com.ciphermail.rest.server.service.RestValidator;
import com.ciphermail.rest.server.test.RestTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPPrivateKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockPart;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PGPController.class)
@RunWith(SpringRunner.class)
@Import({
        ResponseStatusExceptionFactory.class,
        ClientAddressProvider.class,
        AuthenticatedUserProvider.class,
        RestValidator.class,
        PermissionProviderRegistry.class,
        PGPKeyServerSubmitDetailsFactory.class})
public class PGPControllerTest
{
    private static final File TEST_BASE_CORE = new File(RestTestUtils.getCipherMailGatewayHomeDir(),
            TestUtils.getTestDataDir().getPath());

    @MockBean
    private PGPKeyRing keyRing;

    @MockBean
    private PGPKeyRingImporter keyRingImporter;

    @MockBean
    private PGPKeyRingExporter keyRingExporter;

    @MockBean
    private PGPPublicKeySelector encryptionKeySelector;

    @MockBean
    private PGPPrivateKeySelector signingKeySelector;

    @MockBean
    private PGPSecretKeyRequestor secretKeyRequestor;

    @MockBean
    private PGPKeyRingEntryRevoker revoker;

    @MockBean
    private PGPUserIDValidator userIDValidator;

    @MockBean
    private PGPUserIDRevocationChecker userIDrevocationChecker;

    @MockBean
    private PGPKeyDetailsFactory detailsFactory;

    @MockBean
    private UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelector;

    @MockBean
    private UserWorkflow userWorkflow;

    @MockBean
    private DomainManager domainManager;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PermissionChecker permissionChecker;

    private final List<PGPKeyRingEntry> keyRingEntries = new LinkedList<>();

    @Before
    public void setup()
    throws Exception
    {
        keyRingEntries.addAll(new StaticPGPKeyRingEntryProvider(new File(TEST_BASE_CORE,
                "pgp/test-multiple-sub-keys.gpg.asc")).getPGPKeyRingEntries());
        keyRingEntries.addAll(new StaticPGPKeyRingEntryProvider(new File(TEST_BASE_CORE,
                "pgp/secp256k1@example.com.asc")).getPGPKeyRingEntries());

        doThrow(new PermissionDeniedException("Default denied")).when(permissionChecker).checkPermission(
                any(), any());
    }

    @TestConfiguration
    public static class LocalServices
    {
        @Bean
        public TransactionOperations createTransactionOperations() {
            return new MockTransactionOperations();
        }

        @Bean
        public UserPropertiesFactoryRegistry createUserPropertiesFactoryRegistry(ApplicationContext applicationContext) {
            return new UserPropertiesFactoryRegistryImpl(applicationContext);
        }
    }

    private PGPDTO.PGPKeyDetails createMockPGPKeyDetails(int i)
    throws Exception
    {
        byte[] b = Integer.toString(i).getBytes();

        String sha256Fingerprint = Digests.digestHex(b, Digest.SHA256);

        Date now = new Date();

        return new PGPDTO.PGPKeyDetails(
                UUID.randomUUID(),
                sha256Fingerprint.substring(sha256Fingerprint.length()-16),
                "FFFFFFFFFFFFFFF0",
                now.getTime(),
                DateUtils.addHours(now, 1).getTime(),
                DateUtils.addHours(now, 2).getTime(),
                "alias" + i,
                true,
                true,
                true,
                Digests.digestHex(b, Digest.SHA1),
                sha256Fingerprint,
                4096,
                "RSA",
                Set.of(String.format("test%d <test1@example.com>", i)),
                Set.of(String.format("test%d@example.com", i)),
                true,
                UUID.randomUUID(),
                true,
                PGPPublicKeyValidatorFailureSeverity.CRITICAL,
                "This is a test " + i);
    }

    @Test
    @WithMockUser
    public void importKeyring()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "import-keyring");

        when(keyRingImporter.importKeyRing(any(), any(), anyBoolean())).thenReturn(keyRingEntries);

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.PGP_IMPORT_KEYRING_PATH)
                        .file("keyringFile", "not-a-real-keyring".getBytes())
                        .part(new MockPart("password", "some password".getBytes(StandardCharsets.UTF_8)))
                        .param("ignoreParsingErrors", "true")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().string("8"));

        verify(keyRingImporter).importKeyRing(any(), any(), anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "import-keyring");
    }

    @Test
    @WithMockUser
    public void importKeyringNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.PGP_IMPORT_KEYRING_PATH)
                        .file("keyringFile", "not-a-real-keyring".getBytes())
                        .param("password", "some password")
                        .param("ignoreParsingErrors", "true")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRingImporter, never()).importKeyRing(any(), any(), anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "import-keyring");
    }

    @Test
    @WithMockUser
    public void importKeyringInvalidPassword()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "import-keyring");

        when(keyRingImporter.importKeyRing(any(), any(), anyBoolean())).thenThrow(new PGPException("invalid password"));

        mockMvc.perform(MockMvcRequestBuilders.multipart(RestPaths.PGP_IMPORT_KEYRING_PATH)
                        .file("keyringFile", "not-a-real-keyring".getBytes())
                        .param("password", "some password")
                        .param("ignoreParsingErrors", "true")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("Keyring is invalid or the password is incorrect"));

        verify(keyRingImporter).importKeyRing(any(), any(), anyBoolean());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "import-keyring");
    }

    @Test
    @WithMockUser
    public void exportPublicKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");

        List<UUID> ids = new LinkedList<>();

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                ids.add(entry.getID());
            }
        }

        assertEquals(2, ids.size());

        when(keyRingExporter.exportPublicKeys(any(), any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, ids);

        mockMvc.perform(post(RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(keyRingExporter).exportPublicKeys(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");
    }

    @Test
    @WithMockUser
    public void exportPublicKeysNoPermission()
    throws Exception
    {
       List<UUID> ids = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, ids);

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRingExporter, never()).exportPublicKeys(any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");
    }

    @Test
    @WithMockUser
    public void exportPublicKeysNotAMasterKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");

        List<UUID> ids = new LinkedList<>();

        UUID subkeyID = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                ids.add(entry.getID());
            }
            else if (subkeyID == null) {
                subkeyID = entry.getID();
            }
        }

        assertNotNull(subkeyID);
        ids.add(subkeyID);

        assertEquals(3, ids.size());

        when(keyRingExporter.exportPublicKeys(any(), any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, ids);

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("com.ciphermail.core.app.properties.PropertyValidationFailedException", error.className());
        assertEquals("backend.validation.key-is-not-a-masterkey", error.key());
        assertTrue(error.message().endsWith("is not a master key"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");
    }

    @Test
    @WithMockUser
    public void exportPublicKeysIDsMissing()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");

        List<UUID> ids = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, ids);

        mockMvc.perform(post(RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("id's are missing"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-public-keys");
    }

    @Test
    @WithMockUser
    public void exportSecretKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");

        List<UUID> ids = new LinkedList<>();

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                ids.add(entry.getID());
            }
        }

        assertEquals(2, ids.size());

        when(keyRingExporter.exportSecretKeys(any(), any(), any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new PGPDTO.ExportSecretKeysRequestBody(ids, "some password"));

        mockMvc.perform(post(RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
                        .param("password", "some password")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(keyRingExporter).exportSecretKeys(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");
    }

    @Test
    @WithMockUser
    public void exportSecretKeysNoPermission()
    throws Exception
    {
        List<UUID> ids = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new PGPDTO.ExportSecretKeysRequestBody(ids, "some password"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
                        .param("password", "some password")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(keyRingExporter, never()).exportSecretKeys(any(), any(), any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");
    }

    @Test
    @WithMockUser
    public void exportSecretKeysNotAMasterKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");

        List<UUID> ids = new LinkedList<>();

        UUID subkeyID = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                ids.add(entry.getID());
            }
            else if (subkeyID == null) {
                subkeyID = entry.getID();
            }
        }

        assertNotNull(subkeyID);
        ids.add(subkeyID);

        assertEquals(3, ids.size());

        when(keyRingExporter.exportSecretKeys(any(), any(), any())).thenReturn(true);

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new PGPDTO.ExportSecretKeysRequestBody(ids, "some password"));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
                        .param("password", "some password")
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("com.ciphermail.core.app.properties.PropertyValidationFailedException", error.className());
        assertEquals("backend.validation.key-is-not-a-masterkey", error.key());
        assertTrue(error.message().endsWith("is not a master key"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");
    }

    @Test
    @WithMockUser
    public void exportSecretKeysIDsMissing()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");

        List<UUID> ids = new LinkedList<>();

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, new PGPDTO.ExportSecretKeysRequestBody(ids, "some password"));

        mockMvc.perform(post(RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason("id's are missing"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "export-secret-keys");
    }

    @Test
    @WithMockUser
    public void setPGPKeyEmailAndDomains()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-pgp-key-email-and-domains");

        PGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = entry;
            }
        }

        assertNotNull(masterKeyEntry);

        List<String> emailAndDomains = List.of("test1@example.com", "test2@exaple.com", "one.example.com",
                "two.example.com");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, emailAndDomains);

        mockMvc.perform(post(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .param("id", masterKeyEntry.getID().toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        // test email only
        emailAndDomains = List.of("test1@example.com", "test2@exaple.com");

        jsonWriter = new StringWriter();
        mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, emailAndDomains);

        mockMvc.perform(post(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .param("id", masterKeyEntry.getID().toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        // test domain only
        emailAndDomains = List.of("one.example.com", "two.exaple.com");

        jsonWriter = new StringWriter();
        mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, emailAndDomains);

        mockMvc.perform(post(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .param("id", masterKeyEntry.getID().toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(permissionChecker, times(3)).checkPermission(PermissionCategory.PGP,
                "set-pgp-key-email-and-domains");
    }

    @Test
    @WithMockUser
    public void setPGPKeyEmailAndDomainsNoPermission()
    throws Exception
    {
        List<String> emailAndDomains = List.of("test1@example.com", "test2@exaple.com",
                "one.example.com", "two.example.com");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, emailAndDomains);

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-pgp-key-email-and-domains");
    }

    @Test
    @WithMockUser
    public void setPGPKeyEmailAndDomainsInvalid()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-pgp-key-email-and-domains");

        List<String> emailAndDomains = List.of("invalid", "example.com");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, emailAndDomains);

        mockMvc.perform(post(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason(String.format("%s is not a valid email address or domain", "invalid")));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-pgp-key-email-and-domains");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");

        when(encryptionKeySelector.select(any())).thenReturn(new HashSet<>(keyRingEntries));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(8, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForEmailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForEmailNoMatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");

        when(encryptionKeySelector.select(any())).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(0, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForEmailInvalidEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
                        .param("email", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("invalid is not a valid email address", error.message());
        assertEquals("backend.validation.invalid-email-address", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");

        when(encryptionKeySelector.select(any())).thenReturn(new HashSet<>(keyRingEntries));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(8, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");
    }


    @Test
    @WithMockUser
    public void getEncryptionKeysForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForDomainNoMatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");

        when(encryptionKeySelector.select(any())).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(0, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getEncryptionKeysForDomainInvalidDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                "\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"" +
                ",\"error\":\"Bad Request\",\"message\":\"invalid is not a valid domain\"," +
                "\"key\":\"backend.validation.invalid-domain\",\"params\":null,\"path\":\"\"}"));

        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError propertyValidationFailedError =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError.class);

        assertEquals("invalid is not a valid domain", propertyValidationFailedError.message());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-encryption-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");

        when(signingKeySelector.select(any())).thenReturn(new HashSet<>(keyRingEntries));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(8, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForEmailNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForEmailNoMatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");

        when(signingKeySelector.select(any())).thenReturn(null);

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(0, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForEmailInvalidEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
                        .param("email", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("invalid is not a valid email address", error.message());
        assertEquals("backend.validation.invalid-email-address", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "available-signing-keys-for-email");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");

        when(signingKeySelector.select(any())).thenReturn(new HashSet<>(keyRingEntries));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(8, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForDomainNoMatch()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");

        when(signingKeySelector.select(any())).thenReturn(new HashSet<>());

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails[] keyDetailsResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails[].class);

        assertEquals(0, keyDetailsResult.length);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getAvailableSigningKeysForDomainInvalidDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
                        .param("domain", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                "\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"" +
                ",\"error\":\"Bad Request\",\"message\":\"invalid is not a valid domain\"," +
                "\"key\":\"backend.validation.invalid-domain\",\"params\":null,\"path\":\"\"}"));

        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError propertyValidationFailedError =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError.class);

        assertEquals("invalid is not a valid domain", propertyValidationFailedError.message());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-available-signing-keys-for-domain");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");

        when(userWorkflow.getUser(any(), any())).thenAnswer(invocation ->
                new MockupUser(invocation.getArgument(0), new MockupUserPreferences(invocation.getArgument(0),
                        UserPreferencesCategory.USER.name()),
                        null));

        when(userPreferencesPGPSigningKeySelector.select(any())).thenReturn(keyRingEntries.get(0));

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(createMockPGPKeyDetails(0));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails keyDetailResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails.class);

        assertNotNull(keyDetailResult);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForUserNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForUserNonExistingUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");

        mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "test@example.com"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("User with email address %s not found", "test@example.com")));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForUserInvalidEmail()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("invalid is not a valid email address", error.message());
        assertEquals("backend.validation.invalid-email-address", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");

        when(domainManager.getDomainPreferences(any())).thenAnswer(invocation ->
                new MockupUserPreferences(invocation.getArgument(0), UserPreferencesCategory.DOMAIN.name()));

        when(userPreferencesPGPSigningKeySelector.select(any())).thenReturn(keyRingEntries.get(0));

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(createMockPGPKeyDetails(0));

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.PGPKeyDetails keyDetailResult = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.PGPKeyDetails.class);

        assertNotNull(keyDetailResult);

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");
    }


    @Test
    @WithMockUser
    public void getSigningKeyForDomainNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForDomainNonExistingDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");

        mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "example.com"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("Domain %s not found", "example.com")));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");
    }

    @Test
    @WithMockUser
    public void getSigningKeyForDomainInvalidDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");

        MvcResult result = mockMvc.perform(get(RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "invalid"))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        assertTrue(result.getResponse().getContentAsString().endsWith(
                "\"status\":400,\"className\":\"com.ciphermail.core.app.properties.PropertyValidationFailedException\"" +
                ",\"error\":\"Bad Request\",\"message\":\"invalid is not a valid domain\"," +
                "\"key\":\"backend.validation.invalid-domain\",\"params\":null,\"path\":\"\"}"));

        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError propertyValidationFailedError =
                JacksonUtil.getObjectMapper().readValue(result.getResponse().getContentAsString(),
                        PropertyValidationFailedExceptionHandler.PropertyValidationFailedError.class);

        assertEquals("invalid is not a valid domain", propertyValidationFailedError.message());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "get-signing-key-for-domain");
    }

    @Test
    @WithMockUser
    public void setSigningKeyForUser()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-user");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        when(userWorkflow.getUser(any(), any())).thenAnswer(invocation ->
                new MockupUser(invocation.getArgument(0), new MockupUserPreferences(invocation.getArgument(0),
                        UserPreferencesCategory.USER.name()),
                        null));

        mockMvc.perform(post(RestPaths.PGP_SET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("id", masterKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void setSigningKeyForUserNoPermission()
    throws Exception
    {
        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        when(userWorkflow.getUser(any(), any())).thenAnswer(invocation ->
                new MockupUser(invocation.getArgument(0), new MockupUserPreferences(invocation.getArgument(0),
                        UserPreferencesCategory.USER.name()),
                        null));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_SET_SIGNING_KEY_FOR_USER_PATH)
                        .param("email", "test@example.com")
                        .param("id", masterKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-user");
    }

    @Test
    @WithMockUser
    public void setSigningKeyForDomain()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-domain");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }
        when(domainManager.getDomainPreferences(any())).thenAnswer(invocation ->
                new MockupUserPreferences(invocation.getArgument(0), UserPreferencesCategory.DOMAIN.name()));

        mockMvc.perform(post(RestPaths.PGP_SET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("id", masterKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-domain");
    }

    @Test
    @WithMockUser
    public void setSigningKeyForDomainNoPermission()
    throws Exception
    {
        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }
        when(domainManager.getDomainPreferences(any())).thenAnswer(invocation ->
                new MockupUserPreferences(invocation.getArgument(0), UserPreferencesCategory.DOMAIN.name()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_SET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .param("domain", "example.com")
                        .param("id", masterKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "set-signing-key-for-domain");
    }

    @Test
    @WithMockUser
    public void generateSecretKeyRing()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "generate-secret-key-ring");

        PGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = entry;
            }
        }

        assertNotNull(masterKeyEntry);

        final PGPKeyRingEntry finalMasterKeyEntry = masterKeyEntry;

        when(detailsFactory.createPGPKeyDetails(any())).thenReturn(createMockPGPKeyDetails(0));

        when(secretKeyRequestor.requestSecretKey(any(), any(), any(), anyBoolean())).thenReturn(
                new PGPSecretKeyRequestorResult()
                {
                    @Override
                    public PGPKeyRingEntry getMasterKeyEntry() {
                        return finalMasterKeyEntry;
                    }

                    @Override
                    public KeyServerClientSubmitResult getKeyServerClientSubmitResult()
                    {
                        return new KeyServerClientSubmitResultImpl(
                                List.of("response1", "response2"),
                                List.of("url1", "url2"),
                                List.of("error1", "error2"));
                    }
                });

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_GENERATE_SECRET_KEY_RING_PATH)
                        .param("email", "test@example.com")
                        .param("name", "test user")
                        .param("keyType", PGPKeyGenerationType.ECC_NIST_P_384.name())
                        .param("publishKeys", "true")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andReturn();

        PGPDTO.GenerateSecretKeyRingDetails secretKeyRingDetails = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), PGPDTO.GenerateSecretKeyRingDetails.class);

        assertNotNull(secretKeyRingDetails.pgpKey());
        assertNotNull(secretKeyRingDetails.keyServerResult());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "generate-secret-key-ring");
    }

    @Test
    @WithMockUser
    public void generateSecretKeyRingNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_GENERATE_SECRET_KEY_RING_PATH)
                        .param("email", "test@example.com")
                        .param("name", "test user")
                        .param("keyType", PGPKeyGenerationType.ECC_NIST_P_384.name())
                        .param("publishKeys", "true")
                        .with(csrf()))
            .andExpect(status().isForbidden())
            .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "generate-secret-key-ring");
    }

    @Test
    @WithMockUser
    public void generateSecretKeyRingInvalidEmailAddress()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "generate-secret-key-ring");

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_GENERATE_SECRET_KEY_RING_PATH)
                        .param("email", "invalid")
                        .param("name", "test user")
                        .param("keyType", PGPKeyGenerationType.ECC_NIST_P_384.name())
                        .param("publishKeys", "true")
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andReturn();

        RestExceptionHandler.RestError error = JacksonUtil.getObjectMapper().readValue(
                result.getResponse().getContentAsString(), RestExceptionHandler.RestError.class);

        assertEquals("invalid is not a valid email address", error.message());
        assertEquals("backend.validation.invalid-email-address", error.key());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "generate-secret-key-ring");
    }

    @Test
    @WithMockUser
    public void revokeMasterKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        // generate a dummy private key
        // Note: the key does not match the public key but we just need a private key for testing
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPair = kpg.generateKeyPair();

        masterKeyEntry.setPrivateKey(new JcaPGPPrivateKey(0, keyPair.getPrivate()));

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                .param("id", masterKeyEntry.getID().toString())
                .with(csrf()))
                .andExpect(status().isOk());

        verify(revoker).revokeKeyRingEntry(masterKeyEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeMasterKeyNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                        .param("id", UUID.randomUUID().toString())
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(revoker, never()).revokeKeyRingEntry(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeSubKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        StaticPGPKeyRingEntry subKeyEntry = (StaticPGPKeyRingEntry) masterKeyEntry.getSubkeys().iterator().next();

        assertNotNull(subKeyEntry);

        // generate a dummy private key
        // Note: the key does not match the public key but we just need a private key for testing
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPair = kpg.generateKeyPair();

        masterKeyEntry.setPrivateKey(new JcaPGPPrivateKey(0, keyPair.getPrivate()));

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                        .param("id", subKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(revoker).revokeKeyRingEntry(subKeyEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeMasterKeyNoPrivateKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                        .param("id", masterKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason(String.format("Master key with id %s does not have a private key",
                        masterKeyEntry.getID())));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeSubKeyNoPrivateKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        StaticPGPKeyRingEntry subKeyEntry = (StaticPGPKeyRingEntry) masterKeyEntry.getSubkeys().iterator().next();

        assertNotNull(subKeyEntry);

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                        .param("id", subKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason(String.format("Master key with id %s does not have a private key",
                        masterKeyEntry.getID())));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeSubKeyMissingMasterKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        StaticPGPKeyRingEntry subKeyEntry = (StaticPGPKeyRingEntry) masterKeyEntry.getSubkeys().iterator().next();

        assertNotNull(subKeyEntry);

        subKeyEntry.setParentKey(null);

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEY_PATH)
                        .param("id", subKeyEntry.getID().toString())
                        .with(csrf()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(status().reason(String.format("Key with id %s does not have a master key",
                        subKeyEntry.getID())));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-key");
    }

    @Test
    @WithMockUser
    public void revokeKeys()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-keys");

        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        // generate a dummy private key
        // Note: the key does not match the public key but we just need a private key for testing
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPair = kpg.generateKeyPair();

        masterKeyEntry.setPrivateKey(new JcaPGPPrivateKey(0, keyPair.getPrivate()));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(masterKeyEntry.getID()));

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(revoker).revokeKeyRingEntry(masterKeyEntry.getID());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-keys");
    }

    @Test
    @WithMockUser
    public void revokeKeysNoMatchingKey()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-keys");

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(UUID.randomUUID()));

        mockMvc.perform(post(RestPaths.PGP_REVOKE_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isOk());

        verify(revoker, never()).revokeKeyRingEntry(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-keys");
    }

    @Test
    @WithMockUser
    public void revokeKeysNoPermission()
    throws Exception
    {
        StaticPGPKeyRingEntry masterKeyEntry = null;

        for (PGPKeyRingEntry entry : keyRingEntries)
        {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);

            if (entry.isMasterKey()) {
                masterKeyEntry = (StaticPGPKeyRingEntry) entry;
            }
        }

        assertNotNull(masterKeyEntry);

        // generate a dummy private key
        // Note: the key does not match the public key but we just need a private key for testing
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair keyPair = kpg.generateKeyPair();

        masterKeyEntry.setPrivateKey(new JcaPGPPrivateKey(0, keyPair.getPrivate()));

        StringWriter jsonWriter = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(jsonWriter, List.of(masterKeyEntry.getID()));

        MvcResult result = mockMvc.perform(post(RestPaths.PGP_REVOKE_KEYS_PATH)
                        .content(jsonWriter.toString())
                        .contentType("application/json")
                        .with(csrf()))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(revoker, never()).revokeKeyRingEntry(any());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "revoke-keys");
    }

    @Test
    @WithMockUser
    public void isUserIDValid()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");

        for (PGPKeyRingEntry entry : keyRingEntries) {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);
        }

        when(userIDValidator.validateUserID(any(), any())).thenReturn(true);

        mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_VALID_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");
    }

    @Test
    @WithMockUser
    public void isUserIDValidNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_VALID_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");
    }

    @Test
    @WithMockUser
    public void isUserIDValidRevokedUserID()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");

        for (PGPKeyRingEntry entry : keyRingEntries) {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);
        }

        when(userIDValidator.validateUserID(any(), any())).thenThrow(new PGPUserIDRevokedException(
                "some-user-id", 1, "forced"));

        mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_VALID_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");
    }

    @Test
    @WithMockUser
    public void isUserIDValidMissingEntry()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");

        mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_VALID_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().is(HttpStatus.NOT_FOUND.value()))
                .andExpect(status().reason(String.format("Key with id %s not found", keyRingEntries.get(0).getID())));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-valid");
    }

    @Test
    @WithMockUser
    public void isUserIDRevoked()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-revoked");

        for (PGPKeyRingEntry entry : keyRingEntries) {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);
        }

        when(userIDrevocationChecker.isRevoked(any(), any())).thenReturn(true);

        mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_REVOKED_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-revoked");
    }

    @Test
    @WithMockUser
    public void isUserIDRevokedNoPermission()
    throws Exception
    {
        MvcResult result = mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_REVOKED_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isForbidden())
                .andReturn();

        assertEquals("Default denied", result.getResponse().getContentAsString());

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-revoked");
    }

    @Test
    @WithMockUser
    public void isUserIDRevokedException()
    throws Exception
    {
        doNothing().when(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-revoked");

        for (PGPKeyRingEntry entry : keyRingEntries) {
            when(keyRing.getByID(entry.getID())).thenReturn(entry);
        }

        when(userIDrevocationChecker.isRevoked(any(), any())).thenThrow(new RuntimeException("forced"));

        mockMvc.perform(get(RestPaths.PGP_IS_USER_ID_REVOKED_PATH)
                        .param("id", keyRingEntries.get(0).getID().toString())
                        .param("userID", "test"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));

        verify(permissionChecker).checkPermission(PermissionCategory.PGP,
                "is-user-id-revoked");
    }
}