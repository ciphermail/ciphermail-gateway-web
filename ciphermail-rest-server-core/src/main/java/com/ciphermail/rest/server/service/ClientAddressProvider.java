/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.util.AddressUtils;
import com.google.common.net.InetAddresses;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Service which returns the client IP address from the request. If an X-Forward-For header is present and checking
 * X-Forward-For is enabled, the client IP from the X-Forward-For header is returned.
 */
@SuppressWarnings("java:S6813")
@Component
public class ClientAddressProvider
{
    private static final Logger logger = LoggerFactory.getLogger(ClientAddressProvider.class);

    @Value("${ciphermail.rest.authentication.forwarded-for-header:X-Forwarded-For}")
    private String forwardForHeader;

    /*
     * If set, X-Forwarded-For will be locally checked for the remote IP. If the system property
     * server.forward-headers-strategy is set, spring boot will handle proxy forward headers
     */
    @Value("${ciphermail.rest.authentication.check-forwarded-for:false}")
    private boolean checkForwardForHeader;

    /*
     * Which index of the list of values from X-Forward-For should be used. -1 means the value before the last
     */
    @Value("${ciphermail.rest.authentication.trusted-proxy-index:-1}")
    private int trustedProxyIndex;

    @Autowired
    private HttpServletRequest request;

    /**
     * Returns the IP address of the remote client. If an X-Forward-For header is present and checking
     * X-Forward-For is enabled, the client IP from the X-Forward-For header is returned.
     * <p>
     * If the system property server.forward-headers-strategy is set, spring boot will handle proxy forward headers
     */
    public String getRemoteAddress()
    {
        String remoteAddress = null;

        if (checkForwardForHeader)
        {
            String xForwardFor = StringUtils.trimToNull(request.getHeader(forwardForHeader));

            if (xForwardFor != null)
            {
                String[] clients = StringUtils.stripAll(xForwardFor.split(","));

                remoteAddress = ArrayUtils.get(clients, trustedProxyIndex == -1 ? clients.length - 1 :
                        trustedProxyIndex);

                if (remoteAddress == null) {
                    logger.warn("trustedProxyIndex out of range. X-Forward-for: {}, trustedProxyIndex: {}", xForwardFor,
                            trustedProxyIndex);
                }
            }
        }

        if (remoteAddress == null) {
            remoteAddress = request.getRemoteAddr();
        }

        // if the client IP is an ipv6 address, we need to return the 64bit network prefix otherwise an attacker
        // can use 64bit addresses to brute force
        InetAddress address = InetAddresses.forString(remoteAddress);

        if (address instanceof Inet6Address) {
            try {
                remoteAddress = AddressUtils.getIPv6NetworkPrefix(address.getHostAddress());
            }
            catch (UnknownHostException e) {
                logger.error("Error getting IPv6 network prefix", e);
            }
        }

        return remoteAddress;
    }
}