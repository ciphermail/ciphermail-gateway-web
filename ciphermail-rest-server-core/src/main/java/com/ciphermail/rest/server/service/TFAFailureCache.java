package com.ciphermail.rest.server.service;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.Duration;

/**
 * Service which keeps track of 2FA failures. If the number of 2FA failures exceeds the max,
 * {@link TFAFailureCache#isBanned(String)}  returns true. Failed 2FA attempts will be cached for some time
 * after which they will be removed from the cache.
 */
@SuppressWarnings("java:S6813")
@Component
public class TFAFailureCache
{
    private static final Logger logger = LoggerFactory.getLogger(TFAFailureCache.class);

    @Value("${ciphermail.rest.authentication.max-2fa-failures}")
    private int maxFailures;

    /*
     * Limits the size of the cached username to prevent memory exhaustion attack
     */
    @Value("${ciphermail.rest.authentication.failure-cache-max-username-length:64}")
    private int maxUsernameLength;

    /*
     * Cache of all login failures
     */
    private final LoadingCache<String, Integer> failureCache;

    public TFAFailureCache(
            @Value("${ciphermail.rest.authentication.2fa-failure-lifetime-seconds}") int failureLifetimeSeconds,
            @Value("${ciphermail.rest.authentication.2fa-failure-max-cached-items}") int failureMaxCachedItems)
    {
        super();

        failureCache = Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofSeconds(failureLifetimeSeconds))
                .maximumSize(failureMaxCachedItems)
                .build(
                    new CacheLoader<>() {
                        @Override
                        @Nonnull public Integer load(@Nonnull String key) {
                            // set initial value to 0
                            return 0;
                        }
                    });
    }

    private String getKey(String username) {
        return StringUtils.left(StringUtils.lowerCase(username), maxUsernameLength);
    }

    public void registerFailure(String username)
    {
        int attempts;

        String key = getKey(username);

        attempts = failureCache.get(key);

        attempts++;

        failureCache.put(key, attempts);

        logger.debug("failureCache size {}", failureCache.estimatedSize());
    }

    public boolean isBanned(String username) {
        return failureCache.get(getKey(username)) >= maxFailures;
    }
}