/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Nonnull;

/**
 * Factory service for creating {@link  ResponseStatusException} instances
 */
@SuppressWarnings({"java:S6813"})
@Component
public class ResponseStatusExceptionFactory
{
    @Autowired
    private ClientAddressProvider clientAddressProvider;

    @Autowired
    private AuthenticatedUserProvider authenticatedUserProvider;

    private String getClientDetails()
    {
        return String.format("[Client remote address: %s, Authenticated user: %s]",
                clientAddressProvider.getRemoteAddress(), authenticatedUserProvider.getAuthenticatedUser());
    }

    public ResponseStatusException createResponseStatusException(@Nonnull Exception e, Logger logger)
    {
        if (logger != null) {
            logger.atError().setCause(e).log("Internal error {}", getClientDetails());
        }

        return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
    }

    public ResponseStatusException createResponseStatusException(@Nonnull HttpStatusCode status,
            @Nonnull String message, Logger logger)
    {
        if (logger != null) {
            logger.atWarn().log("{} {}", message, getClientDetails());
        }

        return new ResponseStatusException(status, message);
    }

    public ResponseStatusException createResponseStatusException(@Nonnull HttpStatusCode status,
            @Nonnull String message, @Nonnull Exception exception, Logger logger)
    {
        if (logger != null) {
            logger.atError().setCause(exception).log("{} {}", message, getClientDetails());
        }

        return new ResponseStatusException(status, message, exception);
    }
}
