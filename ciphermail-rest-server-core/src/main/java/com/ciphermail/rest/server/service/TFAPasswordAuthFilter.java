/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.server.service;

import com.ciphermail.core.common.security.otp.TOTP;
import com.ciphermail.core.common.security.otp.TOTPValidationFailureException;
import com.ciphermail.core.common.util.Base32Utils;
import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Objects;

public class TFAPasswordAuthFilter extends OncePerRequestFilter
{
    private static final Logger slf4jLogger = LoggerFactory.getLogger(TFAPasswordAuthFilter.class);

    private static final String TOTP_HEADER_NAME = "TOTP";

    /*
     * Provides user details service for OTP (One-Time Password) based authentication.
     * This service is used to load user-specific data during authentication, particularly
     * to fetch user details including their OTP secret.
     */
    private final UserDetailsService userDetailsService;

    /*
     * An implementation of TOTP used for generating and verifying Time-based One-Time Passwords.
     * Used within the OTPPasswordAuthFilter to handle OTP generation and validation for user authentication.
     */
    private final TOTP totp;

    /*
     * A cache that tracks two-factor authentication (2FA) failures.
     * It helps to monitor the number of failed attempts for each user, and enforces a limit
     * on the maximum number of allowed failures before the user is banned.
     * The failures are temporarily cached to prevent excessive memory usage.
     */
    private final TFAFailureCache failureCache;

    /*
     * Throws when the TOTP header is missing
     */
    static class TOTPCodeMissingException extends Exception {}

    public TFAPasswordAuthFilter(
            @Nonnull UserDetailsService userDetailsService,
            @Nonnull TOTP totp,
            @Nonnull TFAFailureCache failureCache)
    {
        this.userDetailsService = Objects.requireNonNull(userDetailsService);
        this.totp = Objects.requireNonNull(totp);
        this.failureCache = Objects.requireNonNull(failureCache);
    }

    private void validateOTP(
            @Nonnull HttpServletRequest request,
            @Nonnull UsernamePasswordAuthenticationToken authenticationToken,
            @Nonnull TFAUserDetails otpUserDetails)
    throws IOException, TOTPCodeMissingException
    {
        String otp = request.getHeader(TOTP_HEADER_NAME);

        if (StringUtils.isBlank(otp))
        {
            logger.info("OTP code not set");

            throw new TOTPCodeMissingException();
        }

        if (failureCache.isBanned(otpUserDetails.getUsername()))
        {
            String failureMessage = "Too many 2FA failures for user %s. User will be banned."
                    .formatted(otpUserDetails.getUsername());

            slf4jLogger.warn(failureMessage);

            throw new BadCredentialsException(failureMessage);
        }

        try {
            totp.verifyOTP(Base32Utils.base32Decode(otpUserDetails.getOtpSecret()), otp);

            SecurityContextHolder.getContext().setAuthentication(new TFAUsernamePasswordAuthenticationToken(
                    authenticationToken));
        }
        catch (TOTPValidationFailureException e)
        {
            failureCache.registerFailure(otpUserDetails.getUsername());

            String failureMessage = "TOTP verification failure for user %s"
                    .formatted(otpUserDetails.getUsername());

            logger.warn(failureMessage);

            throw new BadCredentialsException(failureMessage);
        }
    }

    @Override
    protected void doFilterInternal(
            @Nonnull HttpServletRequest request,
            @Nonnull HttpServletResponse response,
            @Nonnull FilterChain filterChain)
    throws ServletException, IOException
    {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            if (authentication != null)
            {
                if (authentication.isAuthenticated())
                {
                    if (authentication instanceof UsernamePasswordAuthenticationToken authenticationToken) {
                        UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationToken.getName());

                        if (userDetails instanceof TFAUserDetails otpUserDetails) {
                            validateOTP(request, authenticationToken, otpUserDetails);
                        }
                        else {
                            slf4jLogger.debug("userDetails is not a TFAUserDetails");
                        }
                    }
                    else {
                        slf4jLogger.debug("authentication is not a UsernamePasswordAuthenticationToken but a {}",
                                authentication.getClass());
                    }
                }
                else {
                    slf4jLogger.debug("User is not authenticated");
                }
            }
            else {
                slf4jLogger.debug("authentication is null");
            }

            filterChain.doFilter(request, response);
        }
        catch (TOTPCodeMissingException e)
        {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setHeader("WWW-Authenticate", "TOTP");
        }
    }
}
