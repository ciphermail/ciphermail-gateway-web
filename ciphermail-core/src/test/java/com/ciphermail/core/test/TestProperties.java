/*
 * Copyright (c) 2020-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.UnhandledException;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * This class provides access to system properties used for test configurations.
 */
public class TestProperties
{
    /*
     * The system property that configures the test performance factor
     */
    private static final String TEST_PERFORMANCE_FACTOR_SYSTEM_PROPERTY = "cm.test.performance-factor";

    /*
     * The system property that configures whether the Docker compose service names should be used or whether (local) IP
     * addresses should be use
     */
    private static final String DOCKER_LOCALHOST_SYSTEM_PROPERTY = "cm.test.docker-localhost";

    /*
     * The system property that configures the URL of the EJBCA server
     */
    private static final String EJBCA_URL_SYSTEM_PROPERTY = "cm.test.ejbca.url";

    /*
     * The system property that configures the URL of the test PGP key server 1
     */
    private static final String PGP_KEYSERVER_URL_1_SYSTEM_PROPERTY = "cm.test.pgp.keyserver.url.1";

    /*
     * The system property that configures the URL of the test PGP key server 1 resolvable from the proxy server
     */
    private static final String PGP_KEYSERVER_PROXY_URL_1_SYSTEM_PROPERTY = "cm.test.pgp.keyserver.proxy.url.1";

    /*
     * The system property that configures the URL of the test PGP key server 2
     */
    private static final String PGP_KEYSERVER_URL_2_SYSTEM_PROPERTY = "cm.test.pgp.keyserver.url.2";

    /*
     * The system property that configures the URL of the test PGP key server 2 resolvable from the proxy server
     */
    private static final String PGP_KEYSERVER_PROXY_URL_2_SYSTEM_PROPERTY = "cm.test.pgp.keyserver.proxy.url.2";

    /*
     * The system property that configures the URL of the test HTTP download server
     */
    private static final String HTTP_DOWNLOAD_SERVER_URL_SYSTEM_PROPERTY = "cm.test.http.download.url";

    /*
     * The system property that configures the URL of the test HTTP download server resolvable from the proxy server
     */
    private static final String HTTP_DOWNLOAD_SERVER_PROXY_URL_SYSTEM_PROPERTY = "cm.test.http.download.proxy.url";

    /*
     * The system property that configures the URL of the wiremock server
     */
    private static final String WIREMOCK_URL_SYSTEM_PROPERTY = "cm.test.wiremock.url";

    /*
     * The system property that configures the URL of the wiremock server resolvable from the proxy server
     */
    private static final String WIREMOCK_PROXY_URL_SYSTEM_PROPERTY = "cm.test.wiremock.proxy.url";

    /*
     * The system property that configures the URL of the test proxy server
     */
    private static final String HTTP_PROXY_URL_SYSTEM_PROPERTY = "cm.test.http.proxy.url";

    /*
     * Some unit tests test the performance of the unit. To make it possible to run the tests on slow and fast systems
     * without failing the performance tests, a performance factor can be configured which lowers or increases the
     * expected result.
     */
    private static final float TEST_PERFORMANCE_FACTOR;

    /*
     * If true the Docker compose service names should be used or whether (local) IP addresses should be use
     */
    private static final boolean DOCKER_LOCALHOST;

    /*
     * The URL of test EJBCA server
     */
    private static final String EJBCA_SERVER_URL;

    /*
     * The URL of test HTTP download server
     */
    private static final String HTTP_DOWNLOAD_SERVER_URL;

    /*
     * The URL of test HTTP download server resolvable from the proxy server
     */
    private static final String HTTP_DOWNLOAD_SERVER_PROXY_URL;

    /*
     * The URL of test PGP key server 1
     */
    private static final String PGP_KEYSERVER_URL_1;

    /*
     * The URL of test PGP key server 1 resolvable from the proxy server
     */
    private static final String PGP_KEYSERVER_PROXY_URL_1;

    /*
     * The URL of test PGP key server 2
     */
    private static final String PGP_KEYSERVER_URL_2;

    /*
     * The URL of test PGP key server 2 resolvable from the proxy server
     */
    private static final String PGP_KEYSERVER_PROXY_URL_2;

    /*
     * The URL of the test proxy server
     */
    private static final String HTTP_PROXY_URL;

    /*
     * The URL of wiremock server
     */
    private static final String WIREMOCK_URL;

    /*
     * The URL of wiremock server resolvable from the proxy server
     */
    private static final String WIREMOCK_PROXY_URL;

    private static String resolveHostname(String hostname)
    {
        try {
            return InetAddress.getByName(hostname).getHostAddress();
        }
        catch (UnknownHostException e) {
            throw new UnhandledException(e);
        }
    }

    private static String resolveEJBCAURL() {
        return "http://" + resolveHostname("ejbca") + ":8080";
    }

    private static String resolveSKS1URL() {
        return "http://" + resolveHostname("sks") + ":11371";
    }

    private static String resolveSKS2URL() {
        return "http://" + resolveHostname("sks2") + ":11371";
    }

    private static String resolveHttpdURL() {
        return "http://" + resolveHostname("httpd");
    }

    private static String resolveWiremockURL() {
        return "http://" + resolveHostname("wiremock") + ":8080";
    }

    private static String resolveSquidURL() {
        return "http://" + resolveHostname("squid") + ":3128";
    }

    static {
        TEST_PERFORMANCE_FACTOR = Float.parseFloat(System.getProperty(TEST_PERFORMANCE_FACTOR_SYSTEM_PROPERTY, "1"));

        DOCKER_LOCALHOST = BooleanUtils.toBoolean(System.getProperty(DOCKER_LOCALHOST_SYSTEM_PROPERTY, "true"));

        EJBCA_SERVER_URL = System.getProperty(EJBCA_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.1:8002" : resolveEJBCAURL());

        HTTP_DOWNLOAD_SERVER_URL = System.getProperty(HTTP_DOWNLOAD_SERVER_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.1:8000": resolveHttpdURL());
        HTTP_DOWNLOAD_SERVER_PROXY_URL = System.getProperty(HTTP_DOWNLOAD_SERVER_PROXY_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://httpd" : resolveHttpdURL());

        PGP_KEYSERVER_URL_1 = System.getProperty(PGP_KEYSERVER_URL_1_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.1:11371" : resolveSKS1URL());
        PGP_KEYSERVER_PROXY_URL_1 = System.getProperty(PGP_KEYSERVER_PROXY_URL_1_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://sks:11371" : resolveSKS1URL());

        PGP_KEYSERVER_URL_2 = System.getProperty(PGP_KEYSERVER_URL_2_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.2:11371" : resolveSKS2URL());
        PGP_KEYSERVER_PROXY_URL_2 = System.getProperty(PGP_KEYSERVER_PROXY_URL_2_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://sks2:11371" : resolveSKS2URL());

        HTTP_PROXY_URL = System.getProperty(HTTP_PROXY_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.1:3128" : resolveSquidURL());

        WIREMOCK_URL = System.getProperty(WIREMOCK_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://127.0.0.1:8001": resolveWiremockURL());
        WIREMOCK_PROXY_URL = System.getProperty(WIREMOCK_PROXY_URL_SYSTEM_PROPERTY,
                DOCKER_LOCALHOST ? "http://wiremock:8080" : resolveWiremockURL());
    }

    public static String getEJBCAServerURL() {
        return EJBCA_SERVER_URL;
    }

    public static String getPGPKeyserverURL1() {
        return PGP_KEYSERVER_URL_1;
    }

    public static String getPGPKeyserverProxyURL1() {
        return PGP_KEYSERVER_PROXY_URL_1;
    }

    public static String getPGPKeyserverURL2() {
        return PGP_KEYSERVER_URL_2;
    }

    public static String getPGPKeyserverProxyURL2() {
        return PGP_KEYSERVER_PROXY_URL_2;
    }

    public static String getHTTPDownloadServerURL() {
        return HTTP_DOWNLOAD_SERVER_URL;
    }

    public static String getHTTPDownloadServerProxyURL() {
        return HTTP_DOWNLOAD_SERVER_PROXY_URL;
    }

    public static String getWiremockURL() {
        return WIREMOCK_URL;
    }

    public static String getWiremockProxyURL() {
        return WIREMOCK_PROXY_URL;
    }

    public static String getHTTPProxyURL() {
        return HTTP_PROXY_URL;
    }

    public static float getTestPerformanceFactor() {
        return TEST_PERFORMANCE_FACTOR;
    }
}
