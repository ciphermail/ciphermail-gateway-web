/*
 * Copyright (c) 2008-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.BooleanUtils;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;

/**
 * Helper class for openssl make it possible to use openssl for additional tests like
 * interoperability checking between openssl and Java.
 *
 * Note:
 *
 * Only use this class for testing purposes with private keys that need not be secured. Private keys will be saved to
 * the temp directory.
 *
 * !!! DO NOT USE FOR PRODUCTION. ONLY FOR TESTING PURPOSES !!!
 *
 * @author Martijn Brinkers
 *
 */
public class OpenSSL
{
    /*
     * Path to the openssl binary file.
     *
     * Note: this should be and openssl version which supports SHA256WITHRSAANDMGF1
     */
    private static final File SYSTEM_OPENSSL = new File("openssl");

    /*
     * Path to the local provided openssl binary file.
     */
    private static final File LOCAL_OPENSSL = new File(TestUtils.getTestResourcesBaseDir(), "bin/cm-openssl");

    /*
     * The CAs used when verifying signatures
     */
    private Collection<X509Certificate> certificateAuthorities;

    /*
     * PEM file with private keys for decryption
     */
    private File privateKeyFile;

    private File getOpenSSLBinary()
    {
        /*
         * Use system OpenSSL unless the system property ciphermail.openssl.binary.use-system is set
         */
        return BooleanUtils.toBoolean(System.getProperty("ciphermail.openssl.binary.use-system", "true")) ?
                SYSTEM_OPENSSL : LOCAL_OPENSSL;
    }

    /**
     * Sets the trusted root certificates (used with verify)
     *
     * @param certificates
     */
    public void setCertificateAuthorities(Collection<X509Certificate> certificates) {
        this.certificateAuthorities = certificates;
    }

    /**
     * Sets the trusted root certificates (used with verify)
     *
     * @param certificates
     */
    public void setCertificateAuthorities(X509Certificate... certificates) {
        setCertificateAuthorities(Arrays.asList(certificates));
    }

    /**
     * The private key to be used for decrypting the message.
     *
     * Notes: The private key will be saved unprotected to the temp directory so
     * only use it for testing with private keys that need not be secured.
     *
     * @param privateKey
     */
    public void setPrivateKey(PrivateKey privateKey)
    throws IOException
    {
        if (privateKey == null) {
            throw new IllegalStateException("Private key is not set");
        }

        privateKeyFile = File.createTempFile("ciphermail-openssl", ".key");

        privateKeyFile.deleteOnExit();

        FileWriter fileWriter = new FileWriter(privateKeyFile);

        JcaPEMWriter pemWriter = new JcaPEMWriter(fileWriter);

        try {
            pemWriter.writeObject(privateKey);
        }
        finally {
            IOUtils.closeQuietly(pemWriter);
        }
    }

    /**
     * Sets the private key PEM file.
     *
     * Note: the PEM file should not be encrypted
     *
     * @param privateKeyFile
     */
    public void setPrivateKeyFile(File privateKeyFile) {
        this.privateKeyFile = privateKeyFile;
    }

    private File getCAFile()
    throws IOException
    {
        if (certificateAuthorities == null) {
            throw new IllegalStateException("certificateAuthorities key is not set");
        }

        File caFile = File.createTempFile("ciphermail-openssl", ".ca");

        caFile.deleteOnExit();

        FileWriter fileWriter = new FileWriter(caFile);

        JcaPEMWriter pemWriter = new JcaPEMWriter(fileWriter);

        try {
            for (X509Certificate certificate : certificateAuthorities) {
                pemWriter.writeObject(certificate);
            }
        }
        finally {
            IOUtils.closeQuietly(pemWriter);
        }

        return caFile;
    }

    public void cmsDecrypt(File inputFile, OutputStream output, OutputStream error)
    throws IOException, InterruptedException
    {
        String[] cmd = new String[] {
            getOpenSSLBinary().toString(),
            "cms",
            "-decrypt",
            "-in", inputFile.getAbsolutePath(),
            "-inkey", privateKeyFile.getAbsolutePath()};

        Process process = Runtime.getRuntime().exec(cmd);

        IOUtils.copy(process.getInputStream(), output);
        IOUtils.copy(process.getErrorStream(), error);

        process.waitFor();

        if (process.exitValue() > 0) {
            throw new IOException("OpenSSL error. Exit code: " + process.exitValue());
        }
    }

    public void cmsVerify(File inputFile, OutputStream output, OutputStream error)
    throws IOException, InterruptedException
    {
        String[] cmd = new String[] {
            getOpenSSLBinary().toString(),
            "cms",
            "-verify",
            "-in", inputFile.getAbsolutePath(),
            "-CAfile", getCAFile().getAbsolutePath()};

        Process process = Runtime.getRuntime().exec(cmd);

        IOUtils.copy(process.getInputStream(), output);
        IOUtils.copy(process.getErrorStream(), error);

        process.waitFor();

        if (process.exitValue() > 0) {
            throw new IOException("OpenSSL error. Exit code: " + process.exitValue());
        }
    }

    public void cmsASN1Dump(File inputFile, OutputStream output, OutputStream error)
    throws IOException, InterruptedException
    {
        String[] cmd = new String[] {
            getOpenSSLBinary().toString(),
            "cms",
            "-cmsout",
            "-print",
            "-in", inputFile.getAbsolutePath()};

        Process process = Runtime.getRuntime().exec(cmd);

        IOUtils.copy(process.getInputStream(), output);
        IOUtils.copy(process.getErrorStream(), error);

        process.waitFor();

        if (process.exitValue() > 0) {
            throw new IOException("OpenSSL error. Exit code: " + process.exitValue());
        }
    }
}
