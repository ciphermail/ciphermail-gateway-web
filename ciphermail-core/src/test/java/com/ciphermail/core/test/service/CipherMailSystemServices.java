/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.test.service;

import com.ciphermail.core.app.ClientSecretGenerator;
import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.EncryptionRecipientSelector;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserManager;
import com.ciphermail.core.app.UserPreferenceMerger;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.UserPreferencesSelector;
import com.ciphermail.core.app.admin.AdminManager;
import com.ciphermail.core.app.admin.AdminManagerImpl;
import com.ciphermail.core.app.admin.RoleManager;
import com.ciphermail.core.app.admin.RoleManagerImpl;
import com.ciphermail.core.app.ca.CAPropertiesProviderImpl;
import com.ciphermail.core.app.dlp.PolicyPatternManager;
import com.ciphermail.core.app.dlp.PolicyPatternManagerImpl;
import com.ciphermail.core.app.dlp.UpdateableWordSkipper;
import com.ciphermail.core.app.dlp.UpdateableWordSkipperImpl;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManager;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManagerImpl;
import com.ciphermail.core.app.impl.ClientSecretGeneratorImpl;
import com.ciphermail.core.app.impl.DefaultDomainManager;
import com.ciphermail.core.app.impl.DefaultEncryptionRecipientSelector;
import com.ciphermail.core.app.impl.DefaultGlobalPreferencesManager;
import com.ciphermail.core.app.impl.DefaultUserPreferenceMerger;
import com.ciphermail.core.app.impl.DefaultUserPreferencesSelector;
import com.ciphermail.core.app.impl.UserPreferencesPGPSigningKeySelectorImpl;
import com.ciphermail.core.app.impl.hibernate.UserManagerHibernate;
import com.ciphermail.core.app.impl.hibernate.UserPreferencesCategoryManagerHibernate;
import com.ciphermail.core.app.james.DefaultMessageOriginatorIdentifier;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.mail.repository.MailRepositoryMailStorer;
import com.ciphermail.core.app.openpgp.DefaultUserIDResolver;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestor;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestorImpl;
import com.ciphermail.core.app.openpgp.UserIDResolver;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientImpl;
import com.ciphermail.core.app.password.validator.UserPropertyNOutOfMRule;
import com.ciphermail.core.app.properties.CipherMailFactoryProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistryImpl;
import com.ciphermail.core.app.properties.UserPropertyRegistry;
import com.ciphermail.core.app.properties.UserPropertyRegistryImpl;
import com.ciphermail.core.app.service.CipherMailServices;
import com.ciphermail.core.app.service.CoreApplicationMailAttributesContributor;
import com.ciphermail.core.app.service.TemplateServicesContributor;
import com.ciphermail.core.app.service.TemplateServicesImpl;
import com.ciphermail.core.app.stats.StatsPublisher;
import com.ciphermail.core.app.stats.StatsPublisherImpl;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.impl.KeyAndCertificateWorkflowImpl;
import com.ciphermail.core.app.workflow.impl.UserPreferencesWorkflowImpl;
import com.ciphermail.core.app.workflow.impl.UserWorkflowImpl;
import com.ciphermail.core.common.cache.PatternCache;
import com.ciphermail.core.common.cache.SimpleMemoryPatternCache;
import com.ciphermail.core.common.csr.CSRStore;
import com.ciphermail.core.common.csr.CSRStoreImpl;
import com.ciphermail.core.common.dkim.DKIMPrivateKeyManager;
import com.ciphermail.core.common.dkim.DKIMPrivateKeyManagerImpl;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.MimeMessageTextExtractor;
import com.ciphermail.core.common.dlp.PolicyCheckerPipeline;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.dlp.TextNormalizer;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.dlp.impl.MatchFilterRegistryImpl;
import com.ciphermail.core.common.dlp.impl.MimeMessageTextExtractorImpl;
import com.ciphermail.core.common.dlp.impl.PolicyCheckerPipelineImpl;
import com.ciphermail.core.common.dlp.impl.PolicyPatternMarshallerImpl;
import com.ciphermail.core.common.dlp.impl.RegExpPolicyChecker;
import com.ciphermail.core.common.dlp.impl.TextNormalizerImpl;
import com.ciphermail.core.common.dlp.impl.ValidatorRegistryImpl;
import com.ciphermail.core.common.dlp.impl.matchfilter.MaskingFilter;
import com.ciphermail.core.common.extractor.MimeTypeDetector;
import com.ciphermail.core.common.extractor.TextExtractorFactory;
import com.ciphermail.core.common.extractor.TextExtractorFactoryRegistry;
import com.ciphermail.core.common.extractor.impl.HeuristicMimeTypeDetector;
import com.ciphermail.core.common.extractor.impl.TextExtractorFactoryRegistryImpl;
import com.ciphermail.core.common.extractor.impl.UnicodeTextExtractorFactory;
import com.ciphermail.core.common.extractor.impl.XHTMLTextExtractorFactory;
import com.ciphermail.core.common.hibernate.DatabaseMonitor;
import com.ciphermail.core.common.hibernate.DatabaseMonitorImpl;
import com.ciphermail.core.common.hibernate.HibernateInfo;
import com.ciphermail.core.common.hibernate.HibernateSessionFactoryBuilder;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SessionManagerImpl;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.hibernate.MailRepositoryImpl;
import com.ciphermail.core.common.mime.FileExtensionResolver;
import com.ciphermail.core.common.mime.FileExtensionResolverImpl;
import com.ciphermail.core.common.pdf.FileFontProvider;
import com.ciphermail.core.common.pdf.FontProvider;
import com.ciphermail.core.common.pdf.MessagePDFBuilder;
import com.ciphermail.core.common.pdf.MessagePDFBuilderImpl;
import com.ciphermail.core.common.properties.DefaultPropertyProviderRegistry;
import com.ciphermail.core.common.properties.DefaultPropertyProviderRegistryImpl;
import com.ciphermail.core.common.properties.FactoryPropertiesProviderClasspathLoader;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.properties.hibernate.NamedBlobManagerImpl;
import com.ciphermail.core.common.security.DefaultPKISecurityServicesFactory;
import com.ciphermail.core.common.security.HMACSecretKeyDeriverImpl;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertStoreImpl;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.PKISecurityServicesFactory;
import com.ciphermail.core.common.security.SecretKeyDeriver;
import com.ciphermail.core.common.security.ca.CA;
import com.ciphermail.core.common.security.ca.CAImpl;
import com.ciphermail.core.common.security.ca.CAPropertiesProvider;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistryImpl;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.ca.KeyAndCertificateIssuer;
import com.ciphermail.core.common.security.ca.SMIMEKeyAndCertificateIssuer;
import com.ciphermail.core.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import com.ciphermail.core.common.security.ca.handlers.pkcs10.PKCS10CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.hibernate.CertificateRequestStoreImpl;
import com.ciphermail.core.common.security.certificate.SerialNumberGenerator;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import com.ciphermail.core.common.security.certificate.validator.PKISMIMETrustCheckCertificateValidatorFactory;
import com.ciphermail.core.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import com.ciphermail.core.common.security.certpath.CertStoreTrustAnchorBuilder;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certpath.DefaultCertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreExtHibernate;
import com.ciphermail.core.common.security.crl.CRLPathBuilderFactory;
import com.ciphermail.core.common.security.crl.DefaultCRLPathBuilderFactory;
import com.ciphermail.core.common.security.crl.PKIXRevocationChecker;
import com.ciphermail.core.common.security.crl.RevocationChecker;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.crypto.impl.RandomGeneratorImpl;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.security.ctl.CTLManagerImpl;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.security.openpgp.*;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClient;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServerClientSettingsImpl;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServers;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListImpl;
import com.ciphermail.core.common.security.openpgp.validator.EncryptionKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.ExpirationValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorChain;
import com.ciphermail.core.common.security.openpgp.validator.PGPTrustListValidator;
import com.ciphermail.core.common.security.openpgp.validator.RevocationValidator;
import com.ciphermail.core.common.security.openpgp.validator.SigningKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.SubKeyBindingSignatureValidator;
import com.ciphermail.core.common.security.otp.ClientSecretIdGenerator;
import com.ciphermail.core.common.security.otp.ClientSecretIdGeneratorImpl;
import com.ciphermail.core.common.security.otp.HMACOTPGenerator;
import com.ciphermail.core.common.security.otp.OTPGenerator;
import com.ciphermail.core.common.security.otp.OTPQRCodeGenerator;
import com.ciphermail.core.common.security.otp.OTPQRCodeGeneratorImpl;
import com.ciphermail.core.common.security.password.PasswordGenerator;
import com.ciphermail.core.common.security.password.PasswordGeneratorImpl;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidator;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidatorImpl;
import com.ciphermail.core.common.security.password.validator.UsernameEmailRule;
import com.ciphermail.core.common.security.smime.selector.EncryptionCertificateSelector;
import com.ciphermail.core.common.sms.DummySMSTransport;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import com.ciphermail.core.common.sms.impl.SMSGatewayImpl;
import com.ciphermail.core.common.sms.impl.SMSTransportFactoryRegistryImpl;
import com.ciphermail.core.common.sms.impl.StaticSMSTransportFactory;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateBuilderImpl;
import com.ciphermail.core.common.template.TemplateHashModelBuilder;
import com.ciphermail.core.common.template.TemplateHashModelBuilderImpl;
import com.ciphermail.core.test.TestProperties;
import org.apache.hc.client5.http.auth.CredentialsStore;
import org.apache.hc.client5.http.impl.auth.SystemDefaultCredentialsProvider;
import org.apache.hc.core5.http2.HttpVersionPolicy;
import org.hibernate.SessionFactory;
import org.hibernate.hikaricp.internal.HikariCPConnectionProvider;
import org.passay.EnglishSequenceData;
import org.passay.IllegalSequenceRule;
import org.passay.PasswordValidator;
import org.passay.RepeatCharacterRegexRule;
import org.passay.UsernameRule;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * Spring context configuration class which constructs all the relevant CipherMail service beans for unit testing
 */
public class CipherMailSystemServices
{
    public interface CipherMailHomeDirProvider {
        File getDir();
    }

    public interface TestResourcesBaseDirProvider {
        File getDir();
    }

    /*
     * Hibernate connection settings.
     *
     * To use MariaDB set the following system properties
     *
     * -Dciphermail.hibernate.connection.driver_class=org.mariadb.jdbc.Driver
     * -Dciphermail.hibernate.connection.url=jdbc:mariadb://127.0.0.1:3306/ciphermail-gateway-test
     *
     */
    // Dialect is not required since it will be automatically selected based on the driver
    @Value("${ciphermail.hibernate.dialect:}")
    private String hibernateDialect;

    @Value("${ciphermail.hibernate.connection.driver_class:org.postgresql.Driver}")
    private String connectionDriverClass;

    @Value("${ciphermail.hibernate.connection.url:jdbc:postgresql://127.0.0.1/ciphermail-test}")
    private String connectionURL;

    @Value("${ciphermail.hibernate.connection.username:ciphermail}")
    private String connectionUsername;

    @Value("${ciphermail.hibernate.connection.password:ciphermail}")
    private String connectionPassword;

    @Value("${ciphermail.hibernate.physical_naming_strategy:com.ciphermail.core.common.hibernate.PrefixPhysicalNamingStrategy}")
    private String physicalNamingStrategyClassName;

    // Names of service beans to be used with @Qualifier to resolve bean ambiguity.
    public static final String CERT_STORE_SERVICE_NAME = "certStore";
    public static final String ROOT_STORE_SERVICE_NAME = "rootStore";
    public static final String KEY_STORE_PROVIDER_SERVICE_NAME = "keyStoreProvider";
    public static final String KEY_AND_CERT_STORE_SERVICE_NAME = "keyAndCertStore";
    public static final String KEY_AND_ROOT_CERT_STORE_SERVICE_NAME = "keyAndRootCertStore";
    public static final String KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME = "keyAndCertificateWorkflow";
    public static final String KEY_AND_ROOT_CERTIFICATE_WORKFLOW_SERVICE_NAME = "keyAndRootCertificateWorkflow";
    public static final String PGP_KEY_STORE_PROVIDER_SERVICE_NAME = "pgpKeyStoreProvider";
    public static final String PGP_SIGNING_KEY_VALIDATOR_SERVICE_NAME = "pgpSigningKeyValidator";
    public static final String PGP_ENCRYPTION_KEY_VALIDATOR_SERVICE_NAME = "pgpEncryptionKeyValidator";
    public static final String CA_SERVICE_NAME = "ca";
    public static final String DEFAULT_TRANSACTION_TEMPLATE = "defaultTransactionTemplate";
    public static final String DKIM_KEY_STORE_PROVIDER_SERVICE_NAME = "dkimKeyStoreProvider";

    @Bean
    public HibernateInfo hibernateInfoService()  {
        return new HibernateInfo();
    }

    @Bean
    public CipherMailHomeDirProvider cipherMailHomeDirProviderService()
    {
        // Return the current dir. For enterprise-core, this bean should be overridden
        return () -> new File(".");
    }

    @Bean
    public TestResourcesBaseDirProvider testResourcesBaseDirProviderService(
            CipherMailHomeDirProvider cipherMailHomeDirProvider)
    {
        // Return the current dir. For enterprise-core, this bean should be overridden
        return () -> new File(cipherMailHomeDirProvider.getDir(), "src/test/resources");
    }

    @Bean
    public SessionFactory sessionFactoryService(HibernateInfo hibernateInfo)
    {
        Properties props = new Properties();

        props.setProperty("hibernate.dialect", hibernateDialect);
        props.setProperty("hibernate.connection.driver_class", connectionDriverClass);
        props.setProperty("hibernate.connection.url", connectionURL);
        props.setProperty("hibernate.connection.username", connectionUsername);
        props.setProperty("hibernate.connection.password", connectionPassword);

        props.setProperty("hibernate.connection.provider_class", HikariCPConnectionProvider.class.getName());
        props.setProperty("hibernate.hikari.maximumPoolSize", "5");
        props.setProperty("hibernate.hikari.autoCommit", "false");
        props.setProperty("hibernate.hikari.leakDetectionThreshold", "30000");

        props.setProperty("hibernate.jdbc.fetch_size", "10");
        props.setProperty("hibernate.jdbc.batch_size", "10");

        props.setProperty("hibernate.hbm2ddl.auto", "create-drop");

        props.setProperty("hibernate.show_sql", "false");
        props.setProperty("hibernate.format_sql", "true");

        // Sorting order of null values depends on the database. MariaDB will have the null values at the start
        // whereas Postgres will have the null values at the end. By setting default_null_ordering, ordering will
        // be the same for all databases
        props.setProperty("hibernate.order_by.default_null_ordering", "first");

        return HibernateSessionFactoryBuilder.createInstance()
                .setHibernateInfo(hibernateInfo)
                .setProperties(props)
                .setPackagesToScan("com.ciphermail")
                .setPhysicalNamingStrategyClassName(physicalNamingStrategyClassName)
                .build();
    }

    @Bean
    public HibernateTransactionManager transactionManagerService(SessionFactory sessionFactory)  {
        return new HibernateTransactionManager(sessionFactory);
    }

    @Bean
    public CloseableHttpAsyncClientFactory closeableHttpAsyncClientFactoryService(HTTPClientProxyProvider proxyProvider)
    {
        CloseableHttpAsyncClientFactoryImpl factory = new CloseableHttpAsyncClientFactoryImpl(proxyProvider);

        factory.setValidateAfterInactivity(1000);
        factory.setEvictIdleConnectionsMaxIdleTime(5000);
        factory.setSoTimeout(30000);
        factory.setVersionPolicy(HttpVersionPolicy.NEGOTIATE);

        return factory;
    }

    @Bean("httpClientSystemDefaultCredentialsProvider")
    public CredentialsStore httpClientSystemDefaultCredentialsProviderService()
    {
        return new SystemDefaultCredentialsProvider();
    }

    @Bean("httpClientProxyProvider")
    public HTTPClientProxyProvider httpClientProxyProviderService() {
        return new HTTPClientStaticProxyProvider();
    }

    @Bean("httpClientContextFactory")
    public HttpClientContextFactory httpClientContextFactoryService(
            CredentialsStore credentialsStore)
    {
        return new HttpClientContextFactoryImpl(credentialsStore);
    }

    @Bean("sessionManager")
    public SessionManager sessionManagerService(SessionFactory sessionFactory)  {
        return new SessionManagerImpl(sessionFactory);
    }

    @Primary
    @Bean(DEFAULT_TRANSACTION_TEMPLATE)
    public TransactionOperations defaultTransactionTemplateService(HibernateTransactionManager transactionManager)  {
        return new TransactionTemplate(transactionManager);
    }

    @Bean("databaseMonitor")
    public DatabaseMonitor databaseMonitorService(SessionManager sessionManager,
            TransactionOperations transactionOperations)
    {
        return new DatabaseMonitorImpl(sessionManager, transactionOperations);
    }

    @Bean
    public PasswordProvider passwordProviderService() {
        return new StaticPasswordProvider("test");
    }

    @Bean(CERT_STORE_SERVICE_NAME)
    public X509CertStoreExt certStoreService(SessionManager sessionManager)  {
        return new X509CertStoreExtHibernate("certificates", sessionManager);
    }

    @Bean(ROOT_STORE_SERVICE_NAME)
    public X509CertStoreExt rootStoreService(SessionManager sessionManager)  {
        return new X509CertStoreExtHibernate("roots", sessionManager);
    }

    @Bean(KEY_STORE_PROVIDER_SERVICE_NAME)
    public KeyStoreProvider keyStoreProviderService(SessionManager sessionManager)
    throws Exception
    {
        return CipherMailServices.buildKeyStoreProvider("keys", sessionManager);
    }

    @Bean(PGP_KEY_STORE_PROVIDER_SERVICE_NAME)
    public KeyStoreProvider pgpKeyStoreProviderService(SessionManager sessionManager)
    throws Exception
    {
        return CipherMailServices.buildPGPKeyStoreProvider("pgp", sessionManager);
    }

    @Bean(KEY_AND_CERT_STORE_SERVICE_NAME)
    public KeyAndCertStore keyAndCertStoreService(
            @Qualifier(CERT_STORE_SERVICE_NAME) X509CertStoreExt certStore,
            @Qualifier(KEY_STORE_PROVIDER_SERVICE_NAME) KeyStoreProvider keyStoreProvider,
            PasswordProvider passwordProvider)
    {
        return new KeyAndCertStoreImpl(certStore, keyStoreProvider, passwordProvider);
    }

    @Bean(KEY_AND_ROOT_CERT_STORE_SERVICE_NAME)
    public KeyAndCertStore keyAndRootCertStoreService(
            @Qualifier(ROOT_STORE_SERVICE_NAME) X509CertStoreExt certStore,
            @Qualifier(KEY_STORE_PROVIDER_SERVICE_NAME) KeyStoreProvider keyStoreProvider,
            PasswordProvider passwordProvider)
    {
        return new KeyAndCertStoreImpl(certStore, keyStoreProvider, passwordProvider);
    }

    @Bean
    public UserPropertiesFactoryRegistry createUserPropertiesFactoryRegistry(ApplicationContext applicationContext) {
        return new UserPropertiesFactoryRegistryImpl(applicationContext);
    }

    @Bean
    public UserPreferencesWorkflow userPreferencesWorkflowService(
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
            SessionManager sessionManager,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new UserPreferencesWorkflowImpl(keyAndCertStore, sessionManager, userPropertiesFactoryRegistry);
    }

    @Bean
    public UserPreferencesCategoryManager userPreferencesCategoryManagerService(
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME)KeyAndCertStore keyAndCertStore,
            SessionManager sessionManager,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new UserPreferencesCategoryManagerHibernate(keyAndCertStore, sessionManager,
                userPropertiesFactoryRegistry);
    }

    @Bean
    public UserPropertyRegistry createUserPropertyRegistryService(Environment environment) {
        return new UserPropertyRegistryImpl(environment, "com.ciphermail");
    }

    @Bean
    public DefaultPropertyProviderRegistry defaultPropertyProviderRegistryService(
            ApplicationContext applicationContext,
            Environment environment,
            UserPropertyRegistry userPropertyRegistry)
    {
        return new DefaultPropertyProviderRegistryImpl(
                applicationContext,
                environment,
                List.of("com.ciphermail"),
                userPropertyRegistry);
    }

    @Bean
    public FactoryPropertiesProviderClasspathLoader factoryPropertiesProviderService() {
        return new FactoryPropertiesProviderClasspathLoader();
    }

    @Bean
    public CipherMailFactoryProperties factoryPropertiesService(
            FactoryPropertiesProviderClasspathLoader propertiesProvider,
            TestResourcesBaseDirProvider testResourcesBaseDirProvider)
    throws Exception
    {
        return CipherMailServices.buildCipherMailFactoryProperties(
                new File(testResourcesBaseDirProvider.getDir(), "conf/factory.properties.d"),
                propertiesProvider,
                testResourcesBaseDirProvider.getDir());
    }

    @Bean("globalPreferencesManager")
    public GlobalPreferencesManager globalPreferencesManagerService(
            UserPreferencesCategoryManager userPreferencesCategoryManager,
            CipherMailFactoryProperties factoryProperties)
    {
        return new DefaultGlobalPreferencesManager(userPreferencesCategoryManager, factoryProperties);
    }

    @Bean("domainManager")
    public DomainManager domainManagerService(UserPreferencesCategoryManager userPreferencesCategoryManager,
            GlobalPreferencesManager globalPreferencesManager)
    {
        return new DefaultDomainManager(userPreferencesCategoryManager, globalPreferencesManager);
    }

    @Bean
    public X509CRLStoreExt x509CRLStoreExtService(SessionManager sessionManager) {
        return new X509CRLStoreExtHibernate("crls", sessionManager);
    }

    @Bean
    public TrustAnchorBuilder trustAnchorBuilderService(@Qualifier(ROOT_STORE_SERVICE_NAME) X509CertStoreExt rootStore)
    {
        // To make unit tests consistent, we set the TrustAnchorBuilder update interval to 0 otherwise we can have
        // some race condition where a root is added to the store but the trust anchors are not rebuilt
        return new CertStoreTrustAnchorBuilder(rootStore, 0);
    }

    @Bean("certificatePathBuilderFactory")
    public CertificatePathBuilderFactory certificatePathBuilderFactoryService(TrustAnchorBuilder trustAnchorBuilder,
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore)
    {
        return new DefaultCertificatePathBuilderFactory(trustAnchorBuilder, keyAndCertStore);
    }

    @Bean
    public RevocationChecker revocationCheckerService(X509CRLStoreExt crlStore) {
        return new PKIXRevocationChecker(crlStore);
    }

    @Bean
    public CRLPathBuilderFactory cRLPathBuilderFactoryService(
            CertificatePathBuilderFactory certificatePathBuilderFactory)
    {
        return new DefaultCRLPathBuilderFactory(certificatePathBuilderFactory);
    }

    @Bean
    public CTLManager cTLManagerService(SessionManager sessionManager) {
        return new CTLManagerImpl(sessionManager);
    }

    @Bean
    public PKITrustCheckCertificateValidatorFactory pKITrustCheckCertificateValidatorFactoryService(
            CertificatePathBuilderFactory certificatePathBuilderFactory, RevocationChecker revocationChecker,
            CTLManager ctlManager)
    {
        return new PKISMIMETrustCheckCertificateValidatorFactory(certificatePathBuilderFactory, revocationChecker,
                ctlManager);
    }

    @Bean
    public PKISecurityServicesFactory pKISecurityServicesFactoryService(
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
            @Qualifier(ROOT_STORE_SERVICE_NAME) X509CertStoreExt rootStore,
            X509CRLStoreExt crlStore,
            TrustAnchorBuilder trustAnchorBuilder,
            RevocationChecker revocationChecker,
            CertificatePathBuilderFactory certificatePathBuilderFactory,
            CRLPathBuilderFactory crlPathBuilderFactory,
            PKITrustCheckCertificateValidatorFactory validatorFactory)
    {
        return new DefaultPKISecurityServicesFactory(
                keyAndCertStore,
                rootStore,
                crlStore,
                trustAnchorBuilder,
                revocationChecker,
                certificatePathBuilderFactory,
                crlPathBuilderFactory,
                validatorFactory);
    }

    @Bean("pkiSecurityServices")
    public PKISecurityServices pKISecurityServicesService(
            PKISecurityServicesFactory pKISecurityServicesFactory)
    {
        return pKISecurityServicesFactory.createPKISecurityServices();
    }

    @Bean
    public EncryptionCertificateSelector encryptionCertificateSelectorService(
            PKISecurityServices pKISecurityServices)
    {
        return new EncryptionCertificateSelector(pKISecurityServices);
    }

    @Bean
    public UserManager userManagerService(
            PKISecurityServices pKISecurityServices,
            EncryptionCertificateSelector encryptionCertificateSelector,
            SessionManager sessionManager,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new UserManagerHibernate(pKISecurityServices, encryptionCertificateSelector, sessionManager,
                userPropertiesFactoryRegistry);
    }

    @Bean
    public UserPreferencesSelector userPreferencesSelectorService(DomainManager domainManager) {
        return new DefaultUserPreferencesSelector(domainManager);
    }

    @Bean
    public UserPreferenceMerger userPreferenceMergerService() {
        return new DefaultUserPreferenceMerger();
    }

    @Bean
    public ClientSecretGenerator clientSecretGeneratorService(RandomGenerator randomGenerator) {
        return new ClientSecretGeneratorImpl(randomGenerator, 32);
    }

    @Bean("userWorkflow")
    public UserWorkflow userWorkflowService(
            UserManager userManager,
            GlobalPreferencesManager globalPreferencesManager,
            UserPreferencesSelector userPreferencesSelector,
            UserPreferenceMerger userPreferenceMerger,
            UserPreferencesCategoryManager userPreferencesCategoryManager,
            ClientSecretGenerator clientSecretGenerator)
    {
        return new UserWorkflowImpl(userManager, globalPreferencesManager, userPreferencesSelector,
                userPreferenceMerger, userPreferencesCategoryManager, clientSecretGenerator);
    }

    @Bean(KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    public KeyAndCertificateWorkflow keyAndCertificateWorkflowService(
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
            CertificatePathBuilderFactory certificatePathBuilderFactory,
            SessionManager sessionManager,
            TransactionOperations transactionOperations)
    {
        return new KeyAndCertificateWorkflowImpl(keyAndCertStore, certificatePathBuilderFactory, sessionManager,
                transactionOperations);
    }

    @Bean(KEY_AND_ROOT_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    public KeyAndCertificateWorkflow keyAndRootCertificateWorkflowService(
            @Qualifier(KEY_AND_ROOT_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
            CertificatePathBuilderFactory certificatePathBuilderFactory,
            SessionManager sessionManager,
            TransactionOperations transactionOperations)
    {
        return new KeyAndCertificateWorkflowImpl(keyAndCertStore, certificatePathBuilderFactory, sessionManager,
                transactionOperations);
    }

    @Bean
    public NamedBlobManager namedBlobManagerService(SessionManager sessionManager) {
        return new NamedBlobManagerImpl(sessionManager);
    }

    @Bean
    public AdminManager adminManagerService(SessionManager sessionManager) {
        return new AdminManagerImpl(sessionManager);
    }

    @Bean
    public RoleManager roleManagerService(SessionManager sessionManager) {
        return new RoleManagerImpl(sessionManager);
    }

    @Bean
    public MatchFilterRegistry matchFilterRegistryService() {
        return new MatchFilterRegistryImpl(Collections.singletonList(new MaskingFilter()));
    }

    @Bean
    public ValidatorRegistry validatorRegistryService() {
        return new ValidatorRegistryImpl();
    }

    @Bean("patternCache")
    public PatternCache patternCacheService() {
        return new SimpleMemoryPatternCache();
    }

    @Bean
    public PolicyPatternMarshaller policyPatternMarshallerService(PatternCache patternCache,
            MatchFilterRegistry matchFilterRegistry, ValidatorRegistry validatorRegistry)
    {
        return new PolicyPatternMarshallerImpl(patternCache, matchFilterRegistry, validatorRegistry);
    }

    @Bean
    public PolicyPatternManager policyPatternManagerService(NamedBlobManager namedBlobManager,
            PolicyPatternMarshaller policyPatternMarshaller, UserPreferencesWorkflow userPreferencesWorkflow)
    {
        return new PolicyPatternManagerImpl(namedBlobManager, policyPatternMarshaller, userPreferencesWorkflow);
    }

    @Bean("userPreferencesPolicyPatternManager")
    public UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManagerService(
            PolicyPatternMarshaller policyPatternMarshaller)
    {
        return new UserPreferencesPolicyPatternManagerImpl(policyPatternMarshaller);
    }

    @Bean
    public UpdateableWordSkipper updateableWordSkipperService(NamedBlobManager namedBlobManager,
            TransactionOperations transactionOperations, TestResourcesBaseDirProvider testResourcesBaseDirProvider)
    {
        return new UpdateableWordSkipperImpl(
                namedBlobManager,
                new ClassPathResource("conf/dlp-skip-words.txt"),
                transactionOperations);
    }

    @Bean("textNormalizer")
    public TextNormalizer textNormalizerService(UpdateableWordSkipper updateableWordSkipper) {
        return new TextNormalizerImpl(updateableWordSkipper);
    }

    @Bean("policyCheckerPipeline")
    public PolicyCheckerPipeline policyCheckerPipelineService()
    {
        PolicyCheckerPipelineImpl policyCheckerPipeline = new PolicyCheckerPipelineImpl();

        RegExpPolicyChecker regExpPolicyChecker = new RegExpPolicyChecker();

        regExpPolicyChecker.setOverlapLength(64);
        regExpPolicyChecker.setMaxMatchWidth(64);
        regExpPolicyChecker.setTotalMaxMatchWidth(1024);

        policyCheckerPipeline.addPolicyChecker(regExpPolicyChecker);

        policyCheckerPipeline.setFastFail(false);

        return policyCheckerPipeline;
    }

    @Bean("mimeTypeDetector")
    public MimeTypeDetector mimeTypeDetectorService() {
        return new HeuristicMimeTypeDetector();
    }

    @Bean("textExtractorFactoryRegistry")
    public TextExtractorFactoryRegistry textExtractorFactoryRegistryService()
    {
        UnicodeTextExtractorFactory unicodeTextExtractorFactory = new UnicodeTextExtractorFactory(
                new String[] {"text/plain"});

        unicodeTextExtractorFactory.setThreshold(1048576);
        unicodeTextExtractorFactory.setMaxPartSize(20971520);

        XHTMLTextExtractorFactory xhtmlTextExtractorFactory = new XHTMLTextExtractorFactory(
                new String[] {"text/html", "application/xhtml+xml"});

        xhtmlTextExtractorFactory.setThreshold(1048576);
        xhtmlTextExtractorFactory.setMaxPartSize(20971520);
        xhtmlTextExtractorFactory.setSkipComments(true);
        xhtmlTextExtractorFactory.setOnlyScanBody(true);

        List<TextExtractorFactory> factories = new LinkedList<>();

        factories.add(unicodeTextExtractorFactory);
        factories.add(xhtmlTextExtractorFactory);

        return new TextExtractorFactoryRegistryImpl(factories);
    }

    @Bean("mimeMessageTextExtractor")
    public MimeMessageTextExtractor mimeMessageTextExtractorService(MimeTypeDetector mimeTypeDetector,
            TextExtractorFactoryRegistry textExtractorFactoryRegistry)
    {
        MimeMessageTextExtractorImpl extractor = new MimeMessageTextExtractorImpl(mimeTypeDetector,
                textExtractorFactoryRegistry);

        extractor.setFallbackTextExtractorMimeType("text/plain");
        extractor.setExtractMetaInfo(true);

        Set<String> skipHeaders = new HashSet<>();

        skipHeaders.add("received");
        skipHeaders.add("from");
        skipHeaders.add("reply-to");
        skipHeaders.add("references");
        skipHeaders.add("message-id");

        extractor.setSkipHeaders(skipHeaders);

        return extractor;
    }

    @Bean
    public PGPSignatureValidator pGPSignatureValidatorService() {
        return new PGPSignatureValidatorImpl();
    }

    @Bean
    public PGPUserIDValidator pGPUserIDValidatorService(PGPSignatureValidator pGPSignatureValidator) {
        return new PGPUserIDValidatorImpl(pGPSignatureValidator);
    }

    @Bean
    public PGPPublicKeyMerger pGPPublicKeyMergerService() {
        return new PGPPublicKeyMergerImpl();
    }

    @Bean
    public PGPUserIDRevocationChecker pGPUserIDRevocationCheckerService(PGPSignatureValidator pGPSignatureValidator) {
        return new PGPUserIDRevocationCheckerImpl(pGPSignatureValidator);
    }

    @Bean
    public PGPKeyRingEntryEmailUpdater pGPKeyRingEntryEmailUpdaterService(PGPUserIDValidator userIDValidator,
            PGPUserIDRevocationChecker userIDRevocationChecker)
    {
        return new PGPKeyRingEntryEmailUpdaterImpl(userIDValidator, userIDRevocationChecker);
    }

    @Bean
    public PGPKeyRing pGPKeyRingService(
            @Qualifier(PGP_KEY_STORE_PROVIDER_SERVICE_NAME) KeyStoreProvider keyStoreProvider,
            PasswordProvider passwordProvider,
            SessionManager sessionManager,
            PGPPublicKeyMerger publicKeyMerger,
            PGPKeyRingEntryEmailUpdater keyRingEntryEmailUpdater)
    {
        return new PGPKeyRingImpl("pgp", keyStoreProvider,  passwordProvider, sessionManager, publicKeyMerger,
                keyRingEntryEmailUpdater);
    }

    @Bean("pgpKeyRingEntryRevoker")
    public PGPKeyRingEntryRevoker pgpKeyRingEntryRevokerService(PGPKeyRing pGPkeyRing)
    {
        return new PGPKeyRingEntryRevokerImpl(pGPkeyRing);
    }

    @Bean
    public PGPKeyFlagChecker pGPKeyFlagCheckerService(PGPSignatureValidator pGPSignatureValidator,
            PGPUserIDValidator pGPUserIDValidator)
    {
        return new PGPKeyFlagCheckerImpl(pGPSignatureValidator, pGPUserIDValidator);
    }

    @Bean("pgpKeyRingImporter")
    public PGPKeyRingImporter pGPKeyRingImporterService(PGPKeyRing keyRing) {
        return new PGPKeyRingImporterImpl(keyRing);
    }

    @Bean("pgpExpirationChecker")
    public PGPExpirationChecker pGPExpirationCheckerService(PGPSignatureValidator signatureValidator)
    {
        return new PGPExpirationCheckerImpl(signatureValidator);
    }

    @Bean
    public ExpirationValidator expirationValidatorService(PGPExpirationChecker expirationChecker) {
        return new ExpirationValidator(expirationChecker);
    }

    @Bean(PGP_ENCRYPTION_KEY_VALIDATOR_SERVICE_NAME)
    public PGPPublicKeyValidator pGPEncryptionKeyValidatorService(
            ExpirationValidator expirationValidator,
            PGPSignatureValidator signatureValidator,
            PGPKeyFlagChecker keyFlagChecker,
            PGPTrustList trustList)
    {
        PGPPublicKeyValidatorChain validatorChain = new PGPPublicKeyValidatorChain();

        validatorChain.addValidators(
                expirationValidator,
                new RevocationValidator(signatureValidator),
                new SubKeyBindingSignatureValidator(signatureValidator, keyFlagChecker),
                new PGPTrustListValidator(trustList),
                new EncryptionKeyValidator(keyFlagChecker));

        return validatorChain;
    }

    @Bean(PGP_SIGNING_KEY_VALIDATOR_SERVICE_NAME)
    public PGPPublicKeyValidator pGPSigningKeyValidatorService(
            ExpirationValidator expirationValidator,
            PGPSignatureValidator signatureValidator,
            PGPKeyFlagChecker keyFlagChecker,
            PGPTrustList trustList)
    {
        PGPPublicKeyValidatorChain validatorChain = new PGPPublicKeyValidatorChain();

        validatorChain.addValidators(
                expirationValidator,
                new RevocationValidator(signatureValidator),
                new SubKeyBindingSignatureValidator(signatureValidator, keyFlagChecker),
                new PGPTrustListValidator(trustList),
                new SigningKeyValidator(keyFlagChecker));

        return validatorChain;
    }

    @Bean("pgpSigningKeySelector")
    public PGPPrivateKeySelector pGPPrivateKeySelectorService(PGPKeyRing keyRing,
            @Qualifier(PGP_SIGNING_KEY_VALIDATOR_SERVICE_NAME) PGPPublicKeyValidator signingKeyValidator)
    {
        return new PGPPrivateKeySelectorImpl(keyRing, signingKeyValidator);
    }

    @Bean("pgpEncryptionKeySelector")
    @Primary
    public PGPPublicKeySelector pGPEncryptionKeySelectorService(PGPKeyRing keyRing,
            @Qualifier(PGP_ENCRYPTION_KEY_VALIDATOR_SERVICE_NAME) PGPPublicKeyValidator encryptionKeyValidator)
    {
        return new PGPPublicKeySelectorImpl(keyRing, encryptionKeyValidator);
    }

    @Bean("userPreferencesPGPSigningKeySelector")
    public UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelectorService(
            PGPPrivateKeySelector privateKeySelector,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new UserPreferencesPGPSigningKeySelectorImpl(privateKeySelector, userPropertiesFactoryRegistry);
    }

    @Bean("pgpTrustList")
    public PGPTrustList pGPTrustListService(SessionManager sessionManager, PGPKeyRing keyRing)
    {
        return new PGPTrustListImpl("pgp", sessionManager, keyRing);
    }

    @Bean("pgpKeyPairProvider")
    public PGPKeyPairProvider pGPKeyPairProviderService(PGPKeyRing keyRing)
    {
        return new PGPKeyRingKeyPairProvider(keyRing);
    }

    @Bean("pgpKeyRingEntryProvider")
    public PGPKeyRingEntryProvider pGPKeyRingEntryProviderService(PGPKeyRing keyRing)
    {
        return new PGPKeyRingEntryProviderImpl(keyRing);
    }

    @Bean("pgpContentSignatureVerifier")
    public PGPContentSignatureVerifier pGPContentSignatureVerifierService(PGPKeyRingEntryProvider keyRingEntryProvider)
    {
        return new PGPContentSignatureVerifierImpl(keyRingEntryProvider);
    }

    @Bean("pgpContentSignatureValidator")
    public PGPContentSignatureValidator pGPContentSignatureValidatorService(
            PGPContentSignatureVerifier contentSignatureVerifier,
            @Qualifier(PGP_SIGNING_KEY_VALIDATOR_SERVICE_NAME) PGPPublicKeyValidator signingKeyValidator)
    {
        return new PGPContentSignatureValidatorImpl(contentSignatureVerifier, signingKeyValidator);
    }

    @Bean("hkpKeyServerClient")
    public HKPKeyServerClient hkpKeyServerClientService(
            CloseableHttpAsyncClientFactory closeableHttpAsyncClientFactory,
            HttpClientContextFactory httpClientContextFactory)
    {
        return new HKPKeyServerClientImpl(closeableHttpAsyncClientFactory, httpClientContextFactory);
    }

    @Bean("pgpKeyServerClient")
    public KeyServerClient pgpKeyServerClientService(
            GlobalPreferencesManager globalPreferencesManager,
            HKPKeyServerClient hkpKeyServerClient,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new KeyServerClientImpl(
                globalPreferencesManager,
                hkpKeyServerClient,
                userPropertiesFactoryRegistry)
        {
            @Override
            public @Nonnull HKPKeyServers getServers()
            throws IOException
            {
                HKPKeyServers servers = super.getServers();

                // for unit tests, we need a default server
                if (servers.getClientSettings().isEmpty())
                {
                    servers.getClientSettings().add(new HKPKeyServerClientSettingsImpl(
                            TestProperties.getPGPKeyserverURL1()));
                }

                return servers;
            }
        };
    }

    @Bean("pgpSecretKeyRingGenerator")
    public PGPSecretKeyRingGenerator pgpSecretKeyRingGeneratorService() {
        return new PGPSecretKeyRingGeneratorImpl();
    }

    @Bean("pgpKeyRingExporter")
    public PGPKeyRingExporter pgpKeyRingExporterService()
    throws Exception
    {
        return new PGPKeyRingExporterImpl();
    }

    @Bean("pgpUserIDResolver")
    public UserIDResolver pgpUserIDResolverService() {
        return new DefaultUserIDResolver();
    }

    @Bean("pgpSecretKeyRequestor")
    public PGPSecretKeyRequestor pgpSecretKeyRequestorService(
            UserWorkflow userWorkflow,
            PGPSecretKeyRingGenerator pgpSecretKeyRingGenerator,
            PGPKeyRingImporter pgpKeyRingImporter,
            PGPKeyRingExporter pgpKeyRingExporter,
            PGPTrustList pgpTrustList,
            KeyServerClient pgpKeyServerClient,
            UserIDResolver pgpUserIDResolver,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        return new PGPSecretKeyRequestorImpl(
                        userWorkflow,
                        pgpSecretKeyRingGenerator,
                        pgpKeyRingImporter,
                        pgpKeyRingExporter,
                        pgpTrustList,
                        pgpKeyServerClient,
                        pgpUserIDResolver,
                        userPropertiesFactoryRegistry);
    }

    @Bean("messageOriginatorIdentifier")
    public MessageOriginatorIdentifier messageOriginatorIdentifierService() {
        return new DefaultMessageOriginatorIdentifier();
    }

    @Bean("templateBuilder")
    public TemplateBuilder templateBuilderService(TestResourcesBaseDirProvider testResourcesBaseDirProvider)
    throws Exception
    {
        return new TemplateBuilderImpl(new File(testResourcesBaseDirProvider.getDir(), "templates"), "/templates/");
    }

    @Bean("notificationService")
    public MockNotificationService notificationServiceService() {
        return new MockNotificationService();
    }

    @Bean(DKIM_KEY_STORE_PROVIDER_SERVICE_NAME)
    public KeyStoreProvider dkimKeyStoreProviderService(SessionManager sessionManager)
    throws Exception
    {
        return CipherMailServices.buildKeyStoreProvider("dkim", sessionManager);
    }

    @Bean("dKIMPrivateKeyManager")
    public DKIMPrivateKeyManager dKIMPrivateKeyManagerService(@Qualifier(
            DKIM_KEY_STORE_PROVIDER_SERVICE_NAME) KeyStoreProvider keyStoreProvider,
            PasswordProvider passwordProvider)
    {
        return new DKIMPrivateKeyManagerImpl(keyStoreProvider, passwordProvider);
    }

    @Bean("randomGenerator")
    public RandomGenerator randomGeneratorService()
    throws Exception
    {
        return new RandomGeneratorImpl();
    }

    @Bean("passwordGenerator")
    public PasswordGenerator passwordGeneratorService(RandomGenerator randomGenerator) {
        return new PasswordGeneratorImpl(randomGenerator);
    }

    @Bean("fontProvider")
    public FontProvider fontProviderService(TestResourcesBaseDirProvider testResourcesBaseDirProvider) {
        return new FileFontProvider(new File(testResourcesBaseDirProvider.getDir(), "fonts"));
    }

    @Bean("messagePDFBuilder")
    public MessagePDFBuilder messagePDFBuilderService(FontProvider fontProvider,
            TestResourcesBaseDirProvider testResourcesBaseDirProvider)
    throws IOException
    {
        MessagePDFBuilderImpl messagePDFBuilder =  new MessagePDFBuilderImpl();

        messagePDFBuilder.setFontProvider(fontProvider);
        messagePDFBuilder.setReplyButtonResource(new ClassPathResource("images/reply-button.png"));
        messagePDFBuilder.setMaxInlineSize(262144);

        return messagePDFBuilder;
    }

    @Bean("otpGenerator")
    public OTPGenerator otpGeneratorService() {
        return new HMACOTPGenerator("HmacSHA256");
    }

    @Bean
    public SecretKeyDeriver secretKeyDeriverService() {
        return new HMACSecretKeyDeriverImpl();
    }

    @Bean("clientSecretIdGenerator")
    public ClientSecretIdGenerator clientSecretIdGeneratorService(OTPGenerator otpGenerator) {
        return new ClientSecretIdGeneratorImpl(otpGenerator);
    }

    @Bean
    public OTPQRCodeGenerator oTPQRCodeGeneratorService(ClientSecretIdGenerator clientSecretIdGenerator) {
        return new OTPQRCodeGeneratorImpl(clientSecretIdGenerator);
    }

    @Bean("fileExtensionResolver")
    public FileExtensionResolver fileExtensionResolverService()
    {
        return new FileExtensionResolverImpl();
    }

    @Bean("quarantineMailRepository")
    public MailRepository quarantineMailRepositoryService(SessionManager sessionManager)
    {
        return new MailRepositoryImpl(sessionManager, "quarantine");
    }

    @Bean("quarantineMailStorer")
    public MailRepositoryMailStorer quarantineMailStorerService(MailRepository quarantineMailRepository,
            MessageOriginatorIdentifier messageOriginatorIdentifier)
    {
        return new MailRepositoryMailStorer(quarantineMailRepository, messageOriginatorIdentifier);
    }

    @Bean("certificateRequestHandlerRegistry")
    public CertificateRequestHandlerRegistry certificateRequestHandlerRegistryService(
            KeyAndCertStore keyAndCertStore,
            CAPropertiesProvider caPropertiesProvider,
            KeyAndCertificateIssuer keyAndCertificateIssuer,
            KeyStoreProvider keyStoreProvider,
            PasswordProvider passwordProvider,
            SerialNumberGenerator serialNumberGenerator)
    {
        CertificateRequestHandlerRegistry registry = new CertificateRequestHandlerRegistryImpl();

        registry.registerHandler(new BuiltInCertificateRequestHandler(keyAndCertStore, caPropertiesProvider,
                keyAndCertificateIssuer));

        registry.registerHandler(new PKCS10CertificateRequestHandler(keyStoreProvider, passwordProvider,
                serialNumberGenerator));

        return registry;
    }

    @Bean("certificateRequestStore")
    public CertificateRequestStore certificateRequestStoreService(SessionManager sessionManager,
            CertificateRequestHandlerRegistry certificateRequestHandlerRegistry)
    {
        return new CertificateRequestStoreImpl(sessionManager, certificateRequestHandlerRegistry);
    }

    @Bean(CA_SERVICE_NAME)
    public CA caService(
            CertificateRequestStore certificateRequestStore,
            CertificateRequestHandlerRegistry certificateRequestHandlerRegistry,
            @Qualifier(KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
            TransactionOperations transactionOperations,
            SessionManager sessionManager)
    {
        CAImpl ca = new CAImpl(certificateRequestStore, certificateRequestHandlerRegistry, keyAndCertStore,
                transactionOperations, sessionManager, 5 /* in seconds */, 2592000, new int[] {5});

        // For the tests, the background should run immediately when started
        ca.setThreadStartupDelay(0);

        return ca;
    }

    @Bean("serialNumberGenerator")
    public SerialNumberGenerator serialNumberGeneratorService()
    throws Exception
    {
        return new StandardSerialNumberGenerator();
    }

    @Bean("keyAndCertificateIssuer")
    public SMIMEKeyAndCertificateIssuer keyAndCertificateIssuerService(SerialNumberGenerator serialNumberGenerator)
    throws Exception
    {
        return new SMIMEKeyAndCertificateIssuer(serialNumberGenerator);
    }

    @Bean("caPropertiesProvider")
    public CAPropertiesProvider caPropertiesProviderService(GlobalPreferencesManager globalPreferencesManager) {
        return new CAPropertiesProviderImpl(globalPreferencesManager);
    }

    @Bean("dummySMSTransport")
    public DummySMSTransport dummySMSTransportService() {
        return new DummySMSTransport();
    }

    @Bean("smsTransportFactoryRegistry")
    public SMSTransportFactoryRegistry smsTransportFactoryRegistryService(DummySMSTransport dummySMSTransport,
            CipherMailFactoryProperties factoryProperties)
    throws HierarchicalPropertiesException
    {
        SMSTransportFactoryRegistry registry = new SMSTransportFactoryRegistryImpl();

        registry.registerFactory(new SMSTransportFactoryRegistry.Registration("dummy",
                new StaticSMSTransportFactory(dummySMSTransport)));

        // Register the dummy transport as the default
        factoryProperties.setProperty("sms-active-transport", "dummy");

        return registry;
    }

    @Bean("smsGateway")
    public SMSGateway smsGatewayService(
            SMSTransportFactoryRegistry smsTransportFactoryRegistry,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            GlobalPreferencesManager globalPreferencesManager,
            SessionManager sessionManager,
            TransactionOperations transactionOperations)
    {
        SMSGatewayImpl smsGateway = new SMSGatewayImpl(
                smsTransportFactoryRegistry,
                userPropertiesFactoryRegistry,
                globalPreferencesManager,
                sessionManager,
                transactionOperations);

        smsGateway.setStartupDelayTime(0);
        smsGateway.setSleepTime(100);
        smsGateway.setSleepTimeOnError(100);
        smsGateway.setExpirationTime(86400000);

        return smsGateway;
    }

    @Bean("encryptionRecipientSelector")
    public EncryptionRecipientSelector encryptionRecipientSelectorService(
            PKISecurityServices pkiSecurityServices)
    {
        return new DefaultEncryptionRecipientSelector(pkiSecurityServices, true);
    }

    @Bean("statsPublisher")
    public StatsPublisher statsPublisherService()
    {
        return new StatsPublisherImpl();
    }

    @Bean("passwordStrengthValidator")
    public PasswordStrengthValidator passwordStrengthValidatorService(UserWorkflow userWorkflow)
    {
        return new PasswordStrengthValidatorImpl(new PasswordValidator(
                new UserPropertyNOutOfMRule(userWorkflow, "password-policy"),
                new RepeatCharacterRegexRule(5),
                new IllegalSequenceRule(EnglishSequenceData.USQwerty, 5, true)));
    }

   @Bean("portalPasswordStrengthValidator")
    public PasswordStrengthValidator portalPasswordStrengthValidator(UserWorkflow userWorkflow)
    {
        return new PasswordStrengthValidatorImpl(new PasswordValidator(
                new UserPropertyNOutOfMRule(userWorkflow, "portal-password-policy"),
                new RepeatCharacterRegexRule(5),
                new IllegalSequenceRule(EnglishSequenceData.USQwerty, 5, true),
                new UsernameRule(),
                new UsernameEmailRule()));
    }

    @Bean("templateHashModelBuilder")
    public TemplateHashModelBuilder templateHashModelBuilder(UserWorkflow userWorkflow,
            UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        TemplateHashModelBuilderImpl templateHashModelBuilder = new TemplateHashModelBuilderImpl();

        templateHashModelBuilder.addContributor(new TemplateServicesContributor(
                new TemplateServicesImpl(
                        userWorkflow,
                        userPropertiesFactoryRegistry,
                        List.of(
                            "email-footer",
                            "email-footer-link",
                            "email-footer-link-title",
                            "organization-id",
                            "pdf-qrcode-enabled",
                            "pdf-otp-url",
                            "portal-base-url",
                            "portal-password-reset-url",
                            "portal-signup-url",
                            "comment"
                        ))));

        templateHashModelBuilder.addContributor(new CoreApplicationMailAttributesContributor());

        return templateHashModelBuilder;
    }

    @Bean
    public CSRStore csrStoreService(NamedBlobManager namedBlobManager) {
        return new CSRStoreImpl(namedBlobManager);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8();
    }
}
