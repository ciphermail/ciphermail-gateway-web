/*
 * Copyright (c) 2008-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import com.ciphermail.core.common.properties.PropertiesResolver.PropertiesResolverException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class PropertiesResolverTest
{
    @Test
    void testResolveFiles()
    throws PropertiesResolverException
    {
        Properties props = new Properties();

        props.setProperty("prop1", "value1");
        props.setProperty("prop2", "file://src/test/resources/testdata/other/test.properties");

        new PropertiesResolver().resolve(props);

        Assertions.assertEquals("value1", props.getProperty("prop1"));
        Assertions.assertEquals("test=123", props.getProperty("prop2").trim());
    }

    @Test
    void testUnknownBinding()
    throws PropertiesResolverException
    {
        Properties props = new Properties();

        props.setProperty("prop1", "value1");
        props.setProperty("prop2", "unknown://src/test/resources/testdata/other/test.properties");

        new PropertiesResolver().resolve(props);
        Assertions.assertEquals("unknown://src/test/resources/testdata/other/test.properties",
                props.getProperty("prop2"));
    }

    @Test
    void testLiteral()
    throws PropertiesResolverException
    {
        Properties props = new Properties();

        props.setProperty("prop1", "value1");
        props.setProperty("prop2", "literal:file://");

        new PropertiesResolver().resolve(props);

        Assertions.assertEquals("value1", props.getProperty("prop1"));
        Assertions.assertEquals("file://", props.getProperty("prop2").trim());
    }
}
