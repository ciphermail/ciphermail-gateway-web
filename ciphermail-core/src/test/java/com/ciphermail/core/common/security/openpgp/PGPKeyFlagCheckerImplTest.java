/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PGPKeyFlagCheckerImplTest
{
    private static final File testBase = new File(TestUtils.getTestDataDir(), "pgp/");

    private static PGPKeyFlagCheckerImpl keyFlagChecker;

    @Before
    public void before()
    {
        keyFlagChecker = new PGPKeyFlagCheckerImpl(new PGPSignatureValidatorImpl(),
                new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl()));
    }

    @Test
    public void testIsValidForEncryption()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        // Master key does not allow encryption
        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        // Master key does not allow encryption
        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(2)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(3)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(4)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(5)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "subkey-allow-sign-encrypt-0x4DB66E16.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "one-key-sign-certifiy-only-0x6FBF2E26.asc"));

        assertEquals(1, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "missing-key-flags-0x2DDB17D0.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "0xF43856A3.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
    }

    @Test
    public void testIsValidForEncryptionEC()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "curve-25519@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-256@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-384@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-521@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-256@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-384@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-512@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "secp256k1@example.com.asc"));

        assertEquals(2, keys.size());

        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));
        assertFalse(keys.get(0).isEncryptionKey());
        assertTrue(keys.get(1).isEncryptionKey());
    }

    @Test
    public void testIsValidForSigning()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        // Sub key does not allow signing
        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        // Master key does not allow encryption
        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));
        assertTrue(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(2)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(3)));
        assertTrue(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(4)));
        assertTrue(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(5)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "subkey-allow-sign-encrypt-0x4DB66E16.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertTrue(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "one-key-sign-certifiy-only-0x6FBF2E26.asc"));

        assertEquals(1, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "missing-key-flags-0x2DDB17D0.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "0xF43856A3.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));
    }

    @Test
    public void testIsValidForSigningEC()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "curve-25519@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-256@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-384@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "nist-p-521@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-256@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-384@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "brainpool-p-512@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));

        keys = PGPKeyUtils.readPublicKeys(new File(testBase, "secp256k1@example.com.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));
    }

    @Test
    public void testIsValidForSigningDSAMasterKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase,
                "SIGN-DATA-flag-missing-on-master-key.asc"));

        assertEquals(2, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
    }

    @Test
    public void testIsValidForSigningV3Key()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase,
                "short-key-collision-1-19980101.gpg.asc"));

        assertEquals(1, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
    }

    /*
     * Test for CVE-2013-4351 (key with empty, empty and not missing, key flags should not be usable)
     *
     * See this discussion for more info: http://article.gmane.org/gmane.comp.encryption.gpg.devel/17712
     */
    @Test
    public void testEmptyKeyFlags()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "empty-key-flags.asc"));

        assertEquals(4, keys.size());

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));

        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(1)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(1)));

        assertTrue(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(2)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(2)));

        assertFalse(keyFlagChecker.isSubKeyValidForSigning(keys.get(0), keys.get(3)));
        assertFalse(keyFlagChecker.isSubKeyValidForEncryption(keys.get(0), keys.get(3)));
    }

    @Test
    public void testInvalidUserID()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "invalid-userid-sig.asc"));

        assertFalse(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
    }

    @Test
    public void testInvalidUserIDSkipSignatureCheck()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(testBase, "invalid-userid-sig.asc"));

        keyFlagChecker.setValidateSignatures(false);

        assertTrue(keyFlagChecker.isMasterKeyValidForSigning(keys.get(0)));
        assertFalse(keyFlagChecker.isMasterKeyValidForEncryption(keys.get(0)));
    }
}
