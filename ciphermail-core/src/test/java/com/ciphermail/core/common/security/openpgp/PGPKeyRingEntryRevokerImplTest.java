/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorResult;
import com.ciphermail.core.common.security.openpgp.validator.RevocationValidator;
import com.ciphermail.core.common.security.openpgp.validator.StandardValidatorContextObjects;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingEntryRevokerImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "/pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPKeyRingEntryRevoker keyRingEntryRevoker;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private List<PGPKeyRingEntry> importSecretKeyRing(InputStream input, String importPassword)
    throws Exception
    {
        PGPKeyRingImporterImpl importer = new PGPKeyRingImporterImpl(keyRing);

        return importer.importKeyRing(input, new StaticPasswordProvider(importPassword));
    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, MissingKeyAlias.ALLOWED);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private void addEmailAddress(long keyID, String email)
    throws Exception
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        Set<String> emails = entry.getEmail();

        emails.add(email);

        entry.setEmail(emails);
    }

    private Set<String> getEmailAddresses(long keyID)
    throws Exception
    {
        return new LinkedHashSet<>(getByKeyID(keyID).getEmail());
    }

    private void revokeKey(long keyID)
    throws Exception
    {
        keyRingEntryRevoker.revokeKeyRingEntry(getByKeyID(keyID).getID());
    }

    private boolean isRevoked(long keyID)
    throws Exception
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        PGPPublicKey key = entry.getPublicKey();

        PGPPublicKey masterKey = null;

        if (!key.isMasterKey()) {
            masterKey = entry.getParentKey().getPublicKey();
        }

        RevocationValidator revocationValidator = new RevocationValidator(new PGPSignatureValidatorImpl());

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, masterKey);

        PGPPublicKeyValidatorResult v = revocationValidator.validate(key, context);

        return !v.isValid();
    }

    @Test
    public void testRevokeMasterKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TEST_BASE, "test@example.com.gpg.key");

                List<PGPKeyRingEntry> imported = importSecretKeyRing(new FileInputStream(file), "test");

                assertEquals(2, imported.size());

                PGPPublicKey master = imported.get(0).getPublicKey();
                PGPPublicKey sub = imported.get(1).getPublicKey();

                // Add an email address. After revocation the email address should not be removed
                addEmailAddress(master.getKeyID(), "extra@example.com");

                Set<String> emails = getEmailAddresses(master.getKeyID());

                assertEquals(2, emails.size());
                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("extra@example.com"));

                assertFalse(isRevoked(master.getKeyID()));
                assertFalse(isRevoked(sub.getKeyID()));

                revokeKey(master.getKeyID());

                assertTrue(isRevoked(master.getKeyID()));

                // Sub key should also be revoked (this is actually more a test of RevocationValidator)
                assertTrue(isRevoked(sub.getKeyID()));

                // After revocation the email address should not be removed
                emails = getEmailAddresses(master.getKeyID());

                assertEquals(2, emails.size());
                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("extra@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRevokeSubKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                File file = new File(TEST_BASE, "test@example.com.gpg.key");

                List<PGPKeyRingEntry> imported = importSecretKeyRing(new FileInputStream(file), "test");

                assertEquals(2, imported.size());

                PGPPublicKey master = imported.get(0).getPublicKey();
                PGPPublicKey sub = imported.get(1).getPublicKey();

                assertFalse(isRevoked(master.getKeyID()));
                assertFalse(isRevoked(sub.getKeyID()));

                revokeKey(sub.getKeyID());

                assertTrue(isRevoked(sub.getKeyID()));

                // master key should not be revoked
                assertFalse(isRevoked(master.getKeyID()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
