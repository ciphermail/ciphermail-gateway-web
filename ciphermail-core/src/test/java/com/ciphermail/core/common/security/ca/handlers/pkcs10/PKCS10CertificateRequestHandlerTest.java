/*
 * Copyright (c) 2016-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.security.ca.handlers.pkcs10;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.handlers.pkcs10.PKCS10CertificateRequestHandler.DataWrapper;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import com.ciphermail.core.common.security.keystore.KeyStoreProviderKeyStoreWrapper;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.security.auth.x500.X500Principal;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.UUID;

class PKCS10CertificateRequestHandlerTest
{
    private static PKCS10CertificateRequestHandler certificateRequestHandler;
    private static KeyStore keyStore;
    private static PasswordProvider passwordProvider;

    private static final String ENCODED_DATA =
            "{\"pemEncodedPKCS10Request\":\"-----BEGIN CERTIFICATE REQUEST-----\\n" +
            "MIICdTCCAV0CAQAwMDENMAsGA1UEAwwEdGVzdDEfMB0GCSqGSIb3DQEJAQwQdGVz\\n" +
            "dEBleGFtcGxlLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMWz\\n" +
            "9XW4++UC4Jf9psvQ/ATPhGVehRZl9Cur3IcRDE5JO6kmaJXNrjUiWjL/FfAtSg/w\\n" +
            "C0ubDDpgJ6tQfOBiX50BMSq5IdiXrssNyGzllxDY65K5tG7F6YbUKbXajmJIepqa\\n" +
            "lmz7qrBTkE3hmPzm5JIFt4Z9ftK3+mfPSlf9vnM70hIxrIhN9HdTwXssICYxMJ2C\\n" +
            "qUwhZ4Kt8qxySjHVO4ljz/gYBhaHA0Sr6F57bWmyNQtLeaS1qJpfH7TJGsmI2nCW\\n" +
            "rhwq3rGQfxcjacihNg6NnkoziYQossWBlrH9aeCMUR/53O7/Y1S5DYiaKpJTbUxf\\n" +
            "WiIqNnO/IlFjU+9CmmMCAwEAAaAAMA0GCSqGSIb3DQEBCwUAA4IBAQA+1dkefbH5\\n" +
            "XsCg9sOru8zTbR62jyamn5Xl8GOyO1xIPMOTfjUY1DTTucAts94Z6I14ofO5cvBP\\n" +
            "ZswIuNU21UxNHh/fLJORoulYPLO8WTneeUCgXyFeptsImHSs5au3emGvuFMOdnUg\\n" +
            "AvdZr3WYt+axvPyhStcz7z9ogVsMjMOJ+8v3DaAhVMJJqXI2j+HlgQwcgEhLHzko\\n" +
            "iCaOcTkaEP5Dv2utdYwQZwhvdjElWtPqTDPJAUZl0ktKd8/prsSIlJWPtv5kbUYd\\n" +
            "EVTrzNqvIBtv3FVgdZUo9wnqk0sY5n5WCEnxGf/MFHhloFLsdtlJvEcGyvPmpfeB\\n" +
            "ie4+sY7iLMV7\\n" +
            "-----END CERTIFICATE REQUEST-----\\n" +
            "\",\"encodedPublicKey\":\"rO0ABXcEAAAAAXQAA1JTQXQABVguNTA5fnIANmNvb" +
            "S5jaXBoZXJtYWlsLmNvcmUuY29tbW9uLnNlY3VyaXR5LktleUVuY29kZXIkS2V5VHlw" +
            "ZQAAAAAAAAAAEgAAeHIADmphdmEubGFuZy5FbnVtAAAAAAAAAAASAAB4cHQABlBVQkx" +
            "JQ3cBAHVyAAJbQqzzF/gGCFTgAgAAeHAAAAEmMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ" +
            "8AMIIBCgKCAQEAxbP1dbj75QLgl/2my9D8BM+EZV6FFmX0K6vchxEMTkk7qSZolc2uN" +
            "SJaMv8V8C1KD/ALS5sMOmAnq1B84GJfnQExKrkh2Jeuyw3IbOWXENjrkrm0bsXphtQp" +
            "tdqOYkh6mpqWbPuqsFOQTeGY/ObkkgW3hn1+0rf6Z89KV/2+czvSEjGsiE30d1PBeyw" +
            "gJjEwnYKpTCFngq3yrHJKMdU7iWPP+BgGFocDRKvoXnttabI1C0t5pLWoml8ftMkayY" +
            "jacJauHCresZB/FyNpyKE2Do2eSjOJhCiyxYGWsf1p4IxRH/nc7v9jVLkNiJoqklNtT" +
            "F9aIio2c78iUWNT70KaYwIDAQAB\",\"keyAlias\":\"PKCS10-CERTIFICATE-REQUEST-HANDLER:" +
            "0E51B6E78FB3F632F77E7A05ECD2A1D41973DD652DC9C0FB9013117BFAE435FF9B031936E201A288C" +
            "3476BC7481AE9E50F6A28471F4ED0B0C42AB0F8C263A77A\"}";

    @BeforeAll
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        keyStore.load(null);

        passwordProvider = new StaticPasswordProvider("test");

        certificateRequestHandler = new PKCS10CertificateRequestHandler(new KeyStoreProviderKeyStoreWrapper(keyStore),
                passwordProvider, new StandardSerialNumberGenerator());
    }

    private static class TestCertificateRequest implements CertificateRequest
    {
        private X500Principal subject;
        private String email;
        private int keyLength;
        private byte[] data;

        @Override
        public UUID getID() {
            return UUID.fromString("static");
        }

        @Override
        public Date getCreated() {
            return new Date();
        }

        @Override
        public void setSubject(X500Principal subject) {
            this.subject = subject;
        }

        @Override
        public X500Principal getSubject() {
            return subject;
        }

        @Override
        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public String getEmail() {
            return email;
        }

        @Override
        public void setValidity(int validity) {
        }

        @Override
        public int getValidity() {
            return 100;
        }

        @Override
        public void setSignatureAlgorithm(String SignatureAlgorithm) {
        }

        @Override
        public String getSignatureAlgorithm() {
            return "SHA256WithRSA";
        }

        @Override
        public void setKeyLength(int keyLength) {
            this.keyLength = keyLength;
        }

        @Override
        public int getKeyLength() {
            return keyLength;
        }

        @Override
        public void setCRLDistributionPoint(String CRLDistributionPoint) {
        }

        @Override
        public String getCRLDistributionPoint() {
            return null;
        }

        @Override
        public void setInfo(String info) {
        }

        @Override
        public String getInfo() {
            return null;
        }

        @Override
        public void setData(byte[] data) {
            this.data = data;
        }

        @Override
        public byte[] getData() {
            return data;
        }

        @Override
        public void setIteration(int iteration) {
        }

        @Override
        public int getIteration() {
            return 0;
        }

        @Override
        public void setLastUpdated(Date lastUpdated) {
        }

        @Override
        public Date getLastUpdated() {
            return new Date();
        }

        @Override
        public void setNextUpdate(Date nextUpdate) {
        }

        @Override
        public Date getNextUpdate() {
            return new Date();
        }

        @Override
        public void setLastMessage(String message) {
        }

        @Override
        public String getLastMessage() {
            return null;
        }

        @Override
        public String getCertificateHandlerName() {
            return PKCS10CertificateRequestHandler.NAME;
        }
    }

    @Test
    void testPKCS10CertificateRequestHandler()
    throws Exception
    {
        CertificateRequest request = new TestCertificateRequest();

        X500PrincipalBuilder principalBuilder = X500PrincipalBuilder.getInstance();

        principalBuilder.setCommonName("test");
        principalBuilder.setEmail("test@example.com");

        request.setEmail("test@example.com");
        request.setKeyLength(2048);
        request.setSubject(principalBuilder.buildPrincipal());

        certificateRequestHandler.handleRequest(request);

        Assertions.assertNotNull(request.getData());

        DataWrapper wrapper = DataWrapper.decode(request.getData());

        Assertions.assertNotNull(wrapper.getPemEncodedPKCS10Request());
        Assertions.assertNotNull(wrapper.getKeyAlias());
        Assertions.assertNotNull(wrapper.getEncodedPublicKey());

        Assertions.assertTrue(wrapper.getKeyAlias().startsWith("PKCS10-CERTIFICATE-REQUEST-HANDLER:"));
        Assertions.assertEquals(163, wrapper.getKeyAlias().length());

        String pemEncodedRequest = certificateRequestHandler.getPemEncodedRequest(request);

        PKCS10CertificationRequest pkcs10Request;

        try (PEMParser pemParser = new PEMParser(new StringReader(pemEncodedRequest))) {
            pkcs10Request = (PKCS10CertificationRequest) pemParser.readObject();
        }

        Assertions.assertNotNull(pkcs10Request);

        Assertions.assertEquals("CN=test,E=test@example.com", pkcs10Request.getSubject().toString());

        // use the generated dummy certificate to test whether handleRequest works
        X509Certificate certificate = (X509Certificate) keyStore.getCertificate(wrapper.getKeyAlias());

        Assertions.assertNotNull(certificate);

        KeyAndCertificate keyAndCertificate = certificateRequestHandler.handleRequest(request, certificate);

        Assertions.assertNotNull(keyAndCertificate);
        Assertions.assertNotNull(keyAndCertificate.getPrivateKey());
        Assertions.assertNotNull(keyAndCertificate.getCertificate());
        Assertions.assertEquals(keyStore.getKey(wrapper.getKeyAlias(), passwordProvider.getPassword()), keyAndCertificate.getPrivateKey());
        Assertions.assertEquals(keyStore.getCertificate(wrapper.getKeyAlias()), keyAndCertificate.getCertificate());
    }

    @Test
    void testPKCS10CertificateRequestHandlerDecode()
    throws Exception
    {
        DataWrapper wrapper = DataWrapper.decode(ENCODED_DATA.getBytes(StandardCharsets.UTF_8));

        Assertions.assertNotNull(wrapper.getPemEncodedPKCS10Request());
        Assertions.assertEquals("PKCS10-CERTIFICATE-REQUEST-HANDLER:" +
                                "0E51B6E78FB3F632F77E7A05ECD2A1D41973DD652DC9C0FB9013117BFAE435FF9B0319" +
                                "36E201A288C3476BC7481AE9E50F6A28471F4ED0B0C42AB0F8C263A77A", wrapper.getKeyAlias());
        Assertions.assertNotNull(wrapper.getEncodedPublicKey());

        PKCS10CertificationRequest pkcs10Request;

        try (PEMParser pemParser = new PEMParser(new StringReader(wrapper.getPemEncodedPKCS10Request()))) {
            pkcs10Request = (PKCS10CertificationRequest) pemParser.readObject();
        }

        Assertions.assertEquals("CN=test,E=test@example.com", pkcs10Request.getSubject().toString());
    }
}
