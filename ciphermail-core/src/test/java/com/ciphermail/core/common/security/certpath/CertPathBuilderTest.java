/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreParameters;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertPathBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509RootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509CertStore;

    @Autowired
    private X509CRLStoreExt x509CRLStore;

    private CertStore certStore;
    private CertStore rootStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters rootStoreParams;
    private Set<TrustAnchor> trustAnchors;

    @BeforeClass
    public static void setUpBeforeClass()
    {
        // testAlgorithmIdentifierComparisonFailed requires the following system property
        System.setProperty("org.bouncycastle.x509.allow_absent_equiv_NULL", "true");
    }

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(x509CertStore, x509CRLStore);

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        rootStoreParams = new X509CertStoreParameters(x509RootStore);

        rootStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, rootStoreParams,
                CipherMailProvider.PROVIDER);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCertStore().removeAllEntries();
                certStoreParams.getCRLStore().removeAllEntries();
                rootStoreParams.getCertStore().removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCRL(String filename, X509CRLStoreExt crlStore)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                crlStore.addCRL(TestUtils.loadX509CRL(new File(TEST_BASE, "crls/" + filename)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * When importing a large number of certificates, using a transaction per certificate is faster than one large
     * transaction
     */
    private int importCertificatesPerTransaction(String certificateFilename, X509CertStoreExt store)
    throws Exception
    {
        MutableInt imported = new MutableInt();

        File file = new File(TEST_BASE, "certificates/" + certificateFilename);

        for (Certificate certificate : CertificateUtils.readCertificates(file))
        {
            if (certificate instanceof X509Certificate)
            {
                transactionOperations.executeWithoutResult(status ->
                {
                    try {
                        if (!store.contains((X509Certificate) certificate))
                        {
                            store.addCertificate((X509Certificate) certificate);

                            imported.increment();
                        }
                    }
                    catch (Exception e) {
                        throw new UnhandledException(e);
                    }
                });
            }
        }

        return imported.intValue();
    }

    private Set<TrustAnchor> getTrustAnchors()
    throws Exception
    {
        TrustAnchorBuilder builder = new SimpleTrustAnchorBuilder();

        builder.addCertificates(rootStore);

        return builder.getTrustAnchors();
    }

    /*
     * This tests checks whether a certificate is accepted which has a missing AlgorithmIdentifier parameter
     * for the outer cert and a NULL parameter for the inner TBS cert. This is strange but most PKI implementation
     * accept such a certificate.
     *
     *  According to RFC 3370
     *
     * The parameter should be absent when generating a certificate but applications must accept a NULL parameter.
     *
     *   There are two possible encodings for the SHA-1 AlgorithmIdentifier
     *   parameters field.  The two alternatives arise from the fact that when
     *   the 1988 syntax for AlgorithmIdentifier was translated into the 1997
     *   syntax, the OPTIONAL associated with the AlgorithmIdentifier
     *   parameters got lost.  Later the OPTIONAL was recovered via a defect
     *   report, but by then many people thought that algorithm parameters
     *   were mandatory.  Because of this history some implementations encode
     *   parameters as a NULL element and others omit them entirely.  The
     *   correct encoding is to omit the parameters field; however,
     *   implementations MUST also handle a SHA-1 AlgorithmIdentifier
     *   parameters field which contains a NULL.
     *
     *   The AlgorithmIdentifier parameters field is OPTIONAL.  If present,
     *   the parameters field MUST contain a NULL.  Implementations MUST
     *   accept SHA-1 AlgorithmIdentifiers with absent parameters.
     *   Implementations MUST accept SHA-1 AlgorithmIdentifiers with NULL
     *   parameters.  Implementations SHOULD generate SHA-1
     *   AlgorithmIdentifiers with absent parameters.
     */
    @Test
    public void testAlgorithmIdentifierComparisonFailed()
    throws Exception
    {
        importCertificatesPerTransaction("AC_MINEFI_DPMA.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("MINEFI_AUTORITE_DE_CERTIFICATION_RACINE.cer", rootStoreParams.getCertStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                // Since the certificates under test expired on 08/06/2016 we need to set
                // the date explicitly
                builder.setDate(TestUtils.parseDate("01-Jun-2016 00:00:00 GMT"));
                builder.addCertStore(certStore);
                builder.setTrustAnchors(getTrustAnchors());

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("30303031303935373731383130383135"));
                selector.setIssuer(new X500Principal(
                        "CN=MINEFI-AUTORITE DE CERTIFICATION RACINE, OU=AGENCE AUTORITE, O=MINEFI, C=FR"));

                CertPathBuilderResult results = builder.buildPath(selector);

                assertNotNull(results.getCertPath());
                assertEquals(1, results.getCertPath().getCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testNoTrustAnchors()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
                importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
                importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();
                builder.addCertStore(certStore);

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                try {
                    builder.buildPath(selector);

                    fail("Should have failed");
                }
                catch(CertPathBuilderException e) {
                    assertEquals(PKIXCertificatePathBuilder.NO_ROOTS_ERROR_MESSAGE, e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * The CRL is not signed with the CA private key but by another private key
     */
    @Test
    public void testBuildPathCRLSignedByIncorrectKey()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());
        addCRL("test-ca-signed-incorrect-key.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    // should be thrown because the crl was not signed by the CA but the issuer is the CA
                    Throwable rootCause = ExceptionUtils.getRootCause(e);

                    assertEquals("CRL does not verify with supplied public key.", rootCause.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Same test as above but now there is also the correct CRL. This should succeed.
     */
    @Test
    public void testBuildPathCRLSignedByIncorrectKeyAndCorrectKey()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());
        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-ca-signed-incorrect-key.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                CertPathBuilderResult result = builder.buildPath(selector);

                assertEquals(2, result.getCertPath().getCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Test path building for a certificate with critical extended key usage containing EMAILPROTECTION when
     * SMIMEExtendedKeyUsageCertPathChecker is added to the path checkers
     */
    @Test
    public void testBuildPathManyCertificates()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("random-self-signed-1000.p7b", certStoreParams.getCertStore());
        //addCertificatesBulk("random-self-signed-10000.p7b");
        //addCertificatesBulk("random-self-signed-40000.p7b");

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                int tries = 100;

                long start = System.currentTimeMillis();

                TrustAnchorBuilder trustAnchorBuilder = new CertStoreTrustAnchorBuilder(rootStoreParams.getCertStore(),
                        0 /* milliseconds */);

                for (int i = 0; i < tries; i++)
                {
                    X509CertSelector selector = new X509CertSelector();

                    selector.setSerialNumber(BigIntegerUtils.hexDecode("116A448F117FF69FE4F2D4D38F689D7"));
                    selector.setIssuer(new X500Principal(
                            "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                    CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                    builder.setTrustAnchors(trustAnchorBuilder.getTrustAnchors());
                    builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                    builder.addCertStore(certStore);
                    builder.setRevocationEnabled(true);

                    CertPathBuilderResult result = builder.buildPath(selector);

                    assertEquals(2, result.getCertPath().getCertificates().size());
                }

                double end = (System.currentTimeMillis() - start) * 0.001 / tries;

                System.out.println("Seconds / build: " + end);

                start = System.currentTimeMillis();

                Collection<? extends Certificate> certificates = certStore.getCertificates(new X509CertSelector());

                end = (System.currentTimeMillis() - start) * 0.001 / certificates.size();

                System.out.println("Seconds / certificate: " + end);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathCRLUnavailable()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    Throwable cause = ExceptionUtils.getCause(e);

                    assertTrue(cause.getMessage().startsWith("No CRLs found"));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathCRLUnavailableButCRLCheckOff()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(false);

                CertPathBuilderResult result = builder.buildPath(selector);

                List<? extends Certificate> certificates = result.getCertPath().getCertificates();

                assertEquals(2, certificates.size());

                CertStore store = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certificates),
                        SecurityFactoryBouncyCastle.PROVIDER_NAME);

                Collection<? extends Certificate> foundCertificates = store.getCertificates(selector);

                assertEquals(1, foundCertificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathRootNotFound()
    throws Exception
    {
        // root store cannot be empty so we just load something
        importCertificatesPerTransaction("dod-mega-crl.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertStore(certStore);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    assertEquals("No issuer certificate for certificate in certification path found.", e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathTargetNotFound()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("123"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertStore(certStore);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    assertEquals("No certificate found matching targetConstraints.", e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathEndCertRevoked()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCD741088707366E9727452C9770"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e)
                {
                    // CertPathValidatorException should have been thrown because the certificate is revoked
                    Throwable cause = ExceptionUtils.getCause(e);

                    assertEquals("Certificate revocation after 2007-11-30 10:38:35 +0000, reason: privilegeWithdrawn",
                            cause.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPathCACertRevoked()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    // CertPathValidatorException should have been thrown because the certificate has a
                    // key usage extension that is critical.
                    Throwable cause = ExceptionUtils.getCause(e);

                    assertEquals("Certificate revocation after 2007-11-30 10:38:35 +0000, reason: cACompromise",
                            cause.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildPath()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("windows-xp-all-roots.p7b", rootStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("windows-xp-all-intermediates.p7b", certStoreParams.getCertStore());
        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("intel-basic-enterprise-issuing-CA.crl", certStoreParams.getCRLStore());
        addCRL("itrus.com.cn.crl", certStoreParams.getCRLStore());
        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());
        addCRL("ThawteSGCCA.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                final int tries = 5;

                long start = System.currentTimeMillis();

                for (int i = 0; i < tries; i++)
                {
                    trustAnchors = getTrustAnchors();

                    X509CertSelector selector = new X509CertSelector();

                    selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD110A82F742D0AE14A71B651962"));
                    selector.setIssuer(new X500Principal(
                            "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                    CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                    builder.setTrustAnchors(trustAnchors);
                    builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                    builder.addCertStore(certStore);
                    builder.setRevocationEnabled(true);

                    CertPathBuilderResult result = builder.buildPath(selector);

                    List<? extends Certificate> certificates = result.getCertPath().getCertificates();

                    assertEquals(2, certificates.size());
                    assertEquals("115FD110A82F742D0AE14A71B651962", X509CertificateInspector.getSerialNumberHex(
                            (X509Certificate) certificates.get(0)));
                    assertEquals("115FCAD6B536FD8D49E72922CD1F0DA", X509CertificateInspector.getSerialNumberHex(
                            (X509Certificate) certificates.get(1)));
                }

                System.out.println("testBuildPath. Seconds / try: " +
                        (System.currentTimeMillis() - start) * 0.001 / tries);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Test a certificate path building with a certificate containing a critical extended key usage. Default
     * version of BC path builder does not understand this critical extension.
     */
    @Test
    public void testBuildPathEKUCriticalNoEmailProtection()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("116A448F117FF69FE4F2D4D38F689D7"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                /*
                 * Newer releases of BC no longer check the EKU
                 * See PKIXCertPathValidatorSpi commit fddfc766c9398792bbb43b63de845ed65040d44e at 2015-04-23 06:48:11
                 */
//                try {
//                    builder.buildPath(selector);
//
//                    fail();
//                }
//                catch(CertPathBuilderException e) {
//                    // CertPathValidatorException should have been thrown because the certificate has a
//                    // key usage extension that is critical.
//                    Throwable cause = ExceptionUtils.getCause(e);
//
//                    assertTrue(cause instanceof CertPathValidatorException);
//                    assertNotNull(cause);
//                    assertEquals("Certificate has unsupported critical extension: [2.5.29.37]", cause.getMessage());
//                }

                CertPathBuilderResult result = builder.buildPath(selector);

                assertEquals(2, result.getCertPath().getCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Test path building for a certificate with critical extended key usage containing EMAILPROTECTION when
     * SMIMEExtendedKeyUsageCertPathChecker is added to the path checkers
     */
    @Test
    public void testBuildPathEKUCriticalCertPathCheckerAdded()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("116A448F117FF69FE4F2D4D38F689D7"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                CertPathBuilderResult result = builder.buildPath(selector);

                assertEquals(2, result.getCertPath().getCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Test a certificate path building with a certificate containing a critical extended key usage without the
     * EMAILPROTECTION parameter. Default version of BC path builder does not understand this critical extension so
     * we add a SMIMEExtendedKeyUsageCertPathChecker. The certificate does not contain EMAILPROTECTION so the
     * CertPath checker will throw an exception.
     */
    @Test
    public void testBuildPathEKUCriticalNoEmailProtectionCertPathCheckerAdded()
    throws Exception
    {
        // add roots
        importCertificatesPerTransaction("mitm-test-root.cer", rootStoreParams.getCertStore());

        importCertificatesPerTransaction("mitm-test-ca.cer", certStoreParams.getCertStore());
        importCertificatesPerTransaction("testCertificates.p7b", certStoreParams.getCertStore());

        addCRL("test-ca.crl", certStoreParams.getCRLStore());
        addCRL("test-root-ca-not-revoked.crl", certStoreParams.getCRLStore());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                trustAnchors = getTrustAnchors();

                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD035BA042503BCC6CA44680F9F8"));
                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

                builder.setTrustAnchors(trustAnchors);
                builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
                builder.addCertStore(certStore);
                builder.setRevocationEnabled(true);

                try {
                    builder.buildPath(selector);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    // CertPathValidatorException should have been thrown because the certificate has a
                    // key usage extension that is critical.
                    Throwable cause = ExceptionUtils.getRootCause(e);

                    assertTrue(cause instanceof CertPathValidatorException);

                    assertEquals(SMIMEExtendedKeyUsageCertPathChecker.MISSING_SMIME_EXTENDED_KEY_USAGE, cause.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
