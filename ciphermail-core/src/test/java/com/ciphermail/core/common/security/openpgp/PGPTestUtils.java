/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartFlattener;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.SigningKeyValidator;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for testing PGP functionality
 *
 * @author Martijn Brinkers
 *
 */
public class PGPTestUtils
{
    public static List<Part> getAllParts(MimeMessage message)
    throws MessagingException, IOException, PartException
    {
        PartFlattener flattener = new PartFlattener();

        return flattener.flatten(message);
    }

    /**
     * Verify a signed MIME part using GPG
     */
    public static boolean verify(Part part, Writer outputWriter, Writer errorWriter)
    throws Exception
    {
        GPG gpg = new GPG();

        File tmpFile = TestUtils.createTempFile(".asc");

        IOUtils.copy(part.getInputStream(), new FileOutputStream(tmpFile));

        OutputStream output = WriterOutputStream.builder().setWriter(outputWriter).setCharset(StandardCharsets.UTF_8).get();
        OutputStream error = WriterOutputStream.builder().setWriter(errorWriter).setCharset(StandardCharsets.UTF_8).get();

        try {
            boolean valid = gpg.verify(tmpFile, output, error);

            if (!valid)
            {
                System.out.println("**** output ****");
                System.out.println(outputWriter.toString());
                System.out.println("**** error ****");
                System.out.println(errorWriter.toString());
            }

            return valid;
        }
        catch (Exception e)
        {
            System.out.println("**** output ****");
            System.out.println(outputWriter.toString());
            System.out.println("**** error ****");
            System.out.println(errorWriter.toString());

            throw e;
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }
    }

    /**
     * Verify a signed MIME part with detached signature using GPG
     */
    public static boolean verifyDetached(Part dataPart, boolean verifyMIME, Part signaturePart, Writer outputWriter,
            Writer errorWriter)
    throws Exception
    {
        GPG gpg = new GPG();

        File dataFile = TestUtils.createTempFile(".bin");
        File sigFile = TestUtils.createTempFile(".sig");

        FileOutputStream errorOutputStream;

        try (
            FileOutputStream dataOutputStream = new FileOutputStream(dataFile);
            FileOutputStream sigOutputStream = new FileOutputStream(sigFile);
        )
        {
            // should we verify the MIME encoded content or the content itself?
            if (verifyMIME) {
                dataPart.writeTo(dataOutputStream);
            }
            else {
                IOUtils.copy(dataPart.getInputStream(), dataOutputStream);
            }
            IOUtils.copy(signaturePart.getInputStream(), sigOutputStream);
        }

        OutputStream output = WriterOutputStream.builder().setWriter(outputWriter).setCharset(StandardCharsets.UTF_8).get();
        OutputStream error = WriterOutputStream.builder().setWriter(errorWriter).setCharset(StandardCharsets.UTF_8).get();

        try {
            boolean valid = gpg.verify(dataFile, sigFile, output, error);

            if (!valid)
            {
                System.out.println("**** output ****");
                System.out.println(outputWriter.toString());
                System.out.println("**** error ****");
                System.out.println(errorWriter.toString());
            }

            return valid;
        }
        catch (Exception e)
        {
            System.out.println("**** output ****");
            System.out.println(outputWriter.toString());
            System.out.println("**** error ****");
            System.out.println(errorWriter.toString());

            throw e;
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }
    }

    /**
     * Verify a signed MIME part with detached signature using GPG
     */
    public static boolean verifyPGPMIME(MimeMessage signedMessage, Writer outputWriter, Writer errorWriter)
    throws Exception
    {
        // Clone message to make sure it has been written and additional CR/LF pairs have been removed
        signedMessage = MailUtils.cloneMessage(signedMessage);

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) signedMessage.getContent());

        if (parts.length != 2) {
            throw new IllegalArgumentException("MIME message does not have two parts");
        }

        return verifyDetached(parts[0], true, parts[1], outputWriter, errorWriter);
    }

    public static boolean decrypt(Part part, String password, Writer outputWriter, Writer errorWriter)
    throws Exception
    {
        return decrypt(part, password, outputWriter, errorWriter, false);
    }

    /**
     * Decrypt a message with GPG
     */
    public static boolean decrypt(Part part, String password, Writer outputWriter, Writer errorWriter,
            boolean ignoreMDCError)
    throws Exception
    {
        GPG gpg = new GPG();

        gpg.setIgnoreMDCError(ignoreMDCError);

        File file = TestUtils.createTempFile(".asc");

        IOUtils.copy(part.getInputStream(), new FileOutputStream(file));

        OutputStream output = WriterOutputStream.builder().setWriter(outputWriter).setCharset(StandardCharsets.UTF_8).get();
        OutputStream error = WriterOutputStream.builder().setWriter(errorWriter).setCharset(StandardCharsets.UTF_8).get();

        try {
            return gpg.decrypt(file, password, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }
    }

    /**
     * Create a handler which can be used to validate a PGP signature with BC
     */
    public static PGPRecursiveValidatingMIMEHandler createPGPHandler(File publicKeyRingFile, File secretKeyRingFile,
        String secretKeyPassword)
    throws IOException, PGPException
    {
        PGPKeyPairProvider keyPairProvider;

        if (secretKeyRingFile != null)
        {
            PGPSecretKeyRingCollection secretKeyRing = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                    new FileInputStream(secretKeyRingFile)));

            keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(secretKeyRing, new StaticPasswordProvider(
                    secretKeyPassword));
        }
        else {
            // Create a dummy PGPKeyPairProvider that always returns an empty list
            keyPairProvider = keyID -> new LinkedList<PGPKeyRingPair>();
        }

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(publicKeyRingFile);

        PGPContentSignatureVerifier signatureVerifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureValidator signatureValidator = new PGPSignatureValidatorImpl();

        PGPPublicKeyValidator publicKeyValidator = new SigningKeyValidator(new PGPKeyFlagCheckerImpl(
                signatureValidator, new PGPUserIDValidatorImpl(signatureValidator)));

        PGPContentSignatureValidator contentSignatureValidator = new PGPContentSignatureValidatorImpl(signatureVerifier,
                publicKeyValidator);

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                contentSignatureValidator);

        handler.setEnablePGPUniversalWorkaround(true);
        handler.setInlinePGPEnabled(true);
        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        return handler;
    }
}
