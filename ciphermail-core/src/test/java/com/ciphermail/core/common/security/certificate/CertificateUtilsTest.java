/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CertificateUtilsTest
{
    public static final File TEST_BASE = TestUtils.getTestDataDir();

    /*
     * A test certificate which is not an X509Certificate
     */
    private static class DummyCertificate extends Certificate
    {
        public DummyCertificate() {
            super("dummy-certificate");
        }

        @Override
        public byte[] getEncoded() {return null;}

        @Override
        public void verify(PublicKey key) {}

        @Override
        public void verify(PublicKey key, String sigProvider) {}

        @Override
        public String toString() {return null;}

        @Override
        public PublicKey getPublicKey() {return null;}
    }

    @Test
    public void testLoadP7B()
    throws Exception
    {
        // Somehow this fails when used with the SUN provider. There must be a certificate that is not
        // accepted by the SUN provider
        File file = new File(TEST_BASE, "certificates/windows-xp-all-intermediates.p7b");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        assertEquals(123, certificates.size());
    }

    @Test
    public void testLoadCertificatesBase64File()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/testcertificateBase64.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        assertEquals(1, certificates.size());
    }

    @Test
    public void testLoadCertificatesBinaryFile()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/testcertificate.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        assertEquals(1, certificates.size());
    }

    @Test
    public void testWriteCertificates()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/testcertificate.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        file = new File(TEST_BASE, "certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<>();

        allCerts.addAll(certificates);

        allCerts.addAll(CertificateUtils.readCertificates(file));

        assertEquals(2, allCerts.size());

        File p7b = TestUtils.createTempFile(".p7b");

        CertificateUtils.writeCertificates(allCerts, p7b);

        Collection<? extends Certificate> loaded = CertificateUtils.readCertificates(p7b);

        assertEquals(2, loaded.size());
    }

    @Test
    public void testWriteCertificatesPEM()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/testcertificate.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        file = new File(TEST_BASE, "certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<>();

        allCerts.addAll(certificates);

        allCerts.addAll(CertificateUtils.readCertificates(file));

        assertEquals(2, allCerts.size());

        File pemFile = TestUtils.createTempFile(".pem");

        CertificateUtils.writeCertificates(allCerts, pemFile, ObjectEncoding.PEM);

        Collection<? extends Certificate> loaded = CertificateUtils.readCertificates(pemFile);

        assertEquals(2, loaded.size());
    }

    @Test
    public void testGetMatchingCertificates()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/testcertificate.cer");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        file = new File(TEST_BASE, "certificates/missingsmimekeyusage.cer");

        Collection<Certificate> allCerts = new LinkedList<>();

        allCerts.addAll(certificates);

        allCerts.addAll(CertificateUtils.readCertificates(file));

        assertEquals(2, allCerts.size());

        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("115fd035ba042503bcc6ca44680f9f8"));

        Collection<? extends Certificate> matching = CertificateUtils.getMatchingCertificates(allCerts, selector);

        assertEquals(1, matching.size());
    }

    @Test
    public void testGetX509CertificatesCollection()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/windows-xp-all-intermediates.p7b");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        List<X509Certificate> x509Certificates = CertificateUtils.getX509Certificates(certificates);

        assertEquals(123, x509Certificates.size());

        x509Certificates = CertificateUtils.getX509Certificates((Collection<? extends Certificate>)null);

        assertEquals(0, x509Certificates.size());

        x509Certificates = CertificateUtils.getX509Certificates(new ArrayList<>());

        assertEquals(0, x509Certificates.size());

        Collection<Certificate> certificates2 = new ArrayList<>();

        certificates2.add(null);
        certificates2.add(new DummyCertificate());
        certificates2.addAll(certificates);

        assertEquals(125, certificates2.size());

        x509Certificates = CertificateUtils.getX509Certificates(certificates2);

        assertEquals(123, x509Certificates.size());
    }

    @Test
    public void testGetX509CertificatesArray()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/windows-xp-all-intermediates.p7b");

        Certificate[] certificates = new ArrayList<Certificate>(CertificateUtils.readCertificates(file)).
                toArray(new Certificate[]{});

        X509Certificate[] x509Certificates = CertificateUtils.getX509Certificates(certificates);

        assertEquals(123, x509Certificates.length);

        x509Certificates = CertificateUtils.getX509Certificates((Certificate[])null);

        assertEquals(0, x509Certificates.length);

        x509Certificates = CertificateUtils.getX509Certificates();

        assertEquals(0, x509Certificates.length);

        certificates = new Certificate[]{null, new DummyCertificate(), certificates[0]};

        assertEquals(3, certificates.length);

        x509Certificates = CertificateUtils.getX509Certificates(certificates);

        assertEquals(1, x509Certificates.length);
    }

    @Test
    public void testFindChain()
    throws Exception
    {
        X509Certificate intermediate = TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-ca.cer"));

        X509Certificate root = TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/mitm-test-root.cer"));

        Collection<X509Certificate> certificates = CertificateUtils.readX509Certificates(
                new File(TEST_BASE, "certificates/testCertificates.p7b"));

        certificates.add(intermediate);
        certificates.add(root);

        X509Certificate target = TestUtils.loadCertificate(
                "src/test/resources/testdata/certificates/valid_certificate_mitm_test_ca.cer");

        List<X509Certificate> chain = CertificateUtils.findChain(target, certificates);

        assertNotNull(chain);
        assertEquals(3, chain.size());

        assertEquals(chain.get(0), target);
        assertEquals(chain.get(1), intermediate);
        assertEquals(chain.get(2), root);
    }

    @Test
    public void testFindCycle()
    throws Exception
    {
        X509Certificate cycleCertificateA = TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/cycle-certificate-A.cer"));

        X509Certificate cycleCertificateB = TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/cycle-certificate-B.cer"));

        X509Certificate cycleCertificateC = TestUtils.loadCertificate(
                new File(TEST_BASE, "certificates/cycle-certificate-C.cer"));

        Collection<X509Certificate> certificates = new LinkedList<>();

        certificates.add(cycleCertificateB);
        certificates.add(cycleCertificateC);

        List<X509Certificate> chain = CertificateUtils.findChain(cycleCertificateA, certificates);

        assertNotNull(chain);
        assertEquals(3, chain.size());

        assertEquals(chain.get(0), cycleCertificateA);
        assertEquals(chain.get(1), cycleCertificateC);
        assertEquals(chain.get(2), cycleCertificateB);
    }

    /*
     * Test for BC#758 https://github.com/bcgit/bc-java/issues/758. We will fix this for CertificateUtils by
     * filtering the PEM (if we detect that the input is PEM)
     */
    @Test
    public void testLoadPEMAdditionalNewline()
    throws Exception
    {
        String pem = """
                -----BEGIN CERTIFICATE-----
                MIIFnTCCA4WgAwIBAgIQAU3ninOCS2ONavh0497qEjANBgkqhkiG9w0BAQsFADBe
                MQswCQYDVQQGEwJOTDELMAkGA1UECAwCTkgxEjAQBgNVBAcMCUFtc3RlcmRhbTEu
                MCwGA1UEAwwlQ2lwaGVyTWFpbCBsaWNlbnNpbmcgcm9vdCBjZXJ0aWZpY2F0ZTAe
                Fw0xNTA2MTIxMTMxNTlaFw0zNTA2MTIxMTMxNTlaMF4xCzAJBgNVBAYTAk5MMQsw
                CQYDVQQIDAJOSDESMBAGA1UEBwwJQW1zdGVyZGFtMS4wLAYDVQQDDCVDaXBoZXJN
                YWlsIGxpY2Vuc2luZyByb290IGNlcnRpZmljYXRlMIICIjANBgkqhkiG9w0BAQEF
                AAOCAg8AMIICCgKCAgEAqV/iuV6dRUyck5hUePp9v0SsmO7G/RSCT87S6BykOrsO
                VZScamnJDy274QwUhPgh10QDeRgchNsSTjQccL8X/kVrXwcANENyl99s3PUjNTiY
                kP92oFYK5O7C2JNmCekaErsdRymKiKo8kb3MARlfv6e2IbwMQhCaQoeBqJW3dNx+
                lYw5dlULPp6UtoQl+JgSuphx6944CG32WOLdMYF/J9mUM1RrQKZkTP0cARXxnwwk
                kAvH4ye9frQc6qBpTQW6r7oQ66W77tlOho/KlEHdNe3lG+g7xo3scOIsXuBNTvee
                TiMZnNVHV7T+p4X4hZL3grAl57EXKKnOOQXwSf3FLNMJ2sKphu/Tm2xi1csZpnge
                axtbl/UMqnEOc3WTJbN9gD1KeQiJ4cfO9jmKnr39QxGSfgWvsDRqEVh62k+7TM4B
                GXpXtuS/gk6vzyGlcVAyanIWhibfsDsXQTPFjhjJqno1gxFvI1R/bWp0+WYw+LZg
                hZbWLEpIxhx6QTIWpTE60x+MEnv1j22kUZ0mj7K/JSmW0zriwA8CjDwmGp8OOiC/
                8qs+nTx6mwZWD6mrBgQPRLw57fnY3ok9gzzxSvTgmGQRIOyGQ79muoDyFTuJbU/7
                TFH2HZDP+Xt+6xHjzz+ECGxeUvDga0u3mbFUTn9DVM45a/ADrkILwmOfSflLhkMC
                AwEAAaNXMFUwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwEwYDVR0l
                BAwwCgYIKwYBBQUHAwMwHQYDVR0OBBYEFHMbqoKyKrhg++J9Dc3sgcybW+wPMA0G
                CSqGSIb3DQEBCwUAA4ICAQB6luyggsEhNYQywZAyC9BRvLSIpb3kGK1rgUcd4Jec
                JO9L0PE8raCveRTf3NgVpvwgZqsAoWG2IAGKkdFGwgi4DJmdMDaO5YMRtK8qgDKt
                SZfYI0TZCOrrFAPeET6Vy0tB/SelSbhW6jM0VJM1hnNo0WfLFJz8W2PjpgV8ikUj
                hKd+sg+Qb6xOZw70vQkp/RauBbs2GSqOQXBFRvUyMD5Xjb5alK2rwXCUsk/IMDHc
                y0OjsCgJIKpaQPME7pIyw6CVDxaeOZE/XuYw9LlFovcOUkhGBfzLBonGnCFsr9jF
                ElKlpT2fSoOWd25tLaTXY1hoPzknoRF1d8q1tlqp6khe9vmzfRY0RRHDAE/5L+6L
                3P/Kfr76Sn03YZYw9g/4YMdWvwScO2Ibt/nU4+din9vw5tpcUtYiCnQoS92L7EGp
                lMgtwEDXPBmDsFl2m4zSytljTx4jrxHO8LKu0ZYsTKBGKF5/2nr074x14uNYVMrC
                xy9+XWcC3yc9jh9n01U4XE/XE0xrAZGC4Rp/OUsTPuPnq+LfvIXcIDj/WYG8R7Yr
                xS8PkFntbPCaQFoTzqZ4dtFk+snd6+lrWWs5uAUnSYgMf0r3axAwwswwB4/gXPur
                BzBv00+GdMolSAOs9lgTAaDwafkp7v2nNX0FAunWwNR7j2jbHPAl+8D4HCLcjNO2
                Dg==
                -----END CERTIFICATE-----

                """;

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                new ByteArrayInputStream(pem.getBytes(StandardCharsets.UTF_8)));

        assertEquals(1, certificates.size());
    }

    @Test
    public void testLoadMultiplePEMCerts()
    throws Exception
    {
        String cert = """
                -----BEGIN CERTIFICATE-----
                MIIFnTCCA4WgAwIBAgIQAU3ninOCS2ONavh0497qEjANBgkqhkiG9w0BAQsFADBe
                MQswCQYDVQQGEwJOTDELMAkGA1UECAwCTkgxEjAQBgNVBAcMCUFtc3RlcmRhbTEu
                MCwGA1UEAwwlQ2lwaGVyTWFpbCBsaWNlbnNpbmcgcm9vdCBjZXJ0aWZpY2F0ZTAe
                Fw0xNTA2MTIxMTMxNTlaFw0zNTA2MTIxMTMxNTlaMF4xCzAJBgNVBAYTAk5MMQsw
                CQYDVQQIDAJOSDESMBAGA1UEBwwJQW1zdGVyZGFtMS4wLAYDVQQDDCVDaXBoZXJN
                YWlsIGxpY2Vuc2luZyByb290IGNlcnRpZmljYXRlMIICIjANBgkqhkiG9w0BAQEF
                AAOCAg8AMIICCgKCAgEAqV/iuV6dRUyck5hUePp9v0SsmO7G/RSCT87S6BykOrsO
                VZScamnJDy274QwUhPgh10QDeRgchNsSTjQccL8X/kVrXwcANENyl99s3PUjNTiY
                kP92oFYK5O7C2JNmCekaErsdRymKiKo8kb3MARlfv6e2IbwMQhCaQoeBqJW3dNx+
                lYw5dlULPp6UtoQl+JgSuphx6944CG32WOLdMYF/J9mUM1RrQKZkTP0cARXxnwwk
                kAvH4ye9frQc6qBpTQW6r7oQ66W77tlOho/KlEHdNe3lG+g7xo3scOIsXuBNTvee
                TiMZnNVHV7T+p4X4hZL3grAl57EXKKnOOQXwSf3FLNMJ2sKphu/Tm2xi1csZpnge
                axtbl/UMqnEOc3WTJbN9gD1KeQiJ4cfO9jmKnr39QxGSfgWvsDRqEVh62k+7TM4B
                GXpXtuS/gk6vzyGlcVAyanIWhibfsDsXQTPFjhjJqno1gxFvI1R/bWp0+WYw+LZg
                hZbWLEpIxhx6QTIWpTE60x+MEnv1j22kUZ0mj7K/JSmW0zriwA8CjDwmGp8OOiC/
                8qs+nTx6mwZWD6mrBgQPRLw57fnY3ok9gzzxSvTgmGQRIOyGQ79muoDyFTuJbU/7
                TFH2HZDP+Xt+6xHjzz+ECGxeUvDga0u3mbFUTn9DVM45a/ADrkILwmOfSflLhkMC
                AwEAAaNXMFUwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwEwYDVR0l
                BAwwCgYIKwYBBQUHAwMwHQYDVR0OBBYEFHMbqoKyKrhg++J9Dc3sgcybW+wPMA0G
                CSqGSIb3DQEBCwUAA4ICAQB6luyggsEhNYQywZAyC9BRvLSIpb3kGK1rgUcd4Jec
                JO9L0PE8raCveRTf3NgVpvwgZqsAoWG2IAGKkdFGwgi4DJmdMDaO5YMRtK8qgDKt
                SZfYI0TZCOrrFAPeET6Vy0tB/SelSbhW6jM0VJM1hnNo0WfLFJz8W2PjpgV8ikUj
                hKd+sg+Qb6xOZw70vQkp/RauBbs2GSqOQXBFRvUyMD5Xjb5alK2rwXCUsk/IMDHc
                y0OjsCgJIKpaQPME7pIyw6CVDxaeOZE/XuYw9LlFovcOUkhGBfzLBonGnCFsr9jF
                ElKlpT2fSoOWd25tLaTXY1hoPzknoRF1d8q1tlqp6khe9vmzfRY0RRHDAE/5L+6L
                3P/Kfr76Sn03YZYw9g/4YMdWvwScO2Ibt/nU4+din9vw5tpcUtYiCnQoS92L7EGp
                lMgtwEDXPBmDsFl2m4zSytljTx4jrxHO8LKu0ZYsTKBGKF5/2nr074x14uNYVMrC
                xy9+XWcC3yc9jh9n01U4XE/XE0xrAZGC4Rp/OUsTPuPnq+LfvIXcIDj/WYG8R7Yr
                xS8PkFntbPCaQFoTzqZ4dtFk+snd6+lrWWs5uAUnSYgMf0r3axAwwswwB4/gXPur
                BzBv00+GdMolSAOs9lgTAaDwafkp7v2nNX0FAunWwNR7j2jbHPAl+8D4HCLcjNO2
                Dg==
                -----END CERTIFICATE-----""";

        StrBuilder sb = new StrBuilder();

        sb.append("random text")
            .appendNewLine()
            .appendNewLine()
            .appendln(cert)
            .append("random text")
            .appendNewLine()
            .appendNewLine()
            .appendln(cert)
            .appendNewLine()
            .append("random text")
            .appendNewLine();

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8)));

        assertEquals(2, certificates.size());
    }

    @Test
    public void testInvalidPEM()
    throws Exception
    {
        String pem = "some random string";

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                new ByteArrayInputStream(pem.getBytes(StandardCharsets.UTF_8)));

        assertEquals(0, certificates.size());
    }
}
