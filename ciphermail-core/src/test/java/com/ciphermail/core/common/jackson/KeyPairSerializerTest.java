/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.jackson;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import org.junit.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class KeyPairSerializerTest
{
    private final static String ENCODED_KEYPAIR =
        "{\"publicKey\":\"eyJyYXdLZXkiOiJNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQ" +
        "W0wNmowSC9ZUElvankwYW1pWFNHSnlMRmlkUkhyN3pwU1dUdW5kYUozaDhCbThoNUhVYVFBeGVWQWZFNFBhRW5KMmU" +
        "rRWlXdlRKd3p0aXJkbTNwR09FeFM0a1hQRXZIMUplS2VLZW5tdDdoVlFPQ1YxL2ZYaUR1RXpWQlNLYjc3Q0hlVU10Q" +
        "mpwMjlHV2UyRGhzb0xZZjdZWTdFV0tIQnNmN29MQyt0RnNaTWxqMEtRUWxjbWo2a1duMXJMUGJ5dU9weGs2WnNsbjA" +
        "0K0VKWDgwY1JuWWo2RDJXYW1kQWU5S2xuUDhXQ2JRbDNWZlFFbDMyd0lZL0djRnZWczFhWm9EcGpveW5IMUFzRkY0S" +
        "TlhS085VWtiY3EvdGpVM0Q4MU9jeitLcVRPMkVvY0tVZUM1SUZieURGU3hNRXEwakM1L0J5WENZSWdkaXJuMDNObk5" +
        "VMUFwd0lEQVFBQiIsImFsZ29yaXRobSI6IlJTQSIsImZvcm1hdCI6IlguNTA5Iiwia2V5VHlwZSI6IlBVQkxJQyIsI" +
        "nByb3RlY3Rpb24iOiJOT05FIn0=\",\"privateKey\":\"eyJyYXdLZXkiOiJNSUlFdmdJQkFEQU5CZ2txaGtpRzl" +
        "3MEJBUUVGQUFTQ0JLZ3dnZ1NrQWdFQUFvSUJBUUNiVHFQUWY5ZzhpaVBMUnFhSmRJWW5Jc1dKMUVldnZPbEpaTzZkM" +
        "W9uZUh3R2J5SGtkUnBBREY1VUI4VGc5b1Njblo3NFNKYTlNbkRPMkt0MmJla1k0VEZMaVJjOFM4ZlVsNHA0cDZlYTN" +
        "1RlZBNEpYWDk5ZUlPNFROVUZJcHZ2c0lkNVF5MEdPbmIwWlo3WU9HeWd0aC90aGpzUllvY0d4L3Vnc0w2MFd4a3lXU" +
        "FFwQkNWeWFQcVJhZldzczl2SzQ2bkdUcG15V2ZUajRRbGZ6UnhHZGlQb1BaWnFaMEI3MHFXYy94WUp0Q1hkVjlBU1h" +
        "mYkFoajhad1c5V3pWcG1nT21PaktjZlVDd1VYZ2oxb283MVNSdHlyKzJOVGNQelU1elA0cXBNN1lTaHdwUjRMa2dWd" +
        "klNVkxFd1NyU01MbjhISmNKZ2lCMkt1ZlRjMmMxVFVDbkFnTUJBQUVDZ2dFQUt0K3V3V1JKb1l6UkRORFlNS0lHVnM" +
        "5MmtQSVZTQ0p5bEZ4VXN5WUNzVnNGNnFlUko4L08rY3VNeXhoNFdpc2V6L1lIdHFMSW5mR1JKUHo0YUc5ODZ4WEcyO" +
        "GhCdmcwbGkrdmZsZHFJWXJkMGNaQ09UU3lTUjMyR3o5Y1lsNFd0YjB6emtJT092eTRzMGE2TDNLM3lMbjlYNVUyQnZ" +
        "IQXN3NlhzWHNINFgyaTlPdEkrRGRiSWdpMmlkUjZydzR6bER5ZTMrVUJoRmhIMU1rU1lRdk9CMVpRUEJaQjlrNVBNS" +
        "1hhMTVVaFA5enJtVVBHcWRqNVFPcjkxL2wzMzIzT2JjU0pFVE9lUVMyNnBKUDBvRFZxaXpCVCtFSkVsdmFWS3RDNUl" +
        "ZQ0E0R2RXUlh6VG1GVTRwZXNRRkEzMU9sdFR0V2d4MVk2bTdWVXZDV21za0xvdXRZUUtCZ1FEWk83NFNmVzFQenNuV" +
        "TdXWTdqdG9TNndWdWorQk9rREpybEdXT0dnN1J3bUlZVXcwQTVDZkJKOVZrTjBZckYwTXgydkw5RkYvMEZudVlEeFI" +
        "1N21tcFdURmxDU3lsa055M25GZ3RIQlE0aUtJNE02dTlqSlZaODE0dFMzMGJUQnV4Q0M5NnUraXl6MC9PTjZZWHl6c" +
        "W5yM1JNOG9vVGJIeWZwZFdnemRMWHF3S0JnUUMzQmRCU2hxUmJvcnNVbWxaYVZidEM1dEFWYVBNOG51S2NRbTZvQUx" +
        "uZit5RWZtZzNDUWMzOVN6VUZTQ01VaWtPWWJiTndSNXlvQXpUcWJBRUNaRGQvTVlETFhYelJUSEJLdjROMUgySHRlT" +
        "jU0SHNqdkI0TkFLOStxNm5keWJkSUFwNHJhcll1YVRMMkdCNXh1Zlh6VklUaEphS2VVbTQwQm45QXJJS3lPOVFLQmd" +
        "RQ3Fnb001M2l1Y0ZoTkVSNmNVSHBlN2U3QUN6WG5pRUxzU3lDTkNlOStmODdTdHBScXkxV01DOFBKRlBFR2VkeDd1R" +
        "mc3ZWFVeTd1WjMvaCs2cG1tSmpZN2dxdHgzTlF6eUwvbk1UelZOdDRJbDhGY3E2K0lzT1ZNRmFTa09EMXFSeHU1d1E" +
        "rVWU0ZXBNSXFaTjhYNi9GaVNsL2NHekN2RWZCeEJnL3BPN1hDd0tCZ0RnbGYzQWh3QUFnbnZYR0Z5R2xWVXBEMXFnS" +
        "3h6dmNualpEcHdEQklFU1hIT1U5OUdTWW5XSE1GWnJtTTJUVXVjSGtQWHovYXc2YjFUcnBJVElqbm1iVnYrWWphdXd" +
        "tWC9oK1lneW5NUHFSN3VXNHcvbVlPMnFQU3hGNjFtUHpUczVHbDdGWU5oQ0M1RTFldmwyaS80MitvQzdjL1RQNHVEd" +
        "zhVN2MzVWRpVkFvR0JBTFM3Unh0UkxnUWN2ZGxESFk2bEE3aXNKVVZwUGVKbU1xb1JLV0NpeGZYbGxpdllVZkJtY25" +
        "YLzl2Mm5ZL2swS2FrTUpJcVZ4eU9tQ3RWNXlPTVZRY01QbkFHK2RUZVhvU2NJWTRVVmNxTjN1RFlGUnhCdi9JZUpBQ" +
        "XdEVG5JSEZVMkRya1Z3S0swa1dldGZSbDdGMTZud21qU2owcHFNNDFkWUFPNGpvaVVVIiwiYWxnb3JpdGhtIjoiUlN" +
        "BIiwiZm9ybWF0IjoiUEtDUyM4Iiwia2V5VHlwZSI6IlBSSVZBVEUiLCJwcm90ZWN0aW9uIjoiTk9ORSJ9\"}";

    private static KeyPair generateKeyPair()
    throws Exception
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        SecureRandom randomSource = securityFactory.createSecureRandom();

        KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

        keyPairGenerator.initialize(2048, randomSource);

        return keyPairGenerator.generateKeyPair();
    }

    @Test
    public void serialize()
    throws Exception
    {
        KeyPair keyPair = generateKeyPair();

        String json = JacksonUtil.getObjectMapper().writeValueAsString(keyPair);

        KeyPair deserialized = JacksonUtil.getObjectMapper().readValue(json, KeyPair.class);

        assertEquals(keyPair.getPublic(), deserialized.getPublic());
        assertEquals(keyPair.getPrivate(), deserialized.getPrivate());
    }

    @Test
    public void serializePublicKeyOnly()
    throws Exception
    {
        KeyPair keyPair = new KeyPair(generateKeyPair().getPublic(), null);

        String json = JacksonUtil.getObjectMapper().writeValueAsString(keyPair);

        KeyPair deserialized = JacksonUtil.getObjectMapper().readValue(json, KeyPair.class);

        assertEquals(keyPair.getPublic(), deserialized.getPublic());
        assertNull(keyPair.getPrivate());
    }

    @Test
    public void serializePrivateKeyOnly()
    throws Exception
    {
        KeyPair keyPair = new KeyPair(null, generateKeyPair().getPrivate());

        String json = JacksonUtil.getObjectMapper().writeValueAsString(keyPair);

        KeyPair deserialized = JacksonUtil.getObjectMapper().readValue(json, KeyPair.class);

        assertEquals(keyPair.getPrivate(), deserialized.getPrivate());
        assertNull(keyPair.getPublic());
    }

    @Test
    public void serializeEmptyKeyPair()
    throws Exception
    {
        KeyPair keyPair = new KeyPair(null, null);

        String json = JacksonUtil.getObjectMapper().writeValueAsString(keyPair);

        KeyPair deserialized = JacksonUtil.getObjectMapper().readValue(json, KeyPair.class);

        assertNull(deserialized.getPublic());
        assertNull(deserialized.getPrivate());
    }

    /*
     * Deserialize a static value making sure old JSON will continue to work
     */
    @Test
    public void deserialize()
    throws Exception
    {
        KeyPair deserialized = JacksonUtil.getObjectMapper().readValue(ENCODED_KEYPAIR, KeyPair.class);

        assertNotNull(deserialized.getPublic());
        assertNotNull(deserialized.getPrivate());
    }
}