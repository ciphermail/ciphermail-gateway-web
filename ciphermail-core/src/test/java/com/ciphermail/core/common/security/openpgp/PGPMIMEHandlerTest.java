/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPMIMEHandlerTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private PGPKeyPairProvider createPGPKeyPairProvider(String secretKeyFile, String password)
    throws Exception
    {
        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, secretKeyFile))));

        return new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection, new StaticPasswordProvider(password));
    }

    private void decryptGPG(String password, File file, StringBuilder output, StringBuilder error)
    throws Exception
    {
        GPG gpg = new GPG();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

        TeeOutputStream outputTee = new TeeOutputStream(outputStream, System.out);
        TeeOutputStream errorTee = new TeeOutputStream(errorStream, System.err);

        try {
            assertTrue(gpg.decrypt(file, password, outputTee, errorTee));
        }
        finally
        {
            if (output != null) {
                output.append(MiscStringUtils.toStringFromASCIIBytes(outputStream.toByteArray()));
            }

            if (error != null) {
                error.append(MiscStringUtils.toStringFromASCIIBytes(errorStream.toByteArray()));
            }
        }
    }

    @Test
    public void testInlineEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        // Check if all headers are the same
        assertEquals(StringUtils.join(IteratorUtils.asIterator(message.getAllHeaderLines()), ","),
                StringUtils.join(IteratorUtils.asIterator(handled.getAllHeaderLines()), ","));

        // Check order of received headers
        String[] received = handled.getHeader("Received");
        assertEquals("from lists.djigzo.com (unknown [10.0.1.5])", received[0]);
        assertEquals("from lists.djigzo.com (localhost [127.0.0.1])", received[1]);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testInlineEncryptedInlineNotEnabled()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setInlinePGPEnabled(false);

        assertNull(handler.handleMessage(message));
    }

    @Test
    public void testInlineEncryptedNoDecryptionKey()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com-expiration-2030-12-31.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        assertNotNull(handled);
        assertNotSame(handled, message);

        MailUtils.validateMessage(handled);

        assertFalse(handler.isChanged());
        assertFalse(handler.isMixedContent());

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("-----BEGIN PGP MESSAGE-----"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf.pgp", pdf.getFileName());
    }

    @Test
    public void testInlineEncryptedExceedMaxBytes()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setDecompressionUpperlimit(5);

        MimeMessage handled = handler.handleMessage(message);

        // Message could not be decrypted because the decompression upper limit was exceeded. But since inline PGP
        // was enabled, a cloned message is returned
        assertNotNull(handled);
        assertNotSame(handled, message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("-----BEGIN PGP MESSAGE-----"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf.pgp", pdf.getFileName());

        handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        // The body should now be decrypted but not the attachment
        handler.setDecompressionUpperlimit(1000);

        handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf.pgp", pdf.getFileName());
    }

    @Test
    public void testInlineEncryptedExceedMaxObjects()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setMaxObjects(1);

        MimeMessage handled = handler.handleMessage(message);

        // Message could not be decrypted because the max nr of pgp objects was reached. But since inline PGP
        // was enabled, a cloned message is returned
        assertNotNull(handled);
        assertNotSame(handled, message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("-----BEGIN PGP MESSAGE-----"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf.pgp", pdf.getFileName());
    }

    @Test
    public void testInlineSignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-signed-and-encrypted.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        // Check if all headers are the same
        assertEquals(StringUtils.join(IteratorUtils.asIterator(message.getAllHeaderLines()), ","),
                StringUtils.join(IteratorUtils.asIterator(handled.getAllHeaderLines()), ","));

        // Check order of received headers
        String[] received = handled.getHeader("Received");
        assertEquals("from lists.djigzo.com (unknown [10.0.1.5])", received[0]);
        assertEquals("from lists.djigzo.com (localhost [127.0.0.1])", received[1]);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf", pdf.getFileName());
        assertEquals("attachment", pdf.getDisposition());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testPGPMIMESignAndEncryped()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-sign&encrypt.eml"));

        assertEquals("<123>", message.getMessageID());

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("<123>", handled.getMessageID());

        // Check order of received headers
        String[] received = handled.getHeader("Received");
        assertEquals("from lists.djigzo.com (unknown [10.0.1.5])", received[0]);
        assertEquals("from lists.djigzo.com (localhost [127.0.0.1])", received[1]);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIMESignAndEncrypedNotRetainMessageID()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-sign&encrypt.eml"));

        assertEquals("<123>", message.getMessageID());

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setRetainMessageID(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertNotEquals("<123>", handled.getMessageID());
    }

    @Test
    public void testPGPMIMESignAndEncrypedNoDecryptionKey()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-sign&encrypt.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com-expiration-2030-12-31.gpg.key", "test"));


        MimeMessage handled = handler.handleMessage(message);

        // The message could not be decrypted
        assertNull(handled);
    }

    @Test
    public void testPGPMIMESignedKeepSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setRemoveSignature(false);

        MimeMessage handled = handler.handleMessage(message);

        assertNotNull(handled);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/signed; micalg=pgp-sha1;\r\n protocol=\"application/pgp-signature\";\r\n " +
        		"boundary=\"TfQn8OqbXQB93hsKCF25Rupb9nLKPgaQB\"", handled.getContentType());

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        body = mp.getBodyPart(1);

        assertEquals("application/pgp-signature; name=\"signature.asc\"", body.getContentType());

        String sig = IOUtils.toString(body.getInputStream(), StandardCharsets.UTF_8);

        assertTrue(sig.contains("-----END PGP SIGNATURE-----"));

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have a signed/literal layer
        assertEquals(1, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(0);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        assertNotNull(literalLayer.getPGPLiteralData());

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIMESignedRemoveSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        assertTrue(message.isMimeType("multipart/signed"));

        assertEquals("abcd\r\n  efgh\r\n  ijkl", StringUtils.join(message.getHeader("X-Folding-test")));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        // The message was signed but signature is removed
        assertFalse(handled.isMimeType("multipart/signed"));
        assertTrue(handled.isMimeType("text/plain"));

        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("abcd\r\n  efgh\r\n  ijkl", StringUtils.join(handled.getHeader("X-Folding-test")));

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have a signed/literal layer
        assertEquals(1, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(0);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        assertNotNull(literalLayer.getPGPLiteralData());

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    /*
     * PGP-Inline-sign&encrypt-HTML-text.eml has a PGP header that ends with a space
     *
     * HTML scanning disabled
     */
    @Test
    public void testPGPInlineSignedAndEncrypedHTMLTextHTMLScanningDisabled()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-Inline-sign&encrypt-HTML-text.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setScanHTMLForPGP(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String) body.getContent()).contains("test"));

        BodyPart htmlPart = mp.getBodyPart(1);

        assertEquals("text/html;\r\n\tcharset=\"us-ascii\"", htmlPart.getContentType());
    }

    /*
     * PGP-Inline-sign&encrypt-HTML-text.eml has a PGP header that ends with a space
     *
     * Now with HTML scanning enabled
     */
    @Test
    public void testPGPInlineSignedAndEncrypedHTMLText()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-Inline-sign&encrypt-HTML-text.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String) body.getContent()).contains("test"));

        BodyPart htmlPart = mp.getBodyPart(1);

        assertEquals("text/plain; charset=ISO-8859-1", htmlPart.getContentType());
    }

    @Test
    public void testFilenameFromLiteralPacket()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed-and-encrypted-filename-different.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setSkipNonPGPExtensions(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf", pdf.getFileName());
        assertEquals("attachment", pdf.getDisposition());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testNoContentDisposition()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed-and-encrypted-no-content-disposition.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setSkipNonPGPExtensions(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdf = mp.getBodyPart(1);

        assertEquals("djigzo-whitepaper.pdf", pdf.getFileName());
        assertNull(pdf.getDisposition());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdf.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testPGPInlineSignedAndEncrypedUnicodeUTF8WithCharsetArmor()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-encrypted-unicode-charset-armor-UTF8.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Τη γλώσσα μου έδωσαν ελληνική"));
    }

    @Test
    public void testPGPInlineSignedAndEncrypedUnicodeUTF8NoCharsetArmor()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-encrypted-unicode-no-charset-armor-UTF8.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("Τη γλώσσα μου έδωσαν ελληνική"));
    }

    @Test
    public void testPGPInlineSignedAndEncryped_Turkish_ISO_8859_9WithCharsetArmor()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-encrypted-turkish-charset-armor-ISO-8859-9.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-9", handled.getContentType());
        assertTrue(((String) handled.getContent()).contains("turkish characters (ş, ğ)"));
    }

    @Test
    public void testPGPInlineSignedAndEncryped_Turkish_ISO_8859_9NoCharsetArmor()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-encrypted-turkish-no-charset-armor-ISO-8859-9.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-9", handled.getContentType());
        assertTrue(((String) handled.getContent()).contains("turkish characters (ş, ğ)"));
    }

    /*
     * The illegal charset will fallback to US-ASCCII. The content will therefore not be valid
     */
    @Test
    public void testPGPInlineSignedAndEncrypedInvalidCharsetArmor()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-encrypted-unicode-invalid-charset-armor.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/mixed; \r\n\tboundary=\"----=_Part_0_1201848792.1395916727786\"",
                handled.getContentType());
    }

    /*
     * The illegal charset will fallback to US-ASCCII. The content will therefore not be valid
     */
    @Test
    public void testPGPInlineSignedAndEncrypedInvalidCharset()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-invalid-charset.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/mixed; \r\n\tboundary=\"----=_Part_0_1201848792.1395916727786\"",
                handled.getContentType());
    }

    @Test
    public void testInlineHTMLSymantecEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-symantec-encryption-desktop.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handled.isMimeType("multipart/mixed"));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart body = outerMP.getBodyPart(0);

        assertTrue(body.isMimeType("multipart/alternative"));

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only
        Multipart alternativeMP = (Multipart) body.getContent();

        assertEquals(2, alternativeMP.getCount());

        body = alternativeMP.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertEquals("Test red",  ((String) body.getContent()).trim());

        // The other (alternative part which was originally HTML) is similar
        body = alternativeMP.getBodyPart(1);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertEquals("Test red",  ((String) body.getContent()).trim());

        // And then we we have an HTML part (was PGP partitioned)
        body = outerMP.getBodyPart(1);

        assertEquals("application/octet-stream; name=PGPexch.htm", body.getContentType());
        assertTrue(IOUtils.toString(body.getInputStream(), StandardCharsets.UTF_8).contains(
                "<p class=MsoNormal>Test <span style='color:red'>red</span><o:p></o:p></p>"));
    }

    /*
     * Message which was sent with PGP desktop as text only. However, when used in combination with Exchange, a
     * HTML part containing encrypted text will always be added
     */
    @Test
    public void testInlineSentTextOnlyYetHTMLSymantecEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/sent-text-only-yes-html-symantec-encryption-desktop.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handled.isMimeType("multipart/alternative"));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only
        BodyPart body = outerMP.getBodyPart(0);

        body = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertEquals("test",  ((String) body.getContent()).trim());

        // The other (alternative part which was originally HTML) is similar
        body = outerMP.getBodyPart(1);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertEquals("test",  ((String) body.getContent()).trim());
    }

    /*
     * Message which was sent with Enigmail with HTML enabled. This results in an email with HTML alt part which
     * actually contains text only encrypted PGP but now embedded in HTML
     */
    @Test
    public void testInlineHTMLEnigmailEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/html-encryption-enigmail.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handled.isMimeType("multipart/alternative"));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only
        BodyPart body = outerMP.getBodyPart(0);

        body = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("test red"));

        // The other (alternative part which was originally HTML) is similar
        body = outerMP.getBodyPart(1);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("test red"));
    }

    /*
     * Message which was sent with Enigmail with HTML enabled. This results in an email with HTML alt part which
     * actually contains text only encrypted PGP but now embedded in HTML
     *
     * This time, scan for HTML is disabled
     */
    @Test
    public void testInlineHTMLEnigmailEncryptedNoScanHTML()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/html-encryption-enigmail.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        handler.setScanHTMLForPGP(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handled.isMimeType("multipart/alternative"));

        Multipart outerMP = (Multipart) handled.getContent();

        assertEquals(2, outerMP.getCount());

        // After decryption of text and html part, both parts of the alternative part are text parts because the
        // html part actually contains text only

        BodyPart body = outerMP.getBodyPart(0);

        body = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("test red"));

        // The other part should no contain HTML with encrypted PGP content since HTML scanning was disabled
        body = outerMP.getBodyPart(1);

        assertEquals("text/html; charset=utf-8", body.getContentType());
        assertTrue(((String) body.getContent()).contains("-----BEGIN PGP MESSAGE-----"));
    }

    /*
     * Test what happens on a file with HTML without PGP content
     */
    @Test
    public void testHTMLAltWithNoPGP()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/html-alternative.eml"));

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/test@example.com.gpg.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handled.isMimeType("multipart/alternative"));

        // PGPMIMEHandler clones the message before inline
        assertNotSame(message, handled);

        // Check if MIME content is not changed
        ByteArrayOutputStream mime1 = new ByteArrayOutputStream();
        message.writeTo(mime1);

        ByteArrayOutputStream mime2 = new ByteArrayOutputStream();
        handled.writeTo(mime2);

        assertEquals(mime1.toString(), mime2.toString());
    }

    @Test
    public void testPGPMIME_NIST_P_256_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/nist-p-256@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid AF8B30CEB2B736DE"));
        assertTrue(error.contains(":signature packet: algo 19, keyid E5AB02E8B807FE20"));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm nistp256"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/nist-p-256@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/nist-p-256@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("E5AB02E8B807FE20", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_NIST_P_384_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/nist-p-384@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid EFD63D644076AA44"));
        assertTrue(error.contains(":signature packet: algo 19, keyid 710125A5A5E55957"));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm nistp384"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/nist-p-384@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/nist-p-384@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("710125A5A5E55957", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_NIST_P_521_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/nist-p-521@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid 5C93EB074ECFED6E"));
        assertTrue(error.contains(":signature packet: algo 19, keyid 39C26CEC5BEFE0CD"));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm nistp521"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/nist-p-521@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/nist-p-521@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("39C26CEC5BEFE0CD", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_Brainpool_P_256_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/brainpool-p-256@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid CA9DD1D7A12E2513"));
        assertTrue(error.contains(":signature packet: algo 19, keyid C33A4DA311820160"));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm brainpoolP256r1"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/brainpool-p-256@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/brainpool-p-256@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("C33A4DA311820160", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_Brainpool_P_384_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/brainpool-p-384@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid B4338BDA88A2BA0E"));
        assertTrue(error.contains(":signature packet: algo 19, keyid 1E64823DF04CCB65"));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm brainpoolP384r1"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/brainpool-p-384@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/brainpool-p-384@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("1E64823DF04CCB65", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_Brainpool_P_512_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/brainpool-p-512@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid 13635FC461FAB8E4"));
        assertTrue(error.contains(":signature packet: algo 19, keyid F969CD61623B6BFC"));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm brainpoolP512r1"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/brainpool-p-512@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/brainpool-p-512@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("F969CD61623B6BFC", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }

    @Test
    public void testPGPMIME_Brainpool_secp256k1_SignAndEncryped()
    throws Exception
    {
        File mimeFile = new File(TEST_BASE, "mail/secp256k1@example.com-signed-encrypted.eml");

        StringBuilder errorBuilder = new StringBuilder();

        // Check if gpg can decrypt it
        decryptGPG("test", mimeFile, null, errorBuilder);

        String error = errorBuilder.toString();

        assertTrue(error.contains("pubkey enc packet: version 3, algo 18, keyid ECBCD7754520A2C0"));
        assertTrue(error.contains(":signature packet: algo 19, keyid 6BBAEB73B8C5B26C"));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm secp256k1"));

        MimeMessage message = MailUtils.loadMessage(mimeFile);

        PGPMIMEHandler handler = new PGPMIMEHandler(createPGPKeyPairProvider(
                "pgp/secp256k1@example.com.key", "test"));

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        List<PGPHandler> handlers = handler.getHandlers().get(0);

        assertEquals(1, handlers.size());

        List<PGPLayer> layers = handlers.get(0).getPGPLayers();

        // The message should have an encrypted, compressed and signed/literal data layer
        assertEquals(3, layers.size());

        // signature/literal layer
        PGPLayer layer = layers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/secp256k1@example.com.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("6BBAEB73B8C5B26C", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }
}
