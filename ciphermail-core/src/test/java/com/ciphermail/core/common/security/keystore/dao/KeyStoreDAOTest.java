/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.dao;

import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.keystore.hibernate.KeyStoreEntity;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class KeyStoreDAOTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        // Delete existing entries
        transactionOperations.executeWithoutResult(status ->
        {
            GenericHibernateDAO dao = GenericHibernateDAO.createInstance(SessionAdapterFactory.create(
                    sessionManager.getSession()));

            for (KeyStoreEntity keyEntry : dao.findAll(KeyStoreEntity.class)) {
                dao.delete(keyEntry);
            }
        });
    }

    private KeyStoreDAO getDAO(String storeName) {
        return new KeyStoreDAO(storeName, sessionManager.getSession());
    }

    private UUID insertKeyStoreEntry(KeyStoreDAO dao, String alias)
    {
        try {
            File file = new File(TEST_BASE, "certificates/testcertificate.cer");

            X509Certificate certificate = TestUtils.loadCertificate(file);

            file = new File(TEST_BASE, "certificates/rim.cer");

            X509Certificate otherCertificate = TestUtils.loadCertificate(file);

            Certificate[] chain = new Certificate[] {certificate, otherCertificate};

            KeyStoreEntity entry = new KeyStoreEntity(dao.getStoreName(), certificate, chain,
                    null, alias, new Date());

            dao.persist(entry);

            return (UUID) sessionManager.getSession().getIdentifier(entry);
        }
        catch (Exception e) {
            throw new UnhandledException(e);
        }
    }

    private void insertKeyStoreEntry(KeyStoreDAO dao, String alias, X509Certificate certificate)
    {
        try {
            Certificate[] chain = new Certificate[] {certificate};

            KeyStoreEntity entry = new KeyStoreEntity(dao.getStoreName(), certificate, chain,
                    null, alias, new Date());

            dao.persist(entry);
        }
        catch (Exception e) {
            throw new UnhandledException(e);
        }
    }

    @Test
    public void testAliasDulication()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            KeyStoreDAO dao = getDAO("store");

            try {
                insertKeyStoreEntry(dao, "alias");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        try {
            transactionOperations.executeWithoutResult(status ->
            {
                KeyStoreDAO dao = getDAO("store");

                try {
                    insertKeyStoreEntry(dao, "alias");
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch(Exception e) {
            assertTrue(ExceptionUtils.indexOfThrowable(e, ConstraintViolationException.class) >= 0);
        }
    }

    @Test
    public void testLoadById()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            KeyStoreDAO dao = getDAO("store");
            KeyStoreDAO otherDAO = getDAO("other-store");

            insertKeyStoreEntry(dao, "alias");
            insertKeyStoreEntry(otherDAO, "alias");
            UUID testID = insertKeyStoreEntry(dao, "alias3");

            KeyStoreEntity entry = dao.findById(testID, KeyStoreEntity.class);

            assertEquals("alias3", entry.getKeyAlias());

            assertTrue(entry.getCertificate() instanceof X509Certificate);

            // test whether the certificate is not created using the default provider but using the
            // bc provider. The BC need to be installed and used for this check to work
            assertEquals("org.bouncycastle.jcajce.provider.asymmetric.x509.X509CertificateObject",
                    entry.getCertificate().getClass().getName());

            assertEquals(2, entry.getChain().length);

            for (Certificate certificate : entry.getChain())
            {
                // test whether the certificate is not created using the default provider but using the
                // bc provider. The BC need to be installed and used for this check to work
                assertEquals("org.bouncycastle.jcajce.provider.asymmetric.x509.X509CertificateObject",
                        certificate.getClass().getName());
            }
        });
    }

    @Test
    public void testGetAliases()
    {
        transactionOperations.executeWithoutResult(status ->
        {

            KeyStoreDAO dao = getDAO("store");
            KeyStoreDAO otherDAO = getDAO("other-store");

            insertKeyStoreEntry(dao, "alias");
            insertKeyStoreEntry(dao, "alias2");
            insertKeyStoreEntry(otherDAO, "alias2");

            List<String> aliases = dao.getAliases();

            assertEquals(2, aliases.size());
            assertTrue(aliases.contains("alias"));
            assertTrue(aliases.contains("alias2"));

            aliases = otherDAO.getAliases();
            assertEquals(1, aliases.size());
            assertTrue(aliases.contains("alias2"));
        });
    }

    @Test
    public void testGetByAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            KeyStoreDAO dao = getDAO("store");
            KeyStoreDAO otherDAO = getDAO("other-store");

            insertKeyStoreEntry(dao, "alias");
            insertKeyStoreEntry(dao, "alias2");
            insertKeyStoreEntry(otherDAO, "alias2");

            KeyStoreEntity entry;

            entry = dao.getEntryByAlias("alias");
            assertNotNull(entry);
            assertEquals("alias", entry.getKeyAlias());

            entry = otherDAO.getEntryByAlias("alias");
            assertNull(entry);

            entry = otherDAO.getEntryByAlias("alias2");
            assertNotNull(entry);
            assertEquals("alias2", entry.getKeyAlias());
        });
    }

    @Test
    public void testGetByCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStoreDAO dao = getDAO("store");
                KeyStoreDAO otherDAO = getDAO("other-store");

                insertKeyStoreEntry(dao, "alias");
                insertKeyStoreEntry(dao, "alias2");
                insertKeyStoreEntry(otherDAO, "alias3");

                File file = new File(TEST_BASE, "certificates/testcertificate.cer");

                X509Certificate certificate = TestUtils.loadCertificate(file);

                KeyStoreEntity entry;

                entry = dao.getEntryByCertificate(certificate);

                assertNotNull(entry);

                /*
                 * NOte: there are two entries with the certificate for the store. Depending on "luck",
                 * either the first or second entry is returned
                 */
                assertTrue("alias".equals(entry.getKeyAlias()) || "alias2".equals(entry.getKeyAlias()));
                assertEquals(2, entry.getChain().length);

                entry = otherDAO.getEntryByCertificate(certificate);

                assertNotNull(entry);

                assertEquals("alias3", entry.getKeyAlias());
                assertEquals(2, entry.getChain().length);

                X509Certificate additionalCert = TestUtils.loadCertificate(new File(TEST_BASE,
                        "certificates/1000bits_public_key.cer"));

                entry = dao.getEntryByCertificate(additionalCert);

                assertNull(entry);

                insertKeyStoreEntry(dao, "alias4", additionalCert);

                entry = dao.getEntryByCertificate(additionalCert);

                assertNotNull(entry);
                assertEquals("alias4", entry.getKeyAlias());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetEntryCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            KeyStoreDAO dao = getDAO("store");
            KeyStoreDAO otherDAO = getDAO("other-store");

            assertEquals(0L, dao.getEntryCount());

            insertKeyStoreEntry(dao, "alias");
            insertKeyStoreEntry(dao, "alias2");
            insertKeyStoreEntry(otherDAO, "alias3");

            assertEquals(2L, dao.getEntryCount());
            assertEquals(1L, otherDAO.getEntryCount());
        });
    }

    @Test
    public void testGetEntryIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStoreDAO dao = getDAO("store");
                KeyStoreDAO otherDAO = getDAO("other-store");

                Set<KeyStoreEntity> entries;

                entries = new HashSet<>(CloseableIteratorUtils.toList(dao.getEntryIterator()));

                assertEquals(0, entries.size());

                insertKeyStoreEntry(dao, "alias");
                insertKeyStoreEntry(dao, "alias2");
                insertKeyStoreEntry(otherDAO, "alias3");

                entries = new HashSet<>(CloseableIteratorUtils.toList(dao.getEntryIterator()));

                assertEquals(2, entries.size());
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias", null)));
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias2", null)));
                assertFalse(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias3", null)));

                entries = new HashSet<>(CloseableIteratorUtils.toList(otherDAO.getEntryIterator()));
                assertEquals(1, entries.size());
                assertFalse(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias", null)));
                assertFalse(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias2", null)));
                assertTrue(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias3", null)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetAllEntriesIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStoreDAO dao = getDAO("store");
                KeyStoreDAO otherDAO = getDAO("other-store");

                Set<KeyStoreEntity> entries;

                entries = new HashSet<>(CloseableIteratorUtils.toList(dao.getEntryIterator()));
                assertEquals(0, entries.size());

                entries = new HashSet<>(CloseableIteratorUtils.toList(otherDAO.getEntryIterator()));
                assertEquals(0, entries.size());

                insertKeyStoreEntry(dao, "alias");
                insertKeyStoreEntry(dao, "alias2");
                insertKeyStoreEntry(otherDAO, "alias2");
                insertKeyStoreEntry(otherDAO, "alias3");

                //  #getAllEntriesIterator returns all entries for all stores
                entries = new HashSet<>(CloseableIteratorUtils.toList(KeyStoreDAO.getAllEntriesIterator(dao.getSession())));
                assertEquals(4, entries.size());
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias", null)));
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias2", null)));
                assertTrue(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias2", null)));
                assertTrue(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias3", null)));

                entries = new HashSet<>(CloseableIteratorUtils.toList(KeyStoreDAO.getAllEntriesIterator(otherDAO.getSession())));
                assertEquals(4, entries.size());
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias", null)));
                assertTrue(entries.contains(new KeyStoreEntity(dao.getStoreName(), "alias2", null)));
                assertTrue(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias2", null)));
                assertTrue(entries.contains(new KeyStoreEntity(otherDAO.getStoreName(), "alias3", null)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
