/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.locale.CharacterEncoding;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.test.TestUtils;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class BodyPartUtilsTest
{
    @Test
    public void testsearchForRFC822()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/attached-encrypted-rfc822.eml");

        message = BodyPartUtils.searchForRFC822(message);

        assertNotNull(message);

        assertTrue(message.isMimeType("application/pkcs7-mime"));
    }

    @Test
    public void testToRFC822()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "testtorfc822.eml");

        assertTrue(rfc822Part.isMimeType("message/rfc822"));

        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);

        MailUtils.validateMessage(rfc822Message);

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }

    @Test
    public void testToRFC822UnknownCharset()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-charset-xxx.eml");

        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "unknown-charset-xxx.eml");

        assertTrue(rfc822Part.isMimeType("message/rfc822"));

        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);

        MailUtils.validateMessage(rfc822Message);

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }

    @Test
    public void testToRFC822UnknownContentTransferEncoding()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-transfer-encoding.eml");

        MimeBodyPart rfc822Part = BodyPartUtils.toRFC822(message, "unknown-content-transfer-encoding.eml");

        assertTrue(rfc822Part.isMimeType("message/rfc822"));

        MimeMessage rfc822Message = BodyPartUtils.toMessage(rfc822Part);

        MailUtils.validateMessage(rfc822Message);

        assertTrue(rfc822Message.isMimeType("message/rfc822"));
    }

    @Test
    public void testExtractFromRFC822()
    throws Exception
    {
        MimeMessage rfc822Message = TestUtils.loadTestMessage("mail/rfc822.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));

        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);

        assertTrue(message.isMimeType("text/plain"));

        Object content = message.getContent();
        assertTrue(content instanceof String);
        assertEquals("test", ((String) content).trim());
    }

    @Test
    public void testExtractFromRFC822UnknownContentType()
    throws Exception
    {
        MimeMessage rfc822Message = TestUtils.loadTestMessage(
                "mail/rfc822-unknown-content-transfer-encoding-attached-message.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));

        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);

        assertTrue(message.isMimeType("text/plain"));
        assertEquals("test simple message", message.getSubject());
        assertEquals("xxx", message.getHeader("Content-Transfer-Encoding", ","));
    }

    /*
     * Note: The content type is corrupt. If -Dmail.mime.parameters.strict is set to false, the content
     * type is detected. However, calling getContent() does not return a MimeMessage
     */
    @Ignore("This test requires system property -Dmail.mime.parameters.strict=false")
    @Test
    public void testExtractFromRFC822CorruptContentType()
    throws Exception
    {
        System.setProperty("mail.mime.parameters.strict", "false");

        MimeMessage rfc822Message = TestUtils.loadTestMessage("mail/rfc822-corrupt-content-type.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));

        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);

        assertTrue(message.isMimeType("text/plain"));
        assertEquals("test simple message", message.getSubject());
    }

    @Test
    public void testExtractFromRFC822UnknownCharset()
    throws Exception
    {
        MimeMessage rfc822Message = TestUtils.loadTestMessage("mail/rfc822-unknown-charset.eml");

        assertTrue(rfc822Message.isMimeType("message/rfc822"));

        MimeMessage message = BodyPartUtils.extractFromRFC822(rfc822Message);

        assertTrue(message.isMimeType("text/plain"));
    }

    @Test
    public void testCharsetForBody()
    {
        assertEquals(CharacterEncoding.US_ASCII, BodyPartUtils.charsetForBody("abcd"));
        assertEquals(CharacterEncoding.US_ASCII, BodyPartUtils.charsetForBody(""));
        assertEquals(CharacterEncoding.US_ASCII, BodyPartUtils.charsetForBody(null));
        assertEquals(CharacterEncoding.UTF_8, BodyPartUtils.charsetForBody("Füße"));
    }

    @Test
    public void testWriteMIMEBodyPartTextOnly()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/folded-header.eml");

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        BodyPartUtils.writeMIMEBodyPart(message, new ContentHeaderNameMatcher(), output);

        output.close();

        MimeMessage result = new MimeMessage(null, new ByteArrayInputStream(output.toByteArray()));

        assertEquals(message.getContentType(), result.getContentType());
        assertEquals(message.getContent(), result.getContent());
    }

    @Test
    public void testWriteMIMEBodyPartMultipartSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        BodyPartUtils.writeMIMEBodyPart(message, new ContentHeaderNameMatcher(), output);

        output.close();

        MimeMessage result = new MimeMessage(null, new ByteArrayInputStream(output.toByteArray()));

        assertEquals(message.getContentType(), result.getContentType());
    }

    /*
     * Test which will test whether MIME is extracted if MimeMessage#getRawInputStream fails
     */
    @Test
    public void testWriteMIMEBodyPartFromMimeMessageDirect()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("someone@example.com"));

        message.saveChanges();

        ByteArrayOutputStream output = new ByteArrayOutputStream();

        BodyPartUtils.writeMIMEBodyPart(message, new ContentHeaderNameMatcher(), output);

        output.close();

        MimeMessage result = new MimeMessage(null, new ByteArrayInputStream(output.toByteArray()));

        assertEquals(message.getContentType(), result.getContentType());
        assertEquals(message.getContent(), result.getContent());
    }
}
