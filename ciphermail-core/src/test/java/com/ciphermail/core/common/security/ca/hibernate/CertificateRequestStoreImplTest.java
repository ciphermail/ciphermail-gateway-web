/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.hibernate;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CertificateRequestStoreImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private CertificateRequestStore certificateRequestStore;

    @Autowired
    private CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    private TestCertificateRequestHandler testCertificateRequestHandler;

    @Before
    public void setup()
    {
        deleteAllRequests();

        testCertificateRequestHandler = new TestCertificateRequestHandler();

        certificateRequestHandlerRegistry.registerHandler(testCertificateRequestHandler);
    }

    static class TestCertificateRequestHandler implements CertificateRequestHandler
    {
        private static final String NAME = "dummy";

        private final List<String> cleaned = new LinkedList<>();

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isInstantlyIssued() {
            return false;
        }

        @Override
        public String getCertificateHandlerName() {
            return NAME;
        }

        @Override
        public KeyAndCertificate handleRequest(CertificateRequest request)
        {
            return null;
        }

        @Override
        public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate) {
            return null;
        }

        @Override
        public void cleanup(CertificateRequest request) {
            cleaned.add(request.getEmail());
        }
    }

    private void deleteAllRequests()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            List<? extends CertificateRequest> all = certificateRequestStore.getAllRequests(null, null);

            for (CertificateRequest request : all) {
                certificateRequestStore.deleteRequest(request.getID());
            }

            assertEquals(0, certificateRequestStore.getSize());
        });
    }

    private CertificateRequest addRequest(X500Principal subject, String email, int validity, String signatureAlgorithm,
            String crlDistPoint, String certificateHandlerName, byte[] data, Date lastUpdated, String lastMessage,
            KeyPair keyPair)
    {
        return addRequest(null, subject, email, validity, signatureAlgorithm, crlDistPoint, certificateHandlerName,
                data, lastUpdated, lastMessage, keyPair);
    }

    private CertificateRequest addRequest(Date created, X500Principal subject, String email, int validity, String
            signatureAlgorithm, String crlDistPoint, String certificateHandlerName, byte[] data, Date lastUpdated,
            String lastMessage, KeyPair keyPair)
    {
        return transactionOperations.execute(status ->
        {
            try {
                CertificateRequestEntity request = created != null ?
                        new CertificateRequestEntity(certificateHandlerName, created) :
                        new CertificateRequestEntity(certificateHandlerName);

                request.setSubject(subject);
                request.setEmail(email);
                request.setValidity(validity);
                request.setSignatureAlgorithm(signatureAlgorithm);
                request.setCRLDistributionPoint(crlDistPoint);
                request.setData(data);
                request.setLastUpdated(lastUpdated);
                request.setLastMessage(lastMessage);

                certificateRequestStore.addRequest(request);

                return request;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private List<? extends CertificateRequest> getRequestsByEmail(String email, Match match, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status ->
            certificateRequestStore.getRequestsByEmail(email, match, firstResult, maxResults)
        );
    }

    private List<? extends CertificateRequest> getAllRequests(Integer firstResult, Integer maxResults) {
        return transactionOperations.execute(status -> certificateRequestStore.getAllRequests(firstResult, maxResults)
        );
    }

    private CloseableIterator<? extends CertificateRequest> getAllRequests() {
        return transactionOperations.execute(status -> certificateRequestStore.getAllRequests());
    }

    private void updateRequest(UUID id, byte[] data, Date lastUpdated, String lastMessage, Date nextUpdate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            CertificateRequest request = certificateRequestStore.getRequest(id);

            assertNotNull(request);

            request.setData(data);
            request.setLastUpdated(lastUpdated);
            request.setLastMessage(lastMessage);
            request.setNextUpdate(nextUpdate);
        });
    }

    private CertificateRequest getRequest(UUID id) {
        return transactionOperations.execute(status -> certificateRequestStore.getRequest(id));
    }

    private CertificateRequest getNextRequest() {
        return transactionOperations.execute(status -> certificateRequestStore.getNextRequest());
    }

    private long getSize()
    {
        return Optional.ofNullable(transactionOperations.execute(status ->
                certificateRequestStore.getSize())).orElseThrow();
    }

    private long getSizeByEmail(String email, Match match)
    {
        return Optional.ofNullable(transactionOperations.execute(status ->
                certificateRequestStore.getSizeByEmail(email, match))).orElseThrow();
    }

    private KeyPair generateKeyPair()
    throws Exception
    {
        return SecurityFactoryFactory.getSecurityFactory().createKeyPairGenerator("RSA").generateKeyPair();
    }

    @Test
    public void testGetSizeByEmail()
    {
        addRequest(new Date(), null, "test@example.com", 0,
                null, null, "dummy",
                null, null, null, null);

        addRequest(new Date(), null, "other@example.com", 0,
                null, null, "dummy",
                null, null, null, null);

        addRequest(new Date(), null, "test@example.com", 0,
                null, null, "dummy",
                null, null, null, null);

        addRequest(new Date(), null, " test@EXAMPLE.com ", 0,
                null, null, "dummy",
                null, null, null, null);

        addRequest(new Date(), null, "andanother@example.com", 0,
                null, null, "dummy",
                null, null, null, null);

        assertEquals(3, getSizeByEmail("test@example.com", Match.EXACT));
        assertEquals(3, getSizeByEmail(" TEST@example.COM ", Match.EXACT));
        assertEquals(1, getSizeByEmail("other@example.com", Match.EXACT));
        assertEquals(5, getSizeByEmail("%example%", Match.LIKE));
    }

    @Test
    public void testGetAllRequests()
    {
        int total = 10;

        for (int i = 0; i < total; i++)
        {
            addRequest(new Date(), null, i + "@example.com", 0,
                    null, null, "dummy",
                    null, null, null, null);

            // wait some time to make sure the creation date is always different (otherwise sorting might return
            // a different order than expected which will then fail the unit tests
            ThreadUtils.sleepQuietly(5);
        }

        assertEquals(total, getSize());

        List<? extends CertificateRequest> all = getAllRequests(null, null);

        assertEquals(total, all.size());

        for (int i = 0; i < total; i++) {
            assertEquals(i + "@example.com", all.get(i).getEmail());
        }

        int start = 0;
        int max = 3;

        all = getAllRequests(start, max);
        assertEquals(max, all.size());

        for (int i = 0; i < max; i++) {
            assertEquals(i + start + "@example.com", all.get(i).getEmail());
        }

        start = 4;
        max = 2;

        all = getAllRequests(start, max);
        assertEquals(max, all.size());

        for (int i = 0; i < max; i++) {
            assertEquals(i + start + "@example.com", all.get(i).getEmail());
        }

        start = 7;
        max = 100;

        all = getAllRequests(start, max);
        assertEquals(Math.min(total - start, max), all.size());

        for (int i = 0; i < Math.min(total - start, max); i++) {
            assertEquals(i + start + "@example.com", all.get(i).getEmail());
        }

        start = 100;

        all = getAllRequests(start, max);
        assertEquals(0, all.size());

        deleteAllRequests();

        assertEquals(total, testCertificateRequestHandler.cleaned.size());

        for (int i = 0; i < total; i++) {
            assertTrue(testCertificateRequestHandler.cleaned.contains(i + "@example.com"));
        }
    }

    @Test
    public void testGetAllRequestsIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                int total = 10;

                long now = System.currentTimeMillis();

                for (int i = 0; i < total; i++)
                {
                    addRequest(new Date(now + i * 1000), null, i + "@example.com", 0,
                            null, null, "dummy",
                            null, null, null, null);
                }

                assertEquals(total, getSize());

                CloseableIterator<? extends CertificateRequest> iterator = getAllRequests();

                int i = 0;

                while (iterator.hasNext())
                {
                    CertificateRequest request = iterator.next();

                    assertEquals(i + "@example.com", request.getEmail());

                    i++;
                }

                assertEquals(total, i);

                assertTrue(iterator.isClosed());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetNextRequest()
    {
        Date now = new Date();

        Date newer = DateUtils.addDays(now, 1);

        addRequest(newer, null, "2@example.com", 0, null,
                null, "dummy",
                null, null, null, null);

        addRequest(now, null, "1@example.com", 0, null,
                null, "dummy",
                null, null, null, null);

        CertificateRequest request = getNextRequest();

        assertNotNull(request);
        assertEquals("1@example.com", request.getEmail());

        request = getNextRequest();

        assertNotNull(request);
        assertEquals("1@example.com", request.getEmail());

        updateRequest(request.getID(), new byte[]{1,2,3}, new Date(), "message",
                DateUtils.addSeconds(new Date(), 5));

        request = getNextRequest();

        assertNotNull(request);
        assertEquals("2@example.com", request.getEmail());

        request = getNextRequest();

        assertNotNull(request);
        assertEquals("2@example.com", request.getEmail());

        updateRequest(request.getID(), new byte[]{1,2,3}, new Date(), "message",
                DateUtils.addSeconds(new Date(), 6));

        request = getNextRequest();

        assertNull(request);
    }

    @Test
    public void testUpdate()
    {
        Date now = new Date();

        CertificateRequest request = addRequest(now, null, "test@example.com", 1,
                null, null, "dummy",
                null, null, null, null);

        assertNotNull(request.getID());
        assertNull(request.getData());
        assertNull(request.getLastUpdated());
        assertNull(request.getLastMessage());

        Date updated = DateUtils.addDays(now, 1);

        updateRequest(request.getID(), new byte[]{1,2,3}, updated, "message", null);

        request = getRequest(request.getID());

        assertEquals("test@example.com", request.getEmail());
        assertTrue(ArrayUtils.isEquals(new byte[]{1,2,3}, request.getData()));

        // Because MySQL does not store milliseconds, we need to truncate before comparing
        assertEquals(DateUtils.truncate(updated, Calendar.SECOND),
                DateUtils.truncate(request.getLastUpdated(), Calendar.SECOND));

        assertEquals("message", request.getLastMessage());
    }

    @Test
    public void testSorting()
    throws Exception
    {
        Date now = new Date();

        Date old = DateUtils.addDays(now, -1);

        addRequest(now, null, "now@example.com", 365, null,
                null, "dummy",
                null, new Date(), null, null);

        addRequest(old, null, "old@example.com", 365, null,
                null, "dummy",
                null, new Date(), null, null);

        List<? extends CertificateRequest> found = getAllRequests(null, null);

        assertEquals(2, found.size());

        CertificateRequest request = found.get(0);
        assertEquals("old@example.com", request.getEmail());

        request = found.get(1);
        assertEquals("now@example.com", request.getEmail());
    }

    @Test
    public void testAddRequest()
    throws Exception
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("john doe");
        builder.setEmail("johndoe@example.com");

        KeyPair keyPair = generateKeyPair();

        addRequest(builder.buildPrincipal(), "test@example.com", 365, "SHA1",
                "http://example.com", "dummy",
                new byte[]{1,2,3}, new Date(), "Some message", keyPair);

        List<? extends CertificateRequest> found = getRequestsByEmail("test@example.com", null,
                null, null);

        assertEquals(1, found.size());

        CertificateRequest request = found.get(0);
    }

    @Test
    public void testAddDuplicateRequest()
    throws Exception
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("john doe");
        builder.setEmail("johndoe@example.com");

        KeyPair keyPair = generateKeyPair();

        Date date = new Date();

        addRequest(date, builder.buildPrincipal(), "test@example.com", 365, "SHA1",
                "http://example.com", "dummy",
                new byte[]{1,2,3}, new Date(), "Some message", keyPair);

        addRequest(date, builder.buildPrincipal(), "test@example.com", 365, "SHA1",
                "http://example.com", "dummy",
                new byte[]{1,2,3}, new Date(), "Some message", keyPair);

        List<? extends CertificateRequest> found = getRequestsByEmail("test@example.com", Match.EXACT, null, null);

        assertEquals(2, found.size());
    }

    @Test
    public void testAddRequestOnlyEmail()
    {
        addRequest(null, "test@example.com", 365, null, null,
                "dummy", null, null, null, null);
    }
}
