/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class PGPSignatureValidatorImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final PGPSignatureValidator signatureValidator = new PGPSignatureValidatorImpl();

    @Test
    public void testValidateSingleUserIDSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(1, userIDs.size());

        assertEquals("test key djigzo (this is a test key) <test@example.com>", PGPUtils.userIDToString(
                userIDs.get(0)));

        int i = 0;

        for (int signatureType : PGPSignatureInspector.USER_ID_CERTIFICATION_TYPES)
        {
            List<PGPSignature> signatures = PGPSignatureInspector.getSignaturesOfType(masterKey, signatureType);

            for (PGPSignature signature : signatures)
            {
                for (byte[] userID : userIDs)
                {
                    assertTrue(signatureValidator.validateUserIDSignature(masterKey, userID, signature));

                    i++;
                }
            }
        }

        assertEquals(1, i);
    }

    @Test
    public void testValidateUserIDNotSelfSigned()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "re@pgp.com-0x53691E61FA85D00F.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<byte[]> userIDs = PGPPublicKeyInspector.getUserIDs(masterKey);

        assertEquals(3, userIDs.size());

        List<PGPSignature> defaultCertSigs = PGPSignatureInspector.getSignaturesOfType(masterKey,
                PGPSignature.DEFAULT_CERTIFICATION);

        assertEquals(450, defaultCertSigs.size());

        byte[] userID = userIDs.iterator().next();

        try {
            signatureValidator.validateUserIDSignature(masterKey, userID, defaultCertSigs.get(0));

            fail();
        }
        catch (PGPNotSelfSignedException e)
        {
            assertEquals("Signature is not a self signature. It was signed by ACBB164BCF73EC4C and not by " +
            		"53691E61FA85D00F", e.getMessage());
        }
    }

    @Test
    public void testValidateUserIDTampered()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<PGPSignature> defaultCertSigs = PGPSignatureInspector.getSignaturesOfType(masterKey,
                PGPSignature.POSITIVE_CERTIFICATION);

        assertEquals(1, defaultCertSigs.size());

        assertFalse(signatureValidator.validateUserIDSignature(masterKey, "invalid user id".getBytes(),
                defaultCertSigs.get(0)));
    }

    @Test
    public void testValidateUserIDIncorrectSignatureType()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "revoked-master-key.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(masterKey,
                PGPSignature.KEY_REVOCATION);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateUserIDSignature(masterKey, "".getBytes(), sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("Signature type 32 is not a User-ID certification signature.", e.getMessage());
        }
    }

    @Test
    public void testValidateSubkeyBindingSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(subKey,
                PGPSignature.SUBKEY_BINDING);

        assertEquals(1, sigs.size());

        assertTrue(signatureValidator.validateSubkeyBindingSignature(masterKey, subKey, sigs.get(0)));
    }

    @Test
    public void testValidateSubkeyBindingSignatureIncorrectSignatureType()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(masterKey,
                PGPSignature.POSITIVE_CERTIFICATION);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateSubkeyBindingSignature(masterKey, subKey, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("Signature type 19 is not a subkey binding signature.", e.getMessage());
        }
    }

    @Test
    public void testValidateSubkeyBindingSignatureKeyTypeIncorrect()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(subKey,
                PGPSignature.SUBKEY_BINDING);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateSubkeyBindingSignature(masterKey, masterKey, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("key with key id 4EC4E8813E11A9A0 is not a sub key but a master key.", e.getMessage());
        }

        try {
            signatureValidator.validateSubkeyBindingSignature(subKey, subKey, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("key with key id 8AAA6718AADEAF7F is not a master key but a sub key.", e.getMessage());
        }
    }

    @Test
    public void testValidateSubkeyBindingSignatureNotSelfSigned()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(subKey,
                PGPSignature.SUBKEY_BINDING);

        assertEquals(1, sigs.size());

        PGPPublicKey otherMaster = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")).get(0);

        try {
            signatureValidator.validateSubkeyBindingSignature(otherMaster, subKey, sigs.get(0));

            fail();
        }
        catch (PGPNotSelfSignedException e)
        {
            assertEquals("Signature is not a self signature. It was signed by 4EC4E8813E11A9A0 and not by " +
            		"D7EE5B33E871B92F", e.getMessage());
        }
    }

    @Test
    public void testValidateSubkeyBindingSignatureSignatureIncorrect()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(subKey,
                PGPSignature.SUBKEY_BINDING);

        assertEquals(1, sigs.size());

        PGPPublicKey otherSubKey = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")).get(1);

        assertFalse(signatureValidator.validateSubkeyBindingSignature(masterKey, otherSubKey, sigs.get(0)));
    }

    @Test
    public void testValidateKeyRevocationSignatureSubKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey revokedSubKey = null;

        for (PGPPublicKey key : keys)
        {
            if (PGPUtils.getKeyIDHex(key.getKeyID()).equals("80C4F95A9FE00D79"))
            {
                revokedSubKey = key;

                break;
            }
        }

        assertNotNull(revokedSubKey);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(revokedSubKey,
                PGPSignature.SUBKEY_REVOCATION);

        assertEquals(1, sigs.size());

        assertTrue(signatureValidator.validateKeyRevocationSignature(master, revokedSubKey, sigs.get(0)));
    }

    @Test
    public void testValidateKeyRevocationSignatureSubKeyNull()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc"));

        assertEquals(6, keys.size());

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey revokedSubKey = null;

        for (PGPPublicKey key : keys)
        {
            if (PGPUtils.getKeyIDHex(key.getKeyID()).equals("80C4F95A9FE00D79"))
            {
                revokedSubKey = key;

                break;
            }
        }

        assertNotNull(revokedSubKey);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(revokedSubKey,
                PGPSignature.SUBKEY_REVOCATION);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateKeyRevocationSignature(master, null, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("Signature is a sub key revocation but sub key is not set.", e.getMessage());
        }
    }

    @Test
    public void testValidateKeyRevocationSignatureMasterKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "revoked-master-key.asc"));

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(master,
                PGPSignature.KEY_REVOCATION);

        assertEquals(1, sigs.size());

        assertTrue(signatureValidator.validateKeyRevocationSignature(master, null, sigs.get(0)));
    }

    @Test
    public void testValidateKeyRevocationSignatureIncorrectKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "revoked-master-key.asc"));

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey otherMaster = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")).get(0);

        assertTrue(otherMaster.isMasterKey());

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(master,
                PGPSignature.KEY_REVOCATION);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateKeyRevocationSignature(otherMaster, null, sigs.get(0));

            fail();
        }
        catch (PGPNotSelfSignedException e)
        {
            assertEquals("Signature is not a self signature. It was signed by 956AB99A20F57334 and not by " +
            		"D7EE5B33E871B92F", e.getMessage());
        }
    }

    @Test
    public void testValidateKeyRevocationSignatureWrongKeyType()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "revoked-master-key.asc"));

        PGPPublicKey master = keys.get(0);

        assertTrue(master.isMasterKey());

        PGPPublicKey subKey = keys.get(1);

        assertFalse(subKey.isMasterKey());

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(master,
                PGPSignature.KEY_REVOCATION);

        assertEquals(1, sigs.size());

        try {
            signatureValidator.validateKeyRevocationSignature(subKey, null, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("key with key id DF59F7DCE9CBAAD5 is not a master key but a sub key.", e.getMessage());
        }

        try {
            signatureValidator.validateKeyRevocationSignature(master, master, sigs.get(0));

            fail();
        }
        catch (PGPException e) {
            assertEquals("key with key id 956AB99A20F57334 is not a sub key but a master key.", e.getMessage());
        }
    }

    @Test
    public void testValidateUserIDRevocationSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-revoked-userid.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertTrue(masterKey.isMasterKey());

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesOfType(masterKey,
                PGPSignature.CERTIFICATION_REVOCATION);

        assertEquals(1, sigs.size());

        byte[] userID = PGPUtils.userIDToByteArray(
                "Revoked User ID (This User ID will be revoked) <test123@example.com>");

        List<PGPSignature> userIDSigs = PGPSignatureInspector.getSignaturesForUserID(masterKey, userID);

        assertEquals(2, userIDSigs.size());

        assertTrue(ArrayUtils.isEquals(sigs.get(0).getEncoded(), userIDSigs.get(0).getEncoded()));

        assertTrue(signatureValidator.validateUserIDRevocationSignature(masterKey, userID, sigs.get(0)));
    }
}
