/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingKeyPairProviderTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPKeyPairProvider keyPairProvider;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddPrivateKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPSecretKey> keys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : keys)
                {
                    keyRing.addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                            secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, keyRing.getSize());

                List<PGPKeyRingPair> pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("D80D1572D0486F55"));

                assertEquals(1, pairs.size());
                assertEquals("D80D1572D0486F55", PGPUtils.getKeyIDHex(pairs.get(0).getPublicKey().getKeyID()));
                assertNotNull(pairs.get(0).getPrivateKey());

                pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("639BCE48E8891CC7"));

                assertEquals(1, pairs.size());
                assertEquals("639BCE48E8891CC7", PGPUtils.getKeyIDHex(pairs.get(0).getPublicKey().getKeyID()));
                assertNotNull(pairs.get(0).getPrivateKey());

                pairs = keyPairProvider.getByKeyID(PGPUtils.getKeyIDFromHex("FF"));
                assertEquals(0, pairs.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
