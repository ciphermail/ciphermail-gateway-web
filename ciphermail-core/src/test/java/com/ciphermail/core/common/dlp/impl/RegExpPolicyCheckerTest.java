/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.cache.PatternCache;
import com.ciphermail.core.common.cache.SimpleMemoryPatternCache;
import com.ciphermail.core.common.dlp.PolicyChecker;
import com.ciphermail.core.common.dlp.PolicyCheckerContext;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.dlp.PolicyViolationException;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.test.TestProperties;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RegExpPolicyCheckerTest
{
    @Test
    public void testThresholdReached()
    {
        RegExpPolicyChecker policyChecker = new RegExpPolicyChecker();

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        Collection<PolicyPattern> patterns = new LinkedList<>();

        PolicyPatternImpl pattern = new PolicyPatternImpl("email", Pattern.compile(EmailAddressUtils.EMAIL_REG_EXPR));

        pattern.setAction(PolicyViolationAction.BLOCK);

        policyChecker.setOverlapLength(0);
        pattern.setThreshold(4);

        patterns.add(pattern);

        context.setContent("test1@example.com, test2@example.com ");
        context.setPatterns(patterns);

        policyChecker.init(context);

        policyChecker.update(context);
        context.setContent("test3@example.com, test4@example.com");
        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(1, violations.size());

            assertEquals("Policy: RegExp, Rule: email, Priority: BLOCK, Match: test1@example.com, test2@example.com, " +
                    "test3@example.com, test4@example.com", violations.get(0).toString());
        }
    }

    @Test
    public void testThresholdNotReached()
    throws PolicyViolationException
    {
        RegExpPolicyChecker policyChecker = new RegExpPolicyChecker();

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        Collection<PolicyPattern> patterns = new LinkedList<>();

        PolicyPatternImpl pattern = new PolicyPatternImpl("email", Pattern.compile(EmailAddressUtils.EMAIL_REG_EXPR));

        policyChecker.setOverlapLength(0);
        pattern.setThreshold(5);

        patterns.add(pattern);

        context.setContent("test1@example.com, test2@example.com ");
        context.setPatterns(patterns);

        policyChecker.init(context);

        policyChecker.update(context);
        context.setContent("test3@example.com, test4@example.com");
        policyChecker.update(context);

        policyChecker.finish(context);
    }

    @Test
    public void testOverlap()
    {
        RegExpPolicyChecker policyChecker = new RegExpPolicyChecker();

        policyChecker.setOverlapLength(3);

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        Collection<PolicyPattern> patterns = new LinkedList<>();

        patterns.add(new PolicyPatternImpl("test", Pattern.compile("\\b\\d+\\b")));

        context.setContent("123 aaa 456");
        context.setPatterns(patterns);

        policyChecker.init(context);

        policyChecker.update(context);
        context.setContent("789 bb 987");
        context.setPartial(true);

        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(1, violations.size());

            // because of overlap, 456 will be matched the second update as well but now complete
            assertEquals("Policy: RegExp, Rule: test, Priority: , Match: 123, 456, 456789, 987",
                    violations.get(0).toString());
        }
    }

    @Test
    public void testPolicyViolation()
    {
        PolicyChecker policyChecker = new RegExpPolicyChecker();

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        String content = "test 1234 other test 567 and another 89,01";

        Collection<PolicyPattern> patterns = new LinkedList<>();

        patterns.add(new PolicyPatternImpl("test", Pattern.compile("\\w\\d+\\w")));
        patterns.add(new PolicyPatternImpl("test2", Pattern.compile("\\w\\d+,\\d+\\w")));

        context.setContent(content);
        context.setPatterns(patterns);

        policyChecker.init(context);
        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(2, violations.size());

            assertEquals("Policy: RegExp, Rule: test2, Priority: , Match: \"89,01\"", violations.get(0).toString());
            assertEquals("Policy: RegExp, Rule: test, Priority: , Match: 1234, 567", violations.get(1).toString());
        }
    }

    @Test
    public void testMaxMatchWidth()
    {
        RegExpPolicyChecker policyChecker = new RegExpPolicyChecker();

        policyChecker.setMaxMatchWidth(4);

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        String content = "test 123456 aaa 1234 and 0987654";

        Collection<PolicyPattern> patterns = new LinkedList<>();

        patterns.add(new PolicyPatternImpl("test", Pattern.compile("\\b\\d+\\b")));

        context.setContent(content);
        context.setPatterns(patterns);

        policyChecker.init(context);
        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(1, violations.size());

            assertEquals("Policy: RegExp, Rule: test, Priority: , Match: 1..., 1234, 0...",
                    violations.get(0).toString());
        }
    }

    @Test
    public void testTotalMaxMatchWidth()
    {
        RegExpPolicyChecker policyChecker = new RegExpPolicyChecker();

        policyChecker.setTotalMaxMatchWidth(6);

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        String content = "test 123456 aaa 1234 and 0987654";

        Collection<PolicyPattern> patterns = new LinkedList<>();

        patterns.add(new PolicyPatternImpl("test", Pattern.compile("\\b\\d+\\b")));

        context.setContent(content);
        context.setPatterns(patterns);

        policyChecker.init(context);
        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(1, violations.size());

            assertEquals("Policy: RegExp, Rule: test, Priority: , Match: 123456...", violations.get(0).toString());
        }
    }

    @Test
    public void testLongString()
    {
        PolicyChecker policyChecker = new RegExpPolicyChecker();

        PolicyCheckerContext context = new PolicyCheckerContextImpl();

        String content = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

        Collection<PolicyPattern> patterns = new LinkedList<>();

        patterns.add(new PolicyPatternImpl("test", Pattern.compile("AAA")));

        context.setContent(content);
        context.setPatterns(patterns);

        policyChecker.init(context);
        policyChecker.update(context);

        try {
            policyChecker.finish(context);

            fail("PolicyViolationException expected.");
        }
        catch (PolicyViolationException e)
        {
            assertEquals("Policy violation", e.getMessage());

            List<PolicyViolation> violations = e.getViolations();

            assertNotNull(violations);

            assertEquals(1, violations.size());

            assertEquals("Policy: RegExp, Rule: test, Priority: , Match: AAA, AAA, AAA, AAA, AAA, AAA, AAA, " +
                    "AAA, AAA, AAA", violations.get(0).toString());
        }
    }

    @Test
    public void testSpeed()
    {
        PolicyChecker policyChecker = new RegExpPolicyChecker();

        String content = StringUtils.repeat("1234567890 ", 1000000) + "matchonthis";

        Collection<PolicyPattern> patterns = new LinkedList<>();

        PatternCache cache = new SimpleMemoryPatternCache();

        PolicyPatternImpl pattern = new PolicyPatternImpl();

        pattern.setPatternCache(cache);
        pattern.setName("test");
        pattern.setRegExp("matchonthis");

        patterns.add(pattern);

        int repeat = 1000;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            PolicyCheckerContext context = new PolicyCheckerContextImpl();

            context.setContent(content);
            context.setPatterns(patterns);

            policyChecker.init(context);
            policyChecker.update(context);

            try {
                policyChecker.finish(context);

                fail("PolicyViolationException expected.");
            }
            catch (PolicyViolationException e)
            {
                assertEquals("Policy violation", e.getMessage());

                List<PolicyViolation> violations = e.getViolations();

                assertNotNull(violations);

                assertEquals(1, violations.size());

                assertEquals("Policy: RegExp, Rule: test, Priority: , Match: matchonthis", violations.get(0).toString());
            }
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("checks/sec: " + perSecond);

        double expected = 50 * TestProperties.getTestPerformanceFactor();

        // NOTE: !!! can fail on a slower system
        assertTrue("Too slow. !!! this can fail on a slower system !!!", perSecond > expected);
    }
}
