/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPTrustListImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private PGPKeyRing keyRing;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                PGPTrustListDAO.deleteAll(SessionAdapterFactory.create(sessionManager.getSession()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PGPTrustList getTrustList(String name) {
        return new PGPTrustListImpl(name, sessionManager, keyRing);
    }

    @Test
    public void testSetEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                trustList.setEntry(entry, false);

                PGPTrustListEntry added = trustList.getEntry(publicKey);

                assertEquals(entry, added);
                assertEquals(1, trustList.size());

                // default is untrusted
                assertEquals(PGPTrustListStatus.UNTRUSTED, added.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetExistingEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                assertEquals(0, trustList.size());

                trustList.setEntry(entry, false);

                assertEquals(1, trustList.size());

                PGPTrustListEntry duplicate = trustList.createEntry(publicKey);

                trustList.setEntry(duplicate, false);

                assertEquals(1, trustList.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetSameKeyDifferentTrustList()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList1 = getTrustList("global1");
                PGPTrustList trustList2 = getTrustList("global2");

                assertEquals(0, trustList1.size());
                assertEquals(0, trustList2.size());

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry1 = trustList1.createEntry(publicKey);

                trustList1.setEntry(entry1, false);

                assertEquals(1, trustList1.size());
                assertEquals(0, trustList2.size());

                PGPTrustListEntry entry2 = trustList2.createEntry(publicKey);

                trustList2.setEntry(entry2, false);

                assertEquals(1, trustList1.size());
                assertEquals(1, trustList2.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetMulitpleEntries()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry1 = trustList.createEntry(publicKey);

                trustList.setEntry(entry1, false);

                PGPTrustListEntry entry2 = trustList.createEntry("abc");

                trustList.setEntry(entry2, false);

                PGPTrustListEntry entry3 = trustList.createEntry("def");

                trustList.setEntry(entry3, false);

                assertEquals(3, trustList.size());
                assertEquals(entry1, trustList.getEntry(publicKey));
                assertEquals(entry2, trustList.getEntry("abc"));
                assertEquals(entry3, trustList.getEntry("def"));

                List<? extends PGPTrustListEntry> entries = trustList.getEntries(0, Integer.MAX_VALUE);

                assertEquals(3, entries.size());
                assertTrue(entries.contains(entry1));
                assertTrue(entries.contains(entry2));
                assertTrue(entries.contains(entry3));

                List<? extends PGPTrustListEntry> someEntries = trustList.getEntries(0, 1);
                assertEquals(1, someEntries.size());
                assertTrue(someEntries.contains(entries.get(0)));

                someEntries = trustList.getEntries(1, 1);
                assertEquals(1, someEntries.size());
                assertTrue(someEntries.contains(entries.get(1)));

                someEntries = trustList.getEntries(2, 1);
                assertEquals(1, someEntries.size());
                assertTrue(someEntries.contains(entries.get(2)));

                someEntries = trustList.getEntries(1, 2);
                assertEquals(2, someEntries.size());
                assertTrue(someEntries.contains(entries.get(1)));
                assertTrue(someEntries.contains(entries.get(2)));

                someEntries = trustList.getEntries(10, 1);
                assertEquals(0, someEntries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetEntryIncludeSubKeysMasterKeyNotPresent()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                trustList.setEntry(entry, true);

                PGPTrustListEntry added = trustList.getEntry(publicKey);

                assertEquals(entry, added);
                assertEquals(1, trustList.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetEntryIncludeSubKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                assertEquals(2, publicKeys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey publicKey : publicKeys)
                {
                    keyRing.addPGPPublicKey(publicKey);

                    if (publicKey.isMasterKey()) {
                        masterKey = publicKey;
                    }
                    else {
                        subKey = publicKey;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                PGPTrustListEntry entry = trustList.createEntry(masterKey);

                entry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(entry, true);

                PGPTrustListEntry masterEntry = trustList.getEntry(masterKey);

                assertEquals(entry, masterEntry);
                assertEquals(2, trustList.size());

                PGPTrustListEntry subEntry = trustList.getEntry(subKey);

                assertNotNull(subEntry);

                assertEquals(masterEntry.getStatus(), subEntry.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetEntryIncludeSubKeysEntriesAlreadyExist()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                assertEquals(2, publicKeys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey publicKey : publicKeys)
                {
                    PGPTrustListEntry entry = trustList.createEntry(publicKey);

                    trustList.setEntry(entry, false);

                    keyRing.addPGPPublicKey(publicKey);

                    if (publicKey.isMasterKey()) {
                        masterKey = publicKey;
                    }
                    else {
                        subKey = publicKey;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                PGPTrustListEntry entry = trustList.getEntry(masterKey);

                assertEquals(PGPTrustListStatus.UNTRUSTED, entry.getStatus());

                entry = trustList.getEntry(subKey);

                assertEquals(PGPTrustListStatus.UNTRUSTED, entry.getStatus());

                entry = trustList.createEntry(masterKey);

                entry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(entry, true);

                PGPTrustListEntry masterEntry = trustList.getEntry(masterKey);

                assertEquals(PGPTrustListStatus.TRUSTED, masterEntry.getStatus());

                assertEquals(entry, masterEntry);
                assertEquals(2, trustList.size());

                PGPTrustListEntry subEntry = trustList.getEntry(subKey);

                assertNotNull(subEntry);

                assertEquals(masterEntry.getStatus(), subEntry.getStatus());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateTrusted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                entry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(entry, false);

                assertEquals(PGPTrustListStatus.TRUSTED, trustList.getEntry(publicKey).getStatus());

                PGPTrustListValidityResult result = trustList.checkValidity(publicKey);

                assertEquals(PGPTrustListValidity.VALID, result.getValidity());
                assertEquals(PGPTrustListImpl.TRUSTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateTrustedExpiredButAllowed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/expired.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                entry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(entry, false);

                assertEquals(PGPTrustListStatus.TRUSTED, trustList.getEntry(publicKey).getStatus());

                PGPTrustListValidityResult result = trustList.checkValidity(publicKey);

                assertEquals(PGPTrustListValidity.VALID, result.getValidity());
                assertEquals(PGPTrustListImpl.TRUSTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateUntrusted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                PGPTrustListEntry entry = trustList.createEntry(publicKey);

                entry.setStatus(PGPTrustListStatus.UNTRUSTED);

                trustList.setEntry(entry, false);

                assertEquals(PGPTrustListStatus.UNTRUSTED, trustList.getEntry(publicKey).getStatus());

                PGPTrustListValidityResult result = trustList.checkValidity(publicKey);

                assertEquals(PGPTrustListValidity.INVALID, result.getValidity());
                assertEquals(PGPTrustListImpl.UNTRUSTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateNotListed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                PGPTrustListValidityResult result = trustList.checkValidity(publicKey);

                assertEquals(PGPTrustListValidity.NOT_LISTED, result.getValidity());
                assertEquals(PGPTrustListImpl.NOT_LISTED_MESSAGE, result.getMessage());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testValidateNullCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PGPTrustList trustList = getTrustList("global");

            NullPointerException e = assertThrows(NullPointerException.class, () ->
                    trustList.checkValidity(null));
        });
    }

    @Test
    public void testDeleteEntryIncludeSubKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                assertEquals(0, trustList.size());

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                assertEquals(2, publicKeys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey publicKey : publicKeys)
                {
                    keyRing.addPGPPublicKey(publicKey);

                    if (publicKey.isMasterKey()) {
                        masterKey = publicKey;
                    }
                    else {
                        subKey = publicKey;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                PGPTrustListEntry entry = trustList.createEntry(masterKey);

                entry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(entry, true);

                assertEquals(2, trustList.size());

                trustList.deleteEntry(entry, true);

                assertEquals(0, trustList.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteEntryIncludeSubKeyTrustNotAvailable()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPTrustList trustList = getTrustList("global");

                assertEquals(0, trustList.size());

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                assertEquals(2, publicKeys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey publicKey : publicKeys)
                {
                    keyRing.addPGPPublicKey(publicKey);

                    if (publicKey.isMasterKey()) {
                        masterKey = publicKey;
                    }
                    else {
                        subKey = publicKey;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                PGPTrustListEntry entry = trustList.createEntry(masterKey);

                trustList.setEntry(entry, false);

                assertEquals(1, trustList.size());

                trustList.deleteEntry(entry, true);

                assertEquals(0, trustList.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
