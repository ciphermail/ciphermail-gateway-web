/*
 * Copyright (c) 2012-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MimeUtilsTest
{
    private static final File MAIL_DIR = new File(TestUtils.getTestDataDir(), "mail/");
    private static final File OTHER_DIR = new File(TestUtils.getTestDataDir(), "other/");

    @Test
    public void testIsMIME()
    throws Exception
    {
        // Read all eml files and check
        Collection<File> files = FileUtils.listFiles(MAIL_DIR, new String[]{"eml"}, false);

        for (File file : files)
        {
            System.out.println("Testing file: " + file);

            assertTrue(file + " is not valid MIME", MimeUtils.isMIME(new FileInputStream(file)));
        }
    }

    @Test
    public void testNoMIME()
    throws Exception
    {
        // Read all files and check
        Collection<File> files = FileUtils.listFiles(OTHER_DIR, null, false);

        for (File file : files)
        {
            System.out.println("Testing file: " + file);

            assertFalse(file + " is valid MIME but should not be", MimeUtils.isMIME(new FileInputStream(file)));
        }
    }

    @Test
    public void testIsMimeType()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(MAIL_DIR, "multipart-report.eml"));

        assertTrue(MimeUtils.isMimeType(message, "multipart/report"));

        assertTrue(MimeUtils.isMimeType((Multipart) message.getContent(), "multipart/report"));
    }
}
