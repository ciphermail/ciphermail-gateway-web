/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.CertificateInspector;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class PEMToPKCS12BuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    public KeyStore createKeyStore(Collection<X509Certificate> extraCertificates)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(new File(TEST_BASE, "keys/testCertificates.p12")), "test".toCharArray());

        PEMToPKCS12Builder builder = new PEMToPKCS12Builder();

        builder.addPEM(FileUtils.readFileToString(new File(TEST_BASE, "certificates/mitm-test-root.pem.cer"),
                    StandardCharsets.UTF_8), null)
                .addPEM(FileUtils.readFileToString(new File(TEST_BASE, "certificates/mitm-test-ca.pem.cer"),
                    StandardCharsets.UTF_8), null);

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");
        PrivateKey key = (PrivateKey) keyStore.getKey("Validcertificate", "test".toCharArray());

        assertNotNull(certificate);
        assertNotNull(key);

        StringWriter pem = new StringWriter();

        JcaPEMWriter writer = new JcaPEMWriter(pem);

        writer.writeObject(key);
        writer.writeObject(certificate);

        if (extraCertificates != null)
        {
            for (X509Certificate extraCertificate : extraCertificates) {
                writer.writeObject(extraCertificate);
            }
        }

        writer.close();

        builder.addPEM(pem.toString(), null);

        KeyStore resultKeyStore = builder.buildPKCS12("123".toCharArray());

        String alias = CertificateInspector.getThumbprint(certificate);

        assertEquals(certificate, resultKeyStore.getCertificate(alias));
        assertEquals(key, resultKeyStore.getKey(alias, "123".toCharArray()));

        Certificate[] chain = resultKeyStore.getCertificateChain(alias);

        assertEquals(3, chain.length);
        assertEquals(certificate, chain[0]);
        assertEquals(TestUtils.loadCertificate(new File(TEST_BASE, "certificates/mitm-test-ca.pem.cer")), chain[1]);
        assertEquals(TestUtils.loadCertificate(new File(TEST_BASE, "certificates/mitm-test-root.pem.cer")), chain[2]);

        return resultKeyStore;
    }

    @Test
    public void testBuildPFX()
    throws Exception
    {
        KeyStore keyStore = createKeyStore(null);

        assertEquals(1, keyStore.size());
    }

    @Test
    public void testBuildPFXExtraCertificates()
    throws Exception
    {
        Collection<X509Certificate> extra = CertificateUtils.readX509Certificates(new File(TEST_BASE,
                "certificates/testCertificates.p7b"));

        assertEquals(20, extra.size());

        KeyStore keyStore = createKeyStore(extra);

        assertEquals(20, keyStore.size());
    }

    @Test
    public void testBuildPFXCertificatesOnly()
    throws Exception
    {
        Collection<X509Certificate> certs = CertificateUtils.readX509Certificates(new File(TEST_BASE,
                "certificates/testCertificates.p7b"));

        assertEquals(20, certs.size());

        PEMToPKCS12Builder builder = new PEMToPKCS12Builder();

        StringWriter pem = new StringWriter();

        JcaPEMWriter writer = new JcaPEMWriter(pem);

        for (X509Certificate cert : certs) {
            writer.writeObject(cert);
        }

        writer.close();

        builder.addPEM(pem.toString(), null);

        KeyStore keyStore = builder.buildPKCS12("123".toCharArray());

        assertEquals(20, keyStore.size());

        Enumeration<String> aliasEnum = keyStore.aliases();

        int count = 0;

        while (aliasEnum.hasMoreElements())
        {
            String alias = aliasEnum.nextElement();

            assertTrue(keyStore.isCertificateEntry(alias));
            assertFalse(keyStore.isKeyEntry(alias));

            count++;
        }

        assertEquals(20, count);
    }

    @Test(expected = IOException.class)
    public void testBuildPFXNoPEM()
    throws Exception
    {
        PEMToPKCS12Builder builder = new PEMToPKCS12Builder();
        builder.buildPKCS12("123".toCharArray());
    }

    @Test
    public void testEncryptedPEM()
    throws Exception
    {
        PEMToPKCS12Builder builder = new PEMToPKCS12Builder();

        builder.addPEM(FileUtils.readFileToString(new File(TEST_BASE, "keys/testCertificates.protected.pem"),
                StandardCharsets.UTF_8),
                "test".toCharArray());

        KeyStore keyStore = builder.buildPKCS12("123".toCharArray());

        assertEquals(20, keyStore.size());

        Enumeration<String> aliasEnum = keyStore.aliases();

        int count = 0;

        while (aliasEnum.hasMoreElements())
        {
            String alias = aliasEnum.nextElement();

            assertTrue(keyStore.isKeyEntry(alias));

            count++;
        }

        assertEquals(20, count);
    }
}
