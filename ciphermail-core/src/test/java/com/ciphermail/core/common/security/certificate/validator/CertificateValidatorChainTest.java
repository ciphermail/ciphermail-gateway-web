/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate.validator;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class CertificateValidatorChainTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testCertificateValidatorChain()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/rim.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMESigning");

        CertificateValidatorChain validator = new CertificateValidatorChain("CertificateValidatorChain");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);

        assertTrue(validator.isValid(certificate));
    }

    @Test
    public void testCertificateValidatorChainInvalid()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/not-for-signing.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMESigning");

        CertificateValidatorChain validator = new CertificateValidatorChain("CertificateValidatorChain");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);

        assertFalse(validator.isValid(certificate));
        assertSame(signingValidator, validator.getFailingValidator());
    }

    @Test
    public void testCertificateValidatorEncryptionChainInvalid()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/not-for-encryption.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMEEncryption");

        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);

        assertFalse(validator.isValid(certificate));
        assertSame(encryptionValidator, validator.getFailingValidator());
    }

    @Test
    public void testCertificateValidatorNoValidators()
    throws Exception
    {
        File file = new File(TEST_BASE, "certificates/not-for-encryption.cer");

        X509Certificate certificate = TestUtils.loadCertificate(file);

        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        assertFalse(validator.isValid(certificate));
        assertNull(validator.getFailingValidator());
    }

    @Test
    public void testCertificateValidatorNullCertificate()
    throws Exception
    {
        CertificateValidator encryptionValidator = new IsValidForSMIMEEncryption("IsValidForSMIMEEncryption");
        CertificateValidator signingValidator = new IsValidForSMIMESigning("IsValidForSMIMEEncryption");

        CertificateValidatorChain validator = new CertificateValidatorChain("IsValidForSMIMEEncryption");

        validator.addValidators(encryptionValidator);
        validator.addValidators(signingValidator);

        assertFalse(validator.isValid(null));
        assertSame(encryptionValidator, validator.getFailingValidator());
    }
}
