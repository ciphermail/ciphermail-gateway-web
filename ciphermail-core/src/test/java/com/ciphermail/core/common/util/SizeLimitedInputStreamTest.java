/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collection;

class SizeLimitedInputStreamTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    void testTestMaxReached()
    throws IOException
    {
        InputStream input = new FileInputStream(new File(TEST_BASE, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        byte[] buf = new byte[maxBytes - 1];

        Assertions.assertEquals(buf.length, limitedInput.read(buf));

        try {
            Assertions.assertEquals(1, limitedInput.read(buf, 0, 1));

            Assertions.fail();
        }
        catch(IOException e) {
            // should be thrown
            Assertions.assertEquals(SizeLimitedInputStream.MAXIMUM_REACHED, e.getMessage());
        }
    }

    @Test
    void testTestMaxNotReached()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(TEST_BASE, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(limitedInput);

        Assertions.assertEquals(1000, certificates.size());
    }

    @Test
    void testTestMaxNotReachedMaxBytesNegative()
    throws IOException, CertificateException, NoSuchProviderException
    {
        InputStream input = new FileInputStream(new File(TEST_BASE, "certificates/random-self-signed-10.p7b"));

        final int maxBytes = -1;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes);

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(limitedInput);

        Assertions.assertEquals(10, certificates.size());
    }

    @Test
    void testLimitNoException()
    throws IOException
    {
        InputStream input = new FileInputStream(new File(TEST_BASE, "certificates/random-self-signed-1000.p7b"));

        final int maxBytes = 1000;

        InputStream limitedInput = new SizeLimitedInputStream(input, maxBytes, false);

        byte[] data = IOUtils.toByteArray(limitedInput);

        Assertions.assertEquals(1000, data.length);

        Assertions.assertEquals(0, limitedInput.available());
        Assertions.assertEquals(-1, limitedInput.read());
        Assertions.assertEquals(-1, limitedInput.read(data));
        Assertions.assertEquals(-1, limitedInput.read(data, 0, 100));
        Assertions.assertEquals(0, limitedInput.skip(10));
    }
}
