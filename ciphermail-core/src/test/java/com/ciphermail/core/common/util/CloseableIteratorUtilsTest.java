/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Test;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CloseableIteratorUtilsTest
{
    private static class StringListCloseableIterator implements CloseableIterator<String>
    {
        private final Iterator<String> iterator;

        private boolean closed;

        StringListCloseableIterator(List<String> stringList) {
            iterator = stringList.stream().iterator();
        }
        @Override
        public boolean hasNext()
        {
            boolean hasNext = iterator.hasNext();

            if (!hasNext) {
                closed = true;
            }

            return hasNext;
        }

        @Override
        public String next() {
            return iterator.next();
        }

        @Override
        public void close() {
            closed = true;
        }

        @Override
        public boolean isClosed() {
            return closed;
        }
    }

    @Test
    public void testToList()
    throws CloseableIteratorException
    {
        List<String> list = CloseableIteratorUtils.toList(new StringListCloseableIterator(List.of("one", "two")));

        assertEquals(2, list.size());
        assertEquals("one", list.get(0));
        assertEquals("two", list.get(1));

        list = CloseableIteratorUtils.toList(new StringListCloseableIterator(Collections.emptyList()));
        assertEquals(0, list.size());
    }

    @Test
    public void testGetCount()
    throws CloseableIteratorException
    {
        assertEquals(2, CloseableIteratorUtils.getCount(
                new StringListCloseableIterator(List.of("one", "two"))));
        assertEquals(0, CloseableIteratorUtils.getCount(
                new StringListCloseableIterator(Collections.emptyList())));
    }

    @Test
    public void testCloseQuietly()
    throws CloseableIteratorException
    {
        CloseableIterator<String> ci = new StringListCloseableIterator(List.of("one", "two"));

        assertFalse(ci.isClosed());

        CloseableIteratorUtils.closeQuietly(ci);

        assertTrue(ci.isClosed());

        CloseableIteratorUtils.closeQuietly(null);
    }
}