/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import com.ciphermail.core.common.security.password.validator.NOutOfMRuleJSONParser.NOutOfMRuleDef;
import com.ciphermail.core.common.security.password.validator.NOutOfMRuleJSONParser.WeightedRuleDef;
import org.json.JSONException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class NOutOfMRuleJSONParserTest
{
    @Test
    public void testParseJSONInvalidRegex()
    {
        String json = "{r:[{p:'['}]}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("pattern (p) is not a valid regular expression", e.getMessage());
        }
    }

    @Test
    public void testParseJSONInvalidMinimalRequired()
    {
        String json = "{m: 'a', r:[{p:'^.{8,}$'}]}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("minimal required value (m) is not a valid integer", e.getMessage());
        }
    }

    @Test
    public void testParseJSONInvalidWeight()
    {
        String json = "{r:[{w: 'a', p:'^.{8,}$'}]}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("weight value (w) is not a valid integer", e.getMessage());
        }
    }

    @Test
    public void testParseJSONMissingPattern()
    {
        String json = "{r:[{w: 1}]}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("patterns (p) are not defined", e.getMessage());
        }
    }

    @Test
    public void testParseJSONNoArray()
    {
        String json = "{r:1}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("rule value (r) is not a JSON array", e.getMessage());
        }
    }

    @Test
    public void testParseJSONEmptyArray()
    {
        String json = "{r:[]}";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("rule value (r) is empty", e.getMessage());
        }
    }

    @Test
    public void testParseJSONSingleRules()
    {
        String json = "{r:[{p:'^.{8,}$'}]}";

        NOutOfMRuleDef ruleDef = NOutOfMRuleJSONParser.parseJSON(json);

        assertEquals(1, ruleDef.getMinimumRequired());

        List<WeightedRuleDef> rules = ruleDef.getWeightedRules();

        assertEquals(1, rules.size());

        WeightedRuleDef rule = rules.get(0);

        assertEquals(1, rule.getWeight());
        assertEquals("^.{8,}$", rule.getRegex());
    }

    @Test
    public void testParseJSONMultipleRules()
    {
        String json = "{m:10, r:[{w:3, p:'(?U)^(?=.*[0-9]).{8,}$'},{p:'(?U)^(?=.*[a-z]).{8,}$'}]}";

        NOutOfMRuleDef ruleDef = NOutOfMRuleJSONParser.parseJSON(json);

        assertEquals(10, ruleDef.getMinimumRequired());

        List<WeightedRuleDef> rules = ruleDef.getWeightedRules();

        assertEquals(2, rules.size());

        WeightedRuleDef rule = rules.get(0);

        assertEquals(3, rule.getWeight());
        assertEquals("(?U)^(?=.*[0-9]).{8,}$", rule.getRegex());

        rule = rules.get(1);

        assertEquals(1, rule.getWeight());
        assertEquals("(?U)^(?=.*[a-z]).{8,}$", rule.getRegex());
    }

    @Test
    public void testParseInvalidJSON()
    {
        String json = "no-json";

        try {
            NOutOfMRuleJSONParser.parseJSON(json);

            fail();
        }
        catch (JSONException e) {
            assertEquals("the rule defintion should be a valid JSON string", e.getMessage());
        }
    }

    @Test
    public void testParseEmptyJSON()
    {
        try {
            NOutOfMRuleJSONParser.parseJSON("  ");

            fail();
        }
        catch (JSONException e) {
            assertEquals("the rule defintion cannot be empty", e.getMessage());
        }
    }

    @Test
    public void testParseNullJSON()
    {
        try {
            NOutOfMRuleJSONParser.parseJSON(null);

            fail();
        }
        catch (JSONException e) {
            assertEquals("the rule defintion cannot be empty", e.getMessage());
        }
    }
}
