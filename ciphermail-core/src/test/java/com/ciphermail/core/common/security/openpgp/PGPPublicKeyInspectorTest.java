/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PGPPublicKeyInspectorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testGetSHA256FingerprintHex()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals("ADE29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey));
    }

    @Test
    public void testGetUserIDs()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals("test key djigzo (this is a test key) <test@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(publicKey), ","));
    }

    @Test
    public void testGetMultipleUserIDs()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com-additional-userid.gpg.asc"));

        assertEquals("test key djigzo (this is a test key) <test@example.com>,additional user ID <test2@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(publicKey), ","));
    }

    @Test
    public void testGetUserIDsSubKey()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        for (PGPPublicKey publicKey : keys)
        {
            if (publicKey.isMasterKey())
            {
                assertEquals("test key djigzo (this is a test key) <test@example.com>",
                        StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(publicKey), ","));
            }
            else {
                assertEquals(0, PGPPublicKeyInspector.getUserIDsAsStrings(publicKey).size());
            }
        }
    }

    @Test
    public void testNullExpirationDate()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertNull(PGPPublicKeyInspector.getExpirationDate(publicKey));
    }

    @Test
    public void testExpirationDate()
    throws Exception
    {
        assertEquals(TestUtils.parseDate("31-Dec-2030 00:00:00 CET"),
                PGPPublicKeyInspector.getExpirationDate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "test@example.com-expiration-2030-12-31.gpg.asc"))));

        assertEquals(TestUtils.parseDate("11-Jan-2013 08:02:10 CET"),
                PGPPublicKeyInspector.getExpirationDate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "expired.asc"))));

        assertNull(PGPPublicKeyInspector.getExpirationDate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "test@example.com.gpg.asc"))));
    }

    /*
     * BC checks multiple certification types for expiration signatures. However if a certification type returns a valid
     * expiration date, BC will stop checking other certification types. If there are other certification types which
     * indicate that the key is still valid, BC will report an incorrect expiration time
     */
    @Test
    public void testExpirationDateMultipleSelfCertifications()
    throws Exception
    {
        assertEquals(TestUtils.parseDate("24-Feb-2022 12:25:21 CET"),
                PGPPublicKeyInspector.getExpirationDate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "C5C0375C920BD3F66564FB2AD465EEF3F810745C.asc"))));
    }

    @Test
    public void testGetKeyID()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(publicKey.getKeyID()));
    }

    @Test
    public void testGetShortKeyID()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "short-key-collision-1-19980101.gpg.asc"));

        assertEquals("19980101", PGPUtils.getShortKeyIDHex(publicKey.getKeyID()));

        publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        assertEquals("3E11A9A0", PGPUtils.getShortKeyIDHex(publicKey.getKeyID()));
    }

    @Test
    public void testGetSubkeyBindingSignatureKeyID()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        PGPPublicKey master = null;

        int i = 0;

        for (PGPPublicKey publicKey : keys)
        {
            if (publicKey.isMasterKey()) {
                master = publicKey;
            }
            else {
                i++;
                assertEquals(master.getKeyID(), (long) PGPPublicKeyInspector.getSubkeyBindingSignatureKeyID(publicKey));
            }
        }

        assertEquals(1, i);
    }

    @Test
    public void testGetSubkeyBindingSignatures()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "multiple-sub-key-bindings-per-subkey.asc"));

        assertEquals(7, keys.size());

        List<PGPSignature> signatures;

        int i = 0;

        for (PGPPublicKey key : keys)
        {
            String keyID = PGPUtils.getKeyIDHex(key.getKeyID());

            if ("3D182120ACD964D9".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(1, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
            else if ("54C21956B2A67D71".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(2, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
            else if ("A38C7C0C6C860E33".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(1, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
            else if ("16846342BF5BAE8A".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(2, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
            else if ("0C96953C847974F2".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(2, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
            else if ("F59CA933556EFC4C".equals(keyID))
            {
                i++;

                signatures = PGPPublicKeyInspector.getSubkeyBindingSignatures(key);

                assertEquals(2, signatures.size());

                for (PGPSignature signature : signatures) {
                    assertEquals("B10BA5F2D6D3E613", PGPUtils.getKeyIDHex(signature.getKeyID()));
                }
            }
        }

        assertEquals(6, i);
    }
}
