/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.cache;

import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class SimpleMemoryPatternCacheTest
{
    @Test
    public void test()
    {
        SimpleMemoryPatternCache patternCache = new SimpleMemoryPatternCache();

        Pattern p1 = patternCache.getPattern(".*");
        assertTrue(patternCache.getPatterns().containsKey("0:.*"));

        Pattern p2 = patternCache.getPattern(".*", Pattern.CASE_INSENSITIVE);
        assertTrue(patternCache.getPatterns().containsKey("2:.*"));

        assertNotSame(p1, p2);
        assertSame(p1, patternCache.getPattern(".*"));
        assertSame(p2, patternCache.getPattern(".*", Pattern.CASE_INSENSITIVE));
        assertEquals(0, p1.flags());
        assertEquals(Pattern.CASE_INSENSITIVE, p2.flags());
    }
}
