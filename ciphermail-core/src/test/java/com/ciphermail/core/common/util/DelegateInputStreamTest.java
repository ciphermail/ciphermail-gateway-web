/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;

public class DelegateInputStreamTest
{

    @Test
    public void testSingleInputStream()
    throws Exception
    {
        byte[] data = new byte[]{0,1,2,3};

        InputStream input = new DelegateInputStream(new ByteArrayInputStream(data));

        byte[] read = new byte[255];

        assertEquals(data.length, input.read(read));

        for (int i = 0; i < data.length; i++) {
            assertEquals(data[i], read[i]);
        }

        assertEquals(-1, input.read());

        input.close();

        input = new DelegateInputStream(new ByteArrayInputStream(data));

        int ch;
        int i = 0;

        while ((ch = input.read()) != -1)
        {
            assertEquals(data[i], ch);
            i++;
        }

        assertEquals(4, i);
    }

    @Test
    public void testMultipleInputStreams()
    throws Exception
    {
        byte[] data1 = new byte[]{0,1};
        byte[] data2 = new byte[]{2,3};
        byte[] data3 = new byte[]{3};

        InputStream input = new DelegateInputStream(new ByteArrayInputStream(data1),
                new ByteArrayInputStream(data2), new ByteArrayInputStream(data3));

        byte[] read = new byte[255];

        int bytesRead = input.read(read);

        assertEquals(data1.length + data2.length + data3.length, bytesRead);

        byte[] all = ArrayUtils.addAll(ArrayUtils.addAll(data1, data2), data3);

        assertEquals(all.length, bytesRead);

        for (int i = 0; i < all.length; i++)
        {
            assertEquals(all[i], read[i]);
        }

        assertEquals(-1, input.read());

        input.close();

        input = new DelegateInputStream(new ByteArrayInputStream(data1),
                new ByteArrayInputStream(data2), new ByteArrayInputStream(data3));

        int ch;
        int i = 0;

        while ((ch = input.read()) != -1)
        {
            assertEquals(all[i], ch);
            i++;
        }

        assertEquals(5, i);
    }
}
