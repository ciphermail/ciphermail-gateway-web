/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.junit.Test;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.ParseException;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class EmailAddressUtilsTest
{
    private static final File TEST_BASE = new File("src/test/resources/testdata");

    @Test
    public void testStripQuotes()
    {
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("test@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("'test@example.com'"));
        assertEquals("testexample.com", EmailAddressUtils.stripQuotes("testexample.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("\"test\"@example.com"));
        assertEquals("te\"st@example.com", EmailAddressUtils.stripQuotes("\"te\"st\"@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.stripQuotes("\"test@example.com\""));
    }

    @Test
    public void testValidate()
    {
        assertEquals("test@example.com", EmailAddressUtils.validate("test@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com"));
        assertEquals("\"test\"@example.com", EmailAddressUtils.validate("\"test\"@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com"));
        assertNull(EmailAddressUtils.validate("invalid"));
        assertNull(EmailAddressUtils.validate("<script>test.@example.com"));
        assertNull(EmailAddressUtils.validate("<test.@example.com"));
        assertEquals("test@example.com", EmailAddressUtils.validate("  test@example.com  "));
        assertNull(EmailAddressUtils.validate("test @example.com"));
        assertEquals("\"test \"@example.com", EmailAddressUtils.validate("\"test \"@example.com"));
        assertNull(EmailAddressUtils.validate("test@ex_ample.com"));
        assertEquals("\"test,123\"@example.com", EmailAddressUtils.validate("\"test,123\"@example.com"));
        assertNull(EmailAddressUtils.validate("test,123@example.com"));
        assertTrue(EmailAddressUtils.isValid(StringUtils.repeat("a", 314) + "@x.nl"));
        assertFalse(EmailAddressUtils.isValid(StringUtils.repeat("a", 315) + "@x.nl"));
    }

    @Test
    public void testIsValid()
    {
        assertTrue(EmailAddressUtils.isValid("test@example.com"));
        assertTrue(EmailAddressUtils.isValid("test@example.com", true));
        assertTrue(EmailAddressUtils.isValid("test@example.com", false));
        assertFalse(EmailAddressUtils.isValid("invalid"));
        assertFalse(EmailAddressUtils.isValid("invalid", true));
        assertTrue(EmailAddressUtils.isValid("invalid", false));
        assertFalse(EmailAddressUtils.isValid("invalid:email"));
        assertFalse(EmailAddressUtils.isValid("invalid:email", true));
        assertFalse(EmailAddressUtils.isValid("invalid:email", false));
    }

    @Test
    public void testIsValidEmailAddress()
    throws AddressException
    {
        assertTrue(EmailAddressUtils.isValid(new InternetAddress("test@example.com", false)));
        assertTrue(EmailAddressUtils.isValid(new InternetAddress("test@example.com", false), true));
        assertTrue(EmailAddressUtils.isValid(new InternetAddress("test@example.com", false), false));
        assertFalse(EmailAddressUtils.isValid(new InternetAddress("invalid", false)));
        assertFalse(EmailAddressUtils.isValid(new InternetAddress("invalid", false), true));
        assertTrue(EmailAddressUtils.isValid(new InternetAddress("invalid", false), false));
        assertFalse(EmailAddressUtils.isValid((InternetAddress) null));
    }

    @Test
    public void testValidateStrict()
    {
        assertEquals("test@example.com", EmailAddressUtils.validate("test@example.com", true));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com", true));
        assertEquals("test.@example.com", EmailAddressUtils.validate("test.@example.com"));
        assertEquals("valid", EmailAddressUtils.validate("valid", false));
        assertNull(EmailAddressUtils.validate("invalid:value", false));
    }

    @Test
    public void testCanonicalize()
    {
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("TEST.@example.com"));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("'TEST.@example.com'"));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalize("\"TEST.\"@example.com"));

        // The following email address is valid but only with quotes. canonicalize should not remove the quotes
        assertTrue(EmailAddressUtils.isValid("\".!@#$%^&*{}|:;/\"@djigzo.com"));
        assertEquals("\".!@#$%^&*{}|:;/\"@djigzo.com", EmailAddressUtils.canonicalize(
                "\".!@#$%^&*{}|:;/\"@djigzo.com"));
    }

    @Test
    public void testNormalize()
    {
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("TEST.@example.com", false));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("\"TEST.\"@example.com", false));
        assertEquals("test.@example.com", EmailAddressUtils.canonicalizeAndValidate("'TEST.@example.com'", false));
        assertEquals("test@example.com", EmailAddressUtils.canonicalizeAndValidate("\"test@example.com\"", false));
        assertNull(EmailAddressUtils.canonicalizeAndValidate("123", true));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate("123", false));
        assertNull(EmailAddressUtils.canonicalizeAndValidate((String)null, true));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate(
                EmailAddressUtils.INVALID_EMAIL, false));
        assertEquals(EmailAddressUtils.INVALID_EMAIL, EmailAddressUtils.canonicalizeAndValidate(
                EmailAddressUtils.INVALID_EMAIL, true));
        assertEquals("\".!@#$%^&*{}|:;/\"@djigzo.com", EmailAddressUtils.canonicalizeAndValidate(
                "\".!@#$%^&*{}|:;/\"@djigzo.com", true));
    }

    @Test
    public void testParseAddressList()
    throws ParseException
    {
        assertEquals("test@example.com", StringUtils.join(EmailAddressUtils.parseAddressList(
                "test@example.com"), ")"));

        assertEquals("test1@example.com,test2@example.com,test3@example.com", StringUtils.join(
                EmailAddressUtils.parseAddressList("test1@example.com,test2@example.com,  test3@example.com"),
                ","));

        assertEquals("test1@example.com#\"te,st\"@example.com", StringUtils.join(
                EmailAddressUtils.parseAddressList("test1@example.com,\"te,st\"@example.com"),
                "#"));

        assertEquals("test@example.com,invalid contains an invalid email address",
                assertThrows(AddressException.class, () -> EmailAddressUtils.parseAddressList(
                        "test@example.com,invalid")).getMessage());

        assertEquals("", StringUtils.join(EmailAddressUtils.parseAddressList(
                null), ")"));

        assertEquals("", StringUtils.join(EmailAddressUtils.parseAddressList(
                "   "), ")"));
    }

    @Test
    public void testInvalidEmail()
    throws ParseException
    {
        // Make sure that INVALID_EMAIL can be used to create an InternetAdddress
        InternetAddress address = new InternetAddress(EmailAddressUtils.INVALID_EMAIL);

        assertEquals(EmailAddressUtils.INVALID_EMAIL, address.getAddress());

        MailAddress mailAddress = new MailAddress(address);

        assertEquals(EmailAddressUtils.INVALID_EMAIL, mailAddress.toString());
    }

    @Test
    public void testNormalizeCollectionSingle()
    {
        List<String> emails = new LinkedList<>();

        emails.add("  test@example.com  ");
        emails.add(" 'test@example.com' ");
        emails.add(" \"TESt\"@example.com ");

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);

        assertEquals(1, normalized.size());
        assertEquals("test@example.com", normalized.iterator().next());
    }

    @Test
    public void testNormalizeCollectionMultiple()
    {
        List<String> emails = new LinkedList<>();

        emails.add("  test@example.com  ");
        emails.add(" xxx ");
        emails.add(" test2@example.com ");
        emails.add("  ");
        emails.add(" test @ example.com  ");

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);

        assertEquals(2, normalized.size());

        Iterator<String> it = normalized.iterator();

        assertEquals("test@example.com", it.next());
        assertEquals("test2@example.com", it.next());

        normalized = EmailAddressUtils.canonicalizeAndValidate(emails, false);

        assertEquals(3, normalized.size());

        assertTrue(normalized.contains("test@example.com"));
        assertTrue(normalized.contains("test2@example.com"));
        assertTrue(normalized.contains(EmailAddressUtils.INVALID_EMAIL));
    }

    @Test
    public void testNormalizeArrayMultiple()
    {
        String[] emails = new String[]{"  test@example.com  ", " xxx ", " test2@example.com ", "  ",
                " test @ example.com  "};

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);

        assertEquals(2, normalized.size());

        Iterator<String> it = normalized.iterator();

        assertEquals("test@example.com", it.next());
        assertEquals("test2@example.com", it.next());

        normalized = EmailAddressUtils.canonicalizeAndValidate(emails, false);

        assertEquals(3, normalized.size());

        assertTrue(normalized.contains("test@example.com"));
        assertTrue(normalized.contains("test2@example.com"));
        assertTrue(normalized.contains(EmailAddressUtils.INVALID_EMAIL));
    }

    @Test
    public void testNormalizeCollectionNull()
    {
        Collection<String> emails = null;

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);

        assertEquals(0, normalized.size());
    }

    @Test
    public void testNormalizeArrayNull()
    {
        String[] emails = null;

        Set<String> normalized = EmailAddressUtils.canonicalizeAndValidate(emails, true);

        assertEquals(0, normalized.size());
    }

    @Test
    public void testGetDomain()
    throws AddressException
    {
        assertEquals("example.com", EmailAddressUtils.getDomain("test@example.com"));
        assertNull(EmailAddressUtils.getDomain("example.com"));
        assertEquals("example.com", EmailAddressUtils.getDomain("@example.com"));
        assertNull(EmailAddressUtils.getDomain((String)null));
        assertNull(EmailAddressUtils.getDomain(""));
        assertEquals("example.com", EmailAddressUtils.getDomain(new InternetAddress("test@example.com")));
        assertNull(EmailAddressUtils.getDomain((InternetAddress)null));
    }

    @Test
    public void testGetDomainWithAt()
    {
        assertEquals("test.com", EmailAddressUtils.getDomain("\"x@example.com\"@test.com"));
    }

    @Test
    public void testGetLocalPart()
    throws AddressException
    {
        assertEquals("test", EmailAddressUtils.getLocalPart("test@example.com"));
        assertNull(EmailAddressUtils.getLocalPart("@example.com"));
        assertEquals("123", EmailAddressUtils.getLocalPart("123"));
        assertNull(EmailAddressUtils.getLocalPart((String)null));
        assertNull(EmailAddressUtils.getLocalPart(""));
        assertEquals("\"x@example.com\"", EmailAddressUtils.getLocalPart("\"x@example.com\"@test.com"));
        assertEquals("test", EmailAddressUtils.getLocalPart(new InternetAddress("test@example.com")));
        assertNull(EmailAddressUtils.getLocalPart((InternetAddress)null));
    }

    @Test
    public void testQuoteLocalPart()
    {
        assertEquals("test@example.com", EmailAddressUtils.quoteLocalPart("test@example.com"));
        assertEquals("\"te,st\"@example.com", EmailAddressUtils.quoteLocalPart("te,st@example.com"));
        assertEquals("\"te,st\"@example.com", EmailAddressUtils.quoteLocalPart("\"te,st\"@example.com"));
        assertEquals("\"te,st@example.com", EmailAddressUtils.quoteLocalPart("\"te,st@example.com"));
        assertEquals("test.123@example.com", EmailAddressUtils.quoteLocalPart("test.123@example.com"));
        assertEquals("test_123@example.com", EmailAddressUtils.quoteLocalPart("test_123@example.com"));
        assertEquals("test-123@example.com", EmailAddressUtils.quoteLocalPart("test-123@example.com"));
        assertEquals("te\"st@example.com", EmailAddressUtils.quoteLocalPart("te\"st@example.com"));
    }

    @Test
    public void testEmailPattern()
    {
        String replaceWith = "123";

        String s = "test@example.com";
        assertEquals("123", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  test@example.com  ";
        assertEquals("  123  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  test@example.com>  ";
        assertEquals("  123>  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  <test@example.com>  ";
        assertEquals("  <123>  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));

        s = "  &lt;test@example.com&gt;  ";
        assertEquals("  &lt;123&gt;  ", s.replaceAll(EmailAddressUtils.EMAIL_REG_EXPR, replaceWith));
    }

    @Test
    public void testContainsEmail()
    {
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", List.of("test@example.com")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", List.of("   test@eXAmple.com ")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", List.of("a@b.c", "test@eXAmple.com")));
        assertTrue(EmailAddressUtils.containsEmail("test@example.com", List.of("***", "test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("test2@example.com", List.of("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail(null, List.of("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("**", List.of("test@eXAmple.com")));
        assertFalse(EmailAddressUtils.containsEmail("test@example.com", null));
        assertFalse(EmailAddressUtils.containsEmail(null, null));
    }

    @Test
    public void testAddressesToStrings()
    throws AddressException
    {
        Address[] addresses = new Address[]{new InternetAddress("test@example.com", false),
                new InternetAddress("test user<test@example.com>", false),
                new InternetAddress("\"test user \" <test@example.com>", false),
                new InternetAddress("\"=?big5?q?=AFQ=C0s=B8=E9@=BD=DE=C0Y=20j?=\"<test@example.com>", false)};

        String[] s = EmailAddressUtils.addressesToStrings(addresses, true);

        assertEquals(4, s.length);
        assertEquals("test@example.com", s[0]);
        assertEquals("\"test user\" <test@example.com>", s[1]);
        assertEquals("\"test user\" <test@example.com>", s[2]);
        assertEquals("\"烏龍賊@豬頭 j\" <test@example.com>", s[3]);
    }

    @Test
    public void testAddressesToStringsRemoveUnsafeChars()
    throws AddressException
    {
        Address[] addresses = InternetAddress.parse(
                "\"=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?=\" <to@example.com>", false);

        String[] s = EmailAddressUtils.addressesToStrings(addresses, true);

        assertEquals(1, s.length);
        assertEquals("\"=\" <to@example.com>", s[0]);
    }

    @Test
    public void testGetAddress()
    throws Exception
    {
        assertEquals("test@example.com", EmailAddressUtils.getAddress(new Address[]{
                new InternetAddress("test@example.com")}).toString());

        assertEquals("test@example.com", EmailAddressUtils.getAddress(new Address[]{
                new InternetAddress("test@example.com"), new InternetAddress("test2@example.com")}).toString());

        assertNull(EmailAddressUtils.getAddress(new Address[]{}));
        assertNull(EmailAddressUtils.getAddress(null));
    }

    @Test
    public void testInternetAddress()
    throws Exception
    {
        assertEquals( "test@example.com", EmailAddressUtils.toInternetAddress(
                "test@example.com").toString());

        assertNull(EmailAddressUtils.toInternetAddress("@"));

        assertNull(EmailAddressUtils.toInternetAddress((String) null));

        assertEquals("test@example.com", EmailAddressUtils.toInternetAddress(
                new InternetAddress("test@example.com")).getAddress());

        assertEquals(new InternetAddress("test@example.com"), EmailAddressUtils.toInternetAddress(new Address()
        {
            @Override
            public String getType() {
                return null;
            }

            @Override
            public String toString() {
                return "test@example.com";
            }

            @Override
            public boolean equals(Object address) {
                return false;
            }

            @Override
            public int hashCode() {
                return 0;
            }
        }));
    }

    @Test
    public void testGetReplyToQuietly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/invalid-from-to-cc-reply-to.eml"));

        Address[] replyTo = EmailAddressUtils.getReplyToQuietly(message);

        assertNull(replyTo);

        message = MailUtils.loadMessage(new File(TEST_BASE, "mail/embedded-clear-signed.eml"));

        replyTo = EmailAddressUtils.getReplyToQuietly(message);

        assertNotNull(replyTo);
        assertEquals(1, replyTo.length);
        assertEquals("\"Ubuntu user technical support, not for general discussions\" <ubuntu-users@lists.ubuntu.com>",
                replyTo[0].toString());

        assertNull(EmailAddressUtils.getReplyToQuietly(null));
    }

    @Test
    public void testGetFromQuietly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/invalid-from-to-cc-reply-to.eml"));

        Address[] from = EmailAddressUtils.getFromQuietly(message);

        assertNull(from);

        message = MailUtils.loadMessage(new File(TEST_BASE, "mail/embedded-clear-signed.eml"));

        from = EmailAddressUtils.getFromQuietly(message);

        assertNotNull(from);
        assertEquals(1, from.length);
        assertEquals("James Gray <james@gray.net.au>", from[0].toString());

        assertNull(EmailAddressUtils.getFromQuietly(null));
    }

    @Test
    public void testToInternetAddresses()
    throws Exception
    {
        assertEquals("test@example.com", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                "test@example.com"), ","));

        assertEquals("test@example.com,a@b.c", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                "test@EXAMPLE.com", "a@B.c"), ","));

        assertEquals("test@example.com,a@b.c", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                "   'test@example.com'", "  a@b.c    "), ","));

        assertEquals("", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                "invalid"), ","));

        assertEquals("test@example.com", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                "invalid", "TEST@example.com"), ","));

        assertEquals("", StringUtils.join(EmailAddressUtils.toInternetAddresses((String[])null), ","));

        assertEquals("test@example.com", StringUtils.join(EmailAddressUtils.toInternetAddresses(
                List.of("test@example.com")), ","));
    }

    @Test
    public void testToInternetAddressesStrict()
    throws Exception
    {
        assertEquals("test@example.com", StringUtils.join(EmailAddressUtils.toInternetAddressesStrict(
                "test@example.com"), ","));

        assertEquals("test@example.com,a@b.c", StringUtils.join(EmailAddressUtils.toInternetAddressesStrict(
                "test@EXAMPLE.com", "a@B.c"), ","));

        assertEquals("test@example.com,a@b.c", StringUtils.join(EmailAddressUtils.toInternetAddressesStrict(
                List.of("test@EXAMPLE.com", "a@B.c")), ","));

        try {
            EmailAddressUtils.toInternetAddressesStrict("invalid");

            fail();
        }
        catch (IllegalArgumentException e) {
            assertEquals("invalid is not a valid email address", e.getMessage());
        }

        try {
            EmailAddressUtils.toInternetAddressesStrict("test@example.com", "invalid");

            fail();
        }
        catch (IllegalArgumentException e) {
            assertEquals("invalid is not a valid email address", e.getMessage());
        }

        try {
            EmailAddressUtils.toInternetAddressesStrict((String) null);

            fail();
        }
        catch (IllegalArgumentException e) {
            assertEquals("null is not a valid email address", e.getMessage());
        }
    }

    @Test
    public void testGetRecipientsNonStrict()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                Subject: test simple message
                From: <test@example.com>
                To: "test1" <test1@example.com>
                Cc: "test2" <test2@example.com>
                Bcc: "test3" <test3@example.com>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("test1 <test1@example.com>", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO), ","));
        assertEquals("test2 <test2@example.com>", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.CC), ","));
        assertEquals("test3 <test3@example.com>", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.BCC), ","));

        mime = """
                Content-Type: text/plain
                Subject: test simple message
                From: "test" <test@example.com>
                To: test@example.com, 'test2@example.com'

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        List<InternetAddress> addresses = EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO);

        assertNull(addresses.get(0).getPersonal());
        assertEquals("test@example.com", addresses.get(0).getAddress());
        assertNull(addresses.get(1).getPersonal());
        assertEquals("'test2@example.com'", addresses.get(1).getAddress());

        assertNull(EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.CC));
        assertNull(EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.BCC));

        mime = """
                Content-Type: text/plain
                Subject: test simple message
                From: "test" <test@example.com>
                To: test@example.com, test2@exa_mple.com

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        addresses = EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO);

        assertNull(addresses.get(0).getPersonal());
        assertEquals("test@example.com", addresses.get(0).getAddress());
        assertNull(addresses.get(1).getPersonal());
        assertEquals("test2@exa_mple.com", addresses.get(1).getAddress());
    }

    @Test
    public void testGetRecipientsNonStrictEmpty()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                Subject: test simple message
                From: <test@example.com>
                To: <>
                Cc: <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO), ","));
        assertEquals("", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.CC), ","));

        mime = """
                Content-Type: text/plain
                Subject: test simple message
                From: <test@example.com>
                To: "to" <>
                Cc: "cc" <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("to <>", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO), ","));
        assertEquals("cc <>", StringUtils.join(
                EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.CC), ","));
    }

    @Test
    public void testFromNonStrict()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                From: "bla" <test@example.com>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("bla <test@example.com>", StringUtils.join(EmailAddressUtils.getFromNonStrict(message), ","));

        mime = """
                Content-Type: text/plain
                From: !@#$%^&*

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("!@#$%^&*", StringUtils.join(EmailAddressUtils.getFromNonStrict(message), ","));
    }

    @Test
    public void testFromNonStrictEmptyAddress()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                From: <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("", StringUtils.join(EmailAddressUtils.getFromNonStrict(message), ","));

        mime = """
                Content-Type: text/plain
                From: "from" <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("from <>", StringUtils.join(EmailAddressUtils.getFromNonStrict(message), ","));
    }

    @Test
    public void testReplyToNonStrict()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                From: from@example.com
                Reply-to: reply@example.com

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("reply@example.com", StringUtils.join(EmailAddressUtils.getReplyToNonStrict(message), ","));

        mime = """
                Content-Type: text/plain
                From: "from" <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertNull(StringUtils.join(EmailAddressUtils.getReplyToNonStrict(message), ","));

        mime = """
                Content-Type: text/plain
                From: "from" <>
                Reply-to: <>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("", StringUtils.join(EmailAddressUtils.getReplyToNonStrict(message), ","));

        mime = """
                Content-Type: text/plain
                From: from@example.com

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        assertEquals("from@example.com", StringUtils.join(EmailAddressUtils.getReplyToNonStrict(
                message, true), ","));
    }

    @Test
    public void testGetAddressHeaderNonStrictInvalidAddress()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                From: " <from@example.com>
                Reply-To: " <reply@example.com>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        try {
            EmailAddressUtils.getFromNonStrict(message);

            fail();
        }
        catch (AddressException e) {
            /*
             * Expected exception
             */
        }

        try {
            EmailAddressUtils.getReplyToNonStrict(message);

            fail();
        }
        catch (AddressException e) {
            // Expected exception
        }
    }

    @Test
    public void testGetRecipientsNonStrictInvalidAddress()
    throws Exception
    {
        String mime;
        MimeMessage message;

        mime = """
                Content-Type: text/plain
                To: " <from@example.com>

                test""";

        message = new MimeMessage(null, IOUtils.toInputStream(mime, StandardCharsets.UTF_8));

        try {
            EmailAddressUtils.getRecipientsNonStrict(message, Message.RecipientType.TO);

            fail();
        }
        catch (AddressException e) {
            // Expected exception
        }
    }
}
