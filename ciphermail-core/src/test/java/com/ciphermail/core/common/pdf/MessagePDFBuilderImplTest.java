/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MessagePDFBuilderImplTest
{
    private static final FontProvider FONT_PROVIDER = new FileFontProvider(new File(
            TestUtils.getTestResourcesBaseDir(),"fonts/"));

    private MessagePDFBuilderImpl createPDFBuilder() {
        return new MessagePDFBuilderImpl();
    }

    private MessagePDFBuilderImpl buildPDFBuilder()
    throws Exception
    {
        MessagePDFBuilderImpl pdfBuilder = createPDFBuilder();

        pdfBuilder.setFontProvider(FONT_PROVIDER);

        pdfBuilder.setReplyButtonResource(new ClassPathResource("images/reply-button.png"));

        return pdfBuilder;
    }

    static String extractTextFromPDF(File pdfFile)
    throws Exception
    {
        PDDocument doc = PDDocument.load(pdfFile);
        return new PDFTextStripper().getText(doc);
    }

    private String buildPDF(MimeMessage message, String replyURL)
    throws Exception
    {
        MessagePDFBuilderImpl pdfBuilder = buildPDFBuilder();

        File file = File.createTempFile("ciphermail", ".pdf");
        file.deleteOnExit();

        OutputStream pdfOutput = new FileOutputStream(file);

        pdfBuilder.buildPDF(message, new MessagePDFBuilderParametersImpl(replyURL), pdfOutput);

        pdfOutput.close();

        return extractTextFromPDF(file);
    }

    @Test
    public void testEncodedFilename()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/message-with-attach-encoded-filename.eml"),
                "http://www.example.com");

        assertEquals("""
            From: test@example.com
            To: test@example.com
            CC:
            Subject: normal message with attachment
            Date: Sat Nov 03 14:51:28 CET 2007
            Attachments: äöü ÄÖÜ.zip;
            Test
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testISO8859_9()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/8859-9.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "Egemen Tanırer" <from@example.com>
            To: "Egemen Tanırer" <to@example.com>
            CC: "Egemen Tanırer" <cc@example.com>
            Subject: Turkish Character Problem
            Date: Fri Apr 08 11:27:32 CEST 2011
            Attachments:
            Dear Sir / Madam,
            Djigzo is really a fantastic email encrytption gateway software.
            We are using especially pdf encryption, but we have a problems with some turkish characters (ş, ğ) . Is it possible to change
            charset of program to "charset=windows-1254 or x-EBCDIC-Turkish or charset=iso-8859-9"
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testUmlautsISO885915()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/email-with-umlauts-iso-8859-15.eml"),
                null);

        assertEquals("""
            From: test@example.com
            To: test@example.com
            CC:
            Subject: message with Umlauts iso-8859-15
            Date: Mon Jun 14 12:10:28 CEST 2010
            Attachments:
            with Umlauts (max. Größe).
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testUnicode()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/unicode.eml"),
                null);

        // We compare all unicode chars because not all unicode chars are supported
            assertTrue(text.startsWith("""
            From: "Martijn Brinkers" <m.brinkers@pobox.com>
            To: test@example.com
            CC:
            Subject: unicode
            Date: Sat Oct 04 11:12:48 CEST 2008
            Attachments:
            Examples taken from http://www.columbia.edu/kermit/utf8.html
            """));
    }

    @Test
    public void testKoi8()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/message-koi8-r.eml"),
                "http://www.example.com");

        assertEquals("""
           From: test@example.com
           To: test@example.com
           CC:
           Subject: Большие груди
           Date: Fri Oct 03 10:32:01 CEST 2008
           Attachments:
           она ждет тебя без трусов
           Created with CipherMail free community edition.                                                                   
           """, text);
    }

    @Test
    public void testMessageWithLinks()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/message-with-links.eml"),
                "http://www.example.com");

        assertEquals("""
            From: test@example.com
            To: test@example.com
            CC:
            Subject: test simple message
            Date:
            Attachments:
            test http://example.com & www.example.com 123 & www.test.com
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testAttachedMessage()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/attached-message.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "list@mitm.nl" <list@mitm.nl>
            To: test@mitm.nl
            CC:
            Subject: forward test rfc822
            Date: Mon Apr 07 22:12:06 CEST 2008
            Attachments: Forwarded message - link test [pdf].pdf;
            This message contains an attached message
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testAttachedMessageWithEncodedName()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/attached-message-encoded-name.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "list@mitm.nl" <list@mitm.nl>
            To: test@mitm.nl
            CC:
            Subject: forward test rfc822
            Date: Mon Apr 07 22:12:06 CEST 2008
            Attachments: äöü ÄÖÜ.zip.pdf;
            This message contains an attached message
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testMessageChineseSubject()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/chinese-text.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "滕华" <tengxh@magic-sw.com.cn>
            To: "'Tapestry users'" <users@tapestry.apache.org>
            CC:
            Subject: 答复: is there tapestry 5 component for htmlarea?
            Date: Mon Apr 07 08:19:42 CEST 2008
            Attachments:
            You can find from http://code.google.com/p/tapestry5-components/ ,but I can
            not find the document this,so I don't know how to use it
            -----邮件原件-----
            发件人: Ahmad Maimunif [mailto:p037r4@yahoo.co.id]
            发送间: 2008年4月7日 13:54
            收件人: users@tapestry.apache.org
            主: is there tapestry 5 component for htmlarea?
            where can i find it?
            thx
            ---------------------------------
            Bergabunglah dengan orang-orang yang berwawasan, di bidang Anda di Yahoo!
            Answers
            ---------------------------------------------------------------------
            To unsubscribe, e-mail: users-unsubscribe@tapestry.apache.org
            For additional commands, e-mail: users-help@tapestry.apache.org
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testMessageChineseFrom()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/chinese-encoded-from.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "烏龍賊@豬頭 j" <wulungcheung3007@yahoo.com.hk>
            To:
            CC:
            Subject: I accept the payment of paypal
            Date: Sun Nov 04 19:53:49 CET 2007
            Attachments:
            XX
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testMessageWithAttachment()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/normal-message-with-attach.eml"),
                "http://www.example.com");

        assertEquals("""
            From: test@example.com
            To: test@example.com
            CC:
            Subject: normal message with attachment
            Date: Sat Nov 03 14:51:28 CET 2007
            Attachments: cityinfo.zip;
            Test
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testMessageWithMultipleAttachments()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "Martijn Brinkers" <martijn@djigzo.com>
            To: "Martijn Brinkers" <martijn@djigzo.com>
            CC:
            Subject: test multiple attachments
            Date: Wed Apr 29 15:58:25 CEST 2009
            Attachments: image1.png; image2.png; image3.png; image4.png; image5.png;
            --
            Djigzo open source email encryption gateway www.djigzo.com
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testHtmlOnly()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/html-only.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "Google Alerts" <googlealerts-noreply@google.com>
            To: m.brinkers@gmail.com
            CC:
            Subject: Google Alert - email encryption
            Date: Mon Apr 27 19:14:23 CEST 2009
            Attachments: attachment.bin;
            *** There was no inline text body. Make sure the email is sent as text only or is sent as html with alternative text part ***
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testTextAttachmentInline()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/text-attachment-inline.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "Martijn Brinkers" <martijn@djigzo.com>
            To: test@example.com
            CC:
            Subject: text attachment
            Date: Tue Jan 29 10:19:11 CET 2013
            Attachments:
            --
            DJIGZO email encryption
            test 123
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void testTextAttachment()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/text-attachment.eml"),
                "http://www.example.com");

        assertEquals("""
            From: "Martijn Brinkers" <martijn@djigzo.com>
            To: test@example.com
            CC:
            Subject: text attachment
            Date: Tue Jan 29 10:19:11 CET 2013
            Attachments: test.txt;
            --
            DJIGZO email encryption
            Created with CipherMail free community edition.
            """, text);
    }

    /*
     * The inline text exceeds the max and should therefore be added as an attachment
     */
    @Test
    public void testLargeTextBodyInlineSizeExceeded()
    throws Exception
    {
        String text = buildPDF(TestUtils.loadTestMessage("mail/test-5120K.eml"),
                "http://www.example.com");

        assertEquals("""
            From: test@example.com
            To: test@example.com
            CC:
            Subject:
            Date: Sat Nov 07 21:28:38 CET 2009
            Attachments: attachment.txt;
            *** There was no inline text body. Make sure the email is sent as text only or is sent as html with alternative text part ***
            Created with CipherMail free community edition.
            """, text);
    }

    @Test
    public void speedTest()
    throws Exception
    {
        int repeat = 1000;

        MimeMessage message = TestUtils.loadTestMessage("mail/8859-9.eml");

        // Warmup run
        for (int i = 0; i < repeat; i++)
        {
            MessagePDFBuilderImpl pdfBuilder = buildPDFBuilder();

            pdfBuilder.buildPDF(message, new MessagePDFBuilderParametersImpl("http://www.example.com"),
                    NullOutputStream.INSTANCE);
        }

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            MessagePDFBuilderImpl pdfBuilder = buildPDFBuilder();

            pdfBuilder.buildPDF(message, new MessagePDFBuilderParametersImpl("http://www.example.com"),
                    NullOutputStream.INSTANCE);
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("builds/sec: " + perSecond);

        double expected = 100 * TestProperties.getTestPerformanceFactor();

        // NOTE: !!! can fail on a slower system
        assertTrue("MessagePDFBuilder too slow. Expected > " + expected + " but was " + perSecond +
                " !!! this can fail on a slower system !!!", perSecond > expected);
    }
}
