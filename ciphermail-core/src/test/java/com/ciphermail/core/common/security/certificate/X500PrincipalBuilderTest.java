/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class X500PrincipalBuilderTest
{
    @Test
    public void testAllProperties()
    throws IOException
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("Martijn Brinkers");
        builder.setCountryCode("NL");
        builder.setEmail("test@example.com");
        builder.setGivenName("Martijn");
        builder.setSurname("Brinkers");
        builder.setLocality("Amsterdam");
        builder.setOrganisation("Example INC.");
        builder.setOrganisationalUnit("Test Unit");
        builder.setState("NH");
        X500Principal principal = builder.buildPrincipal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);

        assertEquals("[Martijn Brinkers]", inspector.getCommonName().toString());
        assertEquals("[NL]", inspector.getCountryCode().toString());
        assertEquals("[test@example.com]", inspector.getEmail().toString());
        assertEquals("[Martijn]", inspector.getGivenName().toString());
        assertEquals("[Brinkers]", inspector.getSurname().toString());
        assertEquals("[Amsterdam]", inspector.getLocality().toString());
        assertEquals("[Example INC.]", inspector.getOrganisation().toString());
        assertEquals("[Test Unit]", inspector.getOrganisationalUnit().toString());
        assertEquals("[NH]", inspector.getState().toString());
    }

    @Test
    public void testAllPropertiesFluent()
    throws IOException
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("Martijn Brinkers")
            .setCountryCode("NL")
            .setEmail("test@example.com")
            .setGivenName("Martijn")
            .setSurname("Brinkers")
            .setLocality("Amsterdam")
            .setOrganisation("Example INC.")
            .setOrganisationalUnit("Test Unit")
            .setState("NH");
        X500Principal principal = builder.buildPrincipal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);

        assertEquals("[Martijn Brinkers]", inspector.getCommonName().toString());
        assertEquals("[NL]", inspector.getCountryCode().toString());
        assertEquals("[test@example.com]", inspector.getEmail().toString());
        assertEquals("[Martijn]", inspector.getGivenName().toString());
        assertEquals("[Brinkers]", inspector.getSurname().toString());
        assertEquals("[Amsterdam]", inspector.getLocality().toString());
        assertEquals("[Example INC.]", inspector.getOrganisation().toString());
        assertEquals("[Test Unit]", inspector.getOrganisationalUnit().toString());
        assertEquals("[NH]", inspector.getState().toString());
    }

    @Test
    public void testJDKCompatibility()
    throws IOException
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("Martijn Brinkers")
                .setCountryCode("NL")
                .setEmail("test@example.com")
                .setGivenName("Martijn")
                .setSurname("Brinkers")
                .setLocality("Amsterdam")
                .setOrganisation("Example INC.")
                .setOrganisationalUnit("Test Unit")
                .setState("NH");
        X500Principal principal = builder.buildPrincipal();

        X500Principal jdkPrincipal = new X500Principal(
                "EMAILADDRESS=test@example.com, GIVENNAME=Martijn, SURNAME=Brinkers, " +
                "CN=Martijn Brinkers, OU=Test Unit, O=Example INC., L=Amsterdam, ST=NH, C=NL");

        // jdk version is different because the email address is encoded differently
        assertNotEquals(jdkPrincipal, principal);

        // re-encode the string version results in an equivalent version
        assertEquals(jdkPrincipal, new X500Principal(principal.toString()));
    }

    @Test
    public void testUsingCollection()
    throws IOException
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName(List.of("Martijn Brinkers"));
        builder.setCountryCode(List.of("NL"));
        builder.setEmail(List.of("test@example.com"));
        builder.setGivenName(List.of("Martijn"));
        builder.setSurname(List.of("Brinkers"));
        builder.setLocality(List.of("Amsterdam"));
        builder.setOrganisation(List.of("Example INC."));
        builder.setOrganisationalUnit(List.of("Test Unit"));
        builder.setState(List.of("NH"));
        X500Principal principal = builder.buildPrincipal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);

        assertEquals("[Martijn Brinkers]", inspector.getCommonName().toString());
        assertEquals("[NL]", inspector.getCountryCode().toString());
        assertEquals("[test@example.com]", inspector.getEmail().toString());
        assertEquals("[Martijn]", inspector.getGivenName().toString());
        assertEquals("[Brinkers]", inspector.getSurname().toString());
        assertEquals("[Amsterdam]", inspector.getLocality().toString());
        assertEquals("[Example INC.]", inspector.getOrganisation().toString());
        assertEquals("[Test Unit]", inspector.getOrganisationalUnit().toString());
        assertEquals("[NH]", inspector.getState().toString());
    }

    @Test
    public void testCustomValues()
    throws IOException
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("CipherMail");
        builder.addOID(RFC4519Style.st, "Some street");
        builder.addOID(RFC4519Style.serialNumber, "123", "456");

        X500Principal principal = builder.buildPrincipal();

        X500PrincipalInspector inspector = new X500PrincipalInspector(principal);

        assertEquals("[CipherMail]", inspector.getCommonName().toString());
        assertEquals("[Some street]", inspector.asStrings(RFC4519Style.st).toString());
        assertEquals("[123, 456]", inspector.asStrings(RFC4519Style.serialNumber).toString());
    }
}
