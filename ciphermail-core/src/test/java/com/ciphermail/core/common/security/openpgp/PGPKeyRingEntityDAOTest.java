/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingEntityDAOTest
{
    private static final String KEY_RING_NAME = "test";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                GenericHibernateDAO dao = new GenericHibernateDAO(SessionAdapterFactory.create(
                        sessionManager.getSession()));

                for (PGPKeyRingEntity entity : dao.findAll(PGPKeyRingEntity.class)) {
                    dao.delete(entity);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PGPKeyRingEntityDAO createDAO() {
        return createDAO(KEY_RING_NAME);
    }

    private PGPKeyRingEntityDAO createDAO(String storeName) {
        return PGPKeyRingEntityDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    private PGPKeyRingEntity getOrAddPGPKeyRingEntity(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey)
    throws Exception
    {
        PGPKeyRingEntity entity = dao.getByPublicKey(publicKey);

        if (entity == null)
        {
            entity = new PGPKeyRingEntity(publicKey, dao.getKeyRingName());

            dao.persist(entity);
        }

        return entity;
    }

    private void logKey(PGPPublicKey key)
    throws Exception
    {
        System.out.println("Master key: " + key.isMasterKey() + "  KeyID: " + PGPUtils.getKeyIDHex(key.getKeyID()) +
                ", Fingerprint 256: " + PGPPublicKeyInspector.getSHA256FingerprintHex(key) +
                ", Fingerprint: " + PGPPublicKeyInspector.getFingerprintHex(key));
    }

    private void addPublicKey(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey)
    throws Exception
    {
        logKey(publicKey);

        dao.persist(new PGPKeyRingEntity(publicKey, dao.getKeyRingName()));
    }

    private void addSubKey(PGPKeyRingEntityDAO dao, PGPPublicKey masterKey, PGPPublicKey subKey)
    throws Exception
    {
        PGPKeyRingEntity masterEntity = getOrAddPGPKeyRingEntity(dao, masterKey);

        PGPKeyRingEntity subKeyEntity = getOrAddPGPKeyRingEntity(dao, subKey);

        masterEntity.getSubkeys().add(subKeyEntity);

        subKeyEntity.setParentKey(masterEntity);
    }

    private PGPPublicKey addPublicKeys(PGPKeyRingEntityDAO dao, Collection<PGPPublicKey> keys)
    throws Exception
    {
        PGPPublicKey masterKey = null;

        for (PGPPublicKey key : keys)
        {
            addPublicKey(dao, key);

            if (key.isMasterKey())
            {
                masterKey = key;
            }
            else {
                addSubKey(dao, masterKey, key);
            }

            // Add some delay to make sorting on insertion date deterministic
            ThreadUtils.sleepQuietly(10);
        }

        return masterKey;
    }

    private void setSecretKeyAlias(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey, String alias)
    throws Exception
    {
        getOrAddPGPKeyRingEntity(dao, publicKey).setPrivateKeyAlias(alias);
    }

    private boolean keyExist(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey)
    throws Exception
    {
        return dao.getByPublicKey(publicKey) != null;
    }

    private void addEmailAddresses(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey, String... emails)
    throws Exception
    {
        getOrAddPGPKeyRingEntity(dao, publicKey).getEmail().addAll(CollectionUtils.asLinkedHashSet(emails));
    }

    private Set<String> getEmail(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey)
    throws Exception
    {
        PGPKeyRingEntity entity = dao.getByPublicKey(publicKey);

        return entity != null ? new HashSet<>(entity.getEmail()) : new HashSet<>();
    }

    private void deleteMasterKey(PGPKeyRingEntityDAO dao, PGPPublicKey publicKey)
    throws Exception
    {
        PGPKeyRingEntity entity = dao.getByPublicKey(publicKey);

        assertTrue(entity.isMasterKey());

        dao.delete(entity);
    }

    private void deleteSubKey(PGPKeyRingEntityDAO dao, PGPPublicKey masterKey, PGPPublicKey subKey)
    throws Exception
    {
        PGPKeyRingEntity masterEntity = dao.getByPublicKey(masterKey);

        Objects.requireNonNull(masterEntity, "masterEntity");
        assertTrue(masterEntity.isMasterKey());

        PGPKeyRingEntity subEntity = dao.getByPublicKey(subKey);

        Objects.requireNonNull(subEntity, "subEntity");
        assertFalse(subEntity.isMasterKey());

        masterEntity.getSubkeys().remove(subEntity);

        dao.delete(subEntity);
    }

    @Test
    public void testAddPublicKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                assertFalse(keyExist(dao, publicKey));
                assertFalse(keyExist(otherDAO, publicKey));

                addPublicKey(dao, publicKey);

                assertTrue(keyExist(dao, publicKey));
                assertFalse(keyExist(otherDAO, publicKey));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDuplicatePublicKey()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.asc"));

        transactionOperations.executeWithoutResult(status ->
        {
            PGPKeyRingEntityDAO dao = createDAO();

            try {
                assertFalse(keyExist(dao, publicKey));

                addPublicKey(dao, publicKey);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

            try {
                assertFalse(keyExist(otherDAO, publicKey));

                addPublicKey(otherDAO, publicKey);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        try {
            transactionOperations.executeWithoutResult(status ->
            {
                PGPKeyRingEntityDAO dao = createDAO();

                try {
                    assertTrue(keyExist(dao, publicKey));

                    addPublicKey(dao, publicKey);
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (DataIntegrityViolationException e) {
            /*
             * Expected
             */
        }
    }

    @Test
    public void testAddEmails()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addEmailAddresses(dao, publicKey, "test1@example.com", "test2@example.com", "test3@example.com");

                Set<String> emails = getEmail(dao, publicKey);

                assertEquals(3, emails.size());

                assertTrue(emails.contains("test1@example.com"));
                assertTrue(emails.contains("test2@example.com"));
                assertTrue(emails.contains("test3@example.com"));

                emails = getEmail(otherDAO, publicKey);

                assertEquals(0, emails.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                PGPPublicKey publicKey1 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                Date date = TestUtils.parseDate("09-Aug-2022 07:38:35 GMT");

                addPublicKey(dao, publicKey1);
                setSecretKeyAlias(dao, publicKey1, "alias1");
                assertEquals("alias1", dao.getByPublicKey(publicKey1).getPrivateKeyAlias());
                assertEquals(1, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));

                PGPPublicKey publicKey2 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                addPublicKey(dao, publicKey2);

                addEmailAddresses(dao, publicKey1, "test@example.com", "test1@example.com", "test2@example.com",
                        "test3@example.com");
                addEmailAddresses(dao, publicKey2, "test@example.com", "test3@example.com");

                Set<String> emails = getEmail(dao, publicKey1);

                assertEquals(4, emails.size());

                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("test1@example.com"));
                assertTrue(emails.contains("test2@example.com"));
                assertTrue(emails.contains("test3@example.com"));

                emails = getEmail(dao, publicKey2);

                assertEquals(2, emails.size());

                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("test3@example.com"));

                // Check matching test3@example.com
                List<PGPKeyRingEntity> entities = CloseableIteratorUtils.toList(dao.search(
                        PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(2, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey1, KEY_RING_NAME)));
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey2, KEY_RING_NAME)));

                // Search count for above parameters
                assertEquals(2, dao.searchCount(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));

                // Check not matching email address
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "testxx@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(0, entities.size());

                // Search count for above parameters
                assertEquals(0, dao.searchCount(PGPSearchField.EMAIL, "testxx@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));

                // Check LIKE
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(2, entities.size());

                // Search count for above parameters
                assertEquals(2, dao.searchCount(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));

                // Search master keys only
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), date, null, null));

                assertEquals(2, entities.size());

                // Search count for above parameters
                assertEquals(2, dao.searchCount(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), date));

                // Search sub keys only
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.SUB_KEY), date, null, null));

                assertEquals(0, entities.size());

                // Search count for above parameters
                assertEquals(0, dao.searchCount(PGPSearchField.EMAIL, "%example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.SUB_KEY), date));

                // Check expired not allowed, not expired
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), date, null, null));

                assertEquals(2, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey1,KEY_RING_NAME)));
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey2, KEY_RING_NAME)));

                // Search count for above parameters
                assertEquals(2, dao.searchCount(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), date));

                // Check expired not allowed, expired
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2031 00:00:00 CET"), null, null));

                assertEquals(1, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey1, KEY_RING_NAME)));

                // Search count for above parameters
                assertEquals(1, dao.searchCount(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2031 00:00:00 CET")));

                // Check expired only
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2077 00:00:00 CET"), null, null));

                // Search count for above parameters
                assertEquals(1, dao.searchCount(PGPSearchField.EMAIL, "%%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2077 00:00:00 CET")));

                assertEquals(1, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey2, KEY_RING_NAME)));

                // Check missing key alias not allowed
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(1, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(publicKey1, KEY_RING_NAME)));

                // Search count for above parameters
                assertEquals(1, dao.searchCount(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED, PGPKeyType.ALL),
                        date));

                // Check case sensitivity
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%EXAMPLE.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(2, entities.size());

                // Check first result
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, 1, null));

                assertEquals(1, entities.size());

                // Check max result
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, "%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, 0, 1));

                assertEquals(1, entities.size());

                // Search with null email address, i.e., search for entries without email
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, null,
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(0, entities.size());

                // Search count for above parameters
                assertEquals(0, dao.searchCount(PGPSearchField.EMAIL, null,
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));

                // Clear email addresses and retry
                getOrAddPGPKeyRingEntity(dao, publicKey1).getEmail().clear();

                // Search with null email address, i.e., search for entries without email
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.EMAIL, null,
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date, null, null));

                assertEquals(1, entities.size());

                // Search count for above parameters
                assertEquals(1, dao.searchCount(PGPSearchField.EMAIL, null,
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        date));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchUserID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                PGPPublicKey publicKey1 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addPublicKey(dao, publicKey1);
                setSecretKeyAlias(dao, publicKey1, "alias1");

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/zimbra_security.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/expired.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.asc")));

                List<PGPKeyRingEntity> entities;
                PGPSearchParameters searchParameters;

                Date date = TestUtils.parseDate("09-Aug-2022 07:38:35 GMT");

                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "Zimbra Security <security@zimbra.com>",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.USER_ID,
                        "Zimbra Security <security@zimbra.com>",
                        searchParameters, date));

                entities = CloseableIteratorUtils.toList(otherDAO.search(PGPSearchField.USER_ID,
                        "Zimbra Security <security@zimbra.com>",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, otherDAO.searchCount(PGPSearchField.USER_ID,
                        "Zimbra Security <security@zimbra.com>",
                        searchParameters, date));

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date));

                // Search with LIKE
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(4, entities.size());
                // Count of above query
                assertEquals(4, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date));

                // Search with LIKE but no expired keys
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(3, entities.size());
                // Count of above query
                assertEquals(3, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date));

                // Search with LIKE but only expired keys
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, TestUtils.parseDate("31-Dec-2077 00:00:00 CET"), null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, TestUtils.parseDate("31-Dec-2077 00:00:00 CET")));

                // Search with LIKE with key alias required
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date));

                // Search with LIKE, only master keys (should not make a difference because only master keys should
                // contains UserIDs
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.MASTER_KEY);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(4, entities.size());
                // Count of above query
                assertEquals(4, dao.searchCount(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date));

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, 1, null));
                assertEquals(3, entities.size());

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        "%",
                        searchParameters, date, 1, 2));
                assertEquals(2, entities.size());

                // Search null UserID. Should return all sub keys because they lack a UserID
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date, null, null));
                assertEquals(7, entities.size());
                // Count of above query
                assertEquals(7, dao.searchCount(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date));

                // Search null UserID. Only search master keys. Should return 0 because all master keys have a userID
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.MASTER_KEY);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, dao.searchCount(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date));

                // Search null UserID. Only search sub keys.
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.SUB_KEY);

                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date, null, null));
                assertEquals(7, entities.size());
                // Count of above query
                assertEquals(7, dao.searchCount(PGPSearchField.USER_ID,
                        null,
                        searchParameters, date));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchKeyID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                PGPPublicKey publicKey1 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addPublicKey(dao, publicKey1);
                setSecretKeyAlias(dao, publicKey1, "alias1");

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/zimbra_security.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/expired.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.asc")));

                List<PGPKeyRingEntity> entities;
                PGPSearchParameters searchParameters;

                Date date = TestUtils.parseDate("09-Aug-2030 07:38:35 GMT");

                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "4F892F3BC089A521",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.KEY_ID,
                        "4F892F3BC089A521",
                        searchParameters, date));

                // Other DAO
                entities = CloseableIteratorUtils.toList(otherDAO.search(PGPSearchField.KEY_ID,
                        "4F892F3BC089A521",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, otherDAO.searchCount(PGPSearchField.KEY_ID,
                        "4F892F3BC089A521",
                        searchParameters, date));

                // Match like
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "4%",
                        searchParameters, date, null, null));
                assertEquals(2, entities.size());
                // Count of above query
                assertEquals(2, dao.searchCount(PGPSearchField.KEY_ID,
                        "4%",
                        searchParameters, date));

                // Match like all
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(11, entities.size());
                // Count of above query
                assertEquals(11, dao.searchCount(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date));

                // No expired
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(9, entities.size());
                // Count of above query
                assertEquals(9, dao.searchCount(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date));

                // No missing key alias
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date));

                // Only master keys
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.MASTER_KEY);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(4, entities.size());
                // Count of above query
                assertEquals(4, dao.searchCount(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date));

                // Only sub keys
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.SUB_KEY);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date, null, null));
                assertEquals(7, entities.size());
                // Count of above query
                assertEquals(7, dao.searchCount(PGPSearchField.KEY_ID,
                        "%",
                        searchParameters, date));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSize()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                assertEquals(0, dao.getSize());
                assertEquals(0, otherDAO.getSize());

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.asc")));

                assertEquals(6, dao.getSize());
                assertEquals(0, otherDAO.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchFingerprint()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                PGPPublicKey publicKey1 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addPublicKey(dao, publicKey1);
                setSecretKeyAlias(dao, publicKey1, "alias1");

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/zimbra_security.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/expired.asc")));
                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.asc")));

                List<PGPKeyRingEntity> entities;
                PGPSearchParameters searchParameters;

                Date date = TestUtils.parseDate("09-Aug-2022 07:38:35 GMT");

                // Search for fingerprints
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date));

                // Other DAO
                entities = CloseableIteratorUtils.toList(otherDAO.search(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, otherDAO.searchCount(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date));

                // Search for fingerprints, only sub key allowed
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.SUB_KEY);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, dao.searchCount(PGPSearchField.FINGERPRINT,
                        "F56BB61C17ECF2CA70BBFA1CD80D1572D0486F55",
                        searchParameters, date));

                // Search for sub key fingerprint
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "0EFD4E328FC6878782B83C4633F215C0B0EC29E0",
                        searchParameters, date, null, null));
                assertEquals(1, entities.size());
                // Count of above query
                assertEquals(1, dao.searchCount(PGPSearchField.FINGERPRINT,
                        "0EFD4E328FC6878782B83C4633F215C0B0EC29E0",
                        searchParameters, date));

                // Search for sub key fingerprint, only master allowed
                searchParameters = new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.MASTER_KEY);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "0EFD4E328FC6878782B83C4633F215C0B0EC29E0",
                        searchParameters, date, null, null));
                assertEquals(0, entities.size());
                // Count of above query
                assertEquals(0, dao.searchCount(PGPSearchField.FINGERPRINT,
                        "0EFD4E328FC6878782B83C4633F215C0B0EC29E0",
                        searchParameters, date));

                // Search for fingerprints like
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "%D4%",
                        searchParameters, date, null, null));
                assertEquals(3, entities.size());
                // Count of above query
                assertEquals(3, dao.searchCount(PGPSearchField.FINGERPRINT,
                        "%D4%",
                        searchParameters, date));

                // Search for fingerprints like with limits
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "%",
                        searchParameters, date, 1, null));
                assertEquals(10, entities.size());

                // Search for fingerprints like with limits
                searchParameters = new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL);
                entities = CloseableIteratorUtils.toList(dao.search(PGPSearchField.FINGERPRINT,
                        "%",
                        searchParameters, date, 0, 5));
                assertEquals(5, entities.size());

            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testIsMasterKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            PGPKeyRingEntityDAO dao = createDAO();

            try {
                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                assertFalse(keyExist(dao, publicKey));

                addPublicKey(dao, publicKey);

                assertTrue(dao.getByPublicKey(publicKey).isMasterKey());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddSubKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = null;

                for (PGPPublicKey key : keys)
                {
                    addPublicKey(dao, key);

                    if (masterKey == null) {
                        masterKey = key;
                    }
                    else {
                        addSubKey(dao, masterKey, key);
                    }
                }

                Set<PGPKeyRingEntity> subKeys = dao.getByPublicKey(masterKey).getSubkeys();

                assertEquals(1, subKeys.size());
                assertEquals(dao.getByPublicKey(masterKey), subKeys.iterator().next().getParentKey());

                assertNull(dao.getByPublicKey(masterKey).getParentKey());

                assertEquals(2, dao.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByKeyID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                List<PGPKeyRingEntity> result = CloseableIteratorUtils.toList(dao.getByKeyID(
                        5675917072183437728L, MissingKeyAlias.ALLOWED));

                assertEquals(0, result.size());

                PGPPublicKey publicKey1 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addPublicKey(dao, publicKey1);

                result = CloseableIteratorUtils.toList(dao.getByKeyID(5675917072183437728L,
                        MissingKeyAlias.ALLOWED));

                assertEquals(1, result.size());

                result = CloseableIteratorUtils.toList(otherDAO.getByKeyID(5675917072183437728L,
                        MissingKeyAlias.ALLOWED));

                assertEquals(0, result.size());

                PGPPublicKey publicKey2 = PGPKeyUtils.decodePublicKey(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                addPublicKey(dao, publicKey2);

                result = CloseableIteratorUtils.toList(dao.getByKeyID(5675917072183437728L,
                        MissingKeyAlias.ALLOWED));

                assertEquals(1, result.size());

                result = CloseableIteratorUtils.toList(dao.getByKeyID(-2887270032596682449L,
                        MissingKeyAlias.ALLOWED));

                assertEquals(1, result.size());

                result = CloseableIteratorUtils.toList(dao.getByKeyID(0L,
                        MissingKeyAlias.ALLOWED));

                assertEquals(0, result.size());

                // Missing key alias not allowed
                result = CloseableIteratorUtils.toList(dao.getByKeyID(5675917072183437728L,
                        MissingKeyAlias.NOT_ALLOWED));

                assertEquals(0, result.size());

                // Set alias an try again
                dao.getByKeyID(5675917072183437728L, MissingKeyAlias.ALLOWED).next().setPrivateKeyAlias("alias");

                result = CloseableIteratorUtils.toList(dao.getByKeyID(5675917072183437728L,
                        MissingKeyAlias.NOT_ALLOWED));

                assertEquals(1, result.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetBySHA256Fingerprint()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/zimbra_security.asc")));

                assertNotNull(dao.getBySHA256Fingerprint(
                        "1EFD677DD41A93DD9C6FD3CF3FFA3B062E7AC062CB57690A75C19C1EFF3EADC3"));
                assertNull(dao.getBySHA256Fingerprint(
                        "2222677DD41A93DD9C6FD3CF3FFA3B062E7AC062CB57690A75C19C1EFF3EADC3"));
                assertNull(otherDAO.getBySHA256Fingerprint(
                        "1EFD677DD41A93DD9C6FD3CF3FFA3B062E7AC062CB57690A75C19C1EFF3EADC3"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByPublicKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                addPublicKeys(dao, PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/zimbra_security.asc")));

                PGPKeyRingEntity entity = dao.getBySHA256Fingerprint(
                        "1EFD677DD41A93DD9C6FD3CF3FFA3B062E7AC062CB57690A75C19C1EFF3EADC3");
                assertNotNull(entity);

                assertEquals(entity, dao.getByPublicKey(PGPKeyUtils.decodePublicKey(entity.getEncodedPublicKey())));
                assertNull(otherDAO.getByPublicKey(PGPKeyUtils.decodePublicKey(entity.getEncodedPublicKey())));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteMasterKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                PGPPublicKey masterKey = addPublicKeys(dao, keys);

                assertEquals(2, dao.getSize());

                // Deleting the master should also result in the deletion of the subkey
                deleteMasterKey(dao, masterKey);

                assertEquals(0, dao.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteSubKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                PGPPublicKey masterKey = addPublicKeys(dao, keys);

                assertEquals(2, dao.getSize());

                PGPPublicKey subKey = PGPKeyUtils.decodePublicKey(dao.getBySHA256Fingerprint(
                        "ADE17128BDA503F8069AC2FFBCA17F25BA09A85431DAEE15B4567134B843BB35").getEncodedPublicKey());

                // Deleting the sub key should not result in the deletion of the master key
                deleteSubKey(dao, masterKey, subKey);

                assertEquals(1, dao.getSize());

                assertTrue(dao.getByPublicKey(masterKey).isMasterKey());
                assertNull(dao.getByPublicKey(subKey));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetByParentKeyID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.asc"));

                PGPPublicKey masterKey = addPublicKeys(dao, keys);

                assertEquals(6, dao.getSize());

                List<PGPKeyRingEntity> allSubKeys = CloseableIteratorUtils.toList(dao.getByParentKeyID(
                        masterKey.getKeyID(), null, null));

                assertEquals(5, allSubKeys.size());

                for (PGPKeyRingEntity entity : allSubKeys) {
                    assertFalse(entity.isMasterKey());
                }

                assertEquals(5, dao.getByParentKeyIDCount(masterKey.getKeyID()));

                // Now with first and max
                List<PGPKeyRingEntity> someSubKeys = CloseableIteratorUtils.toList(dao.getByParentKeyID(
                        masterKey.getKeyID(), 0, 2));

                assertEquals(2, someSubKeys.size());

                for (int i = 0; i < someSubKeys.size(); i++) {
                    assertEquals(allSubKeys.get(i), someSubKeys.get(i));
                }

                // Now with first and max
                int start = 2;

                someSubKeys = CloseableIteratorUtils.toList(dao.getByParentKeyID(masterKey.getKeyID(), start,
                        1000));

                assertEquals(3, someSubKeys.size());

                for (int i = 0; i < someSubKeys.size(); i++) {
                    assertEquals(allSubKeys.get(i + start), someSubKeys.get(i));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                for (PGPPublicKey key : keys) {
                    addPublicKey(dao, key);
                }

                assertEquals(2, dao.getSize());

                setSecretKeyAlias(dao, keys.get(0), "alias");

                List<PGPPublicKey> otherKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                for (PGPPublicKey key : otherKeys) {
                    addPublicKey(dao, key);
                }

                setSecretKeyAlias(dao, otherKeys.get(0), "alias");

                assertEquals(4, dao.getSize());

                Date now = TestUtils.parseDate("09-Aug-2022 07:38:35 GMT");

                // All
                assertEquals(4, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        now));

                // skip expired but not expired
                assertEquals(4, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), now));

                // skip expired
                assertEquals(2, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2031 00:00:00 CET")));

                // skip without key alias
                assertEquals(2, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED,
                                PGPKeyType.ALL), now));

                // Only master keys
                assertEquals(2, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), now));

                // Only sub keys
                assertEquals(2, dao.getCount(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.SUB_KEY), now));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testgetPGPKeyRingEntityIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                addPublicKeys(dao, keys);

                assertEquals(2, dao.getSize());

                setSecretKeyAlias(dao, keys.get(0), "alias");

                List<PGPPublicKey> otherKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                addPublicKeys(dao, otherKeys);

                assertEquals(4, dao.getSize());

                setSecretKeyAlias(dao, otherKeys.get(0), "alias");

                Date now = TestUtils.parseDate("09-Aug-2022 07:38:35 GMT");

                // All keys
                List<PGPKeyRingEntity> entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        now, null, null));

                assertEquals(4, entities.size());

                assertEquals("D7EE5B33E871B92F", PGPUtils.getKeyIDHex(entities.get(0).getKeyID()));
                assertEquals("BA8FB3A1F6A0582B", PGPUtils.getKeyIDHex(entities.get(1).getKeyID()));
                assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(entities.get(2).getKeyID()));
                assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(entities.get(3).getKeyID()));

                // Skip expired
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2031 00:00:00 CET"), null, null));

                assertEquals(2, entities.size());

                // Expired only
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_EXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), TestUtils.parseDate("31-Dec-2077 00:00:00 CET"), null, null));

                assertEquals(2, entities.size());

                // Skip without a private key alias
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED, PGPKeyType.ALL),
                        now, null, null));

                assertEquals(2, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(keys.get(0), KEY_RING_NAME)));
                assertTrue(entities.contains(new PGPKeyRingEntity(otherKeys.get(0), KEY_RING_NAME)));
                assertFalse(entities.contains(new PGPKeyRingEntity(otherKeys.get(0), "other ring name")));

                // Only the master keys
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), now, null, null));

                assertEquals(2, entities.size());

                for (PGPKeyRingEntity entity : entities) {
                    assertTrue(entity.isMasterKey());
                }

                // Only the sub keys
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.SUB_KEY),
                        now, null, null));

                assertEquals(2, entities.size());

                for (PGPKeyRingEntity entity : entities) {
                    assertFalse(entity.isMasterKey());
                }

                // first result and max
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        now, 1, 2));

                assertEquals(2, entities.size());
                assertTrue(entities.contains(new PGPKeyRingEntity(keys.get(1), KEY_RING_NAME)));
                assertTrue(entities.contains(new PGPKeyRingEntity(otherKeys.get(0), KEY_RING_NAME)));

                // first result and max null
                entities = CloseableIteratorUtils.toList(dao.getIterator(
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        now, 1, null));

                assertEquals(3, entities.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetAllKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntityDAO dao = createDAO();
                PGPKeyRingEntityDAO otherDAO = createDAO("other-keyring");

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com-expiration-2030-12-31.gpg.asc"));

                for (PGPPublicKey key : keys) {
                    addPublicKey(dao, key);
                }

                assertEquals(2, dao.getSize());

                setSecretKeyAlias(dao, keys.get(0), "alias");

                List<PGPPublicKey> otherKeys = PGPKeyUtils.readPublicKeys(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"));

                for (PGPPublicKey key : otherKeys) {
                    addPublicKey(otherDAO, key);
                }

                // All keys
                List<PGPKeyRingEntity> entities = CloseableIteratorUtils.toList(PGPKeyRingEntityDAO.getAllKeys(
                        dao.getSession(), null));

                assertEquals(4, entities.size());

                // Only default keyring
                entities = CloseableIteratorUtils.toList(PGPKeyRingEntityDAO.getAllKeys(dao.getSession(),
                        KEY_RING_NAME));

                assertEquals(2, entities.size());

                // Only default keyring2
                entities = CloseableIteratorUtils.toList(PGPKeyRingEntityDAO.getAllKeys(otherDAO.getSession(),
                        otherDAO.getKeyRingName()));

                assertEquals(2, entities.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
