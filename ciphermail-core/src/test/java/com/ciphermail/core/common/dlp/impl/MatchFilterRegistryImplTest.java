/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.impl.matchfilter.MaskingFilter;
import com.ciphermail.core.common.dlp.impl.matchfilter.RegExpMaskingFilter;


public class MatchFilterRegistryImplTest
{
    @Test
    public void testRegistry()
    {
        List<MatchFilter> filters = new LinkedList<>();

        filters.add(new MaskingFilter());
        filters.add(new RegExpMaskingFilter("regexp", "descr.", "", ""));

        MatchFilterRegistry registry = new MatchFilterRegistryImpl(filters);

        assertEquals(2, registry.getMatchFilters().size());

        MatchFilter filter = registry.getMatchFilter("MasK");

        assertNotNull(filter);
        assertEquals("Mask", filter.getName());
        assertNotNull(filter.getDescription());

        filter = registry.getMatchFilter("REgEXP");

        assertNotNull(filter);
        assertEquals("regexp", filter.getName());
        assertEquals("descr.", filter.getDescription());

        assertNull(registry.getMatchFilter(null));
    }

    @Test
    public void testReplaceExisting()
    {
        List<MatchFilter> filters = new LinkedList<>();

        filters.add(new RegExpMaskingFilter("filter1", "desc 1", "", ""));

        MatchFilterRegistry registry = new MatchFilterRegistryImpl(filters);

        assertEquals(1, registry.getMatchFilters().size());

        MatchFilter filter = registry.getMatchFilter("filter1");

        assertNotNull(filter);
        assertEquals("desc 1", filter.getDescription());

        registry.addMatchFilter(new RegExpMaskingFilter("filter1", "desc X", "", ""));

        filter = registry.getMatchFilter("filter1");

        assertNotNull(filter);
        assertEquals("desc X", filter.getDescription());
    }
}
