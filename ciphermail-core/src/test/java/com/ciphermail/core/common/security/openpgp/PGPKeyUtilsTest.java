/*
 * Copyright (c) 2013-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PGPKeyUtilsTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testDecode()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        for (PGPPublicKey publicKey : keys)
        {
            byte[] encoded = publicKey.getEncoded();

            PGPPublicKey decoded = PGPKeyUtils.decodePublicKey(encoded);

            assertEquals(publicKey.isMasterKey(), decoded.isMasterKey());

            assertArrayEquals(encoded, decoded.getEncoded());
        }
    }

    @Test
    public void testReadSecretKeys()
    throws Exception
    {
        List<PGPSecretKey> keys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "test-multiple-sub-keys.gpg.key"));

        assertEquals(6, keys.size());
    }

    @Test
    public void testEquals()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        assertEquals(2, keys.size());

        assertFalse(PGPKeyUtils.isEquals(keys.get(0), keys.get(1)));
        assertFalse(PGPKeyUtils.isEquals(keys.get(1), keys.get(0)));
        assertFalse(PGPKeyUtils.isEquals(keys.get(0), null));
        assertFalse(PGPKeyUtils.isEquals(null, keys.get(0)));
        assertTrue(PGPKeyUtils.isEquals(keys.get(0), keys.get(0)));
        assertTrue(PGPKeyUtils.isEquals(keys.get(1), keys.get(1)));
        assertTrue(PGPKeyUtils.isEquals(null, null));
    }

    @Test
    public void testGetPublicKeys()
    throws Exception
    {
        PGPPublicKeyRing ring = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(
                new File(TEST_BASE, "test@example.com.gpg.asc"))),
                new JcaKeyFingerprintCalculator());

        List<PGPPublicKey> keys = PGPKeyUtils.getPublicKeys(ring);

        assertEquals(2, keys.size());
        assertEquals("4EC4E8813E11A9A0", PGPUtils.getKeyIDHex(keys.get(0).getKeyID()));
        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(keys.get(1).getKeyID()));
    }

    private void testGetAlgorithmFriendly(String pgpKeyFile, String... expectedFriendlyName)
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, pgpKeyFile));

        assertTrue(keys.size() > 0);

        for (int i = 0; i < keys.size(); i++)
        {
            PGPPublicKey key = keys.get(i);

            assertEquals(expectedFriendlyName[i], PGPKeyUtils.getAlgorithmFriendly(key));
        }
    }

    @Test
    public void testGetAlgorithmFriendly()
    throws Exception
    {
        testGetAlgorithmFriendly("rsa-1024@example.com.asc", "RSA", "RSA");
        testGetAlgorithmFriendly("rsa-2048@example.com.asc", "RSA", "RSA");
        testGetAlgorithmFriendly("rsa-3072@example.com.asc", "RSA", "RSA");
        testGetAlgorithmFriendly("rsa-4096@example.com.asc", "RSA", "RSA");
        testGetAlgorithmFriendly("curve-25519@example.com.asc", "EDDSA:Ed25519", "ECDH:curve25519");
        testGetAlgorithmFriendly("nist-p-256@example.com.asc", "ECDSA:prime256v1", "ECDH:prime256v1");
        testGetAlgorithmFriendly("nist-p-384@example.com.asc", "ECDSA:secp384r1", "ECDH:secp384r1");
        testGetAlgorithmFriendly("nist-p-521@example.com.asc", "ECDSA:secp521r1", "ECDH:secp521r1");
        testGetAlgorithmFriendly("brainpool-p-256@example.com.asc", "ECDSA:brainpoolP256r1", "ECDH:brainpoolP256r1");
        testGetAlgorithmFriendly("brainpool-p-384@example.com.asc", "ECDSA:brainpoolP384r1", "ECDH:brainpoolP384r1");
        testGetAlgorithmFriendly("brainpool-p-512@example.com.asc", "ECDSA:brainpoolP512r1", "ECDH:brainpoolP512r1");
        testGetAlgorithmFriendly("secp256k1@example.com.asc", "ECDSA:secp256k1", "ECDH:secp256k1");
    }
}
