/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.jce;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.crlstore.hibernate.X509CRLStoreExtHibernate;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.CRL;
import java.security.cert.CertStore;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CertStoreCRLProviderTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "crls/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private CertStore certStore;
    private CertStore otherCertStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters otherCertStoreParams;

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(new X509CRLStoreExtHibernate("crls", sessionManager));

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        otherCertStoreParams = new X509CertStoreParameters(new X509CRLStoreExtHibernate("otherCRLs", sessionManager));

        otherCertStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, otherCertStoreParams,
                CipherMailProvider.PROVIDER);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCRLStore().removeAllEntries();
                otherCertStoreParams.getCRLStore().removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCRL("intel-basic-enterprise-issuing-CA.crl", certStoreParams.getCRLStore());
                addCRL("test-ca-no-next-update.crl", certStoreParams.getCRLStore());
                addCRL("itrus.com.cn.crl", certStoreParams.getCRLStore());
                addCRL("itrus.com.cn.crl", otherCertStoreParams.getCRLStore());
                addCRL("test-ca.crl", otherCertStoreParams.getCRLStore());
                addCRL("ThawteSGCCA.crl", otherCertStoreParams.getCRLStore());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCRL(String filename, X509CRLStoreExt crlStore)
    throws Exception
    {
        crlStore.addCRL(TestUtils.loadX509CRL(new File(TEST_BASE, filename)));
    }

    @Test
    public void testContains()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File crlFile = new File(TEST_BASE, "itrus.com.cn.crl");

                X509CRL crl = TestUtils.loadX509CRL(crlFile);
                assertNotNull(crl);

                assertTrue(certStoreParams.getCRLStore().contains(crl));

                crlFile = new File(TEST_BASE, "test-ca.crl");

                crl = TestUtils.loadX509CRL(crlFile);
                assertNotNull(crl);

                assertFalse(certStoreParams.getCRLStore().contains(crl));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDuplicate()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    addCRL("intel-basic-enterprise-issuing-CA.crl", certStoreParams.getCRLStore());
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch(DataIntegrityViolationException e) {
            // Expected
        }
    }

    @Test
    public void testGetCRLDate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                Date now = TestUtils.parseDate("28-Nov-2007 11:38:35 GMT");

                selector.setDateAndTime(now);

                Collection<? extends CRL> crls = certStore.getCRLs(selector);
                assertEquals(1, crls.size());

                crls = otherCertStore.getCRLs(selector);
                assertEquals(2, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLNullDate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                Date now = TestUtils.parseDate("30-Nov-2026 11:38:35 GMT");

                selector.setDateAndTime(now);

                Collection<? extends CRL> crls = certStore.getCRLs(selector);
                // should be 0 because the crl did not have a next date
                assertEquals(0, crls.size());

                crls = otherCertStore.getCRLs(selector);
                assertEquals(1, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    @Test
    public void testGetAllCRLs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                Collection<? extends CRL> crls = certStore.getCRLs(selector);
                assertEquals(3, crls.size());

                crls = otherCertStore.getCRLs(selector);
                assertEquals(3, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLIssuer()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                selector.addIssuer(new X500Principal(
                        "CN=Intel Corporation Basic Enterprise Issuing CA 1, " +
                             "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " +
                             "L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com"));

                Collection<? extends CRL> crls = certStore.getCRLs(selector);
                assertEquals(1, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLMultipleIssuers()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                selector.addIssuer(new X500Principal(
                        "CN=Intel Corporation Basic Enterprise Issuing CA 1, " +
                        "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " +
                        "L=Folsom, ST=CA, C=US, EMAILADDRESS=pki@intel.com"));

                selector.addIssuer(new X500Principal(
                        "CN=Thawte SGC CA, O=Thawte Consulting (Pty) Ltd., C=ZA"));

                selector.addIssuer(new X500Principal(
                        "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

                Collection<? extends CRL> crls = otherCertStore.getCRLs(selector);
                assertEquals(2, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCRLNoMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRLSelector selector = new X509CRLSelector();

                selector.addIssuer(new X500Principal(
                        "CN=Intel Corporation Basic Enterprise Issuing CA 1, " +
                        "OU=Information Technology Enterprise Business Computing, O=Intel Corporation, " +
                        "L=Folsom, ST=CA, C=US"));

                Collection<? extends CRL> crls = certStore.getCRLs(selector);
                assertEquals(0, crls.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
