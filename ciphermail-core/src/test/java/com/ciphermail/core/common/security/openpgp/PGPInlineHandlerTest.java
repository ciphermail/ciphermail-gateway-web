/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.MutableString;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPInlineHandlerTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static PGPKeyPairProvider keyPairProvider;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "test@example.com.gpg.key"))));

        keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider("test"));
    }

    @Test
    public void testEncryptedTextWithExtraTest()
    throws Exception
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "pgp-encrypted-with-extra.txt.asc"));

        File outputFile = TestUtils.createTempFile(".txt");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            assertTrue(handler.handle(input, new MutableString("US-ASCII"), output));
        }
        finally {
            output.close();
        }

        assertTrue(handler.isMixedContent());

        String result = FileUtils.readFileToString(outputFile, StandardCharsets.UTF_8);

        String expected =
                """
                \r
                some line in between\r
                \r
                some line before\r
                test\r
                \r
                -- \r
                DJIGZO email encryption\r
                some line after\r
                \r
                """;

        assertEquals(expected, result);

        List<PGPLayer> pgpLayers = handler.getPGPLayers();

        // The message should have an encrypted, compressed and literal data layer
        assertEquals(3, pgpLayers.size());

        // First layer
        PGPLayer layerInfo = pgpLayers.get(0);
        assertEquals(1, layerInfo.getParts().size());
        PGPLayerPart layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPEncryptionLayerPart);

        PGPEncryptionLayerPart encryptionLayer = (PGPEncryptionLayerPart) layer;

        assertEquals("Charset: ISO-8859-1,Version: GnuPG v1.4.11 (GNU/Linux),Comment: Using GnuPG with Thunderbird - " +
                "http://www.enigmail.net/", StringUtils.join(layer.getArmorHeaders(), ","));
        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(encryptionLayer.getPublicKeyEncryptedData().getKeyID()));
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().isIntegrityProtected());
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().verify());

        // Second layer
        layerInfo = pgpLayers.get(1);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPCompressionLayerPart);

        PGPCompressionLayerPart compressionLayer = (PGPCompressionLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals(PGPCompressionAlgorithm.ZLIB, PGPCompressionAlgorithm.fromTag(
                compressionLayer.getCompressedData().getAlgorithm()));

        // Third layer
        layerInfo = pgpLayers.get(2);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPLiteralLayerPart);

        PGPLiteralLayerPart literalInfo = (PGPLiteralLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals("", literalInfo.getPGPLiteralData().getFileName());
        assertEquals(PGPLiteralPacketType.TEXT, PGPLiteralPacketType.fromTag(
                literalInfo.getPGPLiteralData().getFormat()));
    }

    @Test
    public void testNoInlinePGP()
    throws Exception
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "../documents/test.txt"));

        File outputFile = TestUtils.createTempFile(".txt");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            assertFalse(handler.handle(input, new MutableString("US-ASCII"), output));
        }
        finally {
            output.close();
        }
    }

    @Test
    public void testMultipleInlineMessages()
    throws Exception
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "pgp-encrypted-multiple-messages.txt.asc"));

        File outputFile = TestUtils.createTempFile(".txt");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            assertTrue(handler.handle(input, new MutableString("US-ASCII"), output));
        }
        finally {
            output.close();
        }

        assertFalse(handler.isMixedContent());

        String result = FileUtils.readFileToString(outputFile, StandardCharsets.UTF_8);

        String expected =
                """
                test\r
                \r
                -- \r
                DJIGZO email encryption\r
                \r
                test\r
                \r
                -- \r
                DJIGZO email encryption\r
                \r
                """;

        assertEquals(expected, result);
    }

    /*
     * Test for https://jira.djigzo.com/browse/GATEWAY-91 (i.e., PGP blob starts with ws)
     */
    @Test
    public void testGATEWAY91NotEnabled()
    throws Exception
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "pgp-encrypted-GATEWAY-91.txt.asc"));

        File outputFile = TestUtils.createTempFile(".txt");

        FileOutputStream output = new FileOutputStream(outputFile);

        assertFalse(handler.handle(input, new MutableString("US-ASCII"), output));
    }

    /*
     * Test for https://jira.djigzo.com/browse/GATEWAY-91 (i.e., PGP blob starts with ws)
     */
    @Test
    public void testGATEWAY91Enabled()
    throws Exception
    {
        PGPInlineHandler handler = new PGPInlineHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "pgp-encrypted-GATEWAY-91.txt.asc"));

        File outputFile = TestUtils.createTempFile(".txt");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            PGPInlineHandler.setFullStripHeaders(true);

            assertTrue(handler.handle(input, new MutableString("US-ASCII"), output));
        }
        finally {
            PGPInlineHandler.setFullStripHeaders(false);

            output.close();
        }

        assertFalse(handler.isMixedContent());

        String result = FileUtils.readFileToString(outputFile, StandardCharsets.UTF_8);

        String expected =
                """
                test\r
                \r
                -- \r
                DJIGZO email encryption\r
                \r
                """;

        assertEquals(expected, result);
    }
}
