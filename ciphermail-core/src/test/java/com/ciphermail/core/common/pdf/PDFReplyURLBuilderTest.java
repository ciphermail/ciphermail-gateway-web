/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class PDFReplyURLBuilderTest
{
    @Test
    public void testUnicode()
    throws Exception
    {
        final String key = "abc";
        final String subject = "Größe";

        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance()
                .setBaseURL("http://www.example.com")
                .setUser("user@example.com")
                .setFrom("test@example.com")
                .setRecipient("test@recipient.example.com")
                .setSubject(subject)
                .setKey(key)
                .setTime(8765L)
                .setNonce("123");

        String url = builder.buildURL();

        assertNotNull(url);

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> key;

        String envelope = StringUtils.substringBetween(url, "env=", "&hmac=");
        String hmac = StringUtils.substringAfter(url, "hmac=");

        System.out.println(url);
        System.out.println(envelope);
        System.out.println(hmac);

        builder = PDFReplyURLBuilder.createInstance();
        builder.loadFromEnvelope(envelope, hmac, keyProvider);

        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals(subject, builder.getSubject());
    }

    @Test
    public void testPDFReplyURLBuilder()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance()
                .setBaseURL("http://www.example.com")
                .setUser("user@example.com")
                .setFrom("test@example.com")
                .setRecipient("test@recipient.example.com")
                .setSubject("test")
                .setKey("1234");

        String url = builder.buildURL();

        assertNotNull(url);
    }

    @Test
    public void testPDFReplyURLBuilderVerify()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> "1234";

        builder.loadFromEnvelope("eyJpZCI6InNvbWUgaWQiLCJmIjoidGVzdEBleGFtcGxlLmNvbSIsInUiOiJ1c2VyQGV4YW1wbGUuY29tIi" +
        		"widCI6ODc2NSwicyI6InRlc3QiLCJyIjoidGVzdEByZWNpcGllbnQuZXhhbXBsZS5jb20ifQ==",
                "azfl35nzidcqdjr6wgm3fedlfcq5tom4", keyProvider);

        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals(8765L, (long) builder.getTime());
        assertEquals("some id", builder.getNonce());
        assertEquals("test", builder.getSubject());
    }

    @Test(expected = PDFURLBuilderException.class)
    public void testPDFReplyURLBuilderIncorrectMac()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> "1234";

        builder.loadFromEnvelope("eyJpZCI6InNvbWUgaWQiLCJmIjoidGVzdEBleGFtcGxlLmNvbSIsInUiOiJ1c2VyQGV4YW1wbGUuY29tIi" +
        		"widCI6ODc2NSwicyI6InRlc3QiLCJyIjoidGVzdEByZWNpcGllbnQuZXhhbXBsZS5jb20ifQ==",
                "abc", keyProvider);
    }

    @Test(expected = PDFURLBuilderException.class)
    public void testPDFReplyURLBuilderIncorrectBase64()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> "";

        builder.loadFromEnvelope("x", "xx", keyProvider);
    }

    @Test
    public void testPDFReplyURLBuilderWithMessageID()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        builder.setBaseURL("http://www.example.com")
            .setUser("user@example.com")
            .setFrom("test@example.com")
            .setRecipient("test@recipient.example.com")
            .setSubject("test")
            .setMessageID("abcdef")
            .setKey("1234");

        String url = builder.buildURL();

        assertNotNull(url);

        System.out.println(url);
    }

    @Test
    public void testPDFReplyURLBuilderVerifyWithMessageID()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> "1234";

        builder.loadFromEnvelope("eyJpZCI6ImJsb280ZXl0NXNiNXkiLCJmIjoidGVzdEBleGFtcGxlLmNvbSIsInUiOiJ1c2VyQGV4YW1wb" +
        		"GUuY29tIiwidCI6MTM5NDAzNzgzNjYwMiwicyI6InRlc3QiLCJyIjoidGVzdEByZWNpcGllbnQuZXhhbXBsZS5jb20iLCJtaWQ" +
        		"iOiJhYmNkZWYifQ==", "vkar7bla6bp6r7tvsnwawrx2fljw4ywa", keyProvider);

        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals(1394037836602L, (long) builder.getTime());
        assertEquals("bloo4eyt5sb5y", builder.getNonce());
        assertEquals("test", builder.getSubject());
    }

    @Test
    public void testMessageIDTooLong()
    throws Exception
    {
        PDFReplyURLBuilder builder = PDFReplyURLBuilder.createInstance();

        builder.setBaseURL("http://www.example.com")
            .setUser("user@example.com")
            .setFrom("test@example.com")
            .setRecipient("test@recipient.example.com")
            .setSubject("subject")
            .setKey("abc")
            .setTime(8765L)
            .setNonce("123")
            .setMessageID(StringUtils.repeat("a", 10));

        String url = builder.buildURL();

        assertNotNull(url);

        PDFReplyURLBuilder.KeyProvider keyProvider = b -> "abc";

        String envelope = StringUtils.substringBetween(url, "env=", "&hmac=");
        String hmac = StringUtils.substringAfter(url, "hmac=");

        System.out.println(url);
        System.out.println(envelope);
        System.out.println(hmac);

        builder = PDFReplyURLBuilder.createInstance();
        builder.loadFromEnvelope(URLDecoder.decode(envelope, StandardCharsets.UTF_8), hmac, keyProvider);

        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals("subject", builder.getSubject());
        assertEquals("aaaaaaaaaa", builder.getMessageID());

        // Now repeat but with too long message id
        builder = PDFReplyURLBuilder.createInstance();

        builder.setBaseURL("http://www.example.com")
            .setUser("user@example.com")
            .setFrom("test@example.com")
            .setRecipient("test@recipient.example.com")
            .setSubject("subject")
            .setKey("abc")
            .setTime(8765L)
            .setNonce("123")
            .setMessageID(StringUtils.repeat("a", 1800));

        url = builder.buildURL();

        assertNotNull(url);

        envelope = StringUtils.substringBetween(url, "env=", "&hmac=");
        hmac = StringUtils.substringAfter(url, "hmac=");

        System.out.println(url);
        System.out.println(envelope);
        System.out.println(hmac);

        builder = PDFReplyURLBuilder.createInstance();
        builder.loadFromEnvelope(URLDecoder.decode(envelope, StandardCharsets.UTF_8), hmac, keyProvider);

        assertEquals("user@example.com", builder.getUser());
        assertEquals("test@example.com", builder.getFrom());
        assertEquals("test@recipient.example.com", builder.getRecipient());
        assertEquals("subject", builder.getSubject());
        assertNull(builder.getMessageID());
    }
}
