/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class NamedBlobDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        createDAO().deleteAll();
    }

    private NamedBlobDAO createDAO() {
        return NamedBlobDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Test
    public void testGetNamedBlob()
    {
        NamedBlobDAO dao = createDAO();

        NamedBlobEntity entity = dao.getNamedBlob("cat1", "name1");

        assertNull(entity);

        entity = new NamedBlobEntity("cat1", "name2");
        dao.persist(entity);
        assertNull(dao.getNamedBlob("cat1", "name1"));

        entity = new NamedBlobEntity("cat2", "name1");
        dao.persist(entity);
        assertNull(dao.getNamedBlob("cat1", "name1"));

        entity = new NamedBlobEntity("cat1", "name1");
        dao.persist(entity);

        entity = dao.getNamedBlob("cat1", "name1");

        assertNotNull(entity);
        assertEquals("cat1", entity.getCategory());
        assertEquals("name1", entity.getName());
    }

    @Test
    public void testGetByCategory()
    {
        NamedBlobDAO dao = createDAO();

        List<NamedBlobEntity> entities = dao.getByCategory("cat1", null, null);

        assertEquals(0, entities.size());

        NamedBlobEntity entity = new NamedBlobEntity("cat1", "name2");
        dao.persist(entity);

        entity = new NamedBlobEntity("cat1", "name1");
        dao.persist(entity);

        entity = new NamedBlobEntity("cat2", "name1");
        dao.persist(entity);

        entities = dao.getByCategory("cat1", null, null);

        assertEquals(2, entities.size());
        assertEquals("name1", entities.get(0).getName());
        assertEquals("name2", entities.get(1).getName());

        entities = dao.getByCategory("cat1", 0, 1);
        assertEquals(1, entities.size());
        assertEquals("name1", entities.get(0).getName());

        entities = dao.getByCategory("cat1", 1, null);
        assertEquals(1, entities.size());
        assertEquals("name2", entities.get(0).getName());

        entities = dao.getByCategory("cat2", null, null);
        assertEquals(1, entities.size());
    }

    @Test
    public void testGetByCategoryCount()
    {
        NamedBlobDAO dao = createDAO();

        assertEquals(0, dao.getByCategoryCount("cat1"));

        NamedBlobEntity entity = new NamedBlobEntity("cat1", "name1");
        dao.persist(entity);

        entity = new NamedBlobEntity("cat1", "name2");
        dao.persist(entity);

        entity = new NamedBlobEntity("cat2", "name1");
        dao.persist(entity);

        assertEquals(2, dao.getByCategoryCount("cat1"));
        assertEquals(1, dao.getByCategoryCount("cat2"));
    }

    @Test
    public void testGetReferencedByCount()
    {
        NamedBlobDAO dao = createDAO();

        NamedBlobEntity entity1 = new NamedBlobEntity("cat1", "name1");
        dao.persist(entity1);

        NamedBlobEntity entity2 = new NamedBlobEntity("cat1", "name2");
        dao.persist(entity2);

        NamedBlobEntity entity3 = new NamedBlobEntity("cat2", "name1");
        dao.persist(entity3);

        NamedBlobEntity entity4 = new NamedBlobEntity("cat2", "name2");
        dao.persist(entity4);

        entity1.getNamedBlobs().add(entity2);
        entity1.getNamedBlobs().add(entity3);

        entity4.getNamedBlobs().add(entity1);
        entity4.getNamedBlobs().add(entity2);

        assertEquals(2, dao.getReferencedByCount(entity2));
        assertEquals(1, dao.getReferencedByCount(entity3));
        assertEquals(1, dao.getReferencedByCount(entity1));
        assertEquals(0, dao.getReferencedByCount(entity4));
    }

    @Test
    public void testGetReferencedBy()
    {
        NamedBlobDAO dao = createDAO();

        NamedBlobEntity entity1 = new NamedBlobEntity("cat1", "name1");
        dao.persist(entity1);

        NamedBlobEntity entity2 = new NamedBlobEntity("cat1", "name2");
        dao.persist(entity2);

        NamedBlobEntity entity3 = new NamedBlobEntity("cat2", "name1");
        dao.persist(entity3);

        NamedBlobEntity entity4 = new NamedBlobEntity("cat2", "name2");
        dao.persist(entity4);

        entity1.getNamedBlobs().add(entity2);
        entity1.getNamedBlobs().add(entity3);

        entity4.getNamedBlobs().add(entity1);
        entity4.getNamedBlobs().add(entity2);

        Set<NamedBlobEntity> entities;

        entities = new HashSet<>(dao.getReferencedBy(entity2, null, null));
        assertEquals(2, entities.size());
        assertTrue(entities.contains(entity1));
        assertTrue(entities.contains(entity4));

        entities = new HashSet<>(dao.getReferencedBy(entity2, 1, null));
        assertEquals(1, entities.size());

        entities = new HashSet<>(dao.getReferencedBy(entity2, 0, 1));
        assertEquals(1, entities.size());

        entities = new HashSet<>(dao.getReferencedBy(entity3, null, null));
        assertEquals(1, entities.size());
        assertTrue(entities.contains(entity1));

        entities = new HashSet<>(dao.getReferencedBy(entity1, null, null));
        assertEquals(1, entities.size());
        assertTrue(entities.contains(entity4));

        entities = new HashSet<>(dao.getReferencedBy(entity4, null, null));
        assertEquals(0, entities.size());
    }

    @Test
    public void testDeleteNamedBlob()
    {
        NamedBlobDAO dao = createDAO();

        assertEquals(0, dao.findAll(NamedBlobEntity.class).size());

        dao.persist(new NamedBlobEntity("cat1", "name1"));
        dao.persist(new NamedBlobEntity("cat1", "name2"));
        dao.persist(new NamedBlobEntity("cat2", "name1"));

        assertNotNull(dao.getNamedBlob("cat1", "name1"));

        dao.deleteNamedBlob("cat1", "name1");

        assertNull(dao.getNamedBlob("cat1", "name1"));
        assertNotNull(dao.getNamedBlob("cat1", "name2"));
        assertNotNull(dao.getNamedBlob("cat2", "name1"));

        /*
         * Deleting non-existent entity should not fail
         */
        dao.deleteNamedBlob("cat1", "name1");
    }

    @Test
    public void testDeleteAll()
    {
        NamedBlobDAO dao = createDAO();

        assertEquals(0, dao.findAll(NamedBlobEntity.class).size());

        dao.persist(new NamedBlobEntity("cat1", "name1"));
        dao.persist(new NamedBlobEntity("cat1", "name2"));
        dao.persist(new NamedBlobEntity("cat2", "name1"));

        assertEquals(3, dao.findAll(NamedBlobEntity.class).size());

        dao.deleteAll();

        assertEquals(0, dao.findAll(NamedBlobEntity.class).size());

        /*
         * Deleting non-existent entities should not fail
         */
        assertEquals(0, dao.findAll(NamedBlobEntity.class).size());
    }

    @Test
    public void testGetNamedBlobIterator()
    throws CloseableIteratorException
    {
        NamedBlobDAO dao = createDAO();

        dao.persist(new NamedBlobEntity("cat1", "name1"));
        dao.persist(new NamedBlobEntity("cat1", "name2"));
        dao.persist(new NamedBlobEntity("cat2", "name1"));

        List<NamedBlobEntity> entities = CloseableIteratorUtils.toList(dao.getNamedBlobIterator());

        assertEquals(3, entities.size());
    }
}
