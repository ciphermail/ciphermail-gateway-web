/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.cert.CRL;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLEntry;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class X509CRLInspectorTest
{
    @Test
    public void testDeltaCRL()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "PKITS/crls/deltaCRLCA1deltaCRL.crl");

        X509CRL crl = TestUtils.loadX509CRL(file);

        assertNotNull(crl);

        assertEquals(1L, X509CRLInspector.getDeltaIndicator(crl).longValue());
    }

    @Test
    public void testIssuingDistributionPoint()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "PKITS/crls/indirectCRLCA1CRL.crl");

        X509CRL crl = TestUtils.loadX509CRL(file);

        assertNotNull(crl);

        IssuingDistributionPoint idp = X509CRLInspector.getIssuingDistributionPoint(crl);

        assertTrue(idp.isIndirectCRL());
    }

    @Test
    public void testIssuingDistributionPoint2()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "PKITS/crls/distributionPoint1CACRL.crl");

        X509CRL crl = TestUtils.loadX509CRL(file);

        assertNotNull(crl);

        IssuingDistributionPoint idp = X509CRLInspector.getIssuingDistributionPoint(crl);

        assertFalse(idp.isIndirectCRL());

        DistributionPointName dp = idp.getDistributionPoint();

        GeneralName[] genNames = GeneralNames.getInstance(dp.getName()).getNames();

        assertEquals(1, genNames.length);
        assertEquals(DistributionPointName.FULL_NAME, dp.getType());
    }

    @Test(timeout = 40000)
    public void testLargeCRL()
    {
        File file = new File(TestUtils.getTestDataDir(), "crls/dod-large.crl");

        X509CRL crl;

        final int tries = 5;

        long start = System.currentTimeMillis();

        for (int i = 0; i < tries; i++)
        {
           crl = TestUtils.loadX509CRL(file);

           System.out.println("Issuer: " + crl.getIssuerX500Principal());

           assertNull(crl.getRevokedCertificate(new BigInteger("1")));
        }

        double timeSpent = (System.currentTimeMillis() - start) * 0.001 / tries;

        System.out.println("Seconds / check: " + timeSpent);

        // should be faster than 5 seconds on a fast machine
        assertTrue(timeSpent < 5);
    }

    @Test(timeout = 30000)
    public void testLargeCRLIsRevoked()
    {
        final int tries = 20;

        X509CRL crl = TestUtils.loadX509CRL(new File(TestUtils.getTestDataDir(), "crls/dod-large.crl"));

        X509Certificate certificate = TestUtils.loadCertificate(new File("src/test/resources/testdata/certificates" +
                "/dod-mega-crl.cer"));

        long start = System.currentTimeMillis();

        for (int i = 0; i < tries; i++) {
            assertFalse(crl.isRevoked(certificate));
        }

        double timePerCheck = (System.currentTimeMillis() - start) * 0.001 / tries;

        System.out.println("Seconds / check: " + timePerCheck);

        // should be faster than 1 second on a fast machine
        assertTrue(timePerCheck < 1);
    }

    @Test(timeout = 60000)
    public void testLargeCRLEncode()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "crls/mitm-ca-large.crl.gz");

        X509CRL crl = TestUtils.loadX509CRL(new GZIPInputStream(
                new FileInputStream(file)));

        System.out.println("Issuer: " + crl.getIssuerX500Principal());

        assertNotNull(crl.getEncoded());

        Collection<? extends CRL> crls = CRLUtils.readCRLs(new ByteArrayInputStream(crl.getEncoded()));

        assertEquals(1, crls.size());

        assertEquals(crl, crls.iterator().next());
    }

    @Test
    public void testRevoked()
    {
        File file = new File(TestUtils.getTestDataDir(), "crls/test-ca.crl");

        X509CRL crl = TestUtils.loadX509CRL(file);

        assertNotNull(crl);

        Set<? extends X509CRLEntry> revoked = crl.getRevokedCertificates();

        assertEquals(1, revoked.size());
    }

    @Test
    public void testCRLNumber()
    throws Exception
    {
        File file = new File(TestUtils.getTestDataDir(), "crls/intel-basic-enterprise-issuing-CA.crl");

        X509CRL crl = TestUtils.loadX509CRL(file);

        assertNotNull(crl);

        BigInteger crlNumber = X509CRLInspector.getCRLNumber(crl);

        assertEquals("E3", BigIntegerUtils.hexEncode(crlNumber));
    }
}
