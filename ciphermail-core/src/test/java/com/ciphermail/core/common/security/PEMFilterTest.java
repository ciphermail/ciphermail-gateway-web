/*
 * Copyright (c) 2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import org.apache.commons.lang.text.StrBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class PEMFilterTest
{
    private static final String NL = System.lineSeparator();

    private static final String CERT_PEM =
            """
            -----BEGIN CERTIFICATE-----
            invalid pem content. just for testing
            invalid pem content. just for testing
            -----END CERTIFICATE-----""";

    private static final String CERT_PEM_WITH_START_END_SPACES =
            """
               -----BEGIN CERTIFICATE-----            \s
                  invalid pem content. just for testing    \s
               invalid pem content. just for testing     \s
               -----END CERTIFICATE-----      \
            """;

    private static final String X509_CERT_PEM =
            """
            -----BEGIN X509 CERTIFICATE-----
            invalid pem content. just for testing
            invalid pem content. just for testing
            -----END X509 CERTIFICATE-----""";

    private static final String PKCS7_CERT_PEM =
            """
            -----BEGIN PKCS7-----
            invalid pem content. just for testing
            invalid pem content. just for testing
            -----END PKCS7-----""";

    private static final String UNSUPPORTED_CERT_PEM =
            """
            -----BEGIN UNSUP-----
            invalid pem content. just for testing
            invalid pem content. just for testing
            -----END UNSUP-----""";

    private static final String CERT_MISSING_FOOTER_PEM =
            """
            -----BEGIN CERTIFICATE-----
            invalid pem content. just for testing
            invalid pem content. just for testing""";

    @Test
    public void testAddNewlineLastLine()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String filtered = filter.readPEM(new StringReader(CERT_PEM));

        // A newline should be added
        assertEquals(CERT_PEM + NL, filtered);
    }

    @Test
    public void testRemoveAdditionalNewlines()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String pem = NL + NL + CERT_PEM + NL + NL;

        String filtered = filter.readPEM(new StringReader(pem));

        // additional new lines should be removed
        assertEquals(CERT_PEM + NL, filtered);
    }

    @Test
    public void testMulitpleSupportedPEMBlocks()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String pem = NL + NL + CERT_PEM + NL + "some line" + NL + CERT_PEM + NL + NL;

        String filtered = filter.readPEM(new StringReader(pem));

        // additional new lines should be removed
        assertEquals(CERT_PEM + NL + CERT_PEM + NL, filtered);
    }

    @Test
    public void testX509Certificate()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String filtered = filter.readPEM(new StringReader(X509_CERT_PEM));

        // A newline should be added
        assertEquals(X509_CERT_PEM + NL, filtered);
    }

    @Test
    public void testPKCS7Certificate()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String filtered = filter.readPEM(new StringReader(PKCS7_CERT_PEM));

        // A newline should be added
        assertEquals(PKCS7_CERT_PEM + NL, filtered);
    }

    @Test
    public void testAllSupported()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        StrBuilder pem = new StrBuilder();

        pem.appendNewLine()
            .appendNewLine()
            .appendln(CERT_PEM)
            .appendln("some string")
            .appendNewLine()
            .appendNewLine()
            .appendln("some string")
            .appendln(X509_CERT_PEM)
            .appendln("some string")
            .appendNewLine()
            .appendln(PKCS7_CERT_PEM)
            .appendln(UNSUPPORTED_CERT_PEM)
            .appendNewLine()
            .appendNewLine()
            .appendNewLine();

        String filtered = filter.readPEM(new StringReader(pem.toString()));

        // A newline should be added
        assertEquals(CERT_PEM + NL + X509_CERT_PEM + NL + PKCS7_CERT_PEM + NL, filtered);
    }

    @Test
    public void testTrimLines()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        String filtered = filter.readPEM(new StringReader(CERT_PEM_WITH_START_END_SPACES));

        // A newline should be added
        assertEquals(CERT_PEM + NL, filtered);
    }

    @Test
    public void testMissingFooter()
    throws IOException
    {
        PEMFilter filter = new PEMFilter(PEMFilter.DEFAULT_CERTIFICATE_HEADER_FOOTERS);

        StrBuilder pem = new StrBuilder();

        pem.appendNewLine()
            .appendNewLine()
            .appendNewLine()
            .appendNewLine()
            .appendln("some string")
            .appendln(CERT_MISSING_FOOTER_PEM)
            .appendln("some string")
            .appendNewLine();

        String filtered = filter.readPEM(new StringReader(pem.toString()));

        // All lines after BEGIN will be added
        assertEquals(CERT_MISSING_FOOTER_PEM + NL + "some string" + NL, filtered);
    }
}
