/*
 * Copyright (c) 2013-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.security.cert.CertificateFactory;

import com.ciphermail.core.common.security.openpgp.PGPCertificate;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyInspector;

import org.junit.BeforeClass;
import org.junit.Test;

public class CipherMailProviderTest
{
    private static final File testBase = new File("src/test/resources/testdata/pgp/");

    @BeforeClass
    public static void beforeClass()
    {
        CipherMailProvider.initialize(null);
    }

    @Test
    public void testCreatePGPCertificate()
    throws Exception
    {
        CertificateFactory cf = CertificateFactory.getInstance("PGP");

        PGPCertificate certificate = (PGPCertificate) cf.generateCertificate(new FileInputStream(
                new File(testBase, "test@example.com.gpg.asc")));

        assertNotNull(certificate);

        assertEquals("ADE29F0A314B1760C151946F4E6F8E97B251B464DDE17003DA44DE07C3B20995",
                PGPPublicKeyInspector.getSHA256FingerprintHex(certificate.getPGPPublicKey()));

        assertNotNull(certificate.getPublicKey());
    }
}
