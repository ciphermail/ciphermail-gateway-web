/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CertificateEncoderTest
{
    @Test
    public void testEncodePKCS7()
    throws CertificateException, NoSuchProviderException, IOException
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/random-self-signed-10.p7b");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        byte[] encoded = CertificateEncoder.encode(certificates, ObjectEncoding.DER);

        assertNotNull(encoded);

        Collection<? extends Certificate> fromBytes = CertificateUtils.readCertificates(
                new ByteArrayInputStream(encoded));

        assertEquals(10, fromBytes.size());

        assertTrue(certificates.containsAll(fromBytes));
    }

    @Test
    public void testEncodePEM()
    throws CertificateException, NoSuchProviderException, IOException
    {
        File file = new File(TestUtils.getTestDataDir(),"certificates/random-self-signed-10.p7b");

        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        byte[] encoded = CertificateEncoder.encode(certificates, ObjectEncoding.PEM);

        assertNotNull(encoded);

        Collection<? extends Certificate> fromBytes = CertificateUtils.readCertificates(
                new ByteArrayInputStream(encoded));

        assertEquals(10, fromBytes.size());

        assertTrue(certificates.containsAll(fromBytes));
    }
}
