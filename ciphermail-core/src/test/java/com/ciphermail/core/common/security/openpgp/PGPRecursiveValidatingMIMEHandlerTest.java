/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.security.openpgp.validator.EncryptionKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.SigningKeyValidator;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPRecursiveValidatingMIMEHandlerTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static PGPKeyPairProvider keyPairProvider;

    private static PGPKeyPairProvider otherKeyPairProvider;

    private static PGPContentSignatureVerifier signatureVerifier;

    private static PGPContentSignatureValidator signatureValidator;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "pgp/test@example.com.gpg.key"))));

        keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider("test"));

        keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "pgp/test@example.com-expiration-2030-12-31.gpg.key"))));

        otherKeyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider("test"));

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/martijn@djigzo.com_0x271AD23B.asc"));

        signatureVerifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPPublicKeyValidator publicKeyValidator = new SigningKeyValidator(new PGPKeyFlagCheckerImpl(
                new PGPSignatureValidatorImpl(), new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl())));

        signatureValidator = new PGPContentSignatureValidatorImpl(signatureVerifier, publicKeyValidator);
    }

    @Test
    public void testPGPMIMESignedMIMEEncrypted()
    throws Exception
    {
        // This message is first signed with PGP/MIME and then the mime content is encrypted with PGP/MIME
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Test
    public void testPGPMIMESignedMIMEEncryptedKeepSignature()
    throws Exception
    {
        // This message is first signed with PGP/MIME and then the mime content is encrypted with PGP/MIME
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setRemoveSignature(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        // The message should have been decrypted but the signature should have been kept
        assertTrue(handler.isDecrypted());

        assertEquals("multipart/signed; micalg=pgp-sha1;\r\n protocol=\"application/pgp-signature\";\r\n " +
        		"boundary=\"TfQn8OqbXQB93hsKCF25Rupb9nLKPgaQB\"", handled.getContentType());
        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart sigPart = mp.getBodyPart(1);

        assertEquals("application/pgp-signature; name=\"signature.asc\"", sigPart.getContentType());

        String sig = IOUtils.toString(sigPart.getInputStream(), StandardCharsets.UTF_8);

        assertTrue(sig.contains("-----END PGP SIGNATURE-----"));
    }

    @Test
    public void testPGPMIMESignedMIMEEncryptedAddSecurityInfoToSubject()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        assertEquals("PGP/MIME signed", message.getSubject());

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));

        assertEquals("PGP/MIME signed [decrypted] [signed]", handled.getSubject());
    }

    @Test
    public void testPGPMIMEAddSecurityInfoToSubjectNullTags()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        assertEquals("PGP/MIME signed", message.getSubject());

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag(null);
        handler.setSignedValidTag(null);
        handler.setSignedInvalidTag(null);
        handler.setSignedByValidTag(null);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));

        assertEquals("PGP/MIME signed", handled.getSubject());
    }

    @Test
    public void testPGPMIMESignedMIMEEncryptedSignerSenderMismatch()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        message.setFrom(new InternetAddress("someoneelse@example.com"));

        assertEquals("PGP/MIME signed", message.getSubject());

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-128", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signer-Mismatch", ","));
        assertEquals("martijn@djigzo.com, martijn@ciphermail.com",
                handled.getHeader("X-CipherMail-Info-PGP-Signer-Email", ","));

        assertEquals("PGP/MIME signed [decrypted] [signed by martijn@djigzo.com, martijn@ciphermail.com]",
                handled.getSubject());
    }

    @Test
    public void testPGPMIMESigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertFalse(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
    }

    @Test
    public void testPGPMIMESignedTampered()
    throws Exception
    {
        // This message is first signed with PGP/MIME and then the mime content is encrypted with PGP/MIME
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-tampered.eml"));

        assertEquals("PGP/MIME signed", message.getSubject());

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertFalse(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Message has been tampered with.", handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));

        assertEquals("PGP/MIME signed [signed invalid]", handled.getSubject());
        assertFalse(handler.isDecryptionKeyNotFound());
    }

    @Test
    public void testPGPMIMEEncryptedKeyNotFound()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-encrypted-text-only.eml"));

        assertEquals("PGP/MIME encrypted text only", message.getSubject());

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(otherKeyPairProvider,
                signatureValidator);

        assertFalse(handler.isDecrypted());

        MimeMessage handled = handler.handleMessage(message);

        assertNull(handled);
        assertTrue(handler.isDecryptionKeyNotFound());
    }

    @Test
    public void testPGPInlineEncryptedKeyNotFound()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-encrypted-signed-mixed-content.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(otherKeyPairProvider,
                signatureValidator);

        assertFalse(handler.isDecrypted());

        MimeMessage handled = handler.handleMessage(message);

        assertNull(handled);
        assertTrue(handler.isDecryptionKeyNotFound());
    }

    @Test
    public void testPGPMIMESignedInvalidKey()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                new PGPContentSignatureValidatorImpl(signatureVerifier,
                new EncryptionKeyValidator(new PGPKeyFlagCheckerImpl(new PGPSignatureValidatorImpl(),
                        new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl())))));

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("Key is not valid for encryption.", handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));

        assertEquals("PGP/MIME signed [signed invalid]", handled.getSubject());
    }

    @Test
    public void testPGPInlineSignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-signed-and-encrypted.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertFalse(handler.isDecryptionKeyNotFound());

        assertEquals("multipart/mixed;\r\n boundary=\"------------070101000007080509080904\"",
                handled.getContentType());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertEquals("test PGP signed and encrypted [decrypted] [signed]", handled.getSubject());

        Multipart mp = (Multipart) handled.getContent();

        BodyPart bodyPart = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", bodyPart.getContentType());
        assertTrue(((String)bodyPart.getContent()).contains("DJIGZO email encryption"));

        bodyPart = mp.getBodyPart(1);

        assertEquals("application/octet-stream; name=djigzo-whitepaper.pdf", bodyPart.getContentType());
        assertEquals("attachment; filename=djigzo-whitepaper.pdf",
                StringUtils.join(bodyPart.getHeader("Content-Disposition"), ","));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(bodyPart.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testPGPInlineSignedEncryptedMixedContent()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed-and-encrypted-mixed-content.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertEquals("multipart/mixed;\r\n boundary=\"------------070101000007080509080904\"",
                handled.getContentType());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content", ","));

        assertEquals("test PGP signed and encrypted [decrypted] [signed] [mixed content]", handled.getSubject());

        Multipart mp = (Multipart) handled.getContent();

        BodyPart bodyPart = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", bodyPart.getContentType());
        assertTrue(((String)bodyPart.getContent()).contains("DJIGZO email encryption"));

        bodyPart = mp.getBodyPart(1);

        assertEquals("application/octet-stream; name=djigzo-whitepaper.pdf", bodyPart.getContentType());
        assertEquals("attachment; filename=djigzo-whitepaper.pdf",
                StringUtils.join(bodyPart.getHeader("Content-Disposition"), ","));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(bodyPart.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testPGPInlineEncryptedTextOnly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-encrypted-text-only.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signed", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));

        assertEquals("PGP/INLINE encrypted text only [decrypted]", handled.getSubject());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));
    }

    @Test
    public void testPGPInlineEncryptedSignedTampered()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-encrypted-signed-tampered.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertTrue(handler.isDecrypted());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertEquals("Message has been tampered with.", handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));

        assertEquals("PGP/INLINE tampered [decrypted] [signed invalid]", handled.getSubject());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));
    }

    @Test
    public void testPGPInlineEncryptedSignedMixedContent()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-encrypted-signed-mixed-content.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content", ","));

        assertEquals("PGP/INLINE encrypted signed mixed content [decrypted] [signed] [mixed content]",
                handled.getSubject());

        assertEquals("text/plain; charset=ISO-8859-1", handled.getContentType());
        assertTrue(((String)handled.getContent()).contains("Some text before signed part"));
        assertTrue(((String)handled.getContent()).contains("DJIGZO email encryption"));
        assertTrue(((String)handled.getContent()).contains("Some text after signed part"));
    }

    @Test
    public void testPGPInlineSigned()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertFalse(handler.isDecrypted());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure"));

        assertTrue(message.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart pdfPart = mp.getBodyPart(1);

        assertEquals("application/pdf;\r\n name=\"djigzo-whitepaper.pdf\"", pdfPart.getContentType());
        assertEquals("attachment;\r\n filename=\"djigzo-whitepaper.pdf\"",
                StringUtils.join(pdfPart.getHeader("Content-Disposition"), ","));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdfPart.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));
    }

    @Test
    public void testPGPInlineSignedKeepSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");
        handler.setRemoveSignature(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure"));

        assertTrue(message.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(3, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("-----BEGIN PGP SIGNED MESSAGE-----"));

        BodyPart pdfPart = mp.getBodyPart(1);

        assertEquals("application/pdf;\r\n name=\"djigzo-whitepaper.pdf\"", pdfPart.getContentType());
        assertEquals("attachment;\r\n filename=\"djigzo-whitepaper.pdf\"",
                StringUtils.join(pdfPart.getHeader("Content-Disposition"), ","));

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(pdfPart.getInputStream(), bos);

        // Check md5 of pdf
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(bos.toByteArray(), Digest.MD5));

        BodyPart sigPart = mp.getBodyPart(2);

        assertEquals("application/octet-stream;\r\n name=\"djigzo-whitepaper.pdf.sig\"", sigPart.getContentType());
        assertEquals("attachment;\r\n filename=\"djigzo-whitepaper.pdf.sig\"",
                StringUtils.join(sigPart.getHeader("Content-Disposition"), ","));
    }

    @Test
    public void testPGPInlineSignedMixedContentAtStartKeepSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-mixed-content-at-start.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");
        handler.setRemoveSignature(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("10C01C5A2F6059E7", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("False", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content", ","));
        assertEquals("Signer's key with key ID\r\n 10C01C5A2F6059E7 not found.",
                handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ","));

        assertEquals("text/plain; charset=\"us-ascii\"", handled.getContentType());

        String body = (String) handled.getContent();

        assertTrue(body.contains("Some text before the signature"));
        assertTrue(body.contains("-----BEGIN PGP SIGNED MESSAGE-----"));
        assertTrue(body.contains("-----END PGP SIGNATURE-----"));
        assertTrue(body.contains("Hosted and sponsored by Secunia - http://secunia.com/"));
    }

    @Test
    public void testPGPInlineSignedKeyAttached()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-attached-key.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        final List<PGPPublicKeyRing> keyrings = new LinkedList<PGPPublicKeyRing>();

        handler.setPublicKeyRingListener(keyrings::add);

        handler.setAddSecurityInfoToSubject(true);
        handler.setDecryptedTag("[decrypted]");
        handler.setSignedValidTag("[signed]");
        handler.setSignedInvalidTag("[signed invalid]");
        handler.setSignedByValidTag("[signed by %s]");
        handler.setMixedContentTag("[mixed content]");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure"));

        assertTrue(message.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) handled.getContent();

        assertEquals(2, mp.getCount());

        BodyPart body = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", body.getContentType());
        assertTrue(((String)body.getContent()).contains("DJIGZO email encryption"));

        BodyPart keyPart = mp.getBodyPart(1);

        assertEquals("application/pgp-keys;\r\n name=\"0x271AD23B.asc\"", keyPart.getContentType());

        // The keyring is added twice because the message is scanned twice by PGPRecursiveValidatingMIMEHandler
        assertEquals(2, keyrings.size());

        PGPPublicKeyRing keyring = keyrings.get(0);

        assertEquals("4605970C271AD23B", PGPUtils.getKeyIDHex(keyring.getPublicKey().getKeyID()));
    }

    @Test
    public void testPGPUnsignedKeyAttached()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-attached-key.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        MimeMessage handled = handler.handleMessage(message);

        assertNull(handled);
    }

    @Test
    public void testPGPInlineSignedISO_8859_1()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-ISO-8859-1-xa8a90b8ead0c6e69.eml"));

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "pgp/xa8a90b8ead0c6e69.asc"));

        PGPContentSignatureVerifier signatureVerifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPPublicKeyValidator publicKeyValidator = new SigningKeyValidator(new PGPKeyFlagCheckerImpl(
                new PGPSignatureValidatorImpl(), new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl())));

        PGPContentSignatureValidator signatureValidator = new PGPContentSignatureValidatorImpl(signatureVerifier,
                publicKeyValidator);

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("text/plain; charset=iso-8859-1", handled.getContentType());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("A8A90B8EAD0C6E69", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Test
    public void testPGPUniversalMIMESignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/pgp-universal-signed-encrypted-mime.eml"));

        // Since the message is encrypted with a different key, we need to use a different handler
        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TEST_BASE, "pgp/test@example.com.pgp-universal.asc"),
                new File(TEST_BASE, "pgp/test@test.djigzo.com.key"), "test");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/mixed;\r\n boundary=\"------------000606040603070700030306\"",
                handled.getContentType());

        assertEquals("PGP/MIME", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("1E1068D0AABAEC28", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Test
    public void testPGPUniversalPartitionedSignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/pgp-universal-signed-encrypted-partitioned.eml"));

        // Since the message is encrypted with a different key, we need to use a different handler
        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TEST_BASE, "pgp/test@example.com.pgp-universal.asc"),
                new File(TEST_BASE, "pgp/test@test.djigzo.com.key"), "test");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/mixed;\r\n boundary=\"------------030206020906080703040000\"", handled.getContentType());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("1E1068D0AABAEC28", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Test
    public void testPGPUniversalHTMLPartitionedSignedEncrypted()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/pgp-universal-signed-encrypted-html-partitioned.eml"));

        // Since the message is encrypted with a different key, we need to use a different handler
        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TEST_BASE, "pgp/test@example.com.pgp-universal.asc"),
                new File(TEST_BASE, "pgp/test@test.djigzo.com.key"), "test");

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertEquals("multipart/mixed;\r\n boundary=\"------------050102060901020803040602\"", handled.getContentType());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Encrypted", ","));
        assertEquals("AES-256", handled.getHeader("X-CipherMail-Info-PGP-Encryption-Algorithm", ","));
        assertEquals("1E1068D0AABAEC28", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
    }

    @Ignore
    @Test
    public void testPGPMIMESignedBulk()
    throws Exception
    {
        assertTrue("Please extract example-mails.tar.gz", new File(TEST_BASE, "pgp/example-mails/pgp-mime-signed").
                isDirectory());

        Collection<File> files = FileUtils.listFiles(new File(TEST_BASE, "pgp/example-mails/pgp-mime-signed"),
                new String[]{"eml"}, false);

        Collection<File> notPGP = new LinkedList<>();
        Collection<File> success = new LinkedList<>();
        Collection<File> failed = new LinkedList<>();
        Collection<File> tampered = new LinkedList<>();
        Collection<File> unsigned = new LinkedList<>();

        for (File file : files)
        {
            System.out.println("Loading file: " + file);

            MimeMessage message = MailUtils.loadMessage(file);

            PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                    signatureValidator);

            handler.setAddSecurityInfoToSubject(true);
            handler.setDecryptedTag("[decrypted]");
            handler.setSignedValidTag("[signed]");
            handler.setSignedInvalidTag("[signed invalid]");
            handler.setSignedByValidTag("[signed by %s]");
            handler.setMixedContentTag("[mixed content]");

            try {
                MimeMessage handled = handler.handleMessage(message);

                if (handled != null)
                {
                    success.add(file);

                    MailUtils.validateMessage(handled);

                    String headerValue = handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure", ",");

                    if (StringUtils.contains(headerValue, "tampered")) {
                        tampered.add(file);
                    }

                    headerValue = handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ",");

                    if (headerValue == null) {
                        unsigned.add(file);
                    }
                }
                else {
                    notPGP.add(file);
                }
            }
            catch(Exception e) {
                failed.add(file);
            }
        }

        assertEquals(26, success.size());

        for (File file : success) {
            System.out.println("PGP: " + file);
        }

        assertEquals(8, failed.size());

        for (File file : failed) {
            System.out.println("PGP failed: " + file);
        }

        assertEquals(0, tampered.size());

        for (File file : tampered) {
            System.out.println("Tampered: " + file);
        }

        assertEquals(0, unsigned.size());

        for (File file : unsigned) {
            System.out.println("Unsigned: " + file);
        }

        assertEquals(821, notPGP.size());

        for (File file : notPGP) {
            System.out.println("!PGP: " + file);
        }
    }

    @Test
    public void testPGPMIMEDecryptionDisabled()
    throws Exception
    {
        // This message is first signed with PGP/MIME and then the mime content is encrypted with PGP/MIME
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/PGP-MIME-signed-MIME-encrypted.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setDecrypt(false);

        MimeMessage handled = handler.handleMessage(message);

        assertNull(handled);
    }

    @Test
    public void testPGPInlineDecryptionDisabled()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-encrypted-signed-mixed-content.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setDecrypt(false);

        MimeMessage handled = handler.handleMessage(message);

        assertNull(handled);
    }

    @Test
    public void testPGPInlineSignedDecryptionDisabled()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed.eml"));

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        // This should not make any difference since the email is signed only. This is to test whether this setting
        // does not interfere with handling signed only email
        handler.setDecrypt(false);

        MimeMessage handled = handler.handleMessage(message);

        MailUtils.validateMessage(handled);

        assertFalse(handler.isDecrypted());

        assertEquals("PGP/INLINE", handled.getHeader("X-CipherMail-Info-PGP-Encoding", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Encrypted"));
        assertEquals("4605970C271AD23B", handled.getHeader("X-CipherMail-Info-PGP-Signer-KeyID", ","));
        assertEquals("True", handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Mixed-Content"));
        assertNull(handled.getHeader("X-CipherMail-Info-PGP-Signature-Failure"));
    }
}
