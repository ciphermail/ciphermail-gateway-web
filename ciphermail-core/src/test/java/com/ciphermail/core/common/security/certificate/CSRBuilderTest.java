/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.operator.DefaultAlgorithmNameFinder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.junit.Test;

import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CSRBuilderTest
{
    @Test
    public void testAlgorithms()
    throws Exception
    {
        CSRBuilder builder = CSRBuilder.getInstance();

        builder.setSubject(X500PrincipalBuilder.getInstance().setCommonName("test").buildName());

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.RSA_2048);
        PKCS10CertificationRequest csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof RSAPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.RSA_2048, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA256WITHRSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.RSA_3072);
        csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof RSAPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.RSA_3072, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA384WITHRSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.RSA_4096);
        csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof RSAPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.RSA_4096, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA512WITHRSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.SECP521R1);
        csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof ECPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.SECP521R1, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA512WITHECDSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.SECP384R1);
        csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof ECPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.SECP384R1, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA384WITHECDSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));

        builder.setKeyAlgorithm(CSRBuilder.Algorithm.SECP256R1);
        csr = builder.buildPKCS10();
        assertTrue(builder.getKeyPair().getPublic() instanceof ECPublicKey);
        assertEquals(TLSKeyPairBuilder.Algorithm.SECP256R1, TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                builder.getKeyPair().getPublic()));
        assertTrue(csr.isSignatureValid(new JcaContentVerifierProviderBuilder().build(
                builder.getKeyPair().getPublic())));
        assertEquals("SHA256WITHECDSA", new DefaultAlgorithmNameFinder().getAlgorithmName(
                csr.getSignatureAlgorithm().getAlgorithm()));
    }

    @Test
    public void testAddDomains()
    throws Exception
    {
        CSRBuilder builder = CSRBuilder.getInstance();

        builder.setSubject(X500PrincipalBuilder.getInstance().setCommonName("test").buildName());
        builder.addDomain("1.example.com").addDomain("2.example.com");

        PKCS10CertificationRequest csr = builder.buildPKCS10();

        Extensions extensions = csr.getRequestedExtensions();

        Extension extension = extensions.getExtension(Extension.subjectAlternativeName);

        AltNamesInspector altNamesInspector = new AltNamesInspector(ASN1Sequence.getInstance(
                extension.getExtnValue().getOctets()));

        assertThat(altNamesInspector.getDNSNames(), hasItems("1.example.com", "2.example.com"));
    }
}