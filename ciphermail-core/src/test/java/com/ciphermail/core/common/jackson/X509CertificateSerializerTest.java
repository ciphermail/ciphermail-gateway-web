package com.ciphermail.core.common.jackson;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;

public class X509CertificateSerializerTest
{
    private static final String ENCODED_CERTIFICATE =
        """
        "MIIDVjCCAr+gAwIBAgIOUtwAAQACsrt0dJQB9HUwDQYJKoZIhvcNAQEEBQAwgbwxCzAJBgNVBAY
        TAkRFMRAwDgYDVQQIEwdIYW1idXJnMRAwDgYDVQQHEwdIYW1idXJnMTowOAYDVQQKEzFUQyBUcnV
        zdENlbnRlciBmb3IgU2VjdXJpdHkgaW4gRGF0YSBOZXR3b3JrcyBHbWJIMSIwIAYDVQQLExlUQyB
        UcnVzdENlbnRlciBDbGFzcyAxIENBMSkwJwYJKoZIhvcNAQkBFhpjZXJ0aWZpY2F0ZUB0cnVzdGN
        lbnRlci5kZTAeFw0wNTA5MjQyMDE5NDNaFw0wNjA5MjQyMDE5NDNaME0xCzAJBgNVBAYTAk5MMRk
        wFwYDVQQDExBNYXJ0aWpuIEJyaW5rZXJzMSMwIQYJKoZIhvcNAQkBFhRtLmJyaW5rZXJzQHBvYm9
        4LmNvbTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA2F1WbWOnUMPJ4X+zCtS0LGT6rUqRKgl
        nb4SVNVVX4RbGWN2p3QrhO2HrwGaPBWWvGF2mYL3S2B1T5/8naMHZZrkrlQPZe4EwYo3LBIySAJF
        QdZnCDs68xBwXJilqRoCfNHeV86hVQkU4qN3g6CgdlrDyoUaFPzALyRv79P/f2CsCAwEAAaOByDC
        BxTAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIF4DAzBglghkgBhvhCAQgEJhYkaHR0cDovL3d
        3dy50cnVzdGNlbnRlci5kZS9ndWlkZWxpbmVzMBEGCWCGSAGG+EIBAQQEAwIFoDBdBglghkgBhvh
        CAQMEUBZOaHR0cHM6Ly93d3cudHJ1c3RjZW50ZXIuZGUvY2dpLWJpbi9jaGVjay1yZXYuY2dpLzU
        yREMwMDAxMDAwMkIyQkI3NDc0OTQwMUY0NzU/MA0GCSqGSIb3DQEBBAUAA4GBAIsb6tL1nA0rZzE
        ff+lQpkhIyte0dGmFSrq6t34T92Ql87XW63EeMLrJZl0/17B1jancthAbSzV/h82XfYmjI2I95J8
        6J1ZyF6Hk84N37QA7mAghv/HlhjL5x8kQ7KgOQ6KmbcKfK2sbYmRgZ3q7/6EDKeXLIu9/c4CswqK
        zWdlb"
        """;

    @Test
    public void serialize()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/testcertificate.cer"));

        String json = JacksonUtil.getObjectMapper().writeValueAsString(certificate);

        X509Certificate deserialized = JacksonUtil.getObjectMapper().readValue(json,
                X509Certificate.class);

        assertEquals(certificate, deserialized);
    }

    /*
     * Deserialize a static value making sure old JSON will continue to work
     */
    @Test
    public void deserialize()
    throws Exception
    {
        X509Certificate certificate = JacksonUtil.getObjectMapper().readValue(
                StringUtils.remove(ENCODED_CERTIFICATE, '\n'),
                X509Certificate.class);

        assertEquals(TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/testcertificate.cer")), certificate);
    }
}