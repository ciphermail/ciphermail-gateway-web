/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import org.junit.Test;
import org.passay.PasswordData;
import org.passay.RuleResult;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AbstractJSONNOutOfMRuleTest
{
    private static final String digitRegex = "{p:'^(?=.*[0-9]).{8,}$'}";
    private static final String lowercaseRegex = "{p:'^(?=.*[a-z]).{8,}$'}";
    private static final String uppercaseRegex = "{p:'^(?=.*[A-Z]).{8,}$'}";
    private static final String specialCharsRegex = "{p:'^(?=.*[\\\\p{Punct}]).{1,}$'}";

    static class StaticJSONNOutOfMRule extends AbstractJSONNOutOfMRule
    {
        private final String json;

        public StaticJSONNOutOfMRule(String json) {
            this.json = json;
        }

        @Override
        public String getJSON(PasswordData passwordData) {
            return json;
        }
    }

    @Test
    public void test2OutOf3()
    {
        String json = String.format("{m:2, r:[%s, %s, %s]}", digitRegex, lowercaseRegex, uppercaseRegex);

        System.out.println(json);

        StaticJSONNOutOfMRule rule = new StaticJSONNOutOfMRule(json);

        RuleResult result;

        result = rule.validate(new PasswordData("12345678"));
        assertFalse(result.isValid());

        result = rule.validate(new PasswordData("abcdefgh"));
        assertFalse(result.isValid());

        result = rule.validate(new PasswordData("ABCDEFGH"));
        assertFalse(result.isValid());

        result = rule.validate(new PasswordData("1234567A"));
        assertTrue(result.isValid());

        result = rule.validate(new PasswordData("123456A"));
        assertFalse(result.isValid());

        result = rule.validate(new PasswordData("123456a8"));
        assertTrue(result.isValid());

        result = rule.validate(new PasswordData("abcdefgH"));
        assertTrue(result.isValid());
    }

    @Test
    public void testSpecialChars()
    {
        String json = String.format("{m:1, r:[%s]}", specialCharsRegex);

        System.out.println(json);

        StaticJSONNOutOfMRule rule = new StaticJSONNOutOfMRule(json);

        assertFalse(rule.validate(new PasswordData("aA1")).isValid());

        assertTrue(rule.validate(new PasswordData("!")).isValid());
        assertTrue(rule.validate(new PasswordData("\"")).isValid());
        assertTrue(rule.validate(new PasswordData("#")).isValid());
        assertTrue(rule.validate(new PasswordData("$")).isValid());
        assertTrue(rule.validate(new PasswordData("%")).isValid());
        assertTrue(rule.validate(new PasswordData("&")).isValid());
        assertTrue(rule.validate(new PasswordData("'")).isValid());
        assertTrue(rule.validate(new PasswordData("(")).isValid());
        assertTrue(rule.validate(new PasswordData("*")).isValid());
        assertTrue(rule.validate(new PasswordData("+")).isValid());
        assertTrue(rule.validate(new PasswordData(",")).isValid());
        assertTrue(rule.validate(new PasswordData("-")).isValid());
        assertTrue(rule.validate(new PasswordData(".")).isValid());
        assertTrue(rule.validate(new PasswordData("/")).isValid());
        assertTrue(rule.validate(new PasswordData(":")).isValid());
        assertTrue(rule.validate(new PasswordData(";")).isValid());
        assertTrue(rule.validate(new PasswordData("<")).isValid());
        assertTrue(rule.validate(new PasswordData("=")).isValid());
        assertTrue(rule.validate(new PasswordData(">")).isValid());
        assertTrue(rule.validate(new PasswordData("?")).isValid());
        assertTrue(rule.validate(new PasswordData("@")).isValid());
        assertTrue(rule.validate(new PasswordData("[")).isValid());
        assertTrue(rule.validate(new PasswordData("\\")).isValid());
        assertTrue(rule.validate(new PasswordData("]")).isValid());
        assertTrue(rule.validate(new PasswordData("^")).isValid());
        assertTrue(rule.validate(new PasswordData("_")).isValid());
        assertTrue(rule.validate(new PasswordData("`")).isValid());
        assertTrue(rule.validate(new PasswordData("{")).isValid());
        assertTrue(rule.validate(new PasswordData("|")).isValid());
        assertTrue(rule.validate(new PasswordData("}")).isValid());
        assertTrue(rule.validate(new PasswordData("~")).isValid());
    }

    @Test
    public void testEmptyJSON()
    {
        StaticJSONNOutOfMRule rule = new StaticJSONNOutOfMRule("");

        assertFalse(rule.validate(new PasswordData("12345678")).isValid());

        rule = new StaticJSONNOutOfMRule(null);

        assertFalse(rule.validate(new PasswordData("12345678")).isValid());
    }

    @Test
    public void testAllowAllNonEmpty()
    {
        StaticJSONNOutOfMRule rule = new StaticJSONNOutOfMRule("{r:[{p:'\\\\S'}]}");

        assertTrue(rule.validate(new PasswordData("12345678")).isValid());
        assertFalse(rule.validate(new PasswordData("")).isValid());
        assertFalse(rule.validate(new PasswordData(" ")).isValid());
        assertTrue(rule.validate(new PasswordData("\u2202")).isValid());
    }
}
