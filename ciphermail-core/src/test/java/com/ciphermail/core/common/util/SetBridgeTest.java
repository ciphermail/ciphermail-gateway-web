/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class SetBridgeTest
{
    class A {
        int a;
    };

    class B extends A {
        int b;
    };

    class C {

    }

    private static final Set<B> superSet = new LinkedHashSet<B>();

    @Before
    public void setup()
    {
        superSet.clear();
    }

    @Test
    public void testAdd()
    {
        B b = new B();

        superSet.add(b);

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        assertEquals(1, superSet.size());
        assertEquals(1, baseSet.size());
        assertTrue(baseSet.contains(b));

        B b2 = new B();

        baseSet.add(b2);

        assertEquals(2, superSet.size());
        assertEquals(2, baseSet.size());
        assertTrue(superSet.contains(b2));
        assertTrue(baseSet.contains(b2));
    }

    @Test
    public void testClear()
    {
        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(new B());
        baseSet.add(new B());

        assertEquals(2, superSet.size());
        assertEquals(2, baseSet.size());

        assertFalse(superSet.isEmpty());
        assertFalse(baseSet.isEmpty());

        baseSet.clear();

        assertEquals(0, superSet.size());
        assertEquals(0, baseSet.size());

        assertTrue(superSet.isEmpty());
        assertTrue(baseSet.isEmpty());

        superSet.add(new B());
        superSet.add(new B());

        assertEquals(2, superSet.size());
        assertEquals(2, baseSet.size());

        baseSet.clear();

        assertEquals(0, superSet.size());
        assertEquals(0, baseSet.size());

        assertTrue(superSet.isEmpty());
        assertTrue(baseSet.isEmpty());
    }

    @Test
    public void testContainsAll()
    {
        Collection<Object> c = new LinkedList<Object>();

        B b1 = new B();
        B b2 = new B();
        B b3 = new B();

        c.add(b1);
        c.add(b2);

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(b1);
        superSet.add(b2);
        baseSet.add(b3);

        assertTrue(baseSet.containsAll(c));
        assertTrue(superSet.containsAll(c));

        assertTrue(baseSet.containsAll(superSet));
        assertTrue(superSet.containsAll(baseSet));

        c.add(new B());

        assertFalse(baseSet.containsAll(c));
        assertFalse(superSet.containsAll(c));
    }

    @Test
    public void testRemoveAll()
    {
        Collection<Object> c = new LinkedList<Object>();

        B b1 = new B();
        B b2 = new B();
        B b3 = new B();

        c.add(b1);
        c.add(b2);

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(b1);
        superSet.add(b2);
        baseSet.add(b3);

        assertEquals(3, baseSet.size());

        baseSet.removeAll(c);

        assertEquals(1, baseSet.size());
        assertTrue(baseSet.contains(b3));
    }

    @Test
    public void testRetainAll()
    {
        Collection<Object> c = new LinkedList<Object>();

        B b1 = new B();
        B b2 = new B();
        B b3 = new B();

        c.add(b1);
        c.add(b2);

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(b1);
        superSet.add(b2);
        baseSet.add(b3);

        assertEquals(3, baseSet.size());

        baseSet.retainAll(c);

        assertEquals(2, baseSet.size());
        assertTrue(baseSet.contains(b1));
        assertTrue(baseSet.contains(b2));
    }

    @Test
    public void testIterator()
    {
        B b1 = new B();
        B b2 = new B();
        B b3 = new B();

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(b1);
        superSet.add(b2);
        baseSet.add(b3);

        assertEquals(3, baseSet.size());

        Iterator<A> it = baseSet.iterator();

        assertEquals(b1, it.next());
        assertTrue(it.hasNext());
        assertEquals(b2, it.next());
        assertTrue(it.hasNext());
        assertEquals(b3, it.next());
        assertFalse(it.hasNext());
    }

    @Test
    public void testRemove()
    {
        B b1 = new B();
        B b2 = new B();

        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        assertFalse(baseSet.contains(b1));
        assertFalse(baseSet.contains(b2));

        assertFalse(superSet.contains(b1));
        assertFalse(superSet.contains(b2));

        baseSet.add(b1);
        baseSet.add(b2);

        assertTrue(baseSet.contains(b1));
        assertTrue(baseSet.contains(b2));

        assertTrue(superSet.contains(b1));
        assertTrue(superSet.contains(b2));

        baseSet.remove(b1);

        assertFalse(baseSet.contains(b1));
        assertTrue(baseSet.contains(b2));

        assertFalse(superSet.contains(b1));
        assertTrue(superSet.contains(b2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNonSuperSet()
    {
        Set<A> baseSet = new SetBridge<A, B>(superSet, B.class);

        baseSet.add(new A());
    }
}
