/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.scheduler;

import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class TaskSchedulerTest
{
    private static class RunawayInputStream extends InputStream
    {
        private boolean closed;
        private final Object lock = new Object();

        @Override
        public int read()
        {
            ThreadUtils.sleepQuietly(10);

            synchronized (lock) {
                return closed ? -1 : 0;
            }
        }

        @Override
        public void close()
        {
            synchronized (lock) {
                closed = true;
            }
        }
    }

    private static class SlowInputStream extends InputStream
    {
        private final int count;

        private final Object lock = new Object();
        private boolean closed;
        private int i;

        public SlowInputStream(int count) {
            this.count = count;
        }

        @Override
        public int read()
        {
            if (i >= count) {
                close();
            }

            i++;

            ThreadUtils.sleepQuietly(10);

            synchronized (lock) {
                return closed ? -1 : 0;
            }
        }

        @Override
        public void close()
        {
            synchronized (lock) {
                closed = true;
            }
        }
    }

    @Test
    void testInputStreamTimeoutTask()
    throws IOException
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        InputStream runawayInputStream = new RunawayInputStream();

        Task task = new InputStreamTimeoutTask(runawayInputStream, "testInputStreamTimeoutTask");

        scheduler.addTask(task, 500);

        OutputStream devNull = NullOutputStream.INSTANCE;

        IOUtils.copy(runawayInputStream, devNull);

        Assertions.assertTrue(task.hasRun());
        Assertions.assertFalse(task.isCanceled());
    }

    @Test
    void testInputStreamTimeoutTaskCancelTask()
    throws IOException
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        InputStream input = new SlowInputStream(50);

        Task task = new InputStreamTimeoutTask(input, "testInputStreamTimeoutTaskCancelTask");

        scheduler.addTask(task, 10);

        Assertions.assertFalse(task.isCanceled());

        task.cancel();

        Assertions.assertTrue(task.isCanceled());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(input, bos);

        Assertions.assertEquals(50, bos.size());
    }

    @Test
    void testInputStreamTimeoutTaskCancelScheduler()
    throws IOException
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        InputStream input = new SlowInputStream(50);

        Task task = new InputStreamTimeoutTask(input, "testInputStreamTimeoutTaskCancelScheduler");

        scheduler.addTask(task, 10);

        Assertions.assertFalse(scheduler.isCanceled());

        scheduler.cancel();

        Assertions.assertTrue(scheduler.isCanceled());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(input, bos);

        Assertions.assertEquals(50, bos.size());
    }


    @Test
    void testThreadInterruptTimeoutTask()
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        Task task = new ThreadInterruptTimeoutTask(Thread.currentThread(), "ThreadInterruptTimeoutTask");

        scheduler.addTask(task, 500);

        try {
            Thread.sleep(2000);

            Assertions.fail();
        }
        catch (InterruptedException e) {
            // expected exception
            Thread.interrupted();
        }

        Assertions.assertTrue(task.hasRun());
        Assertions.assertFalse(task.isCanceled());
    }

    @Test
    void testThreadInterruptTimeoutTaskCancelTask()
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        Task task = new ThreadInterruptTimeoutTask(Thread.currentThread(), "ThreadInterruptTimeoutTask");

        scheduler.addTask(task, 500);

        Assertions.assertFalse(task.isCanceled());

        task.cancel();

        Assertions.assertTrue(task.isCanceled());

        ThreadUtils.sleepQuietly(1000);
    }

    @Test
    void testThreadInterruptTimeoutTaskCancelScheduler()
    {
        TaskScheduler scheduler = new TaskScheduler("test");

        Task task = new ThreadInterruptTimeoutTask(Thread.currentThread(), "ThreadInterruptTimeoutTask");

        scheduler.addTask(task, 500);

        Assertions.assertFalse(scheduler.isCanceled());

        scheduler.cancel();

        Assertions.assertTrue(scheduler.isCanceled());

        ThreadUtils.sleepQuietly(1000);
    }
}
