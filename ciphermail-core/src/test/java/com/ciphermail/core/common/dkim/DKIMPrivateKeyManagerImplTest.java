/*
 * Copyright (c) 2016-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dkim;

import com.ciphermail.core.common.security.PublicKeyInspector;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.keystore.KeyStoreProviderKeyStoreWrapper;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.KeyPair;
import java.security.KeyStore;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class DKIMPrivateKeyManagerImplTest
{
    private DKIMPrivateKeyManager privateKeyManager;
    private KeyStore keyStore;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();
    }

    @Before
    public void setup()
    throws Exception
    {
        keyStore = KeyStore.getInstance("PKCS12", SecurityFactoryBouncyCastle.PROVIDER_NAME);

        keyStore.load(null);

        privateKeyManager = new DKIMPrivateKeyManagerImpl(new KeyStoreProviderKeyStoreWrapper(keyStore),
                new StaticPasswordProvider("test"));
    }

    @Test
    public void testGenerateKey()
    throws Exception
    {
        KeyPair keyPair = privateKeyManager.generateKey("test@example.com", 1024);

        assertNotNull(keyPair);
        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());

        assertEquals("RSA", PublicKeyInspector.getFriendlyAlgorithm(keyPair.getPublic()));
        assertEquals(1024, PublicKeyInspector.getKeyLength(keyPair.getPublic()));
        assertTrue(keyStore.containsAlias("DKIM:test@example.com"));
        assertEquals(keyPair.getPublic(), privateKeyManager.getKey("test@example.com").getPublic());
        assertEquals(keyPair.getPrivate(), privateKeyManager.getKey("test@example.com").getPrivate());

        keyPair = privateKeyManager.generateKey("test@example.com", 2048);

        assertNotNull(keyPair);
        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());

        assertEquals("RSA", PublicKeyInspector.getFriendlyAlgorithm(keyPair.getPublic()));
        assertEquals(2048, PublicKeyInspector.getKeyLength(keyPair.getPublic()));
        assertTrue(keyStore.containsAlias("DKIM:test@example.com"));
    }

    @Test
    public void testDeleteKey()
    throws Exception
    {
        privateKeyManager.generateKey("test@example.com", 1024);

        KeyPair keyPair = privateKeyManager.getKey("test@example.com");

        assertNotNull(keyPair);
        assertNotNull(keyPair.getPrivate());
        assertNotNull(keyPair.getPublic());

        privateKeyManager.deleteKey("test@example.com");

        keyPair = privateKeyManager.getKey("test@example.com");

        assertNull(keyPair);

        privateKeyManager.deleteKey("test@example.com");
    }

    @Test
    public void testGetKeyIds()
    throws Exception
    {
        List<String> keyIds = privateKeyManager.getKeyIds();

        assertEquals(0, keyIds.size());

        privateKeyManager.generateKey("test@example.com", 1024);

        keyIds = privateKeyManager.getKeyIds();
        assertEquals(1, keyIds.size());

        privateKeyManager.generateKey("test2@example.com", 1024);

        keyIds = privateKeyManager.getKeyIds();
        assertEquals(2, keyIds.size());

        privateKeyManager.generateKey("test2@example.com", 1024);

        keyIds = privateKeyManager.getKeyIds();
        assertEquals(2, keyIds.size());
    }
}
