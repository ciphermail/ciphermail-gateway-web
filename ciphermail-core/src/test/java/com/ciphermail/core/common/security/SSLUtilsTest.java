/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SSLUtilsTest
{
    private X509Certificate loadCertificate(String filename) {
        return TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(), "certificates/" + filename));
    }

    @Test
    public void testIsValidSSLServerCertificate()
    throws Exception
    {
        // No key usage and extended keu usage
        assertTrue(SSLUtils.isValidSSLServerCertificate(loadCertificate("thawte-freemail-valid-till-091108.cer")));

        // Invalid key usage
        assertFalse(SSLUtils.isValidSSLServerCertificate(loadCertificate("user-trust-intermediate.cer")));

        // Valid extended key usage, no key usage
        assertTrue(SSLUtils.isValidSSLServerCertificate(loadCertificate("gmail-ssl.cer")));

        // Correct key usage, incorrect extended key usage
        assertFalse(SSLUtils.isValidSSLServerCertificate(loadCertificate(
                "Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer")));
    }

    @Test
    public void testGetSystemTrustedRoots()
    throws Exception
    {
        List<X509Certificate> roots = SSLUtils.getSystemTrustedRoots();

        assertTrue(roots.size() > 0);
    }
}
