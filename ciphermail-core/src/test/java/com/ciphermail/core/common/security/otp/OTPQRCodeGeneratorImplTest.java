/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.ImageUtils;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

class OTPQRCodeGeneratorImplTest
{
    private static final ClientSecretIdGenerator clientSecretIdGenerator = new ClientSecretIdGeneratorImpl(
            new HMACOTPGenerator("HmacSHA256"));

    @Test
    void buildQRCodeDefaultValues()
    throws Exception
    {
        OTPQRCodeGeneratorImpl generator = new OTPQRCodeGeneratorImpl(clientSecretIdGenerator);

        byte[] qrCode = generator.createSecretKeyQRCode("secret".getBytes(), "example.com");

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(qrCode));

        Assertions.assertEquals(200, image.getWidth(null));
        Assertions.assertEquals(200, image.getHeight(null));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new RGBLuminanceSource(image.getWidth(null), image.getHeight(null),
                        ImageUtils.getPixels(image))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        JSONObject json = new JSONObject(qrCodeResult.getText());

        Assertions.assertEquals("cs", json.getString("a"));
        Assertions.assertEquals("example.com", json.getString("h"));
        Assertions.assertEquals("fvcigmi", json.getString("k"));
        Assertions.assertEquals("c2VjcmV0", json.getString("s"));

        Assertions.assertEquals("secret", new String(Base64Utils.decode("c2VjcmV0"), StandardCharsets.US_ASCII));
    }

    @Test
    void buildQRCodeNonDefaultValues()
    throws Exception
    {
        OTPQRCodeGeneratorImpl generator = new OTPQRCodeGeneratorImpl(clientSecretIdGenerator);

        generator.setWidth(210);
        generator.setHeight(220);
        generator.setClientSecretApp("other");
        generator.setImageFormat("jpeg");

        byte[] qrCode = generator.createSecretKeyQRCode("othersecret".getBytes(),
                "some-very-long-hostname.some-very-long-domain-name.com");

        BufferedImage image = ImageIO.read(new ByteArrayInputStream(qrCode));

        Assertions.assertEquals(210, image.getWidth(null));
        Assertions.assertEquals(220, image.getHeight(null));

        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new RGBLuminanceSource(image.getWidth(null), image.getHeight(null),
                        ImageUtils.getPixels(image))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);

        JSONObject json = new JSONObject(qrCodeResult.getText());

        Assertions.assertEquals("other", json.getString("a"));
        Assertions.assertEquals("some-very-long-hostname.some-very-long-domain-name.com", json.getString("h"));
        Assertions.assertEquals("45xnavy", json.getString("k"));
        Assertions.assertEquals("b3RoZXJzZWNyZXQ=", json.getString("s"));

        Assertions.assertEquals("othersecret", new String(Base64Utils.decode("b3RoZXJzZWNyZXQ="),
                StandardCharsets.US_ASCII));
    }
}