/*
 * Copyright (c) 2008-2022 CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.common.util.URIUtils.URIType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;

class URIUtilsTest
{
    @Test
    void testURIValidatorFULL()
    {
        Assertions.assertTrue(URIUtils.isValidURI("http://www.example.com", URIType.FULL));
        Assertions.assertTrue(URIUtils.isValidURI("http://www.example.com:80/?", URIType.FULL));
        Assertions.assertTrue(URIUtils.isValidURI("hTTps://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian." +
                                                  "jira.plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213",
                URIType.FULL));
        Assertions.assertTrue(URIUtils.isValidURI("xxp://test.nl#1234", URIType.FULL));
        Assertions.assertTrue(URIUtils.isValidURI("xxp://test:xx@test.nl?x=1#1234", URIType.FULL));
        Assertions.assertTrue(URIUtils.isValidURI("xxp://test:xx@test.nl?x=1#1234", null));
        Assertions.assertFalse(URIUtils.isValidURI("www.example.com", URIType.FULL));
        Assertions.assertFalse(URIUtils.isValidURI("<script>alert(1)</script>", URIType.FULL));
        Assertions.assertFalse(URIUtils.isValidURI("http://<script>", URIType.FULL));

        Assertions.assertFalse(URIUtils.isValidURI("", URIType.BASE));
    }

    @Test
    void testURIValidatorBASE()
    {
        Assertions.assertTrue(URIUtils.isValidURI("http://www.example.com", URIType.BASE));
        Assertions.assertTrue(URIUtils.isValidURI("http://test:123@www.example.com:80", URIType.BASE));
        Assertions.assertTrue(URIUtils.isValidURI("http://test:123@www.example.com:80/", URIType.BASE));
        Assertions.assertFalse(URIUtils.isValidURI("http://test:123@www.example.com:80/?", URIType.BASE));
        Assertions.assertFalse(URIUtils.isValidURI("http://test:123@www.example.com:80?", URIType.BASE));
        Assertions.assertFalse(URIUtils.isValidURI("http://test:123@www.example.com:80/?a", URIType.BASE));
        Assertions.assertFalse(URIUtils.isValidURI("www.example.com", URIType.BASE));
        Assertions.assertFalse(URIUtils.isValidURI("http://www.example.com#123", URIType.BASE));
    }

    @Test
    void testURIValidatorRELATIVE()
    {
        Assertions.assertTrue(URIUtils.isValidURI("/test", URIType.RELATIVE));
        Assertions.assertTrue(URIUtils.isValidURI("/test?x#10", URIType.RELATIVE));
        Assertions.assertTrue(URIUtils.isValidURI("test", URIType.RELATIVE));
        Assertions.assertFalse(URIUtils.isValidURI("http://www.example.com", URIType.RELATIVE));
        Assertions.assertFalse(URIUtils.isValidURI("http://www.example.com?x", URIType.RELATIVE));
        Assertions.assertFalse(URIUtils.isValidURI("http://www.example.com#x", URIType.RELATIVE));
        Assertions.assertFalse(URIUtils.isValidURI("http://www.example.com/123", URIType.RELATIVE));
    }

    @Test
    void testGetBaseURI()
    throws Exception
    {
        Assertions.assertEquals("https://martijn@www.example.com:8443", URIUtils.getBaseURI("https://martijn@www.example.com:8443/test/123?email=xxx#99").toString());

        Assertions.assertEquals("http://www.example.com", URIUtils.getBaseURI("http://www.example.com").toString());
    }

    @Test
    void testParseQuery()
    {
        Map<String, List<String>> params = URIUtils.parseQuery("test1=abc&test1=123&test2=one%26two");

        Assertions.assertEquals(2, params.size());
        List<String> values = params.get("test1");
        Assertions.assertEquals(2, values.size());
        assertThat(values, allOf(hasItem("abc"), hasItem("123")));

        values = params.get("test2");
        Assertions.assertEquals(1, values.size());
        assertThat(values, hasItem("one&two"));
    }
}
