/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.ca.handlers.BuiltInCertificateRequestHandler;
import com.ciphermail.core.common.security.ca.handlers.DelayedBuiltInCertificateRequestHandler;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class, CAImplTest.LocalServices.class})
public class CAImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static final int INITIAL_KEY_STORE_SIZE = 2;

    static class DummyCAProperties implements CAProperties
    {
        @Override
        public @Nonnull Integer getKeyLength() throws HierarchicalPropertiesException {
            return 1024;
        }

        @Override
        public String getIssuerThumbprint()
        {
            return "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" +
                "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";
        }

        @Override
        public @Nonnull Integer getCertificateValidityDays() {
            return 365;
        }

        @Override
        public void setKeyLength(Integer length) {
        }

        @Override
        public void setIssuerThumbprint(String thumbprint) {
        }

        @Override
        public void setCertificateValidityDays(Integer days) {
        }

        @Override
        public void setDefaultCommonName(String commonName) {
        }

        @Override
        public String getDefaultCommonName() {
            return null;
        }

        @Override
        public void setSignatureAlgorithm(CACertificateSignatureAlgorithm signatureAlgorithm) {
        }

        @Override
        public @Nonnull CACertificateSignatureAlgorithm getSignatureAlgorithm() {
            return CACertificateSignatureAlgorithm.SHA256_WITH_RSA_ENCRYPTION;
        }

        @Override
        public String getCRLDistributionPoint() {
            return null;
        }

        @Override
        public void setCRLDistributionPoint(String url) {
        }

        @Override
        public String getDefaultCertificateRequestHandler() {
            return null;
        }

        @Override
        public void setDefaultCertificateRequestHandler(String certificateRequestHandler) {
        }
    }

    static class DummyCSRRequestHandler implements CertificateRequestHandler
    {
        static final String NAME = "DummyCSRRequestHandler";

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public boolean isInstantlyIssued() {
            return false;
        }

        @Override
        public String getCertificateHandlerName() {
            return NAME;
        }

        @Override
        public KeyAndCertificate handleRequest(CertificateRequest request) {
            return null;
        }

        @Override
        public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate)
        throws CAException
        {
            assertNotNull(certificate);

            KeyAndCertificate result = null;

            X509CertificateInspector inspector;

            try {
                inspector = new X509CertificateInspector(certificate);
            }
            catch (Exception e) {
                throw new CAException(e);
            }

            if (inspector.getEmail().contains(request.getEmail()))
            {
                // For this test we set key to null since we do not need it
                result = new KeyAndCertificateImpl(null, certificate);
            }

            return result;
        }

        @Override
        public void cleanup(CertificateRequest request) {
        }
    }

    static class TestDelayedBuiltInCertificateRequestHandler extends DelayedBuiltInCertificateRequestHandler
    {
        private final List<String> cleaned = Collections.synchronizedList(new LinkedList<>());

        public TestDelayedBuiltInCertificateRequestHandler(CertificateRequestHandlerRegistry registry,
            KeyAndCertStore keyAndCertStore, CAPropertiesProvider propertiesProvider,
            KeyAndCertificateIssuer keyAndCertificateIssuer, int steps)
        {
            super(registry, keyAndCertStore, propertiesProvider, keyAndCertificateIssuer, steps);
        }

        @Override
        public void cleanup(CertificateRequest request) {
            cleaned.add(request.getEmail());
        }
    }

    public static class LocalServices
    {
        /*
         * For this test we must override the CA bean to make it a prototype bean so we can better handle the background
         * thread
         */
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        @Bean(CipherMailSystemServices.CA_SERVICE_NAME)
        public CA caService(
                CertificateRequestStore certificateRequestStore,
                CertificateRequestHandlerRegistry certificateRequestHandlerRegistry,
                @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME) KeyAndCertStore keyAndCertStore,
                TransactionOperations transactionOperations,
                SessionManager sessionManager)
        {
            CAImpl ca = new CAImpl(certificateRequestStore, certificateRequestHandlerRegistry, keyAndCertStore,
                    transactionOperations, sessionManager, 5 /* in seconds */, 2592000, new int[] {5});

            // For the tests, the background should run immediately when started
            ca.setThreadStartupDelay(0);

            return ca;
        }
    }

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    @Autowired
    private CertificateRequestStore certificateRequestStore;

    @Autowired
    private SMIMEKeyAndCertificateIssuer sMIMEKeyAndCertificateIssuer;

    @Autowired
    private CA ca;

    private TestDelayedBuiltInCertificateRequestHandler multiStephandler;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key store
                keyAndCertStore.removeAllEntries();

                // Remove pending requests
                List<? extends CertificateRequest> all = certificateRequestStore.getAllRequests(null, null);

                for (CertificateRequest request : all) {
                    certificateRequestStore.deleteRequest(request.getID());
                }

                // Load CA
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

                // initialize key store
                keyStore.load(new FileInputStream(new File(TEST_BASE, "keys/testCA.p12")), "test".toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        CAPropertiesProvider propertiesProvider = DummyCAProperties::new;

        CertificateRequestHandler builtInhandler = new BuiltInCertificateRequestHandler(
                keyAndCertStore,
                propertiesProvider,
                sMIMEKeyAndCertificateIssuer);

        certificateRequestHandlerRegistry.registerHandler(builtInhandler);

        multiStephandler = new TestDelayedBuiltInCertificateRequestHandler(
                certificateRequestHandlerRegistry, keyAndCertStore, propertiesProvider,
                sMIMEKeyAndCertificateIssuer, 3);

        certificateRequestHandlerRegistry.registerHandler(multiStephandler);

        certificateRequestHandlerRegistry.registerHandler(new DummyCSRRequestHandler());

        ((CAImpl) ca).start();
    }

    @After
    public void setUpAfter()
    throws Exception
    {
        // Wait for background thread to stop
        ((CAImpl) ca).stop(DateUtils.MILLIS_PER_SECOND * 30);
    }

    private X509Certificate testRequestCertificate(String requestHandler, boolean immediate, String email)
    {
        return transactionOperations.execute(status ->
        {
            try {
                X500PrincipalBuilder pb = X500PrincipalBuilder.getInstance();

                pb.setCommonName("TEST CA");
                pb.setEmail(email);

                RequestParameters parameters = new RequestParametersImpl();

                parameters.setEmail(email);
                parameters.setSubject(pb.buildPrincipal());
                parameters.setCertificateRequestHandler(requestHandler);
                parameters.setKeyLength(1024);
                parameters.setSignatureAlgorithm(CACertificateSignatureAlgorithm
                        .SHA256_WITH_RSA_ENCRYPTION.getAlgorithmName());
                parameters.setValidity(365);

                CA.RequestResponse issued = ca.requestCertificate(parameters);

                if (immediate)
                {
                    assertNotNull(issued);

                    X509CertStoreEntry entry = keyAndCertStore.getByCertificate(issued.certStoreEntry().getCertificate());

                    assertNotNull(entry);
                    assertNotNull(entry.getKeyAlias());
                }
                else {
                    assertNotNull(issued.request());
                    assertNull(issued.certStoreEntry());
                }

                return Optional.ofNullable(issued.certStoreEntry()).map(
                        X509CertStoreEntry::getCertificate).orElse(null);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void assertKeyAndCertWithEmailCount(String email, long count)
    {
        long found = transactionOperations.execute(status ->
        {
            try {
                return keyAndCertStore.getByEmailCount(email, Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY,
                        MissingKeyAlias.NOT_ALLOWED);
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        }).longValue();

        assertEquals(count, found);
    }

    private void assertRequestStoreSize(int size) {
        transactionOperations.executeWithoutResult(status -> assertEquals(size, certificateRequestStore.getSize()));
    }

    private long getRequestStoreSize() {
        return transactionOperations.execute(status -> certificateRequestStore.getSize()).longValue();
    }

    private void assertKeyAndCertStoreSize(int size) {
        transactionOperations.executeWithoutResult(status -> assertEquals(size, keyAndCertStore.size()));
    }

    private KeyAndCertificate handleRequest(X509Certificate certificate)
    {
        return transactionOperations.execute(status -> {
            try {
                return ca.handleRequest(certificate);
            }
            catch (CAException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testInvalidEmailAddress()
    {
        try {
            testRequestCertificate(BuiltInCertificateRequestHandler.NAME, true, "&^&^&");

            fail();
        }
        catch (UnhandledException e)
        {
            assertTrue(e.getCause() instanceof CAException);

            assertEquals("&^&^& is not a valid email address", e.getCause().getMessage());
        }
    }

    @Test
    public void testIssueCertificateDefaultRequestHandler()
    {
        testRequestCertificate(BuiltInCertificateRequestHandler.NAME, true, "test1@example.com");
        testRequestCertificate(null, true, "test@example.com");

        assertKeyAndCertWithEmailCount("non-existing@example.com", 0);
        assertKeyAndCertWithEmailCount("test1@example.com", 1);
        assertKeyAndCertWithEmailCount("test@example.com", 1);
    }

    @Test(timeout = 10000)
    public void testDelayedIssue()
    {
        ((CAImpl)ca).setDelayTimes(new int[]{1, 0});

        // Wait until notified
        ((CAImpl)ca).setThreadSleepTime(0);

        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test2@example.com");
        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test3@example.com");
        assertRequestStoreSize(2);

        while (getRequestStoreSize() != 0)
        {
            ((CAImpl)ca).kick();

            ThreadUtils.sleepQuietly(100);
        }

        assertRequestStoreSize(0);
        assertKeyAndCertStoreSize(INITIAL_KEY_STORE_SIZE + 2);

        assertKeyAndCertWithEmailCount("test2@example.com", 1);
        assertKeyAndCertWithEmailCount("test3@example.com", 1);
    }

    @Test(timeout = 10000)
    public void testExpire()
    {
        ((CAImpl)ca).setDelayTimes(new int[]{1});

        assertEquals(0, multiStephandler.cleaned.size());

        /*
         * Wait until notified
         */
        ((CAImpl)ca).setThreadSleepTime(0);

        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test4@example.com");
        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test5@example.com");
        assertRequestStoreSize(2);

        ((CAImpl)ca).setExpirationTime(1);

        while (getRequestStoreSize() != 0)
        {
            ((CAImpl)ca).kick();

            ThreadUtils.sleepQuietly(100);
        }

        assertRequestStoreSize(0);
        assertKeyAndCertStoreSize(INITIAL_KEY_STORE_SIZE);

        assertEquals(2, multiStephandler.cleaned.size());
    }

    @Test
    public void testPendingRequest()
    {
        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test@example.com");
        testRequestCertificate(DelayedBuiltInCertificateRequestHandler.NAME, false, "test@example.com");
        assertRequestStoreSize(2);
    }

    @Test
    public void testRequestCertificateUnknownCertificateRequestHandler()
    {
        try {
            testRequestCertificate("unknown", true, "test@example.com");

            fail();
        }
        catch (UnhandledException e)
        {
            assertTrue(e.getCause() instanceof CAException);

            assertEquals("CertificateRequestHandler with name unknown not available", e.getCause().getMessage());
        }
    }

    @Test
    public void testHandleRequest()
    {
        assertNull(testRequestCertificate(DummyCSRRequestHandler.NAME, false, "support@ciphermail.com"));
        assertNull(testRequestCertificate(DummyCSRRequestHandler.NAME, false, "m.brinkers@pobox.com"));
        assertNull(testRequestCertificate(DummyCSRRequestHandler.NAME, false, "m.brinkers@pobox.com"));
        assertNull(testRequestCertificate(DummyCSRRequestHandler.NAME, false, "info@ciphermail.com"));

        assertRequestStoreSize(4);

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/testcertificate.cer"));

        assertNotNull(certificate);

        KeyAndCertificate keyAndCertificate = handleRequest(certificate);

        assertNotNull(keyAndCertificate);
        assertEquals(certificate, keyAndCertificate.getCertificate());

        assertRequestStoreSize(3);

        X509Certificate otherCertificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/invalid-email-addresses.cer"));

        assertNotNull(otherCertificate);

        keyAndCertificate = handleRequest(otherCertificate);

        assertNull(keyAndCertificate);

        keyAndCertificate = handleRequest(certificate);

        assertNotNull(keyAndCertificate);
        assertEquals(certificate, keyAndCertificate.getCertificate());

        assertRequestStoreSize(2);

        keyAndCertificate = handleRequest(certificate);

        assertNull(keyAndCertificate);

        assertRequestStoreSize(2);
    }

    @Test
    public void testHandleRequestUnknownCertificateRequestHandler()
    {
        final String name = "donothinghandler";

        CertificateRequestHandler doNothingHandler = new CertificateRequestHandler() {

            @Override
            public boolean isEnabled() {
                return true;
            }

            @Override
            public boolean isInstantlyIssued() {
                return false;
            }

            @Override
            public String getCertificateHandlerName() {
                return name;
            }

            @Override
            public KeyAndCertificate handleRequest(CertificateRequest request) {
                return null;
            }

            @Override
            public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate) {
                return null;
            }

            @Override
            public void cleanup(CertificateRequest request) {
            }
        };

        // We need to add a handler so we can add a request with some handler and then we can remove it to test
        // what happens if a handler is no longer available. We need to add the handler otherwise we cannot create
        // a request with an unknown handler
        certificateRequestHandlerRegistry.registerHandler(doNothingHandler);

        assertNull(testRequestCertificate(name, false, "m.brinkers@pobox.com"));

        assertTrue(certificateRequestHandlerRegistry.removeHandler(name));

        assertRequestStoreSize(1);

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/testcertificate.cer"));

        assertNotNull(certificate);

        KeyAndCertificate keyAndCertificate = handleRequest(certificate);

        assertNull(keyAndCertificate);
    }
}
