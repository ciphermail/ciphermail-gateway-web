/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.dao;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.crl.X509CRLInspector;
import com.ciphermail.core.common.security.crlstore.hibernate.X509CRLStoreEntity;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CRLStoreDAOTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    throws Exception
    {
        createDAO().removeAllEntries();
    }

    private X509CRLStoreDAO createDAO() {
        return createDAO("test");
    }

    private X509CRLStoreDAO createDAO(String storeName) {
        return X509CRLStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    private X509CRL loadCRL(String filename) {
        return TestUtils.loadX509CRL(new File(TEST_BASE, "crls/" + filename));
    }

    @Test
    public void testGetCRL()
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl = loadCRL("test-ca.crl");

        dao.addCRL(crl);

        assertEquals(crl, dao.getCRL("EDCCF455AD8349CBE6F568E6DCB023F117A5DF93F02E9C3135E0FBB39B12EEA3A5C869E5"
                + "A023895D7FA4E5CC6C43DBAD4B2ECFF346AE7236C63EED4D06446AAA"));

        assertEquals(crl, dao.getCRL("edccf455ad8349cbe6f568e6dcb023f117a5df93f02e9c3135e0fbb39b12eea3a5c869e5"
                + "a023895d7fa4e5cc6c43dbad4b2ecff346ae7236c63eed4d06446aaa"));

        assertNull(dao.getCRL("AAACF455AD8349CBE6F568E6DCB023F117A5DF93F02E9C3135E0FBB39B12EEA3A5C869E5"
                + "A023895D7FA4E5CC6C43DBAD4B2ECFF346AE7236C63EED4D06446AAA"));

        assertNull(otherDAO.getCRL("EDCCF455AD8349CBE6F568E6DCB023F117A5DF93F02E9C3135E0FBB39B12EEA3A5C869E5"
                + "A023895D7FA4E5CC6C43DBAD4B2ECFF346AE7236C63EED4D06446AAA"));
    }

    @Test
    public void testRemove()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl = loadCRL("test-ca.crl");

        dao.addCRL(crl);

        assertEquals(1, dao.size());

        otherDAO.remove(crl);

        assertEquals(1, dao.size());

        dao.remove(crl);

        assertEquals(0, dao.size());
    }

    @Test
    public void testReplace()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl1 = loadCRL("test-ca.crl");
        X509CRL crl2 = loadCRL("test-ca-no-next-update.crl");

        dao.addCRL(crl1);

        assertNotNull(dao.getCRL(X509CRLInspector.getThumbprint(crl1)));

        // Test what happens when we try to replace a CRL by itself. It should not result in an exception
        dao.replace(crl1, crl1);

        assertNotNull(dao.getCRL(X509CRLInspector.getThumbprint(crl1)));

        otherDAO.replace(crl1, crl2);

        assertNotNull(dao.getCRL(X509CRLInspector.getThumbprint(crl1)));

        dao.replace(crl1, crl2);

        assertNull(dao.getCRL(X509CRLInspector.getThumbprint(crl1)));
        assertNotNull(dao.getCRL(X509CRLInspector.getThumbprint(crl2)));
    }

    @Test
    public void testContains()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl1 = loadCRL("test-ca.crl");

        assertFalse(dao.contains(crl1));
        assertFalse(otherDAO.contains(crl1));

        dao.addCRL(crl1);

        assertTrue(dao.contains(crl1));
        assertFalse(otherDAO.contains(crl1));
    }

    @Test
    public void testGetCRLIterator()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl1 = loadCRL("test-ca.crl");
        X509CRL crl2 = loadCRL("max-next-update-time.crl");
        X509CRL crl3 = loadCRL("crl-ca-same-subject-1.crl");
        X509CRL crl4 = loadCRL("test-ca-thisupdate-far-future.crl");

        dao.addCRL(crl1);
        dao.addCRL(crl2);
        dao.addCRL(crl3);
        dao.addCRL(crl4);

        X509CRLSelector crlSelector = null;

        List<X509CRL> crls = CloseableIteratorUtils.toList(dao.getCRLIterator(crlSelector, null, null));
        assertEquals(4, crls.size());

        assertEquals(crl4, crls.get(0));
        assertEquals(crl2, crls.get(1));
        assertEquals(crl3, crls.get(2));
        assertEquals(crl1, crls.get(3));

        crls = CloseableIteratorUtils.toList(otherDAO.getCRLIterator(crlSelector, null, null));
        assertEquals(0, crls.size());

        crlSelector = new X509CRLSelector();

        crlSelector.setIssuers(Collections.singleton(crl1.getIssuerX500Principal()));

        crls = CloseableIteratorUtils.toList(dao.getCRLIterator(crlSelector, null, null));

        assertEquals(2, crls.size());

        // Try non-matching issuer
        // Use an issuer which should not match
        X500PrincipalBuilder x500PrincipalBuilder = X500PrincipalBuilder.getInstance();
        x500PrincipalBuilder.setCommonName("test");

        crlSelector = new X509CRLSelector();

        crlSelector.setIssuers(Collections.singleton(x500PrincipalBuilder.buildPrincipal()));

        crls = CloseableIteratorUtils.toList(dao.getCRLIterator(crlSelector, null, null));

        assertEquals(0, crls.size());
    }

    @Test
    public void testGetCRLStoreIterator()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        X509CRL crl1 = loadCRL("test-ca.crl");
        X509CRL crl2 = loadCRL("Class3SecurityServices_2-thisupdate-201207.crl");
        X509CRL crl3 = loadCRL("crl-ca-same-subject-1.crl");
        X509CRL crl4 = loadCRL("test-ca-thisupdate-far-future.crl");

        dao.addCRL(crl1);
        dao.addCRL(crl2);
        dao.addCRL(crl3);
        dao.addCRL(crl4);

        X509CRLSelector crlSelector = null;

        List<X509CRLStoreEntity> entries = CloseableIteratorUtils.toList(
                dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(4, entries.size());

        assertEquals(crl4, entries.get(0).getCRL());
        assertEquals(crl3, entries.get(1).getCRL());
        assertEquals(crl2, entries.get(2).getCRL());
        assertEquals(crl1, entries.get(3).getCRL());

        // Try first and max results
        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, 1, null));
        assertEquals(3, entries.size());
        assertEquals(crl3, entries.get(0).getCRL());
        assertEquals(crl2, entries.get(1).getCRL());
        assertEquals(crl1, entries.get(2).getCRL());

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, 2, 2));
        assertEquals(2, entries.size());
        assertEquals(crl2, entries.get(0).getCRL());
        assertEquals(crl1, entries.get(1).getCRL());

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, 2));
        assertEquals(2, entries.size());
        assertEquals(crl4, entries.get(0).getCRL());
        assertEquals(crl3, entries.get(1).getCRL());

        // Try different storeName
        entries = CloseableIteratorUtils.toList(otherDAO.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(0, entries.size());

        crlSelector = new X509CRLSelector();

        crlSelector.setIssuers(Collections.singleton(crl1.getIssuerX500Principal()));

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(2, entries.size());

        // Try non-matching issuer
        // Use an issuer which should not match
        X500PrincipalBuilder x500PrincipalBuilder = X500PrincipalBuilder.getInstance();
        x500PrincipalBuilder.setCommonName("test");

        crlSelector = new X509CRLSelector();

        crlSelector.setIssuers(Collections.singleton(x500PrincipalBuilder.buildPrincipal()));

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(0, entries.size());

        // Try multiple issuers
        crlSelector = new X509CRLSelector();

        List<X500Principal> issuers = new LinkedList<>();

        issuers.add(x500PrincipalBuilder.buildPrincipal());
        issuers.add(crl1.getIssuerX500Principal());
        issuers.add(crl2.getIssuerX500Principal());

        crlSelector.setIssuers(issuers);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(3, entries.size());
        assertEquals(crl4, entries.get(0).getCRL());
        assertEquals(crl2, entries.get(1).getCRL());
        assertEquals(crl1, entries.get(2).getCRL());

        // Try date
        crlSelector = new X509CRLSelector();

        Date date = TestUtils.parseDate("01-Dec-2007 07:38:35 GMT");

        crlSelector.setDateAndTime(date);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(1, entries.size());
        assertEquals(crl1, entries.get(0).getCRL());

        // Date too early
        date = TestUtils.parseDate("01-Dec-2006 07:38:35 GMT");

        crlSelector.setDateAndTime(date);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(0, entries.size());

        // Date too late
        date = TestUtils.parseDate("01-Dec-2041 07:38:35 GMT");

        crlSelector.setDateAndTime(date);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(0, entries.size());

        // Only one match
        date = TestUtils.parseDate("01-Dec-2035 07:38:35 GMT");

        crlSelector.setDateAndTime(date);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(1, entries.size());
        assertEquals(crl4, entries.get(0).getCRL());

        // Multiple matches
        date = TestUtils.parseDate("21-Dec-2007 07:38:35 GMT");

        crlSelector.setDateAndTime(date);

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(2, entries.size());
        assertEquals(crl2, entries.get(0).getCRL());
        assertEquals(crl1, entries.get(1).getCRL());

        // Date and issuer
        date = TestUtils.parseDate("21-Dec-2007 07:38:35 GMT");

        crlSelector.setDateAndTime(date);
        crlSelector.setIssuers(Collections.singleton(crl2.getIssuerX500Principal()));

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(1, entries.size());
        assertEquals(crl2, entries.get(0).getCRL());

        // Date and issuer but no match on date
        date = TestUtils.parseDate("01-Dec-2007 07:38:35 GMT");

        crlSelector.setDateAndTime(date);
        crlSelector.setIssuers(Collections.singleton(crl2.getIssuerX500Principal()));

        entries = CloseableIteratorUtils.toList(dao.getCRLStoreIterator(crlSelector, null, null));

        assertEquals(0, entries.size());
    }

    @Test
    public void testGetSize()
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());
        dao.addCRL(loadCRL("test-ca.crl"));
        assertEquals(1, dao.size());
        assertEquals(0, otherDAO.size());
        dao.addCRL(loadCRL("Class3SecurityServices_2-thisupdate-201207.crl"));
        assertEquals(2, dao.size());
        assertEquals(0, otherDAO.size());
    }

    @Test
    public void testRemoveAllEntries()
    throws Exception
    {
        X509CRLStoreDAO dao = createDAO();
        X509CRLStoreDAO otherDAO = createDAO("other-store");

        dao.addCRL(loadCRL("test-ca.crl"));
        dao.addCRL(loadCRL("Class3SecurityServices_2-thisupdate-201207.crl"));
        assertEquals(2, dao.size());
        assertEquals(0, otherDAO.size());
        dao.removeAllEntries();
        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());
    }
}
