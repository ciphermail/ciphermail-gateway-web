/*
 * Copyright (c) 2010-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class SMIMEKeyAndCertificateIssuerTest
{
    private static KeyAndCertificate signer;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        KeyStore keyStore = TestUtils.loadKeyStore(new File(TestUtils.getTestDataDir(), "keys/testCA.p12"),
                "test");

        signer = new KeyAndCertificateImpl((PrivateKey) keyStore.getKey("ca", null),
                (X509Certificate) keyStore.getCertificate("ca"));
    }

    @Test
    public void testEmailAddress()
    throws Exception
    {
        RequestParameters request = new RequestParametersImpl();

        X500PrincipalBuilder subjectBuilder = X500PrincipalBuilder.getInstance();

        subjectBuilder.setCommonName("testCN");
        subjectBuilder.setEmail("subject@example.com");

        request.setCRLDistributionPoint("http://127.0.0.1");
        request.setEmail("test@example.com");
        request.setKeyLength(1024);
        request.setSignatureAlgorithm("SHA256WithRSAEncryption");
        request.setValidity(365);
        request.setSubject(subjectBuilder.buildPrincipal());

        SMIMEKeyAndCertificateIssuer issuer = new SMIMEKeyAndCertificateIssuer(new StandardSerialNumberGenerator());

        KeyAndCertificate issued = issuer.issueKeyAndCertificate(request, signer);

        assertNotNull(issued);
        assertNotNull(issued.getCertificate());
        assertNotNull(issued.getPrivateKey());

        X509CertificateInspector inspector = new X509CertificateInspector(issued.getCertificate());

        assertEquals(2, inspector.getEmail().size());
        assertTrue(inspector.getEmail().contains("test@example.com"));
        assertTrue(inspector.getEmail().contains("subject@example.com"));

        assertEquals(1, inspector.getEmailFromDN().size());
        assertEquals(1, inspector.getEmailFromAltNames().size());

        assertEquals("subject@example.com", StringUtils.join(inspector.getEmailFromDN(), ","));
        assertEquals("test@example.com", StringUtils.join(inspector.getEmailFromAltNames(), ","));
    }
}
