/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.PKIXCertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.SMIMEExtendedKeyUsageCertPathChecker;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.jce.X509CertStoreParameters;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PKIXCRLPathBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509CertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt x509RootStore;

    @Autowired
    private X509CRLStoreExt x509CRLStore;

    @Autowired
    private TrustAnchorBuilder trustAnchorBuilder;

    private CertStore certStore;
    private X509CertStoreParameters certStoreParams;
    private X509CertStoreParameters rootStoreParams;

    @Before
    public void setup()
    throws Exception
    {
        certStoreParams = new X509CertStoreParameters(x509CertStore, x509CRLStore);

        certStore = CertStore.getInstance(CipherMailProvider.DATABASE_CERTSTORE, certStoreParams,
                CipherMailProvider.PROVIDER);

        rootStoreParams = new X509CertStoreParameters(x509RootStore);

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStoreParams.getCertStore().removeAllEntries();
                rootStoreParams.getCertStore().removeAllEntries();
                certStoreParams.getCRLStore().removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : CertificateUtils.readCertificates(file))
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private CertificatePathBuilder getCertPathBuilder()
    throws CertStoreException
    {
        CertificatePathBuilder builder = new PKIXCertificatePathBuilder();

        builder.setTrustAnchors(trustAnchorBuilder.getTrustAnchors());
        builder.addCertPathChecker(new SMIMEExtendedKeyUsageCertPathChecker());
        builder.addCertStore(certStore);
        builder.setRevocationEnabled(false);

        return builder;
    }

    @Test
    public void testBuildCRLPath()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"),
                        rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                X509CRL crl = TestUtils.loadX509CRL(new File(TEST_BASE, "crls/test-ca.crl"));

                CertificatePathBuilder certPathBuilder = getCertPathBuilder();

                PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);

                CertPathBuilderResult result = crlPathBuilder.buildPath(crl);

                assertTrue(result instanceof PKIXCertPathBuilderResult);

                PKIXCertPathBuilderResult pkixResult = (PKIXCertPathBuilderResult) result;

                CertPath path = pkixResult.getCertPath();
                TrustAnchor trustAnchor = pkixResult.getTrustAnchor();

                assertNotNull(path);
                List<? extends Certificate> certificates = path.getCertificates();
                // should only contain the CA cert
                assertEquals(1, certificates.size());
                assertEquals("115FCAD6B536FD8D49E72922CD1F0DA", X509CertificateInspector.getSerialNumberHex(
                        (X509Certificate) certificates.get(0)));

                assertNotNull(trustAnchor);
                assertEquals("115FCAC409FB2022B7D06920A00FE42", X509CertificateInspector.getSerialNumberHex(
                        trustAnchor.getTrustedCert()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildCRLPathTrustAnchor()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"),
                        rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                X509CRL crl = TestUtils.loadX509CRL(new File(TEST_BASE, "crls/test-root-ca-not-revoked.crl"));

                CertificatePathBuilder certPathBuilder = getCertPathBuilder();

                PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);

                CertPathBuilderResult result = crlPathBuilder.buildPath(crl);

                assertTrue(result instanceof PKIXCertPathBuilderResult);

                PKIXCertPathBuilderResult pkixResult = (PKIXCertPathBuilderResult) result;

                CertPath path = pkixResult.getCertPath();
                TrustAnchor trustAnchor = pkixResult.getTrustAnchor();

                assertNotNull(path);
                List<? extends Certificate> certificates = path.getCertificates();
                assertEquals(0, certificates.size());

                assertNotNull(trustAnchor);
                assertEquals("115FCAC409FB2022B7D06920A00FE42", X509CertificateInspector.getSerialNumberHex(
                        trustAnchor.getTrustedCert()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildCRLPathNoPath()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add root
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"),
                        rootStoreParams.getCertStore());

                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-ca.cer"),
                        certStoreParams.getCertStore());
                addCertificates(new File(TEST_BASE, "certificates/testCertificates.p7b"),
                        certStoreParams.getCertStore());

                X509CRL crl = TestUtils.loadX509CRL(new File(TEST_BASE, "crls/itrus.com.cn.crl"));

                CertificatePathBuilder certPathBuilder = getCertPathBuilder();

                PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);

                try {
                    crlPathBuilder.buildPath(crl);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    assertEquals(PKIXCRLPathBuilder.CRL_ISSUER_NOT_FOUND, e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testBuildCRLPathNoRootShouldNotBeValid()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // add certificates
                addCertificates(new File(TEST_BASE, "certificates/user-trust-intermediate.cer"),
                        certStoreParams.getCertStore());

                X509CRL crl = TestUtils.loadX509CRL(new File(TEST_BASE, "crls/user-trust-intermediate.crl"));

                CertificatePathBuilder certPathBuilder = getCertPathBuilder();

                PKIXCRLPathBuilder crlPathBuilder = new PKIXCRLPathBuilder(certPathBuilder);

                try {
                    crlPathBuilder.buildPath(crl);

                    fail();
                }
                catch(CertPathBuilderException e) {
                    assertEquals(PKIXCertificatePathBuilder.NO_ROOTS_ERROR_MESSAGE, e.getMessage());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
