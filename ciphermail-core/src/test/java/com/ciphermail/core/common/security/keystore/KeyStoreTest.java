/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateInspector;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.keystore.jce.DatabaseKeyStoreLoadStoreParameter;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.crypto.KeyGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class KeyStoreTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    private KeyStore databaseKeyStore;
    private KeyStore databaseKeyStore2;

    private Key aesSecretKey;
    private Key desSecretKey;

    @Before
    public void setup()
    throws Exception
    {
        CipherMailProvider.initialize(sessionManager);

        databaseKeyStore = KeyStore.getInstance(CipherMailProvider.DATABASE_KEYSTORE, CipherMailProvider.PROVIDER);
        databaseKeyStore.load(null);

        databaseKeyStore2 = KeyStore.getInstance(CipherMailProvider.DATABASE_KEYSTORE, CipherMailProvider.PROVIDER);
        databaseKeyStore2.load(new DatabaseKeyStoreLoadStoreParameter("other", sessionManager));

        aesSecretKey = KeyGenerator.getInstance("AES").generateKey();
        desSecretKey = KeyGenerator.getInstance("DES").generateKey();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                databaseKeyStore.setKeyEntry("aes", aesSecretKey, null, null);
                databaseKeyStore.setKeyEntry("protected.aes", aesSecretKey, "test".toCharArray(), null);

                databaseKeyStore.setKeyEntry("des", desSecretKey, null, null);
                databaseKeyStore.setKeyEntry("protected.des", desSecretKey, "test".toCharArray(), null);

                loadPKCS12(new File(TEST_BASE, "keys/testCertificates.p12"), "test",
                        databaseKeyStore, true);
                loadPKCS12(new File(TEST_BASE, "keys/testCertificates.p12"), "test",
                        databaseKeyStore2, false);
                loadPKCS12(new File(TEST_BASE, "keys/ca-no-crlsign.pfx"), "test",
                        databaseKeyStore2, true);
                loadPKCS12(new File(TEST_BASE, "keys/ca-same-subject.pfx"), "test",
                        databaseKeyStore2, true);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void loadPKCS12(File file, String password, KeyStore destination, boolean storeKey)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        keyStore.load(new FileInputStream(file), password.toCharArray());

        Enumeration<String> aliases = keyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            Key key = keyStore.getKey(alias, null);
            Certificate certificate = keyStore.getCertificate(alias);
            Certificate[] chain = keyStore.getCertificateChain(alias);

            if (storeKey && key != null)
            {
                destination.setKeyEntry(alias, key, null, chain);

                // again but now password protected
                destination.setKeyEntry("protected." + alias, key, "test".toCharArray(), chain);
            }
            else {
                destination.setCertificateEntry(alias, certificate);
            }
        }
    }

    @Test
    public void testSecretKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Key key = databaseKeyStore.getKey("aes", null);
                assertEquals(aesSecretKey, key);

                key = databaseKeyStore.getKey("protected.aes", "test".toCharArray());
                assertEquals(aesSecretKey, key);

                key = databaseKeyStore.getKey("des", null);
                assertEquals(desSecretKey, key);

                key = databaseKeyStore.getKey("protected.des", "test".toCharArray());
                assertEquals(desSecretKey, key);
            }
            catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAliases()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Enumeration<String> aliasesEnum = databaseKeyStore.aliases();

                Set<String> aliases = new HashSet<>();

                int size = 0;

                while (aliasesEnum.hasMoreElements())
                {
                    String alias = aliasesEnum.nextElement();

                    aliases.add(alias);

                    size++;
                }

                assertEquals(46, size);

                // 20 * 2 (key entries also get a protect.* alias) + 2 (root&ca) + 4 secret keys =  46
                assertEquals(46, aliases.size());
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testContainsAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertTrue(databaseKeyStore.containsAlias("ca"));
                assertTrue(databaseKeyStore.containsAlias("rsa2048"));
                assertFalse(databaseKeyStore.containsAlias("non-existing-alias"));
                assertTrue(databaseKeyStore.containsAlias("protected.rsa2048"));
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateByAlias()
    {
        String alias = "ValidCertificate";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Certificate certificate = databaseKeyStore.getCertificate(alias);

                CertificateInspector inspector = new CertificateInspector(certificate);

                assertNotNull(certificate);
                assertTrue(certificate instanceof X509Certificate);
                assertEquals("F9DD44188EB18B5B91BF4C459A6A40A183A5DD43", inspector.getThumbprint(Digest.SHA1));
            }
            catch (KeyStoreException | CertificateEncodingException | NoSuchAlgorithmException |
                    NoSuchProviderException e)
            {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateByNonExistingAlias()
    {
        String alias = "non-existing";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Certificate certificate = databaseKeyStore.getCertificate(alias);

                assertNull(certificate);
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateByNullAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertNull(databaseKeyStore.getCertificate(null));
            }
            catch (KeyStoreException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    public void testGetCertificateAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Certificate certificate = databaseKeyStore.getCertificate("protected.UppercaseEmail");

                String alias = databaseKeyStore.getCertificateAlias(certificate);

                // the protected. or without prefix alias can be returned because they both
                // contain the same certificate
                assertTrue(alias.endsWith("UppercaseEmail"));
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateAliasNonExisting()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "certificates/dod-mega-crl.cer");

                X509Certificate certificate = TestUtils.loadCertificate(file);

                String alias = databaseKeyStore.getCertificateAlias(certificate);

                assertNull(alias);
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateChain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Certificate[] chain = databaseKeyStore.getCertificateChain("protected.NoExtendedKeyUsage");

                assertNotNull(chain);
                assertEquals(3, chain.length);

                chain = databaseKeyStore.getCertificateChain("root");

                assertNull(chain);
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificateChainNonExistingAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Certificate[] chain = databaseKeyStore.getCertificateChain("non-existing");

                assertNull(chain);
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCreationDate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Enumeration<String> aliasesEnum = databaseKeyStore.aliases();

                while (aliasesEnum.hasMoreElements())
                {
                    String alias = aliasesEnum.nextElement();

                    Date date = databaseKeyStore.getCreationDate(alias);

                    assertNotNull(date);

                    assertTrue(new Date().getTime() - date.getTime() < 3600 * 1000);
                }
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCreationdateNonExistingAlias()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Date date = databaseKeyStore.getCreationDate("non-existing");

                assertNull(date);
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

                KeyStore.Entry entry = databaseKeyStore.getEntry("protected.ValidCertificate", passwd);

                assertNotNull(entry);
            }
            catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetEntryWrongPassword()
    {
        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("wrong-password".toCharArray());

        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    // this should throw an UnrecoverableEntryException
                    databaseKeyStore.getEntry("protected.ValidCertificate", passwd);

                    fail();
                }
                catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (UnhandledException e) {
            assertTrue(e.getCause() instanceof UnrecoverableEntryException);
        }
    }

    @Test
    public void testGetEntryNonExistingAlias()
    {
        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStore.Entry entry = databaseKeyStore.getEntry("non-existing", passwd);

                assertNull(entry);
            }
            catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Key key1 = databaseKeyStore.getKey("ValidCertificate", null);

                assertNotNull(key1);
                assertTrue(key1 instanceof PrivateKey);

                Key key2 = databaseKeyStore.getKey("protected.ValidCertificate", "test".toCharArray());

                assertNotNull(key2);
                assertTrue(key2 instanceof PrivateKey);

                assertEquals(key1, key2);
            }
            catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetKeyPasswordWithNonProtectedEntry()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    // key is not a protected key so an exception should be thrown
                    databaseKeyStore.getKey("ValidCertificate", "test".toCharArray());

                    fail();
                }
                catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (UnhandledException e) {
            assertTrue(e.getCause() instanceof UnrecoverableEntryException);
        }
    }

    @Test
    public void testGetKeyWrongPassword()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    // incorrect password
                    databaseKeyStore.getKey("protected.ValidCertificate", "incorrect password".toCharArray());

                    fail();
                }
                catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch (UnhandledException e) {
            assertTrue(e.getCause() instanceof UnrecoverableEntryException);
        }
    }

    @Test
    public void testGetIsCertificateEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertTrue(databaseKeyStore.isCertificateEntry("ca"));
                assertFalse(databaseKeyStore.isCertificateEntry("ValidCertificate"));
                assertFalse(databaseKeyStore.isCertificateEntry("protected.ValidCertificate"));
                assertFalse(databaseKeyStore.isCertificateEntry("non-existing"));
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetIsKeyEntry()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertFalse(databaseKeyStore.isKeyEntry("ca"));
                assertTrue(databaseKeyStore.isKeyEntry("ValidCertificate"));
                assertTrue(databaseKeyStore.isKeyEntry("protected.ValidCertificate"));
                assertFalse(databaseKeyStore.isKeyEntry("non-existing"));
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSize()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // 20 * 2 (key entries also get a protect.* alias) + 2 (root&ca) + 4 secret keys =  46
                assertEquals(46, databaseKeyStore.size());
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSizeKeyStore2()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(28, databaseKeyStore2.size());
            }
            catch (KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetAliasesKeyStore2()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Enumeration<String> aliasEnum = databaseKeyStore2.aliases();

                int i = 0;

                int certCount = 0;
                int keyCount = 0;

                while (aliasEnum.hasMoreElements())
                {
                    i++;

                    String alias = aliasEnum.nextElement();

                    if (databaseKeyStore2.isCertificateEntry(alias))
                    {
                        Certificate certificate = databaseKeyStore2.getCertificate(alias);

                        assertNotNull(certificate);

                        certCount++;
                    }
                    else if (databaseKeyStore2.isKeyEntry(alias))
                    {
                        KeyStore.Entry entry = databaseKeyStore2.getEntry(alias,
                                alias.startsWith("protected.") ?
                                    new KeyStore.PasswordProtection("test".toCharArray()) :
                                    new KeyStore.PasswordProtection(null));

                        assertNotNull(entry);

                        keyCount++;
                    }
                    else {
                        fail();
                    }
                }

                assertEquals(28, i);
                assertEquals(22, certCount);
                assertEquals(6, keyCount);
            }
            catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableEntryException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
