/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Properties;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class HierarchicalPropertiesUtilsTest
{
    private static HierarchicalProperties properties;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        properties = new StandardHierarchicalProperties("properties", null, new Properties());
    }

    @Before
    public void setup()
    throws Exception
    {
        properties.deleteAll();
    }

    @Test
    public void testCopyPropertiesRegEx()
    throws Exception
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123);
        properties.setProperty("someProp", "someValue");
        properties.setProperty("someOtherProp", "someOtherValue");
        properties.setProperty("protected.test", "protectedValue");

        assertEquals(4, properties.getProperyNames(false).size());
        assertEquals("123", properties.getProperty("int"));
        assertEquals("someValue", properties.getProperty("someProp"));
        assertEquals("someOtherValue", properties.getProperty("someOtherProp"));
        assertEquals("protectedValue", properties.getProperty("protected.test"));

        HierarchicalProperties other = new StandardHierarchicalProperties(
                "other", null, new Properties());

        HierarchicalPropertiesUtils.copyProperties(properties, other, Pattern.compile("(?i)^protected\\..*"));

        assertEquals(3, other.getProperyNames(false).size());
        assertEquals("123", other.getProperty("int"));
        assertEquals("someValue", other.getProperty("someProp"));
        assertEquals("someOtherValue", other.getProperty("someOtherProp"));
    }

    @Test
    public void testCopyProperties()
    throws Exception
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123);
        properties.setProperty("someProp", "someValue");
        properties.setProperty("someOtherProp", "someOtherValue");
        properties.setProperty("protected.test", "protectedValue");

        assertEquals(4, properties.getProperyNames(false).size());
        assertEquals("123", properties.getProperty("int"));
        assertEquals("someValue", properties.getProperty("someProp"));
        assertEquals("someOtherValue", properties.getProperty("someOtherProp"));
        assertEquals("protectedValue", properties.getProperty("protected.test"));

        HierarchicalProperties other = new StandardHierarchicalProperties(
                "other", null, new Properties());

        HierarchicalPropertiesUtils.copyProperties(properties, other, null);

        assertEquals(4, other.getProperyNames(false).size());
        assertEquals("123", other.getProperty("int"));
        assertEquals("someValue", other.getProperty("someProp"));
        assertEquals("someOtherValue", other.getProperty("someOtherProp"));
        assertEquals("protectedValue", other.getProperty("protected.test"));
    }

    @Test
    public void testIntegerProperty()
    throws Exception
    {
        HierarchicalPropertiesUtils.setInteger(properties, "int", 123);

        assertEquals(123, (int) HierarchicalPropertiesUtils.getInteger(properties, "int", 456));
        assertNull(HierarchicalPropertiesUtils.getInteger(properties, "xxx", null));
        assertEquals(456, (int) HierarchicalPropertiesUtils.getInteger(properties, "xxx", 456));
    }

    @Test
    public void testLongProperty()
    throws Exception
    {
        HierarchicalPropertiesUtils.setLong(properties, "long", 123L);

        assertEquals(123L, (long) HierarchicalPropertiesUtils.getLong(properties, "long", 890L));
        assertNull(HierarchicalPropertiesUtils.getLong(properties, "xxx", null));
        assertEquals(890L, (long) HierarchicalPropertiesUtils.getLong(properties, "xxx", 890L));
    }

    @Test
    public void testBooleanProperty()
    throws Exception
    {
        HierarchicalPropertiesUtils.setBoolean(properties, "boolean", true);

        assertEquals(true, HierarchicalPropertiesUtils.getBoolean(properties, "boolean", true));
        assertNull(HierarchicalPropertiesUtils.getBoolean(properties, "xxx", null));
        assertEquals(true, HierarchicalPropertiesUtils.getBoolean(properties, "xxx", true));
    }
}
