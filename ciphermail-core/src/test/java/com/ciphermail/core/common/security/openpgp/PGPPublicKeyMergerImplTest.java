/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorResult;
import com.ciphermail.core.common.security.openpgp.validator.RevocationValidator;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.junit.Test;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPPublicKeyMergerImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final RevocationValidator revocationValidator =
            new RevocationValidator(new PGPSignatureValidatorImpl());

    @Test
    public void testMergeRevocation()
    throws Exception
    {
        PGPPublicKeyMerger merger = new PGPPublicKeyMergerImpl();

        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        List<PGPPublicKey> revokedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-revoked.gpg.asc"));

        PGPPublicKey revokedMaster = revokedKeys.get(0);

        Context context = new ContextImpl();

        PGPPublicKeyValidatorResult v1 = revocationValidator.validate(master, context);

        assertTrue(v1.isValid());

        PGPPublicKeyValidatorResult v2 = revocationValidator.validate(revokedMaster, context);

        assertFalse(v2.isValid());
        assertEquals("Key with key id 4EC4E8813E11A9A0 is revoked.", v2.getFailureMessage());

        PGPPublicKey merged = merger.merge(master, revokedMaster);

        assertEquals(master.getKeyID(), merged.getKeyID());

        PGPPublicKeyValidatorResult v3 = revocationValidator.validate(merged, context);

        assertFalse(v3.isValid());
        assertEquals("Key with key id 4EC4E8813E11A9A0 is revoked.", v3.getFailureMessage());
    }

    @Test
    public void testMergeUserID()
    throws Exception
    {
        PGPPublicKeyMerger merger = new PGPPublicKeyMergerImpl();

        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        assertEquals("test key djigzo (this is a test key) <test@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(master), ","));

        List<PGPPublicKey> updatedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-additional-userid.gpg.asc"));

        PGPPublicKey updated = updatedKeys.get(0);

        assertEquals("test key djigzo (this is a test key) <test@example.com>,additional user ID <test2@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(updated), ","));

        PGPPublicKey merged = merger.merge(master, updated);

        assertEquals(master.getKeyID(), merged.getKeyID());

        assertEquals("test key djigzo (this is a test key) <test@example.com>,additional user ID <test2@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(merged), ","));

        PGPSignatureValidator signatureValidator = new PGPSignatureValidatorImpl();

        int i = 0;

        for (byte[] userID : PGPPublicKeyInspector.getUserIDs(merged))
        {
            Iterator<?> sigIt = merged.getSignaturesForID(userID);

            while (sigIt.hasNext())
            {
                PGPSignature signature = (PGPSignature) sigIt.next();

                assertTrue(signatureValidator.validateUserIDSignature(merged, userID, signature));

                i++;
            }
        }

        assertEquals(2, i);
    }

    @Test
    public void testMergeSkipUserID()
    throws Exception
    {
        PGPPublicKeyMergerImpl merger = new PGPPublicKeyMergerImpl();

        merger.setSkipUserIDs(true);

        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        assertEquals("test key djigzo (this is a test key) <test@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(master), ","));

        List<PGPPublicKey> updatedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-additional-userid.gpg.asc"));

        PGPPublicKey updated = updatedKeys.get(0);

        assertEquals("test key djigzo (this is a test key) <test@example.com>,additional user ID <test2@example.com>",
                StringUtils.join(PGPPublicKeyInspector.getUserIDsAsStrings(updated), ","));

        assertNull(merger.merge(master, updated));

        // Revocation should still be handled
        List<PGPPublicKey> revokedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-revoked.gpg.asc"));

        PGPPublicKey revokedMaster = revokedKeys.get(0);

        PGPPublicKey merged = merger.merge(master, revokedMaster);

        assertEquals(master.getKeyID(), merged.getKeyID());
    }

    @Test
    public void testMergeRevocationSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        Context context = new ContextImpl();

        PGPPublicKeyValidatorResult v1 = revocationValidator.validate(master, context);

        assertTrue(v1.isValid());

        PGPSignatureList revocationSignatureList = PGPKeyUtils.readRevocationCertificate(new File(TEST_BASE,
                "test@example.com-revocation-cert.gpg.asc"));

        assertEquals(1, revocationSignatureList.size());

        PGPPublicKeyMerger merger = new PGPPublicKeyMergerImpl();

        PGPPublicKey merged = merger.merge(master, revocationSignatureList);

        PGPPublicKeyValidatorResult v2 = revocationValidator.validate(merged, context);

        assertFalse(v2.isValid());
        assertEquals("Key with key id 4EC4E8813E11A9A0 is revoked.", v2.getFailureMessage());
    }

    @Test
    public void testMergeNothing()
    throws Exception
    {
        PGPPublicKeyMerger merger = new PGPPublicKeyMergerImpl();

        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey master = keys.get(0);

        assertNull(merger.merge(master, master));

        PGPSignatureList revocationSignatureList = PGPKeyUtils.readRevocationCertificate(new File(TEST_BASE,
                "test@example.com-revocation-cert.gpg.asc"));

        PGPPublicKey merged = merger.merge(master, revocationSignatureList);

        assertNotNull(merged);

        assertNull(merger.merge(merged, revocationSignatureList));
    }

    @Test
    public void testMergeUserIDRevocationSignature()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        List<PGPSignature> sigs = PGPSignatureInspector.getSignaturesForUserID(masterKey,
                MiscStringUtils.getBytesUTF8("test key djigzo (this is a test key) <test@example.com>"));

        assertEquals(1, sigs.size());
        assertEquals(0x13, sigs.get(0).getSignatureType());

        List<PGPPublicKey> updatedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-new-primary-old-primary-revoked.gpg.asc"));

        PGPPublicKey updatedMasterKey = updatedKeys.get(0);

        List<PGPSignature> updatedSigs = PGPSignatureInspector.getSignaturesForUserID(updatedMasterKey,
                MiscStringUtils.getBytesUTF8("test key djigzo (this is a test key) <test@example.com>"));

        assertEquals(2, updatedSigs.size());
        assertEquals(0x30, updatedSigs.get(0).getSignatureType());
        assertEquals(0x13, updatedSigs.get(1).getSignatureType());

        PGPPublicKeyMerger merger = new PGPPublicKeyMergerImpl();

        PGPPublicKey merged = merger.merge(masterKey, updatedMasterKey);

        List<PGPSignature> mergedSigs = PGPSignatureInspector.getSignaturesForUserID(merged,
                MiscStringUtils.getBytesUTF8("test key djigzo (this is a test key) <test@example.com>"));

        // The updates key generated an updates positive signature for the User ID and a certification revocation so
        // there are now 3 sigs
        assertEquals(3, mergedSigs.size());
        assertEquals(0x13, mergedSigs.get(0).getSignatureType());
        assertEquals(0x30, mergedSigs.get(1).getSignatureType());
        assertEquals(0x13, mergedSigs.get(2).getSignatureType());
    }
}
