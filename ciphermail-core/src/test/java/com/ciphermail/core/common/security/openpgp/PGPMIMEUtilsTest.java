/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class PGPMIMEUtilsTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testGetPGPMIMEType()
    throws Exception
    {
        assertEquals(PGPMIMEType.ENCRYPTED, PGPMIMEUtils.getPGPMIMEType("multipart/encrypted; protocol=\"application/" +
        		"pgp-encrypted\"; boundary=\"sfLiGUh3MQ8ltGpUkk3WNQ17U0qpaTVsE\""));

        assertEquals(PGPMIMEType.SIGNED, PGPMIMEUtils.getPGPMIMEType("multipart/signed; micalg=pgp-sha1; " +
        		"protocol=\"application/pgp-signature\"; boundary=\"TfQn8OqbXQB93hsKCF25Rupb9nLKPgaQB\""));

        assertNull(PGPMIMEUtils.getPGPMIMEType("application/octet-stream"));
    }

    @Test
    public void testDissectPGPMIMESigned()
    throws Exception
    {
        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) MailUtils.loadMessage(
                new File(TEST_BASE, "mail/PGP-MIME-signed-with-attachment.eml")).getContent());

        assertEquals(2, parts.length);
        assertTrue(parts[0].isMimeType("multipart/mixed"));
        assertTrue(parts[1].isMimeType("application/pgp-signature"));

        // Check a non PGP/MIME email
        try {
            PGPMIMEUtils.dissectPGPMIMESigned((Multipart) MailUtils.loadMessage(
                    new File(TEST_BASE, "mail/normal-message-with-attach.eml")).getContent());

            fail();
        }
        catch(MessagingException e) {
            assertEquals("PGP/MIME signature part not found.", e.getMessage());
        }

    }
}
