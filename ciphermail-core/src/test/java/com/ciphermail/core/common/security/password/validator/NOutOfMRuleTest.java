/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password.validator;

import org.junit.Test;
import org.passay.AllowedRegexRule;
import org.passay.PasswordData;
import org.passay.Rule;
import org.passay.RuleResult;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class NOutOfMRuleTest
{

    /*
    ^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$

    ^                 # start-of-string
    (?=.*[0-9])       # a digit must occur at least once
    (?=.*[a-z])       # a lower case letter must occur at least once
    (?=.*[A-Z])       # an upper case letter must occur at least once
    (?=.*[@#$%^&+=])  # a special character must occur at least once
    (?=\S+$)          # no whitespace allowed in the entire string
    .{8,}             # anything, at least eight places though
    $                 # end-of-string
*/
    private static final Rule digitRule = new AllowedRegexRule("(?U)^(?=.*\\p{Digit}).{8,}$");
    private static final Rule lowercaseRule = new AllowedRegexRule("(?U)^(?=.*\\p{Lower}).{8,}$");
    private static final Rule uppercaseRule = new AllowedRegexRule("(?U)^(?=.*\\p{Upper}).{8,}$");

    @Test
    public void testNOutOfMDefaultWeight()
    {
        NOutOfMRule nOutOfMRule = new NOutOfMRule(2, new WeightedRuleImpl(1, digitRule),
                new WeightedRuleImpl(1, lowercaseRule), new WeightedRuleImpl(1, uppercaseRule));

        RuleResult result = nOutOfMRule.validate(new PasswordData("12345678"));
        assertFalse(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("1234567A"));
        assertTrue(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("123456a8"));
        assertTrue(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("abcdefgH"));
        assertTrue(result.isValid());

        nOutOfMRule = new NOutOfMRule(3, new WeightedRuleImpl(1, digitRule), new WeightedRuleImpl(1, lowercaseRule),
                new WeightedRuleImpl(1, uppercaseRule));

        result = nOutOfMRule.validate(new PasswordData("123456a8"));
        assertFalse(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("123456aA"));
        assertTrue(result.isValid());
    }

    @Test
    public void testNOutOfMWeightTest()
    {
        NOutOfMRule nOutOfMRule = new NOutOfMRule(2, new WeightedRuleImpl(2, digitRule),
                new WeightedRuleImpl(1, lowercaseRule), new WeightedRuleImpl(1, uppercaseRule));

        RuleResult result = nOutOfMRule.validate(new PasswordData("12345678"));
        assertTrue(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("1234567"));
        assertFalse(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("aBcDeFgH"));
        assertTrue(result.isValid());
    }

    @Test
    public void testNOutOfMMinWeigthMoreThanRulesTest()
    {
        NOutOfMRule nOutOfMRule = new NOutOfMRule(4, new WeightedRuleImpl(2, digitRule),
                new WeightedRuleImpl(1, lowercaseRule), new WeightedRuleImpl(1, uppercaseRule));

        RuleResult result = nOutOfMRule.validate(new PasswordData("12345678"));
        assertFalse(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("1234567"));
        assertFalse(result.isValid());

        result = nOutOfMRule.validate(new PasswordData("aB123456"));
        assertTrue(result.isValid());
    }
}
