/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.keystore.KeyStoreKeyProvider;
import com.ciphermail.core.test.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.List;

class SMIMEInspectorImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static SecurityFactory securityFactory;
    private static KeyStoreKeyProvider keyProvider;

    @BeforeAll
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        KeyStore keyStore = loadKeyStore(new File(TEST_BASE, "keys/testCertificates.p12"), "test");

        keyProvider = new KeyStoreKeyProvider(keyStore, "test");
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    @Test
    void testClearSignedNoCertificates()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/signed-no-certificates.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        Assertions.assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNotNull(signedInspector);
        Assertions.assertEquals(0, signedInspector.getCertificates().size());
        Assertions.assertEquals(0, signedInspector.getCRLs().size());
        Assertions.assertEquals(1, signedInspector.getSigners().size());

        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();

        Assertions.assertNotNull(mimeBodyPart);
        Assertions.assertTrue(mimeBodyPart.isMimeType("text/plain"));
        Assertions.assertEquals("test", ((String) mimeBodyPart.getContent()).trim());

        message = inspector.getContentAsMimeMessage();

        Assertions.assertNotNull(message);
        Assertions.assertTrue(message.isMimeType("text/plain"));
        Assertions.assertEquals("test", ((String) message.getContent()).trim());
    }

    @Test
    void testSMIMECapabilities()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/signed-no-certificates.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        Assertions.assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNotNull(signedInspector);

        List<SignerInfo> signers = signedInspector.getSigners();
        Assertions.assertEquals(1, signers.size());

        SignerInfo signer = signers.get(0);

        List<SMIMECapabilityInfo> capabilities = SMIMECapabilitiesInspector.inspect(signer.getSignedAttributes());

        Assertions.assertEquals(11, capabilities.size());

        Assertions.assertEquals("AES256_CBC", capabilities.get(0).toString());
        Assertions.assertEquals("AES192_CBC", capabilities.get(1).toString());
        Assertions.assertEquals("AES128_CBC", capabilities.get(2).toString());
        Assertions.assertEquals("DES_EDE3_CBC", capabilities.get(3).toString());
        Assertions.assertEquals("1.2.840.113549.3.2, 128", capabilities.get(4).toString());
        Assertions.assertEquals("1.3.6.1.4.1.188.7.1.1.2", capabilities.get(5).toString());
        Assertions.assertEquals("1.2.840.113533.7.66.10, 128", capabilities.get(6).toString());
        Assertions.assertEquals("1.2.392.200011.61.1.1.1.4", capabilities.get(7).toString());
        Assertions.assertEquals("1.2.392.200011.61.1.1.1.3", capabilities.get(8).toString());
        Assertions.assertEquals("1.2.392.200011.61.1.1.1.2", capabilities.get(9).toString());
        Assertions.assertEquals("1.2.410.200004.1.4", capabilities.get(10).toString());
    }

    @Test
    void testEnveloped()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/encrypted-validcertificate.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        Assertions.assertNotNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());

        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();

        Assertions.assertNotNull(mimeBodyPart);
        Assertions.assertTrue(mimeBodyPart.isMimeType("multipart/mixed"));
    }

    @Test
    void testClearSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNotNull(inspector.getSignedInspector());
    }

    @Test
    void testClearSignedSwapped()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-swapped-parts.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNotNull(inspector.getSignedInspector());
    }

    @Test
    void testOpaqueSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/signed-opaque-validcertificate.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNotNull(inspector.getSignedInspector());
    }

    @Test
    void testCompressed()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/compressed.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.COMPRESSED, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNotNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());

        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();

        Assertions.assertNotNull(mimeBodyPart);
        Assertions.assertTrue(mimeBodyPart.isMimeType("multipart/mixed"));
    }

    @Test
    void testNoSMIME()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());
    }

    @Test
    void testFakeEncrypted()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/fake-encrypted.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());
    }

    @Test
    void testFakeOpaqueSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/fake-opaque-signed.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());
    }

    @Test
    void testFakeClearSigned()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/fake-clear-signed.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
        Assertions.assertNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());
    }

    @Test
    void testEnvelopedAES256GCM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/smime-encrypted-gcm-256.eml");

        SMIMEInspector inspector = new SMIMEInspectorImpl(message, keyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        Assertions.assertEquals(SMIMEType.AUTH_ENCRYPTED, inspector.getSMIMEType());
        Assertions.assertNotNull(inspector.getEnvelopedInspector());
        Assertions.assertNull(inspector.getCompressedInspector());
        Assertions.assertNull(inspector.getSignedInspector());

        MimeBodyPart mimeBodyPart = inspector.getContentAsMimeBodyPart();

        Assertions.assertNotNull(mimeBodyPart);
        Assertions.assertTrue(mimeBodyPart.isMimeType("multipart/mixed"));

        String mime = MailUtils.partToMimeString(mimeBodyPart);

        Assertions.assertTrue(mime.contains("filename=\"cityinfo.zip\""));
    }
}
