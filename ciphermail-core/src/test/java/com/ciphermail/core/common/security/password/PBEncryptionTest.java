/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Base64Utils;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class PBEncryptionTest
{
    private static final String ENCRYPTED_SAMPLE_DEFAULT_SETTINGS =
            "lklfx6SWxIkAAAAQm6EHKDkbcoBDoJShyHIqzAAACAAAAAAQoiESQG7aqGdQIjK3yuK1Ng==";

    @Test
    public void testEncrypt()
    throws Exception
    {
        String password = "test";

        PBEncryptionImpl pbe = new PBEncryptionImpl();

        byte[] data = new byte[]{1, 2, 3, 4};

        byte[] encrypted = pbe.encrypt(data, password.toCharArray());

        byte[] decrypted = pbe.decrypt(encrypted, password.toCharArray());

        assertArrayEquals(data, decrypted);
    }

    @Test
    public void testEncryptLarge()
    throws Exception
    {
        String password = "test";

        PBEncryptionImpl pbe = new PBEncryptionImpl();

        byte[] data = new byte[4541];

        SecurityFactoryFactory.getSecurityFactory().createSecureRandom().nextBytes(data);

        byte[] encrypted = pbe.encrypt(data, password.toCharArray());

        byte[] decrypted = pbe.decrypt(encrypted, password.toCharArray());

        assertArrayEquals(data, decrypted);
    }

    @Test
    public void testDecrypt()
    throws Exception
    {
        PBEncryptionImpl pbe = new PBEncryptionImpl();

        String password = "test";

        assertEquals("test 1234", new String(pbe.decrypt(Base64Utils.decode(ENCRYPTED_SAMPLE_DEFAULT_SETTINGS),
                password.toCharArray()), StandardCharsets.UTF_8));
    }
}
