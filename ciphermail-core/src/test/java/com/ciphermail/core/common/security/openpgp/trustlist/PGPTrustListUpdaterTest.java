/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingPairImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPTrustListUpdaterTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    private PGPKeyRing keyRing;

    private PGPTrustList trustList;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        trustList = new PGPTrustListImpl("global", sessionManager, keyRing);

        keyRing.addPGPKeyRingEventListener(new PGPTrustListUpdater(trustList));

    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, MissingKeyAlias.ALLOWED);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private void trustKey(PGPPublicKey publicKey)
    throws Exception
    {
        PGPTrustListEntry entry = trustList.createEntry(publicKey);

        entry.setStatus(PGPTrustListStatus.TRUSTED);

        trustList.setEntry(entry, false);
    }

    @Test
    public void testDeleteKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                    keyRing.addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                            secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(0, trustList.size());

                trustKey(secretKeys.get(1).getPublicKey());

                assertEquals(1, trustList.size());

                keyRing.delete(getByKeyID(secretKeys.get(1).getKeyID()).getID());

                assertEquals(0, trustList.size());

                trustKey(secretKeys.get(0).getPublicKey());
                trustKey(secretKeys.get(2).getPublicKey());
                trustKey(secretKeys.get(3).getPublicKey());

                assertEquals(3, trustList.size());

                // Deleting the master should result in the deletion of trust of subkeys
                keyRing.delete(getByKeyID(secretKeys.get(0).getKeyID()).getID());

                assertEquals(0, trustList.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteAllKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                    keyRing.addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                            secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(0, trustList.size());

                trustKey(secretKeys.get(1).getPublicKey());
                trustKey(secretKeys.get(2).getPublicKey());
                trustKey(secretKeys.get(3).getPublicKey());

                assertEquals(3, trustList.size());

                keyRing.deleteAll();

                assertEquals(0, trustList.size());
                assertEquals(0, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
