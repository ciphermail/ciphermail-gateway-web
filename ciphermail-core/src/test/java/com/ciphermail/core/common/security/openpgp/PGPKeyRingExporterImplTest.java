/*
 * Copyright (c) 2014-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingExporterImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private int importKeyRing(InputStream input, String importPassword)
    throws Exception
    {
        PGPKeyRingImporterImpl importer = new PGPKeyRingImporterImpl(keyRing);

        return importer.importKeyRing(input, new StaticPasswordProvider(importPassword)).size();
    }

    private int importKeyRing(PrivateKeysAndPublicKeyRing privateKeysAndPublicKeyRing)
    throws Exception
    {
        PGPKeyRingImporterImpl importer = new PGPKeyRingImporterImpl(keyRing);

        return importer.importKeyRing(privateKeysAndPublicKeyRing).size();
    }

    private boolean exportSecretKeys(OutputStream output, String exportPassword)
    throws Exception
    {
        PGPKeyRingExporterImpl exporter = new PGPKeyRingExporterImpl();

        return exporter.exportSecretKeys(getAllEntries(), output, new StaticPasswordProvider(exportPassword));
    }

    private boolean exportPublicKeys(OutputStream output)
    throws Exception
    {
        PGPKeyRingExporterImpl exporter = new PGPKeyRingExporterImpl();

        return exporter.exportPublicKeys(getAllEntries(), output);
    }

    private List<PGPKeyRingEntry> getAllEntries()
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getIterator(new PGPSearchParameters(),
                null, null);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        return list;
    }
    @Test
    public void testExportGeneratedSecretKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPSecretKeyRingGenerator ringGenerator = new PGPSecretKeyRingGeneratorImpl();

                PrivateKeysAndPublicKeyRing secretRing = ringGenerator.generateKeyRing(PGPKeyGenerationType.RSA_2048,
                        "john doen <test2@example.com>");

                assertEquals(2, importKeyRing(secretRing));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportGeneratedSecretKeys.key.asc");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportSecretKeys(output, "export"));

                output.close();

                InputStream input = new BufferedInputStream(new FileInputStream(outputFile));

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(input);
                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(input);

                assertEquals(2, publicKeys.size());
                assertEquals(2, secretKeys.size());

                input.close();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportSecretKeysSingleKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.key");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(), "testExportSecretKeysSingleKeyRing.gpg.key");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportSecretKeys(output, "export"));

                output.close();

                InputStream input = new BufferedInputStream(new FileInputStream(outputFile));

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(input);
                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(input);

                assertEquals(6, publicKeys.size());
                assertEquals(6, secretKeys.size());

                input.close();

                // Test whether the public keys stored in the secret key ring are equal to the public keys from the
                // public key ring
                for (int i = 0; i < publicKeys.size(); i++)
                {
                    PGPPublicKey publicKey1 = publicKeys.get(i);
                    PGPPublicKey publicKey2 = secretKeys.get(i).getPublicKey();

                    assertEquals(publicKey1.getKeyID(), publicKey2.getKeyID());
                    assertEquals(new PGPPublicKeyWrapper(publicKey1), new PGPPublicKeyWrapper(publicKey2));
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportSecretKeysMulitpleKeyRings()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.key");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                file = new File(TEST_BASE, "test@example.com.gpg.key");

                assertEquals(2, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportSecretKeysMulitpleKeyRings.gpg.key");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportSecretKeys(output, "export"));

                output.close();

                InputStream input = new BufferedInputStream(new FileInputStream(outputFile));

                List<PGPPublicKey> publicKeys = PGPKeyUtils.readPublicKeys(input);
                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(input);

                assertEquals(8, publicKeys.size());
                assertEquals(8, secretKeys.size());

                input.close();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportPublicKeysSingleKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportPublicKeysSingleKeyRing.gpg.asc");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportPublicKeys(output));

                output.close();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(outputFile);

                assertEquals(6, keys.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportPublicKeysSecretKeyRing()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.key");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportPublicKeysSecretKeyRing.gpg.asc");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportPublicKeys(output));

                output.close();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(outputFile);

                assertEquals(6, keys.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportPublicKeysMultipleKeyRings()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.key");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                file = new File(TEST_BASE, "test@example.com.gpg.key");

                assertEquals(2, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportPublicKeysMultipleKeyRings.gpg.asc");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportPublicKeys(output));

                output.close();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(outputFile);

                assertEquals(8, keys.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testExportSecretKeysButNoSecretKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                File file = new File(TEST_BASE, "test-multiple-sub-keys.gpg.asc");

                assertEquals(6, importKeyRing(new FileInputStream(file), "test"));

                File outputFile = new File(FileUtils.getTempDirectory(),
                        "testExportSecretKeysButNoSecretKeys.gpg.asc");

                FileOutputStream output = new FileOutputStream(outputFile);

                assertTrue(exportSecretKeys(output, "test"));

                output.close();

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(outputFile);

                assertEquals(6, keys.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
