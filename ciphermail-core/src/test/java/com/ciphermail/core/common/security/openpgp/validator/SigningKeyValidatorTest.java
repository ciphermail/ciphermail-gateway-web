/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPKeyFlagCheckerImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidatorImpl;
import com.ciphermail.core.common.security.openpgp.PGPUserIDValidatorImpl;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SigningKeyValidatorTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final SigningKeyValidator signingKeyValidator = new SigningKeyValidator(
            new PGPKeyFlagCheckerImpl(new PGPSignatureValidatorImpl(),
            new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl())));

    @Test
    public void testValidForSigning()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test-multiple-sub-keys.gpg.asc"));

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, keys.get(0));

        assertTrue(signingKeyValidator.validate(keys.get(0), context).isValid());
        assertFalse(signingKeyValidator.validate(keys.get(1), context).isValid());
        assertTrue(signingKeyValidator.validate(keys.get(2), context).isValid());
        assertFalse(signingKeyValidator.validate(keys.get(3), context).isValid());
        assertTrue(signingKeyValidator.validate(keys.get(4), context).isValid());
        assertTrue(signingKeyValidator.validate(keys.get(5), context).isValid());
    }
}
