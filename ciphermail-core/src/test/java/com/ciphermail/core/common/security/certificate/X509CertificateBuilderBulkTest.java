/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.impl.StandardSerialNumberGenerator;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class X509CertificateBuilderBulkTest
{
    private static final int CERTIFICATE_COUNT = 5;

    private static SecurityFactory securityFactory;
    private static SecureRandom randomSource;
    private static SerialNumberGenerator serialNumberGenerator;
    private static KeyPairGenerator keyPairGenerator;

    @BeforeClass
    public static void setUpBeforeClass()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        randomSource = securityFactory.createSecureRandom();
        serialNumberGenerator = new StandardSerialNumberGenerator();

        keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

        keyPairGenerator.initialize(2048, randomSource);
    }

    private String generateRandomEmail()
    {
        return RandomStringUtils.randomAlphabetic(10) + "@example.com";
    }

    private String[] generateRandomEmails()
    {
        int size = randomSource.nextInt(5);

        String[] emails = new String[size];

        for (int i = 0; i < emails.length; i++)
        {
            emails[i] = generateRandomEmail();
        }

        return emails;
    }

    private String generateRandomCommonName() {
        return RandomStringUtils.randomAlphabetic(10) + " " + RandomStringUtils.randomAlphabetic(10);
    }

    public void generateSelfSignedV3Certificate(Collection<X509Certificate> certificates)
    throws Exception
    {
        X509CertificateBuilder certificateBuilder = securityFactory.createX509CertificateBuilder();

        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        X500PrincipalBuilder issuerBuilder = X500PrincipalBuilder.getInstance();

        String[] emails = generateRandomEmails();

        String cn = generateRandomCommonName();

        issuerBuilder.setCommonName(cn);
        issuerBuilder.setCountryCode("NL");
        issuerBuilder.setEmail(emails);
        issuerBuilder.setLocality("Amsterdam");
        issuerBuilder.setState("NH");

        AltNamesBuilder altNamesBuider = new AltNamesBuilder();

        altNamesBuider.setRFC822Names(emails);

        X500Principal issuer = issuerBuilder.buildPrincipal();
        GeneralNames altNames = altNamesBuider.buildAltNames();

        Set<KeyUsageType> keyUsage = new HashSet<KeyUsageType>();

        keyUsage.add(KeyUsageType.DIGITALSIGNATURE);
        keyUsage.add(KeyUsageType.KEYENCIPHERMENT);
        keyUsage.add(KeyUsageType.NONREPUDIATION);

        Set<ExtendedKeyUsageType> extendedKeyUsage = new HashSet<ExtendedKeyUsageType>();

        extendedKeyUsage.add(ExtendedKeyUsageType.CLIENTAUTH);
        extendedKeyUsage.add(ExtendedKeyUsageType.EMAILPROTECTION);

        BigInteger serialNumber = serialNumberGenerator.generate();

        certificateBuilder.setSubject(issuer);
        certificateBuilder.setIssuer(issuer);
        certificateBuilder.setAltNames(altNames, true);
        certificateBuilder.setKeyUsage(keyUsage, true);
        certificateBuilder.setExtendedKeyUsage(extendedKeyUsage, true);
        certificateBuilder.setNotBefore(DateUtils.addHours(new Date(), -1));
        certificateBuilder.setNotAfter(DateUtils.addYears(new Date(), 20));
        certificateBuilder.setPublicKey(keyPair.getPublic());
        certificateBuilder.setSerialNumber(serialNumber);
        certificateBuilder.setSignatureAlgorithm("SHA256WithRSAEncryption");

        X509Certificate certificate = certificateBuilder.generateCertificate(keyPair.getPrivate(), null);

        assertNotNull(certificate);

        certificates.add(certificate);
    }

    @Test
    public void generateBulk()
    throws Exception
    {
        long startTime = System.currentTimeMillis();

        List<X509Certificate> certificates = new LinkedList<X509Certificate>();

        for (int i = 0; i < CERTIFICATE_COUNT; i++) {
            generateSelfSignedV3Certificate(certificates);
        }

        long duration = System.currentTimeMillis() - startTime;

        System.out.println("Duration: " + duration);

        assertTrue("May fail on slower systems!!!", duration < 10000);
    }
}
