/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.hibernate;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.cms.SignerIdentifier;
import com.ciphermail.core.common.security.cms.SignerIdentifierImpl;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertSelector;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class X509CertStoreExtHibernateTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    private KeyStore keyStore;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStore.removeAllEntries();

                keyStore = importKeyStore(certStore, new File(TEST_BASE, "keys/testCertificates.p12"), "test");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private KeyStore importKeyStore(X509CertStoreExt certStore, File file, String password)
    throws Exception
    {
        KeyStore localKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        // initialize key store
        localKeyStore.load(new FileInputStream(file), password.toCharArray());

        Enumeration<String> aliases = localKeyStore.aliases();

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            Certificate certificate = localKeyStore.getCertificate(alias);

            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            boolean hasPrivateKey = localKeyStore.isKeyEntry(alias) &&
                    (localKeyStore.getKey(alias, null) instanceof PrivateKey);

            alias = hasPrivateKey ? alias : null;

            certStore.addCertificate((X509Certificate)certificate, alias);
        }

        return localKeyStore;
    }

    @Test
    public void testGetCertificatesIssuerIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setIssuer(new X500Principal(
                        "EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(selector);

                // should find root and ca certificate
                List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);

                assertEquals(2, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesAllNullSelectorIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(null);

                // should find root and ca certificate
                List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesAllIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                CloseableIterator<X509Certificate> certificateIterator = certStore.getCertificateIterator(selector);

                // should find root and ca certificate
                List<X509Certificate> certificates = CloseableIteratorUtils.toList(certificateIterator);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreEntryIssuerIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                // should find root and ca certificate
                List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);

                assertEquals(2, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreEntryAllNullSelectorIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(
                        (CertSelector) null, MissingKeyAlias.ALLOWED, null, null);

                // should find root and ca certificate
                List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertStoreEntryAllIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                CloseableIterator<? extends X509CertStoreEntry> iterator = certStore.getCertStoreIterator(selector,
                        MissingKeyAlias.ALLOWED, null, null);

                // should find root and ca certificate
                List<? extends X509CertStoreEntry> certificates = CloseableIteratorUtils.toList(iterator);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSize()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(22, certStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSizeMissingKeyAliasNotAllowed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(20, certStore.size(null, MissingKeyAlias.NOT_ALLOWED));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSizeExpiredNotAllowed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(21, certStore.size(Expired.MATCH_UNEXPIRED_ONLY, null));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveCertificate()
   {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                assertTrue(certStore.contains(certificate));

                certStore.removeCertificate(certificate);

                assertFalse(certStore.contains(certificate));

                assertEquals(21, certStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveAll()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                assertEquals(22, certStore.getCertificates(selector).size());

                certStore.removeAllEntries();

                assertEquals(0, certStore.getCertificates(selector).size());

                assertEquals(0, certStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDuplicate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(22, certStore.size());

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                // Adding an existing cert should not result in an error.
                certStore.addCertificate(certificate);

                assertEquals(22, certStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesIssuerSerial()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                selector.setIssuer(certificate.getIssuerX500Principal());
                selector.setSerialNumber(certificate.getSerialNumber());

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSignerIdentifier()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                SignerIdentifier identifier = new SignerIdentifierImpl(certificate);

                CertSelector selector = identifier.getSelector();

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSubjectKeyIdentifier()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                SignerIdentifier identifier = new SignerIdentifierImpl(null, null,
                        X509CertificateInspector.getSubjectKeyIdentifier(certificate));

                CertSelector selector = identifier.getSelector();

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesUnoptimized()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                X509CertSelector selector = new X509CertSelector();

                selector.setKeyUsage(certificate.getKeyUsage());

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(17, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesNoMatch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(new BigInteger("123", 10));

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(0, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesValidOnly()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setCertificateValid(new Date());

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(21, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesByCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("Validcertificate");

                selector.setCertificate(certificate);

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());

                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesNoRestriction()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesNoRestrictionNullSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<X509Certificate> certificates = certStore.getCertificates(null);

                assertEquals(22, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSerial()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSerialNumber(BigIntegerUtils.hexDecode("115FCAC409FB2022B7D06920A00FE42"));

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesIssuer()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setIssuer(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                // should find root and ca certificate
                assertEquals(2, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesSubject()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setSubject(new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"));

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                // should find root
                X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");

                assertEquals(1, certificates.size());
                assertEquals(certificate, certificates.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesEmailAltNameUsingSelector()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.addSubjectAlternativeName(1, "TEST3@exAmple.com");

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesExpired()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertSelector selector = new X509CertSelector();

                selector.setCertificateValid(TestUtils.parseDate("01-Oct-2006 00:00:00 GMT"));

                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                // All certificates should not yet be valid
                assertEquals(0, certificates.size());

                selector.setCertificateValid(TestUtils.parseDate("01-Oct-2009 00:00:00 GMT"));

                certificates = certStore.getCertificates(selector);

                // All certificates but one should be valid
                assertEquals(21, certificates.size());

                selector.setCertificateValid(TestUtils.parseDate("01-Oct-2030 00:00:00 GMT"));

                certificates = certStore.getCertificates(selector);

                // only one should be valid
                assertEquals(1, certificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
