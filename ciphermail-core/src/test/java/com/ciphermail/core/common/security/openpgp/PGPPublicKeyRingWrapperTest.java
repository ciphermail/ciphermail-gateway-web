/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class PGPPublicKeyRingWrapperTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testWrapper()
    throws Exception
    {
        PGPPublicKeyRing ring1 = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(
                new File(TEST_BASE, "pgp/test@example.com.gpg.asc"))),
                new JcaKeyFingerprintCalculator());

        PGPPublicKeyRing ring2 = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(
                new File(TEST_BASE, "pgp/test-multiple-sub-keys.gpg.asc"))),
                new JcaKeyFingerprintCalculator());

        assertTrue(ArrayUtils.isEquals(ring1.getEncoded(), ring1.getEncoded()));
        assertTrue(ArrayUtils.isEquals(ring2.getEncoded(), ring2.getEncoded()));
        assertFalse(ArrayUtils.isEquals(ring1.getEncoded(), ring2.getEncoded()));

        assertEquals(new PGPPublicKeyRingWrapper(ring1), new PGPPublicKeyRingWrapper(ring1));
        assertNotEquals(new PGPPublicKeyRingWrapper(ring1), new PGPPublicKeyRingWrapper(ring2));
        assertEquals(new PGPPublicKeyRingWrapper(ring1).hashCode(), new PGPPublicKeyRingWrapper(ring1).hashCode());
        assertNotEquals(new PGPPublicKeyRingWrapper(ring1).hashCode(), new PGPPublicKeyRingWrapper(ring2).hashCode());
    }

    @Test
    public void testWrapperWithSet()
    throws Exception
    {
        PGPPublicKeyRing ring1 = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(
                new File(TEST_BASE, "pgp/test@example.com.gpg.asc"))),
                new JcaKeyFingerprintCalculator());

        PGPPublicKeyRing ring2 = new PGPPublicKeyRing(PGPUtil.getDecoderStream(new FileInputStream(
                new File(TEST_BASE, "pgp/test-multiple-sub-keys.gpg.asc"))),
                new JcaKeyFingerprintCalculator());

        Set<PGPPublicKeyRing> rings = new HashSet<PGPPublicKeyRing>();

        rings.add(ring1);

        assertEquals(1, rings.size());

        rings.add(ring1);

        assertEquals(1, rings.size());

        assertTrue(rings.contains(ring1));
        assertFalse(rings.contains(ring2));

        rings.add(ring2);

        assertEquals(2, rings.size());

        assertTrue(rings.contains(ring1));
        assertTrue(rings.contains(ring2));

        rings.add(ring2);

        assertEquals(2, rings.size());

        rings.remove(ring1);

        assertEquals(1, rings.size());

        rings.remove(ring2);

        assertEquals(0, rings.size());

        rings.remove(ring2);

        assertEquals(0, rings.size());
    }
}
