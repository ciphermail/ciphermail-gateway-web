/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.selector;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLUtils;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SigningKeyAndCertificateSelectorTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore certStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key/root/crl store
                certStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();

                importKeyStore(keyAndCertificateWorkflow, new File(TEST_BASE, "keys/testCertificates.p12"), "test");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile,
            String password)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

        keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
    }

    private static void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        addCertificates(CertificateUtils.readCertificates(file), certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private static void addCRLs(File file, X509CRLStoreExt crlStore)
    throws Exception
    {
        addCRLs(CRLUtils.readCRLs(file), crlStore);
    }

    private static void addCRLs(Collection<? extends CRL> crls, X509CRLStoreExt crlStore)
    throws Exception
    {
        for (CRL crl : crls)
        {
            if (crl instanceof X509CRL && !crlStore.contains((X509CRL) crl)) {
                crlStore.addCRL((X509CRL) crl);
            }
        }
    }

    @Test
    public void testGetKeyAndCertificatesIntermediateRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStore);
                addCRLs(new File(TEST_BASE, "crls/test-root-ca-revoked.crl"), crlStore);

                KeyAndCertificateSelector selector = new SigningKeyAndCertificateSelector(pKISecurityServices);

                Set<KeyAndCertificate> keyAndCertificates = selector.getMatchingKeyAndCertificates("test@example.com");

                // all certificates should have been revoked because the intermediate has been revoked
                assertEquals(0, keyAndCertificates.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetKeyAndCertificates()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TEST_BASE, "certificates/example-self-signed-10.p7b"), certStore);

                KeyAndCertificateSelector selector = new SigningKeyAndCertificateSelector(pKISecurityServices);

                Set<KeyAndCertificate> keyAndCertificates = selector.getMatchingKeyAndCertificates("test@example.com");

                Set<String> serials = new HashSet<>();

                for (KeyAndCertificate keyAndCertificate : keyAndCertificates)
                {
                    String serial = X509CertificateInspector.getSerialNumberHex(keyAndCertificate.getCertificate());

                    serials.add(serial);

                    assertNotNull(keyAndCertificate.getPrivateKey());
                }

                assertEquals(14, keyAndCertificates.size());
                assertEquals(serials.size(), keyAndCertificates.size());

                assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
                assertTrue(serials.contains("1178C336C7B51CADD4CCECDD14DAF22")); /* KeyUsageNotForEncryption*/
                assertTrue(serials.contains("1178C3B653829E895ACB7100EB1F627")); /* KeyUsageOnlyNonRepudiation*/
                assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
                assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/
                assertTrue(serials.contains("115FCD741088707366E9727452C9770")); /* ValidCertificate*/
                assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
                assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
                assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
                assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
                assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
                assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetCertificatesEndEntityRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                addCertificates(new File(TEST_BASE, "certificates/mitm-test-root.cer"), rootStore);
                addCRLs(new File(TEST_BASE, "crls/test-ca.crl"), crlStore);

                KeyAndCertificateSelector selector = new SigningKeyAndCertificateSelector(pKISecurityServices);

                Set<KeyAndCertificate> keyAndCertificates = selector.getMatchingKeyAndCertificates("test@example.com");

                Set<String> serials = new HashSet<>();

                for (KeyAndCertificate keyAndCertificate : keyAndCertificates)
                {
                    String serial = X509CertificateInspector.getSerialNumberHex(keyAndCertificate.getCertificate());

                    serials.add(serial);

                    assertNotNull(keyAndCertificate.getPrivateKey());
                }

                assertEquals(13, keyAndCertificates.size());
                assertEquals(serials.size(), keyAndCertificates.size());

                assertTrue(serials.contains("116A448F117FF69FE4F2D4D38F689D7")); /* CriticalEKU */
                assertTrue(serials.contains("1178C336C7B51CADD4CCECDD14DAF22")); /* KeyUsageNotForEncryption*/
                assertTrue(serials.contains("1178C3B653829E895ACB7100EB1F627")); /* KeyUsageOnlyNonRepudiation*/
                assertTrue(serials.contains("115FD0E5EE990D9426C93DEA720E970")); /* NoCN */
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoExtendedKeyUsage*/
                assertTrue(serials.contains("115FD08D3F0E6159746AEA96A50C5D6")); /* NoKeyUsage*/
                assertTrue(serials.contains("115FCDE9DC082E7E9C8EEF4CC69B94C")); /* UppercaseEmail*/

                // This certificate is revoked
                // assertTrue(serials.contains("115FCD741088707366E9727452C9770"));
                assertTrue(serials.contains("115FCEECCD07FE8929F68CC6B359A5A")); /* EmailInAltNamesNotInSubject*/
                assertTrue(serials.contains("115FCEB7F46B98775DBB8287965F838")); /* EmailInSubjectNotInAltNames*/
                assertTrue(serials.contains("115FD1392A8FF07AA727558FA50B262")); /* MD5Hash*/
                assertTrue(serials.contains("115FD110A82F742D0AE14A71B651962")); /* MultipleEmail*/
                assertTrue(serials.contains("115FD1606444BC50DE5464AF7D0D468")); /* RSA2048*/
                assertTrue(serials.contains("115FD16008275F2616B8A235D761FFF")); /* SHA256Hash*/
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
