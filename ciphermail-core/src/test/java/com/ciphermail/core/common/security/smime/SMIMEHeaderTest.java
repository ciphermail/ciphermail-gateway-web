/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.smime.SMIMEHeader.Strict;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;

public class SMIMEHeaderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testInvalidContentDisposition()
    throws Exception
    {
        File mailFile = new File(TEST_BASE, "mail/invalid-content-disposition.eml");

        MimeMessage message = MailUtils.loadMessage(mailFile);

        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType(message);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);

        type = SMIMEHeader.getSMIMEContentType(message, Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testClearSigned()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("multipart/signed; protocol" +
                "=\"application/pkcs7-signature\"; micalg=sha1; boundary=\"----=_Part_1_60863806.1194045718778\"",
                null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, type);
    }

    /*
     * Note: this test fails if system property mail.mime.parameters.strict is false
     */
    @Test
    public void testClearSignedInvalidProtocolHeader()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("multipart/signed; protocol" +
                "=application/pkcs7-signature; micalg=sha1; boundary=\"----=_Part_1_60863806.1194045718778\"",
                null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testSignedMissingProtocol()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("multipart/signed; micalg=sha1; " +
                "boundary=\"----=_Part_1_60863806.1194045718778\"",
                null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.UNKNOWN_CLEAR_SIGNED, type);
    }


    @Test
    public void testEncrypted()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.ENCRYPTED, type);
    }

    @Test
    public void testEncryptedOctet()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream; name=\"smime.p7m\"; smime-type=enveloped-data",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.ENCRYPTED, type);
    }

    @Test
    public void testEncryptedOctetEnveloped()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream; name=\"smime.p7m\"",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.ENVELOPED, type);
    }

    @Test
    public void testEncryptedOctetEnvelopedFilename()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream",
            "xxx.P7m", SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.ENVELOPED, type);
    }

    @Test
    public void testEncryptedOctetEnvelopedFilenameStrict()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream",
            "xxx.P7m", SMIMEHeader.Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testEncryptedOctetEnvelopedFilenameNoMatch()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream",
            "xxx.P7k", SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testEncryptedOctetEnvelopedFilenameNoMatchPrimary()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/word",
            "smime.p7m", SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testCompressed()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.COMPRESSED, type);
    }

    @Test
    public void testCompressedName()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/pkcs7-mime; name=\"smime.p7z\"",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.COMPRESSED, type);
    }

    @Test
    public void testCompressedNameOctetStream()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream; name=\"smime.p7z\"",
            null, SMIMEHeader.Strict.NO);

        assertEquals(SMIMEHeader.Type.COMPRESSED, type);
    }

    @Test
    public void testCompressedNameOctetStreamStrict()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/octet-stream; name=\"smime.p7z\"",
            null, SMIMEHeader.Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testCompressedNameStrict()
    {
        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType("application/pkcs7-mime; name=\"smime.p7z\"",
            null, SMIMEHeader.Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testOctetStreamMessage()
    throws FileNotFoundException, MessagingException
    {
        File mailFile = new File(TEST_BASE, "mail/signed-opaque-non-standard-content-type.eml");

        MimeMessage message = MailUtils.loadMessage(mailFile);

        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType(message);

        assertEquals(SMIMEHeader.Type.ENVELOPED, type);

        type = SMIMEHeader.getSMIMEContentType(message, SMIMEHeader.Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }

    @Test
    public void testMimeEncodedFilename()
    throws FileNotFoundException, MessagingException
    {
        File mailFile = new File(TEST_BASE, "mail/encrypted-mime-encoded-filename.eml");

        MimeMessage message = MailUtils.loadMessage(mailFile);

        SMIMEHeader.Type type = SMIMEHeader.getSMIMEContentType(message);

        assertEquals(SMIMEHeader.Type.ENVELOPED, type);

        type = SMIMEHeader.getSMIMEContentType(message, SMIMEHeader.Strict.YES);

        assertEquals(SMIMEHeader.Type.NO_SMIME, type);
    }
}
