/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.impl;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.SMS;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMSGatewayDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        getDAO().deleteAll();
    }

    private SMSGatewayDAO getDAO() {
        return new SMSGatewayDAO(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private UUID addSMS(String phoneNumber, String message, Date dateLastTry) {
        return addSMS(phoneNumber, message, dateLastTry, null);
    }

    private UUID addSMS(String phoneNumber, String message, Date dateLastTry, Date created)
    {
        SMSGatewayDAO dao = getDAO();

        SMSGatewayEntity entity = created != null ? dao.addSMS(phoneNumber, message, created) :
                dao.addSMS(phoneNumber, message);

        // Introduce some wait time to make sorting on created date deterministic
        ThreadUtils.sleepQuietly(100);

        if (dateLastTry != null) {
            entity.setDateLastTry(dateLastTry);
        }

        return (UUID) sessionManager.getSession().getIdentifier(entity);
    }

    @Test
    public void testGetAllSMS()
    {
        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(new Date(), -2)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(new Date(), -3)));
        assertNotNull(addSMS("1111", "asasadsa", DateUtils.addHours(new Date(), -1)));
        assertNotNull(addSMS("999", "asasadsa", null));

        List<SMS> all = getDAO().getAll(null, null, SortColumn.PHONENUMBER, SortDirection.ASC);

        assertEquals(4, all.size());
        assertEquals("1111", all.get(0).getPhoneNumber());
        assertEquals("123456", all.get(1).getPhoneNumber());
        assertEquals("789", all.get(2).getPhoneNumber());
        assertEquals("999", all.get(3).getPhoneNumber());

        all = getDAO().getAll(null, null, SortColumn.PHONENUMBER, SortDirection.DESC);

        assertEquals(4, all.size());
        assertEquals("999", all.get(0).getPhoneNumber());
        assertEquals("789", all.get(1).getPhoneNumber());
        assertEquals("123456", all.get(2).getPhoneNumber());
        assertEquals("1111", all.get(3).getPhoneNumber());

        all = getDAO().getAll(1, 2, SortColumn.PHONENUMBER, SortDirection.DESC);

        assertEquals(2, all.size());
        assertEquals("789", all.get(0).getPhoneNumber());
        assertEquals("123456", all.get(1).getPhoneNumber());

        all = getDAO().getAll(1, 2, SortColumn.PHONENUMBER, SortDirection.ASC);

        assertEquals(2, all.size());
        assertEquals("123456", all.get(0).getPhoneNumber());
        assertEquals("789", all.get(1).getPhoneNumber());

        all = getDAO().getAll(0, 1, SortColumn.PHONENUMBER, SortDirection.ASC);

        assertEquals(1, all.size());
        assertEquals("1111", all.get(0).getPhoneNumber());

        all = getDAO().getAll(null, null, SortColumn.CREATED, SortDirection.ASC);

        assertEquals(4, all.size());
        assertEquals("123456", all.get(0).getPhoneNumber());
        assertEquals("789", all.get(1).getPhoneNumber());
        assertEquals("1111", all.get(2).getPhoneNumber());
        assertEquals("999", all.get(3).getPhoneNumber());

        all = getDAO().getAll(null, null, SortColumn.LAST_TRY, SortDirection.ASC);

        assertEquals(4, all.size());
        assertEquals("999", all.get(0).getPhoneNumber());
        assertEquals("789", all.get(1).getPhoneNumber());
        assertEquals("123456", all.get(2).getPhoneNumber());
        assertEquals("1111", all.get(3).getPhoneNumber());
    }

    @Test
    public void testGetAvailableCount()
    {
        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(new Date(), -1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(new Date(), -2)));
        assertNotNull(addSMS("999", "qwerty", null));

        Date notAfter = new Date();

        assertEquals(3, getDAO().getAvailableCount(notAfter));
    }

    @Test
    public void testGetAvailableCountNullDate()
    {
        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(new Date(), -1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(new Date(), -2)));

        assertEquals(2, getDAO().getAvailableCount(null));
    }

    @Test
    public void testGetAvailableCountOneMatch()
    {
        Date now = new Date();

        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(now, 1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, -2)));

        assertEquals(1, getDAO().getAvailableCount(now));
    }

    @Test
    public void testGetAvailableCountNoRecords()
    {
        Date notAfter = new Date();

        assertEquals(0, getDAO().getAvailableCount(notAfter));
    }

    @Test
    public void testGetExpired()
    {
        Date now = new Date();

        assertNotNull(addSMS("123456", "sms body", now));
        assertNotNull(addSMS("789", "qwerty", now));
        assertNotNull(addSMS("789", "qwerty",  now, DateUtils.addHours(now, 2)));

        List<SMSGatewayEntity> entities;

        entities = getDAO().getExpired(DateUtils.addHours(now, -1));
        assertEquals(0, entities.size());

        entities = getDAO().getExpired(DateUtils.addHours(now, 1));
        assertEquals(2, entities.size());

        entities = getDAO().getExpired(DateUtils.addHours(now, 3));
        assertEquals(3, entities.size());
    }

    @Test
    public void testGetExpiredNoExpired()
    {
        Date now = new Date();

        assertNotNull(addSMS("123456", "sms body", now));

        Date olderThan = DateUtils.addHours(now, -1);

        List<SMSGatewayEntity> entities = getDAO().getExpired(olderThan);

        assertNotNull(entities);
        assertEquals(0, entities.size());
    }

    @Test
    public void testGetAvailableOldest()
    {
        Date now = new Date();

        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(now, -1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, -2)));

        SMSGatewayEntity entity = getDAO().getNextAvailable(now);

        assertNotNull(entity);
        assertEquals("789", entity.getPhoneNumber());
    }

    @Test
    public void testGetAvailableNotYetValid()
    {
        Date now = new Date();

        assertNotNull(addSMS("123456", "sms body", now));

        Date notAfter1 = DateUtils.addHours(now, -1);

        SMSGatewayEntity entity = getDAO().getNextAvailable(notAfter1);

        assertNull(entity);

        Date notAfter2 = DateUtils.addHours(now, 1);

        entity = getDAO().getNextAvailable(notAfter2);

        assertNotNull(entity);
        assertEquals("123456", entity.getPhoneNumber());
    }

    @Test
    public void testGetAvailableNullTryDate()
    {
        assertNotNull(addSMS("123456", "sms body", null));

        Date notAfter = new Date();

        SMSGatewayEntity entity = getDAO().getNextAvailable(notAfter);

        assertNotNull(entity);
        assertEquals("123456", entity.getPhoneNumber());
    }

    @Test
    public void testGetAvailableNoRecords()
    {
        Date notAfter = new Date();

        SMSGatewayEntity entity = getDAO().getNextAvailable(notAfter);

        assertNull(entity);
    }

    @Test
    public void testAddSMS()
    {
        UUID id = addSMS("123456", "sms body", null);

        assertNotNull(id);

        SMSGatewayEntity entity = getDAO().findById(id, SMSGatewayEntity.class);

        SMS sms = SMSGatewayDAO.entityToSMS(entity);

        assertEquals(id, sms.getID());

        assertEquals("123456", entity.getPhoneNumber());
        assertEquals("sms body", entity.getMessage());
    }

    @Test
    public void testDeleteAll()
    {
        int size = getDAO().findAll(SMSGatewayEntity.class).size();

        assertEquals(0, size);

        assertNotNull(addSMS("123456", "sms body", null));

        size = getDAO().findAll(SMSGatewayEntity.class).size();

        assertEquals(1, size);

        getDAO().deleteAll();

        size =  getDAO().findAll(SMSGatewayEntity.class).size();

        assertEquals(0, size);
    }

    @Test
    public void testGetNextAvailableNoNullLastTry()
    {
        Date now = new Date();

        assertNotNull(addSMS("222", "sms body", DateUtils.addHours(now, -3)));
        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(now, -1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, -2)));

        SMSGatewayEntity entity = getDAO().getNextAvailable(now);

        assertEquals("222", entity.getPhoneNumber());
    }

    @Test
    public void testGetNextAvailableAllTooNew()
    {
        Date now = new Date();

        assertNotNull(addSMS("222", "sms body", DateUtils.addHours(now, 3)));
        assertNotNull(addSMS("123456", "sms body", DateUtils.addHours(now, 1)));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, 2)));

        assertNull(getDAO().getNextAvailable(now));
    }

    @Test
    public void testGetNextAvailableNullLastTry()
    {
        Date now = new Date();

        assertNotNull(addSMS("222", "sms body", DateUtils.addHours(now, -3)));
        assertNotNull(addSMS("123456", "sms body", null));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, -2)));

        SMSGatewayEntity entity = getDAO().getNextAvailable(now);

        assertEquals("123456", entity.getPhoneNumber());
    }

    @Test
    public void testGetNextAvailableNullLastTryOrderByCreated()
    {
        Date now = new Date();

        assertNotNull(addSMS("222", "sms body", DateUtils.addHours(now, -3)));
        assertNotNull(addSMS("123456", "sms body", null));
        assertNotNull(addSMS("789", "qwerty", DateUtils.addHours(now, -2)));
        assertNotNull(addSMS("888", "sms body", null, DateUtils.addHours(now, -3)));

        SMSGatewayEntity entity = getDAO().getNextAvailable(now);

        assertEquals("888", entity.getPhoneNumber());
    }
}
