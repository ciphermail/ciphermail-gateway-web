/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPTrustListDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        PGPTrustListDAO.deleteAll(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private PGPTrustListDAO createDAO() {
        return createDAO("test");
    }

    private PGPTrustListDAO createDAO(String name) {
        return PGPTrustListDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), name);
    }

    private void addPGPTrustListEntity(PGPTrustListDAO dao, String fingerprint)
    {
        PGPTrustListEntity entity = new PGPTrustListEntity(fingerprint);

        entity.setName(dao.getName());

        dao.persist(entity);
    }

    @Test
    public void testGetPGPTrustListEntities()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other-dao");

        List<PGPTrustListEntity> entities;

        entities = dao.getPGPTrustListEntities(null, null);
        assertEquals(0, entities.size());

        entities = otherDAO.getPGPTrustListEntities(null, null);
        assertEquals(0, entities.size());

        addPGPTrustListEntity(dao, "fingerprint1");
        addPGPTrustListEntity(dao, "fingerprint2");

        entities = dao.getPGPTrustListEntities(null, null);
        assertEquals(2, entities.size());

        entities = otherDAO.getPGPTrustListEntities(null, null);
        assertEquals(0, entities.size());

        entities = dao.getPGPTrustListEntities(1, null);
        assertEquals(1, entities.size());

        entities = dao.getPGPTrustListEntities(null, 1);
        assertEquals(1, entities.size());

        entities = dao.getPGPTrustListEntities(0, 10);
        assertEquals(2, entities.size());
    }

    @Test
    public void testGetAllPGPTrustListEntities()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other");

        List<PGPTrustListEntity> entities;

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(dao.getSession(), null, null);
        assertEquals(0, entities.size());

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(otherDAO.getSession(),null, null);
        assertEquals(0, entities.size());

        addPGPTrustListEntity(dao, "fingerprint1");
        addPGPTrustListEntity(dao, "fingerprint2");

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(dao.getSession(),null, null);
        assertEquals(2, entities.size());

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(otherDAO.getSession(),null, null);
        assertEquals(2, entities.size());

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(otherDAO.getSession(),1, null);
        assertEquals(1, entities.size());

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(otherDAO.getSession(),null, 1);
        assertEquals(1, entities.size());

        entities = PGPTrustListDAO.getAllPGPTrustListEntities(otherDAO.getSession(),0, 10);
        assertEquals(2, entities.size());
    }

    @Test
    public void testSize()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other-dao");

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());

        addPGPTrustListEntity(dao, "fingerprint1");
        addPGPTrustListEntity(dao, "fingerprint2");

        assertEquals(2, dao.size());
        assertEquals(0, otherDAO.size());
    }

    @Test
    public void testGetPGPTrustListEntity()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other-dao");

        assertNull(dao.getPGPTrustListEntity("fingerprint1"));
        assertNull(otherDAO.getPGPTrustListEntity("fingerprint1"));

        addPGPTrustListEntity(dao, "fingerprint1");

        assertNotNull(dao.getPGPTrustListEntity("fingerprint1"));
        assertNull(otherDAO.getPGPTrustListEntity("fingerprint1"));
    }

    @Test
    public void testDelete()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other-dao");

        addPGPTrustListEntity(dao, "fingerprint1");
        addPGPTrustListEntity(otherDAO, "fingerprint1");

        assertEquals(1, dao.size());
        assertEquals(1, otherDAO.size());

        dao.delete();

        assertEquals(0, dao.size());
        assertEquals(1, otherDAO.size());
    }

    @Test
    public void testDeleteAll()
    {
        PGPTrustListDAO dao = createDAO();
        PGPTrustListDAO otherDAO = createDAO("other-dao");

        addPGPTrustListEntity(dao, "fingerprint1");
        addPGPTrustListEntity(otherDAO, "fingerprint1");

        assertEquals(1, dao.size());
        assertEquals(1, otherDAO.size());

        PGPTrustListDAO.deleteAll(dao.getSession());

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());
    }
}
