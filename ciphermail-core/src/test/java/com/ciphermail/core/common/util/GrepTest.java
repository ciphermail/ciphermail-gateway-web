/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class GrepTest
{
    private final static File TEST_BASE = new File("src/test/resources/testdata");

    @Test
    public void testLineCount()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "other/ends-with-lf.txt"));

        assertEquals(8, reader.getLineCount());
        assertEquals(8, reader.getLineCount());

        reader = new Grep(new File(TEST_BASE, "other/ends-with-text.txt"));

        assertEquals(7, reader.getLineCount());
        assertEquals(7, reader.getLineCount());
    }

    @Test
    public void testReadSingleLine()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "mail/simple-text-message.eml"));

        List<String> lines = reader.readLines(0, 1);

        assertEquals(1, lines.size());
        assertEquals("Content-Type: text/plain", lines.get(0));

        lines = reader.readLines(2, 1);

        assertEquals(1, lines.size());
        assertEquals("From: test@example.com", lines.get(0));
    }

    @Test
    public void testReadLines()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "other/ends-with-text.txt"));

        List<String> lines = reader.readLines(3, 3);

        assertEquals(3, lines.size());

        assertEquals("", lines.get(0));
        assertEquals("test", lines.get(1));
        assertEquals("", lines.get(2));
    }

    @Test
    public void testReadLinesToEnd()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "other/ends-with-text.txt"));

        List<String> lines = reader.readLines(0, Integer.MAX_VALUE);

        assertEquals(7, lines.size());

        lines = reader.readLines(5, 99);

        assertEquals(2, lines.size());
        assertEquals("", lines.get(0));
        assertEquals("123", lines.get(1));

        reader = new Grep(new File(TEST_BASE, "other/ends-with-lf.txt"));

        lines = reader.readLines(0, Integer.MAX_VALUE);

        assertEquals(8, lines.size());

        lines = reader.readLines(5, 99);

        assertEquals(3, lines.size());
        assertEquals("", lines.get(0));
        assertEquals("123", lines.get(1));
        assertEquals("", lines.get(2));
    }

    @Test
    public void testReadMultipleFiles()
    throws IOException
    {
        List<File> files = new LinkedList<File>();

        files.add(new File(TEST_BASE, "other/ends-with-text.txt"));
        files.add(new File(TEST_BASE, "mail/simple-text-message.eml"));

        Grep reader = new Grep(files);

        List<String> lines = reader.readLines(0, 1000);

        assertEquals(7 + 7, lines.size());

        assertEquals(7 + 7, reader.getLineCount());
    }

    @Test
    public void testReadLinesPattern()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "other/ends-with-text.txt"));

        reader.setPattern(Pattern.compile(".*s$"));

        assertEquals(2, reader.getLineCount());

        List<String> lines = reader.readLines(0, 100);

        assertEquals(2, lines.size());

        assertEquals("this", lines.get(0));
        assertEquals("is", lines.get(1));

        lines = reader.readLines(1, 10);

        assertEquals(1, lines.size());

        assertEquals("is", lines.get(0));
    }

    @Test
    public void testReadLinesNoMatch()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "other/ends-with-text.txt"));

        reader.setPattern(Pattern.compile("nomatch"));

        assertEquals(0, reader.getLineCount());

        List<String> lines = reader.readLines(3, 3);

        assertEquals(0, lines.size());
    }

    @Test
    public void testReadLinesPatternMultipleMatches()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "mail/multiple-attachments.eml"));

        reader.setPattern(Pattern.compile("^Content-"));

        assertEquals(15, reader.getLineCount());

        List<String> lines = reader.readLines(3, 3);

        assertEquals("Content-Type: text/xml;", lines.get(0));
        assertEquals("Content-Transfer-Encoding: 7bit", lines.get(1));
        assertEquals("Content-Disposition: attachment;", lines.get(2));

        lines = reader.readLines(6, 3);

        assertEquals("Content-Type: text/plain; charset=UTF-8;", lines.get(0));
        assertEquals("Content-Transfer-Encoding: 7bit", lines.get(1));
        assertEquals("Content-Disposition: attachment;", lines.get(2));

        lines = reader.readLines(9, 3);

        assertEquals("Content-Type: application/x-java-archive;", lines.get(0));
        assertEquals("Content-Transfer-Encoding: base64", lines.get(1));
        assertEquals("Content-Disposition: attachment;", lines.get(2));

        lines = reader.readLines(12, 100);

        assertEquals("Content-Type: text/html;", lines.get(0));
        assertEquals("Content-Transfer-Encoding: base64", lines.get(1));
        assertEquals("Content-Disposition: attachment;", lines.get(2));
    }

    @Test
    public void testReadLinesContext()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "mail/multiple-attachments.eml"));

        reader.setPattern(Pattern.compile("^Content-"));
        reader.setContext(2);

        assertEquals(43, reader.getLineCount());

        List<String> lines = reader.readLines(0, 1000);

        String expected =
            "To: test@example.com\n" +
            "Subject: multiple attachments\n" +
            "Content-Type: multipart/mixed;\n" +
            " boundary=\"------------000301040004090506080306\"\n" +
            "\n" +
            "This is a multi-part message in MIME format.\n" +
            "--------------000301040004090506080306\n" +
            "Content-Type: text/plain; charset=ISO-8859-1; format=flowed\n" +
            "Content-Transfer-Encoding: 7bit\n" +
            "\n" +
            "test body\n" +
            "\n" +
            "--------------000301040004090506080306\n" +
            "Content-Type: text/xml;\n" +
            " name=\"test.xml\"\n" +
            "Content-Transfer-Encoding: 7bit\n" +
            "Content-Disposition: attachment;\n" +
            " filename=\"test.xml\"\n" +
            "\n" +
            "\n" +
            "--------------000301040004090506080306\n" +
            "Content-Type: text/plain; charset=UTF-8;\n" +
            " name=\"test.txt\"\n" +
            "Content-Transfer-Encoding: 7bit\n" +
            "Content-Disposition: attachment;\n" +
            " filename=\"test.txt\"\n" +
            "\n" +
            "\n" +
            "--------------000301040004090506080306\n" +
            "Content-Type: application/x-java-archive;\n" +
            " name=\"testJAR.jar\"\n" +
            "Content-Transfer-Encoding: base64\n" +
            "Content-Disposition: attachment;\n" +
            " filename=\"testJAR.jar\"\n" +
            "\n" +
            "AAAA7gAAAAAA\n" +
            "--------------000301040004090506080306\n" +
            "Content-Type: text/html;\n" +
            " name=\"testHTML_utf8.html\"\n" +
            "Content-Transfer-Encoding: base64\n" +
            "Content-Disposition: attachment;\n" +
            " filename=\"testHTML_utf8.html\"\n";

        assertEquals(expected, StringUtils.join(lines, "\n"));
    }

    @Test
    public void testReadLinesLargeContext()
    throws IOException
    {
        File file = new File(TEST_BASE, "mail/multiple-attachments.eml");

        Grep reader = new Grep(file);

        reader.setPattern(Pattern.compile("^Content-"));
        reader.setContext(10000);

        assertEquals(4204, reader.getLineCount());

        List<String> lines = reader.readLines(0, Integer.MAX_VALUE);

        assertEquals(FileUtils.readLines(file, StandardCharsets.UTF_8), lines);
    }

    @Test
    public void testReadLinesPatternMatchFirstLine()
    throws IOException
    {
        Grep reader = new Grep(new File(TEST_BASE, "mail/multiple-attachments.eml"));

        reader.setPattern(Pattern.compile("^X-Mozilla-"));
        reader.setContext(3);

        assertEquals(13, reader.getLineCount());

        List<String> lines = reader.readLines(0, 1000);

        String expected = "X-Mozilla-Status: 0000\n" +
            "X-Mozilla-Status2: 00000000\n" +
            "X-Mozilla-Keys:                                                                                 \n" +
            "FCC: mailbox://nobody@Local%20Folders/Sent\n" +
            "X-Identity-Key: id1\n" +
            "X-Account-Key: account2\n" +
            "Message-ID: <51A0E66C.9010305@djigzo.com>\n" +
            "Date: Sat, 25 May 2013 18:27:24 +0200\n" +
            "From: Martijn Brinkers <martijn@djigzo.com>\n" +
            "X-Mozilla-Draft-Info: internal/draft; vcard=0; receipt=0; DSN=0; uuencode=0\n" +
            "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20130510 Thunderbird/17.0.6\n" +
            "MIME-Version: 1.0\n" +
            "To: test@example.com";

        assertEquals(expected, StringUtils.join(lines, "\n"));
    }

    @Test
    public void testPagingWithContext()
    throws IOException
    {
        /*
         * Create a test file with a number in each line from the range 0 .. 99
         */
        File file = new File(FileUtils.getTempDirectory(), "testPagingWithContext.txt");

        file.deleteOnExit();

        FileWriter fw = new FileWriter(file);

        for (int i = 0; i < 100; i++) {
            fw.write(i + "\n");
        }

        fw.close();

        Grep reader = new Grep(file);

        reader.setPattern(Pattern.compile("2"));
        reader.setContext(1);

        List<String> lines = reader.readLines(0, 1000);

        for (String line : lines) {
            System.err.println(line);
        }

        assertEquals("1,2,3", StringUtils.join(reader.readLines(0, 3), ','));
        assertEquals("2,3,11", StringUtils.join(reader.readLines(1, 3), ','));
        assertEquals("11,12,13", StringUtils.join(reader.readLines(3, 3), ','));
        assertEquals("19,20,21", StringUtils.join(reader.readLines(6, 3), ','));
        assertEquals("83,91,92,93", StringUtils.join(reader.readLines(35, 100), ','));
    }
}
