package com.ciphermail.core.common.jackson;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.junit.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PKCS10CertificationRequestSerializerTest
{
    private static final String ENCODED_REQUEST =
            """
            "MIICeTCCAWECAQAwNDERMA8GA1UEAwwIam9obiBkb2UxHzAdBgkqhkiG9w0BCQEMEHRlc3RAZXh
            hbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHIherQOAa3qY5iK0CdDv
            L62GnIlWBWhUzfDrrxPQLI+S4xbv8D31LYA8bjlDrxx3FrYJcnmVfd5E4Atywk2BWg7b5jtw2gT3
            4jbrUyrt8xsmU9ID+Viphk4+GyM50tYlAVhEmexxXtXgwOfkyQR5VHrDZTC45i9EKgNtqAxpBxZ3
            ButGt5yAlChx6S7eP5PWczbfX16qh6rQr60h5J3RMqOxhhHor1gVMcRGmoTSgbpcM1btXltgpvl+
            Gi4UKdnVT0nw+qr6gjwme2BvRnV5HXVnNP5T9Y4929kinSnwyjWsDmqT4SXkLGp6rUlPZB2QJHZZ
            oEgJxVRiFqEUTpT0PAgMBAAGgADANBgkqhkiG9w0BAQsFAAOCAQEAgC+GNYq4lcMyh/gRQ5Fn1h/
            opMMKYWu9SED33r/BiaW7VNfgZHjclbuVqlIKvOEuycW0fBSNYPMy/mYbumNSdGoK0fWbSgEsFbr
            gb5imawDboME/Py4uWBPtFn0aUhlCqINuRDsOgtobfya1lSz/LPPNOVfuXoAL0mAEyGtK7TG84Pu
            AhT/ZrbKxFujnptmfE9OwtIlApKnNq/T8cGG7JovdiHubSi3P/9zCiuzTNQuEf/SJXPNJLJk24y4
            8hPSaleOlMX5+o/LkUNL4Ox6hyUa2rk/hLj8xQoU6Q+gAW7HOPWoGsucBdDUEVN2D0ekksm1QgTO
            i5UmxdnUUL8pB6g=="
            """;

    private static PKCS10CertificationRequest createPKCS10CertificationRequest(KeyPair keyPair)
    throws Exception
    {
        X500PrincipalBuilder builder = X500PrincipalBuilder.getInstance();

        builder.setCommonName("john doe");
        builder.setEmail("test@example.com");

        PKCS10CertificationRequestBuilder requestBuilder = new PKCS10CertificationRequestBuilder(
                X500PrincipalUtils.toX500Name(builder.buildPrincipal()), SubjectPublicKeyInfo.getInstance(
                keyPair.getPublic().getEncoded()));

        JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder("SHA256WithRSA");

        contentSignerBuilder.setProvider("BC");

        return requestBuilder.build(contentSignerBuilder.build(keyPair.getPrivate()));
    }

    private static KeyPair generateKeyPair()
    throws Exception
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        SecureRandom randomSource = securityFactory.createSecureRandom();

        KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

        keyPairGenerator.initialize(2048, randomSource);

        return keyPairGenerator.generateKeyPair();
    }

    @Test
    public void serialize()
    throws Exception
    {
        PKCS10CertificationRequest request = createPKCS10CertificationRequest(generateKeyPair());

        String json = JacksonUtil.getObjectMapper().writeValueAsString(request);

        PKCS10CertificationRequest deserialized = JacksonUtil.getObjectMapper().readValue(json,
                PKCS10CertificationRequest.class);

        assertEquals(request, deserialized);
    }

    /*
     * Deserialize a static value making sure old JSON will continue to work
     */
    @Test
    public void deserialize()
    throws Exception
    {
        PKCS10CertificationRequest request = JacksonUtil.getObjectMapper().readValue(
                StringUtils.remove(ENCODED_REQUEST, '\n'),
                PKCS10CertificationRequest.class);

        assertEquals("CN=john doe,E=test@example.com", request.getSubject().toString());
        assertNotNull(request.getSubjectPublicKeyInfo().getPublicKeyData());
    }
}