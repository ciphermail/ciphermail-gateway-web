/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate.validator;

import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PKITrustCheckCertificateValidatorTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Autowired
    private CTLManager ctlManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                pKISecurityServices.getKeyAndCertStore().removeAllEntries();
                pKISecurityServices.getRootStore().removeAllEntries();
                pKISecurityServices.getCRLStore().removeAllEntries();
                ctlManager.getCTL(CTLManager.DEFAULT_CTL).deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificateFile(X509CertStoreExt certStore, File file)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate)
                    {
                        if (!certStore.contains((X509Certificate) certificate))
                        {
                            certStore.addCertificate((X509Certificate) certificate);
                        }
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificates(X509CertStoreExt certStore, File... files)
    {
        for (File file : files) {
            addCertificateFile(certStore, file);
        }
    }

    private void addCRL(X509CRLStoreExt crlStore, File file)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CRL crl = TestUtils.loadX509CRL(file);

                crlStore.addCRL(crl);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private PKITrustCheckCertificateValidator getValidator(X509Certificate certificate, Date date)
    {
        return transactionOperations.execute(status ->
        {
            try {
                PKITrustCheckCertificateValidator validator = pKISecurityServices.
                        getPKITrustCheckCertificateValidatorFactory().createValidator(null);

                validator.setDate(date);

                validator.isValid(certificate);

                return validator;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private X509Certificate getCertificate(X509CertStoreExt certStore, X509CertSelector selector)
    {
        return transactionOperations.execute(status ->
        {
            try {
                Collection<X509Certificate> certificates = certStore.getCertificates(selector);

                assertEquals(1, certificates.size());

                return certificates.iterator().next();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCTLEntry(X509Certificate certificate, CTLEntryStatus ctlStatus, boolean allowExpired)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);

                CTLEntry entry = ctl.createEntry(certificate);

                entry.setStatus(ctlStatus);
                entry.setAllowExpired(allowExpired);

                ctl.addEntry(entry);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testIntermediateBlackListed()
    throws Exception
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer");

        X509Certificate blacklistedCertificate = TestUtils.loadCertificate(file);

        addCTLEntry(blacklistedCertificate, CTLEntryStatus.BLACKLISTED, false);

        Collection<X509Certificate> certificates = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        PKITrustCheckCertificateValidator validator = getValidator(blacklistedCertificate, new Date());
        assertFalse(validator.isValid());
        assertTrue(validator.isBlackListed());
        assertEquals("Certificate found in CTL and is blacklisted.", validator.getFailureMessage());

        // All certs should be invalid because intermediate is blacklisted
        for (X509Certificate certificate : certificates)
        {
            validator = getValidator(certificate, new Date());

            assertFalse(validator.isValid());

            if (validator.isBlackListed()) {
                assertEquals("Intermediate Certificate found in CTL and is blacklisted.", validator.getFailureMessage());
            }
        }
    }

    @Test
    public void testBlackListed()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        addCTLEntry(certificate, CTLEntryStatus.BLACKLISTED, false);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertFalse(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertTrue(validator.isBlackListed());
    }

    @Test
    public void testNoChainWhiteListed()
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, false);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertTrue(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertTrue(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testNoChainWhiteListedExpiredAllowed()
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/thawte-freemail-valid-till-091108.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, true);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertTrue(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertTrue(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testNoChainWhiteListedExpiredNotAllowed()
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/thawte-freemail-valid-till-091108.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        addCTLEntry(certificate, CTLEntryStatus.WHITELISTED, false);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testSelfSigned()
    {
        File file = new File(TestUtils.getTestDataDir(), "certificates/equifax.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }

    @Test
    public void testRevoked()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        addCRL(pKISecurityServices.getCRLStore(), new File(TestUtils.getTestDataDir(), "crls/test-ca.crl"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertFalse(validator.isValid());
        assertTrue(validator.isTrusted());
        assertTrue(validator.isRevoked());
    }

    @Test
    public void testValidCertificate()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertTrue(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());
        assertFalse(validator.isWhiteListed());
        assertFalse(validator.isBlackListed());
    }

    @Test
    public void testExpiredCertificate()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/valid_certificate_mitm_test_ca.cer");

        final X509Certificate certificate = TestUtils.loadCertificate(file);

        Date future = TestUtils.parseDate("01-Nov-2040 07:39:35 GMT");

        PKITrustCheckCertificateValidator validator = getValidator(certificate, future);

        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }

    @Test
    public void testNotValidForSMIME()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"),
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));


        final X509CertSelector selector = new X509CertSelector();

        selector.setIssuer(new X500Principal(
                "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));
        selector.setSerialNumber(BigIntegerUtils.hexDecode("115FD035BA042503BCC6CA44680F9F8"));

        X509Certificate certificate = getCertificate(pKISecurityServices.getKeyAndCertStore(),
                selector);

        PKITrustCheckCertificateValidator validator = getValidator(certificate, new Date());

        assertFalse(validator.isValid());
        assertFalse(validator.isTrusted());
        assertFalse(validator.isRevoked());
    }

    /*
     * Verisign adds a critcal Authority Key Identifier to the CRL.
     *
     * RFC 3280 explicitly says:
     *
     * 4.2.1.1  Authority Key Identifier
     * ....
     * This extension MUST NOT be marked critical.
     *
     * We will therefore ignore this extension if it's critical. This test checks whether
     * the extension is ignored
     */
    @Test
    public void testCriticalCRLBug()
    {
        addCertificates(pKISecurityServices.getRootStore(),
                new File(TestUtils.getTestDataDir(), "certificates/verisign-root-critical-crl-bug.cer"));

        addCertificates(pKISecurityServices.getKeyAndCertStore(),
                new File(TestUtils.getTestDataDir(), "certificates/verisign-intm-critical-crl-bug.cer"));

        addCRL(pKISecurityServices.getCRLStore(),
                new File(TestUtils.getTestDataDir(), "crls/verisign-critical-crl-bug.crl"));

        File file = new File(TestUtils.getTestDataDir(), "certificates/verisign-end-critical-crl-bug.cer");

        final X509Certificate endEntity = TestUtils.loadCertificate(file);

        Date date = TestUtils.parseDate("17-Jun-2011 07:39:35 GMT");

        PKITrustCheckCertificateValidator validator = getValidator(endEntity, date);

        assertTrue(validator.isValid());
        assertTrue(validator.isTrusted());
        assertFalse(validator.isRevoked());

        X509Certificate root = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/verisign-root-critical-crl-bug.cer"));

        X509Certificate intermediate = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                "certificates/verisign-intm-critical-crl-bug.cer"));

        assertEquals(root, validator.getTrustAnchor().getTrustedCert());

        CertPath certPath = validator.getCertPath();

        List<? extends Certificate> certs = certPath.getCertificates();

        assertEquals(2, certs.size());
        assertTrue(certs.contains(endEntity));
        assertTrue(certs.contains(intermediate));
    }
}
