/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.test.TestUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CRLEncoderTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "crls/");

    private static final List<X509CRL> crls = new LinkedList<X509CRL>();

    private static X509CRL loadCRL(String filename) {
        return TestUtils.loadX509CRL(new File(TEST_BASE, filename));
    }

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        crls.add(loadCRL("intel-basic-enterprise-issuing-CA.crl"));
        crls.add(loadCRL("test-ca-no-next-update.crl"));
        crls.add(loadCRL("itrus.com.cn.crl"));
        crls.add(loadCRL("test-ca.crl"));
        crls.add(loadCRL("ThawteSGCCA.crl"));
    }

    @Test
    public void testEncodeDERMultipleCRLs()
    throws Exception
    {
        byte[] encoded = CRLEncoder.encode(crls, ObjectEncoding.DER);

        assertNotNull(encoded);

        Collection<X509CRL> loaded = CRLUtils.readX509CRLs(new ByteArrayInputStream(encoded));

        assertEquals(5, loaded.size());
        assertTrue(loaded.containsAll(crls));
    }

    @Test
    public void testEncodeDERSingleCRL()
    throws Exception
    {
        byte[] encoded = CRLEncoder.encode(loadCRL("intel-basic-enterprise-issuing-CA.crl"), ObjectEncoding.DER);

        assertNotNull(encoded);

        Collection<X509CRL> loaded = CRLUtils.readX509CRLs(new ByteArrayInputStream(encoded));

        assertEquals(1, loaded.size());
    }

    @Test
    public void testEncodePEM()
    throws Exception
    {
        byte[] encoded = CRLEncoder.encode(crls, ObjectEncoding.PEM);

        assertNotNull(encoded);

        Collection<X509CRL> loaded = CRLUtils.readX509CRLs(new ByteArrayInputStream(encoded));

        assertEquals(5, loaded.size());
        assertTrue(loaded.containsAll(crls));
    }
}
