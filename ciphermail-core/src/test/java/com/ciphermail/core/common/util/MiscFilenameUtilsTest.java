/*
 * Copyright (c) 2008-2015, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;


public class MiscFilenameUtilsTest
{
    @Test
    public void testSafeEncodeFilenameWhitelist()
    {
        assertEquals("aAzZ1234567890-_.@", MiscFilenameUtils.safeEncodeFilenameWhitelist("aAzZ1234567890-_.@", false));
        assertNull(MiscFilenameUtils.safeEncodeFilenameWhitelist(null, false));
        assertEquals(" %25%21@%23%7B", MiscFilenameUtils.safeEncodeFilenameWhitelist(" %!@#{", false));
        assertEquals("%2Etest", MiscFilenameUtils.safeEncodeFilenameWhitelist(".test", false));
        assertEquals("%FCber", MiscFilenameUtils.safeEncodeFilenameWhitelist("über", false));
    }

    @Test
    public void testSafeEncodeFilenameWhitelistUnicode() {
        assertEquals("%3B3%3BB%3CE%3C3%3C3%3B1", MiscFilenameUtils.safeEncodeFilenameWhitelist("γλώσσα", false));
    }

    @Test
    public void testSafeEncodeFilenameWhitelistTooLarge()
    {
        String filename = MiscFilenameUtils.safeEncodeFilenameWhitelist(StringUtils.repeat("A", 256), true);

        assertEquals(filename, StringUtils.repeat("A", 255));

        try {
            MiscFilenameUtils.safeEncodeFilenameWhitelist(StringUtils.repeat("A", 256), false);

            fail();
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    public void testSafeEncodeFilenameBlacklist()
    {
        assertEquals("aAzZ1234567890-_.@", MiscFilenameUtils.safeEncodeFilenameBlacklist("aAzZ1234567890-_.@", false));
        assertNull(MiscFilenameUtils.safeEncodeFilenameBlacklist(null, false));
        assertEquals(" %!@#{", MiscFilenameUtils.safeEncodeFilenameBlacklist(" %!@#{", false));
        assertEquals("%2Etest", MiscFilenameUtils.safeEncodeFilenameBlacklist(".test", false));
        assertEquals("über", MiscFilenameUtils.safeEncodeFilenameBlacklist("über", false));
        assertEquals("%9%5C%2F%3A%2A%3F%22%3C%3E%7C", MiscFilenameUtils.safeEncodeFilenameBlacklist("\t\\/:*?\"<>|",
                false));
    }

    @Test
    public void testSafeEncodeFilenameBlacklistTooLarge()
    {
        String filename = MiscFilenameUtils.safeEncodeFilenameBlacklist(StringUtils.repeat("A", 256), true);

        assertEquals(filename, StringUtils.repeat("A", 255));

        try {
            MiscFilenameUtils.safeEncodeFilenameBlacklist(StringUtils.repeat("A", 256), false);

            fail();
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }
}
