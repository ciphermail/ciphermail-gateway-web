/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPKeyRingImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    @Qualifier(CipherMailSystemServices.PGP_KEY_STORE_PROVIDER_SERVICE_NAME)
    private KeyStoreProvider keyStoreProvider;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean addPublicKey(PGPPublicKey publicKey)
    throws Exception
    {
        return keyRing.addPGPPublicKey(publicKey) != null;
    }

    private void addKeyPair(PGPKeyRingPair keyPair)
    throws Exception
    {
        keyRing.addKeyPair(keyPair);
    }

    private void addPublicKeys(Collection<PGPPublicKey> publicKeys)
    throws Exception
    {
        for (PGPPublicKey publicKey : publicKeys) {
            keyRing.addPGPPublicKey(publicKey);
        }
    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws Exception
    {
        return getByKeyID(keyID, MissingKeyAlias.ALLOWED);
    }

    private PGPKeyRingEntry getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, missingKeyAlias);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private Set<PGPKeyRingEntry> getSubkeyEntries(long keyID)
    throws Exception
    {
        return getByKeyID(keyID).getSubkeys();
    }

    private List<PGPKeyRingEntry> search(PGPSearchField searchField, String searchValue,
            PGPSearchParameters searchParameters, Integer firstResult, Integer maxResults)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.search(searchField, searchValue,
                searchParameters, firstResult, maxResults);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        return list;
    }

    private List<PGPKeyRingEntry> getEntries(PGPSearchParameters searchParameters, Integer firstResult,
            Integer maxResults)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getIterator(searchParameters, firstResult,
                maxResults);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        return list;
    }

    private void addEmailAddresses(PGPKeyRingEntry entry, String... emails)
    throws Exception
    {
        addEmailAddresses(entry.getKeyID(), emails);
    }

    private void addEmailAddresses(long keyID, String... emails)
    throws Exception
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        Set<String> email = entry.getEmail();

        email.addAll(Arrays.asList(emails));

        entry.setEmail(email);
    }

    private void removeEmailAddresses(long keyID, String... emails)
    throws Exception
    {
        PGPKeyRingEntry entry = getByKeyID(keyID);

        Set<String> email = entry.getEmail();

        Arrays.asList(emails).forEach(email::remove);

        entry.setEmail(email);
    }

    private Set<String> getEmailAddresses(long keyID)
    throws Exception
    {
        return new LinkedHashSet<>(getByKeyID(keyID).getEmail());
    }

    @Test
    public void testGetByID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                assertNull(keyRing.getByID(UUID.randomUUID()));

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertTrue(addPublicKey(publicKey));

                PGPKeyRingEntry entry = getByKeyID(publicKey.getKeyID());

                assertEquals(entry, keyRing.getByID(entry.getID()));
                assertEquals(entry.hashCode(), keyRing.getByID(entry.getID()).hashCode());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddPublicKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertTrue(addPublicKey(publicKey));

                assertEquals(1, keyRing.getSize());

                PGPKeyRingEntry entry = getByKeyID(publicKey.getKeyID());

                assertArrayEquals(publicKey.getEncoded(), entry.getPublicKey().getEncoded());

                entry = getByKeyID(publicKey.getKeyID(), MissingKeyAlias.NOT_ALLOWED);

                assertNull(entry);

                /*
                 * Add again
                 */
                assertFalse(addPublicKey(publicKey));
                assertEquals(1, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddMasterAndSubKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey key : keys)
                {
                    if (key.isMasterKey()) {
                        masterKey = key;
                    }
                    else {
                        subKey = key;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                addPublicKey(masterKey);
                addPublicKey(subKey);

                assertEquals(2, keyRing.getSize());

                Set<PGPKeyRingEntry> subKeyEntries = getSubkeyEntries(masterKey.getKeyID());

                assertEquals(1, subKeyEntries.size());

                PGPKeyRingEntry entry = subKeyEntries.iterator().next();

                assertArrayEquals(subKey.getEncoded(), entry.getPublicKey().getEncoded());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddSubKeyAndMaster()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = null;
                PGPPublicKey subKey = null;

                for (PGPPublicKey key : keys)
                {
                    if (key.isMasterKey()) {
                        masterKey = key;
                    }
                    else {
                        subKey = key;
                    }
                }

                assertNotNull(masterKey);
                assertNotNull(subKey);

                addPublicKey(subKey);
                addPublicKey(masterKey);

                assertEquals(2, keyRing.getSize());

                Set<PGPKeyRingEntry> subKeyEntries = getSubkeyEntries(masterKey.getKeyID());

                assertEquals(1, subKeyEntries.size());

                PGPKeyRingEntry entry = subKeyEntries.iterator().next();

                assertArrayEquals(subKey.getEncoded(), entry.getPublicKey().getEncoded());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMultipleSubKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                PGPPublicKey masterKey = null;

                for (PGPPublicKey key : keys)
                {
                    addPublicKey(key);

                    if (key.isMasterKey()) {
                        masterKey = key;
                    }
                }

                assertNotNull(masterKey);

                assertEquals(6, keyRing.getSize());

                Set<PGPKeyRingEntry> subKeyEntries = getSubkeyEntries(masterKey.getKeyID());

                assertEquals(5, subKeyEntries.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddPrivateKey()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPSecretKey> keys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : keys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));

                     PGPKeyRingEntry entry = getByKeyID(secretKey.getKeyID());

                     assertNotNull(entry.getPrivateKey());
                }

                assertEquals(6, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                PGPPublicKey masterKey1 = null;

                PGPPublicKey subKey = null;

                for (PGPSecretKey secretKey : secretKeys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));

                     if (secretKey.getPublicKey().isMasterKey()) {
                         masterKey1 = secretKey.getPublicKey();
                     }
                     else {
                         subKey = secretKey.getPublicKey();
                     }
                }

                assertNotNull(masterKey1);

                assertEquals(6, keyRing.getSize());

                addEmailAddresses(getByKeyID(masterKey1.getKeyID()),
                        "test1@example.com", "test2@example.com");

                addEmailAddresses(getByKeyID(masterKey1.getKeyID()),
                        "test3@example.com");

                addEmailAddresses(getByKeyID(subKey.getKeyID()),
                        "test4@example.com");

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey2 = null;

                for (PGPPublicKey key : keys)
                {
                    addPublicKey(key);

                    if (key.isMasterKey()) {
                        masterKey2 = key;
                    }
                }

                assertNotNull(masterKey2);

                addEmailAddresses(getByKeyID(masterKey2.getKeyID()),
                        "test3@example.com", "test4@example.com");

                keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-expiration-2030-12-31.gpg.asc"));

                addPublicKeys(keys);

                addEmailAddresses(getByKeyID(keys.get(0).getKeyID()),
                        "test3@example.com");

                List<PGPKeyRingEntry> entries = search(PGPSearchField.EMAIL, "test1@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), null, null);

                assertEquals(1, entries.size());
                assertEquals(masterKey1.getKeyID(), (long)entries.get(0).getKeyID());

                // Search count with above parameters
                assertEquals(1, keyRing.searchCount(PGPSearchField.EMAIL, "test1@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL)));

                // Check with LIKE
                entries = search(PGPSearchField.EMAIL, "%@example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), null, null);

                assertEquals(4, entries.size());

                // Search count with above parameters
                assertEquals(4, keyRing.searchCount(PGPSearchField.EMAIL, "%@example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL)));

                // Check with LIKE but now only master keys
                entries = search(PGPSearchField.EMAIL, "%@example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                        PGPKeyType.MASTER_KEY), null, null);

                assertEquals(3, entries.size());

                // Search count with above parameters
                assertEquals(3, keyRing.searchCount(PGPSearchField.EMAIL, "%@example.com",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY)));

                // Check if all entries with test3@example.com are found
                entries = search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), null, null);

                assertEquals(3, entries.size());

                // Check first result
                List<PGPKeyRingEntry> otherEntries = search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), 1,
                                1000); /* Oracle does not like Integer.MAX_VALUE it seems to think it's 0 */

                assertEquals(entries.get(1).getID(), otherEntries.get(0).getID());
                assertEquals(entries.get(2).getID(), otherEntries.get(1).getID());

                // Check max result
                otherEntries = search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.ALLOWED,
                                PGPKeyType.ALL), 2, 1);

                assertEquals(1, otherEntries.size());
                assertEquals(entries.get(2).getID(), otherEntries.get(0).getID());


                // Check if all entries without key alias are skipped
                entries = search(PGPSearchField.EMAIL, "test3@example.com",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_UNEXPIRED_ONLY, MissingKeyAlias.NOT_ALLOWED,
                                PGPKeyType.ALL), null, null);

                assertEquals(1, entries.size());

                // Search no match
                entries = search(PGPSearchField.EMAIL,
                        "FFFFFFFFF",
                        null, null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.EMAIL,
                        "FFFFFFFFF",
                        null));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchUserID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys) {
                    addPublicKey(key);
                }

                assertEquals(6, keyRing.getSize());

                List<PGPKeyRingEntry> entries = search(PGPSearchField.USER_ID,
                        "test multiple sub keys (test multiple sub keys) <test@example.com>",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.USER_ID,
                        "test multiple sub keys (test multiple sub keys) <test@example.com>",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search with LIKE
                entries = search(PGPSearchField.USER_ID,
                        "%test@example.com%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.USER_ID,
                        "%test@example.com%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search no match
                entries = search(PGPSearchField.USER_ID,
                        "FFFFFFFFF",
                        null, null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.USER_ID,
                        "FFFFFFFFF",
                        null));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, keyRing.getSize());

                assertTrue(secretKeys.get(0).isMasterKey());

                List<PGPPublicKey> otherKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-expiration-2030-12-31.gpg.asc"));

                for (PGPPublicKey key : otherKeys) {
                    addPublicKey(key);
                }

                assertTrue(otherKeys.get(0).isMasterKey());

                assertEquals(8, keyRing.getSize());

                List<PGPKeyRingEntry> entries = getEntries(
                        new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(8, entries.size());

                // Count with above settings
                assertEquals(8, keyRing.getCount(new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                        PGPKeyType.ALL)));

                // No missing keys
                entries = getEntries(
                        new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(6, entries.size());

                // Count with above settings
                assertEquals(6, keyRing.getCount(
                        new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.NOT_ALLOWED, PGPKeyType.ALL)));

                // Only master keys
                entries = getEntries(
                        new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.MASTER_KEY),
                        null, null);

                assertEquals(2, entries.size());

                // Count with above settings
                assertEquals(2, keyRing.getCount(
                        new PGPSearchParameters(null, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.MASTER_KEY)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchKeyID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys) {
                    addPublicKey(key);
                }

                assertEquals(6, keyRing.getSize());

                // Search match
                List<PGPKeyRingEntry> entries = search(PGPSearchField.KEY_ID,
                        "2CF5B3CA80EF9A79",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("2CF5B3CA80EF9A79", PGPUtils.getKeyIDHex(entries.get(0).getPublicKey().getKeyID()));

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.KEY_ID,
                        "2CF5B3CA80EF9A79",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search match lowercase
                entries = search(PGPSearchField.KEY_ID,
                        "2cf5b3ca80ef9a79",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("2CF5B3CA80EF9A79", PGPUtils.getKeyIDHex(entries.get(0).getPublicKey().getKeyID()));

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.KEY_ID,
                        "2cf5b3ca80ef9a79",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search like
                entries = search(PGPSearchField.KEY_ID,
                        "%EF9A79%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("2CF5B3CA80EF9A79", PGPUtils.getKeyIDHex(entries.get(0).getPublicKey().getKeyID()));
                assertFalse(entries.get(0).isMasterKey());

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.KEY_ID,
                        "%EF9A79%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search no match
                entries = search(PGPSearchField.KEY_ID,
                        "FFFFFFFFF",
                        null, null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.KEY_ID,
                        "FFFFFFFFF",
                        null));

                // Search like only master keys
                entries = search(PGPSearchField.KEY_ID,
                        "%EF9A79%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.KEY_ID,
                        "%EF9A79%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchFingerprint()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys) {
                    addPublicKey(key);
                }

                assertEquals(6, keyRing.getSize());

                // Search match
                List<PGPKeyRingEntry> entries = search(PGPSearchField.FINGERPRINT,
                        "70844CA90E3E2831F8241A6861FEA9640551AAEE",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("70844CA90E3E2831F8241A6861FEA9640551AAEE", PGPPublicKeyInspector.getFingerprintHex(
                        entries.get(0).getPublicKey()));

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.FINGERPRINT,
                        "70844CA90E3E2831F8241A6861FEA9640551AAEE",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search lowercase
                entries = search(PGPSearchField.FINGERPRINT,
                        "70844ca90e3e2831f8241a6861fea9640551aaee",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("70844CA90E3E2831F8241A6861FEA9640551AAEE", PGPPublicKeyInspector.getFingerprintHex(
                        entries.get(0).getPublicKey()));

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.FINGERPRINT,
                        "70844ca90e3e2831f8241a6861fea9640551aaee",
                        new PGPSearchParameters(Match.EXACT, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search like
                entries = search(PGPSearchField.FINGERPRINT,
                        "%a90e3e2831f8241a6861%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL),
                        null, null);

                assertEquals(1, entries.size());
                assertEquals("70844CA90E3E2831F8241A6861FEA9640551AAEE", PGPPublicKeyInspector.getFingerprintHex(
                        entries.get(0).getPublicKey()));
                assertFalse(entries.get(0).isMasterKey());

                // Search count with above settings
                assertEquals(1, keyRing.searchCount(PGPSearchField.FINGERPRINT,
                        "%a90e3e2831f8241a6861%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED, PGPKeyType.ALL)));

                // Search no match
                entries = search(PGPSearchField.FINGERPRINT,
                        "FFFFFFFFF",
                        null, null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.FINGERPRINT,
                        "FFFFFFFFF",
                        null));

                // Search like master key only
                entries = search(PGPSearchField.FINGERPRINT,
                        "%a90e3e2831f8241a6861%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY), null, null);

                assertEquals(0, entries.size());

                // Search count with above settings
                assertEquals(0, keyRing.searchCount(PGPSearchField.FINGERPRINT,
                        "%a90e3e2831f8241a6861%",
                        new PGPSearchParameters(Match.LIKE, Expired.MATCH_ALL, MissingKeyAlias.ALLOWED,
                                PGPKeyType.MASTER_KEY)));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchNoSearch()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.asc"));

                assertEquals(6, keys.size());

                for (PGPPublicKey key : keys) {
                    addPublicKey(key);
                }

                assertEquals(6, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDelete()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());

                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, keyRing.getSize());
                assertEquals(6, keyStoreProvider.getKeyStore().size());

                assertTrue(secretKeys.get(0).isMasterKey());

                keyRing.delete(getByKeyID(secretKeys.get(1).getKeyID()).getID());

                assertEquals(5, keyRing.getSize());
                assertEquals(5, keyStoreProvider.getKeyStore().size());

                keyRing.delete(getByKeyID(secretKeys.get(0).getKeyID()).getID());

                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDeleteAll()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());

                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, keyRing.getSize());
                assertEquals(6, keyStoreProvider.getKeyStore().size());

                keyRing.deleteAll();

                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testPGPKeyRingEventListener()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                final Set<String> added = new HashSet<>();
                final Set<String> deleted = new HashSet<>();
                final Set<String> updated = new HashSet<>();

                keyRing.addPGPKeyRingEventListener(new PGPKeyRingEventListener()
                {
                    @Override
                    public void onKeyRingEntryDeleted(PGPKeyRingEntry entry) {
                        deleted.add(PGPUtils.getKeyIDHex(entry.getKeyID()));
                    }

                    @Override
                    public void onKeyRingEntryAdded(PGPKeyRingEntry entry) {
                        added.add(PGPUtils.getKeyIDHex(entry.getKeyID()));
                    }

                    @Override
                    public void onKeyRingEntryUpdated(PGPKeyRingEntry entry) {
                        updated.add(PGPUtils.getKeyIDHex(entry.getKeyID()));
                    }
                });

                List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                        "test-multiple-sub-keys.gpg.key"));

                PBESecretKeyDecryptor decryptor = new JcePBESecretKeyDecryptorBuilder().build("test".toCharArray());

                for (PGPSecretKey secretKey : secretKeys)
                {
                     addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                             secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, added.size());
                assertEquals(0, deleted.size());
                assertEquals(0, updated.size());

                keyRing.delete(getByKeyID(secretKeys.get(1).getKeyID()).getID());

                assertEquals(5, keyRing.getSize());
                assertEquals(5, keyStoreProvider.getKeyStore().size());

                assertEquals(6, added.size());
                assertEquals(1, deleted.size());
                assertEquals(0, updated.size());

                // Deleting the master key should result in the deletion of all sub keys
                keyRing.delete(getByKeyID(secretKeys.get(0).getKeyID()).getID());

                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());

                assertEquals(6, added.size());
                assertEquals(6, deleted.size());
                assertEquals(0, updated.size());

                added.clear();
                deleted.clear();

                for (PGPSecretKey secretKey : secretKeys)
                {
                    addKeyPair(new PGPKeyRingPairImpl(secretKey.getPublicKey(),
                            secretKey.extractPrivateKey(decryptor)));
                }

                assertEquals(6, keyRing.getSize());
                assertEquals(6, keyStoreProvider.getKeyStore().size());

                assertEquals(6, added.size());
                assertEquals(0, deleted.size());
                assertEquals(0, updated.size());

                keyRing.deleteAll();

                assertEquals(6, added.size());
                assertEquals(6, deleted.size());
                assertEquals(0, updated.size());

                assertEquals(0, keyRing.getSize());
                assertEquals(0, keyStoreProvider.getKeyStore().size());

                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

                addPublicKey(keys.get(0));
                addPublicKey(keys.get(1));

                assertEquals(8, added.size());
                assertEquals(6, deleted.size());
                assertEquals(0, updated.size());

                keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com-revoked.gpg.asc"));

                addPublicKey(keys.get(0));
                addPublicKey(keys.get(1));

                assertEquals(8, added.size());
                assertEquals(6, deleted.size());
                // The master key was updated
                assertEquals(1, updated.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMergeRevoked()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertFalse(masterKey.hasRevocation());
                assertFalse(subKey.hasRevocation());

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));
                // Reimport same keys should not succeed
                assertFalse(addPublicKey(masterKey));
                assertFalse(addPublicKey(subKey));

                assertFalse(getByKeyID(masterKey.getKeyID()).getPublicKey().hasRevocation());
                assertFalse(getByKeyID(subKey.getKeyID()).getPublicKey().hasRevocation());

                assertEquals(2, keyRing.getSize());

                // Now reimport the key with a revocation signature
                List<PGPPublicKey> revokedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-revoked.gpg.asc"));

                assertEquals(2, revokedKeys.size());

                addPublicKey(revokedKeys.get(0));
                addPublicKey(revokedKeys.get(1));

                // The master key should now be revoked
                assertTrue(getByKeyID(masterKey.getKeyID()).getPublicKey().hasRevocation());
                assertFalse(getByKeyID(subKey.getKeyID()).getPublicKey().hasRevocation());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMergeNewUserIDs()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));

                // Add an additional email address. After revocation the email address should not be removed
                addEmailAddresses(masterKey.getKeyID(), "extra@example.com");

                Set<String> emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(2, emails.size());
                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("extra@example.com"));

                // Now reimport the key with an additional UserID
                List<PGPPublicKey> updatedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-additional-userid.gpg.asc"));

                assertEquals(2, updatedKeys.size());

                addPublicKey(updatedKeys.get(0));
                addPublicKey(updatedKeys.get(1));

                emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(3, emails.size());
                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("extra@example.com"));
                assertTrue(emails.contains("test2@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMergeNewUserIDsPostRemove()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));

                // Add an additional email address. After revocation the email address should not be removed
                addEmailAddresses(masterKey.getKeyID(), "extra@example.com");

                Set<String> emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(2, emails.size());
                assertTrue(emails.contains("test@example.com"));
                assertTrue(emails.contains("extra@example.com"));

                // Now remove an email address and then reimport the key with an additional UserID
                removeEmailAddresses(masterKey.getKeyID(), "test@example.com");

                List<PGPPublicKey> updatedKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-additional-userid.gpg.asc"));

                assertEquals(2, updatedKeys.size());

                addPublicKey(updatedKeys.get(0));
                addPublicKey(updatedKeys.get(1));

                emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(2, emails.size());
                assertTrue(emails.contains("extra@example.com"));
                assertTrue(emails.contains("test2@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportKeyWithRevokedUserID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-revoked-userid.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));

                Set<String> emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(1, emails.size());
                assertTrue(emails.contains("test@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateKeyWithReAdedRevokedUserID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-revoked-userid.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));

                Set<String> emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(1, emails.size());
                assertTrue(emails.contains("test@example.com"));

                // Reimport key but now with revoked user id
                keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-revoked-userid-readded.gpg.asc"));

                assertEquals(2, keys.size());

                masterKey = keys.get(0);
                subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertFalse(addPublicKey(subKey));

                emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(1, emails.size());
                assertTrue(emails.contains("test@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUpdateKeyWithRevokedUserID()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com.gpg.asc"));

                assertEquals(2, keys.size());

                PGPPublicKey masterKey = keys.get(0);
                PGPPublicKey subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertTrue(addPublicKey(subKey));

                Set<String> emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(1, emails.size());
                assertTrue(emails.contains("test@example.com"));

                // Reimport key but now with revoked user id and new primary id
                keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                        "test@example.com-new-primary-old-primary-revoked.gpg.asc"));

                assertEquals(2, keys.size());

                masterKey = keys.get(0);
                subKey = keys.get(1);

                assertTrue(addPublicKey(masterKey));
                assertFalse(addPublicKey(subKey));

                emails = getEmailAddresses(masterKey.getKeyID());

                assertEquals(1, emails.size());
                assertTrue(emails.contains("new-primary@example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
