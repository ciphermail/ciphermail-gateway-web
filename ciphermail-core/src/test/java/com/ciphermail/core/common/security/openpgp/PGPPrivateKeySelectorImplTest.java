/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.SigningKeyValidator;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPPrivateKeySelectorImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();

                importSecretKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "pgp/test-multiple-sub-keys.gpg.key")));
                importPublicKeyRing(new FileInputStream(new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc")));

                assertEquals(8, keyRing.getSize());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importSecretKeyRing(InputStream input)
    throws Exception
    {
        new PGPKeyRingImporterImpl(keyRing).importKeyRing(input,
                new StaticPasswordProvider("test"));
    }

    private void importPublicKeyRing(InputStream input)
    throws Exception
    {
        new PGPKeyRingImporterImpl(keyRing).importKeyRing(input, null);
    }

    private PGPKeyRingEntry getByKeyID(long keyID)
    throws Exception
    {
        return getByKeyID(keyID, MissingKeyAlias.ALLOWED);
    }

    private PGPKeyRingEntry getByKeyID(long keyID, MissingKeyAlias missingKeyAlias)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getByKeyID(keyID, missingKeyAlias);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        // Assume we only have one hit for keyID
        assertTrue(list.size() <= 1);

        return !list.isEmpty() ? list.get(0) : null;
    }

    private void addEmailAddresses(PGPKeyRingEntry entry, String... emails)
    throws Exception
    {
        PGPKeyRingEntry updatableEntry = getByKeyID(entry.getKeyID());

        Set<String> email = updatableEntry.getEmail();

        email.addAll(Arrays.asList(emails));

        updatableEntry.setEmail(email);
    }

    private List<PGPKeyRingEntry> getEntries(PGPSearchParameters searchParameters)
    throws Exception
    {
        CloseableIterator<PGPKeyRingEntry> iterator = keyRing.getIterator(searchParameters, null, null);

        assertFalse(iterator.isClosed());

        List<PGPKeyRingEntry> list = CloseableIteratorUtils.toList(iterator);

        assertTrue(iterator.isClosed());

        return list;
    }

    private Set<PGPKeyRingEntry> select(String email)
    throws Exception
    {
        PGPPrivateKeySelectorImpl selector = new PGPPrivateKeySelectorImpl(keyRing,
                new SigningKeyValidator(
                        new PGPKeyFlagCheckerImpl(new PGPSignatureValidatorImpl(),
                        new PGPUserIDValidatorImpl(new PGPSignatureValidatorImpl()))));

        return selector.select(email);
    }

    @Test
    public void testSelectEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPSearchParameters searchParameters = new PGPSearchParameters();

                List<PGPKeyRingEntry> entries = getEntries(searchParameters);

                assertEquals(8, entries.size());

                for (PGPKeyRingEntry entry : entries) {
                    addEmailAddresses(entry, "test@example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test@example.com");

                assertEquals(4, selected.size());

                Set<String> keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("D80D1572D0486F55"));
                assertTrue(keyIds.contains("61FEA9640551AAEE"));
                assertTrue(keyIds.contains("639BCE48E8891CC7"));
                assertTrue(keyIds.contains("80C4F95A9FE00D79"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSelectDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPSearchParameters searchParameters = new PGPSearchParameters();

                List<PGPKeyRingEntry> entries = getEntries(searchParameters);

                assertEquals(8, entries.size());

                for (PGPKeyRingEntry entry : entries) {
                    addEmailAddresses(entry, "example.com");
                }

                Set<PGPKeyRingEntry> selected = select("test@example.com");

                assertEquals(4, selected.size());

                Set<String> keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("D80D1572D0486F55"));
                assertTrue(keyIds.contains("61FEA9640551AAEE"));
                assertTrue(keyIds.contains("639BCE48E8891CC7"));
                assertTrue(keyIds.contains("80C4F95A9FE00D79"));

                selected = select("example.com");

                assertEquals(4, selected.size());

                keyIds = new HashSet<>();

                for (PGPKeyRingEntry key : selected) {
                    keyIds.add(PGPUtils.getKeyIDHex(key.getKeyID()));
                }

                assertTrue(keyIds.contains("D80D1572D0486F55"));
                assertTrue(keyIds.contains("61FEA9640551AAEE"));
                assertTrue(keyIds.contains("639BCE48E8891CC7"));
                assertTrue(keyIds.contains("80C4F95A9FE00D79"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
