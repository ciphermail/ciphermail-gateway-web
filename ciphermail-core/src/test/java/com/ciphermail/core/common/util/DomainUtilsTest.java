/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class DomainUtilsTest
{
    @Test
    public void testIllegalDomain()
    {
        assertNull(DomainUtils.canonicalizeAndValidate("ex_ample.com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("examp&le.com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("examp:le.com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("-example.com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("example.com-", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("-example.com", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate("example.com-", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate(".a.c", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("a.c.", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate(".a.c", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("a.c.", DomainUtils.DomainType.WILD_CARD));

        assertNull(DomainUtils.canonicalizeAndValidate(".a.c", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate("a.c.", DomainUtils.DomainType.FRAGMENT));

        assertNull(DomainUtils.canonicalizeAndValidate("a..b.c", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("a..b.c", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("a..b.c", DomainUtils.DomainType.FRAGMENT));
    }

    @Test
    public void testWildCardDomain()
    {
        assertEquals("*.example.com", DomainUtils.canonicalizeAndValidate("*.example.com", DomainUtils.DomainType.WILD_CARD));
        assertEquals("*.ex-am123ple.com", DomainUtils.canonicalizeAndValidate("*.ex-am123ple.com", DomainUtils.DomainType.WILD_CARD));
        assertEquals("*.example.com", DomainUtils.canonicalizeAndValidate("  *.example.com  ", DomainUtils.DomainType.WILD_CARD));
        assertEquals("*.example.com", DomainUtils.canonicalizeAndValidate("*.EXAMPLE.com", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("xxx", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("*.example.*.com", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("*.ex+ample.com", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("*.example .com", DomainUtils.DomainType.WILD_CARD));
        assertNull(DomainUtils.canonicalizeAndValidate("*.exa_mple .com", DomainUtils.DomainType.WILD_CARD));
    }

    @Test
    public void testFullyQualifiedDomain()
    {
        assertEquals("1.2.3.4", DomainUtils.canonicalizeAndValidate("1.2.3.4", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertEquals("example.com", DomainUtils.canonicalizeAndValidate("example.com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertEquals("example.com", DomainUtils.canonicalizeAndValidate("  example.com  ", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertEquals("exam--ple.com", DomainUtils.canonicalizeAndValidate("  exam--ple.com  ", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("*.example .com", DomainUtils.DomainType.FULLY_QUALIFIED));
        assertNull(DomainUtils.canonicalizeAndValidate("ex+ample.com", DomainUtils.DomainType.FULLY_QUALIFIED));
    }

    @Test
    public void testFragmentDomain()
    {
        assertEquals("example.com", DomainUtils.canonicalizeAndValidate("example.com", DomainUtils.DomainType.FRAGMENT));
        assertEquals("example.com", DomainUtils.canonicalizeAndValidate("  example.com  ", DomainUtils.DomainType.FRAGMENT));
        assertEquals("exam--ple.com", DomainUtils.canonicalizeAndValidate("  exam--ple.com  ", DomainUtils.DomainType.FRAGMENT));
        assertEquals("example", DomainUtils.canonicalizeAndValidate("  example  ", DomainUtils.DomainType.FRAGMENT));
        assertEquals("exa12--mple", DomainUtils.canonicalizeAndValidate("  exa12--mple  ", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate("*.example .com", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate("ex+ample.com", DomainUtils.DomainType.FRAGMENT));
        assertNull(DomainUtils.canonicalizeAndValidate("ex_ample.com", DomainUtils.DomainType.FRAGMENT));
    }

    @Test
    public void testIsWildcardDomain()
    {
        assertTrue(DomainUtils.isWildcardDomain("*.example.com"));
        assertTrue(DomainUtils.isWildcardDomain("    *.example.com   "));
        assertFalse(DomainUtils.isWildcardDomain("example.com"));
        assertFalse(DomainUtils.isWildcardDomain(null));
        assertFalse(DomainUtils.isWildcardDomain(""));
        assertFalse(DomainUtils.isWildcardDomain("*. example.com"));
    }

    @Test
    public void testRemoveWildcard()
    {
        assertEquals("example.com", DomainUtils.removeWildcard("*.example.com"));
        assertEquals("example.com", DomainUtils.removeWildcard("example.com"));
        assertEquals("example.com", DomainUtils.removeWildcard("    example.com    "));
    }

    @Test
    public void testIsWildcardMatchDomain()
    {
        assertTrue(DomainUtils.isWildcardMatchDomain("example.com", "*.example.com"));
        assertTrue(DomainUtils.isWildcardMatchDomain("  example.com    ", "    *.example.com        "));
        assertTrue(DomainUtils.isWildcardMatchDomain("exAMPle.com", "*.example.COM"));
        assertTrue(DomainUtils.isWildcardMatchDomain("test.example.com", "*.example.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain(".example.com", "*.example.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain("tets.test.example.com", "*.example.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain("example.com", "*.test.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain(null, "*.test.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain(null, null));
        assertFalse(DomainUtils.isWildcardMatchDomain("example.com", null));
        assertFalse(DomainUtils.isWildcardMatchDomain("example.com", "example.com"));
        assertFalse(DomainUtils.isWildcardMatchDomain("*.example.com", "*.example.com"));
    }

    @Test
    public void testGetUpperLevelDomain()
    {
        assertEquals("example.com", DomainUtils.getUpperLevelDomain("sub.example.com"));
        assertEquals("com", DomainUtils.getUpperLevelDomain(".com"));
        assertEquals("", DomainUtils.getUpperLevelDomain("com"));
        assertEquals("", DomainUtils.getUpperLevelDomain(null));
        assertEquals("", DomainUtils.getUpperLevelDomain("   "));
    }

    @Test
    public void testIsValid()
    {
        assertTrue(DomainUtils.isValid("*.example.com", DomainUtils.DomainType.WILD_CARD));
        assertFalse(DomainUtils.isValid("*.example.com", DomainUtils.DomainType.FULLY_QUALIFIED));
    }
}
