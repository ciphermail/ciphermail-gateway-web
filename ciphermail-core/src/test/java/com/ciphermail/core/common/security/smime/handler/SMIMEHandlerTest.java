/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.cms.SignerInfoImpl;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.security.smime.SMIMESignedInspectorImpl;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMIMEHandlerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore certStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    private SMIMEHandler smimeHandler;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean key/root/crl store
                certStore.removeAllEntries();
                rootStore.removeAllEntries();

                importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"), "test");

                addCertificates(new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"), rootStore);
                addCertificates(new File(TestUtils.getTestDataDir(), "certificates/mitm-test-ca.cer"), certStore);

                smimeHandler = new SMIMEHandler(pKISecurityServices);
                smimeHandler.setCertificatesEventListener(createCertificatesEventListener());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile,
            String password)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
        keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

        keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
    }

    private static void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        addCertificates(certificates, certStore);
    }

    private static void addCertificates(Collection<? extends Certificate> certificates, X509CertStoreExt certStore)
    throws Exception
    {
        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate && !certStore.contains((X509Certificate) certificate)) {
                certStore.addCertificate((X509Certificate) certificate);
            }
        }
    }

    private CertificateCollectionEvent createCertificatesEventListener()
    {
        return certificates ->
        {
            for (X509Certificate certificate : certificates)
            {
                try {
                    if (!certStore.contains(certificate)) {
                        certStore.addCertificate(certificate);
                    }
                }
                catch (CertStoreException e) {
                    throw new CertificateException(e);
                }
            }
        };
    }

    @Test
    public void testOpaqueSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-opaque-validcertificate.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testOpaqueSignedNonStandardContentType()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/signed-opaque-non-standard-content-type.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSigned()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-validcertificate.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedRemoveSignature()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-validcertificate.eml"));

                smimeHandler.setRemoveSignature(true);

                MimeMessage newMessage =  new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertTrue(newMessage.isMimeType("multipart/mixed"));

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FCD741088707366E9727452C9770//1.2.840.113549.1.1.1", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncrypted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-validcertificate.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("<test@example.com>", header);

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedNonStandardContentType()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-non-standard-smime-headers.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertFalse(smimeHandler.isDecrypted());

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("\"info@example.com\" <info@example.com>", header);

                assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedHiddenHeaders()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-hidden-headers.eml"));

                smimeHandler.setProtectedHeaders("subject", "to", "from");

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("<test@example.com>", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("To", ","));
                assertEquals("<test@example.com>", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("Subject", ","));
                assertEquals("normal message with attachment", header);

                assertTrue(smimeHandler.isDecrypted());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedHiddenHeadersNoSubjectProtect()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-hidden-headers.eml"));

                smimeHandler.setProtectedHeaders("to", "from");

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("<test@example.com>", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("To", ","));
                assertEquals("<test@example.com>", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("Subject", ","));
                assertEquals("xxx", header);

                assertTrue(smimeHandler.isDecrypted());

                assertFalse(smimeHandler.isDecryptionKeyNotFound());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedNoDecryptionKeyFound()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-no-decryption-key.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertFalse(smimeHandler.isDecrypted());

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("DES_EDE3_CBC, Key size: 168", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("\"info@example.com\" <info@example.com>", header);

                assertEquals(SMIMEHeader.Type.ENCRYPTED, SMIMEHeader.getSMIMEContentType(newMessage,
                        SMIMEHeader.Strict.YES));

                assertTrue(smimeHandler.isDecryptionKeyNotFound());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCompressed()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/compressed.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Compressed-0", ","));
                assertEquals("True", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("<test@example.com>", header);

                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testNoSMIME()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/unknown-charset.eml"));

                MimeMessage newMessage = smimeHandler.handlePart(message);

                assertNull(newMessage);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testFakeEncrypted()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/fake-encrypted.eml"));

                MimeMessage newMessage = smimeHandler.handlePart(message);

                assertNull(newMessage);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * Check if the signers are encoded with RSASSA-PSS
     */
    private void checkIfSignersRSASSA_PSSEncoded(MimeMessage message)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

                SMIMESignedInspector inspector = new SMIMESignedInspectorImpl(message,
                        securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

                List<SignerInfo> signers = inspector.getSigners();

                for (SignerInfo signer : signers) {
                    assertEquals("1.2.840.113549.1.1.10", signer.getEncryptionAlgorithmOID());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedSHA256WITHRSAANDMGF1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-SHA256WITHRSAANDMGF1.eml"));

                checkIfSignersRSASSA_PSSEncoded(message);

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.10", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedSHA384WITHRSAANDMGF1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-SHA384WITHRSAANDMGF1.eml"));

                checkIfSignersRSASSA_PSSEncoded(message);

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.10", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testClearSignedSHA512WITHRSAANDMGF1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/clear-signed-SHA512WITHRSAANDMGF1.eml"));

                checkIfSignersRSASSA_PSSEncoded(message);

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.10", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedRSAES_OAEP_SHA1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-RSAES_OAEP_SHA1.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES128_CBC, Key size: 128", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedRSAES_OAEP_SHA256()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-RSAES_OAEP_SHA256.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES128_CBC, Key size: 128", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedRSAES_OAEP_SHA384()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-RSAES_OAEP_SHA384.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES128_CBC, Key size: 128", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEncryptedRSAES_OAEP_SHA512()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/encrypted-RSAES_OAEP_SHA512.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES128_CBC, Key size: 128", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * OpenSSL uses a non standard salt length. So set the salt lenth to the standard value use rsa_pss_saltlen:-1:
     *
     * ./resources/bin/openssl-ciphermail cms -sign -to test@example.com -from test@example.com \
     * -signer ./src/test/resources/testdata/keys/rsa2048.pem -out ~/temp/test.eml -in \
     * ./src/test/resources/testdata/mail/simple-text-message.eml -keyopt rsa_padding_mode:pss -keyopt rsa_pss_saltlen:-1
     */
    @Test
    public void testClearSignedSHA256WITHRSAANDMGF1OpenSSLStandardSaltLength()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-signed-sha256-default-salt-length.eml"));

                checkIfSignersRSASSA_PSSEncoded(message);

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.10", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * OpenSSL signed message using RSASSA-PSS with on default salt length.
     *
     * Because of a bug in BC (http://www.bouncycastle.org/jira/browse/BJA-665) this test fails.
     */
    @Test
    public void testClearSignedSHA256WITHRSAANDMGF1OpenSSLNonStandardSaltLength()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-signed-sha256-non-default-salt-length.eml"));

                checkIfSignersRSASSA_PSSEncoded(message);

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-ID-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                        "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.10", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Signer-Trusted-0-0", ","));
                assertEquals("True", header);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * OpenSSL encrypted file using OAEP with default digest (i.e., digest algo is not in the generated ASN1)
     *
     * Note: the file was generated with the following command
     *
     * ./resources/bin/openssl-ciphermail cms -encrypt -out ~/temp/test.eml \
     * -in ./src/test/resources/testdata/mail/simple-text-message.eml \
     * -recip src/test/resources/testdata/keys/rsa2048.pem -keyopt rsa_padding_mode:oaep --aes256
     */
    @Test
    public void testOpenSSLEncryptedOAEP()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-AES256-OAEP-SHA1-MGF1SHA1.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES256_CBC, Key size: 256", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * OpenSSL encrypted file using OAEP with SHA1 digest and SHA256 MGF1.
     *
     * Note: the file was generated with the following command
     *
     * ./resources/bin/openssl-ciphermail cms -encrypt -out ~/temp/test.eml \
     * -in ./src/test/resources/testdata/mail/simple-text-message.eml \
     * -recip src/test/resources/testdata/keys/rsa2048.pem -keyopt rsa_padding_mode:oaep -keyopt rsa_mgf1_md:sha256 --aes256
     */
    @Test
    public void testOpenSSLEncryptedOAEPSHA1Mgf1SHA256()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-AES256-OAEP-SHA1-MGF1SHA256.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES256_CBC, Key size: 256", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    /*
     * OpenSSL encrypted file using OAEP with SHA256 digest and SHA256 MGF1.
     *
     * Note: the file was generated with the following command
     *
     * ./resources/bin/openssl-ciphermail cms -encrypt -out ~/temp/test.eml \
     * -in ./src/test/resources/testdata/mail/simple-text-message.eml \
     * -recip src/test/resources/testdata/keys/rsa2048.pem -keyopt rsa_padding_mode:oaep -keyopt rsa_mgf1_md:sha256
     * -keyopt rsa_oaep_md:sha256 --aes256
     */
    @Test
    public void testOpenSSLEncryptedSHA256Mgf1SHA256()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-AES256-OAEP-SHA256-MGF1SHA256.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES256_CBC, Key size: 256", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * OpenSSL encrypted file using OAEP with SHA256 digest and SHA256 MGF1.
     *
     * Note: the file was generated with the following command
     *
     * ./resources/bin/openssl-ciphermail cms -encrypt -out ~/temp/test.eml \
     * -in ./src/test/resources/testdata/mail/simple-text-message.eml \
     * -recip src/test/resources/testdata/keys/rsa2048.pem -keyopt rsa_padding_mode:oaep -keyopt rsa_mgf1_md:sha1
     * -keyopt rsa_oaep_md:sha256 --aes256
     */
    @Test
    public void testOpenSSLEncryptedSHA256Mgf1SHA1()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/openssl-AES256-OAEP-SHA256-MGF1SHA1.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("AES256_CBC, Key size: 256", HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ",")));

                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, "
                    + "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters",
                        HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                                "X-CipherMail-Info-Encryption-Recipient-0-0", ",")));

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    /*
     * S/MIME signed message signed with PGP universal. BC 1.56 verification fails but BC 1.51 succeeds. The message
     * uses DL encoding for the signed attributes but it must be DER encoded. BC 1.56 is now stricter for security
     * reasons.
     */
    @Test
    public void testClearSignedDLEncodedSignedAttributes()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/smime-clear-signed-DL-encoded-signed-attributes.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                // Should not validate because the workaround is not active
                assertEquals("False", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));

                // activate work around
                try {
                    SignerInfoImpl.setDlEncodeSignedAttributes(true);

                    newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(smimeHandler.handlePart(message),
                            smimeHandler, 0);

                    assertNotNull(newMessage);
                    assertNotSame(newMessage, message);

                    MailUtils.validateMessage(newMessage);

                    assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Signed", ","));
                    assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted"));

                    assertEquals("True", newMessage.getHeader("X-CipherMail-Info-Signer-Verified-0-0", ","));
                }
                finally {
                    SignerInfoImpl.setDlEncodeSignedAttributes(false);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAES256_GCM_OAEP_SHA512()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MimeMessage message = MailUtils.loadMessage(new File(TestUtils.getTestDataDir(),
                        "mail/smime-aes256-gcm-oaep-sha512.eml"));

                MimeMessage newMessage = new SMIMEInfoHandlerImpl(pKISecurityServices).handle(
                        smimeHandler.handlePart(message), smimeHandler, 0);

                assertNotNull(newMessage);
                assertNotSame(newMessage, message);

                MailUtils.validateMessage(newMessage);

                String header = HeaderUtils.decodeHeaderValue(newMessage.getHeader(
                        "X-CipherMail-Info-Encryption-Algorithm-0", ","));
                assertEquals("AES256_GCM, Key size: 256", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("X-CipherMail-Info-Encryption-Recipient-0-0", ","));
                assertEquals("EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, " +
                             "C=NL/115FD1606444BC50DE5464AF7D0D468//1.2.840.113549.1.1.7/OAEP Parameters", header);

                header = HeaderUtils.decodeHeaderValue(newMessage.getHeader("From", ","));
                assertEquals("from@example.com", header);

                assertTrue(smimeHandler.isDecrypted());

                assertEquals("True", newMessage.getHeader("X-CipherMail-Info-SMIME-Encrypted", ","));
                assertNull(newMessage.getHeader("X-CipherMail-Info-SMIME-Signed"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
