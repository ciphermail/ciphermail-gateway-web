/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.digest;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class DigestsTest
{
    @Test
    public void testSha512Hex()
    throws Exception
    {
        byte[] data = "martijn".getBytes(StandardCharsets.US_ASCII);

        String hex = Digests.digestHex(data, Digest.SHA512);

        assertEquals("BCC8B65D6B34E48E039F383A7737B509E340648D9C266C89E72B0FB237317722AA31BF9215F62C4C1E11F7114C35447A758DF0472DA955D6D3EF5B51D28C510F", hex);
    }

    @Test
    public void testSha256Hex()
    throws Exception
    {
        byte[] data = "martijn".getBytes(StandardCharsets.US_ASCII);

        String hex = Digests.digestHex(data, Digest.SHA256);

        assertEquals("664ECC33881FA0A64DA3CE5BC83BC6E9A2EBE4E42BF44781822EEAF8A5218C14", hex);
    }

    @Test
    public void testSha1Hex()
    throws Exception
    {
        byte[] data = "martijn".getBytes(StandardCharsets.US_ASCII);

        String hex = Digests.digestHex(data, Digest.SHA1);

        assertEquals("10AE63B69AE5AB71B07F9E64004A0207F53FEA34", hex);
    }

    @Test
    public void testMD5Hex()
    throws Exception
    {
        byte[] data = "martijn".getBytes(StandardCharsets.US_ASCII);

        String hex = Digests.digestHex(data, Digest.MD5);

        assertEquals("87DF3CD22D6F9F8005BB222ED4C11D66", hex);
    }

    @Test
    public void timingTest()
    throws Exception
    {
        final int count = 10000;

        byte[] data = new byte[2048];

        new Random().nextBytes(data);

        long start = System.currentTimeMillis();

        byte[] result;

        for(int i = 0; i < count; i++) {
            result = Digests.digest(data, Digest.SHA512);

            System.arraycopy(result, 0, data, 0, result.length);
        }

        System.out.println("sha512 duration: " + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();

        for(int i = 0; i < count; i++) {
            result = Digests.digest(data, Digest.SHA256);

            System.arraycopy(result, 0, data, 0, result.length);
        }

        System.out.println("sha256 duration: " + (System.currentTimeMillis() - start));
    }
}
