/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.test.OpenSSL;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OpenSSLTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static SecurityFactory securityFactory;
    private static KeyStore keyStore;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        keyStore = loadKeyStore(new File(TEST_BASE, "keys/testCertificates.p12"), "test");
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    @Test(timeout = 5000)
    public void testDecrypt()
    throws Exception
    {
        System.out.println("Starting testDecrypt");

        OpenSSL openssl = new OpenSSL();

        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

        PrivateKeyEntry entry = (PrivateKeyEntry) keyStore.getEntry("ValidCertificate", passwd);

        openssl.setPrivateKey(entry.getPrivateKey());

        File encryptedFile = new File(TEST_BASE, "mail/encrypted-validcertificate.eml");

        OutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        openssl.cmsDecrypt(encryptedFile, output, error);

        String errorText = error.toString(StandardCharsets.US_ASCII);

        System.out.println(error);

        assertEquals("", errorText.trim());

        IOUtils.closeQuietly(output);
        IOUtils.closeQuietly(error);
    }

    @Test(timeout = 5000)
    public void testVerify()
    throws Exception
    {
        System.out.println("Starting testVerify");

        OpenSSL openssl = new OpenSSL();

        File signedFile = new File(TEST_BASE, "mail/clear-signed-validcertificate.eml");

        X509Certificate certificate = (X509Certificate) keyStore.getCertificate("root");

        openssl.setCertificateAuthorities(certificate);

        OutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        openssl.cmsVerify(signedFile, output, error);

        String errorText = error.toString(StandardCharsets.US_ASCII);

        System.out.println(error);

        assertTrue(errorText.startsWith("CMS Verification successful"));

        IOUtils.closeQuietly(output);
        IOUtils.closeQuietly(error);
    }
}
