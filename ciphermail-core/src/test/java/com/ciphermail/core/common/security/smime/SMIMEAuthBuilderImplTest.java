/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.util.WordIterator;
import com.ciphermail.core.test.OpenSSL;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * Test for SMIMEBuilderImpl. This test requires openssl to be installed and in the path because
 * openssl is used for interoperability testing and testing whether the build s/mime messages
 * are correct.
 *
 * @author Martijn Brinkers
 *
 */
class SMIMEAuthBuilderImplTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static SecurityFactory securityFactory;

    private static X509Certificate encryptionCertificate;
    private static PrivateKeyEntry privateKeyEntry;

    @BeforeAll
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        KeyStore keyStore = loadKeyStore(new File(TEST_BASE, "keys/testCertificates.p12"), "test");

        encryptionCertificate = (X509Certificate) keyStore.getCertificate("ValidCertificate");

        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection("test".toCharArray());

        privateKeyEntry = (PrivateKeyEntry) keyStore.getEntry("ValidCertificate", passwd);
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    private static String as1Dump(File messageFile)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        try {
            openssl.cmsASN1Dump(messageFile, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }

        String asn1 = output.toString(StandardCharsets.UTF_8);

        // Print before normalization
        System.out.println(asn1);

        // Normalize the asn1 dump to make it easier to match
        WordIterator wi = new WordIterator(new StringReader(asn1));

        StringWriter sw = new StringWriter();

        String word;

        while ((word = wi.nextWord()) != null)
        {
            word = StringUtils.trimToNull(word);

            if (word != null) {
                sw.append(word.toLowerCase()).append(' ');
            }
        }

        return sw.toString();
    }

    /*
     * decrypt the message using openssl
     */
    private static void decryptMessage(File messageFile, PrivateKey privateKey, File outputFile)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        openssl.setPrivateKey(privateKey);

        FileOutputStream output = new FileOutputStream(outputFile);
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        try {
            openssl.cmsDecrypt(messageFile, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
        }

        output.close();
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        Assertions.assertEquals("<test@example.com>", message.getHeader("from", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }

    /*
     * Check for some headers which should exist because they also exist on the source message.
     */
    private static void checkForSourceHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        Assertions.assertEquals("<test@example.com>", message.getHeader("from", ","));
        Assertions.assertEquals("<test@example.com>", message.getHeader("to", ","));
        Assertions.assertEquals("normal message with attachment", message.getHeader("subject", ","));
        Assertions.assertEquals("1.0", message.getHeader("MIME-Version", ","));
        Assertions.assertEquals("3", message.getHeader("X-Priority", ","));
        Assertions.assertEquals("Normal", message.getHeader("X-MSMail-Priority", ","));
        Assertions.assertEquals("Produced By Microsoft MimeOLE V6.00.2800.1896", message.getHeader("X-MimeOLE", ","));
        Assertions.assertEquals("test 1,test 2", message.getHeader("X-Test", ","));
        Assertions.assertEquals("test 3", message.getHeader("X-Test-a", ","));
        Assertions.assertEquals("test 4", message.getHeader("X-Test-b", ","));
    }

    /*
     * AES Encrypt a multipart/mixed message with AES128 GCM
     */
    @Test
    void testAES128_CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEAuthBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES128_GCM);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED_AUTH, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);

        String mime = MailUtils.partToMimeString(newMessage);
        Assertions.assertTrue(mime.contains("filename=\"cityinfo.zip\""));

        String asn1 = as1Dump(file);

        Assertions.assertTrue(asn1.contains("contenttype: id-smime-ct-authenvelopeddata (1.2.840.113549.1.9.16.1.23)"));
        Assertions.assertTrue(asn1.contains("algorithm: aes-128-gcm (2.16.840.1.101.3.4.1.6)"));
    }

    /*
     * AES Encrypt a multipart/mixed message with AES192 GCM
     */
    @Test
    void testAES192_CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEAuthBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES192_GCM);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED_AUTH, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);

        String mime = MailUtils.partToMimeString(newMessage);
        Assertions.assertTrue(mime.contains("filename=\"cityinfo.zip\""));

        String asn1 = as1Dump(file);

        Assertions.assertTrue(asn1.contains("contenttype: id-smime-ct-authenvelopeddata (1.2.840.113549.1.9.16.1.23)"));
        Assertions.assertTrue(asn1.contains("algorithm: aes-192-gcm (2.16.840.1.101.3.4.1.26)"));
    }

    /*
     * AES Encrypt a multipart/mixed message with AES192 GCM
     */
    @Test
    void testAES256_CGM()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        SMIMEBuilder builder = new SMIMEAuthBuilderImpl(message, "to", "subject", "from");

        builder.addRecipient(encryptionCertificate, SMIMERecipientMode.ISSUER_SERIAL);

        builder.encrypt(SMIMEEncryptionAlgorithm.AES256_GCM);

        MimeMessage newMessage = builder.buildMessage();

        File file = TestUtils.createTempFile(".eml");

        FileOutputStream output = new FileOutputStream(file);

        MailUtils.writeMessage(newMessage, output);

        newMessage = MailUtils.loadMessage(file);

        Assertions.assertEquals(SMIMEHeader.Type.ENCRYPTED_AUTH, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForSourceHeaders(newMessage);

        File opensslOutputFile = TestUtils.createTempFile(".eml");

        decryptMessage(file, privateKeyEntry.getPrivateKey(), opensslOutputFile);

        newMessage = MailUtils.loadMessage(opensslOutputFile);

        Assertions.assertTrue(newMessage.isMimeType("multipart/mixed"));

        Assertions.assertEquals(SMIMEHeader.Type.NO_SMIME, SMIMEHeader.getSMIMEContentType(newMessage));

        checkForEmbeddedHeaders(newMessage);

        String mime = MailUtils.partToMimeString(newMessage);
        Assertions.assertTrue(mime.contains("filename=\"cityinfo.zip\""));

        String asn1 = as1Dump(file);

        Assertions.assertTrue(asn1.contains("contenttype: id-smime-ct-authenvelopeddata (1.2.840.113549.1.9.16.1.23)"));
        Assertions.assertTrue(asn1.contains("algorithm: aes-256-gcm (2.16.840.1.101.3.4.1.46)"));
    }
}
