/*
 * Copyright (c) 2019-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class JSONUtilsTest
{
    @Test
    public void testCheckSupportedNamedParameters()
    throws JSONException
    {
        JSONObject json = new JSONObject("{'a':'1', 'b':'2'}");

        JSONUtils.checkSupportedNamedParameters(json, "a", "b");

        JSONUtils.checkSupportedNamedParameters(json, "a", "b", "c");

        try {
            JSONUtils.checkSupportedNamedParameters(json, "a", "c");

            fail();
        }
        catch (JSONException e) {
            assertEquals("Unsupported parameter b", e.getMessage());
        }

        try {
            JSONUtils.checkSupportedNamedParameters(json, "a", "B");

            fail();
        }
        catch (JSONException e) {
            assertEquals("Unsupported parameter b", e.getMessage());
        }
    }

    @Test
    public void testDeepMerge()
    throws JSONException
    {
        JSONObject source = new JSONObject(
                """
                        {
                          "extended_key_usages": ["1.3.6.1.5.5.7.3.4"],
                          "public_key": "-----BEGIN CERTIFICATE REQUEST-----..-----END CERTIFICATE REQUEST-----\\n",
                          "signature": {"hash_algorithm": "SHA-1"},
                          "subject_dn": {"email": "martijn@ciphermail.com"},
                          "validity": {"not_after": "1646151915"}
                        }
                        """);

        JSONObject target = new JSONObject(
                """
                        {
                          "extended_key_usages": ["1.3.6.1.5.5.7.3.4"],
                          "public_key": "-----BEGIN CERTIFICATE REQUEST-----..-----END CERTIFICATE REQUEST-----\\n",
                          "san": {"emails": ["martijn@ciphermail.com"]},
                          "signature": {"hash_algorithm": "SHA-256"},
                          "subject_dn": {"city": "Amsterdam"},
                          "validity": {"not_before": "1646151915"}
                        }
                        """);

        JSONObject merged = JSONUtils.deepMerge(source, target);

        assertTrue(merged.toString(2), new JSONObject(
                """
                        {
                          "extended_key_usages": [
                            "1.3.6.1.5.5.7.3.4",
                            "1.3.6.1.5.5.7.3.4"
                          ],
                          "public_key": "-----BEGIN CERTIFICATE REQUEST-----..-----END CERTIFICATE REQUEST-----\\n",
                          "san": {"emails": ["martijn@ciphermail.com"]},
                          "subject_dn": {
                            "city": "Amsterdam",
                            "email": "martijn@ciphermail.com"
                          },
                          "signature": {"hash_algorithm": "SHA-1"},
                          "validity": {
                            "not_after": "1646151915",
                            "not_before": "1646151915"
                          }
                        }
                        """).similar(merged));
    }

    @Test
    public void testDeepMergeNested()
    throws JSONException
    {
        JSONObject source = new JSONObject(
                """
                        {"a": {"b": {
                          "x": 9,
                          "y": "a"
                        }}}""");

        JSONObject target = new JSONObject(
                """
                        {"a": {"b": {
                          "x": 1,
                          "z": "a"
                        }}}
                        """);

        JSONObject merged = JSONUtils.deepMerge(source, target);

        assertTrue(merged.toString(2), new JSONObject(
                """
                        {"a": {"b": {
                          "x": 9,
                          "y": "a",
                          "z": "a"
                        }}}
                        """).similar(merged));
    }

    @Test
    public void testDeepMergeNestedMismatch()
    throws JSONException
    {
        JSONObject source = new JSONObject(
                """
                        {"a": {"b": {
                          "x": 9,
                          "y": "a"
                        }}}
                        """);

        JSONObject target = new JSONObject(
                """
                        {"a": {"b": {
                          "x": "a",
                          "z": "a"
                        }}}
                        """);

        try {
            JSONUtils.deepMerge(source, target);

            fail();
        }
        catch (Exception e) {
            assertEquals("Source value is-a class java.lang.Integer but target value is-a class java.lang.String",
                    e.getMessage());
        }
    }

    @Test
    public void testReplaceTokens()
    throws JSONException
    {
        JSONObject json = new JSONObject("{'x':'1'}");

        Map<String, String> replace = new HashMap<>();
        replace.put("replace", "value");

        StrSubstitutor substitutor = new StrSubstitutor(replace);

        assertEquals("{\"x\":\"1\"}", JSONUtils.replaceTokens(json, substitutor).toString());

        json = new JSONObject("{'x':'${replace}'}");
        assertEquals("{\"x\":\"value\"}", JSONUtils.replaceTokens(json, substitutor).toString());

        json = new JSONObject("{'x':'${non_exist}'}");
        assertNull(JSONUtils.replaceTokens(json, substitutor));

        json = new JSONObject("{'x':'1', 'y':'${non_exist}'}");
        assertEquals("{\"x\":\"1\"}", JSONUtils.replaceTokens(json, substitutor).toString());

        json = new JSONObject("{array:['a', '${replace}', '${non_exist}']}");
        assertEquals("{\"array\":[\"a\",\"value\"]}", JSONUtils.replaceTokens(json, substitutor).toString());

        json = new JSONObject("{array:[{x:'${replace}', y:'${non_exist}'}]}");
        assertEquals("{\"array\":[{\"x\":\"value\"}]}", JSONUtils.replaceTokens(json, substitutor).toString());

        assertNull(JSONUtils.replaceTokens(null, substitutor));
        assertNull(JSONUtils.replaceTokens(new JSONObject(), substitutor));
    }

    @Test
    public void testIsValidJSONObject()
    throws JSONException
    {
        assertTrue(JSONUtils.isValidJSONObject("{}"));
        assertTrue(JSONUtils.isValidJSONObject("{array:[{x:1}]}"));
        assertFalse(JSONUtils.isValidJSONObject(""));
        assertFalse(JSONUtils.isValidJSONObject("test"));
        assertFalse(JSONUtils.isValidJSONObject("[1,2]"));
    }

    @Test
    public void testIsValidJSONArray()
    throws JSONException
    {
        assertFalse(JSONUtils.isValidJSONArray("{}"));
        assertFalse(JSONUtils.isValidJSONArray("{array:[{x:1}]}"));
        assertFalse(JSONUtils.isValidJSONArray(""));
        assertTrue(JSONUtils.isValidJSONArray("[1,2]"));
        assertTrue(JSONUtils.isValidJSONArray("[]"));
    }
}