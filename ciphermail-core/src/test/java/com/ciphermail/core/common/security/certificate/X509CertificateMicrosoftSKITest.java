/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.asn1.x509.Extension;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;


public class X509CertificateMicrosoftSKITest
{
    public static final File TEST_BASE = TestUtils.getTestDataDir();

    @Test
    public void testOutlook2000SKI()
    {
        File file = new File(TEST_BASE, "certificates/outlook2010_cert_missing_subjkeyid.pem");

        X509Certificate certificate = new X509CertificateMicrosoftSKI(TestUtils.loadCertificate(file));

        byte[] ski = certificate.getExtensionValue(Extension.subjectKeyIdentifier.getId());

        assertEquals("041604142219E504D5750B37D20CC930B14129E1A2E583B1", HexUtils.hexEncode(ski));
    }

    @Test
    public void testExistingSKI()
    {
        File file = new File(TEST_BASE, "certificates/multipleemail.cer");

        X509Certificate certificate = new X509CertificateMicrosoftSKI(TestUtils.loadCertificate(file));

        byte[] ski = certificate.getExtensionValue(Extension.subjectKeyIdentifier.getId());

        assertEquals("04160414819CAC7CFB18F9C9D9E7939D11B7489CBDCB3D07", HexUtils.hexEncode(ski));
    }
}
