/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class MiscArrayUtilsTest
{
    @Test
    public void testCompareEqualSizeArray()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)2, (byte)3};

        assertEquals(0, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testCompareDiffRightEqualSizeArray()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)4, (byte)3};

        assertEquals(-1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testCompareDiffLeftEqualSizeArray()
    {
        byte[] a = new byte[]{ (byte)1, (byte)4, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)2, (byte)3};

        assertEquals(1, MiscArrayUtils.compareArray(a, b));
    }


    @Test
    public void testSizeDiffLeftSmallerArray()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2};
        byte[] b = new byte[]{ (byte)1, (byte)2, (byte)3};

        assertEquals(-1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testSizeDiffLeftSmallerArrayNeg()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2};
        byte[] b = new byte[]{ (byte)1, (byte)3, (byte)3};

        assertEquals(-1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testSizeDiffLeftSmallerArrayPos()
    {
        byte[] a = new byte[]{ (byte)1, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)2, (byte)3};

        assertEquals(1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testSizeDiffLeftSmallerArray2()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)2};

        assertEquals(1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testSizeDiffLeftSmallerArrayNeg2()
    {
        byte[] a = new byte[]{ (byte)1, (byte)3, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)2};

        assertEquals(1, MiscArrayUtils.compareArray(a, b));
    }

    @Test
    public void testSizeDiffLeftSmallerArrayPos2()
    {
        byte[] a = new byte[]{ (byte)1, (byte)2, (byte)3};
        byte[] b = new byte[]{ (byte)1, (byte)3};

        assertEquals(-1, MiscArrayUtils.compareArray(a, b));
    }

    private void assertClear(char[] array)
    {
        for (char c : array) {
            assertEquals(0, c);
        }
    }

    @Test
    public void clearTest()
    {
        assertClear(MiscArrayUtils.clear("test".toCharArray()));
        assertClear(MiscArrayUtils.clear("".toCharArray()));
        assertNull(MiscArrayUtils.clear(null));
    }

    private void testMaxRadix(byte[] bytes)
    {
        String radix = MiscArrayUtils.toMaxRadix(bytes);

        assertFalse(StringUtils.startsWith(radix, "-"));

        byte[] fromString = MiscArrayUtils.fromMaxRadix(radix);

        assertEquals(0, MiscArrayUtils.compareArray(bytes, fromString));
    }

    @Test
    public void testMaxRadix()
    {
        testMaxRadix(new byte[]{-127, -64, -32, -16, -8, -4, -2, -1, 0, 1, 2, 4, 8, 16, 32, 64, 127});
        testMaxRadix(new byte[]{1, 2, 3});
        testMaxRadix(new byte[]{127});
        testMaxRadix(new byte[]{0, 0, 1});
        testMaxRadix(new byte[]{0, 0, 0});
        testMaxRadix(new byte[]{});
    }

    @Test
    public void testSafeGet()
    {
        assertEquals("1", MiscArrayUtils.safeGet(new String[]{"1", "2"}, 0));
        assertEquals("1", MiscArrayUtils.safeGet(new String[]{"1"}, 0));
        assertNull(MiscArrayUtils.safeGet(new String[]{null}, 0));
        assertEquals("2", MiscArrayUtils.safeGet(new String[]{"1", "2"}, 1));
        assertNull(MiscArrayUtils.safeGet(new String[]{"1", "2"}, 2));
        assertNull(MiscArrayUtils.safeGet(new String[]{"1", "2"}, -1));
        assertNull(MiscArrayUtils.safeGet(null, 0));
    }
}
