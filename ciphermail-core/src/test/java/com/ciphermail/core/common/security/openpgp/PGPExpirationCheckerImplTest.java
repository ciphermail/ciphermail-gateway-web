/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.PGPKeyRingParser.KeyRingEventListener;
import com.ciphermail.core.common.security.password.PasswordProvider;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPExpirationCheckerImplTest
{
    private static final Logger logger = LoggerFactory.getLogger(PGPExpirationCheckerImplTest.class);

    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final PGPExpirationCheckerImpl expirationChecker = new PGPExpirationCheckerImpl(
            new PGPSignatureValidatorImpl());

    @Test
    public void testNoExpirationDate()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys) {
            assertNull(expirationChecker.getKeyExpirationDate(key, masterKey));
        }
    }

    @Test
    public void testIsNotExpired()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys) {
            assertFalse(expirationChecker.isKeyExpired(key, masterKey));
        }
    }

    @Test
    public void testExpirationDate()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys)
        {
            assertEquals(TestUtils.parseDate("31-Dec-2030 00:00:00 CET"),
                    expirationChecker.getKeyExpirationDate(key, masterKey));
        }
    }

    @Test
    public void testIsExpiredNotYet()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys) {
            assertFalse(expirationChecker.isKeyExpired(key, masterKey));
        }
    }

    @Test
    public void testIsExpired()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "expired.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys) {
            assertTrue(expirationChecker.isKeyExpired(key, masterKey));
        }
    }

    @Test
    public void testExpirationDates()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "expired.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        assertEquals(TestUtils.parseDate("11-Jan-2013 08:02:10 CET"),
                expirationChecker.getKeyExpirationDate(keys.get(0), null));

        assertEquals(TestUtils.parseDate("11-Jan-2013 08:02:30 CET"),
                expirationChecker.getKeyExpirationDate(keys.get(1), masterKey));
    }

    @Test
    public void testTwoUserIDsOneExpiration()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "expired-and-non-expired-userid.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        assertEquals(TestUtils.parseDate("29-Jul-2008 17:38:25 CEST"),
                expirationChecker.getKeyExpirationDate(keys.get(0), null));

        assertEquals(TestUtils.parseDate("29-Jul-2008 17:39:28 CEST"),
                expirationChecker.getKeyExpirationDate(keys.get(1), masterKey));
    }

    @Test
    public void testV2Expiration()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "v2-key.asc"));

        assertEquals(1, keys.size());

        assertEquals(TestUtils.parseDate("22-Aug-1999 03:33:50 CEST"),
                expirationChecker.getKeyExpirationDate(keys.get(0), null));
    }

    @Test
    public void testMasterExpirationBulk()
    throws Exception
    {
        final MutableInt publicKeyRingCount = new MutableInt();

        final GPG gpg = new GPG();

        KeyRingEventListener keyRingEventListener = new KeyRingEventListener()
        {
            @Override
            public void handleSecretKeyRing(PGPSecretKeyRing secretKeyRing, PasswordProvider passwordProvider) {
                // empty on purpose
            }

            @Override
            public void handlePublicKeyRing(PGPPublicKeyRing publicKeyRing)
            throws IOException, PGPException
            {
                publicKeyRingCount.increment();

                try {
                    File keyFile = TestUtils.createTempFile(".asc");

                    publicKeyRing.encode(new FileOutputStream(keyFile));

                    // Get the expiration date according to gpg
                    Date gnuPGExpirationDate = gpg.getMasterKeyExpirationDate(keyFile);

                    Iterator<?> it = publicKeyRing.getPublicKeys();

                    if (it.hasNext())
                    {
                        PGPPublicKey key = (PGPPublicKey) it.next();

                        String keyIDHex = PGPUtils.getKeyIDHex(key.getKeyID());

                        if ("79806234ED5DFD06".equals(keyIDHex) ||
                            "FA6BCB8EE50433B5".equals(keyIDHex) ||
                            "A73452FA58A9E840".equals(keyIDHex) ||
                            "D2704076DAE87165".equals(keyIDHex) ||
                            "06F3BCC5C37D02C6".equals(keyIDHex) ||
                            "7BD1FDA20D06BD4D".equals(keyIDHex) ||
                            "5484421823D10CB1".equals(keyIDHex) ||
                            "0D57EF4B12031472".equals(keyIDHex) ||
                            "12CCB32440405470".equals(keyIDHex) ||
                            "EA5AD74273EBB257".equals(keyIDHex) ||
                            "4D3F2C9004CED607".equals(keyIDHex) ||
                            "5EA46C346426C7FE".equals(keyIDHex) ||
                            "D59D7AE3A6D0840C".equals(keyIDHex) ||
                            "8F9A865A6895E04B".equals(keyIDHex) ||
                            "44EABDCAA459ED73".equals(keyIDHex)
                        ) {
                            // Some keys have an expiration mismatch. Should investigate why. Most likely reason is
                            // that a key was revoked. However, the ciphermail code checks revocation not during
                            // key expiration but at a different time
                        }
                        else {
                            if (key.isMasterKey())
                            {
                                // Get the expiration date according to ciphermail
                                Date cipherMailExpirationDate = expirationChecker.getKeyExpirationDate(key, key);

                                assertEquals("Expiration date mismatch for key with keyID " +
                                             keyIDHex, gnuPGExpirationDate, cipherMailExpirationDate);
                            }
                            else {
                                logger.warn("Key with keyID {} is not a master key", keyIDHex);
                            }
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        PGPKeyRingParser parser = new PGPKeyRingParser();

        parser.setKeyRingEventListener(keyRingEventListener);
        parser.parseKeyRing(new FileInputStream(new File(TEST_BASE, "sks-dump-0235.pgp")), null);

        // There are more keyrings but some keys rings cannot be read
        assertEquals(8565, publicKeyRingCount.intValue());
    }

    @Test
    public void testShouldNeverExpire()
    throws Exception
    {
        List<PGPPublicKey> keys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE,
                "D43C4D3C9AC70EBBE87330E9AA976E29616D42D4.asc"));

        PGPPublicKey masterKey = keys.get(0);

        assertEquals(2, keys.size());

        for (PGPPublicKey key : keys) {
            assertNull(expirationChecker.getKeyExpirationDate(key, masterKey));
        }
    }
}
