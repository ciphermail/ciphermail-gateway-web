/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ctl;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CTLDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        CTLDAO.deleteAllEntries(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private CTLDAO createDAO() {
        return createDAO("test");
    }

    private CTLDAO createDAO(String storeName) {
        return CTLDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    @Test
    public void testGetCTLs()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        Set<CTLEntity> entries;

        entries = new HashSet<>(dao.getCTLs(null, null));

        assertEquals(0, entries.size());

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-3"));

        entries = new HashSet<>(dao.getCTLs(null, null));
        assertEquals(3, entries.size());
        assertTrue(entries.contains(new CTLEntity("store", "fake-thumbprint-1")));
        assertTrue(entries.contains(new CTLEntity("store", "fake-thumbprint-2")));
        assertTrue(entries.contains(new CTLEntity("store", "fake-thumbprint-3")));

        entries = new HashSet<>(otherDAO.getCTLs(null, null));
        assertEquals(0, entries.size());

        entries = new HashSet<>(dao.getCTLs(1, null));
        assertEquals(2, entries.size());

        entries = new HashSet<>(dao.getCTLs(null, 1));
        assertEquals(1, entries.size());

        entries = new HashSet<>(dao.getCTLs(1, 1));
        assertEquals(1, entries.size());
    }

    @Test
    public void testGetAllCTLs()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        Set<CTLEntity> entries;

        entries = new HashSet<>(dao.getAllCTLs(null, null));

        assertEquals(0, entries.size());

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-3"));
        otherDAO.persist(dao.createCTLEntry("fake-thumbprint-4"));

        // getAllCTLs should return the CTLs for all stores
        entries = new HashSet<>(dao.getAllCTLs(null, null));
        assertEquals(4, entries.size());

        entries = new HashSet<>(otherDAO.getAllCTLs(null, null));
        assertEquals(4, entries.size());
    }

    @Test
    public void testGetSize()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-3"));

        assertEquals(3, dao.size());
        assertEquals(0, otherDAO.size());
    }

    @Test
    public void testGetCTL()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        assertNull(dao.getCTL("fake-thumbprint-1"));
        assertNull(otherDAO.getCTL("fake-thumbprint-1"));

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));

        assertNotNull(dao.getCTL("fake-thumbprint-1"));
        assertNull(otherDAO.getCTL("fake-thumbprint-1"));

        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));

        assertNotNull(dao.getCTL("fake-thumbprint-1"));
        assertNull(otherDAO.getCTL("fake-thumbprint-1"));
        assertNotNull(dao.getCTL("fake-thumbprint-2"));
        assertNull(otherDAO.getCTL("fake-thumbprint-2"));
    }

    @Test
    public void testDelete()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));
        otherDAO.persist(otherDAO.createCTLEntry("fake-thumbprint-3"));

        assertEquals(2, dao.size());
        assertEquals(1, otherDAO.size());

        dao.delete();

        assertEquals(0, dao.size());
        assertEquals(1, otherDAO.size());

        dao.delete();

        assertEquals(0, dao.size());
        assertEquals(1, otherDAO.size());

        otherDAO.delete();

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());
    }

    @Test
    public void testDeleteAll()
    {
        CTLDAO dao = createDAO("store");
        CTLDAO otherDAO = createDAO("other-store");

        dao.persist(dao.createCTLEntry("fake-thumbprint-1"));
        dao.persist(dao.createCTLEntry("fake-thumbprint-2"));
        otherDAO.persist(otherDAO.createCTLEntry("fake-thumbprint-3"));

        assertEquals(2, dao.size());
        assertEquals(1, otherDAO.size());

        CTLDAO.deleteAllEntries(dao.getSession());


        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());

        CTLDAO.deleteAllEntries(dao.getSession());

        assertEquals(0, dao.size());
        assertEquals(0, otherDAO.size());
    }
}
