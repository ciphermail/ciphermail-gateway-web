/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class BinaryContentWithTypeTest
{
    @Test
    public void testEqualsAndHashCode()
    {
        BinaryContentWithType binary1 = new BinaryContentWithType(new byte[]{1, 2}, "application/octet-stream");
        BinaryContentWithType binary2 = new BinaryContentWithType(new byte[]{1, 2}, "application/octet-stream");
        BinaryContentWithType binary3 = new BinaryContentWithType(new byte[]{1, 2, 3}, "application/octet-stream");
        BinaryContentWithType binary4 = new BinaryContentWithType(new byte[]{1, 2}, "application/somethingother");

        assertEquals(binary1, binary2);
        assertEquals(binary1.hashCode(), binary2.hashCode());
        assertNotEquals(binary1, binary3);
        assertNotEquals(binary1, binary4);
    }

    @Test
    public void testSetterGetters()
    {
        BinaryContentWithType binary = new BinaryContentWithType(new byte[]{1, 2}, "application/octet-stream");
        assertArrayEquals(new byte[]{1, 2}, binary.getContent());

        binary.setContent(new byte[]{3, 4});
        assertArrayEquals(new byte[]{3, 4}, binary.getContent());

        assertEquals("application/octet-stream", binary.getContentType());
        binary.setContentType("text/plain");
        assertEquals("text/plain", binary.getContentType());
    }

    @Test
    public void testClone()
    {
        BinaryContentWithType binary = new BinaryContentWithType();

        binary.setContentType("application/octet-stream");
        binary.setContent(new byte[]{1, 2});

        assertEquals(binary, binary.clone());
    }
}
