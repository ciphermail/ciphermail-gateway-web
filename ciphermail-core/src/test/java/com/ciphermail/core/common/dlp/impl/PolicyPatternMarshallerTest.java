/*
 * Copyright (c) 2010-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.cache.SimpleMemoryPatternCache;
import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.dlp.impl.matchfilter.MaskingFilter;
import com.ciphermail.core.test.TestProperties;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternMarshallerTest
{
    private static PolicyPatternMarshaller marshaller;

    static class DummyValidator implements Validator
    {
        private final String name;
        private final String description;

        DummyValidator(String name, String description)
        {
            this.name = name;
            this.description = description;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getDescription() {
            return description;
        }

        @Override
        public boolean isValid(String input) {
            return false;
        }
    }

    @BeforeClass
    public static void beforeClass()
    {
        MatchFilterRegistry matchFilterRegistry = new MatchFilterRegistryImpl(Collections.singletonList(
                new MaskingFilter()));

        ValidatorRegistry validatorRegistry = new ValidatorRegistryImpl();

        validatorRegistry.addValidator(new DummyValidator("name", "desc"));

        marshaller = new PolicyPatternMarshallerImpl(new SimpleMemoryPatternCache(), matchFilterRegistry,
                validatorRegistry);
    }

    @Test
    public void testMarshall()
    throws Exception
    {
        PolicyPatternImpl policyPattern = new PolicyPatternImpl();

        policyPattern.setName("test name");
        policyPattern.setDescription("test & pattern");
        policyPattern.setNotes("note");
        policyPattern.setMatchFilter(new MaskingFilter());
        policyPattern.setPattern(Pattern.compile(".*"));
        policyPattern.setThreshold(10);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        marshaller.marshall(policyPattern, bos);

        String json = bos.toString(StandardCharsets.US_ASCII);

        assertEquals("{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":null," +
                     "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":\"note\"," +
                     "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}", json);

        PolicyPatternImpl policyPattern2 = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                json.getBytes(StandardCharsets.US_ASCII)));

        bos = new ByteArrayOutputStream();

        marshaller.marshall(policyPattern2, bos);

        json = bos.toString(StandardCharsets.US_ASCII);

        assertEquals("{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":null," +
                     "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":\"note\"," +
                     "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}", json);
    }

    @Test
    public void testMarshallWithValidator()
    throws Exception
    {
        PolicyPatternImpl policyPattern = new PolicyPatternImpl();

        policyPattern.setName("test name");
        policyPattern.setDescription("test & pattern");
        policyPattern.setValidator(new DummyValidator("name", "desc"));
        policyPattern.setMatchFilter(new MaskingFilter());
        policyPattern.setPattern(Pattern.compile(".*"));
        policyPattern.setThreshold(10);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        marshaller.marshall(policyPattern, bos);

        String json = bos.toString(StandardCharsets.US_ASCII);

        assertEquals("{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":\"name\"," +
                     "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":null," +
                     "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}", json);
    }

    @Test
    public void testUnmarshall()
    throws Exception
    {
        String json = "{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":\"name\"," +
                     "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":null," +
                     "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}";

        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                json.getBytes(StandardCharsets.US_ASCII)));

        assertNotNull(policyPattern);
        assertEquals("test name", policyPattern.getName());
        assertEquals("test & pattern", policyPattern.getDescription());
        assertEquals("Mask", policyPattern.getMatchFilterName());
        assertEquals(".*", policyPattern.getRegExp());
        assertEquals(10, policyPattern.getThreshold());
        assertEquals(".*", policyPattern.getPattern().pattern());

        MatchFilter matchFilter = policyPattern.getMatchFilter();
        assertNotNull(matchFilter);
        assertEquals("Mask", matchFilter.getName());
    }

    @Test
    public void testUnmarshallWithValidator()
    throws Exception
    {
        String json = "{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":\"name\"," +
                      "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":null," +
                      "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}";

        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                json.getBytes(StandardCharsets.US_ASCII)));

        assertNotNull(policyPattern);
        assertEquals("test name", policyPattern.getName());
        assertEquals("test & pattern", policyPattern.getDescription());
        assertEquals("name", policyPattern.getValidatorName());
        assertEquals("Mask", policyPattern.getMatchFilterName());
        assertEquals(".*", policyPattern.getRegExp());
        assertEquals(10, policyPattern.getThreshold());
        assertEquals(".*", policyPattern.getPattern().pattern());

        Validator validator = policyPattern.getValidator();
        assertNotNull(validator);
        assertEquals("name", validator.getName());

        MatchFilter matchFilter = policyPattern.getMatchFilter();
        assertNotNull(matchFilter);
        assertEquals("Mask", matchFilter.getName());
    }

    @Test
    public void testUnmarshallWithValidatorNoMatchingName()
    throws Exception
    {
        String json = "{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":\"NOMATCH\"," +
                      "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":null," +
                      "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}";

        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                json.getBytes(StandardCharsets.US_ASCII)));

        assertNotNull(policyPattern);
        assertEquals("test name", policyPattern.getName());
        assertEquals("test & pattern", policyPattern.getDescription());
        assertEquals("NOMATCH", policyPattern.getValidatorName());
        assertEquals("Mask", policyPattern.getMatchFilterName());
        assertEquals(".*", policyPattern.getRegExp());
        assertEquals(10, policyPattern.getThreshold());
        assertEquals(".*", policyPattern.getPattern().pattern());

        Validator validator = policyPattern.getValidator();
        assertNull(validator);

        MatchFilter matchFilter = policyPattern.getMatchFilter();
        assertNotNull(matchFilter);
        assertEquals("Mask", matchFilter.getName());
    }

    @Test
    public void testUnmarshallNoRegExpNoMatchFilter()
    throws Exception
    {
        String json = "{\"name\":\"test name\",\"validatorName\":\"name\"," +
                      "\"description\":\"test & pattern\",\"notes\":null," +
                      "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}";

        PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                json.getBytes(StandardCharsets.US_ASCII)));

        assertNotNull(policyPattern);
        assertNull(policyPattern.getPattern());
        assertNull(policyPattern.getMatchFilter());
    }

    @Test
    public void testUnmarshallSpeedTest()
    throws Exception
    {
        String json = "{\"name\":\"test name\",\"regExp\":\".*\",\"validatorName\":\"name\"," +
                     "\"matchFilterName\":\"Mask\",\"description\":\"test & pattern\",\"notes\":null," +
                     "\"threshold\":10,\"action\":null,\"delayEvaluation\":false}";

        final int repeat = 5000;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            PolicyPatternImpl policyPattern = (PolicyPatternImpl) marshaller.unmarshall(new ByteArrayInputStream(
                    json.getBytes(StandardCharsets.US_ASCII)));

            assertNotNull(policyPattern);
        }

        double perSec = repeat * 1000.0 / (System.currentTimeMillis() - start);

        System.out.println("per/second: " + perSec);

        double expected = 2000 * TestProperties.getTestPerformanceFactor();

        assertTrue("can fail in slower systems.", perSec > expected);
    }
}
