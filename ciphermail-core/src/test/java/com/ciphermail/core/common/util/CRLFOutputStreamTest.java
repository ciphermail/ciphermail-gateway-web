package com.ciphermail.core.common.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CRLFOutputStreamTest
{
    @Test
    public void testLFToCRLF()
    throws IOException
    {
        InputStream input = new ByteArrayInputStream("test1\ntest2\n\ntest3".getBytes());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(input, new CRLFOutputStream(bos));

        assertEquals("test1\r\ntest2\r\n\r\ntest3", bos.toString());
    }

    @Test
    public void testCRToCRLF()
    throws IOException
    {
        InputStream input = new ByteArrayInputStream("test1\rtest2\r\rtest3".getBytes());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(input, new CRLFOutputStream(bos));

        assertEquals("test1\r\ntest2\r\n\r\ntest3", bos.toString());
    }

    @Test
    public void testMixedToCRLF()
    throws IOException
    {
        InputStream input = new ByteArrayInputStream("test1\rtest2\ntest3\r\n".getBytes());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        IOUtils.copy(input, new CRLFOutputStream(bos));

        assertEquals("test1\r\ntest2\r\ntest3\r\n", bos.toString());
    }

    @Test
    public void testClose()
    throws Exception
    {
        MutableBoolean closed = new MutableBoolean();

        OutputStream delegate = new OutputStream()
        {
            @Override
            public void close() {
                closed.setValue(true);
            }

            @Override
            public void write(int i) {
            }
        };

        CRLFOutputStream output = new CRLFOutputStream(delegate);

        assertFalse(closed.booleanValue());
        output.close();
        assertTrue(closed.booleanValue());
    }

    @Test
    public void testFlush()
    throws Exception
    {
        MutableBoolean flushed = new MutableBoolean();

        OutputStream delegate = new OutputStream()
        {
            @Override
            public void flush() {
                flushed.setValue(true);
            }

            @Override
            public void write(int i) {
            }
        };

        CRLFOutputStream output = new CRLFOutputStream(delegate);

        assertFalse(flushed.booleanValue());
        output.flush();
        assertTrue(flushed.booleanValue());
        output.close();
    }
}
