/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mime.FileExtensionResolver;
import com.ciphermail.core.common.mime.FileExtensionResolverImpl;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.security.PrivateKey;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PGPInlineMIMEBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static final FileExtensionResolver extensionResolver = new FileExtensionResolverImpl();

    private static final JcePBESecretKeyDecryptorBuilder secretKeyDecryptorBuilder =
            new JcePBESecretKeyDecryptorBuilder();

    private static final JcaPGPKeyConverter keyConverter = new JcaPGPKeyConverter();

    private static PrivateKey signerPrivateKey;
    private static PGPPublicKey signerPublicKey;

    private static List<PGPPublicKey> encryptionKeys;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                "pgp/test-multiple-sub-keys.gpg.key"));

        PGPSecretKey secretKey = secretKeys.get(0);

        signerPrivateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        signerPublicKey = secretKey.getPublicKey();

        encryptionKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "pgp/test@example.com.gpg.asc"));
    }

    private PGPInlineMIMEBuilder createPGPInlineMIMEBuilder() {
        return new PGPInlineMIMEBuilder(extensionResolver);
    }

    private boolean verify(MimeMessage signedMessage)
    throws Exception
    {
        // Clone message to make sure it has been written
        signedMessage = MailUtils.cloneMessage(signedMessage);

        boolean verified = false;

        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TEST_BASE, "pgp/test-multiple-sub-keys.gpg.asc"),
                new File(TEST_BASE, "pgp/test@example.com.gpg.key"), "test");

        MimeMessage handled = handler.handleMessage(signedMessage);

        if (handled != null) {
            verified = "True".equals(handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        }

        return verified;
    }

    @Test
    public void testPGPInlineSignTextOnly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/simple-text-message.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignAndEncryptTextOnly()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/simple-text-message.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignAlternativeWithAttachment()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/normal-message-with-attach.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignAndEncryptAlternativeWithAttachment()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/normal-message-with-attach.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignHTMLWithEmbeddedImages()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-embedded-images.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(false);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignEncryptHTMLWithEmbeddedImages()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-embedded-images.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignHTMLAlternative()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/test-simple-text&HTML-alternative.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(false);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignHTMLAlternativeHTMLFirst()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/test-simple-text&HTML-alternative-HTML-first.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(false);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignEncryptHTMLAlternativeHTMLFirst()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/test-simple-text&HTML-alternative-HTML-first.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignEncryptUnicodeUTF8()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/unicode.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignUnicodeUTF8()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/unicode.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(false);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignEncryptISO_8859_9()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/8859-9.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(true);
        builder.setEncryptionKeys(encryptionKeys);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }

    @Test
    public void testPGPInlineSignMultipartMultipleCRLFEndings()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-alternative-ends-with-mulitple-crlf.eml"));

        PGPInlineMIMEBuilder builder = createPGPInlineMIMEBuilder();

        builder.setSign(true);
        builder.setSigner(signerPublicKey, signerPrivateKey);
        builder.setEncrypt(false);

        MimeMessage signed = builder.build(message);

        MailUtils.validateMessage(signed);

        assertTrue(verify(signed));
    }
}
