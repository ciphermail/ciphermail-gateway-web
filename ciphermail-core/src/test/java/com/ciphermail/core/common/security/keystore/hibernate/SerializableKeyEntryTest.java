/*
 * Copyright (c) 2010-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.hibernate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.openpgp.PGPSecurityFactoryFactory;
import com.ciphermail.core.common.security.password.PBEncryption;
import com.ciphermail.core.common.security.password.PBEncryptionImpl;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


public class SerializableKeyEntryTest
{
    private static final String ENCODED_PUBLIC_KEY =
        "30820122300d06092a864886f70d01010105000382010f003082010a0282" +
        "010100b7c0b5ac2250390fe9da9b800bdb0a1b7f21614d1e7c5db67438a9" +
        "d1f793c0cf0403a3523981f17885e145dbc0e4c927a184c0246e7a79ae29" +
        "f7ad4e1a36db1db567c793547b96a8275e93d37ad17006ca516f3ed4bfbf" +
        "f9883e2a351d8c5c76a24502344e8711e76843b3ba05f7a5515287dba46b" +
        "05033c7756b531512034845fc7ae350ebc720ed83e7df7ebd8ade16b0d96" +
        "1c15277fd5802b724c3d9392a7e99d2d06bf55b5d2516152b1d9c249561e" +
        "3887f075e2bebe8e331cdc3fe5de60af36285e514b14163b34af15e6bb48" +
        "e6b232366f05d73ad261714854833305ba06f1430d5b0c83837c49c9408c" +
        "8e4d674b9a9494a6b0dc66e0b4d2107d78f26f0203010001";

    private static final String SERIALIZED_BASE64 =
        """
        eyJyYXdLZXkiOiJsa2xmeDZTV3hJa0FBQUFRYVVwZVErU0lkb2FKdDl4RzUwTTB0UUFBQ0FBQUFB
        RXdDWmhrSld0eFJPZHJsalY3bWFXZkNiOUZZMFJoQ3Z5TzdyNEx3dHVFVHdYdzZFR1k3cVVnT1Ns
        Vis2NklvM0pOYmVwOElrWVJwUE5lRXQ0ak5EUEV0S2xCUDRLWVBXRkJpV3VlMGVLM0JFZVpaTjZW
        MWFYQm5vN2g3OUtlV1d3UVlSeHlHU21POUZMeG1GbFBKaGRlQXBCSE95YlprRWVLT0JOM09NL3d6
        ZVhDM1ByM0RmUnJ0Z1E0TWhDWHRQQlE5dEV5Q2IzRnkyYnNvckdpZFFZM2JlZmtnMlBmNFJySEtG
        SDJSRkFuTUd1MjQ0VXNjN2JTUEZkT0Zpdm1UOEcyNE9BeHlIZy9iYU1QSWorOWdUR1NIdEF4WmQ5
        MHR5L0lwcmpNWUdEdzZBdXZDYjJvOFF1MFE3dzVlNFlYUXAxM05HbnZadnJTMERIZWxqMlNFSW1E
        Rit3V2dia09CSHRrNTJQQ1hxa2tSbUliNnV4Vk1HaDV0NUdwTDQ0UmlwL2RzVFo2OGptV2piZk84
        bVpRTDgvQk1BPT0iLCJhbGdvcml0aG0iOiJSU0EiLCJmb3JtYXQiOiJYLjUwOSIsImtleVR5cGUi
        OiJQVUJMSUMiLCJwcm90ZWN0aW9uIjoiRU5DUllQVEVEIn0=
        """;

    private static final String PASSWORD = "test";

    private static KeyFactory keyFactory;
    private static PBEncryption encryptor;
    private static PublicKey publicKey;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        keyFactory = SecurityFactoryFactory.getSecurityFactory().createKeyFactory("RSA");

        publicKey = decodePublicKey(ENCODED_PUBLIC_KEY);

        encryptor = new PBEncryptionImpl();
    }

    private static PublicKey decodePublicKey(String encoded)
    throws Exception
    {
        return keyFactory.generatePublic(new X509EncodedKeySpec(Hex.decodeHex(encoded.toCharArray())));
    }

    private KeyPair generateKeyPair()
    throws Exception
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        SecureRandom randomSource = securityFactory.createSecureRandom();

        KeyPairGenerator keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");

        keyPairGenerator.initialize(2048, randomSource);

        return keyPairGenerator.generateKeyPair();
    }

    @Test
    public void testSerializeKey()
    throws Exception
    {
        SerializableKeyEntry serializer = new SerializableKeyEntry(publicKey, PASSWORD.toCharArray(), encryptor);

        byte[] serialized = serializer.serialize();

        assertNotNull(serialized);

        SerializableKeyEntry deSerialized = SerializableKeyEntry.deserialize(serialized, null);

        Key key = deSerialized.getKey(PASSWORD.toCharArray(), encryptor);

        assertEquals(publicKey, key);
    }

    @Test
    public void testSerializePrivateKey()
    throws Exception
    {
        PrivateKey privateKey = generateKeyPair().getPrivate();

        SerializableKeyEntry serializer = SerializableKeyEntry.createInstance(privateKey, PASSWORD.toCharArray(),
                encryptor);

        byte[] serialized = serializer.serialize();

        assertNotNull(serialized);

        SerializableKeyEntry deSerialized = SerializableKeyEntry.deserialize(serialized, null);

        Key key = deSerialized.getKey(PASSWORD.toCharArray(), encryptor);

        assertEquals(privateKey, key);
    }

    @Test
    public void testSerializeKeyWithSecurityFactory()
    throws Exception
    {
        SerializableKeyEntry serializer = SerializableKeyEntry.createInstance(publicKey, PASSWORD.toCharArray(),
                encryptor);

        serializer.setSecurityFactory(PGPSecurityFactoryFactory.getSecurityFactory());

        byte[] serialized = serializer.serialize();

        assertNotNull(serialized);

        SerializableKeyEntry deserialized = SerializableKeyEntry.deserialize(serialized, null);

        assertNull(deserialized.getSecurityFactory());
    }

    @Test
    public void testDeSerializeKey()
    throws Exception
    {
        byte[] serialized = Base64.decodeBase64(MiscStringUtils.getBytesASCII(SERIALIZED_BASE64));

        SerializableKeyEntry deSerialized = SerializableKeyEntry.deserialize(serialized, null);

        Key key = deSerialized.getKey(PASSWORD.toCharArray(), encryptor);

        assertEquals(publicKey, key);
    }
}
