/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.clickatellconnect;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.test.TestProperties;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ClickatellConnectSMSTransportTest
{
    private ClickatellConnectSMSTransport createClickatellSMSTransport(
            StaticClickatellConnectPropertiesProvider parameterProvider,
            HTTPClientProxyProvider proxyProvider, String username, String password)
    {
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        credentialsProvider.setCredentials(new AuthScope(null, -1),
                new UsernamePasswordCredentials(username, password.toCharArray()));

        HttpClientContextFactoryImpl httpClientContextFactory = new HttpClientContextFactoryImpl(
                credentialsProvider);

        ClickatellConnectSMSTransport transport = new ClickatellConnectSMSTransport(
                new CloseableHttpAsyncClientFactoryImpl(proxyProvider),
                httpClientContextFactory,
                parameterProvider);

        transport.setSendAPIURL((proxyProvider != null ?
                TestProperties.getWiremockProxyURL() : TestProperties.getWiremockURL()) +
                "/sms/clickatell/connect/messages/http/send");

        return transport;
    }

    private ClickatellConnectSMSTransport createClickatellSMSTransport(
            StaticClickatellConnectPropertiesProvider parameterProvider,
            HTTPClientProxyProvider proxyProvider)
    {
        return createClickatellSMSTransport(parameterProvider, proxyProvider, "test", "test");
    }

    private ClickatellConnectSMSTransport createClickatellSMSTransport(
            StaticClickatellConnectPropertiesProvider propertiesProvider)
    {
        return createClickatellSMSTransport(propertiesProvider, null);
    }

    @Before
    public void before()
    {
        // Clear interrupted status
        Thread.interrupted();
    }

    @Test
    public void testSendSMS()
    throws IOException
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("test");

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        try {
            transport.sendSMS("12345678", "test");
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testInvalidKeyid()
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("invalid");

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e)
        {
            assertEquals("Error sending SMS. Status: 401, Error Code: 108, Error: Invalid or missing integration "
                    + "API Key., Error description: The integration API key is either incorrect or has not "
                    + "been included in the API call.", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testUnknownJSON()
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("unknownjson");

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 401, Reason: Unauthorized", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testNoJSON()
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("invalidjson");

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 401, Reason: Unauthorized", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendSMSViaProxy()
    throws IOException, URISyntaxException
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("test");

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider, proxyProvider);

        try {
            transport.sendSMS("12345678", "test");
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendSMSViaProxyInvalidPassword()
    throws URISyntaxException
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("test");

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider, proxyProvider,
                "test", "invalid");

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 407, Reason: Proxy Authentication Required", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendSMSMultithreaded()
    throws Exception
    {
        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("test");

        int nrOfMessages = 50000;

        final AtomicInteger msgCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfMessages);

        final Thread mainThread = Thread.currentThread();

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfMessages; i++)
            {
                executorService.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try {
                            System.out.println("SMS: " + msgCounter.incrementAndGet() + ". Thread: " +
                                    Thread.currentThread().getName());

                            transport.sendSMS("12345678", "test");
                        }
                        catch (IOException  e) {
                            // Stop main thread from waiting
                            mainThread.interrupt();

                            throw new UnhandledException(e);
                        }
                        finally {
                            countDownLatch.countDown();
                        }
                    }
                });
            }

            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
            assertEquals(nrOfMessages, msgCounter.get());
        }
        finally {
            executorService.shutdown();
            transport.shutdown();
        }
    }

    @Test
    public void testTimeout()
    throws Exception
    {
        String localIP = "127.1.0.6";

        StaticClickatellConnectPropertiesProvider propertiesProvider = new StaticClickatellConnectPropertiesProvider();

        propertiesProvider.setAPIKey("test");

        ClickatellConnectSMSTransport transport = createClickatellSMSTransport(propertiesProvider);

        try {
            ServerSocket serverSocket = new ServerSocket();

            // Create a socket which does nothing on 127.0.0.4 for testing timeouts
            serverSocket.bind(new InetSocketAddress(localIP, 0));

            transport.setSendAPIURL("http://" + localIP + ":" + serverSocket.getLocalPort());

            transport.setTotalTimeout(2000);

            try {
                transport.sendSMS("12345678", "test");

                fail();
            }
            catch (IOException e)
            {
                assertEquals("A timeout has occurred connecting to: http://127.1.0.6:" + serverSocket.getLocalPort(),
                        e.getMessage());
            }
            finally {
                serverSocket.close();
            }
        }
        finally {
            transport.shutdown();
        }
    }
}
