/*
 * Copyright (c) 2014-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PGPPublicKeyRingExtractorTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    static class PGPPublicKeyRingListenerImpl implements PGPPublicKeyRingListener
    {
        List<PGPPublicKeyRing> keyRings = new LinkedList<>();

        @Override
        public void onPGPPublicKeyRing(PGPPublicKeyRing keyRing) {
            keyRings.add(keyRing);
        }
    }

    @Test
    public void testExtractKeysNoRemove()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-attached-key.eml"));

        PGPPublicKeyRingListenerImpl listener = new PGPPublicKeyRingListenerImpl();

        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(listener);

        assertFalse(extractor.extract(message));

        assertEquals(1, listener.keyRings.size());

        MailUtils.validateMessage(message);

        Multipart mp = (Multipart) message.getContent();

        assertEquals(3, mp.getCount());

        BodyPart part = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", part.getContentType());
        assertTrue(((String)part.getContent()).contains("DJIGZO email encryption"));

        part = mp.getBodyPart(1);

        assertEquals("application/pgp-keys;\r\n name=\"0x271AD23B.asc\"", part.getContentType());
    }

    @Test
    public void testExtractKeysRemove()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-attached-key.eml"));

        PGPPublicKeyRingListenerImpl listener = new PGPPublicKeyRingListenerImpl();

        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(listener);

        extractor.setRemoveKeys(true);

        assertTrue(extractor.extract(message));

        assertEquals(1, listener.keyRings.size());

        MailUtils.validateMessage(message);

        Multipart mp = (Multipart) message.getContent();

        assertEquals(3, mp.getCount());

        BodyPart part = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", part.getContentType());
        assertTrue(((String)part.getContent()).contains("DJIGZO email encryption"));

        part = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());
        assertEquals("*** Attached PGP keys have been removed ***", part.getContent());
    }

    @Test
    public void testExtractKeysRemoveSetReplacementText()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-attached-key.eml"));

        PGPPublicKeyRingListenerImpl listener = new PGPPublicKeyRingListenerImpl();

        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(listener);

        extractor.setRemoveKeys(true);
        extractor.setReplacementText("aap noot mies");

        assertTrue(extractor.extract(message));

        assertEquals(1, listener.keyRings.size());

        MailUtils.validateMessage(message);

        Multipart mp = (Multipart) message.getContent();

        assertEquals(3, mp.getCount());

        BodyPart part = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", part.getContentType());
        assertTrue(((String)part.getContent()).contains("DJIGZO email encryption"));

        part = mp.getBodyPart(1);

        assertEquals("text/plain; charset=us-ascii", part.getContentType());
        assertEquals("aap noot mies", part.getContent());
    }

    @Test
    public void testExtractKeysNoKeys()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-signed.eml"));

        PGPPublicKeyRingListenerImpl listener = new PGPPublicKeyRingListenerImpl();

        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(listener);

        extractor.setRemoveKeys(true);

        assertFalse(extractor.extract(message));

        assertEquals(0, listener.keyRings.size());
    }

    @Test
    public void testExtractKeysMaxSizeExceed()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/PGP-inline-signed-attached-key.eml"));

        PGPPublicKeyRingListenerImpl listener = new PGPPublicKeyRingListenerImpl();

        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(listener);

        extractor.setMaxKeySize(10);

        assertFalse(extractor.extract(message));

        assertEquals(0, listener.keyRings.size());
    }
}
