/*
 * Copyright (c) 2010-2020, CipherMail B,V,.
 *
 * This file is part of CipherMail.
 */
package com.ciphermail.core.common.extractor.impl;

import com.ciphermail.core.common.extractor.MimeTypeDetector;
import com.ciphermail.core.common.util.RewindableInputStream;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

class HeuristicMimeTypeDetectorTest
{
    private static MimeTypeDetector detector;

    private static final List<RewindableInputStream> toClose = new LinkedList<RewindableInputStream>();

    private static RewindableInputStream readDocument(String filename)
    throws FileNotFoundException
    {
        RewindableInputStream input = new RewindableInputStream(
                new FileInputStream(new File(TestUtils.getTestDataDir(), filename)), SizeUtils.MB);

        toClose.add(input);

        return input;
    }

    @BeforeAll
    public static void beforeClass() {
        detector = new HeuristicMimeTypeDetector();
    }

    @AfterEach
    public void after()
    {
        for (RewindableInputStream input : toClose) {
            IOUtils.closeQuietly(input);
        }

        toClose.clear();
    }

    @Test
    void testPDF()
    throws IOException
    {
        RewindableInputStream input = readDocument("documents/djigzo-whitepaper.pdf");

        Assertions.assertEquals("application/pdf", detector.detectMimeType(input, null).toString());
    }

    @Test
    void testHTML()
    throws IOException
    {
        RewindableInputStream input = readDocument("documents/big-preamble.html");

        Assertions.assertEquals("text/html", detector.detectMimeType(input, null).toString());
    }

    @Test
    void testPKCS12()
    throws IOException
    {
        RewindableInputStream input = readDocument("keys/testCertificates.p12");

        Assertions.assertEquals("application/x-x509-key", detector.detectMimeType(input, null).toString());
    }
}
