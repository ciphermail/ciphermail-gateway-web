/*
 * Copyright (c) 2012-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.ciphermail.core.common.util.KeyedBarrier.KeyedBarrierTimeoutException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class KeyedBarrierTest
{
    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    private static class SleepingCallable implements Callable<Object>
    {
        private final long sleepTime;

        private long executionTime;

        SleepingCallable(long sleepTime) {
            this.sleepTime = sleepTime;
        }

        @Override
        public Object call()
        {
            executionTime = System.currentTimeMillis();

            ThreadUtils.sleepQuietly(sleepTime);

            return executionTime;
        }
    }

    private static Future<?> execute(final KeyedBarrier<String, Object> barrier, final String key,
            final Callable<Object> callable, final long timeout)
    {
        return executorService.submit(new Callable<Object>() {
            @Override
            public Object call()
            throws Exception
            {
                return barrier.execute(key, callable, timeout);
            }
        });
    }

    private static Future<?> execute(final KeyedBarrier<String, Object> barrier, final String key,
            final Callable<Object> callable)
    {
        return execute(barrier, key, callable, DateUtils.MILLIS_PER_SECOND * 60);
    }

    @Test
    public void testConcurrentAccessSameKey()
    throws Exception
    {
        final KeyedBarrier<String, Object> barrier = new KeyedBarrier<>();

        final long waitTime = 1000;

        final SleepingCallable callable1 = new SleepingCallable(waitTime);
        final SleepingCallable callable2 = new SleepingCallable(waitTime);

        Future<?> future1 = execute(barrier, "key", callable1);

        Future<?> future2 = execute(barrier, "key", callable2);

        future1.get();
        future2.get();

        /*
         * it can happen that runnable 2 has run before runnable 1 but the difference in time
         * should be more than waitTime
         */
        assertTrue(Math.abs(callable2.executionTime - callable1.executionTime) >= waitTime);
    }

    @Test
    public void testConcurrentAccessDifferentKey()
    throws Exception
    {
        final KeyedBarrier<String, Object> barrier = new KeyedBarrier<>();

        final long waitTime = 1000;

        final SleepingCallable callable1 = new SleepingCallable(waitTime);
        final SleepingCallable callable2 = new SleepingCallable(waitTime);

        Future<?> future1 = execute(barrier, "key", callable1);

        Future<?> future2 = execute(barrier, "otherkey", callable2);

        future1.get();
        future2.get();

        /*
         * it can happen that runnable 2 has run before runnable 1 but the difference in time
         * should not be more than 1 couple of milliseconds
         */
        assertTrue(Math.abs(callable2.executionTime - callable1.executionTime) < 100);
    }

    @Test
    public void testTimeout()
    throws Exception
    {
        final long timeout = 100;

        final KeyedBarrier<String, Object> barrier = new KeyedBarrier<>(10);

        final long waitTime = 1000;

        final SleepingCallable callable1 = new SleepingCallable(waitTime);
        final SleepingCallable callable2 = new SleepingCallable(waitTime);

        try {
            Future<?> future1 = execute(barrier, "key", callable1, timeout);

            Future<?> future2 = execute(barrier, "key", callable2, timeout);

            future1.get();
            future2.get();

            fail();
        }
        catch (ExecutionException e) {
            assertTrue(e.getCause() instanceof KeyedBarrierTimeoutException);
        }
    }
}
