/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.cms.KeyTransRecipientIdImpl;
import com.ciphermail.core.common.security.keystore.KeyStoreProvider;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class KeyAndCertStoreImplTest
{
    private static final File TEST_BASE_DIR = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_STORE_PROVIDER_SERVICE_NAME)
    private KeyStoreProvider keyStoreProvider;

    private static KeyStore testKeysKeyStore;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        testKeysKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        testKeysKeyStore.load(new FileInputStream(new File(TEST_BASE_DIR, "keys/testCA.p12")),
                "test".toCharArray());
    }

    @Before
    public void setup() {
        removeAllKeys();
    }

    private void removeAllKeys()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyAndCertStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyAndCertStore.addKeyAndCertificate(keyAndCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getKeyAndCertStoreSize()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyAndCertStore.size();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    private boolean keyStoreContainsAlias(String alias)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            try {
                return keyStoreProvider.getKeyStore().containsAlias(alias);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    private int getKeyStoreSize()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyStoreProvider.getKeyStore().size();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).intValue();
    }

    private void importkeyStore(KeyStore keyStore)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Enumeration<String> aliases = keyStore.aliases();

                while (aliases.hasMoreElements())
                {
                    String alias = aliases.nextElement();

                    X509Certificate certificate = (X509Certificate) keyStore.getCertificate(alias);

                    PrivateKey key = (PrivateKey) keyStore.getKey(alias, null);

                    KeyAndCertificate keyAndCertificate = new KeyAndCertificateImpl(key, certificate);

                    addKeyAndCertificate(keyAndCertificate);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private KeyAndCertificate getKeyAndCertificate(String thumbprint)
    {
        return transactionOperations.execute(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = null;

                X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);

                if (entry != null) {
                    keyAndCertificate = keyAndCertStore.getKeyAndCertificate(entry);
                }

                return keyAndCertificate;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void removeCertificate(String thumbprint)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                X509CertStoreEntry entry = keyAndCertStore.getByThumbprint(thumbprint);

                if (entry != null) {
                    keyAndCertStore.removeCertificate(entry.getCertificate());
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyAndCertStore.getMatchingKeys(keyIdentifier);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Collection<? extends PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyAndCertStore.getMatchingKeys(keyIdentifier, firstResult, maxResults);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }


    @Test
    public void testAddKeyAndCertificate()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        assertEquals(2, getKeyAndCertStoreSize());

        String thumbprint = "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" +
            "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";

        KeyAndCertificate keyAndCertificate = getKeyAndCertificate(thumbprint);

        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());

        /*
         * test if importing the same certificate again but now with a null key does not
         * remove the existing key alias
         */
        keyAndCertificate = new KeyAndCertificateImpl(null, keyAndCertificate.getCertificate());

        addKeyAndCertificate(keyAndCertificate);

        assertEquals(2, getKeyAndCertStoreSize());

        keyAndCertificate = getKeyAndCertificate(thumbprint);

        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());
    }

    @Test
    public void testRemoveCertificate()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        assertEquals(2, getKeyAndCertStoreSize());

        String thumbprint = "D6C5DD8B1D2DEEC273E7311924B20F81F9128D32B21C73E163DF14844F3C" +
            "9CA73767EBE9A9DF725046DB731D20BC277559A58B8B8E91F02C9BBC5E3D1C6C7A4D";

        KeyAndCertificate keyAndCertificate = getKeyAndCertificate(thumbprint);

        assertNotNull(keyAndCertificate);
        assertNotNull(keyAndCertificate.getCertificate());
        assertNotNull(keyAndCertificate.getPrivateKey());

        assertTrue(keyStoreContainsAlias(thumbprint));

        removeCertificate(thumbprint);

        keyAndCertificate = getKeyAndCertificate(thumbprint);

        assertNull(keyAndCertificate);

        assertFalse(keyStoreContainsAlias(thumbprint));
    }

    @Test
    public void testRemoveAll()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        assertEquals(2, getKeyAndCertStoreSize());
        assertEquals(2, getKeyStoreSize());

        removeAllKeys();

        assertEquals(0, getKeyAndCertStoreSize());
        assertEquals(0, getKeyStoreSize());
    }

    @Test
    public void testGetMatchingKeys()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                new BigInteger("115FCAD6B536FD8D49E72922CD1F0DA", 16),
                null);

        Collection<? extends PrivateKey> keys = getMatchingKeys(recipientInfo);

        assertEquals(1, keys.size());
    }

    @Test
    public void testGetMatchingKeysMultiple()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);

        Collection<? extends PrivateKey> keys = getMatchingKeys(recipientInfo);

        assertEquals(2, keys.size());
    }

    @Test
    public void testGetMatchingKeysMultipleMaxSize()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);

        Collection<? extends PrivateKey> keys = getMatchingKeys(recipientInfo, 0, 1);

        assertEquals(1, keys.size());
    }

    @Test
    public void testGetMatchingKeysMultipleOutOfRange()
    throws Exception
    {
        importkeyStore(testKeysKeyStore);

        KeyTransRecipientIdImpl recipientInfo = new KeyTransRecipientIdImpl(
                new X500Principal("EMAILADDRESS=root@example.com, CN=MITM Test Root, L=Amsterdam, ST=NH, C=NL"),
                null, null, false /* non strict */);

        Collection<? extends PrivateKey> keys = getMatchingKeys(recipientInfo, 10, 100);

        assertEquals(0, keys.size());
    }
}
