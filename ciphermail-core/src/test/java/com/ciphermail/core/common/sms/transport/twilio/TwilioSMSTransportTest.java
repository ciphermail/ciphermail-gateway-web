/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail.
 */
package com.ciphermail.core.common.sms.transport.twilio;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.test.TestProperties;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.URI;
import java.time.Instant;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TwilioSMSTransportTest
{
    private TwilioSMSTransport createTwilioSMSTransport(
            StaticTwilioPropertiesProvider propertiesProvider,
            HTTPClientProxyProvider proxyProvider,
            String username,
            String password)
    {
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        if (username != null) {
            credentialsProvider.setCredentials(new AuthScope(null, -1),
                    new UsernamePasswordCredentials(username, password.toCharArray()));
        }

        HttpClientContextFactoryImpl httpClientContextFactory = new HttpClientContextFactoryImpl(
                credentialsProvider);

        TwilioSMSTransport transport = new TwilioSMSTransport(
                new CloseableHttpAsyncClientFactoryImpl(proxyProvider),
                httpClientContextFactory,
                propertiesProvider);

        transport.setUrl((proxyProvider != null ?
                TestProperties.getWiremockProxyURL() : TestProperties.getWiremockURL()) +
                "/sms/api.twilio.com/2010-04-01/Accounts/%s/Messages.json");

        return transport;
    }

    private TwilioSMSTransport createTwilioSMSTransport(StaticTwilioPropertiesProvider propertiesProvider) {
        return createTwilioSMSTransport(propertiesProvider, null, null, null);
    }

    @Before
    public void before()
    {
        // Clear interrupted status
        Thread.interrupted();
    }

    @Test
    public void testSendSMS()
    throws Exception
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("APISECRETKEY");

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider);

        transport.sendSMS("12345678", "test");
    }

    @Test
    public void testSendSMSInvalidCredentials()
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("INVALID");

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e)
        {
            assertEquals("Error sending SMS. Status: 401, Error Code: 20003, Error message: Authentication Error - "
                    + "invalid username", e.getMessage());
        }
    }

    @Test
    public void testSendSMSViaProxy()
    throws Exception
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("APISECRETKEY");

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider, proxyProvider, "test",
                "test");

        transport.sendSMS("12345678", "test");
    }

    @Test
    public void testSendSMSViaProxyInvalidPassword()
    throws Exception
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("APISECRETKEY");

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider, proxyProvider, "test",
                "invalid");

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 407, Reason: Proxy Authentication Required", e.getMessage());
        }
    }

    @Test
    public void testSendSMSMultithreaded()
    throws Exception
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("APISECRETKEY");

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider);

        int nrOfMessages = 25000;

        final AtomicInteger msgCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfMessages);

        final Thread mainThread = Thread.currentThread();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfMessages; i++)
            {
                executorService.execute(() ->
                {
                    try {
                        System.out.println(Instant.now() + " SMS: " + msgCounter.incrementAndGet() +
                                ". Thread: " + Thread.currentThread().getName());

                        transport.sendSMS("12345678", "test");
                    }
                    catch (IOException  e) {
                        // Stop main thread from waiting
                        mainThread.interrupt();

                        throw new UnhandledException(e);
                    }
                    finally {
                        countDownLatch.countDown();
                    }
                });
            }

            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
            assertEquals(nrOfMessages, msgCounter.get());
        }
        finally {
            executorService.shutdown();
        }
    }

    @Test
    public void testTimeout()
    throws Exception
    {
        String localIP = "127.1.0.4";

        ServerSocket serverSocket = new ServerSocket();

        // Create a socket which does nothing on 127.0.0.4 for testing timeouts
        serverSocket.bind(new InetSocketAddress(localIP, 0));

        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("valid-account-sid");
        propertiesProvider.setFrom("11111111");
        propertiesProvider.setApiKeySid("APIKEYSID");
        propertiesProvider.setApiKeySecret("APISECRETKEY");

        TwilioSMSTransport transport = createTwilioSMSTransport(propertiesProvider);

        transport.setUrl("http://" + localIP + ":" + serverSocket.getLocalPort());

        transport.setTotalTimeout(2000);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e)
        {
            assertEquals("A timeout has occurred connecting to: http://127.1.0.4:" + serverSocket.getLocalPort(),
                    e.getMessage());
        }
        finally {
            serverSocket.close();
        }
    }
}
