/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mime;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileExtensionResolverImplTest
{
    @Test
    public void getExtensionFromContentType()
    {
        FileExtensionResolver resolver = new FileExtensionResolverImpl();

        assertEquals("xls", resolver.getExtensionFromContentType("application/vnd.ms-excel"));
        assertEquals("ppt", resolver.getExtensionFromContentType("application/vnd.ms-powerpoint"));
        assertEquals("txt", resolver.getExtensionFromContentType("application/json"));
        assertEquals("txt", resolver.getExtensionFromContentType("text/plain"));
        assertEquals("xml", resolver.getExtensionFromContentType("application/xhtml+xml"));
        assertEquals("xml", resolver.getExtensionFromContentType("application/rdf+xml"));
        assertEquals("bin", resolver.getExtensionFromContentType("application/pkcs7-mime"));
        assertEquals("bin", resolver.getExtensionFromContentType("application/octet-stream"));
        assertEquals("jpg", resolver.getExtensionFromContentType("image/jpeg"));
        assertEquals("jpg", resolver.getExtensionFromContentType("   image/jpeg   "));
        assertEquals("zip", resolver.getExtensionFromContentType("applicatION/ZIP"));
        assertEquals("bin", resolver.getExtensionFromContentType("application/something-non-existing"));
        assertEquals("bin", resolver.getExtensionFromContentType("invalid"));
        assertEquals("bin", resolver.getExtensionFromContentType("invalid"));
        assertEquals("bin", resolver.getExtensionFromContentType(null));
        assertEquals("doc", resolver.getExtensionFromContentType("application/msword"));
    }

    @Test
    public void getExtensionFromContentTypeAlias()
    {
        FileExtensionResolver resolver = new FileExtensionResolverImpl();

        assertEquals("doc", resolver.getExtensionFromContentType("application/vnd.ms-word"));
        assertEquals("pdf", resolver.getExtensionFromContentType("application/x-pdf"));
    }

    @Test
    public void getExtensionFromContentTypeAddMapping()
    {
        FileExtensionResolverImpl resolver = new FileExtensionResolverImpl();

        assertEquals("bin", resolver.getExtensionFromContentType("application/something-non-existing"));

        resolver.addMapping("APPLICATION/SOMETHING-NON-EXISTING", "abc");

        assertEquals("abc", resolver.getExtensionFromContentType("application/something-non-existing"));
    }
}
