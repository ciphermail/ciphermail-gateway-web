/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

public class HTMLLinkifyTest
{
    @Test
    public void testLinkyfy()
    {
        assertEquals("aa <a href=\"https://www.youtube.com/watch?v=IPYeCltXpxw\">https://www.youtube.com/watch?v=IPYeCltXpxw</a> bb",
                HTMLLinkify.linkify("aa https://www.youtube.com/watch?v=IPYeCltXpxw](https://www.youtube.com/watch?v=IPYeCltXpxw) bb"));

        assertEquals("The URL: <a href=\"www.ciphermail.com\">www.ciphermail.com</a> test", HTMLLinkify.linkify("The URL: www.ciphermail.com test"));
        assertEquals("The URL: <a href=\"www.ciphermail.com\">www.ciphermail.com</a>. test", HTMLLinkify.linkify("The URL: www.ciphermail.com. test"));

        assertEquals("<a href=\"http://www.ciphermail.com\">http://www.ciphermail.com</a>",
                HTMLLinkify.linkify("http://www.ciphermail.com"));

        assertEquals("  123 <a href=\"http://www.ciphermail.com\">http://www.ciphermail.com</a> 456 ",
                HTMLLinkify.linkify("  123 http://www.ciphermail.com 456 "));

        assertEquals("link 1: <a href=\"http://www.ciphermail.com\">http://www.ciphermail.com</a> link 2: " +
                "<a href=\"http://foo.com/(something)?after=parens\">http://foo.com/(something)?after=parens</a> rest",
                HTMLLinkify.linkify("link 1: http://www.ciphermail.com link 2: http://foo.com/(something)?after=parens rest"));

        assertEquals("<a href=\"http://www.ciphermail.com\">http://www.ciphermail.com</a>.",
                HTMLLinkify.linkify("http://www.ciphermail.com."));

        assertEquals("<a href=\"http://www.ciphermail.com\">http://www.ciphermail.com</a>...",
                HTMLLinkify.linkify("http://www.ciphermail.com..."));

        assertEquals("<a href=\"http://www.ciphermail.com/a-z0-9-_~:/?#@!$&'()*+,;=.[]\">http://www.ciphermail.com/a-z0-9-_~:/?#@!$&'()*+,;=.[]</a>",
                HTMLLinkify.linkify("http://www.ciphermail.com/a-z0-9-_~:/?#@!$&'()*+,;=.[]"));
    }

    @Test
    public void testLinkyfyInvalidLinks()
    {
        String input = "www.ciphermail.com----";

        assertEquals(input, HTMLLinkify.linkify(input));

        input = "http://ciphermail.com----";

        assertEquals(input, HTMLLinkify.linkify(input));

        input = "httpx://ciphermail.com";

        assertEquals(input, HTMLLinkify.linkify(input));
    }

    @Test
    public void testLinkyfyNoLinks()
    {
        String input = "bla\nbla";

        assertEquals(input, HTMLLinkify.linkify(input));

        input = "";

        assertEquals(input, HTMLLinkify.linkify(input));

        input = null;

        assertNull(HTMLLinkify.linkify(input));
    }

    @Test
    public void testLinkyfyLongLinks()
    {
        int repeat = 10;

        String input = "www.ciphermail.com/" + StringUtils.repeat("x", repeat);

        assertEquals("<a href=\"" + input + "\">" + input + "</a>", HTMLLinkify.linkify(input));

        int max = 1024;
        repeat = 10000;

        input = "www.ciphermail.com/" + StringUtils.repeat("x", repeat);

        String expected = "www.ciphermail.com/" + StringUtils.repeat("x", max - "ciphermail.com/".length());

        assertEquals("<a href=\"" + expected + "\">" + expected + "</a>" +
        StringUtils.repeat("x", (repeat - (max - "ciphermail.com/".length()))),
                HTMLLinkify.linkify(input));
    }

    @Test
    public void testLinkyfySpeed()
    {
        int repeat = 1000;

        long start = System.currentTimeMillis();

        String points = StringUtils.repeat(".", 1000);

        String input = "www.ciphermail.com" + points;

        for (int i = 0; i < repeat; i++) {
            assertEquals("<a href=\"www.ciphermail.com\">www.ciphermail.com</a>" + points, HTMLLinkify.linkify(input));
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("per/sec: " + perSecond);
    }

    @Test
    public void testURLs()
    {
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.example.com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.example.com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.example.com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.example.com?var=value").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.exa-mple.com/x?x").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?page=com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=c-om").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com").matches());
        assertTrue(HTMLLinkify.HTTP_LINK_PATTERN.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com." +
                "atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action" +
                "_12580213").matches());
    }

    @Test
    public void testHttpURLs()
    {
        Pattern pattern = HTMLLinkify.HTTP_LINK_PATTERN;

        assertTrue(pattern.matcher("http://www.example.com").matches());
        assertTrue(pattern.matcher("http://www.example.com").matches());
        assertTrue(pattern.matcher("http://www.example.com").matches());
        assertTrue(pattern.matcher("http://www.example.com?var=value").matches());
        assertTrue(pattern.matcher("http://www.exa-mple.com/x?x").matches());
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?page=com").matches());
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=com").matches());
        assertTrue(pattern.matcher("http://issues.apa-che.x-x.orgx:80/xx?pag-e=c-om").matches());
        assertTrue(pattern.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com").matches());
        assertTrue(pattern.matcher("https://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian.jira." +
                "plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213").matches());
        assertTrue(pattern.matcher("www.example.com").matches());
        assertTrue(pattern.matcher("WWW.example.com").matches());
        assertTrue(pattern.matcher("hTTps://issues.apache.org/jira/browse/TAPESTRY-2267?page=com.atlassian.jira." +
                "plugin.system.issuetabpanels:comment-tabpanel&focusedCommentId=12580213#action_12580213").matches());
    }

    @Test
    public void testURLsNoMatch()
    {
        assertFalse(HTMLLinkify.HTTP_LINK_PATTERN.matcher("http://www.example.com xxx").matches());
    }

    @Test
    public void testHttpNoMatch()
    {
        Pattern pattern = HTMLLinkify.HTTP_LINK_PATTERN;

        assertFalse(pattern.matcher("test@sub.example.com").matches());
        assertFalse(pattern.matcher("sub.example.com").matches());
        assertFalse(pattern.matcher("xxx://sub.example.com").matches());
    }
}
