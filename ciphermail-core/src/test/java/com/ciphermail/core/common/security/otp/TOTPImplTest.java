/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import com.ciphermail.core.common.util.HexUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TOTPImplTest
{
    @Test
    void testTOTPTestVectorSHA1()
    throws Exception
    {
        TOTPImpl generator = new TOTPImpl();

        generator.setNrOfDigits(8);

        byte[] key = HexUtils.hexDecode("3132333435363738393031323334353637383930");

        Assertions.assertEquals("94287082", generator.generateOTP(key, 59L * 1000));
        Assertions.assertEquals("07081804", generator.generateOTP(key, 1111111109L * 1000));
        Assertions.assertEquals("14050471", generator.generateOTP(key, 1111111111L * 1000));
        Assertions.assertEquals("89005924", generator.generateOTP(key, 1234567890L * 1000));
        Assertions.assertEquals("69279037", generator.generateOTP(key, 2000000000L * 1000));
        Assertions.assertEquals("65353130", generator.generateOTP(key, 20000000000L * 1000));

        generator.verifyOTP(key, "94287082", 59L * 1000);
    }

    @Test
    void testTOTPValidationFailure()
    throws Exception
    {
        TOTPImpl generator = new TOTPImpl();

        generator.setNrOfDigits(8);

        byte[] key = HexUtils.hexDecode("3132333435363738393031323334353637383930");

        generator.verifyOTP(key, "94287082", 30L * 1000);
        generator.verifyOTP(key, "94287082", 59L * 1000);
        generator.verifyOTP(key, "94287082", 119L * 1000);

        try {
            generator.verifyOTP(key, "94287082", 120L * 1000);

            Assertions.fail();
        }
        catch (TOTPValidationFailureException e) {
            // expected
        }
    }

    @Test
    void testTOTPValidationFailureNoDelayWindow()
    throws Exception
    {
        TOTPImpl generator = new TOTPImpl();

        generator.setDelayWindow(0);
        generator.setNrOfDigits(8);

        byte[] key = HexUtils.hexDecode("3132333435363738393031323334353637383930");

        generator.verifyOTP(key, "94287082", 59L * 1000);

        try {
            generator.verifyOTP(key, "94287082", 29L * 1000);

            Assertions.fail();
        }
        catch (TOTPValidationFailureException e) {
            // expected
        }

        try {
            generator.verifyOTP(key, "94287082", 120L * 1000);

            Assertions.fail();
        }
        catch (TOTPValidationFailureException e) {
            // expected
        }
    }

    @Test
    void testTOTPTestVectorSHA256()
    throws Exception
    {
        TOTPImpl generator = new TOTPImpl();

        generator.setAlgorithm("HmacSHA256");
        generator.setNrOfDigits(8);

        byte[] key = HexUtils.hexDecode("3132333435363738393031323334353637383930" +
                "313233343536373839303132");

        Assertions.assertEquals("46119246", generator.generateOTP(key, 59L * 1000));
        Assertions.assertEquals("68084774", generator.generateOTP(key, 1111111109L * 1000));
        Assertions.assertEquals("67062674", generator.generateOTP(key, 1111111111L * 1000));
        Assertions.assertEquals("91819424", generator.generateOTP(key, 1234567890L * 1000));
        Assertions.assertEquals("90698825", generator.generateOTP(key, 2000000000L * 1000));
        Assertions.assertEquals("77737706", generator.generateOTP(key, 20000000000L * 1000));

        generator.verifyOTP(key, "46119246", 59L * 1000);
    }

    @Test
    void testTOTPTestVectorSHA512()
    throws Exception
    {
        TOTPImpl generator = new TOTPImpl();

        generator.setAlgorithm("HmacSHA512");
        generator.setNrOfDigits(8);

        byte[] key = HexUtils.hexDecode("3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "3132333435363738393031323334353637383930" +
                "31323334");

        Assertions.assertEquals("90693936", generator.generateOTP(key, 59L * 1000));
        Assertions.assertEquals("25091201", generator.generateOTP(key, 1111111109L * 1000));
        Assertions.assertEquals("99943326", generator.generateOTP(key, 1111111111L * 1000));
        Assertions.assertEquals("93441116", generator.generateOTP(key, 1234567890L * 1000));
        Assertions.assertEquals("38618901", generator.generateOTP(key, 2000000000L * 1000));
        Assertions.assertEquals("47863826", generator.generateOTP(key, 20000000000L * 1000));

        generator.verifyOTP(key, "90693936", 59L * 1000);
    }
}
