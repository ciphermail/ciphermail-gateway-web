/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PGPEncryptionBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static List<PGPPublicKey> publicKeys;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        publicKeys = PGPKeyUtils.readPublicKeys(new File(TEST_BASE, "pgp/test@example.com.gpg.asc"));
    }

    private void decrypt(File file)
    throws Exception
    {
        GPG gpg = new GPG();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

        assertTrue(gpg.decrypt(file, "test", outputStream, errorStream));

        String output = MiscStringUtils.toStringFromASCIIBytes(outputStream.toByteArray());
        String error = MiscStringUtils.toStringFromASCIIBytes(errorStream.toByteArray());

        System.out.println(output);
        System.out.println("**********");
        System.out.println(error);

        // Check some debug output.
        // Note: the output of gpg might be different for different versions of gpg
        assertTrue(error.contains("Version: CipherMail"));
        assertTrue(error.contains("gpg: public key encrypted data: good DEK"));
        assertTrue(error.contains("gpg: AES encrypted data"));
        assertTrue(error.contains(":compressed packet: algo=2"));
        assertTrue(error.contains("gpg: original file name=''"));
        assertTrue(error.contains("gpg: decryption okay"));
    }

    @Test
    public void testEncryptBinary()
    throws Exception
    {
        PGPEncryptionBuilder builder = new PGPEncryptionBuilder();

        File encryptedFile = TestUtils.createTempFile(".gpg");

        builder.encrypt(new FileInputStream(new File(TEST_BASE, "documents/testJAR.jar")),
                new FileOutputStream(encryptedFile), publicKeys);

        decrypt(encryptedFile);
    }

    @Test
    public void testEncryptText()
    throws Exception
    {
        PGPEncryptionBuilder builder = new PGPEncryptionBuilder();

        File encryptedFile = TestUtils.createTempFile(".gpg");

        builder.encrypt(new FileInputStream(new File(TEST_BASE, "documents/test.xml")),
                new FileOutputStream(encryptedFile), publicKeys);

        decrypt(encryptedFile);
    }
}
