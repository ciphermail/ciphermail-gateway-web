/*
 * Copyright (c) 2021-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.clickatell;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.test.TestProperties;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ClickatellSMSTransportTest
{
    private ClickatellSMSTransport createClickatellSMSTransport(StaticClickatellPropertiesProvider parameterProvider,
            HTTPClientProxyProvider proxyProvider, String username, String password)
    {
        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        credentialsProvider.setCredentials(new AuthScope(null, -1),
                new UsernamePasswordCredentials(username, password.toCharArray()));

        HttpClientContextFactoryImpl httpClientContextFactory = new HttpClientContextFactoryImpl(
                credentialsProvider);

        ClickatellSMSTransport transport = new ClickatellSMSTransport(
                new CloseableHttpAsyncClientFactoryImpl(proxyProvider),
                httpClientContextFactory,
                parameterProvider);

        transport.setApiURL((proxyProvider != null ?
                TestProperties.getWiremockProxyURL() : TestProperties.getWiremockURL()) +
                "/sms/clickatell/legacy/http/sendmsg");

        return transport;
    }

    private ClickatellSMSTransport createClickatellSMSTransport(StaticClickatellPropertiesProvider parameterProvider,
            HTTPClientProxyProvider proxyProvider)
    {
        return createClickatellSMSTransport(parameterProvider, proxyProvider, "test", "test");
    }

    private ClickatellSMSTransport createClickatellSMSTransport(StaticClickatellPropertiesProvider parameterProvider) {
        return createClickatellSMSTransport(parameterProvider, null);
    }

    @Before
    public void before()
    {
        // Clear interrupted status
        Thread.interrupted();
    }

    @Test
    public void testSendSuccessful()
    throws IOException
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("valid-id",
                "valid-user", "password", null);

        ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider);

        transport.sendSMS("12345678", "test");

        transport.shutdown();
    }

    @Test
    public void testInvalidAPIId()
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("invalid-id",
                "valid-user", "password", null);

        ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider);

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Error: 108, Invalid or missing api", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testInvalidAPIURL()
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("valid-id",
                "valid-user", "password", null);

        ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider);

        transport.setApiURL(TestProperties.getWiremockURL() + "/sms/clickatell/legacy/http/sendmsginvalid");

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 404, Reason: Not Found", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendViaProxy()
    throws IOException, URISyntaxException
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("valid-id",
                "valid-user", "password", null);

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider, proxyProvider);

        try {
            transport.sendSMS("12345678", "test");
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendViaProxyInvalidPassword()
    throws URISyntaxException
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("valid-id",
                "valid-user", "password", null);

        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider, proxyProvider,
                "test", "invalid");

        try {
            transport.sendSMS("12345678", "test");

            fail();
        }
        catch (IOException e) {
            assertEquals("Error sending SMS. Status: 407, Reason: Proxy Authentication Required", e.getMessage());
        }
        finally {
            transport.shutdown();
        }
    }

    @Test
    public void testSendSMSMultithreaded()
    throws Exception
    {
        StaticClickatellPropertiesProvider parameterProvider = new StaticClickatellPropertiesProvider("valid-id",
                "valid-user", "password", null);

        final ClickatellSMSTransport transport = createClickatellSMSTransport(parameterProvider);

        int nrOfMessages = 50000;

        final AtomicInteger msgCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfMessages);

        final Thread mainThread = Thread.currentThread();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfMessages; i++)
            {
                executorService.execute(() ->
                {
                    try {
                        System.out.println("SMS: " + msgCounter.incrementAndGet() + ". Thread: " +
                                Thread.currentThread().getName());

                        transport.sendSMS("12345678", "test");
                    }
                    catch (IOException  e) {
                        // Stop main thread from waiting
                        mainThread.interrupt();

                        throw new UnhandledException(e);
                    }
                    finally {
                        countDownLatch.countDown();
                    }
                });
            }

            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
            assertEquals(nrOfMessages, msgCounter.get());
        }
        finally {
            executorService.shutdown();
            transport.shutdown();
        }
    }
}
