/*
 * Copyright (c) 2013-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.PGPExpirationCheckerImpl;
import com.ciphermail.core.common.security.openpgp.PGPKeyUtils;
import com.ciphermail.core.common.security.openpgp.PGPSignatureValidatorImpl;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PGPPublicKeyValidatorChainTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    @Test
    public void testEmptyChain()
    throws Exception
    {
        Context context = new ContextImpl();

        PGPPublicKeyValidatorChain chain = new PGPPublicKeyValidatorChain();

        PGPPublicKeyValidatorResult result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "test@example.com-expiration-2030-12-31.gpg.asc")), context);

        assertTrue(result.isValid());
        assertTrue(result.getValidator() instanceof PGPPublicKeyValidatorChain);
    }

    @Test
    public void testChain()
    throws Exception
    {
        Context context = new ContextImpl();

        PGPPublicKeyValidatorChain chain = new PGPPublicKeyValidatorChain();

        final MutableInt counter = new MutableInt(0);

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                counter.increment();

                return new PGPPublicKeyValidatorResultImpl(this, true);
            }
        });

        chain.addValidators(new ExpirationValidator(new PGPExpirationCheckerImpl(new PGPSignatureValidatorImpl())));

        assertEquals(0, counter.getValue());

        PGPPublicKeyValidatorResult result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), context);

        assertEquals(1, counter.getValue());
        assertFalse(result.isValid());
        assertEquals("Public key expired on Fri Jan 11 08:02:10 CET 2013", result.getFailureMessage());
        assertTrue(result.getValidator() instanceof ExpirationValidator);
    }

    @Test
    public void testChainFastFailure()
    throws Exception
    {
        Context context = new ContextImpl();

        PGPPublicKeyValidatorChain chain = new PGPPublicKeyValidatorChain();

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                return new PGPPublicKeyValidatorResultImpl(this, false,
                        PGPPublicKeyValidatorFailureSeverity.CRITICAL,"alwaysfail");
            }
        });

        final MutableInt counter = new MutableInt(0);

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                counter.increment();

                return new PGPPublicKeyValidatorResultImpl(this, true);
            }
        });

        assertEquals(0, counter.getValue());

        PGPPublicKeyValidatorResult result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), context);

        assertEquals(0, counter.getValue());

        assertFalse(result.isValid());
        assertEquals("alwaysfail", result.getFailureMessage());
    }


    @Test
    public void testChainSeverityOrdening()
    throws Exception
    {
        Context context = new ContextImpl();

        PGPPublicKeyValidatorChain chain = new PGPPublicKeyValidatorChain();

        final MutableInt counter = new MutableInt(0);

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                counter.increment();

                return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.MAJOR,
                        "major");
            }
        });

        PGPPublicKeyValidatorResult result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), context);

        assertFalse(result.isValid());
        assertEquals(PGPPublicKeyValidatorFailureSeverity.MAJOR, result.getFailureSeverity());
        assertEquals("major", result.getFailureMessage());
        assertEquals(1, counter.getValue());
        counter.setValue(0);

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                counter.increment();

                return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.MINOR,
                        "minor");
            }
        });

        result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), context);

        assertFalse(result.isValid());
        assertEquals(PGPPublicKeyValidatorFailureSeverity.MAJOR, result.getFailureSeverity());
        assertEquals("major", result.getFailureMessage());
        assertEquals(2, counter.getValue());
        counter.setValue(0);

        chain.addValidators(new PGPPublicKeyValidator()
        {
            @Override
            public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
            {
                counter.increment();

                return new PGPPublicKeyValidatorResultImpl(this, false, PGPPublicKeyValidatorFailureSeverity.CRITICAL,
                        "critical");
            }
        });

        result = chain.validate(PGPKeyUtils.decodePublicKey(new File(TEST_BASE,
                "expired.asc")), context);

        assertFalse(result.isValid());
        assertEquals(PGPPublicKeyValidatorFailureSeverity.CRITICAL, result.getFailureSeverity());
        assertEquals("critical", result.getFailureMessage());
        assertEquals(3, counter.getValue());
    }
}
