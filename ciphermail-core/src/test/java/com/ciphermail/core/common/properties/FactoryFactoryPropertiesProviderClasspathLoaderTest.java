/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import com.ciphermail.core.test.TestProperties;
import org.junit.Test;

import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FactoryFactoryPropertiesProviderClasspathLoaderTest
{
    @Test
    public void testAutoload()
    {
        FactoryPropertiesProviderClasspathLoader loader = new FactoryPropertiesProviderClasspathLoader("com.ciphermail");

        List<Properties> properties = loader.getProperties();

        assertEquals(3, properties.size());

        Properties props = properties.get(0);
        assertEquals(2, props.size());
        assertEquals("v1", props.getProperty("p1"));
        assertEquals("v2", props.getProperty("p2"));

        props = properties.get(1);
        assertEquals(2, props.size());
        assertEquals("v3", props.getProperty("p3"));
        assertEquals("v4", props.getProperty("p4"));
    }

    @Test
    public void testSpeedTestLoadAll()
    {
        int repeat = 20;

        // warmup
        for (int i = 0; i < repeat; i++)
        {
            FactoryPropertiesProviderClasspathLoader loader = new FactoryPropertiesProviderClasspathLoader("com.ciphermail");

            loader.getProperties();
        }

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            FactoryPropertiesProviderClasspathLoader loader = new FactoryPropertiesProviderClasspathLoader("com.ciphermail");

            List<Properties> properties = loader.getProperties();

            assertTrue(properties.size() >= 2);
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("calls/sec: " + perSecond);

        double expected = 2 * TestProperties.getTestPerformanceFactor();

        // NOTE: !!! can fail on a slower system
        assertTrue("FactoryPropertiesProviderClasspathLoader too slow!!! this can fail on a slower system !!!",
                perSecond > expected);
    }
}
