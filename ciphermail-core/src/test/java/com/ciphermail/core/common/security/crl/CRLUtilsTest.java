/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import com.ciphermail.core.test.TestUtils;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class CRLUtilsTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();
    }

    @Test
    public void testGetAllDistributionPointURIs()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE, "certificates/ldap-crl.cer"));

        CRLDistPoint distPoint = X509ExtensionInspector.getCRLDistibutionPoints(certificate);

        assertNotNull(distPoint);

        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);

        assertEquals(2, uris.size());
    }

    @Test
    public void testGetAllDistributionPointURIsStartWithSpace()
    throws Exception
    {
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/crl-dist-point-starts-with-space.cer"));

        CRLDistPoint distPoint = X509ExtensionInspector.getCRLDistibutionPoints(certificate);

        assertNotNull(distPoint);

        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);

        assertEquals(1, uris.size());
        assertEquals("http://test-crl.geotrust.com/crls/preprodca2.crl", uris.iterator().next().toString());
    }

    @Test
    public void testGetAllDistributionPointURIsInvalidURIEncoding()
    throws Exception
    {
        // the cert contains a crl dist point with spaces in the url which should have been URL encoded
        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/invalid-uri-encoded-crl-dist-point.cer"));

        CRLDistPoint distPoint = X509ExtensionInspector.getCRLDistibutionPoints(certificate);

        assertNotNull(distPoint);

        /*
         * Because the URL is invalid, no valid URI will be returned
         */
        Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);

        assertEquals(0, uris.size());
    }

    @Test
    public void testCompare()
    throws Exception
    {
        File crlFile = new File(TEST_BASE, "crls/ThawteSGCCA.crl");

        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        assertNotNull(crl);

        crlFile = new File(TEST_BASE, "crls/ThawteSGCCA-thisupdate-211207.crl");

        X509CRL otherCRL = TestUtils.loadX509CRL(crlFile);

        assertTrue(CRLUtils.compare(crl, otherCRL) < 0);
        assertTrue(CRLUtils.compare(otherCRL, crl) > 0);
        assertTrue(CRLUtils.compare(crl, crl) == 0);
    }

    @Test
    public void testIsNewer()
    throws Exception
    {
        File crlFile = new File(TEST_BASE, "crls/ThawteSGCCA.crl");

        X509CRL crl = TestUtils.loadX509CRL(crlFile);

        assertNotNull(crl);

        crlFile = new File(TEST_BASE, "crls/ThawteSGCCA-thisupdate-211207.crl");

        X509CRL otherCRL = TestUtils.loadX509CRL(crlFile);

        assertFalse(CRLUtils.isNewer(crl, otherCRL));
        assertTrue(CRLUtils.isNewer(otherCRL, crl));
        assertFalse(CRLUtils.isNewer(crl, crl));
    }

    @Test
    public void testCompareWithCRLNumber()
    throws Exception
    {
        File crlFile = new File(TEST_BASE, "crls/UTN-USERFirst-ClientAuthenticationandEmail.crl");

        X509CRL crl = TestUtils.loadX509CRL(crlFile);
        assertNotNull(crl);

        crlFile = new File(TEST_BASE, "crls/UTN-USERFirst-ClientAuthenticationandEmail-211207.crl");

        X509CRL newerCRL = TestUtils.loadX509CRL(crlFile);
        assertNotNull(newerCRL);

        assertTrue(CRLUtils.compare(crl, newerCRL) < 0);
        assertTrue(CRLUtils.compare(newerCRL, crl) > 0);
        assertTrue(CRLUtils.compare(crl, crl) == 0);

        assertFalse(CRLUtils.isNewer(crl, newerCRL));
        assertTrue(CRLUtils.isNewer(newerCRL, crl));
        assertFalse(CRLUtils.isNewer(crl, crl));
    }

    /*
     * BC147-RC1 seems to fail CRL verification
     */
    @Test
    public void testCRLProblemBC147_RC()
    throws Exception
    {
        // verify CRL with default (JCE) provider
        CertificateFactory jceFac = CertificateFactory.getInstance("X.509");

        X509Certificate jceIssuer = (X509Certificate) jceFac.generateCertificate(new FileInputStream(
                new File(TEST_BASE, "certificates/ThawteSGCCA.cer")));

        X509CRL jceCRL = (X509CRL) jceFac.generateCRL(new FileInputStream(new File(TEST_BASE, "crls/ThawteSGCCA.crl")));

        jceCRL.verify(jceIssuer.getPublicKey());


        // verify CRL with BC provider
        CertificateFactory bcFac = CertificateFactory.getInstance("X.509", SecurityFactoryBouncyCastle.PROVIDER_NAME);

        X509Certificate bcIssuer = (X509Certificate) bcFac.generateCertificate(new FileInputStream(
                new File(TEST_BASE, "certificates/ThawteSGCCA.cer")));

        X509CRL bcCRL = (X509CRL) bcFac.generateCRL(new FileInputStream(new File(TEST_BASE, "crls/ThawteSGCCA.crl")));

        bcCRL.verify(bcIssuer.getPublicKey());
    }

    /*
     * Check if there is a CRL dist point, that getting the URI does not result in an exception because the URI is
     * invalid
     */
    @Test
    public void testGetAllDistributionPointURIsMozillaCerts()
    throws Exception
    {
        List<X509Certificate> certificates = CertificateUtils.readX509Certificates(new File(TEST_BASE,
                "certificates/mozilla-intermediate-certs.pem"));

        Set<String> excludedSubjects = new HashSet<>();

        excludedSubjects.add("CN=CERTSIGN FOR BANKING QUALIFIED DS PRODUCTION CA, OU=Certificat de productie "
                + "Production certificate, O=certSIGN, C=RO");

        excludedSubjects.add("CN=CERTSIGN FOR BANKING QUALIFIED DS TEST CA, OU=Certificat de test Test certificate, "
                + "O=certSIGN, C=RO");

        excludedSubjects.add("CN=CERTSIGN FOR BANKING SIMPLE SSL PRODUCTION CA, OU=Certificat de productie Production "
                + "certificate, O=certSIGN, C=RO");

        excludedSubjects.add("CN=CERTSIGN FOR BANKING SIMPLE SSL TEST CA, OU=Certificat de test Test certificate, "
                + "O=certSIGN, C=RO");

        for (X509Certificate certificate : certificates)
        {
            CRLDistPoint distPoint = X509ExtensionInspector.getCRLDistibutionPoints(certificate);

            if (distPoint != null)
            {
                Set<URI> uris = CRLUtils.getAllDistributionPointURIs(distPoint);

                // Check if all names are valid URIs
                Set<String> names = CRLDistributionPointsInspector.getURIDistributionPointNames(distPoint);

                String subject = X509CertificateInspector.getSubjectFriendly(certificate);

                // Exclude the following certs because the contain a CRL dist point URI which is not a valid URI
                // (the URI contains spaces in the path)
                if (excludedSubjects.contains(subject))
                {
                    System.out.println("Skipping CRL dist names: " + names);

                    continue;
                }

                assertEquals(names.size(), uris.size());
            }
        }
    }
}
