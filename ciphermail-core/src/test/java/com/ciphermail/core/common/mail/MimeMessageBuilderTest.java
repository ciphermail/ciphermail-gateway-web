/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.util.ByteArrayResource;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

class MimeMessageBuilderTest
{
    @Test
    void buildTextOnly()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .setTextBody("body")
                .addTo("test@example.com")
                .addHeader("X-Custom-Header", "value"))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertEquals("body", loaded.getContent());
            Assertions.assertEquals("text/plain; charset=us-ascii", loaded.getContentType());
            Assertions.assertEquals("value", loaded.getHeader("X-Custom-Header", ","));
        }
    }

    @Test
    void buildHtmlOnly()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .setHtmlBody("<html><body>test</body></html>")
                .addTo("test@example.com"))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertEquals("<html><body>test</body></html>", loaded.getContent());
            Assertions.assertEquals("text/html; charset=us-ascii", loaded.getContentType());
        }
    }

    @Test
    void buildTextAndHtml()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .setTextBody("body")
                .setHtmlBody("<html><body>test</body></html>")
                .addTo("test@example.com"))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertTrue(loaded.isMimeType("multipart/alternative"));
        }
    }

    @Test
    void buildTextAndAttachment()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .setTextBody("body")
                .addTo("test@example.com")
                .addAttachment(new ByteArrayResource(new byte[]{1,2,3}, "application/octet-stream",
                        "test.bin")))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertTrue(loaded.isMimeType("multipart/mixed"));

            Multipart mixedPart = (Multipart) loaded.getContent();

            Assertions.assertEquals(2, mixedPart.getCount());
            Assertions.assertEquals("body", mixedPart.getBodyPart(0).getContent());
            Assertions.assertArrayEquals(new byte[]{1, 2, 3}, IOUtils.toByteArray(mixedPart.getBodyPart(1).getInputStream()));
        }
    }

    @Test
    void buildTextHtmlAndAttachment()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .setTextBody("body")
                .setHtmlBody("<html><body>test</body></html>")
                .addTo("test@example.com")
                .addAttachment(new ByteArrayResource(new byte[]{1,2,3}, "application/octet-stream",
                        "test1.bin"))
                .addAttachment(new ByteArrayResource(new byte[]{4,5,6}, "application/octet-stream",
                        "test2.bin")))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertTrue(loaded.isMimeType("multipart/mixed"));

            Multipart mixedPart = (Multipart) loaded.getContent();

            Assertions.assertEquals(3, mixedPart.getCount());

            MimeBodyPart bodyPart = (MimeBodyPart) mixedPart.getBodyPart(0);
            Assertions.assertTrue(bodyPart.isMimeType("multipart/alternative"));
            MimeMultipart alternativePart = (MimeMultipart) bodyPart.getContent();
            Assertions.assertEquals(2, alternativePart.getCount());
            Assertions.assertEquals("body", alternativePart.getBodyPart(0).getContent());
            Assertions.assertEquals("<html><body>test</body></html>", alternativePart.getBodyPart(1).getContent());
            Assertions.assertTrue(alternativePart.getBodyPart(1).isMimeType("text/html"));

            Assertions.assertArrayEquals(new byte[]{1, 2, 3}, IOUtils.toByteArray(mixedPart.getBodyPart(1).getInputStream()));
            Assertions.assertArrayEquals(new byte[]{4, 5, 6}, IOUtils.toByteArray(mixedPart.getBodyPart(2).getInputStream()));
        }
    }

    @Test
    void buildBinaryAttachmentOnly()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .addTo("test@example.com")
                .addAttachment(new ByteArrayResource(new byte[]{1,2,3}, "application/octet-stream",
                        "test.bin")))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertTrue(loaded.isMimeType("multipart/mixed"));

            Multipart mixedPart = (Multipart) loaded.getContent();

            Assertions.assertEquals(1, mixedPart.getCount());
            Assertions.assertArrayEquals(new byte[]{1, 2, 3}, IOUtils.toByteArray(mixedPart.getBodyPart(0).getInputStream()));
        }
    }

    @Test
    void buildTextAttachmentOnly()
    throws Exception
    {
        try (MimeMessageBuilder mimeMessageBuilder = MimeMessageBuilder.createInstance()
                .setSubject("subject")
                .addTo("test@example.com")
                .addAttachment(new ByteArrayResource("test".getBytes(), "text/plain",
                        "test.txt")))
        {
            MimeMessage message = mimeMessageBuilder.build();

            ByteArrayOutputStream mime = new ByteArrayOutputStream();
            message.writeTo(mime);
            MimeMessage loaded = MailUtils.loadMessage(new ByteArrayInputStream(mime.toByteArray()));

            Assertions.assertEquals("subject", loaded.getSubject());
            Assertions.assertTrue(loaded.isMimeType("multipart/mixed"));

            Multipart mixedPart = (Multipart) loaded.getContent();

            Assertions.assertEquals(1, mixedPart.getCount());
            Assertions.assertEquals("test", (String) mixedPart.getBodyPart(0).getContent());
        }
    }
}