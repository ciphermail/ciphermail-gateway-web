package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.SharedByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class MailUtilsTest
{
    private static final String VALID_7BIT_MIME =
            """
            Content-Type: text/plain
            \rSubject:  !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\t\r
            From: test@example.com
            \rTo: test@example.com
            \r
            \r !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~
            \r""";

    private static final String CONTROL_CHAR_IN_HEADER_MIME =
            """
            Content-Type: text/plain
            Subject: test \u0000\s
            From: test@example.com
            To: test@example.com

            test
            """;

    private static final String CONTROL_CHAR_IN_BODY_MIME =
            """
            Content-Type: text/plain
            Subject: test\s
            From: test@example.com
            To: test@example.com

            test\u0001
            """;

    @Test
    public void testSaveNewMessage()
    throws Exception
    {
        File outputFile = File.createTempFile("ciphermail", ".eml");

        outputFile.deleteOnExit();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.addFrom(new InternetAddress[]{new InternetAddress("test@example.com")});
        message.addRecipients(RecipientType.TO, "recipient@example.com");
        message.setContent("test body", "text/plain");
        message.saveChanges();

        MailUtils.writeMessage(message, new FileOutputStream(outputFile));

        MimeMessage loadedMessage = MailUtils.loadMessage(outputFile);

        String recipients = ArrayUtils.toString(loadedMessage.getRecipients(RecipientType.TO));

        assertEquals("{recipient@example.com}", recipients);

        String from = ArrayUtils.toString(loadedMessage.getFrom());

        assertEquals("{test@example.com}", from);
    }

    @Test
    public void testSaveNewMessageRaw()
    throws Exception
    {
        File outputFile = File.createTempFile("ciphermail", ".eml");

        outputFile.deleteOnExit();

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.addFrom(new InternetAddress[]{new InternetAddress("test@example.com")});
        message.addRecipients(RecipientType.TO, "recipient@example.com");
        message.setContent("test body", "text/plain");
        message.saveChanges();

        MailUtils.writeMessageRaw(message, new FileOutputStream(outputFile));

        MimeMessage loadedMessage = MailUtils.loadMessage(outputFile);

        String recipients = ArrayUtils.toString(loadedMessage.getRecipients(RecipientType.TO));

        assertEquals("{recipient@example.com}", recipients);

        String from = ArrayUtils.toString(loadedMessage.getFrom());

        assertEquals("{test@example.com}", from);
    }

    @Test
    public void testLoadMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        assertTrue(message.isMimeType("multipart/mixed"));
    }

    @Test
    public void testValidateMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        MailUtils.validateMessage(message);
    }

    /*
     * Since Javamail 1.4.4 validating a message with a corrupt base64 encoded attachment no longer fails. Javamail
     * 1.4.4 does not re-encode when not required
     */
    @Test
    public void testValidateMessageCorruptBase64()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/corrupt-base64.eml");

        // Saving the message used to fail with Javamail <= 1.4.3 but no longer fails with 1.4.4
        message.saveChanges();

        MailUtils.validateMessage(message);

        try {
            message.getContent();

            fail();
        }
        catch(IOException e) {
            // expected
        }
    }

    @Test
    public void testValidateMessageShouldFail()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        MimeBodyPart emptyPart = new MimeBodyPart();

        message.setContent(emptyPart, "text/plain");

        message.saveChanges();

        try {
            MailUtils.validateMessage(message);

            fail();
        }
        catch(IOException e) {
            // expected. The message should be corrupt
        }
    }

    @Test
    public void testIsValidMessageInvalidMessage()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        MimeBodyPart emptyPart = new MimeBodyPart();

        message.setContent(emptyPart, "text/plain");

        message.saveChanges();

        assertFalse(MailUtils.isValidMessage(message));
    }

    @Test
    public void testIsValidMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        assertTrue(MailUtils.isValidMessage(message));
    }

    @Test
    public void testUnknownContentType()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        Object content = message.getContent();

        // it seems that Javamail 1.4 returns a SharedByteArrayInputStream when
        // the message uses an unknown content type

        assertTrue(content instanceof SharedByteArrayInputStream);

        InputStream input = (SharedByteArrayInputStream) content;

        byte[] bytes = IOUtils.toByteArray(input);

        assertArrayEquals(MiscStringUtils.getBytesASCII("body\r\n"), bytes);

        MailUtils.validateMessage(message);
    }

    @Test
    public void testUnknownContentTypeAddHeader()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type.eml");

        message.addHeader("X-Test", "test");

        message.saveChanges();

        MailUtils.validateMessage(message);
    }

    @Test
    public void testUnknownCharsetAddHeader()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-charset.eml");

        message.addHeader("X-Test", "test");

        message.saveChanges();

        MailUtils.validateMessage(message);
    }

    @Test
    public void testUnknownContentTypeMultipartAddPart()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-type-multipart.eml");

        MimeMultipart multipart = (MimeMultipart) message.getContent();

        BodyPart newPart = new MimeBodyPart();
        newPart.setContent("new part", "text/plain");

        multipart.addBodyPart(newPart);

        message.saveChanges();

        MailUtils.validateMessage(message);
    }

    @Test
    public void testContainsInvalid7BitChars()
    throws Exception
    {
        assertFalse(MailUtils.containsInvalid7BitChars(TestUtils.loadTestMessage("mail/normal-message-with-attach.eml")));
        assertTrue(MailUtils.containsInvalid7BitChars(TestUtils.loadTestMessage("mail/8bit-text.eml")));
        assertFalse(MailUtils.containsInvalid7BitChars(MailUtils.loadMessage(
                IOUtils.toInputStream(VALID_7BIT_MIME, StandardCharsets.UTF_8))));
        assertTrue(MailUtils.containsInvalid7BitChars(MailUtils.loadMessage(
                IOUtils.toInputStream(CONTROL_CHAR_IN_HEADER_MIME, StandardCharsets.UTF_8))));
        assertTrue(MailUtils.containsInvalid7BitChars(MailUtils.loadMessage(
                IOUtils.toInputStream(CONTROL_CHAR_IN_BODY_MIME, StandardCharsets.UTF_8))));
    }

    @Test
    public void testConvert8BitTo7BitSimpleText()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/8bit-text.eml");

        assertEquals("8bIt", message.getEncoding());

        assertTrue(MailUtils.convertTo7Bit(message));

        message.saveChanges();

        assertEquals("quoted-printable", message.getEncoding());

        MailUtils.validateMessage(message);

        assertEquals("from 8bit to 7bit by CipherMail", message.getHeader("X-MIME-Autoconverted", ","));
    }

    @Test
    public void testConvert8BitTo7BitBinary()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/8bit-binary.eml");

        assertEquals("8bit", message.getEncoding());

        assertTrue(MailUtils.convertTo7Bit(message));

        message.saveChanges();

        assertEquals("base64", message.getEncoding());

        File file = File.createTempFile("ciphermail", ".eml");

        file.deleteOnExit();

        MailUtils.writeMessage(message, file);

        String mime = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("VGhpcyBpcyBhIHRlc3Qgd2l0aCB1bmxhdXRzOiBTY2jDtm4KCg=="));
    }

    @Test
    public void testConvert8BitTo7BitMultipart()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/8bit-multipart.eml");

        assertEquals("<123456789>", message.getMessageID());

        assertTrue(MailUtils.convertTo7Bit(message));

        message.saveChanges();

        File file = File.createTempFile("ciphermail", ".eml");

        file.deleteOnExit();

        MailUtils.writeMessage(message, file);

        // Message-id changed
        assertNotEquals("123456789", message.getMessageID());

        String mime = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));
        assertTrue(mime.contains("VGhpcyBpcyBhIHRlc3Qgd2l0aCB1bmxhdXRzOiBTY2jDtm4K"));
    }

    @Test
    public void testContains8BitMimePart()
    throws Exception
    {
        assertTrue(MailUtils.contains8BitMimePart(TestUtils.loadTestMessage("mail/8bit-multipart.eml")));
        assertTrue(MailUtils.contains8BitMimePart(TestUtils.loadTestMessage("mail/8bit-binary.eml")));
        assertTrue(MailUtils.contains8BitMimePart(TestUtils.loadTestMessage("mail/8bit-text.eml")));
        assertFalse(MailUtils.contains8BitMimePart(TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml")));
    }

    @Test
    public void testConvertTo7BitNoConversion()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml");

        String messageID = message.getMessageID();

        assertFalse(MailUtils.convertTo7Bit(message));

        message.saveChanges();

        MailUtils.validateMessage(message);

        // saveChanges changes the message ID so we cannot compare the result.
        // we need to set the original message id
        MimeMessageWithID mime = new MimeMessageWithID(message, messageID);

        mime.saveChanges();

        File file = File.createTempFile("ciphermail", ".eml");

        file.deleteOnExit();

        MailUtils.writeMessage(mime, file);

        // we need to correct CR/LF pairs because org only uses LF

        String result = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        result = StringUtils.replace(result, "\r\n", "\n").trim();

        String exp = FileUtils.readFileToString(new File(TestUtils.getTestDataDir(),
                        "mail/multiple-inline-attachments.eml"), StandardCharsets.UTF_8);

        exp = StringUtils.replace(exp, "\r\n", "\n").trim();

        assertEquals(exp, result);
    }

    @Test
    public void testAttachMessageAsRFC822()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message-with-id.eml");

        MimeMessage toAttach = TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml");

        String md5 = HexUtils.hexEncode(DigestUtils.md5(IOUtils.toByteArray(toAttach.getRawInputStream())));

        MimeMessage attached = MailUtils.attachMessageAsRFC822(message, toAttach, "attached.eml");

        MailUtils.validateMessage(attached);

        assertTrue(attached.isMimeType("multipart/mixed"));
        assertEquals("test simple message", attached.getSubject());
        assertEquals("<123456>", attached.getMessageID());
        assertEquals("test@example.com", StringUtils.join(attached.getFrom()));
        assertEquals("test@example.com", StringUtils.join(attached.getRecipients(RecipientType.TO)));

        MimeMultipart mp = (MimeMultipart) attached.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain", bp.getContentType());
        assertEquals("test", StringUtils.trim((String) bp.getContent()));

        bp = mp.getBodyPart(1);
        assertEquals("message/rfc822; name=attached.eml", bp.getContentType());

        MimeMessage rfc822 = (MimeMessage) bp.getContent();

        assertArrayEquals(MailUtils.partToByteArray(rfc822), MailUtils.partToByteArray(toAttach));

        assertEquals(md5, HexUtils.hexEncode(DigestUtils.md5(IOUtils.toByteArray(rfc822.getRawInputStream()))));
    }

    @Test
    public void testAttachMessageAsRFC822FromMultipart()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml");

        MimeMessage toAttach = TestUtils.loadTestMessage("mail/simple-text-message-with-id.eml");

        MimeMessage attached = MailUtils.attachMessageAsRFC822(message, toAttach, "attached.eml");

        MailUtils.validateMessage(attached);

        assertTrue(attached.isMimeType("multipart/mixed"));
        assertEquals("test multiple attachments", attached.getSubject());
        assertEquals("<49F85D01.3030000@djigzo.com>", attached.getMessageID());
        assertEquals("Thunderbird 2.0.0.21 (X11/20090318)", attached.getHeader("User-Agent", ","));

        MimeMultipart mp = (MimeMultipart) attached.getContent();

        assertEquals(7, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);
        assertEquals("text/plain; charset=ISO-8859-1; format=flowed", bp.getContentType());
        assertEquals("-- \r\nDjigzo open source email encryption gateway www.djigzo.com",
                ((String) bp.getContent()).trim());

        bp = mp.getBodyPart(1);
        assertEquals("image/png;\r\n name=\"image1.png\"", bp.getContentType());

        bp = mp.getBodyPart(2);
        assertEquals("image/png;\r\n name=\"image2.png\"", bp.getContentType());

        bp = mp.getBodyPart(3);
        assertEquals("image/png;\r\n name=\"image3.png\"", bp.getContentType());

        bp = mp.getBodyPart(4);
        assertEquals("image/png;\r\n name=\"image4.png\"", bp.getContentType());

        bp = mp.getBodyPart(5);
        assertEquals("image/png;\r\n name=\"image5.png\"", bp.getContentType());

        bp = mp.getBodyPart(6);
        assertEquals("message/rfc822; name=attached.eml", bp.getContentType());
        assertEquals("attachment", bp.getDisposition());

        MimeMessage rfc822 = (MimeMessage) bp.getContent();

        assertEquals("test simple message", rfc822.getSubject());
        assertEquals("<123456>", rfc822.getMessageID());
        assertEquals("test@example.com", StringUtils.join(rfc822.getFrom()));
        assertEquals("test@example.com", StringUtils.join(rfc822.getRecipients(RecipientType.TO)));
        assertEquals("text/plain", rfc822.getContentType());
        assertEquals("test", StringUtils.trim((String) rfc822.getContent()));
    }

    @Test
    public void testAttachMessageAsRFC822FromMultipartAlternative()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/html-alternative.eml");

        MimeMessage toAttach = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        MimeMessage attached = MailUtils.attachMessageAsRFC822(message, toAttach, "signed.eml");

        MailUtils.validateMessage(attached);

        assertTrue(attached.isMimeType("multipart/mixed"));
        assertEquals("From Joshua Barger and other Open Source group members on LinkedIn", attached.getSubject());
        assertEquals("<1266236208.59156794.1253209950952.JavaMail.app@ech3-cdn18.prod>", attached.getMessageID());

        MimeMultipart mp = (MimeMultipart) attached.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);
        assertTrue(bp.isMimeType("multipart/alternative"));

        MimeMultipart innerMP = (MimeMultipart) bp.getContent();

        assertEquals(2, innerMP.getCount());

        bp = innerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", bp.getContentType());

        bp = innerMP.getBodyPart(1);

        assertEquals("text/html; charset=UTF-8", bp.getContentType());

        // Continue with outer part
        bp = mp.getBodyPart(1);

        assertEquals("message/rfc822; name=signed.eml", bp.getContentType());
        assertEquals("attachment", bp.getDisposition());

        MimeMessage rfc822 = (MimeMessage) bp.getContent();

        assertEquals("Signed by Evolution", rfc822.getSubject());

        assertArrayEquals(MailUtils.partToByteArray(rfc822), MailUtils.partToByteArray(toAttach));
    }

    @Test
    public void testCloneMessage()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        MimeMessage clone = MailUtils.cloneMessage(message);

        assertEquals(message.getMessageID(), clone.getMessageID());

        // Saving changes the message id
        clone.saveChanges();

        assertNotEquals(message.getMessageID(), clone.getMessageID());

        // The clone with fixed message id will always use the same message id
        MimeMessage cloneWithFixedID = MailUtils.cloneMessageWithFixedMessageID(message);

        assertEquals(message.getMessageID(), cloneWithFixedID.getMessageID());

        // Saving changes the message id
        cloneWithFixedID.saveChanges();

        assertEquals(message.getMessageID(), cloneWithFixedID.getMessageID());
    }

    @Test
    public void testGetMessageIDQuietly()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/clear-signed-evolution.eml");

        assertEquals("<1195030544.6663.27.camel@ubuntu>", MailUtils.getMessageIDQuietly(message, null));
        assertNull(MailUtils.getMessageIDQuietly(null, null));
        assertEquals("1", MailUtils.getMessageIDQuietly(null, "1"));

        assertEquals("<1195030544.6663.27.camel@ubuntu>", MailUtils.getMessageIDQuietly(message));
        assertNull(MailUtils.getMessageIDQuietly(null));
    }

    @Test
    public void testGetSafeSubject()
    throws Exception
    {
        MimeMessage message = new MimeMessage((Session)null);

        message.setSubject("testü");

        assertEquals("testü", MailUtils.getSafeSubject(message));

        message = new MimeMessage((Session)null);

        message.setSubject("testü\t\r\n 0123\u0000\u0008\u000E\u0013 0123\u001B\u000C\u001F\u001E");

        assertEquals("testü\t\r\n 0123\u0000\u0008\u000E\u0013 0123\u001B\u000C\u001F\u001E", message.getSubject());

        assertEquals("testü\t\r\n 0123 0123", MailUtils.getSafeSubject(message));

        byte[] allchars = new byte[127];

        for (int i = 0; i < allchars.length; i++) {
            allchars[i] = (byte)i;
        }

        message = new MimeMessage((Session)null);

        message.setSubject(new String(allchars));

        assertEquals(new String(allchars), message.getSubject());
        assertEquals("\t\n\r !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                MailUtils.getSafeSubject(message));
    }

    @Test
    public void testDeleteUnsafeChars()
    throws Exception
    {
        assertEquals("\t\n\r !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~",
                MailUtils.deleteUnsafeChars(
                        "\t\n\r !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\u0000\u0008\u000E\u0013"
                        + "[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\u0000\u0008\u000E\u0013"));
    }
}
