/*
 * Copyright (c) 2013-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.provider.CipherMailProvider;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.SerializationUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class PGPCertificateTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static final String SERIALIZED_CERTIFICATE =
            "ACED00057372002D6A6176612E73656375726974792E636572742E436572746966696361746524436572746966696361746552" +
            "657089276A9DC9AE3C0C0200025B0004646174617400025B424C0004747970657400124C6A6176612F6C616E672F537472696E" +
            "673B7870757200025B42ACF317F8060854E002000078700000028499010D04525503C5010800A46B100FE6A1D5B85860E41368" +
            "A917173A0DA80DEC78467E772C741BF1A45764041E52106FF4E9694ADC183623E1CBEECBED087544E4406D19F4F875A163BE78" +
            "440DC7A4D28B376227635AF86B63D1227F041E2210941D9D9ECCCFAFC0814884528EFB830BF480CFC1150BBED3477E9FADADA5" +
            "17709753226625419ADB9F08F9457E92A3CB72B3ECA6E7CE567DBBFD0ED591ED1736656ED04F3EF0BD517F80E5B7D978A6378F" +
            "F58EC56EED746CDF2D83D714E6D05E45D8D4387E51BE6103441253FEEEDC6FB545807B84AED3E1FF4720DB2CF2E0B2B51E8459" +
            "58146EC2F0AE5B48732B2C4AD8287227CDE86C057762B4401A3FB889A371CAACCC8CE9DCCF41E10011010001B4377465737420" +
            "6B657920646A69677A6F20287468697320697320612074657374206B657929203C74657374406578616D706C652E636F6D3E89" +
            "01380413010200220502525503C5021B03060B090807030206150802090A0B0416020301021E01021780000A09104EC4E8813E" +
            "11A9A034D307FF5E2474FF242503943AA6A5FF2EBEAE764B4DF6097DFCA461598DDAC38D65AFA4D57774493962EDB6749183CF" +
            "1E5AEDA5CE21C281C9C6ED7B4607548F19C186616EC8F5638BD932CEE3FE57E2AD2875D2B2A1ADF64A36F418E76630EB8B1F51" +
            "BE3B9055BCDFEA6BEB3B74DA93EF60F977EA9AAA27A08807983A715F1FD62A225E05208CF2D3B984AAC16B2A041B21B6CEB451" +
            "20973F717BA321B995DF0063243DAE09839D6F7044B46839A36FB30292B97DF456E64800D548C069D4DE3A5317EA75ADA5A752" +
            "F2C9B4392AFAC3CC64EC990C2B824FD2CCFC6DF688687A9A2708EF245BEBDFF0BC6C1B31FF06399A615D92B29C35A7EE64C959" +
            "D2669184A83E7198740003504750";

    @BeforeClass
    public static void beforeClass() {
        CipherMailProvider.initialize(null);
    }

    @Test
    public void testSerialization()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPCertificate certificate = new PGPCertificate(publicKey);

        byte[] serialized = SerializationUtils.serialize(certificate);

        PGPCertificate certificate2 = (PGPCertificate) SerializationUtils.deserialize(serialized);

        assertEquals(certificate, certificate2);
    }

    @Test
    public void testDeserializeLongTerm()
    throws Exception
    {
        PGPPublicKey publicKey = PGPKeyUtils.decodePublicKey(new File(TEST_BASE, "test@example.com.gpg.asc"));

        PGPCertificate certificate = new PGPCertificate(publicKey);

        PGPCertificate certificate2 = (PGPCertificate) SerializationUtils.deserialize(HexUtils.hexDecode(
                SERIALIZED_CERTIFICATE));

        assertEquals(certificate, certificate2);
    }
}
