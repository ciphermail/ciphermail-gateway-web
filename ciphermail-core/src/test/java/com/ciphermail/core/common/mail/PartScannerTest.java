/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.test.TestUtils;
import org.junit.Before;
import org.junit.Test;

import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PartScannerTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static class PartCounter implements PartScanner.PartListener
    {
        private int count;

        @Override
        public boolean onPart(Part parent, Part part, Object context)
        {
            count++;

            return true;
        }

        public int getCount() {
            return count;
        }
    }

    private static class PartCounterStop implements PartScanner.PartListener
    {
        private int count;

        @Override
        public boolean onPart(Part parent, Part part, Object context)
        {
            count++;

            return false;
        }

        public int getCount() {
            return count;
        }
    }

    private MimeMessage message;

    @Before
    public void before()
    throws Exception
    {
        message = MailUtils.loadMessage(new File(TEST_BASE,"mail/normal-message-with-attach.eml"));
    }

    @Test
    public void testScanPart()
    throws Exception
    {
        PartCounter partCounter = new PartCounter();

        PartScanner scanner = new PartScanner(partCounter, 5);

        scanner.scanPart(message);

        assertEquals(3, partCounter.getCount());
    }

    @Test
    public void testScanPartNoMax()
    throws Exception
    {
        PartCounter partCounter = new PartCounter();

        PartScanner scanner = new PartScanner(partCounter);

        scanner.scanPart(message);

        assertEquals(3, partCounter.getCount());
    }

    @Test
    public void testScanPartMaxDepth0()
    throws Exception
    {
        PartCounter partCounter = new PartCounter();

        PartScanner scanner = new PartScanner(partCounter, 0);

        scanner.scanPart(message);

        assertEquals(1, partCounter.getCount());
    }

    @Test
    public void testScanPartMaxDepth1()
    throws Exception
    {
        PartCounter partCounter = new PartCounter();

        PartScanner scanner = new PartScanner(partCounter, 1);

        scanner.scanPart(message);

        assertEquals(2, partCounter.getCount());
    }

    @Test
    public void testScanPartMaxDepth1ExceptionOnMaxReached()
    throws Exception
    {
        PartCounter partCounter = new PartCounter();

        PartScanner scanner = new PartScanner(partCounter, 1);

        scanner.setExceptionOnMaxDepthReached(true);

        try {
            scanner.scanPart(message);

            fail("MaxDepthReachedException should have been thrown");
        }
        catch(MaxDepthReachedException e) {
            assertEquals("Maximum depth 1 reached", e.getMessage());
        }
    }

    @Test
    public void testScanPartStop()
    throws Exception
    {
        PartCounterStop partCounter = new PartCounterStop();

        PartScanner scanner = new PartScanner(partCounter, -1);

        scanner.scanPart(message);

        assertEquals(1, partCounter.getCount());
    }
}
