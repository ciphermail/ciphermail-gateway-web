/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository.hibernate;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.mail.repository.MailRepositorySearchField;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class MailRepositoryImplTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "mail/");

    private static final String DEFAULT_REPOSITORY = "quarantine";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                getRepository(DEFAULT_REPOSITORY).deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private static MimeMessage loadMessage(String filename)
    throws Exception
    {
        return MailUtils.loadMessage(new File(TEST_BASE, filename));
    }

    private MailRepository getRepository(String repositoryName) {
        return new MailRepositoryImpl(sessionManager, repositoryName);
    }

    private long getItemCount(String repositoryName)
    {
        return Optional.ofNullable(transactionOperations.execute(status ->
        {
            try {
                return getRepository(repositoryName).getItemCount();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        })).orElseThrow();
    }

    private MailRepositoryItem createItem(String repositoryName, MimeMessage message)
    throws Exception
    {
        return getRepository(repositoryName).createItem(message);
    }

    private MailRepositoryItem addItem(String repositoryName, MimeMessage message)
    {
        return transactionOperations.execute(status ->
        {
            try {
                MailRepository repository = getRepository(repositoryName);

                MailRepositoryItem item = repository.createItem(message);

                repository.addItem(item);

                return item;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private MailRepositoryItem addItemWithDate(String repositoryName, MimeMessage message, Date created)
    {
        return transactionOperations.execute(status ->
        {
            try {
                MailRepository repository = getRepository(repositoryName);

                MailRepositoryItem item = ((MailRepositoryImpl) repository).createItem(message, created);

                repository.addItem(item);

                return item;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private MailRepositoryItem addItem(String repositoryName, MimeMessage message, InternetAddress[] recipients,
            InternetAddress sender, String remoteAddress, Date lastUpdated, byte[] additionalData)
    {
        return transactionOperations.execute(status ->
        {
            try {
                MailRepository repository = getRepository(repositoryName);

                MailRepositoryItem item = repository.createItem(message);

                item.setRecipients(Arrays.asList(recipients));
                item.setSender(sender);
                item.setRemoteAddress(remoteAddress);
                item.setLastUpdated(lastUpdated);
                item.setAdditionalData(additionalData);

                repository.addItem(item);

                return item;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void deleteItem(String repositoryName, UUID id) {
        transactionOperations.executeWithoutResult(status -> getRepository(repositoryName).deleteItem(id));
    }

    private MailRepositoryItem getItem(String repositoryName, UUID id) {
        return transactionOperations.execute(status -> getRepository(repositoryName).getItem(id));
    }

    private List<? extends MailRepositoryItem> getItems(String repositoryName, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status -> getRepository(repositoryName).getItems(firstResult, maxResults));
    }

    private List<? extends MailRepositoryItem> searchItems(String repositoryName,
            MailRepositorySearchField searchField, String key, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status -> getRepository(repositoryName).searchItems(
                searchField, key, firstResult, maxResults));
    }

    private long getSearchCount(String repositoryName, MailRepositorySearchField searchField, String key)
    {
        return Optional.ofNullable(transactionOperations.execute(status -> getRepository(repositoryName).getSearchCount(
                searchField, key))).orElseThrow();
    }

    private List<? extends MailRepositoryItem> getItemsBefore(String repositoryName, Date before, Integer firstResult,
            Integer maxResults)
    {
        return transactionOperations.execute(status -> getRepository(repositoryName).getItemsBefore(
                before, firstResult, maxResults));
    }

    private void assertMessage(String repositoryName, UUID id, MimeMessage expected)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                MailRepositoryItem item = getItem(repositoryName, id);

                ByteArrayOutputStream bos1 = new ByteArrayOutputStream();

                MailUtils.writeMessage(item.getMimeMessage(), bos1);

                ByteArrayOutputStream bos2 = new ByteArrayOutputStream();

                MailUtils.writeMessage(expected, bos2);

                assertArrayEquals(bos2.toByteArray(), bos1.toByteArray());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private List<String> getRecipients(String repositoryName, UUID id)
    throws Exception
    {
        return transactionOperations.execute(status ->
        {
            try {
                MailRepositoryItem item = getItem(repositoryName, id);

                return EmailAddressUtils.addressesToStrings(item.getRecipients(), true);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testCreateItem()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryItem item = createItem(DEFAULT_REPOSITORY, message);

        assertNotNull(item);
        assertNotNull(item.getCreated());

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));
    }

    @Test
    public void testAddItem()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryItem item = addItem(DEFAULT_REPOSITORY, message);

        assertNotNull(item);

        assertEquals(1, getItemCount(DEFAULT_REPOSITORY));

        assertEquals("\"Open Source Group Members\" <group-digests@linkedin.com>", item.getFromHeader());
        assertEquals("From Joshua Barger and other Open Source group members on LinkedIn", item.getSubject());
        assertEquals("<1266236208.59156794.1253209950952.JavaMail.app@ech3-cdn18.prod>", item.getMessageID());

        item = addItem(DEFAULT_REPOSITORY, message);

        assertNotNull(item);

        assertEquals(2, getItemCount(DEFAULT_REPOSITORY));
    }

    @Test
    public void testInvalidFrom()
    throws Exception
    {
        MimeMessage message = loadMessage("invalid-from-to-cc-reply-to.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryItem item = addItem(DEFAULT_REPOSITORY, message);

        assertNotNull(item);

        assertEquals(1, getItemCount(DEFAULT_REPOSITORY));

        assertEquals("\"%8787\" <&^&^&^&^&^&)\"(&@^%##()||", item.getFromHeader());
    }

    @Test
    public void testDeleteItem()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryItem item = addItem(DEFAULT_REPOSITORY, message);

        assertNotNull(item);

        assertEquals(1, getItemCount(DEFAULT_REPOSITORY));

        deleteItem(DEFAULT_REPOSITORY, item.getID());

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));
    }

    @Test
    public void testGetItem()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        MailRepositoryItem item = addItem(DEFAULT_REPOSITORY, message,
                InternetAddress.parse("test1@example.com, test2@example.com", false),
                new InternetAddress("sender@example.com"),
                "127.0.0.1",
                new Date(123456),
                new byte[]{1,2,3});

        MailRepositoryItem item2 = getItem(DEFAULT_REPOSITORY, item.getID());

        assertNotNull(item2);

        assertEquals("\"Open Source Group Members\" <group-digests@linkedin.com>", item2.getFromHeader());
        assertEquals("From Joshua Barger and other Open Source group members on LinkedIn", item2.getSubject());
        assertEquals("<1266236208.59156794.1253209950952.JavaMail.app@ech3-cdn18.prod>", item2.getMessageID());
        assertEquals("sender@example.com", item2.getSender().getAddress());
        assertEquals("127.0.0.1", item2.getRemoteAddress());

        /*
         * Because MySQL does not store milliseconds, we need to truncate before comparing
         */
        assertEquals(123000, DateUtils.truncate(item2.getLastUpdated(), Calendar.SECOND).getTime());

        assertArrayEquals(new byte[]{1, 2, 3}, item2.getAdditionalData());

        assertEquals("[test1@example.com, test2@example.com]",
                getRecipients(DEFAULT_REPOSITORY, item2.getID()).toString());

        assertMessage(DEFAULT_REPOSITORY, item2.getID(), message);
    }

    @Test
    public void testGetItems()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        int nrOfItems = 10;

        for (int i = 0; i < nrOfItems; i++)
        {
            addItem(DEFAULT_REPOSITORY, message,
                    InternetAddress.parse("test1@example.com, test2@example.com", false),
                    null,
                    Integer.toString(i),
                    null,
                    null);
        }

        assertEquals(nrOfItems, getItemCount(DEFAULT_REPOSITORY));

        List<? extends MailRepositoryItem> items = getItems(DEFAULT_REPOSITORY, 0, Integer.MAX_VALUE);

        assertEquals(nrOfItems, items.size());

        for (int i = 0; i < nrOfItems; i++)
        {
            MailRepositoryItem item = items.get(i);

            assertEquals(Integer.toString(i), item.getRemoteAddress());
        }

        items = getItems(DEFAULT_REPOSITORY, 0, 1);

        assertEquals(1, items.size());

        items = getItems(DEFAULT_REPOSITORY, 8, 100);

        assertEquals(2, items.size());

        assertEquals("8", items.get(0).getRemoteAddress());
        assertEquals("9", items.get(1).getRemoteAddress());
    }

    @Test
    public void testSearch()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        int nrOfItems = 10;

        for (int i = 0; i < nrOfItems; i++)
        {
            addItem(DEFAULT_REPOSITORY, message,
                    InternetAddress.parse("test" + i + "@example.com, other@example.com", false),
                    new InternetAddress("sender" + i + "@example.com", false),
                    "127.0.0." + i,
                    null,
                    null);
        }

        assertEquals(nrOfItems, getItemCount(DEFAULT_REPOSITORY));

        // search from
        List<? extends MailRepositoryItem> items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.FROM, "%linkedin%", 0, Integer.MAX_VALUE);
        assertEquals(10, items.size());

        // search from no hits
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.FROM, "linkedin", 0, Integer.MAX_VALUE);
        assertEquals(0, items.size());

        // search message ID
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.MESSAGE_ID,
                "<1266236208.59156794.1253209950952.JavaMail.app@ech3-cdn18.prod>",
                3, 2);
        assertEquals(2, items.size());

        // search subject
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.SUBJECT, "%OPEN SOURCE GROUP%",
                3, 1000 /* Oracle does not like Integer.MAX_VALUE */);
        assertEquals(7, items.size());

        // search sender
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.SENDER, "sender4@example.com",
                0, 1000 /* Oracle does not like Integer.MAX_VALUE */);
        assertEquals(1, items.size());
        assertEquals("127.0.0.4", items.get(0).getRemoteAddress());

        // search recipients
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.RECIPIENTS, "test5@example.com",
                0, 1000 /* Oracle does not like Integer.MAX_VALUE */);
        assertEquals(1, items.size());
        assertEquals("127.0.0.5", items.get(0).getRemoteAddress());

        // search recipients
        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.RECIPIENTS, "other@example.com",
                5, 1000 /* Oracle does not like Integer.MAX_VALUE */);
        assertEquals(5, items.size());
    }

    @Test
    public void testSearchCount()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        int nrOfItems = 10;

        for (int i = 0; i < nrOfItems; i++)
        {
            addItem(DEFAULT_REPOSITORY, message,
                    InternetAddress.parse("test" + i + "@example.com, other@example.com", false),
                    new InternetAddress("sender" + i + "@example.com", false),
                    "127.0.0." + i,
                    null,
                    null);
        }

        assertEquals(nrOfItems, getItemCount(DEFAULT_REPOSITORY));
        // search from
        assertEquals(10, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.FROM, "%linkedin%"));

        // search from no hits
        assertEquals(0, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.FROM, "linkedin"));

        // search message ID
        assertEquals(10, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.MESSAGE_ID,
                "<1266236208.59156794.1253209950952.JavaMail.app@ech3-cdn18.prod>"));

        // search subject
        assertEquals(10, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.SUBJECT, "%OPEN SOURCE GROUP%"));

        // search sender
        assertEquals(1, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.SENDER, "sender4@example.com"));

        // search recipients
        assertEquals(1, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.RECIPIENTS, "test5@example.com"));

        // search recipients
        assertEquals(10, getSearchCount(DEFAULT_REPOSITORY,
                MailRepositorySearchField.RECIPIENTS, "other@example.com"));
    }

    @Test
    public void testSorting()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        Date now = new Date();

        MailRepositoryItem item4 = addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, 1));

        MailRepositoryItem item3 = addItemWithDate(DEFAULT_REPOSITORY, message, now);

        MailRepositoryItem item2 = addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, -1));

        MailRepositoryItem item1 = addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, -2));

        assertEquals(4, getItemCount(DEFAULT_REPOSITORY));

        List<? extends MailRepositoryItem> items = getItems(DEFAULT_REPOSITORY, 0, Integer.MAX_VALUE);

        assertEquals(4, items.size());

        assertEquals(item1.getID(), items.get(0).getID());
        assertEquals(item2.getID(), items.get(1).getID());
        assertEquals(item3.getID(), items.get(2).getID());
        assertEquals(item4.getID(), items.get(3).getID());

        items = getItems(DEFAULT_REPOSITORY, 2, 1000 /* Oracle does not like Integer.MAX_VALUE */);

        assertEquals(2, items.size());
        assertEquals(item3.getID(), items.get(0).getID());
        assertEquals(item4.getID(), items.get(1).getID());

        items = searchItems(DEFAULT_REPOSITORY,
                MailRepositorySearchField.FROM, "%linkedin%", 0, 1000 /* Oracle does not like Integer.MAX_VALUE */);

        assertEquals(4, items.size());

        assertEquals(item1.getID(), items.get(0).getID());
        assertEquals(item2.getID(), items.get(1).getID());
        assertEquals(item3.getID(), items.get(2).getID());
        assertEquals(item4.getID(), items.get(3).getID());
    }

    @Test
    public void testBefore()
    throws Exception
    {
        MimeMessage message = loadMessage("html-alternative.eml");

        assertEquals(0, getItemCount(DEFAULT_REPOSITORY));

        Date now = new Date();

        addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, 1));

        addItemWithDate(DEFAULT_REPOSITORY, message, now);

        MailRepositoryItem item2 = addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, -1));

        MailRepositoryItem item1 = addItemWithDate(DEFAULT_REPOSITORY, message, DateUtils.addHours(now, -2));

        assertEquals(4, getItemCount(DEFAULT_REPOSITORY));

        /*
         * Extract 1 minute to make sure the item added with now is not included. With the MariaDB JDBC driver,
         * the item with the same date is also included
         */
        List<? extends MailRepositoryItem> items = getItemsBefore(DEFAULT_REPOSITORY,
                DateUtils.addMinutes(now, -1), 0, Integer.MAX_VALUE);

        assertEquals(2, items.size());

        assertEquals(item1.getID(), items.get(0).getID());
        assertEquals(item2.getID(), items.get(1).getID());
    }

    /*
     * Insert very large messages and check the speed of inserting and loading and whether loading all messages
     * does not result in a OOM (i.e., the messages should be lazy loaded).
     */
    @Test
    public void testPerformance()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        int messageSize = 50 * SizeUtils.MB;

        byte[] content = new byte[messageSize];

        Arrays.fill(content, (byte)'A');

        message.setContent(content, "application/octet-stream");
        message.saveChanges();
        MailUtils.validateMessage(message);

        long start = System.currentTimeMillis();

        int nrOfItems = 10;

        for (int i = 0; i < nrOfItems; i++)
        {
            addItem(DEFAULT_REPOSITORY, message,
                    InternetAddress.parse("test" + i + "@example.com, other@example.com", false),
                    new InternetAddress("sender" + i + "@example.com", false),
                    "127.0.0." + i,
                    null,
                    null);
        }

        double msec = (double)(System.currentTimeMillis() - start) / nrOfItems;

        System.out.println("msec / add:" + msec);

        assertTrue("Can fail on slower systems", msec < 10000);

        start = System.currentTimeMillis();

        List<? extends MailRepositoryItem> items = getItems(DEFAULT_REPOSITORY, 0, Integer.MAX_VALUE);

        for (MailRepositoryItem item : items) {
            System.out.println(item.getID());
        }

        msec = (double)(System.currentTimeMillis() - start) / nrOfItems;

        System.out.println("msec / get:" + msec);

        double expected = 100 / TestProperties.getTestPerformanceFactor();

        assertTrue("Can fail on slower systems", msec < expected);
    }
}
