/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPSecretKeyRingCollection;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PGPHandlerTest
{
    private static final File TEST_BASE = new File(TestUtils.getTestDataDir(), "pgp/");

    private static PGPKeyPairProvider keyPairProvider;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        PGPSecretKeyRingCollection keyRingCollection = new JcaPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(
                new FileInputStream(new File(TEST_BASE, "test@example.com.gpg.key"))));

        keyPairProvider = new PGPSecretKeyRingCollectionKeyPairProvider(keyRingCollection,
                new StaticPasswordProvider("test"));
    }

    @Test
    public void testEncryptedBinary()
    throws Exception
    {
        PGPHandler handler = new PGPHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "djigzo-whitepaper.pdf.gpg"));

        File outputFile = TestUtils.createTempFile(".pdf");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            handler.handle(input, output);
        }
        finally {
            output.close();
        }

        /*
         * Check md5 of the output file
         */
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(FileUtils.readFileToByteArray(outputFile),
                Digest.MD5));

        List<PGPLayer> pgpLayers = handler.getPGPLayers();

        /*
         * The message should have an encrypted, compressed and literal data layer
         */
        assertEquals(3, pgpLayers.size());

        /*
         * First layer
         */
        PGPLayer layerInfo = pgpLayers.get(0);
        assertEquals(1, layerInfo.getParts().size());
        PGPLayerPart layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPEncryptionLayerPart);

        PGPEncryptionLayerPart encryptionLayer = (PGPEncryptionLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(encryptionLayer.getPublicKeyEncryptedData().getKeyID()));
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().isIntegrityProtected());
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().verify());

        /*
         * Second layer
         */
        layerInfo = pgpLayers.get(1);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPCompressionLayerPart);

        assertNull(layer.getArmorHeaders());
        PGPCompressionLayerPart compressionLayer = (PGPCompressionLayerPart) layer;

        assertEquals(PGPCompressionAlgorithm.ZLIB, PGPCompressionAlgorithm.fromTag(
                compressionLayer.getCompressedData().getAlgorithm()));

        /*
         * Third layer
         */
        layerInfo = pgpLayers.get(2);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPLiteralLayerPart);

        PGPLiteralLayerPart literalInfo = (PGPLiteralLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals("djigzo-whitepaper.pdf", literalInfo.getPGPLiteralData().getFileName());
        assertEquals(PGPLiteralPacketType.BINARY, PGPLiteralPacketType.fromTag(
                literalInfo.getPGPLiteralData().getFormat()));
    }

    @Test
    public void testEncryptedText()
    throws Exception
    {
        PGPHandler handler = new PGPHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "pgp-encrypted.txt.asc"));

        FileOutputStream output = new FileOutputStream(TestUtils.createTempFile(".txt"));

        try {
            handler.handle(input, output);
        }
        finally {
            output.close();
        }

        List<PGPLayer> pgpLayers = handler.getPGPLayers();

        /*
         * The message should have an encrypted, compressed and literal data layer
         */
        assertEquals(3, pgpLayers.size());

        /*
         * First layer
         */
        PGPLayer layerInfo = pgpLayers.get(0);
        assertEquals(1, layerInfo.getParts().size());
        PGPLayerPart layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPEncryptionLayerPart);

        PGPEncryptionLayerPart encryptionLayer = (PGPEncryptionLayerPart) layer;

        assertEquals("Charset: ISO-8859-1,Version: GnuPG v1.4.11 (GNU/Linux),Comment: Using GnuPG with Thunderbird - " +
        		"http://www.enigmail.net/", StringUtils.join(layer.getArmorHeaders(), ","));
        assertEquals("8AAA6718AADEAF7F", PGPUtils.getKeyIDHex(encryptionLayer.getPublicKeyEncryptedData().getKeyID()));
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().isIntegrityProtected());
        assertTrue(encryptionLayer.getPublicKeyEncryptedData().verify());

        /*
         * Second layer
         */
        layerInfo = pgpLayers.get(1);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPCompressionLayerPart);

        PGPCompressionLayerPart compressionLayer = (PGPCompressionLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals(PGPCompressionAlgorithm.ZLIB, PGPCompressionAlgorithm.fromTag(
                compressionLayer.getCompressedData().getAlgorithm()));

        /*
         * Third layer
         */
        layerInfo = pgpLayers.get(2);
        assertEquals(1, layerInfo.getParts().size());
        layer = layerInfo.getParts().get(0);

        assertTrue(layer instanceof PGPLiteralLayerPart);

        PGPLiteralLayerPart literalInfo = (PGPLiteralLayerPart) layer;

        assertNull(layer.getArmorHeaders());
        assertEquals("", literalInfo.getPGPLiteralData().getFileName());
        assertEquals(PGPLiteralPacketType.TEXT, PGPLiteralPacketType.fromTag(
                literalInfo.getPGPLiteralData().getFormat()));
    }

    @Test
    public void testSignedEncryptedBinary()
    throws Exception
    {
        PGPHandler handler = new PGPHandler(keyPairProvider);

        InputStream input = new FileInputStream(new File(TEST_BASE, "djigzo-whitepaper-signed&encrypted.pdf.gpg"));

        File outputFile = TestUtils.createTempFile(".pdf");

        FileOutputStream output = new FileOutputStream(outputFile);

        try {
            handler.handle(input, output);
        }
        finally {
            output.close();
        }

        /*
         * Check md5 of the output file
         */
        assertEquals("C2027C6930CB39EC7572EFAFAF566BDB", Digests.digestHex(FileUtils.readFileToByteArray(outputFile),
                Digest.MD5));

        List<PGPLayer> pgpLayers = handler.getPGPLayers();

        /*
         * The message should have an encrypted, compressed and signed/literal data layer
         */
        assertEquals(3, pgpLayers.size());

        /*
         * signature/literal layer
         */
        PGPLayer layer = pgpLayers.get(2);
        assertEquals(2, layer.getParts().size());

        PGPLiteralLayerPart literalLayer = (PGPLiteralLayerPart) layer.getParts().get(0);

        PGPSignatureLayerPart signatureLayer = (PGPSignatureLayerPart) layer.getParts().get(1);

        PGPKeyRingEntryProvider keyProvider = new StaticPGPKeyRingEntryProvider(new File(TEST_BASE,
                "info@djigzo.com_0x0C345BCC.asc"));

        PGPContentSignatureVerifier verifier = new PGPContentSignatureVerifierImpl(keyProvider);

        PGPSignatureVerifierResult result = verifier.verify(literalLayer.getLiteralContent(),
                PGPSignatureInspector.getFirstSignature(signatureLayer.getPGPSignatureList()));

        assertTrue(result.isValid());
        assertEquals("94CEE89F0C345BCC", PGPUtils.getKeyIDHex(result.getSigner().getKeyID()));
    }
}
