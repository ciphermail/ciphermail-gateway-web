/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.tools.GPG;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.PrivateKey;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class PGPMIMESignatureBuilderTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    private static final JcePBESecretKeyDecryptorBuilder secretKeyDecryptorBuilder =
            new JcePBESecretKeyDecryptorBuilder();

    private static final JcaPGPKeyConverter keyConverter = new JcaPGPKeyConverter();

    private static PrivateKey privateKey;
    private static PGPPublicKey publicKey;

    @BeforeClass
    public static void beforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        List<PGPSecretKey> secretKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE,
                "pgp/test-multiple-sub-keys.gpg.key"));

        PGPSecretKey secretKey = secretKeys.get(0);

        keyConverter.setProvider(SecurityFactoryBouncyCastle.PROVIDER_NAME);

        privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        publicKey = secretKey.getPublicKey();
    }

    private boolean verifyGPG(MimeMessage signedMessage, StringBuilder output, StringBuilder error)
    throws Exception
    {
        // Clone message to make sure it has been written and additional CR/LF pairs have been removed
        signedMessage = MailUtils.cloneMessage(signedMessage);

        assertTrue(signedMessage.isMimeType("multipart/signed"));

        BodyPart[] parts = PGPMIMEUtils.dissectPGPMIMESigned((Multipart) signedMessage.getContent());

        assertNotNull(parts);

        File signedContent = TestUtils.createTempFile(".eml");

        parts[0].writeTo(new FileOutputStream(signedContent));

        File signature = TestUtils.createTempFile(".asc");

        parts[1].writeTo(new FileOutputStream(signature));

        GPG gpg = new GPG();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

        TeeOutputStream outputTee = new TeeOutputStream(outputStream, System.out);
        TeeOutputStream errorTee = new TeeOutputStream(errorStream, System.err);

        try {
            return gpg.verify(signedContent, signature, outputTee, errorTee);
        }
        finally
        {
            if (output != null) {
                output.append(MiscStringUtils.toStringFromASCIIBytes(outputStream.toByteArray()));
            }

            if (error != null) {
                error.append(MiscStringUtils.toStringFromASCIIBytes(errorStream.toByteArray()));
            }
        }
    }

    private boolean verifyCM(MimeMessage signedMessage, String signerKeyFile)
    throws Exception
    {
        // Clone message to make sure it has been written
        signedMessage = MailUtils.cloneMessage(signedMessage);

        boolean verified = false;

        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TEST_BASE, signerKeyFile), null, null);

        MimeMessage handled = handler.handleMessage(signedMessage);

        if (handled != null) {
            verified = "True".equals(handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        }

        return verified;
    }

    @Test
    public void testPGPSignMultipartBinarySignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    @Test
    public void testPGPSignMultipartTextSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("textmode signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    @Test
    public void testPGPSignTextOnlyBimarySignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    @Test
    public void testPGPSignTextOnlyTextSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/simple-text-message.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("textmode signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    /*
     * Example message that ends with just one CRLF pair. Enigmail does not like multipart email that ends with more
     * then one CRLF pair.
     *
     * This message is signed with a binary signature and fails when validated on Enigmail 1.7 but not on 1.6
     *
     * See http://sourceforge.net/p/enigmail/bugs/329 for a bug report
     */
    @Test
    public void testHtmlAlternativeEndsWithOneCRLFBinarySignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/html-alternative-ends-with-one-crlf.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    /*
     * Example message that ends with just one CRLF pair. Enigmail does not like multipart email that ends with more
     * then one CRLF pair.
     *
     * This message is signed with a text signature which can be validated on Enigmail 1.7
     */
    @Test
    public void testHtmlAlternativeEndsWithOneCRLFTextSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/html-alternative-ends-with-one-crlf.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("textmode signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    /*
     * Example message that ends with multiple CRLF pairs. javamail removes the additional lines after encoding which
     * will then result in a corrupt signature. Also Enigmail does not like multipart email that ends with more then one
     * CRLF pair.
     *
     * Note: the result should be validated with Enigmail by opening the eml file
     */
    @Test
    public void testHtmlAlternativeEndsWithMultipleCRLFsTextSignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-alternative-ends-with-mulitple-crlf.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("textmode signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    /*
     * Example message that ends with multiple CRLF pairs. javamail removes the additional lines after encoding which
     * will then result in a corrupt signature. Also Enigmail does not like multipart email that ends with more then one
     * CRLF pair.
     *
     * Note: the result should be validated with Enigmail by opening the eml file
     */
    @Test
    public void testHtmlAlternativeEndsWithMultipleCRLFsBinarySignature()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE,
                "mail/html-alternative-ends-with-mulitple-crlf.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        MimeMessage signed = builder.sign(message, publicKey, privateKey);

        MailUtils.validateMessage(signed);

        assertTrue(signed.isMimeType("multipart/signed"));

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 1, keyid D80D1572D0486F55"));
        assertTrue(error.contains("gpg: Good signature from \"test multiple sub keys " +
                "(test multiple sub keys) <test@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm rsa2048"));

        assertTrue(verifyCM(signed, "pgp/test-multiple-sub-keys.gpg.asc"));
    }

    @Test
    public void testPGPSignCurve25519()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/curve-25519@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 22, keyid 971322AF21733471"));
        assertTrue(error.contains("Good signature from \"ciphermail Curve 25519 test key <curve-25519@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm ed25519"));

        assertTrue(verifyCM(signed, "pgp/curve-25519@example.com.asc"));
    }

    @Test
    public void testPGPSignNISTP256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid E5AB02E8B807FE20"));
        assertTrue(error.contains("Good signature from \"CipherMail test NIST P-256 <nist-p-256@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm nistp256"));

        assertTrue(verifyCM(signed, "pgp/nist-p-256@example.com.asc"));
    }

    @Test
    public void testPGPSignNISTP384()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        // gpg requires a hash algorithm >= SHA384. If not verification fails
        builder.setHashAlgorithm(PGPHashAlgorithm.SHA384);

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-384@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid 710125A5A5E55957"));
        assertTrue(error.contains("Good signature from \"CipherMail test key nist-p-384 <nist-p-384@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA384, key algorithm nistp384"));

        assertTrue(verifyCM(signed, "pgp/nist-p-384@example.com.asc"));
    }

    @Test
    public void testPGPSignNISTP521()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        // gpg requires a hash algorithm >= SHA512. If not verification fails
        // gpg: ECDSA key 39C26CEC5BEFE0CD requires a 512 bit or larger hash
        builder.setHashAlgorithm(PGPHashAlgorithm.SHA512);

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/nist-p-521@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid 39C26CEC5BEFE0CD"));
        assertTrue(error.contains("Good signature from \"CipherMail test key nist-p-521 <nist-p-521@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm nistp521"));

        assertTrue(verifyCM(signed, "pgp/nist-p-521@example.com.asc"));
    }

    @Test
    public void testPGPSignBrainpool256()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setHashAlgorithm(PGPHashAlgorithm.SHA256);

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-256@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid C33A4DA311820160"));
        assertTrue(error.contains("Good signature from \"CipherMail test key brainpool-p-256 <brainpool-p-256@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm brainpoolP256r1"));

        assertTrue(verifyCM(signed, "pgp/brainpool-p-256@example.com.asc"));
    }

    @Test
    public void testPGPSignBrainpool384()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        // gpg requires hash >= 384
        // ECDSA key 1E64823DF04CCB65 requires a 384 bit or larger hash
        builder.setHashAlgorithm(PGPHashAlgorithm.SHA384);

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-384@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid 1E64823DF04CCB65"));
        assertTrue(error.contains("Good signature from \"CipherMail test key brainpool-p-384 <brainpool-p-384@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA384, key algorithm brainpoolP384r1"));

        assertTrue(verifyCM(signed, "pgp/brainpool-p-384@example.com.asc"));
    }

    @Test
    public void testPGPSignBrainpool512()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        // gpg requires hash >= 512
        // ECDSA key F969CD61623B6BFC requires a 512 bit or larger hash
        builder.setHashAlgorithm(PGPHashAlgorithm.SHA512);

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/brainpool-p-512@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid F969CD61623B6BFC"));
        assertTrue(error.contains("Good signature from \"CipherMail test key brainpool-p-512 <brainpool-p-512@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA512, key algorithm brainpoolP512r1"));

        assertTrue(verifyCM(signed, "pgp/brainpool-p-512@example.com.asc"));
    }

    @Test
    public void testPGPSignSecp256k1()
    throws Exception
    {
        MimeMessage message = MailUtils.loadMessage(new File(TEST_BASE, "mail/normal-message-with-attach.eml"));

        PGPMIMESignatureBuilder builder = new PGPMIMESignatureBuilder();

        builder.setSignatureType(PGPDocumentType.BINARY);

        List<PGPSecretKey> ecKeys = PGPKeyUtils.readSecretKeys(new File(TEST_BASE, "pgp/secp256k1@example.com.key"));

        assertEquals(2, ecKeys.size());

        PGPSecretKey secretKey = ecKeys.get(0);

        assertTrue(secretKey.isSigningKey());

        PrivateKey privateKey = keyConverter.getPrivateKey(secretKey.extractPrivateKey(secretKeyDecryptorBuilder.build(
                "test".toCharArray())));

        MimeMessage signed = builder.sign(message, secretKey.getPublicKey(), privateKey);

        BodyPart signedBlob = ((Multipart)signed.getContent()).getBodyPart(1);

        assertEquals("attachment; filename=\"signature.asc\"", StringUtils.join(
                signedBlob.getHeader("Content-Disposition")));

        MailUtils.validateMessage(signed);

        StringBuilder errorBuilder = new StringBuilder();

        boolean verified = verifyGPG(signed, null, errorBuilder);

        if (!verified) {
            fail();
        }

        String error = errorBuilder.toString();

        assertTrue(error.contains(":signature packet: algo 19, keyid 6BBAEB73B8C5B26C"));
        assertTrue(error.contains("Good signature from \"CipherMail test key secp256k1 <secp256k1@example.com>\""));
        assertTrue(error.contains("binary signature, digest algorithm SHA256, key algorithm secp256k1"));

        assertTrue(verifyCM(signed, "pgp/secp256k1@example.com.asc"));
    }
}
