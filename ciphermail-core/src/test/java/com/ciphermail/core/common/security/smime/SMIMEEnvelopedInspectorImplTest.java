/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.cms.KeyNotFoundException;
import com.ciphermail.core.common.security.keystore.KeyStoreKeyProvider;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SMIMEEnvelopedInspectorImplTest
{
    private static SecurityFactory securityFactory;
    private static KeyStoreKeyProvider keyStoreKeyProvider;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        KeyStore keyStore = loadKeyStore(new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"),
                "test");

        keyStoreKeyProvider = new KeyStoreKeyProvider(keyStore, "test");
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

        // initialize key store
        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    /*
     * Check for some headers which should exist because they were added to the signed or encrypted blob.
     */
    private static void checkForEmbeddedHeaders(MimeMessage message)
    throws Exception
    {
        // the message should contain the signed from, to and subject
        assertEquals("<test@example.com>", message.getHeader("from", ","));
        assertEquals("<test@example.com>", message.getHeader("to", ","));
        assertEquals("normal message with attachment", message.getHeader("subject", ","));
    }

    @Test
    public void testEnveloped()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/encrypted-validcertificate.eml");

        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message,  keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        assertNotNull(decrypted);
        assertTrue(decrypted.isMimeType("multipart/mixed"));

        checkForEmbeddedHeaders(decrypted);

        MailUtils.validateMessage(decrypted);
    }

    @Test(expected = KeyNotFoundException.class)
    public void testOutlook2010MissingSubjKeyIdNoWorkaround()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/outlook2010_cert_missing_subjkeyid.eml");

        KeyStoreKeyProvider keyStoreKeyProvider = new KeyStoreKeyProvider(loadKeyStore(new File(
                TestUtils.getTestDataDir(), "keys/outlook2010_cert_missing_subjkeyid.p12"),
                ""), "");

        keyStoreKeyProvider.setUseOL2010Workaround(false);

        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message, keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        inspector.getContentAsMimeMessage();
    }

    @Test
    public void testOutlook2010MissingSubjKeyIdWorkAround()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/outlook2010_cert_missing_subjkeyid.eml");

        KeyStoreKeyProvider keyStoreKeyProvider = new KeyStoreKeyProvider(loadKeyStore(new File(
                TestUtils.getTestDataDir(), "keys/outlook2010_cert_missing_subjkeyid.p12"),
                ""), "");

        keyStoreKeyProvider.setUseOL2010Workaround(true);

        SMIMEEnvelopedInspector inspector = new SMIMEEnvelopedInspectorImpl(message, keyStoreKeyProvider,
                securityFactory.getNonSensitiveProvider(), securityFactory.getSensitiveProvider());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        MailUtils.validateMessage(decrypted);

        assertNotNull(decrypted);
        assertTrue(decrypted.isMimeType("text/plain"));
        assertNull(decrypted.getSubject());
        assertEquals("Created with Outlook 2010 Beta (14.0.4536.1000)", StringUtils.trim(
                (String)decrypted.getContent()));
    }
}
