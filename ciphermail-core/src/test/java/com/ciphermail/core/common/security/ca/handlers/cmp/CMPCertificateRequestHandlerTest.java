/*
 * Copyright (c) 2021-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.handlers.cmp;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.properties.StandardHierarchicalProperties;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.test.TestProperties;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.impl.auth.SystemDefaultCredentialsProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.security.auth.x500.X500Principal;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Note: This unit test requires EJBCA to be running with a specific setup
 */
class CMPCertificateRequestHandlerTest
{
    private static class TestCertificateRequest implements CertificateRequest
    {
        private X500Principal subject;
        private String email;
        private int keyLength;
        private byte[] data;

        @Override
        public UUID getID() {
            return UUID.fromString("static");
        }

        @Override
        public Date getCreated() {
            return new Date();
        }

        @Override
        public void setSubject(X500Principal subject) {
            this.subject = subject;
        }

        @Override
        public X500Principal getSubject() {
            return subject;
        }

        @Override
        public void setEmail(String email) {
            this.email = email;
        }

        @Override
        public String getEmail() {
            return email;
        }

        @Override
        public void setValidity(int validity) {
        }

        @Override
        public int getValidity() {
            return 100;
        }

        @Override
        public void setSignatureAlgorithm(String SignatureAlgorithm) {
        }

        @Override
        public String getSignatureAlgorithm() {
            return "SHA256WithRSA";
        }

        @Override
        public void setKeyLength(int keyLength) {
            this.keyLength = keyLength;
        }

        @Override
        public int getKeyLength() {
            return keyLength;
        }

        @Override
        public void setCRLDistributionPoint(String CRLDistributionPoint) {
        }

        @Override
        public String getCRLDistributionPoint() {
            return null;
        }

        @Override
        public void setInfo(String info) {
        }

        @Override
        public String getInfo() {
            return null;
        }

        @Override
        public void setData(byte[] data) {
            this.data = data;
        }

        @Override
        public byte[] getData() {
            return data;
        }

        @Override
        public void setIteration(int iteration) {
        }

        @Override
        public int getIteration() {
            return 0;
        }

        @Override
        public void setLastUpdated(Date lastUpdated) {
        }

        @Override
        public Date getLastUpdated() {
            return new Date();
        }

        @Override
        public void setNextUpdate(Date nextUpdate) {
        }

        @Override
        public Date getNextUpdate() {
            return new Date();
        }

        @Override
        public void setLastMessage(String message) {
        }

        @Override
        public String getLastMessage() {
            return null;
        }

        @Override
        public String getCertificateHandlerName() {
            return "CMP";
        }
    }

    private static CMPSettings createCMPSettings()
    {
        Properties properties = new Properties();

        StandardHierarchicalProperties hierarchicalProperties = new StandardHierarchicalProperties(
                null, null, properties);

        CMPSettings settings = CMPSettingsImpl.getInstance(hierarchicalProperties);

        settings.setIssuerDN("CN=SMIME");
        settings.setMessageProtectionPassword("test");
        settings.setCMPServiceURL(TestProperties.getEJBCAServerURL() + "/ejbca/publicweb/cmp/smime");

        return settings;
    }

    private static CMPCertificateRequestHandler createCMPCertificateRequestHandler(CMPSettings settings)
    {
        return new CMPCertificateRequestHandler(
                () -> settings,
                new CloseableHttpAsyncClientFactoryImpl(null),
                new HttpClientContextFactoryImpl(new SystemDefaultCredentialsProvider()));
    }

    private static TestCertificateRequest createCertificateRequest()
    throws Exception
    {
        X500PrincipalBuilder principalBuilder = X500PrincipalBuilder.getInstance();

        principalBuilder.setCommonName("test");
        principalBuilder.setEmail("test@example.com");

        TestCertificateRequest certificateRequest = new TestCertificateRequest();

        certificateRequest.setSubject(principalBuilder.buildPrincipal());
        certificateRequest.setKeyLength(2048);

        return certificateRequest;
    }

    @Test
    void testRequestCertificate()
    throws Exception
    {
        CMPSettings settings = createCMPSettings();

        CMPCertificateRequestHandler requestHandler = createCMPCertificateRequestHandler(settings);

        TestCertificateRequest certificateRequest = createCertificateRequest();

        KeyAndCertificate keyAndCertificate = requestHandler.handleRequest(certificateRequest);

        Assertions.assertNotNull(keyAndCertificate);
    }

    @Test
    void testRequestCertificateInvalidURL()
    throws Exception
    {
        CMPSettings settings = createCMPSettings();

        settings.setCMPServiceURL(TestProperties.getEJBCAServerURL() + "/ejbca/publicweb/cmp/other");

        CMPCertificateRequestHandler requestHandler = createCMPCertificateRequestHandler(settings);

        TestCertificateRequest certificateRequest = createCertificateRequest();

        try {
            requestHandler.handleRequest(certificateRequest);

            Assertions.fail();
        }
        catch (CAException e) {
            Assertions.assertEquals("CMP response error. Status: 404, Reason: Not Found", e.getMessage());
        }
    }

    @Test
    void testRequestCertificateIncorrectProtectionPassword()
    throws Exception
    {
        CMPSettings settings = createCMPSettings();

        settings.setMessageProtectionPassword("invalid");

        CMPCertificateRequestHandler requestHandler = createCMPCertificateRequestHandler(settings);

        TestCertificateRequest certificateRequest = createCertificateRequest();

        try {
            requestHandler.handleRequest(certificateRequest);

            Assertions.fail();
        }
        catch (CAException e) {
            Assertions.assertEquals("CMP request failed. See logs for more information", e.getMessage());
        }
    }

    @Test
    void testRequestCertificateMultithreaded()
    throws Exception
    {
        CMPSettings settings = createCMPSettings();

        CMPCertificateRequestHandler requestHandler = createCMPCertificateRequestHandler(settings);

        TestCertificateRequest certificateRequest = createCertificateRequest();

        int nrOfRequests = 500;

        final AtomicInteger msgCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfRequests);

        final Thread mainThread = Thread.currentThread();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfRequests; i++)
            {
                executorService.execute(() ->
                {
                    try {
                        System.out.println("CMP Request: " + msgCounter.incrementAndGet() + ". Thread: " +
                                Thread.currentThread().getName());

                        KeyAndCertificate keyAndCertificate = requestHandler.handleRequest(certificateRequest);

                        Assertions.assertNotNull(keyAndCertificate);
                    }
                    catch (CAException  e) {
                        // Stop main thread from waiting
                        mainThread.interrupt();

                        throw new UnhandledException(e);
                    }
                    finally {
                        countDownLatch.countDown();
                    }
                });
            }

            Assertions.assertTrue(countDownLatch.await(30, TimeUnit.SECONDS), "Timeout");
            Assertions.assertEquals(nrOfRequests, msgCounter.get());
        }
        finally {
            executorService.shutdown();
        }
    }
}
