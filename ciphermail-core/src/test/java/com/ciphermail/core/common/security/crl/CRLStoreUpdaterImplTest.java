/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CRLStoreUpdaterImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.CERT_STORE_SERVICE_NAME)
    private X509CertStoreExt certStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private X509CRLStoreExt crlStore;

    @Autowired
    private PKISecurityServices pKISecurityServices;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                certStore.removeAllEntries();
                rootStore.removeAllEntries();
                crlStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificates(File file, X509CertStoreExt certStore)
    throws Exception
    {
        Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(file);

        for (Certificate certificate : certificates)
        {
            if (certificate instanceof X509Certificate x509Certificate)
            {
                if (!certStore.contains(x509Certificate)) {
                    certStore.addCertificate(x509Certificate);
                }
            }
        }
    }

    private List<CRLDownloadHandler> createDownloadHandlers()
    {
        List<CRLDownloadHandler> downloadHandlers = new LinkedList<>();

        downloadHandlers.add(new HTTPCRLDownloadHandler(new CloseableHttpAsyncClientFactoryImpl(
                new HTTPClientStaticProxyProvider()),
                new HttpClientContextFactoryImpl(new BasicCredentialsProvider())));

        downloadHandlers.add(new LDAPCRLDownloadHandler());

        return downloadHandlers;
    }

    @Test
    public void testUpdateCRLStoreIllegalCRLDistPoint()
    {
        CRLStoreUpdaterParameters updaterParams = transactionOperations.execute(status ->
        {
            List<CRLDownloadHandler> downloadHandlers = createDownloadHandlers();

            try {
                addCertificates(new File(TestUtils.getTestDataDir(),
                    "certificates/certeurope_root_ca_illegal_crl_dist_point.crt"), rootStore);

                CRLDownloader crlDownloader = new CRLDownloaderImpl(downloadHandlers);

                CRLStoreMaintainer crlStoreMaintainer = new CRLStoreMaintainerImpl(pKISecurityServices.getCRLStore(),
                        pKISecurityServices.getCRLPathBuilderFactory(), false);

                DefaultCRLStoreUpdaterParametersBuilder paramsBuilder = new DefaultCRLStoreUpdaterParametersBuilder(
                        pKISecurityServices, crlDownloader, crlStoreMaintainer);

                return paramsBuilder.createCRLStoreUpdaterParameters();

            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
            finally {
                for (CRLDownloadHandler handler : downloadHandlers)
                {
                    if (handler instanceof HTTPCRLDownloadHandler) {
                        ((HTTPCRLDownloadHandler)handler).shutdown();
                    }
                }
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            CRLStoreUpdater crlUpdater = new CRLStoreUpdaterImpl(updaterParams, transactionOperations);

            // this should not result in an exception
            try {
                crlUpdater.update();
            }
            catch (CRLStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
