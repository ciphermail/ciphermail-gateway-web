/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HTTPClientProxyProvider;
import com.ciphermail.core.common.http.HTTPClientStaticProxyProvider;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.security.bouncycastle.InitializeBouncycastle;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.util.FileConstants;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.SimpleSocketServer;
import com.ciphermail.core.test.SocketAcceptEvent;
import com.ciphermail.core.test.TestProperties;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.core5.http2.HttpVersionPolicy;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HTTPCRLDownloadHandlerTest
{
    private static final int SERVER_PORT = 61113;

    private int tempFileCount;

    private static SimpleSocketServer server;

    private static class RunawaySocketHandler implements Runnable
    {
        private final Socket socket;
        private volatile boolean stop;

        public RunawaySocketHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run()
        {
            try {
                while (!stop) {
                    OutputStream output = socket.getOutputStream();

                    ThreadUtils.sleepQuietly(1000);

                    output.write(new byte[]{0});
                    output.flush();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        void stop() {
            stop = true;
        }
    }

    private static class RunnawaySocketAcceptEvent implements SocketAcceptEvent
    {
        RunawaySocketHandler runawaySocketHandler;
        Thread acceptThread;

        @Override
        public void accept(Socket socket)
        {
            runawaySocketHandler = new RunawaySocketHandler(socket);

            acceptThread = new Thread(runawaySocketHandler);

            acceptThread.setDaemon(true);
            acceptThread.start();
        }

        void stop() {
            if (runawaySocketHandler != null) {
                runawaySocketHandler.stop();
            }
        }
    }

    private static void startSimpleSocketServer()
    {
        server = new SimpleSocketServer(SERVER_PORT);

        Thread serverThread = new Thread(server);
        serverThread.setDaemon(true);
        serverThread.start();

        while(!server.isRunning()) {
            ThreadUtils.sleepQuietly(1000);
        }
    }

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        InitializeBouncycastle.initialize();

        startSimpleSocketServer();
    }

    @AfterClass
    public static void setUpAfterClass() {
        server.stop();
    }

    @Before
    public void before()
    {
        // Clear interrupted status
        Thread.interrupted();

        // get the current nr of temp files
        tempFileCount = TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp");
    }

    @After
    public void after()
    {
        // Clear interrupted status
        Thread.interrupted();

        // Check if we have any temp file leakage
        assertEquals(tempFileCount, TestUtils.getTempFileCount(FileConstants.TEMP_FILE_PREFIX, ".tmp"));
    }

    private HTTPCRLDownloadHandler createHTTPCRLDownloadHandler(HTTPClientProxyProvider proxyProvider,
            HttpVersionPolicy versionPolicy, Long responseTimeout)
    {
        return createHTTPCRLDownloadHandler(proxyProvider, versionPolicy, responseTimeout,
                null, null);
    }

    private HTTPCRLDownloadHandler createHTTPCRLDownloadHandler(HTTPClientProxyProvider proxyProvider,
            HttpVersionPolicy versionPolicy, Long responseTimeout, String username, String password)
    {
        CloseableHttpAsyncClientFactoryImpl httpClientFactory = new CloseableHttpAsyncClientFactoryImpl(proxyProvider);

        if (versionPolicy != null) {
            httpClientFactory.setVersionPolicy(versionPolicy);
        }

        BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        if (username != null) {
            credentialsProvider.setCredentials(new AuthScope(null, -1),
                    new UsernamePasswordCredentials(username, password.toCharArray()));
        }

        HttpClientContextFactoryImpl httpClientContextFactory = new HttpClientContextFactoryImpl(
                credentialsProvider);

        if (responseTimeout != null) {
            httpClientContextFactory.setResponseTimeout(responseTimeout);
        }

        return new HTTPCRLDownloadHandler(httpClientFactory, httpClientContextFactory);
    }

    private HTTPCRLDownloadHandler createHTTPCRLDownloadHandler()  {
        return createHTTPCRLDownloadHandler(new HTTPClientStaticProxyProvider(), null, null,
                "test", "test");
    }

    @Test(timeout=10000)
    public void testHTTPCRLDownloadHandlerTimeout()
    throws Exception
    {
        RunnawaySocketAcceptEvent runnawaySocketAcceptEvent = new RunnawaySocketAcceptEvent();

        server.setIncomingEvent(runnawaySocketAcceptEvent);

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        downloadHandler.setTotalTimeout(4000);

        URI uri = new URI("http://127.0.0.1:" + SERVER_PORT);

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch(IOException e) {
            // Expected
            assertEquals("A timeout has occurred while downloading CRL from: http://127.0.0.1:61113", e.getMessage());
        }
        finally {
            runnawaySocketAcceptEvent.stop();
            downloadHandler.shutdown();
        }
    }

    @Test(timeout=10000)
    public void testHTTPCRLDownloadHandlerTimeoutNotListening()
    throws Exception
    {
        RunnawaySocketAcceptEvent runnawaySocketAcceptEvent = new RunnawaySocketAcceptEvent();

        server.setIncomingEvent(runnawaySocketAcceptEvent);

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        downloadHandler.setTotalTimeout(4000);

        URI uri = new URI("http://127.0.0.1:65535");

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch(IOException e) {
            // Expected
            assertEquals("org.apache.hc.client5.http.HttpHostConnectException: Connect to http://127.0.0.1:65535 "
                    + "[/127.0.0.1] failed: Connection refused", e.getMessage());
        }
        finally {
            runnawaySocketAcceptEvent.stop();
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerResponseTimeout()
    throws Exception
    {
        SocketAcceptEvent event = socket -> {};

        server.setIncomingEvent(event);

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler(null, null, 2000L);

        URI uri = new URI("http://127.0.0.1:" + SERVER_PORT);

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch(IOException e) {
            assertEquals("java.net.SocketTimeoutException: 2000 MILLISECONDS", e.getMessage());
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerPerformance()
    throws Exception
    {
        URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/test-ca-no-next-update.crl");

        int repeat = 10000;

        long start = System.currentTimeMillis();

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        try {
            for (int i = 0; i < repeat; i++)
            {
                System.out.println(i);

                Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);
                assertEquals(1, crls.size());
            }
        }
        finally {
            downloadHandler.shutdown();
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("downloads/sec: " + perSecond);

        double expected = 100 * TestProperties.getTestPerformanceFactor();

        // NOTE: !!! can fail on a slower system
        assertTrue("Downloads per second too slow. Expected > " + expected + " but was " + perSecond +
                " !!! this can fail on a slower system !!!", perSecond > expected);
    }

    @Test
    public void testHTTPCRLDownloadHandlerSizeLimitExceeded()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        downloadHandler.setMaxCRLSize(SizeUtils.KB * 10);

        URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/dod-large.crl");

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch(IOException e) {
            assertTrue(ExceptionUtils.getStackTrace(e), ExceptionUtils.getRootCause(e) instanceof LimitReachedException);
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerSizeLimitExceededRepeat()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        try {
            downloadHandler.setMaxCRLSize(SizeUtils.KB * 10);

            URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/dod-large.crl");

            for (int i = 0; i < 10000; i++)
            {
                System.out.println(i);

                try {
                    downloadHandler.downloadCRLs(uri);

                    fail();
                }
                catch(IOException e) {
                    assertTrue(ExceptionUtils.getStackTrace(e), ExceptionUtils.getRootCause(e) instanceof LimitReachedException);
                }
            }
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerUnknownURL()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        URI uri = new URI("http://unkownhost.ciphermail.net");

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch(IOException e) {
            assertTrue(ExceptionUtils.getStackTrace(e), ExceptionUtils.getRootCause(e) instanceof UnknownHostException);
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerNoCRL()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/documents/test.txt");

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch (CRLException e) {
            assertEquals("java.io.IOException: malformed PEM data: no header found", e.getMessage());
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerViaProxy()
    throws Exception
    {
        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler(proxyProvider, null,
                null, "test", "test");

        try {
            // Because we are connecting via the proxy we need to specify the hostname of the httpd docker container
            // and the local port (80)
            URI uri = new URI(TestProperties.getHTTPDownloadServerProxyURL() + "/testdata/crls/dod-large.crl");

            Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);

            assertEquals(1, crls.size());

            assertEquals("87461E6F4F1BA6B7732567276B072D2574AC72DC",Digests.digestHex(
                    ((X509CRL)crls.iterator().next()).getEncoded(), Digest.SHA1));
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerViaProxyInvalidCredentials()
    throws Exception
    {
        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler(proxyProvider, null,
                null, "test", "invalid");

        // Because we are connecting via the proxy we need to specify the hostname of the httpd docker container
        // and the local port (80)
        URI uri = new URI(TestProperties.getHTTPDownloadServerProxyURL() + "/testdata/crls/dod-large.crl");

        try {
            downloadHandler.downloadCRLs(uri);

            fail();
        }
        catch (IOException e) {
            assertEquals("Connection failed. HTTP status code: 407", e.getMessage());
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerDefaultHttpVersionPolicy()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        try {
            URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/dod-large.crl");

            Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);

            assertEquals(1, crls.size());

            assertEquals("87461E6F4F1BA6B7732567276B072D2574AC72DC",Digests.digestHex(
                    ((X509CRL)crls.iterator().next()).getEncoded(), Digest.SHA1));
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerDefaultHttp1VersionPolicy()
    throws Exception
    {
        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler(null, HttpVersionPolicy.FORCE_HTTP_1,
                null);

        try {
            URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/dod-large.crl");

            Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);

            assertEquals(1, crls.size());

            assertEquals("87461E6F4F1BA6B7732567276B072D2574AC72DC",Digests.digestHex(
                    ((X509CRL)crls.iterator().next()).getEncoded(), Digest.SHA1));
        }
        finally {
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerMultithreaded()
    throws Exception
    {
        URI uri = new URI(TestProperties.getHTTPDownloadServerURL() + "/testdata/crls/test-ca-no-next-update.crl");

        int nrOfDownloads = 50000;

        final AtomicInteger downloadCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfDownloads);

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfDownloads; i++)
            {
                executorService.execute(() ->
                {
                    try {
                        System.out.println("Download: " + downloadCounter.incrementAndGet() + ". Thread: " +
                                Thread.currentThread().getName());

                        Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);

                        assertEquals(1, crls.size());

                        countDownLatch.countDown();
                    }
                    catch (IOException | CRLException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
        }
        finally {
            executorService.shutdown();
            downloadHandler.shutdown();
        }
    }

    @Test
    public void testHTTPCRLDownloadHandlerMultithreadedViaProxy()
    throws Exception
    {
        HTTPClientStaticProxyProvider proxyProvider = new HTTPClientStaticProxyProvider();

        proxyProvider.setProxyURI(new URI(TestProperties.getHTTPProxyURL()));

        // Because we are connecting via the proxy we need to specify the hostname of the httpd docker container
        // and the local port (80)
        URI uri = new URI(TestProperties.getHTTPDownloadServerProxyURL() + "/testdata/crls/test-ca-no-next-update.crl");

        int nrOfDownloads = 50000;

        final AtomicInteger downloadCounter = new AtomicInteger();

        final CountDownLatch countDownLatch = new CountDownLatch(nrOfDownloads);

        final Thread mainThread = Thread.currentThread();

        HTTPCRLDownloadHandler downloadHandler = createHTTPCRLDownloadHandler(proxyProvider, null,
                null, "test", "test");

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < nrOfDownloads; i++)
            {
                executorService.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try {
                            Collection<? extends CRL> crls = downloadHandler.downloadCRLs(uri);

                            if (crls.size() != 1) {
                                throw new RuntimeException("CRL size expected 1 but was " + crls.size());
                            }

                            System.out.println("Downloaded: " + downloadCounter.incrementAndGet() + ". Thread: " +
                                    Thread.currentThread().getName());
                        }
                        catch (Exception e)
                        {
                            // Stop main thread from waiting
                            mainThread.interrupt();

                            throw new RuntimeException(e);
                        }
                        finally {
                            countDownLatch.countDown();
                        }
                    }
                });
            }

            assertTrue("Timeout", countDownLatch.await(30, TimeUnit.SECONDS));
            assertEquals(nrOfDownloads, downloadCounter.get());
        }
        finally {
            // Clear interrupted status
            Thread.interrupted();
            executorService.shutdown();
            downloadHandler.shutdown();
        }
    }
}
