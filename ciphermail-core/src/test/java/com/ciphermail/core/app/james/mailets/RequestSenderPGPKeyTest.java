/*
 * Copyright (c) 2014-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClient;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSearchResult;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.keyserver.KeyServerKeyInfo;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RequestSenderPGPKeyTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private PGPKeyRing keyring;

    @Autowired
    private KeyServerClient keyServerClient;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Clean KeyRing
                keyring.deleteAll();

                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                setGlobalProperty("pgp-auto-publish-keys", "true");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        try {
            UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

            UserProperties properties = preferences.getProperties();

            properties.setProperty(propertyName, value);
        }
        catch (Exception e) {
            throw new UnhandledException(e);
        }
    }

    private long getKeyRingSize()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyring.getSize();
            }
            catch (IOException e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    private long getUserCount()
    {
        return transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUserCount();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }).longValue();
    }

    private boolean isUser(String email)
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    private KeyServerClientSearchResult searchKeys(String query)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return keyServerClient.searchKeys(query, true, Integer.MAX_VALUE);
            }
            catch (IOException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String from, String sender, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress(from));

        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender(sender)
                .mimeMessage(message)
                .build();
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new RequestSenderPGPKey();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testRequestKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        String from = Long.toString(System.currentTimeMillis()) + "@example.com";

        Mail mail = createMail(from, "sender@example.com", "recipient@example.com");

        assertEquals(0, getKeyRingSize());

        assertEquals(0, getUserCount());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "A new PGP key was created for " + from + " with Key ID"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The new PGP key was submitted to the key server"));

        assertEquals(0, getUserCount());

        // A master and sub key should be created
        assertEquals(2, getKeyRingSize());

        KeyServerClientSearchResult searchResult = searchKeys(from);

        assertNotNull(searchResult);

        List<KeyServerKeyInfo> keys = searchResult.getKeys();

        assertNotNull(keys);

        assertEquals(1, keys.size());

        KeyServerKeyInfo key = keys.get(0);

        assertEquals("<" + from + ">", StringUtils.join(key.getUserIDs(), ","));
    }

    @Test
    public void testAddUser()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("addUser", "true")
                .build();

        String from = Long.toString(System.currentTimeMillis()) + "@example.com";

        Mail mail = createMail(from, "sender@example.com", "recipient@example.com");

        assertEquals(0, getKeyRingSize());

        assertEquals(0, getUserCount());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "A new PGP key was created for " + from + " with Key ID"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The new PGP key was submitted to the key server"));

        assertEquals(1, getUserCount());

        assertTrue(isUser(from));

        // A master and sub key should be created
        assertEquals(2, getKeyRingSize());

        KeyServerClientSearchResult searchResult = searchKeys(from);

        assertNotNull(searchResult);

        List<KeyServerKeyInfo> keys = searchResult.getKeys();

        assertNotNull(keys);

        assertEquals(1, keys.size());

        KeyServerKeyInfo key = keys.get(0);

        assertEquals("<" + from + ">", StringUtils.join(key.getUserIDs(), ","));
    }

    @Test
    public void testInvalidFrom()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        String from = "invalid";

        Mail mail = createMail(from, "sender@example.com", "recipient@example.com");

        assertEquals(0, getKeyRingSize());

        assertEquals(0, getUserCount());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, getUserCount());

        // A master and sub key should be created
        assertEquals(0, getKeyRingSize());
    }

    @Test
    public void testRequestExistingKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        String from = Long.toString(System.currentTimeMillis()) + "@example.com";

        Mail mail = createMail(from, "sender@example.com", "recipient@example.com");

        assertEquals(0, getKeyRingSize());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // A master and sub key should be created
        assertEquals(2, getKeyRingSize());

        KeyServerClientSearchResult searchResult = searchKeys(from);

        assertNotNull(searchResult);

        List<KeyServerKeyInfo> keys = searchResult.getKeys();

        assertNotNull(keys);

        assertEquals(1, keys.size());

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        // Request a new key for the same email address
        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // No new key should have been created
        assertEquals(2, getKeyRingSize());

        searchResult = searchKeys(from);

        assertNotNull(searchResult);

        keys = searchResult.getKeys();

        assertNotNull(keys);

        assertEquals(1, keys.size());
    }

    @Test
    public void testRequestKeyMultiThreaded()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals(0, getKeyRingSize());

        Callable<ObjectUtils.Null> callable = () ->
        {
            Mail mail1 = createMail("sender1@example.com", "sender1@example.com",
                    "recipient@example.com");
            mailet.service(mail1);

            Mail mail2 = createMail("sender2@example.com", "sender2@example.com",
                    "recipient@example.com");
            mailet.service(mail2);

            return null;
        };

        Collection<Future<?>> futures = new LinkedList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < 20; i++) {
                futures.add(executorService.submit(callable));
            }

            for (Future<?> future : futures) {
                future.get(30000, TimeUnit.MILLISECONDS);
            }
        }
        finally {
            executorService.shutdown();
        }

        checkLogsForErrorsOrExceptions();

        // Keys (master and sub key) for from1@example.com and from2@example.com should have been created
        assertEquals(4, getKeyRingSize());
    }

    @Test
    public void testRequestKeyRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        RequestSenderPGPKey mailet = new RequestSenderPGPKey()
        {
            @Override
            protected void onHandleUserEvent(User user)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        String from = Long.toString(System.currentTimeMillis()) + "@example.com";

        Mail mail = createMail(from, "sender@example.com", "recipient@example.com");

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The new PGP key was submitted to the key server"));

        // A master and sub key should be created
        assertEquals(2, getKeyRingSize());

        KeyServerClientSearchResult searchResult = searchKeys(from);

        assertNotNull(searchResult);

        List<KeyServerKeyInfo> keys = searchResult.getKeys();

        assertNotNull(keys);

        assertEquals(1, keys.size());

        KeyServerKeyInfo key = keys.get(0);

        assertEquals("<" + from + ">", StringUtils.join(key.getUserIDs(), ","));
    }
}
