/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class AddSecurityInfoTagsTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    private MimeMessage message;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @Before
    public void setup()
    throws Exception
    {
        message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (MessagingException | HierarchicalPropertiesException  e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new AddSecurityInfoTags();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testUserProperties()
    throws Exception
    {
        setUserProperty("test@example.com", "security-info-decrypted-tag", "[Xdecrypted]");
        setUserProperty("test@example.com", "security-info-signed-valid-tag", "[Xvalid]");
        setUserProperty("test@example.com", "security-info-signed-by-valid-tag", "[Xsigned By: %s]");
        setUserProperty("test@example.com", "security-info-signed-invalid-tag", "[Xinvalid]");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        SecurityInfoTags tags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);

        assertNotNull(tags);

        assertEquals("[Xdecrypted]", tags.getDecryptedTag());
        assertEquals("[Xvalid]", tags.getSignedValidTag());
        assertEquals("[Xsigned By: %s]", tags.getSignedByValidTag());
        assertEquals("[Xinvalid]", tags.getSignedInvalidTag());
    }

    @Test
    public void testInherited()
    throws Exception
    {
        setUserProperty("test@example.com", "security-info-decrypted-tag", "[123]");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        SecurityInfoTags tags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);

        assertNotNull(tags);

        assertEquals("[123]", tags.getDecryptedTag());
        assertEquals("[signed]", tags.getSignedValidTag());
        assertEquals("[signed by: %s]", tags.getSignedByValidTag());
        assertEquals("[invalid signature!]", tags.getSignedInvalidTag());
    }

    @Test
    public void testNoUser()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        SecurityInfoTags tags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);

        assertNotNull(tags);

        assertEquals("[decrypted]", tags.getDecryptedTag());
        assertEquals("[signed]", tags.getSignedValidTag());
        assertEquals("[signed by: %s]", tags.getSignedByValidTag());
        assertEquals("[invalid signature!]", tags.getSignedInvalidTag());
    }

    @Test
    public void testRetry()
    throws Exception
    {
        MutableBoolean constraintViolationThrown = new MutableBoolean();

        setUserProperty("test@example.com", "security-info-decrypted-tag", "[Xdecrypted]");
        setUserProperty("test@example.com", "security-info-signed-valid-tag", "[Xvalid]");
        setUserProperty("test@example.com", "security-info-signed-by-valid-tag", "[Xsigned By: %s]");
        setUserProperty("test@example.com", "security-info-signed-invalid-tag", "[Xinvalid]");

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = new AddSecurityInfoTags()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException
            {
                super.serviceMailTransacted(mail);

                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        assertFalse(constraintViolationThrown.booleanValue());

        mailet.service(mail);

        assertTrue(constraintViolationThrown.booleanValue());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        SecurityInfoTags tags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);

        assertNotNull(tags);

        assertEquals("[Xdecrypted]", tags.getDecryptedTag());
        assertEquals("[Xvalid]", tags.getSignedValidTag());
        assertEquals("[Xsigned By: %s]", tags.getSignedByValidTag());
        assertEquals("[Xinvalid]", tags.getSignedInvalidTag());
    }
}
