/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPropertiesSenderSubjectTriggerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void addUser(String email, String trigger, Boolean removePattern, Boolean enabled)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                String basePropertyName = "b";

                User user = userWorkflow.addUser(email);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                if (trigger != null) {
                    userProperties.setProperty(basePropertyName + ".trigger", trigger);
                }

                if (removePattern != null)
                {
                    userProperties.setProperty(basePropertyName + ".removePattern",
                            BooleanUtils.toStringTrueFalse(removePattern));
                }


                if (enabled != null)
                {
                    userProperties.setProperty(basePropertyName + ".enabled",
                            BooleanUtils.toStringTrueFalse(enabled));
                }

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                user.getUserPreferences().getProperties().setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String from, String subject, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject(subject);
        message.setFrom(from != null ? new InternetAddress(from) : null);
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new UserPropertiesSenderSubjectTrigger();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testSubjectTrigger()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123",
                "someuser@example.com", "someuser2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertTrue(result.contains(new MailAddress("someuser2@example.com")));
        assertEquals("test encrypt 123", mail.getMessage().getSubject());
    }

    @Test
    public void testRemovePattern()
    throws Exception
    {
        addUser("test@example.com", "encrypt", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());
    }

    @Test
    public void testRegExp()
    throws Exception
    {
        addUser("test@example.com", "(?i)\\s*ENCRYPT\\s*", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test123", mail.getMessage().getSubject());
    }

    @Test
    public void testSubjectTriggerQuoteRegExpr()
    throws Exception
    {
        addUser("test@example.com", "\\Q|^@*test\\E", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test \\Q|^@\\Q|^@*test\\E*test\\E 123 ",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test \\Q|^@\\Q\\E*test\\E 123", mail.getMessage().getSubject());
    }

    @Test
    public void testNoMatchingProperty()
    throws Exception
    {
        addUser("test@example.com", null, null, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'X.trigger', " +
                           "'removePatternProperty':'X.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testInvalidRegExp()
    throws Exception
    {
        addUser("test@example.com", "(?i", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "The trigger '(?i' is not a valid pattern. Matcher will not match."));

        assertEquals(0, result.size());
    }

    @Test
    public void testDefaultValues()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test encrypt 123", mail.getMessage().getSubject());
    }

    @Test
    public void testNullTrigger()
    throws Exception
    {
        addUser("test@example.com", null, true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        //checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "WARN  Missing factory property for property b.trigger"));

        assertEquals(0, result.size());
    }

    @Test
    public void testNullSubject()
    throws Exception
    {
        addUser("test@example.com", "test", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", null, "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testEnabledProperty()
    throws Exception
    {
        // first test without setting the enabled property value (default should be false)
        addUser("test@example.com", "encrypt", false, null);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());

        // Now with setting the property to true
        setUserProperty("test@example.com", "b.enabled", "true");

        createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test encrypt 123", mail.getMessage().getSubject());

        // Now with enabled false
        setUserProperty("test@example.com", "b.enabled", "false");

        createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testLogMatch()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', 'logOnMatch':'MACTH {}', " +
                           "'logOnNoMatch':'NOMACTH {}', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "123");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "MACTH 123"));

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
    }

    @Test
    public void testLogNoMatch()
    throws Exception
    {
        addUser("test@example.com", "nomatch", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', 'logOnMatch':'MACTH {}', " +
                           "'logOnNoMatch':'NOMATCH {}', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "123");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "NOMATCH 123"));

        assertEquals(0, result.size());
    }

    @Test
    public void testLogOnDisabled()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        setUserProperty("test@example.com", "b.enabled", "false");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger',"
                           + "'enabledProperty':'b.enabled', 'logOnDisabled':'Disabled {}'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "123");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Disabled 123"));

        assertEquals(0, result.size());
    }

    @Test
    public void testLogOnDisabledEmptyTrigger()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'NO-TRIGGER',"
                           + "'logOnDisabled':'Disabled {}', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test encrypt 123", "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "123");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    /*
     * Test nested triggers.
     *
     * example:
     *
     * to test that enencryptcrypt does not result in encrypt after remove
     */
    @Test
    public void testSubjectTriggerNested()
    throws Exception
    {
        addUser("test@example.com", "encrypt", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test enencryptcrypt 123",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals("test  123", mail.getMessage().getSubject());

        // test whether the original subject mail attribute is set and that it's correct
        assertEquals("test enencryptcrypt 123", CoreApplicationMailAttributes.getOriginalSubject(mail));
    }

    /*
     * Test nested triggers max recursion depth.
     *
     * example:
     *
     * to test that enencryptcrypt does not result in encrypt after remove
     */
    @Test
    public void testSubjectTriggerNestedMaxRecursion()
    throws Exception
    {
        addUser("test@example.com", "encrypt", true, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", "test enenenencryptcryptcryptcrypt 123",
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Subject removed. Max recursion level reached."));

        assertEquals(1, result.size());

        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertEquals(SubjectTrigger.MAX_SUBJECT_RECURSIVE_DEPTH_REACHED, mail.getMessage().getSubject());
    }

    @Test
    public void testNullOriginator()
    throws Exception
    {
        addUser("test@example.com", "encrypt", false, true);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'b.trigger', " +
                           "'removePatternProperty':'b.removePattern', 'enabledProperty':'b.enabled'}")
                .build();

        Mail mail = createMail(null, "test encrypt 123", "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }
}
