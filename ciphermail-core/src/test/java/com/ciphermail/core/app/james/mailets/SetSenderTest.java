/*
 * Copyright (c) 2017-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import org.apache.james.core.MaybeSender;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class SetSenderTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mail createMail(String sender, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));

        message.saveChanges();

        // we need to return MailImpl because SetSender requires that in order to change the sender
        MailImpl.Builder builder = MailImpl.builder().name(MailImpl.getId())
                .addRecipients(recipients)
                .mimeMessage(message)
                .state("initial");

        if (sender != null) {
            builder.sender(sender);
        }

        return builder.build();
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SetSender();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testSetSender()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("sender", "info@ciphermail.com")
                .build();

        Mail mail = createMail("sender@example.com", "test1@example.com", "test2@example.com");

        assertEquals("sender@example.com", mail.getMaybeSender().asString());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, mail.getRecipients().size());
        assertEquals("info@ciphermail.com", mail.getMaybeSender().asString());
    }

    @Test
    public void testSetSenderNullSender()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("sender", "info@ciphermail.com")
                .build();

        Mail mail = createMail(null, "test1@example.com", "test2@example.com");

        assertEquals(MaybeSender.nullSender(), mail.getMaybeSender());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, mail.getRecipients().size());
        assertEquals("info@ciphermail.com", mail.getMaybeSender().asString());
    }

    @Test
    public void testSetNullSender()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .build();

        Mail mail = createMail("info@ciphermail.com", "test1@example.com", "test2@example.com");

        assertEquals("info@ciphermail.com", mail.getMaybeSender().asString());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, mail.getRecipients().size());
        assertEquals(MaybeSender.nullSender(), mail.getMaybeSender());
    }

    @Test
    public void testSetNullSenderExplicit()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("sender", "")
                .build();

        Mail mail = createMail("info@ciphermail.com", "test1@example.com", "test2@example.com");

        assertEquals("info@ciphermail.com", mail.getMaybeSender().asString());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, mail.getRecipients().size());
        assertEquals(MaybeSender.nullSender(), mail.getMaybeSender());
    }
}
