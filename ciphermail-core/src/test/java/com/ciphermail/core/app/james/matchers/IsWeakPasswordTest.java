/*
 * Copyright (c) 2021-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class IsWeakPasswordTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        Configurator.setLevel(IsWeakPassword.class, Level.DEBUG);
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void setSenderPasswordPolicy(String email, String passwordPolicy)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setPasswordPolicy(passwordPolicy);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    // version which allows us to set an invalid password policy
    private void setSenderPasswordPolicyRaw(String email, String passwordPolicy)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty("password-policy", passwordPolicy);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String from, String subject, String... recipient)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setSubject(subject);
        message.setContent("test", "text/plain");
        message.setFrom(from != null ? new InternetAddress(from) : null);

        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipient)
                .sender("sender@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new IsWeakPassword();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testMatchMultiple()
    throws Exception
    {
        // Only the sender policy should be taken into account
        setSenderPasswordPolicy("test@example.com", "{r:[{p:'^.{3,}$'}]}");
        setSenderPasswordPolicy("test1@example.com", "{r:[{p:'^.{1,}$'}]}");
        setSenderPasswordPolicy("test2@example.com", "{r:[{p:'^.{40,}$'}]}");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'springServiceId':'passwordStrengthValidator'}")
                .build();

        Mail mail = createMail("test@example.com", "test", "TEST1@example.com",
                "test2@example.com");

        Passwords passwords = new Passwords();

        PasswordContainer passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("123");

        passwords.put("TEST1@EXAMPLE.COM", passwordContainer);

        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("abcd");

        passwords.put("test2@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Password for test1@example.com, is weak: false"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Password for test2@example.com, is weak: false"));

        assertEquals(0, result.size());

        passwords = new Passwords();

        // Now set a weak password
        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("12");

        passwords.put("TEST1@EXAMPLE.COM", passwordContainer);

        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("abcd");

        passwords.put("test2@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Password for test1@example.com, is weak: true"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Password for test2@example.com, is weak: false"));

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
    }

    @Test
    public void testMissingPasswords()
    throws Exception
    {
        setSenderPasswordPolicy("test@example.com", "{r:[{p:'^.{3,}$'}]}");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'springServiceId':'passwordStrengthValidator'}")
                .build();

        Mail mail = createMail("test@example.com", "test", "TEST1@example.com",
                "test2@example.com");

        Passwords passwords = new Passwords();

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No password found for user test1@example.com"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No password found for user test2@example.com"));

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));

        passwords = new Passwords();

        // Now set one strong password
        PasswordContainer passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("1234");

        passwords.put("test1@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No password found for user test2@example.com"));

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test2@example.com")));
    }

    @Test
    public void testMissingPasswordContainer()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'springServiceId':'passwordStrengthValidator'}")
                .build();

        Mail mail = createMail("test@example.com", "test", "TEST1@example.com",
                "test2@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No passwords found"));

        assertEquals(0, result.size());
    }

    /*
     * If the password policy is invalid, it should match
     */
    @Test
    public void testInvalidPasswordPolicy()
    throws Exception
    {
        setSenderPasswordPolicyRaw("test@example.com","invalid");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'springServiceId':'passwordStrengthValidator'}")
                .build();

        Mail mail = createMail("test@example.com", "test", "test1@example.com",
                "test2@example.com");

        Passwords passwords = new Passwords();

        PasswordContainer passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("1234");

        passwords.put("test1@example.com", passwordContainer);

        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("abcd");

        passwords.put("test2@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Error validating password"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "the rule defintion should be a valid JSON string"));

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test2@example.com")));
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        setSenderPasswordPolicy("test@example.com", "{r:[{p:'^.{9,}$'}]}");

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        IsWeakPassword matcher = new IsWeakPassword()
        {
            @Override
            protected boolean hasMatch(InternetAddress originator, User user, Passwords passwords)
            {
                if (user.getEmail().equals("test4@example.com") && !constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.hasMatch(originator, user, passwords);
            }
        };

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'springServiceId':'passwordStrengthValidator'}")
                .build();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        Mail mail = createMail("test@example.com", "test", "test1@example.com",
                "test2@example.com", "test3@example.com", "test4@example.com");

        Passwords passwords = new Passwords();

        passwords.put("test1@example.com", PasswordContainer.createInstance().setPassword("tooshort"));
        passwords.put("test2@example.com", PasswordContainer.createInstance().setPassword("somepassword"));
        passwords.put("test3@example.com", PasswordContainer.createInstance().setPassword("somepassword"));

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "No password found for user test4@example.com"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test1@example.com")));
        assertTrue(result.contains(new MailAddress("test4@example.com")));
    }
}
