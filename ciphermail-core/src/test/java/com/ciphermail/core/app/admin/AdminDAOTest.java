/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class AdminDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        createDAO().deleteAll();
    }

    private AdminDAO createDAO() {
        return AdminDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Test
    public void testGetAdmin()
    {
        AdminDAO dao = createDAO();

        assertNull(dao.getAdmin("admin1"));

        dao.persist(new Admin("admin2").setIpAddresses(List.of("1.2.3.4")));
        dao.persist(new Admin("admin1"));

        Admin admin = dao.getAdmin("admin1");
        assertNotNull(admin);
        assertEquals("admin1", admin.toString());
        assertTrue(admin.getIpAddresses().isEmpty());

        admin = dao.getAdmin("admin2");
        assertNotNull(admin);
        assertEquals("admin2", admin.toString());
        assertEquals(List.of("1.2.3.4"), admin.getIpAddresses());

        assertEquals(dao.getAdmin("admin1"), dao.getAdmin("admin1"));
        assertNotEquals(dao.getAdmin("admin1"), dao.getAdmin("admin2"));
        assertEquals(dao.getAdmin("admin1").hashCode(), dao.getAdmin("admin1").hashCode());
        assertNotEquals(dao.getAdmin("admin1").hashCode(), dao.getAdmin("admin2").hashCode());
    }

    @Test
    public void testDeleteAdmin()
    {
        AdminDAO dao = createDAO();

        dao.persist(new Admin("admin"));

        assertNotNull(dao.getAdmin("admin"));

        dao.deleteAdmin("admin");

        assertNull(dao.getAdmin("admin"));

        // Deleting unknown admin should be fine
        dao.deleteAdmin("admin");
    }

    @Test
    public void testGetAdmins()
    {
        AdminDAO dao = createDAO();

        List<Admin> admins = dao.getAdmins(null, null, null);
        assertEquals(0, admins.size());

        dao.persist(new Admin("admin2"));
        dao.persist(new Admin("admin1"));
        dao.persist(new Admin("admin3"));

        admins = dao.getAdmins(null, null, null);
        assertEquals(3, admins.size());

        admins = dao.getAdmins(0, Integer.MAX_VALUE, SortDirection.ASC);
        assertEquals(3, admins.size());

        admins = dao.getAdmins(1, Integer.MAX_VALUE, SortDirection.ASC);
        assertEquals(2, admins.size());
        assertEquals("admin2", admins.get(0).getName());
        assertEquals("admin3", admins.get(1).getName());

        admins = dao.getAdmins(1, Integer.MAX_VALUE, SortDirection.DESC);
        assertEquals(2, admins.size());
        assertEquals("admin2", admins.get(0).getName());
        assertEquals("admin1", admins.get(1).getName());

        admins = dao.getAdmins(1, null, SortDirection.DESC);
        assertEquals(2, admins.size());

        admins = dao.getAdmins(1, 1, SortDirection.ASC);
        assertEquals(1, admins.size());
        assertEquals("admin2", admins.get(0).getName());
    }

    @Test
    public void testGetAdminCount()
    {
        AdminDAO dao = createDAO();

        assertEquals(0, dao.getAdminCount());

        dao.persist(new Admin("admin1"));

        assertEquals(1, dao.getAdminCount());

        dao.persist(new Admin("admin2"));

        assertEquals(2, dao.getAdminCount());
    }
}
