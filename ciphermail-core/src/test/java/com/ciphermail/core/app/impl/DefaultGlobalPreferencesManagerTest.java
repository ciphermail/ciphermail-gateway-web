/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.StandardHierarchicalProperties;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class DefaultGlobalPreferencesManagerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserPreferencesCategoryManager userPreferencesCategoryManager;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // We need to delete the global properties otherwise this test sometimes fails when running
                // all tests.
                userPreferencesCategoryManager.getUserPreferencesManager(UserPreferencesCategory.GLOBAL.name()).
                    deleteUserPreferences(new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                            null).getGlobalUserPreferences());
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddGlobalPreferences()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Properties props = new Properties();

                props.setProperty("123", "abc");
                props.setProperty("456", "def");

                HierarchicalProperties factoryProperties = new StandardHierarchicalProperties(
                        "", null, props);

                assertEquals("abc", factoryProperties.getProperty("123"));
                assertEquals("def", factoryProperties.getProperty("456"));

                GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                        factoryProperties);
                assertEquals("abc", manager.getGlobalUserPreferences().getProperties().getProperty("123"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("123"));
                assertEquals("def", manager.getGlobalUserPreferences().getProperties().getProperty("456"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("456"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testLoadFactoryProperties()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Properties props = new Properties();

                props.setProperty("123", "abc");
                props.setProperty("456", "def");

                HierarchicalProperties factoryProperties = new StandardHierarchicalProperties(
                        "", null, props);

                assertEquals("abc", factoryProperties.getProperty("123"));
                assertEquals("def", factoryProperties.getProperty("456"));

                GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                        factoryProperties);

                assertEquals("abc", manager.getGlobalUserPreferences().getProperties().getProperty("123"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("123"));
                assertEquals("def", manager.getGlobalUserPreferences().getProperties().getProperty("456"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("456"));

                props.clear();

                props.setProperty("789", "ghi");
                props.setProperty("abc", "jkl");

                manager.loadFactoryProperties(new StandardHierarchicalProperties(
                        "", null, props));

                assertEquals("ghi", manager.getGlobalUserPreferences().getProperties().getProperty("789"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("789"));
                assertEquals("jkl", manager.getGlobalUserPreferences().getProperties().getProperty("abc"));
                assertTrue(manager.getGlobalUserPreferences().getProperties().isInherited("abc"));

                assertNull(manager.getGlobalUserPreferences().getProperties().getProperty("123"));
                assertNull(manager.getGlobalUserPreferences().getProperties().getProperty("456"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetGlobalPreferencesProperties()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                        null);

                manager.getGlobalUserPreferences().getProperties().setProperty("prop1", "value");
                manager.getGlobalUserPreferences().getProperties().setProperty("prop1", "value2");
                manager.getGlobalUserPreferences().getProperties().setProperty("prop2", "value3");
                manager.getGlobalUserPreferences().getProperties().setProperty("prop3", "value3");
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                GlobalPreferencesManager manager = new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                        null);

                assertEquals("value2", manager.getGlobalUserPreferences().getProperties().getProperty("prop1"));
                assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop1"));
                assertEquals("value3", manager.getGlobalUserPreferences().getProperties().getProperty("prop2"));
                assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop2"));
                assertEquals("value3", manager.getGlobalUserPreferences().getProperties().getProperty("prop3"));
                assertFalse(manager.getGlobalUserPreferences().getProperties().isInherited("prop3"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
