/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.TestUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.base.test.FakeMail;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;


public class DefaultMessageOriginatorIdentifierTest
{
    @Test
    public void testGetOriginator()
    throws Exception
    {
        assertEquals("wulungcheung3007@yahoo.com.hk", new DefaultMessageOriginatorIdentifier().getOriginator(
                TestUtils.loadTestMessage("mail/chinese-encoded-from.eml")).getAddress());
    }

    @Test
    public void testGetOriginatorNullSenderFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");

        message.saveChanges();

        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                message).getAddress());
    }

    @Test
    public void testGetOriginatorInvalidFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("ABC", false));

        message.saveChanges();

        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                message).getAddress());
    }

    @Test
    public void testGetOriginatorMailSender()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");

        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .mimeMessage(message)
                .build();

        assertEquals("test@example.com", new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }

    @Test
    public void testGetOriginatorMailNullSenderNoFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");

        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .mimeMessage(message)
                .build();

        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }

    @Test
    public void testGetOriginatorMailSenderInvalidFrom()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setFrom(new InternetAddress("ABC", false));
        message.setContent("test", "text/plain");

        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .mimeMessage(message)
                .build();

        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }

    /*
     * Test for http://kenai.com/bugzilla/show_bug.cgi?id=5847
     */
    @Test
    public void testJavamailBug5847()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/javamail-bug#5847.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .mimeMessage(message)
                .build();

        assertEquals(EmailAddressUtils.INVALID_EMAIL, new DefaultMessageOriginatorIdentifier().getOriginator(
                mail).getAddress());
    }
}
