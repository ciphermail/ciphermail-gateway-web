/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.james.PhoneNumber;
import com.ciphermail.core.app.james.PhoneNumbers;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.DummySMSTransport;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang.time.DateUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderTemplatePropertySMSPasswordTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private SMSGateway smsGateway;

    @Autowired
    private DummySMSTransport smsTransport;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        smsGateway.start();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @After
    public void setUpAfter()
    throws Exception
    {
        // Wait for background thread to stop
        smsGateway.stop(DateUtils.MILLIS_PER_SECOND * 30);
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        smsTransport.reset();
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SenderTemplatePropertySMSPassword();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testSendSMS()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms2.ftl")
                .setProperty("templateProperty", "smsTemplate")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com", "123@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        String template = FileUtils.readFileToString(new File("src/test/resources/templates/sms2.ftl"),
                StandardCharsets.UTF_8);

        // Set the property for the originator of the email
        setUserProperty("test@example.com", "smsTemplate", template);

        Passwords passwords = new Passwords();

        PasswordContainer passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("test1");
        passwordContainer.setPasswordID("test ID 1");

        passwords.put("m.brinkers@pobox.com", passwordContainer);

        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("test2");
        passwordContainer.setPasswordID("test ID 2");

        passwords.put("123@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("m.brinkers@pobox.com", new PhoneNumber("1234567890"));
        phoneNumbers.put("123@example.com", new PhoneNumber("0987654321"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        ThreadUtils.sleepQuietly(1000);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234567890'"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '0987654321'"));

        assertEquals(2, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234567890"));
        assertTrue(smsTransport.getPhoneNumbers().contains("0987654321"));
        assertEquals(2, smsTransport.getMessages().size());

        String sms0 = smsTransport.getMessages().get(0);
        String sms1 = smsTransport.getMessages().get(1);

        assertTrue(sms0.startsWith("TEST"));
        assertTrue(sms0.contains("test1"));
        assertTrue(sms0.contains("test ID 1"));

        assertTrue(sms1.startsWith("TEST"));
        assertTrue(sms1.contains("test2"));
        assertTrue(sms1.contains("test ID 2"));
    }

    @Test
    public void testSendSMSRetry()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("template", "sms2.ftl")
                .setProperty("templateProperty", "smsTemplate")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/normal-message-with-attach.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.brinkers@pobox.com", "123@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        Mailet mailet = new SenderTemplatePropertySMSPassword()
        {
            @Override
            protected void onHandleUserEvent(User user)
            throws MessagingException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
                super.onHandleUserEvent(user);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        String template = FileUtils.readFileToString(new File("src/test/resources/templates/sms2.ftl"),
                StandardCharsets.UTF_8);

        // Set the property for the originator of the email
        setUserProperty("test@example.com", "smsTemplate", template);

        Passwords passwords = new Passwords();

        PasswordContainer passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("test1");
        passwordContainer.setPasswordID("test ID 1");

        passwords.put("m.brinkers@pobox.com", passwordContainer);

        passwordContainer = new PasswordContainer();
        passwordContainer.setPassword("test2");
        passwordContainer.setPasswordID("test ID 2");

        passwords.put("123@example.com", passwordContainer);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        PhoneNumbers phoneNumbers = new PhoneNumbers();

        phoneNumbers.put("m.brinkers@pobox.com", new PhoneNumber("1234"));
        phoneNumbers.put("123@example.com", new PhoneNumber("5678"));

        CoreApplicationMailAttributes.setPhoneNumbers(mail, phoneNumbers);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        // wait some time for the SMS messages to be transported
        ThreadUtils.sleepQuietly(1000);

        assertTrue(constraintViolationThrown.booleanValue());

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '1234'"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Successfully sent SMS with transport 'DummySMSTransport' to phone number '5678'"));

        assertEquals(2, smsTransport.getPhoneNumbers().size());
        assertTrue(smsTransport.getPhoneNumbers().contains("1234"));
        assertTrue(smsTransport.getPhoneNumbers().contains("5678"));
        assertEquals(2, smsTransport.getMessages().size());

        String sms0 = smsTransport.getMessages().get(0);
        String sms1 = smsTransport.getMessages().get(1);

        assertTrue(sms0.startsWith("TEST"));
        assertTrue(sms0.contains("test1"));
        assertTrue(sms0.contains("test ID 1"));

        assertTrue(sms1.startsWith("TEST"));
        assertTrue(sms1.contains("test2"));
        assertTrue(sms1.contains("test ID 2"));
    }
}
