/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesImpl;
import com.ciphermail.core.common.properties.DefaultPropertyProviderRegistry;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.StandardHierarchicalProperties;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.util.LinkedSet;
import com.ciphermail.core.common.util.LinkedSetImpl;
import com.ciphermail.core.common.util.NullableProperties;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

public class MockupUserPreferences implements UserPreferences
{
    private final String name;
    private final String category;
    private final Set<X509Certificate> certificates = new HashSet<>();
    private final Set<NamedCertificate> namedCertificates = new HashSet<>();
    private final Set<X509Certificate> inheritedCertificates = new HashSet<>();
    private final Set<NamedCertificate> inheritedNamedCertificates = new HashSet<>();
    private KeyAndCertificate keyAndCertificate;
    private final Set<KeyAndCertificate> inheritedKeyAndCertificates = new HashSet<>();
    private HierarchicalProperties properties;
    private final LinkedSet<UserPreferences> inheritedUserPreferences = new LinkedSetImpl<>();
    private final Set<NamedBlob> namedBlobs = new LinkedHashSet<>();

    private DefaultPropertyProviderRegistry defaultPropertyProviderRegistry = new DefaultPropertyProviderRegistry()
    {
        @Override
        public String getDefaultValue(@Nonnull HierarchicalProperties properties, @Nonnull String property) {
            return null;
        }

        @Override
        public boolean isNullDefaultValueAllowed(@Nonnull String property) {
            return false;
        }
    };

    public MockupUserPreferences(@Nonnull String name, @Nonnull String category)
    {
        this.name = Objects.requireNonNull(name);
        this.category = Objects.requireNonNull(category);
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    @Override
    public @Nonnull String getCategory() {
        return category;
    }

    @Override
    public @Nonnull Set<X509Certificate> getCertificates() {
        return certificates;
    }

    @Override
    public @Nonnull Set<NamedCertificate> getNamedCertificates() {
        return namedCertificates;
    }

    @Override
    public @Nonnull Set<X509Certificate> getInheritedCertificates() {
        return inheritedCertificates;
    }

    @Override
    public @Nonnull Set<NamedCertificate> getInheritedNamedCertificates() {
        return inheritedNamedCertificates;
    }

    @Override
    public KeyAndCertificate getKeyAndCertificate()
    throws CertStoreException, KeyStoreException
    {
        return keyAndCertificate;
    }

    @Override
    public void setKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        this.keyAndCertificate = keyAndCertificate;
    }

    @Override
    public Set<KeyAndCertificate> getInheritedKeyAndCertificates() {
        return inheritedKeyAndCertificates;
    }

    @Override
    public @Nonnull UserProperties getProperties()
    {
        if (properties == null) {
            properties = new StandardHierarchicalProperties(category, null, new NullableProperties());
        }

        UserPropertiesImpl userProperties = new UserPropertiesImpl(properties);

        userProperties.setDefaultPropertyProviderRegistry(defaultPropertyProviderRegistry);

        return userProperties;
    }

    @Override
    public @Nonnull UserProperties getExplicitlySetProperties() {
        return getProperties();
    }

    @Override
    public @Nonnull Set<NamedBlob> getNamedBlobs() {
        return namedBlobs;
    }

    @Override
    public @Nonnull LinkedSet<UserPreferences> getInheritedUserPreferences() {
        return inheritedUserPreferences;
    }

    public DefaultPropertyProviderRegistry getDefaultPropertyProviderRegistry() {
        return defaultPropertyProviderRegistry;
    }

    public MockupUserPreferences setDefaultPropertyProviderRegistry(DefaultPropertyProviderRegistry defaultPropertyProviderRegistry)
    {
        this.defaultPropertyProviderRegistry = defaultPropertyProviderRegistry;
        return this;
    }

    @Override
    public String toString() {
        return getName() + ":" + getCategory();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferences otherUserPreferences)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(getCategory(), otherUserPreferences.getCategory())
            .append(getName(), otherUserPreferences.getName())
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(getCategory())
            .append(getName())
            .toHashCode();
    }
}
