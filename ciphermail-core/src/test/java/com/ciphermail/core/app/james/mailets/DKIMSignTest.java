/*
 * Copyright (c) 2016-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dkim.DKIMPrivateKeyManager;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.tagvalue.SignatureRecordImpl;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringReader;
import java.security.KeyPair;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class DKIMSignTest
{
    private static final String RSA_KEY_PAIR_2048 =
            """
                -----BEGIN RSA PRIVATE KEY-----
                MIIEowIBAAKCAQEAjDL09cO7I/fa83FWHAcb9AprRlF+eGuoDD5jgauXq6ATbJPy
                tz0PO01uUWIoFHbfz0XWTgfCL6smaWegS/ATe2/nOx3cbxyn7RlZJFEm/hrDxyvl
                cTk/+sXWvCxDuiyQ3fBOav4kckqozJxB3Dd+k8xDL+rph2WElxwq649dTRVDY9+e
                MUp061H66ZMrsM4VzAJO2LiBQsDXEouts++JXQJbejVSMDbTKMlYT0XnP7t8fNrx
                myQcrgR1cd+py06Nxx6gqitCpDyHNjsHG6qUhZHUkK3tGgG8Qky0sAqmyMvOBQs8
                7/wUDDftaxCKTSy0ymKEZgzZStyj39YXCxupXQIDAQABAoIBAHhhCJQ9i+JGX2nI
                VveZ4xaYG6Q1NTKQvapFp4sfmwtyVztTjYRomTuCMMcQUPnBWErQ3EIDx2jGvCSl
                Ja4OZawHWbQY371IDag3q6LTS3fD43aQ6mFdluHsHYVVPJIO0hS+0ZZswT6hfG6z
                fJlpzPV63fW27u2i7UYSmYfJgYM9CVC0cYB5Yy3fgyoyEi6pa3EgZJrK9mXFCw3P
                5+IBh0W+OSz9f47k4kriAVEEwsao+EyNSz7YoPwLq+5/eXN4XAalDD5TgPgMZAz9
                unclOPmxZbPjQ/hbEuMTW+G58SbSyQWKxTLPIKqmi/0W217QhvgQ9IXAhwS3EPcw
                DipPegECgYEAwD0MOhOUxIIPDjke/QnYOqCZ0gudpCHtdfR8MYZeqiMbRQZsHJjX
                9YzuFV9LUlgoH5uq1qJub9mQrGayl3TQOl8KOeNx0vRoHR0xdkZeJ06E/d/It/FC
                xxzLykjhgrbfjGHkCVi7zz5aqxEz6i1a9A6uvKuHFB20hsEGwHQtZVUCgYEAurM/
                G/WgRb3nNI+aHzevfuXEijdhGZzhIf8AnDJTwnD6rEZDJaq2Wl7pi6Pi6Got05Lp
                +UmiPbttj9uXvp3kO66I7Iwz8aLP/faG8qPNoIlT4j2OaDRS4xppQ4v3McA8QKc2
                SveauuWPkBLOd0OBOLlDoJw4HmRpmSlJdFAws+kCgYAJosW7H4Ike3RvvRM9kcHB
                5ozvR1/Ge2DgubmD7f1Ov+W9Bv5iTJL+nurMLXOkFAEm3HGqwoeQmbIzwg4Po25Q
                jrT/g9QPw+p4Ex+IkyxjAf+OjioMdk5nlpzsbczH9YytB7cbdNQtJi37Ryh4A3/8
                ncPKdzqVGowS+Rfyi6A+5QKBgQCwMaNYZplH2C1PfEpuNaZIstyluBfKDTSmWbqg
                rhpAAVGArZqG/LG1Xac4YoUs46+14QsweR5E9hIy1oFBok6XSGLuIm6PFyEFQtge
                Oxo+6/sfIwq3KtIig3VeCsRRzmLOfT+OxlffE/BFfoodIbbc6nJ5K5UGkaraoY0x
                2M/tqQKBgFtfp0aGkOj/zFQ0aKrO/+WR9lOXfgbv/XhkzJHUOCqxcYBltGx835hx
                wK1r47Qg0f8ykDsEmgQMjlhjZE3JyqaG70WobmN0YqtS4MCaKnPgdav0Jqc6bAzA
                qecD6CBCde5o2U6KEMbPtsblABNssbx0aKA4IuYR4yDwb2V9lujP
                -----END RSA PRIVATE KEY-----""";

    private static final String RSA_KEY_PAIR_1024 =
            """
                -----BEGIN RSA PRIVATE KEY-----
                MIICXAIBAAKBgQCXf8r0Kwki1EAf4BHKcDvHw+3W3gNBgL9ZSj8rpL7OZ7vV/eTn
                7o47aDes6PUyCQeyBB8GlOwznv34T5vlqhSoa2M9WIvu4pzpNbkqql6x3bNE8NwD
                CKK4eNNCLGSQxz491eudIdWnrnNqKcVPAmZWhuRajw/jSLoRVZAoD9u/WwIDAQAB
                AoGALJxCxPdPL2RUpY4zburxj3dQN68UQluI/N/yDgXq8Zh2JSMoHmuOkkuz6USH
                vv4NuAuinyuHCgRNQKsgetZEUoVGrPAGm0d6WgAE3fstGeuDD/PSoPj4JQciuC/M
                Y69KiLQHhI0Ktx/q+hEKoIutdcIUvhmZ+RtNYxzosxNexVECQQDw0bgS7CDE0ya7
                VtHkv+9u7xq3bzAqa8AvXC5nt7zPYRHvir3uNKuX0yiqeLZ+C2aqcgOtpEtdnNpK
                hqvTPzg3AkEAoQymNufLK5baZ4JKVqAmr6YPH8nHVQ9VDL0MkkjdeHwKaPZXWrFM
                mjFvXpp/OouTgw5kzg0vlKRpqSpv863X/QJBAKsLCcbG1+90WcotvB7RBGqygTNQ
                UdGPfo0k2ADqy7wvoeGVlZke59GKNRP/cP7NcjoViO9IBg+TXKgaRuhfurkCQCX7
                Nbf9Mo01Jo0CzUVgv77tCuQUUk5dL1GxxCU7yf+AQXQ/pJpe9hHnVryY8yh7gm6G
                FoAB3BbSL5kVyRFPDoECQAt60VXz60efoAzG3nopD2ee7IWcXiOad1SKIg8k5eyd
                kkXAglr/2ZH/734JlsywDkYcI/o8aoVZOuGKS1USyKM=
                -----END RSA PRIVATE KEY-----""";

    private static KeyPair parsePEM(String pemEncodedKeyPair)
    throws Exception
    {
        PEMParser parser = new PEMParser(new StringReader(pemEncodedKeyPair));

        Object pem = null;

        try {
            pem = parser.readObject();
        }
        finally {
            IOUtils.closeQuietly(parser);
        }

        JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();
        keyConverter.setProvider(SecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        return keyConverter.getKeyPair((PEMKeyPair) pem);
    }

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private DKIMPrivateKeyManager dkimPrivateKeyManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    /*
     * We only want to initialize the database once. However, we cannot do this in setUpBeforeClass because we
     * need instances injected by Spring. This must be static because of the way Junit works
     */
    private static boolean setUpIsDone = false;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @Before
    public void setup()
    {
        // we cannot access the database from BeforeClass method because the required members are not available
        if (!setUpIsDone)
        {
            setUpIsDone = true;

            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    dkimPrivateKeyManager.setKey("*.example.com", parsePEM(RSA_KEY_PAIR_2048));
                    dkimPrivateKeyManager.setKey("test2@example.com", parsePEM(RSA_KEY_PAIR_1024));

                    setDomainProperty("*.example.com", "dkim-key-identifier", "*.example.com");

                    setUserProperty("test2@example.com", "dkim-key-identifier", "test2@example.com");
                    setUserProperty("test2@example.com", "dkim-signature-template",
                        "v=1; c=relaxed/relaxed; s=other; d=${domain}; h=From:Subject:To:Date; a=rsa-sha1; bh=; b=;");
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });
        }
    }

    private void setUserProperty(String email, String propertyName, String value)
    throws Exception
    {
        User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

        UserProperties userProperties = user.getUserPreferences().getProperties();

        userProperties.setProperty(propertyName, value);

        userWorkflow.makePersistent(user);
    }

    private void setDomainProperty(String domain, String propertyName, String value)
    throws Exception
    {
        UserPreferences preferences = domainManager.getDomainPreferences(domain);

        if (preferences == null) {
            preferences = domainManager.addDomain(domain);
        }

        UserProperties properties = preferences.getProperties();

        properties.setProperty(propertyName, value);
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new DKIMSign();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testSign2048Sha256()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("somestate")
                .build();

        assertNull(mail.getMessage().getHeader("DKIM-Signature"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        String dkimHeader = mail.getMessage().getHeader("DKIM-Signature", ",");

        assertNotNull(dkimHeader);

        SignatureRecord signatureRecord = new SignatureRecordImpl(dkimHeader);

        signatureRecord.validate();
        assertEquals("ciphermail", signatureRecord.getSelector());
        assertNotNull(signatureRecord.getSignatureTimestamp());
        assertEquals("sha-256", signatureRecord.getHashAlgo());
    }

    @Test
    public void testDifferentDKIMSignatureTemplate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        message.setFrom(new InternetAddress("test2@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("somestate")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed; Recipients: [recipient@example.com]"));

        MailUtils.validateMessage(mail.getMessage());

        String dkimHeader = mail.getMessage().getHeader("DKIM-Signature", ",");

        assertNotNull(dkimHeader);

        SignatureRecord signatureRecord = new SignatureRecordImpl(dkimHeader);

        signatureRecord.validate();
        assertEquals("other", signatureRecord.getSelector());
        // t= property is missing from DKIM signature template therefore timestamp is null
        assertNull(signatureRecord.getSignatureTimestamp());
        assertEquals("sha-1", signatureRecord.getHashAlgo());
    }

    @Test
    public void testCustomDKIMHeader()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("dkimHeader", "X-Custom-DKIM")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("somestate")
                .build();

        assertNull(mail.getMessage().getHeader("X-Custom-DKIM"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was DKIM signed; Recipients: [recipient@example.com]"));

        MailUtils.validateMessage(mail.getMessage());

        String dkimHeader = mail.getMessage().getHeader("X-Custom-DKIM", ",");

        assertNotNull(dkimHeader);

        SignatureRecord signatureRecord = new SignatureRecordImpl(dkimHeader);

        signatureRecord.validate();
        assertEquals("ciphermail", signatureRecord.getSelector());
        assertNotNull(signatureRecord.getSignatureTimestamp());
        assertEquals("sha-256", signatureRecord.getHashAlgo());
    }

    @Test
    public void testConvert8BitTo7Bit()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/8bit-binary.eml"))
                .build();

        assertEquals("8bit", mail.getMessage().getHeader("Content-Transfer-Encoding", ","));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("base64", mail.getMessage().getHeader("Content-Transfer-Encoding", ","));
        assertEquals("from 8bit to 7bit by CipherMail", mail.getMessage().getHeader(
                "X-MIME-Autoconverted", ","));

        String dkimHeader = mail.getMessage().getHeader("DKIM-Signature", ",");

        assertNotNull(dkimHeader);
    }

    @Test
    public void testRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("somestate")
                .build();

        Set<String> handledUsers = new HashSet<>();

        Mailet mailet = new DKIMSign()
        {
            @Override
            protected void onHandleUserEvent(User user, Mail mail, Context context)
            throws MessagingException
            {
                super.onHandleUserEvent(user, mail, context);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!handledUsers.contains(user.getEmail()))
                {
                    handledUsers.add(user.getEmail());

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        MailUtils.validateMessage(mail.getMessage());

        String dkimHeader = mail.getMessage().getHeader("DKIM-Signature", ",");

        assertNotNull(dkimHeader);

        assertEquals(1, handledUsers.size());
    }
}
