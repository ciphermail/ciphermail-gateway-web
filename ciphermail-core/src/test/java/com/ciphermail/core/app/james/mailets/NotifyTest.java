/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dlp.PolicyViolation;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class NotifyTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup() {
        deleteAllUsers();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void deleteAllUsers()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new Notify();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    /*
     * Tests whether invalid email addresses for the notification are skipped
     */
    @Test
    public void testNotificationInvalidFromToEtc()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}, invalid@invalid.tld, a@b.c, ${invalid}")
                .setProperty("passThroughProcessor", "nextProcessor")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("123@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/invalid-from-to-cc-reply-to.eml"))
                .build();

        List<AttributeValue<?>> params = List.of(
                AttributeValue.of("**"),
                AttributeValue.of(EmailAddressUtils.INVALID_EMAIL),
                AttributeValue.ofUnserializable (new MailAddress(EmailAddressUtils.INVALID_EMAIL)),
                AttributeValue.ofUnserializable (new InternetAddress(EmailAddressUtils.INVALID_EMAIL)),
                AttributeValue.ofUnserializable (new InternetAddress("***", false)));

        mail.setAttribute(new Attribute(AttributeName.of("invalid"), AttributeValue.of(params)));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // state of existing mail should be set to passThroughProcessor
        assertEquals("nextProcessor", mail.getState());

        assertThat(memoryLogAppender.getLogOutput(), containsString("*** is not a valid email address"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("a@b.c", StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());
    }

    /*
     * Tests whether a template with an invalid content-transfer encoding can be handled.
     */
    @Test
    public void testInvalidEncoding()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}")
                .setProperty("templateProperty", "notifyTemplate")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("m.brinkers@pobox.com", "123@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment-invalid-encoding.ftl"),
                StandardCharsets.UTF_8);

        setUserProperty("test@EXAMPLE.com", "notifyTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertNull(mail.getState());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("test@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertNotNull(sentMail.getMsg().getHeader("X-pdf-template-test"));

        MailUtils.validateMessage(sentMail.getMsg());

        String mime = MailUtils.partToMimeString(sentMail.getMsg());

        assertTrue(mime.contains("Content-Transfer-Encoding: SOME_UNKNOWN_ENCODING"));
    }

    @Test
    public void testEncryptionNotification()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "r1@example.com, r2@example.com")
                .setProperty("to", "t1@example.com, t2@example.com")
                .setProperty("from", "from@example.com")
                .setProperty("replyTo", "reply@example.com")
                .setProperty("sender", "sender@example.com")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("a1@example.com", "a2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertNull(mail.getState());

        assertEquals(1, mailContext.getSentMails().size());
        assertEquals("a1@example.com,a2@example.com", StringUtils.join(mail.getRecipients(), ","));

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("r1@example.com,r2@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertEquals("sender@example.com", sentMail.getSender().toString());
        assertEquals("<from@example.com>", sentMail.getMsg().getHeader("from", ","));
        assertEquals("<t1@example.com>, <t2@example.com>", sentMail.getMsg().getHeader("to", ","));
        assertEquals("The message has been encrypted", sentMail.getMsg().getSubject());

        MailUtils.validateMessage(sentMail.getMsg());
    }

    /*
     * Tests whether attributes from the original Mail are cloned to the notification Mail
     */
    @Test
    public void testNotificationCloneAttributes()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "r1@example.com, r2@example.com")
                .setProperty("to", "t1@example.com, t2@example.com")
                .setProperty("from", "from@example.com")
                .setProperty("replyTo", "reply@example.com")
                .setProperty("subject", "some subject")
                .setProperty("sender", "sender@example.com")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("a1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mail.setAttribute("var1", "123");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertNull(mail.getState());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("r1@example.com,r2@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertEquals("sender@example.com", sentMail.getSender().toString());
        assertEquals("<from@example.com>", sentMail.getMsg().getHeader("from", ","));
        assertEquals("<t1@example.com>, <t2@example.com>", sentMail.getMsg().getHeader("to", ","));
        assertEquals("The message has been encrypted", sentMail.getMsg().getSubject());

        MailUtils.validateMessage(sentMail.getMsg());

        assertEquals("123", sentMail.getAttributes().get(AttributeName.of("var1")).getValue().getValue());
    }

    /*
     * Tests whether the "special" ${originator} token is replaced by the originator of the email
     */
    @Test
    public void testEncryptionNotificationOriginator()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}")
                .setProperty("to", "${sameAsRecipients}")
                .setProperty("from", "${null}")
                .setProperty("processor", "testProcessor")
                .setProperty("passThrough", "false")
                .setProperty("sender", "${null}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .recipients("r2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(Mail.GHOST, mail.getState());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("testProcessor", sentMail.getState());
        // recipient of the notification should be set to the From of the original email
        assertEquals("test@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        assertNull(sentMail.getSender());
        // From should be set to <postmaster> because of <null>
        assertEquals("<postmaster>", sentMail.getMsg().getHeader("from", ","));
        assertEquals("The message has been encrypted", sentMail.getMsg().getSubject());
        assertEquals("<test@example.com>", sentMail.getMsg().getHeader("to", ","));

        MailUtils.validateMessage(sentMail.getMsg());

        String mime = MailUtils.partToMimeString(sentMail.getMsg());

        assertTrue(mime.contains("r1@example.com\nr2@example.com\n"));
    }

    /*
     * Tests whether a notification to an invalid recipient is not sent and does not result in some error
     */
    @Test
    public void testEncryptionNotificationInvalidRecipients()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "invalid")
                .setProperty("from", "null")
                .setProperty("passThrough", "false")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("There are no recipients. " +
                "Retrying (retry count: 1)"));

        MailUtils.validateMessage(mail.getMessage());

        // Because of an internal exception ("MissingRecipientsException: There are no recipients"), the
        // email was not handled and therefore passThrough was ignored
        assertNull(mail.getState());

        assertEquals(0, mailContext.getSentMails().size());
    }

    /*
     * Tests whether the template for the notification is read from a user property
     */
    @Test
    public void testNotificationUserTemplateProperty()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${sameAsMessage}")
                .setProperty("templateProperty", "notifyTemplate")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com", "r2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setUserProperty("test@example.com", "notifyTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("r1@example.com,r2@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertNotNull(sentMail.getMsg().getHeader("X-pdf-template-test"));

        MailUtils.validateMessage(sentMail.getMsg());

        String mime = MailUtils.partToMimeString(sentMail.getMsg());

        assertTrue(mime.contains("Content-Disposition: attachment; filename=encrypted.pdf"));
    }

    /*
     * Tests whether a missing template property does not result in an error
     */
    @Test
    public void testNotificationUserTemplateMissingProperty()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("templateProperty", "notifyTemplate")
                .setProperty("recipients", "${originator}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .recipients("r2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setUserProperty("test@example.com", "otherTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        // recipient of the notification should be set to the From of the original email
        assertEquals("test@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        assertEquals("The message has been encrypted", sentMail.getMsg().getSubject());

        MailUtils.validateMessage(sentMail.getMsg());

        String mime = MailUtils.partToMimeString(sentMail.getMsg());

        assertTrue(mime.contains("r1@example.com\nr2@example.com\n"));
    }

    /*
     * Tests whether special variables #{} are replaced by user property values
     */
    @Test
    public void testSpecialUserPropertyRecipient()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}, #{prop.recipient}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        setUserProperty("test@EXAMPLE.com", "prop.recipient",
                "recipient.from.property@example.com, CipherMail Support <support@ciphermail.com>");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        // recipient of the notification should be set to originator and the value of the property
        assertEquals("test@example.com,recipient.from.property@example.com,support@ciphermail.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());
    }

    /*
     * Tests whether missing special variables #{} do not result in an error
     */
    @Test
    public void testSpecialUserPropertyMissing()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}, #{prop.recipient}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        // recipient of the notification should only be set to originator because the user property is missing
        assertEquals("test@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());
    }

    /*
     * Tests whether invalid email addresses in special variables #{} are skipped and does not result in an error
     */
    @Test
    public void testSpecialUserPropertyInvalidEmail()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}, #{prop.recipient}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        setUserProperty("test@EXAMPLE.com", "prop.recipient",
                "invalidemail, CipherMail Support <support@ciphermail.com>");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        // recipient of the notification should be set to originator and the value of the property
        assertEquals("test@example.com,support@ciphermail.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());
    }

    /*
     * Tests what happens when special variables #{} result in no recipients
     */
    @Test
    public void testSpecialUserPropertyNoRecipients()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "#{prop.recipient}")
                .setProperty("passThrough", "false")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        setUserProperty("test@example.com", "prop.recipient",
                "invalidemail, ***");

        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());

        assertThat(memoryLogAppender.getLogOutput(), containsString("There are no recipients. " +
                "Retrying (retry count: 1)"));

        // Because of an internal exception ("MissingRecipientsException: There are no recipients"), the
        // email was not handled and therefore passThrough was ignored
        assertNull(mail.getState());

        assertEquals(0, mailContext.getSentMails().size());
    }

    /*
     * Tests whether setting ignoreMissingRecipients to true will result in using the passThrough property
     */
    @Test
    public void testMissingRecipientsIgnore()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "#{prop.recipient}")
                .setProperty("passThrough", "false")
                .setProperty("ignoreMissingRecipients", "true")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        setUserProperty("test@example.com", "prop.recipient", "invalidemail");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // Even though there were missing recipients, passThrough is used and no exception was thrown in the back
        assertEquals(Mail.GHOST, mail.getState());

        assertEquals(0, mailContext.getSentMails().size());
    }

    /*
     * Tests support for recipients from Mail attributes
     */
    @Test
    public void testMailAttr()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${attr1}, ${attr2}, ${non_exsisting}, direct@example.com,   ${attr3}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mail.setAttribute(new Attribute(AttributeName.of("attr1"), AttributeValue.of(Arrays.asList(
                AttributeValue.of("a1-1@example.com"),
                AttributeValue.ofUnserializable (new InternetAddress("a1-2@example.com", false))))));

        mail.setAttribute(new Attribute(AttributeName.of("attr2"), AttributeValue.of(Arrays.asList(
                AttributeValue.ofUnserializable(new MailAddress("a2-1@example.com")),
                AttributeValue.ofUnserializable (new InternetAddress("a2-2@example.com", false))))));

        mail.setAttribute(new Attribute(AttributeName.of("attr3"), AttributeValue.of("a3-1@example.com")));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("a1-1@example.com,a1-2@example.com,a2-1@example.com,a2-2@example.com," +
                        "direct@example.com,a3-1@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());
    }

    @Test
    public void testInvalidOriginator()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "${originator}")
                .setProperty("passThrough", "false")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test\n", "text/plain");
        message.setHeader("From", "!@#$%^&*(");

        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com")
                .mimeMessage(message)
                .build();

        mailet.service(mail);

        MailUtils.validateMessage(mail.getMessage());

        assertThat(memoryLogAppender.getLogOutput(), containsString("There are no recipients. " +
                "Retrying (retry count: 1)"));

        // Because of an internal exception ("MissingRecipientsException: There are no recipients"), the
        // email was not handled and therefore passThrough was ignored
        assertNull(mail.getState());

        assertEquals(0, mailContext.getSentMails().size());
    }

    /*
     * Tests whether special properties are supported
     */
    @Test
    public void testSpecialAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("templateProperty", "testTemplate")
                .setProperty("sender", "${replyTo}")
                .setProperty("recipients", "${recipients}, ${originator}")
                .setProperty("to", "${from},${sender},${sameAsMessage}")
                .setProperty("cc", "${sameAsRecipients}, ${replyTo},${sameAsMessage}")
                .setProperty("from", "from@example.com")
                .setProperty("subject", "some subject")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setFrom(new InternetAddress("from@example.com"));
        message.setReplyTo(new Address[]{new InternetAddress("reply@example.com")});
        message.setText("test body");
        message.setSubject("test subject");
        message.setRecipients(RecipientType.TO, InternetAddress.parse("o1@example.com, o2@example.com"));
        message.setRecipients(RecipientType.CC, InternetAddress.parse("o3@example.com"));
        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com", "r2@example.com")
                .mimeMessage(message)
                .build();

        String template =
                """
                Subject: ${subject}\r
                From: ${from!}\r
                <#if to??>\r
                To: <#list to as recipient>${recipient}<#if recipient_has_next>, </#if></#list>\r
                </#if>\r
                <#if cc??>\r
                Cc: <#list cc as ccrecipient>${ccrecipient}<#if ccrecipient_has_next>, </#if></#list>\r
                </#if>\r
                Content-Type: text/plain\r
                Content-Transfer-Encoding: 7bit\r
                Mime-Version: 1.0\r
                \r
                Some body\r
                """;

        setUserProperty("from@example.com", "testTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MailUtils.validateMessage(sentMail.getMsg());

        assertEquals("reply@example.com", sentMail.getSender().toString());

        assertEquals("r1@example.com,r2@example.com,from@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        assertEquals("from@example.com,sender@example.com,o1@example.com,o2@example.com",
                StringUtils.join(sentMail.getMsg().getRecipients(Message.RecipientType.TO), ","));

        assertEquals("r1@example.com,r2@example.com,from@example.com,reply@example.com,o3@example.com",
                StringUtils.join(sentMail.getMsg().getRecipients(Message.RecipientType.CC), ","));
    }

    /*
     * Tests sameAsMessage special addresses which are not covered by other tests
     */
    @Test
    public void testSameAsMessageSpecialAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("recipients", "recipient@example.com")
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("templateProperty", "testTemplate")
                .setProperty("sender", "${sameAsMessage}")
                .setProperty("replyTo", "${sameAsMessage}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setFrom(new InternetAddress("from@example.com"));
        message.setReplyTo(new Address[]{new InternetAddress("reply@example.com")});
        message.setText("test body");
        message.setSubject("test subject");
        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com")
                .mimeMessage(message)
                .build();

        String template =
                """
                Subject: Some subject\r
                Reply-To: ${replyTo!}\r
                Content-Type: text/plain\r
                Content-Transfer-Encoding: 7bit\r
                Mime-Version: 1.0\r
                \r
                Some body\r
                """;

        setUserProperty("from@example.com", "testTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MailUtils.validateMessage(sentMail.getMsg());

        assertEquals("recipient@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertEquals("sender@example.com", sentMail.getSender().toString());
        assertEquals("reply@example.com", StringUtils.join(sentMail.getMsg().getReplyTo(), ","));
    }

    /*
     * Tests whether attachSourceMessage set to true will result in adding the original email to the notification
     */
    @Test
    public void testAttachSourceMessage()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "test@example.com")
                .setProperty("attachSourceMessage", "true")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com")
                .mimeMessage(sourceMessage)
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("test@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());

        assertTrue(sentMail.getMsg().isMimeType("multipart/mixed"));
        assertEquals("The message has been encrypted", sentMail.getMsg().getSubject());
        assertEquals("auto-replied", sentMail.getMsg().getHeader("Auto-Submitted", ","));

        Multipart mp = (Multipart) sentMail.getMsg().getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8; format=flowed", bp.getContentType());
        assertEquals("The message with Subject:\n" +
                     "\n" +
                     "\n" +
                     "\n" +
                     "has been sent encrypted to the following recipients:\n" +
                     "\n" +
                     "r1@example.com\n" +
                     "\n" +
                     "---\n" +
                     "Email encryption with support for S/MIME, OpenPGP, PDF Messenger and Webmail Messenger (https://ciphermail.com)",
                ((String)bp.getContent()).trim());

        bp = mp.getBodyPart(1);

        assertEquals("message/rfc822; name=forwarded.eml", bp.getContentType());

        MimeMessage attached = (MimeMessage) bp.getContent();

        assertEquals("test simple message", attached.getSubject());
        assertArrayEquals(IOUtils.toByteArray(attached.getRawInputStream()),
                IOUtils.toByteArray(sourceMessage.getRawInputStream()));
    }

    /*
     * Tests whether the attached source message name can be set
     */
    @Test
    public void testAttachSourceMessageWithExplicitName()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "test@example.com")
                .setProperty("attachSourceMessage", "true")
                .setProperty("sourceMessageName", "some-name-with-weird-chars;=+\".eml")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com")
                .mimeMessage(sourceMessage)
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("test@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        MailUtils.validateMessage(sentMail.getMsg());

        assertTrue(sentMail.getMsg().isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) sentMail.getMsg().getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8; format=flowed", bp.getContentType());

        bp = mp.getBodyPart(1);

        assertEquals("message/rfc822; name=\"some-name-with-weird-chars;=+\\\".eml\"", bp.getContentType());

        MimeMessage attached = (MimeMessage) bp.getContent();

        assertEquals("test simple message", attached.getSubject());
        assertArrayEquals(IOUtils.toByteArray(attached.getRawInputStream()),
                IOUtils.toByteArray(sourceMessage.getRawInputStream()));
    }

    /*
     * Tests whether a from with only the local poart, i.e., no @, is supported
     */
    @Test
    public void testNonFullyQualifiedFrom()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("recipients", "test@example.com")
                .setProperty("from", "from")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage("mail/simple-text-message.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("r1@example.com")
                .mimeMessage(sourceMessage)
                .build();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals(Mail.DEFAULT, sentMail.getState());
        assertEquals("<from>", sentMail.getMsg().getHeader("from", ","));
    }

    /*
     * Tests whether encoded chars for headers is supported
     */
    @Test
    public void testNotificationUnsafeChars()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "sms.ftl")
                .setProperty("templateProperty", "notifyTemplate")
                .setProperty("recipients", "${sender}")
                .setProperty("subject", "${sameAsMessage}")
                .setProperty("from", "${sameAsMessage}")
                .setProperty("to", "${sameAsMessage}, other-to@example.com")
                .setProperty("cc", "${sameAsMessage}, other-cc@example.com")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("from@example.com")
                .recipients("test@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/unsafe-chars-headers.eml"))
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/test.ftl"),
                StandardCharsets.UTF_8);

        setUserProperty("from@example.com", "notifyTemplate", template);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        String mime = MailUtils.partToMimeString(sentMail.getMsg());

        assertTrue(mime.contains("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"));

        assertEquals("=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?= <from@example.com>",
                StringUtils.join(EmailAddressUtils.getFromNonStrict(sentMail.getMsg()), ","));

        assertEquals("=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?= <to@example.com>,other-to@example.com",
                StringUtils.join(EmailAddressUtils.getRecipientsNonStrict(sentMail.getMsg(),
                        Message.RecipientType.TO), ","));

        assertEquals("=?UTF-8?Q?=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D?= <cc@example.com>,other-cc@example.com",
                StringUtils.join(EmailAddressUtils.getRecipientsNonStrict(sentMail.getMsg(),
                        Message.RecipientType.CC), ","));
    }

    /*
     * Tests whether the TemplateServices instance is available from the template
     */
    @Test
    public void testTemplateServicesContributor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("templateProperty", "testTemplate")
                .setProperty("recipients", "test@example.com")
                .setProperty("from", "${sameAsMessage}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setFrom(new InternetAddress("from@example.com"));
        message.setText("test body");
        message.setSubject("test subject");
        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(message)
                .build();

        String template =
                """
                Content-Type: text/plain\r
                Content-Transfer-Encoding: 7bit\r
                Mime-Version: 1.0\r
                \r
                <#assign originatorProperties = application.getPropertiesInstance(originator, "com.ciphermail.core.app.properties.UserPropertiesImpl")>
                Some body\r
                ${originatorProperties.comment}\r
                """;

        setUserProperty("from@example.com", "testTemplate", template);
        setUserProperty("from@example.com", "comment", "some comment");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MailUtils.validateMessage(sentMail.getMsg());

        String body = (String) sentMail.getMsg().getContent();

        assertEquals("Some body\r\nsome comment\r\n", body);
    }

    /*
     * Tests whether the TemplateServices instance is available from the template
     */
    @Test
    public void testCoreApplicationMailAttributesContributor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .setProperty("template", "encryption-notification.ftl")
                .setProperty("templateProperty", "testTemplate")
                .setProperty("recipients", "test@example.com")
                .setProperty("from", "${sameAsMessage}")
                .mailetContext(mailContext)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setFrom(new InternetAddress("from@example.com"));
        message.setText("test body");
        message.setSubject("test subject");
        message.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("test@example.com")
                .mimeMessage(message)
                .build();

        String template =
                """
                Content-Type: text/plain\r
                Content-Transfer-Encoding: 7bit\r
                Mime-Version: 1.0\r
                \r
                <#assign violations = mailAttributes.getPolicyViolations(mail)>
                <#list violations as violation>
                - ${qp((violation!"")?truncate(900))}
                </#list>
                """;

        setUserProperty("from@example.com", "testTemplate", template);

        CoreApplicationMailAttributes.setPolicyViolations(mail, List.of(
                new PolicyViolation("policy1", "rule1", "match1",
                        PolicyViolationAction.MUST_ENCRYPT, false),
                new PolicyViolation("policy2", "rule2", "match2",
                        PolicyViolationAction.BLOCK, true)));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MailUtils.validateMessage(sentMail.getMsg());

        String body = (String) sentMail.getMsg().getContent();

        assertEquals("""
                - Policy: policy1, Rule: rule1, Priority: MUST_ENCRYPT, Match: match1
                - Policy: policy2, Rule: rule2, Priority: BLOCK, Match: match2
                """, body);
    }
}
