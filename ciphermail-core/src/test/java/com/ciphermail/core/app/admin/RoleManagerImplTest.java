/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RoleManagerImplTest
{
    @Autowired
    private RoleManager roleManager;

    @Autowired
    private TransactionOperations transactionOperations;

    @Before
    public void setup() {
        transactionOperations.executeWithoutResult(status -> roleManager.deleteAllRoles());
    }

    @Test
    public void testAddAuthority()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Role authority = roleManager.getRole("TEST");

            assertNull(authority);

            authority = roleManager.persistRole(roleManager.createRole("TEST"));

            assertNotNull(authority);
            assertEquals("TEST", authority.getName());
            assertEquals("TEST", authority.toString());

            authority = roleManager.getRole("TEST");

            assertNotNull(authority);
            assertEquals("TEST", authority.getName());

            assertEquals(authority, roleManager.getRole("TEST"));

            assertNotEquals(authority, roleManager.persistRole(
                    roleManager.createRole("TEST2")));
            assertEquals(authority.hashCode(), roleManager.getRole("TEST").hashCode());
        });
    }

    @Test
    public void testDelete()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            Role authority = roleManager.persistRole(roleManager.createRole("TEST"));

            assertNotNull(authority);
            assertEquals("TEST", authority.getName());

            assertTrue(roleManager.deleteRole("TEST"));
            assertFalse(roleManager.deleteRole("TEST"));
        });
    }

    @Test
    public void testCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            assertEquals(0, roleManager.getRoleCount());

            roleManager.persistRole(roleManager.createRole("TEST1"));

            assertEquals(1, roleManager.getRoleCount());

            roleManager.persistRole(roleManager.createRole("TEST2"));

            assertEquals(2, roleManager.getRoleCount());
        });
    }

    @Test
    public void testAddExisting()
    {
        transactionOperations.executeWithoutResult(status -> roleManager.persistRole(
                roleManager.createRole("TEST")));

        try {
            transactionOperations.executeWithoutResult(status -> roleManager.persistRole(
                    roleManager.createRole("TEST")));

            fail();
        }
        catch(DataIntegrityViolationException pe) {
             // expected
        }
    }

    @Test
    public void testGetAuthority()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            roleManager.persistRole(roleManager.createRole("TEST"));

            Role authority = roleManager.getRole("TEST");

            assertEquals("TEST", authority.getName());
        });
    }

    @Test
    public void testGetAuthories()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            roleManager.persistRole(roleManager.createRole("TEST1"));
            roleManager.persistRole(roleManager.createRole("TEST2"));
            roleManager.persistRole(roleManager.createRole("TEST3"));
            roleManager.persistRole(roleManager.createRole("TEST4"));

            List<? extends Role> authorities = roleManager.getRoles(
                    null, null, null);

            assertEquals("TEST1", authorities.get(0).getName());
            assertEquals("TEST2", authorities.get(1).getName());
            assertEquals("TEST3", authorities.get(2).getName());
            assertEquals("TEST4", authorities.get(3).getName());

            authorities = roleManager.getRoles(
                    1, 2, null);

            assertEquals("TEST2", authorities.get(0).getName());
            assertEquals("TEST3", authorities.get(1).getName());

            authorities = roleManager.getRoles(
                    1, 2, SortDirection.DESC);

            assertEquals("TEST3", authorities.get(0).getName());
            assertEquals("TEST2", authorities.get(1).getName());

            authorities = roleManager.getRoles(
                    100, 100, SortDirection.DESC);

            assertEquals(0, authorities.size());
        });
    }
}
