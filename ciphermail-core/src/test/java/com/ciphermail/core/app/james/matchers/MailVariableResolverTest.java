/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.test.TestUtils;
import net.sourceforge.jeval.VariableResolver;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.junit.Test;

import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MailVariableResolverTest
{
    @Test
    public void testStaticVariables()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/signed-opaque-validcertificate.eml");

        MailImpl mail = MailImpl.builder().name ("test").mimeMessage(message).build();

        Collection<MailAddress> recipients = new LinkedList<>();

        recipients.add(new MailAddress("test1@example.com"));
        recipients.add(new MailAddress("test2@example.com"));
        recipients.add(new MailAddress("test3@example.com"));

        mail.setRecipients(recipients);

        VariableResolver resolver = new MailVariableResolver(mail);

        assertEquals("12696", resolver.resolveVariable("mail.size"));
        assertEquals("3", resolver.resolveVariable("mail.recipients.size"));
        assertEquals("application/pkcs7-mime; name=smime.p7m; smime-type=signed-data",
                resolver.resolveVariable("mail.message.contentType"));
    }

    @Test
    public void testMailAttr()
    throws Exception
    {
        MimeMessage message = TestUtils.loadTestMessage("mail/signed-opaque-validcertificate.eml");

        MailImpl mail = MailImpl.builder().name ("test").mimeMessage(message).build();

        Collection<MailAddress> recipients = new LinkedList<>();

        recipients.add(new MailAddress("test1@example.com"));

        mail.setRecipients(recipients);

        mail.setAttribute("s", "abc");
        mail.setAttribute("i", 123);
        mail.setAttribute("bt", true);
        mail.setAttribute("bf", false);

        VariableResolver resolver = new MailVariableResolver(mail);

        assertEquals("'abc'", resolver.resolveVariable("mail.attribute.s"));
        assertEquals("'123'", resolver.resolveVariable("mail.attribute.i"));
        assertEquals("'true'", resolver.resolveVariable("mail.attribute.bt"));
        assertEquals("'false'", resolver.resolveVariable("mail.attribute.bf"));
        assertEquals("'null'", resolver.resolveVariable("mail.attribute.nomatch"));
        assertNull(resolver.resolveVariable("nomatch"));
    }
}
