/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.keystore.MockBasicKeyStore;
import com.ciphermail.core.common.security.smime.MicAlgStyle;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.security.smime.SMIMESigningAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEType;
import com.ciphermail.core.common.security.smime.SMIMEUtils;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.security.auth.x500.X500Principal;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMIMESignTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                // this test requires that AlwaysUseFreshestSigningCert is disabled
                createSMIMEProperties(globalPreferencesManager.getGlobalUserPreferences().getProperties())
                        .setAlwaysUseFreshestSigningCert(false);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"),"test");

        importCertificates(rootStore, new File(TestUtils.getTestDataDir(),
                "certificates/mitm-test-root.cer"));

        addUserWithCertificate("m.brinkers@pobox.com", new File(TestUtils.getTestDataDir(),
                "certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer"));
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUserWithCertificate(String email, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                userWorkflow.makePersistent(user);

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        user.getUserPreferences().getCertificates().add((X509Certificate) certificate);
                    }
                }
            }
            catch(Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private List<KeyAndCertificate> getKeyAndCertificates(CertSelector certSelector)
    {
        return transactionOperations.execute(status ->
        {
            try {
                List<? extends X509CertStoreEntry> entries = CloseableIteratorUtils.toList(keyAndCertStore.getCertStoreIterator(
                        certSelector, MissingKeyAlias.ALLOWED, null, null));

                List<KeyAndCertificate> keyAndCertificates = new LinkedList<>();

                for (X509CertStoreEntry entry : entries) {
                    keyAndCertificates.add(keyAndCertStore.getKeyAndCertificate(entry));
                }

                return keyAndCertificates;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserSigningKeyAndCertificate(String email, KeyAndCertificate keyAndCertificate)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                userWorkflow.makePersistent(user);

                user.setSigningKeyAndCertificate(keyAndCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SMIMESign();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    /*
     * Sign large message and test the performance
     */
    @Test
    public void testPerformanceTest()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/test-5120K.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        Mailet mailet = createMailet(mailetConfig);

        int repeat = 10;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                    .recipients("recipient@example.com")
                    .sender("sender@example.com")
                    .mimeMessage(new MimeMessage(sourceMessage))
                    .build();

            // reset log output since we are only interested in the logs produced while handling not during init
            memoryLogAppender.reset();

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertThat(memoryLogAppender.getLogOutput(), containsString(
                    "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

            MailUtils.validateMessage(mail.getMessage());

            assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                    SMIMEUtils.dissectSigned((Multipart) mail.getMessage().getContent())[1].getContentType());

            SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                    SecurityFactoryBouncyCastle.PROVIDER_NAME);

            assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
            assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(mail.getMessage()));

            List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

            assertEquals(3, certificates.size());

            SMIMESignedInspector signedInspector = inspector.getSignedInspector();

            assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                    X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                            Digest.SHA1));
            assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                    X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                            Digest.SHA1));
            assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                    X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                            Digest.SHA1));

            assertEquals(1, inspector.getSignedInspector().getSigners().size());
            assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("S/MIME signing/sec: " + perSecond);

        // NOTE: !!! can fail on a slower system
        assertTrue("S/MIME signing too slow. !!! this can fail on a slower system !!!", perSecond > 1);
    }

    @Test
    public void testDoNotRetainMessageID()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("<123456>", sourceMessage.getMessageID());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        assertNotEquals("<123456>", newMessage.getMessageID());
        assertTrue(newMessage.getMessageID().contains("JavaMail"));
    }

    @Test
    public void testRetainMessageID()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("<123456>", sourceMessage.getMessageID());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        assertEquals("<123456>", newMessage.getMessageID());
    }

    @Test
    public void testDeprecatedContentType()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("useDeprecatedContentTypes", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DEPRECATED_DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) mail.getMessage().getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());
    }

    /**
     * Sending an email with invalid from should not result in an error
     */
    @Test
    public void testInvalidFrom()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("xxx"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());
    }

    @Test
    public void testKeyMissing()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        // Remove all keys from KeyStore and try to sign again.
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users otherwise we cannot clean the key store because of a DB constraint
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
                keyAndCertStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());
    }

    @Test
    public void testUnknownContentTransferEncoding()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-content-transfer-encoding.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MailUtils.validateMessage(mail.getMessage());
    }

    @Test
    public void testDoNotAddRoot()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("addRoot", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(2, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());
    }

    @Test
    public void testDefaultSettings()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@EXAMPLE.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        // check that no headers are signed. Only a content-type header should be added to the part
        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(2, mp.getCount());

        BodyPart part =  mp.getBodyPart(0);

        Enumeration<?> e = part.getNonMatchingHeaders(new String[]{"content-type"});

        assertFalse(e.hasMoreElements());

        assertNull(mail.getState());
    }

    @Test
    public void testSHA512Opaque()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "SHA512_WITH_RSA")
                .setProperty("signMode", "opaque")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@EXAMPLE.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA512_WITH_RSA; Sign mode: OPAQUE"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.OPAQUE_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA512.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());
    }

    @Test
    public void testProtectedHeaders()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("protectedHeader", "subject,to,from")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@EXAMPLE.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) mail.getMessage().getContent())[1].getContentType());

        // check if some headers are signed
        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(2, mp.getCount());

        BodyPart part =  mp.getBodyPart(0);

        // there should be 3 non content-type headers
        Enumeration<?> e = part.getNonMatchingHeaders(new String[]{"content-type"});

        int count = 0;
        while(e.hasMoreElements()) {e.nextElement(); count ++;}
        assertEquals(3, count);

        assertEquals("test simple message", StringUtils.join(part.getHeader("subject"), ","));
        assertEquals("test@EXAMPLE.com", StringUtils.join(part.getHeader("from"), ","));
        assertEquals("test@example.com", StringUtils.join(part.getHeader("to"), ","));
    }

    @Test
    public void testExplicitUserSigningCertificate()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // Find a certificate with critical EMAILPROTECTION extension
        X509CertSelector selector = new X509CertSelector();

        selector.setSerialNumber(BigIntegerUtils.hexDecode("1178C3B653829E895ACB7100EB1F627"));

        selector.setIssuer(new X500Principal(
                "EMAILADDRESS=ca@example.com, CN=MITM Test CA, L=Amsterdam, ST=NH, C=NL"));

        List<KeyAndCertificate> keyAndCertificates = getKeyAndCertificates(selector);

        assertEquals(1, keyAndCertificates.size());

        setUserSigningKeyAndCertificate("test@example.com", keyAndCertificates.get(0));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: SHA256_WITH_RSA; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("F18CC8973F9AB82A6C47448282849A72416B6DAB",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());
    }

    /*
     * This is not a unit test but is called by testSigningAlgorithmAttribute
     */
    public void sign(SMIMESigningAlgorithm signingAlgorithm, String digestOID, MicAlgStyle micAlgStyle)
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithmAttribute", "signingAlgo")
                .setProperty("micAlgStyle", micAlgStyle.toString())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        mail.setAttribute(Attribute.convertToAttribute("signingAlgo", signingAlgorithm.name()));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed. Signing algorithm: " + signingAlgorithm.name() +
                "; Sign mode: CLEAR"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals(3, signedInspector.getCertificates().size());
        assertEquals(1, signedInspector.getSigners().size());
        assertEquals(digestOID, signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        // check that no headers are signed. Only a content-type header should be added to the part
        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(2, mp.getCount());

        BodyPart part =  mp.getBodyPart(0);

        Enumeration<?> e = part.getNonMatchingHeaders(new String[]{"content-type"});

        assertFalse(e.hasMoreElements());

        // Check if the new style RFC 5751 micalg name is used as the default style
        String micalg = switch (signingAlgorithm)
        {
            case SHA1_WITH_RSA_ENCRYPTION -> micAlgStyle == MicAlgStyle.RFC3851 ? "sha1" : "sha-1";
            case SHA224_WITH_RSA_ENCRYPTION -> micAlgStyle == MicAlgStyle.RFC3851 ? "sha224" : "sha-224";
            case SHA256_WITH_RSA -> micAlgStyle == MicAlgStyle.RFC3851 ? "sha256" : "sha-256";
            case SHA384_WITH_RSA_ENCRYPTION -> micAlgStyle == MicAlgStyle.RFC3851 ? "sha384" : "sha-384";
            case SHA512_WITH_RSA -> micAlgStyle == MicAlgStyle.RFC3851 ? "sha512" : "sha-512";
            default -> null;
        };

        if (micalg != null)
        {
            assertTrue(micalg + " not found in content type: " + mail.getMessage().getContentType(),
                    mail.getMessage().getContentType().contains(micalg));
        }
    }

    @Test
    public void testSigningAlgorithmAttribute()
    throws Exception
    {
        sign(SMIMESigningAlgorithm.SHA1_WITH_RSA_ENCRYPTION, Digest.SHA1.getOID(), MicAlgStyle.RFC3851);
        sign(SMIMESigningAlgorithm.SHA224_WITH_RSA_ENCRYPTION, Digest.SHA224.getOID(), MicAlgStyle.RFC3851);
        sign(SMIMESigningAlgorithm.SHA256_WITH_RSA, Digest.SHA256.getOID(), MicAlgStyle.RFC3851);
        sign(SMIMESigningAlgorithm.SHA384_WITH_RSA_ENCRYPTION, Digest.SHA384.getOID(), MicAlgStyle.RFC3851);
        sign(SMIMESigningAlgorithm.SHA512_WITH_RSA, Digest.SHA512.getOID(), MicAlgStyle.RFC3851);

        sign(SMIMESigningAlgorithm.SHA1_WITH_RSA_ENCRYPTION, Digest.SHA1.getOID(), MicAlgStyle.RFC5751);
        sign(SMIMESigningAlgorithm.SHA224_WITH_RSA_ENCRYPTION, Digest.SHA224.getOID(), MicAlgStyle.RFC5751);
        sign(SMIMESigningAlgorithm.SHA256_WITH_RSA, Digest.SHA256.getOID(), MicAlgStyle.RFC5751);
        sign(SMIMESigningAlgorithm.SHA384_WITH_RSA_ENCRYPTION, Digest.SHA384.getOID(), MicAlgStyle.RFC5751);
        sign(SMIMESigningAlgorithm.SHA512_WITH_RSA, Digest.SHA512.getOID(), MicAlgStyle.RFC5751);
    }

    @Test
    public void testSignedProcessor()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("signedProcessor", "signed")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("signed", mail.getState());
    }

    @Test
    public void testUnknownCharset()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("signedProcessor", "signed")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-charset-xxx.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());

        assertEquals("signed", mail.getState());
    }

    @Test
    public void testSign8bitMultipart()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-multipart.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        String mime = MailUtils.partToMimeString(newMessage);

        // Check if 8bit was converted to 7bit QP
        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));
    }

    @Test
    public void testSign8bitText()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-text.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        String mime = MailUtils.partToMimeString(newMessage);

        // Check if 8bit was converted to 7bit QP
        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));
    }

    @Test
    public void testRetrySigningKeyAndCertificateTransacted()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        SMIMESign mailet = new SMIMESign()
        {
            @Override
            protected KeyAndCertificate getSigningKeyAndCertificateTransacted(InternetAddress originator)
            throws AddressException, HierarchicalPropertiesException, CertStoreException, KeyStoreException
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.getSigningKeyAndCertificateTransacted(originator);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-text.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        assertTrue(constraintViolationThrown.booleanValue());

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        String mime = MailUtils.partToMimeString(newMessage);

        // Check if 8bit was converted to 7bit QP
        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));
    }

    @Test
    public void testRetryCertificateChainTransacted()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        SMIMESign mailet = new SMIMESign()
        {
            @Override
            protected X509Certificate[] getCertificateChainTransacted(X509Certificate signingCertificate)
            {
                // throw ConstraintViolationException for first time a user is handled
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.getCertificateChainTransacted(signingCertificate);
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-text.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME signed"));

        assertTrue(constraintViolationThrown.booleanValue());

        MimeMessage newMessage = mail.getMessage();

        MailUtils.validateMessage(newMessage);

        assertEquals(SMIMEHeader.DETACHED_SIGNATURE_TYPE,
                SMIMEUtils.dissectSigned((Multipart) newMessage.getContent())[1].getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(newMessage, new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.SIGNED, inspector.getSMIMEType());
        assertEquals(SMIMEHeader.Type.CLEAR_SIGNED, SMIMEHeader.getSMIMEContentType(newMessage));

        List<X509Certificate> certificates = inspector.getSignedInspector().getCertificates();

        assertEquals(3, certificates.size());

        SMIMESignedInspector signedInspector = inspector.getSignedInspector();

        assertEquals("1C1C1CF46CC9233B23391A3B9BEF558969567091",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(0),
                        Digest.SHA1));
        assertEquals("D8F8E5B92E651B1E3EF93B5493EACDE4C13AFEE0",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(1),
                        Digest.SHA1));
        assertEquals("69D7FFAF26BD5E9E4F42083BCA077BFAA8398593",
                X509CertificateInspector.getThumbprint(signedInspector.getCertificates().get(2),
                        Digest.SHA1));

        assertEquals(1, inspector.getSignedInspector().getSigners().size());
        assertEquals(Digest.SHA256.getOID(), signedInspector.getSigners().get(0).getDigestAlgorithmOID());

        String mime = MailUtils.partToMimeString(newMessage);

        // Check if 8bit was converted to 7bit QP
        assertTrue(mime.contains("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertTrue(mime.contains("This is a test with unlauts: Sch=C3=B6n"));
    }
}
