/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.PathMatcher;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ExtractAttachmentsTest
{
    private static final File TEMP_DIR = FileUtils.getTempDirectory();

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new ExtractAttachments();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testExtractAttachments()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("initial", mail.getState());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testSetExtractedProcessor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("extractedProcessor", "next")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        assertEquals("next", mail.getState());
    }

    @Test
    public void testEncodedFilename()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/message-with-attach-encoded-filename.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_äöü ÄÖÜ.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testFilenameFormat()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        String uuid = UUID.randomUUID().toString();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("filenameFormat", uuid + ",%2$s,%3$s")
           .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/" + uuid + ",test@example.com,cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testAttachmentsFoundAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("attachmentsFoundAttribute", "attachmentsFound")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        assertTrue(mail.getAttribute(AttributeName.of("attachmentsFound"))
                .map(Attribute::getValue)
                .map(AttributeValue::value)
                .filter(Boolean.class::isInstance)
                .map(Boolean.class::cast)
                .orElseThrow());
    }

    @Test
    public void testMultipleAttachments()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/multiple-attachments.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_testJAR.jar");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_testHTML_utf8.html");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_test.xml");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_test.txt");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testDuplicateAttachment()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/duplicate-attachments.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // two files should have been extracted
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_test.txt");

        assertEquals(2, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testNoAttachments()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("attachmentsFoundAttribute", "attachmentsFound")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/8bit-text.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertFalse(mail.getAttribute(AttributeName.of("attachmentsFound"))
                .map(Attribute::getValue)
                .map(AttributeValue::value)
                .filter(Boolean.class::isInstance)
                .map(Boolean.class::cast)
                .orElseThrow());
    }

    @Test
    public void testSkipSignatures()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/openssl-signed-only-attachment.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_*");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testDoNotSkipSignatures()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("skipSignatures", "false")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/openssl-signed-only-attachment.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_*");

        assertEquals(2, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_smime.p7s");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testCreateRecipientDir()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("createRecipientDir", "true")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        String uuid = UUID.randomUUID().toString();

        InternetAddress from = new InternetAddress(uuid + "@example.com");

        message.setFrom(from);

        List<MailAddress> recipients = new LinkedList<>();

        recipients.add(new MailAddress("r1" + uuid + "@example.com"));
        recipients.add(new MailAddress("r2" + uuid + "@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients(recipients)
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertTrue(new File(TEMP_DIR.getPath(), recipients.get(0).toString()).isDirectory());
        assertEquals(1, new File(TEMP_DIR.getPath(), recipients.get(0).toString()).list().length);
        assertTrue(new File(TEMP_DIR.getPath(), recipients.get(1).toString()).exists());
        assertEquals(1, new File(TEMP_DIR.getPath(), recipients.get(1).toString()).list().length);
    }

    @Test
    public void testAlwaysExtractIfFilenameSet()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("alwaysExtractIfFilenameSet", "true")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/html-embedded-images.eml");

        // create a unique from so we can find the message in tmp dir
        InternetAddress from = new InternetAddress(UUID.randomUUID().toString() + "@example.com");

        message.setFrom(from);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_*");

        assertEquals(2, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_test.jpg");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());

        matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/*_" + from + "_key.png");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testNullFrom()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        String uuid = UUID.randomUUID().toString();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("filenameFormat", uuid + ",%2$s,%3$s")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");

        message.setFrom(null);

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        // a file with [timestamp]_[from]_[filename] should have been saved to the tmp dir
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(
                "glob:**/" + uuid + ",unknown,cityinfo.zip");

        assertEquals(1, Files.walk(TEMP_DIR.toPath(), 1).filter(matcher::matches).count());
    }

    @Test
    public void testErrorAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attachmentsDir", TEMP_DIR.getPath())
                .setProperty("errorAttribute", "error")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/8bit-text.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertFalse(mail.getAttribute(AttributeName.of("error"))
                .map(Attribute::getValue)
                .map(AttributeValue::value)
                .filter(Boolean.class::isInstance)
                .map(Boolean.class::cast)
                .orElseThrow());
    }
}
