package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistryImpl;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateBuilderImpl;
import com.ciphermail.core.common.util.URIUtils;
import org.apache.commons.lang.SystemUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.security.cert.X509Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;

public class UserPropertiesValidatorsTest
{
    private static final String PEM_KEYPAIR =
            """
            -----BEGIN RSA PRIVATE KEY-----
            MIIEowIBAAKCAQEA2BcJPqzWI4toz+1Gza6IBJBcBqck6AjPXJnenFuMuSFl0Ufu
            g9jlu4HeQH8vf7G5jK8jxuLGJlHkTmtWjX0r1nK6/xN8w0B+1953A048BEJN/neG
            cAeMEFertP9EazNgMcFuqGVXjMRObwNtxhJ3Yuyt8a6xi7qHLWE9m+FT2DP9Hfo6
            XvZsyTBpRNOKlEsbFNgx9z/4+/4gMamF9YgCEfqkvaFkvYiIR6MMIWA1D8lXlFTq
            xxbX9b7Xx3ZS7WHGW80sVvbD+81E2GPvcDvct0hMKeYUjbUlhWo6PtXIMo3rckXl
            g2nAmtPlNm4CTIJuiEkGJ3Dt60nnL03sxF7emwIDAQABAoIBAA1CFJIg3p6drheb
            0k1KWRQWm17ZIH2QFND0q+DiCRdOwkV96VdzllzT0gQZCZwHFoBYOpS4UzoEqkJZ
            eZ0Cwg7NSUXfNzdvnJi4EItGYI5OVyS2xqvW+rQB/QoE43jEVTGYUabJWP7NCJbt
            Ge4eMe8C4IVEl6YgEq0pGzOWNqO8J/I9CIFCU8lzgueGbUGASWH18uq0h5IHm9K2
            wDrM10rLo+fzhd4vydTVzIeJ6s57X202al/VmpCSgoz1xdpqcd259+YiyNJciWxn
            Vbmwaw8O3ESuaJ4XC7Vt4Xr74d74w/Rm7T1Gv5Ao13C3GQBmTRjiKqDaBgj/jCbX
            e/8sYlECgYEA+NXySXilJUJ/73iWvTV7PCKm/O1yYhVDdfwerNCpL2OewSwPwItI
            KFrAHTUGu/ERE3gvT5aJDM0MaiODhueGOTh11oJOaMvYFpXj0ySEjwwUofg2q3kd
            sFaWCJNtPifh/tZu7NjFaiRYoU8zQADIc2o60ODYEcTmy8lz56FxdgMCgYEA3k+8
            Yte/rEJrsbGq9TrOafnwNlVo2kmngseOGUi1sGNtu/6Z3aQr34kVA9kHxJooAVnZ
            XGY5urjU3RbYMlJdaVjUt40Nx0hRoxcYhKR0+s8tmIYwslP14wou/LVRLuGxqKqu
            cGivXLU67LWZgvr6ojpvAtnxG5+R1AD270HYPYkCgYBRNSFZMPJUcptl/6jSHv4U
            5goxSRynPeFCRAGOzJoqSQyqZkLNx6bI5LUF0yRbeZTlEbf+9VXMw65lhSmLgUug
            qPhsBSKpuLzQwiIT6jsIpBynr2XIGELEJb3ZFqP0QPfUPw78ZNIojawPGNXU8Wx5
            nhWLzZU2V1gq8ZfN5DYvGQKBgQDJna7QRbKDW0moQeU1ujtxSUddP2B0d8//f1OU
            Aqkka0gd8u1AYZ+Yfw4cCzxMRTJ65y2F6v/4I6CJUiqImMI2xGVGgho43ZGMQ2nt
            AelFfZdweGK5mAOsAJRUg2w5BF7HBORm9cn0XjgubRPoAg5ECpKbF2TfFHyAtM8+
            dYzTYQKBgDAlGfNMM21rSbTFvecPimFbzJSpxFGlun7Vd05wBHGxUkDuzsX5V1eF
            6fH6CUFLMdOPqfth1YHLz5SJCcuvQQnu4MPsDlqm0XKinwwF9/VqmwvLlZpW6js8
            hrDi83vSMruUnqRUQUoaPGHb+62C7plUDJNY6InztWjn6DFRmFsm
            -----END RSA PRIVATE KEY-----
            """;

    @Test
    public void validateEmail()
    throws PropertyValidationFailedException
    {
        assertEquals("test@example.com", UserPropertiesValidators.validateEmail(
                "property", "test@example.com"));
        assertNull(UserPropertiesValidators.validateEmail("property", null));
        assertEquals("PropertyValidationFailedException[message=invalid is not a valid email address," +
                     "key=backend.validation.invalid-email-address,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateEmail("property", "invalid")).toString());
    }

    @Test
    public void validateEmailList()
    throws PropertyValidationFailedException
    {
        assertEquals("test@example.com", UserPropertiesValidators.validateEmailList(
                "property", "test@example.com"));
        assertEquals("test1@example.com,test2@example.com", UserPropertiesValidators.validateEmailList(
                "property", "test1@example.com,  test2@example.com,"));
        assertNull(UserPropertiesValidators.validateEmailList("property", null));
        assertEquals("PropertyValidationFailedException[message=test@example.com,invalid contains an " +
                     "invalid email address,key=backend.validation.invalid-email-list,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateEmailList("property",
                                "test@example.com,invalid")).toString());
    }

    @Test
    public void validateRegEx()
    throws PropertyValidationFailedException
    {
        assertEquals(".*", UserPropertiesValidators.validateRegEx("property", ".*"));
        assertNull(UserPropertiesValidators.validateRegEx("property", null));
        assertEquals("PropertyValidationFailedException[message=[ is not a valid regular expression," +
                     "key=backend.validation.invalid-regex,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateRegEx("property", "[")).toString());
    }

    @Test
    public void validateJSONObject()
    throws PropertyValidationFailedException
    {
        assertEquals("{}", UserPropertiesValidators.validateJSONObject("property", "{}"));
        assertNull(UserPropertiesValidators.validateJSONObject("property", null));
        assertEquals("PropertyValidationFailedException[message=value is not a valid JSON object," +
                     "key=backend.validation.invalid-json,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateJSONObject("property", "test")).toString());
    }

    @Test
    public void validateFormatString()
    throws PropertyValidationFailedException
    {
        assertEquals("test %s", UserPropertiesValidators.validateFormatString(
                "property", "test %s", "value"));
        assertEquals("PropertyValidationFailedException[message=value is not a valid format string," +
                     "key=backend.validation.invalid-format-string,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateFormatString(
                                "property", "test %", "value")).toString());
    }

    @Test
    public void validateHeaderNameValue()
    throws PropertyValidationFailedException
    {
        assertEquals("name:header", UserPropertiesValidators.validateHeader("property",
                "name:header"));
        assertEquals("name:", UserPropertiesValidators.validateHeader("property",
                "name:"));
        assertEquals("name", UserPropertiesValidators.validateHeader("property",
                "name"));
        assertNull(UserPropertiesValidators.validateHeader("property", null));
        assertEquals("PropertyValidationFailedException[message=fün is not a valid header:name value," +
                     "key=backend.validation.invalid-header-name-value,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateHeader("property",
                                "fün")).toString());
        assertEquals("PropertyValidationFailedException[message=header:fün is not a valid header:name value," +
                     "key=backend.validation.invalid-header-name-value,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateHeader("property",
                                "header:fün")).toString());
    }

    @Test
    public void validatePasswordPolicy()
    throws PropertyValidationFailedException
    {
        assertEquals("{r:[{p:'^.{3,}$'}]}", UserPropertiesValidators.validatePasswordPolicy(
                "property", "{r:[{p:'^.{3,}$'}]}"));
        assertNull(UserPropertiesValidators.validatePasswordPolicy("property", null));
        assertEquals("PropertyValidationFailedException[message={invalid} is not a valid password policy," +
                     "key=backend.validation.invalid-password-policy,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validatePasswordPolicy("property",
                                "{invalid}")).toString());
    }

    @Test
    public void validateURI()
    throws PropertyValidationFailedException
    {
        assertEquals("https://www.example.com", UserPropertiesValidators.validateURI("prop",
                "https://www.example.com", URIUtils.URIType.FULL));
        assertEquals("PropertyValidationFailedException[message={invalid} is not a valid URI," +
                     "key=backend.validation.invalid-uri,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateURI("property",
                                "{invalid}", URIUtils.URIType.FULL)).toString());
    }

    @Test
    public void validateTelephoneNumber()
    throws PropertyValidationFailedException
    {
        assertEquals("+3112345678", UserPropertiesValidators.validateTelephoneNumber("prop",
                "+3112345678"));
        assertEquals("PropertyValidationFailedException[message={invalid} is not a valid phone number," +
                     "key=backend.validation.invalid-phone-number,propertyName=property,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateTelephoneNumber("property",
                                "{invalid}")).toString());
    }

    @Test
    public void validateTemplate()
    throws IOException, PropertyValidationFailedException
    {
        TemplateBuilder templateBuilder = new TemplateBuilderImpl(new File(SystemUtils.USER_DIR));

        assertEquals("test", UserPropertiesValidators.validateTemplate("prop",
                "test", templateBuilder));
        assertEquals("PropertyValidationFailedException[message=Template is not valid," +
                     "key=backend.validation.invalid-template,propertyName=property,params={Param[name=message, " +
                     "value=freemarker.core.InvalidReferenceException: The following has evaluated to null or missing:\n" +
                     "==> invalid  [in template \"ftl\" at line 1, column 3]\n" +
                     "\n" +
                     "----\n" +
                     "Tip: If the failing expression is known to legally refer to something that's sometimes null or " +
                     "missing, either specify a default value like myOptionalVar!myDefault, or use " +
                     "<#if myOptionalVar??>when-present<#else>when-missing</#if>. (These only cover the last step " +
                     "of the expression; to cover the whole expression, use parenthesis: " +
                     "(myOptionalVar.foo)!myDefault, (myOptionalVar.foo)??\n" +
                     "----\n" +
                     "\n" +
                     "----\n" +
                     "FTL stack trace (\"~\" means nesting-related):\n" +
                     "\t- Failed at: ${invalid}  [in template \"ftl\" at line 1, column 1]\n" +
                     "----]}]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateTemplate("property",
                                "${invalid}", templateBuilder)).toString());
    }

    @Test
    public void validateCertificateRequestHandler()
    throws PropertyValidationFailedException
    {
        CertificateRequestHandlerRegistry registry = new CertificateRequestHandlerRegistryImpl();

        class MockCertificateRequestHandler implements CertificateRequestHandler
        {
            @Override
            public boolean isEnabled()
            {
                return true;
            }

            @Override
            public boolean isInstantlyIssued()
            {
                return false;
            }

            @Override
            public String getCertificateHandlerName()
            {
                return "test";
            }

            @Override
            public KeyAndCertificate handleRequest(CertificateRequest request)
            {
                return null;
            }

            @Override
            public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate)
            {
                return null;
            }

            @Override
            public void cleanup(CertificateRequest request)
            {
                // empty on purpose
            }
        }

        registry.registerHandler(new MockCertificateRequestHandler());

        assertEquals("test", UserPropertiesValidators.validateCertificateRequestHandler("prop",
                "test", registry));

        assertNull(UserPropertiesValidators.validateCertificateRequestHandler("prop",
                null, registry));

        assertEquals("PropertyValidationFailedException[message=Certificate Request Handler with name " +
                     "unknown does not exist,key=backend.validation.invalid-certificate-request-handler," +
                     "propertyName=prop,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateCertificateRequestHandler("prop",
                                "unknown", registry)).toString());
    }

    @Test
    public void validateDKIMSignatureTemplate()
    throws PropertyValidationFailedException
    {
        assertEquals("v=1; c=relaxed/relaxed; s=ciphermail; d=example.com; " +
                     "h=From:Subject:To:Date; a=rsa-sha256; t=; bh=; b=;",
                UserPropertiesValidators.validateDKIMSignatureTemplate("prop",
                        "v=1; c=relaxed/relaxed; s=ciphermail; d=example.com; " +
                        "h=From:Subject:To:Date; a=rsa-sha256; t=; bh=; b=;"));
        assertNull(UserPropertiesValidators.validateDKIMSignatureTemplate("prop", null));

        assertEquals("PropertyValidationFailedException[message=DKIM signature template test is invalid," +
                     "key=backend.validation.invalid-dkim-template,propertyName=prop,params={}]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateDKIMSignatureTemplate("prop",
                                "test")).toString());
    }

    @Test
    public void validateKeyPair()
    throws PropertyValidationFailedException
    {
        assertEquals(PEM_KEYPAIR, UserPropertiesValidators.validateKeyPair("prop", PEM_KEYPAIR));

        assertEquals("PropertyValidationFailedException[message=PEM object is not a key pair," +
                     "key=backend.validation.invalid-key-pair,propertyName=prop,params=<null>]",
                assertThrows(PropertyValidationFailedException.class, () ->
                        UserPropertiesValidators.validateKeyPair("prop",
                                "test")).toString());
    }
}