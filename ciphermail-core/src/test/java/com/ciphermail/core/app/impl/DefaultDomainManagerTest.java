/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import jakarta.persistence.PersistenceException;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class  DefaultDomainManagerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private UserWorkflow userWorkflow;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users prior to deleting domains
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                domainManager.deleteAllDomains();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertEquals(0, domainManager.getDomainCount());
                assertNotNull(domainManager.addDomain("exAMPle.com"));
                assertEquals(1, domainManager.getDomainCount());

                UserPreferences prefs = domainManager.getDomainPreferences("example.com");

                assertNotNull(prefs);
                assertEquals(UserPreferencesCategory.DOMAIN.name(), prefs.getCategory());
                assertEquals("example.com", prefs.getName());
                assertNotNull(domainManager.addDomain("example2.com"));
                assertEquals(2, domainManager.getDomainCount());
                List<String> domains = CloseableIteratorUtils.toList(domainManager.getDomainIterator(null,
                        null, null));
                assertEquals(2, domains.size());
                assertEquals("example.com", domains.get(0));
                assertEquals("example2.com", domains.get(1));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testInUse()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                assertFalse(domainManager.isDomainInUse("example.com"));
                assertNotNull(domainManager.addDomain("example.com"));
                assertFalse(domainManager.isDomainInUse("example.com"));
                userWorkflow.makePersistent(userWorkflow.getUser("test@example.com",
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST));
                assertTrue(domainManager.isDomainInUse("example.com"));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences prefs = domainManager.addDomain("*.example.com");

                assertNotNull(prefs);
                assertEquals(UserPreferencesCategory.DOMAIN.name(), prefs.getCategory());
                assertEquals("*.example.com", prefs.getName());

                Set<UserPreferences> inherited = prefs.getInheritedUserPreferences();

                assertEquals(1, inherited.size());

                assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddWildcardDomainAfterDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences domainPrefs = domainManager.addDomain("example.com");
                UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com");

                UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com");

                Set<UserPreferences> inherited = wildcardPrefs.getInheritedUserPreferences();

                assertEquals(1, inherited.size());

                assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

                inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDomainAfterWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com");

                UserPreferences domainPrefs = domainManager.addDomain("example.com");
                UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com");

                Set<UserPreferences> inherited = wildcardPrefs.getInheritedUserPreferences();

                assertEquals(1, inherited.size());

                assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

                inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddLowerPrioWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences wildcardPrefs = domainManager.addDomain("*.sub.Example.com");

                UserPreferences domainPrefs = domainManager.addDomain("eXAmple.com");
                UserPreferences subDomainPrefs = domainManager.addDomain("sub.examPLE.com");

                Set<UserPreferences> inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should inherit from global
                assertEquals(1, inherited.size());

                assertEquals(globalPreferencesManager.getGlobalUserPreferences(), inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                // Now add a lower prio wildcard domain
                UserPreferences lowerPrioWildcardPrefs = domainManager.addDomain("*.example.COM");

                inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should inherit from lower prio wildcard
                assertEquals(1, inherited.size());

                assertEquals(lowerPrioWildcardPrefs, inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should still inherit from 'old' wildcard prefs and not the new one
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddHigherPrioWildcardDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences wildcardPrefs = domainManager.addDomain("*.example.com");

                UserPreferences domainPrefs = domainManager.addDomain("example.com");
                UserPreferences subDomainPrefs = domainManager.addDomain("sub.example.com");

                Set<UserPreferences> inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should inherit from wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                // Now add a higher prio wildcard domain
                UserPreferences higherPrioWildcardPrefs = domainManager.addDomain("*.sub.example.com");

                inherited = domainPrefs.getInheritedUserPreferences();
                // example.com should still inherit from 'old' wildcard
                assertEquals(1, inherited.size());

                assertEquals(wildcardPrefs, inherited.iterator().next());

                inherited = subDomainPrefs.getInheritedUserPreferences();
                // sub.example.com should inherit from higher wildcard prefs
                assertEquals(1, inherited.size());

                assertEquals(higherPrioWildcardPrefs, inherited.iterator().next());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test(expected=PersistenceException.class)
    public void testAddExistingDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                domainManager.addDomain("exAMPle.com");
            }
            catch (CloseableIteratorException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                domainManager.addDomain("example.com");
            }
            catch (HierarchicalPropertiesException | CloseableIteratorException e) {
                throw new RuntimeException(e);
            }

            fail();
        });

        fail();
    }

    @Test
    public void testDeleteDomain()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                domainManager.addDomain("exAMPle.com");
                domainManager.addDomain("123.com");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            assertTrue(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("xxx")));
            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
        });

        transactionOperations.executeWithoutResult(status ->
        {
            assertFalse(domainManager.deleteDomainPreferences(domainManager.getDomainPreferences("example.com")));
            assertNotNull(domainManager.getDomainPreferences("123.com"));
        });
    }
}
