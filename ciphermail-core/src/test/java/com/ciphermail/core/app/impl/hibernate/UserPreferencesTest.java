/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.UserPreferencesManager;
import com.ciphermail.core.app.impl.NamedCertificateImpl;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableObject;
import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPreferencesTest
{
    private static final String NAMED_BLOB_CATEGORY = "test_category";
    private static final String NAMED_BLOB_1 = "blob1";
    private static final String NAMED_BLOB_2 = "blob2";
    private static final String NAMED_BLOB_3 = "blob3";

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private NamedBlobManager namedBlobManager;

    @Autowired
    private UserPreferencesCategoryManager userPreferencesCategoryManager;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    @Before
    public void setup()
    {
        memoryLogAppender.reset();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                removeAllUserPreferences();

                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

                KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

                keyStore.load(new FileInputStream(new File(TestUtils.getTestDataDir(),
                                "keys/testCertificates.p12")), "test".toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

                assertEquals(22, keyAndCertStore.size());

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                        new File(TestUtils.getTestDataDir(),"certificates/mitm-test-root.cer"));

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        rootStore.addCertificate((X509Certificate) certificate);
                    }
                }
                assertEquals(1, rootStore.size());

                namedBlobManager.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // For some reason we cannot add named blobs in the same transaction where we delete all named blobs
        // (using NamedBlobManager#deleteAll). If we do we get a constraint violation error. It appears that Hibernate
        // first inserts and then deletes (https://mossgreen.github.io/JPA-parent-children-updating-violates-constraint)
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);
                namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);
                namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_3);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void removeAllUserPreferences()
    throws CloseableIteratorException
    {
        CloseableIterator<String> categoryIterator = userPreferencesCategoryManager.getCategoryIterator();

        // Because user preferences can contain loops we first have to clear the inherited collections before removing.
        try {
            // first remove all inherited
            while (categoryIterator.hasNext())
            {
                String category = categoryIterator.next();

                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();

                try {
                    while (nameIterator.hasNext())
                    {
                        String name = nameIterator.next();

                        UserPreferences prefs = userPreferencesManager.getUserPreferences(name);

                        prefs.getInheritedUserPreferences().clear();
                    }
                }
                finally {
                    nameIterator.close();
                }
            }
        }
        finally {
            categoryIterator.close();
        }

        categoryIterator = userPreferencesCategoryManager.getCategoryIterator();

        try {
            // now delete all prefs from every category
            while (categoryIterator.hasNext())
            {
                String category = categoryIterator.next();

                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();

                try {
                    while (nameIterator.hasNext())
                    {
                        String name = nameIterator.next();

                        UserPreferences userPreferences = userPreferencesManager.getUserPreferences(name);

                        assertTrue(userPreferencesManager.deleteUserPreferences(userPreferences));
                    }
                }
                finally {
                    nameIterator.close();
                }
            }
        }
        finally {
            categoryIterator.close();
        }
    }

    @Test
    public void testAddInheritedNamedCertificates()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

                File file = new File(TestUtils.getTestDataDir(), "certificates/equifax.cer");

                X509Certificate certificate = TestUtils.loadCertificate(file);

                NamedCertificate namedCertificate = new NamedCertificateImpl("name", certificate);

                userPreferences.getNamedCertificates().add(namedCertificate);

                UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

                file = new File(TestUtils.getTestDataDir(), "certificates/rim.cer");

                X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);

                NamedCertificate inheritedNamedCertificate = new NamedCertificateImpl("inherited",
                        inheritedCertificate);

                inheritedPreferences.getNamedCertificates().add(inheritedNamedCertificate);

                UserPreferences inheritedInheritedPreferences = userPreferencesManager.addUserPreferences(
                        "inherited inherited prefs");

                file = new File(TestUtils.getTestDataDir(), "certificates/intel-crl-test.cer");

                X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);

                NamedCertificate inheritedInheritedNamedCertificate = new NamedCertificateImpl("inheritedinherited",
                        inheritedInheritedCertificate);

                inheritedInheritedPreferences.getNamedCertificates().add(inheritedInheritedNamedCertificate);

                userPreferences.getInheritedUserPreferences().add(inheritedPreferences);

                inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);

                assertEquals(1, userPreferences.getInheritedUserPreferences().size());

                assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());

                assertTrue(userPreferences.getNamedCertificates().contains(namedCertificate));
                assertEquals(1, userPreferences.getNamedCertificates().size());

                assertTrue(inheritedPreferences.getNamedCertificates().contains(inheritedNamedCertificate));
                assertEquals(1, inheritedPreferences.getNamedCertificates().size());

                assertTrue(userPreferences.getInheritedNamedCertificates().contains(inheritedNamedCertificate));
                assertTrue(userPreferences.getInheritedNamedCertificates().contains(
                        inheritedInheritedNamedCertificate));
                assertEquals(2, userPreferences.getInheritedNamedCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testRemoveNamedCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = certificates.iterator().next();

                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                assertEquals(0, userPreferences.getNamedCertificates().size());

                userPreferences.getNamedCertificates().add(namedCertificate);

                assertEquals(1, userPreferences.getNamedCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = certificates.iterator().next();

                assertEquals(1, userPreferences.getNamedCertificates().size());

                NamedCertificate toRemove = new NamedCertificateImpl("test", certificate);

                userPreferences.getNamedCertificates().remove(toRemove);

                assertEquals(0, userPreferences.getNamedCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager("category");

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("test");

            assertEquals(0, userPreferences.getNamedCertificates().size());
        });
    }

    @Test
    public void testAddDuplicateCertificate()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> allCertificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = allCertificates.iterator().next();

                Set<X509Certificate> certificates = userPreferences.getCertificates();

                assertEquals(0, certificates.size());

                certificates.add(certificate);

                assertEquals(1, certificates.size());
                assertEquals(1, userPreferences.getCertificates().size());

                allCertificates = keyAndCertStore.getCertificates(null);

                X509Certificate duplicateCertificate = allCertificates.iterator().next();

                assertEquals(certificate, duplicateCertificate);
                assertNotEquals(System.identityHashCode(certificate), System.identityHashCode(duplicateCertificate));

                certificates.add(certificate);
                assertEquals(1, certificates.size());
                assertEquals(1, userPreferences.getCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddNamedCertificate()
    {
        final MutableObject certificateHolder = new MutableObject();
        final MutableObject namedCertificateHolder = new MutableObject();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = certificates.iterator().next();

                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                certificateHolder.setValue(certificate);
                namedCertificateHolder.setValue(namedCertificate);

                assertEquals(0, userPreferences.getNamedCertificates().size());

                userPreferences.getNamedCertificates().add(namedCertificate);

                assertEquals(1, userPreferences.getNamedCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager("category");

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("test");

            assertNotNull(userPreferences);

            assertEquals(1, userPreferences.getNamedCertificates().size());

            NamedCertificate retrieved = userPreferences.getNamedCertificates().iterator().next();

            assertEquals("test", retrieved.getName());
            assertEquals(certificateHolder.getValue(), retrieved.getCertificate());
            assertEquals(namedCertificateHolder.getValue(), retrieved);
        });
    }

    @Test
    public void testAddDuplicateNamedCertificate()
    {
        final MutableObject certificateHolder = new MutableObject();
        final MutableObject namedCertificateHolder = new MutableObject();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = certificates.iterator().next();

                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                certificateHolder.setValue(certificate);
                namedCertificateHolder.setValue(namedCertificate);

                certificates = keyAndCertStore.getCertificates(null);

                X509Certificate duplicateCertificate = certificates.iterator().next();

                NamedCertificate duplicateNamedCertificate = new NamedCertificateImpl("test", duplicateCertificate);

                assertEquals(0, userPreferences.getNamedCertificates().size());

                userPreferences.getNamedCertificates().add(namedCertificate);
                userPreferences.getNamedCertificates().add(duplicateNamedCertificate);

                assertEquals(1, userPreferences.getNamedCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager("category");

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("test");

            assertNotNull(userPreferences);

            assertEquals(1, userPreferences.getNamedCertificates().size());

            NamedCertificate retrieved = userPreferences.getNamedCertificates().iterator().next();

            assertEquals("test", retrieved.getName());
            assertEquals(certificateHolder.getValue(), retrieved.getCertificate());
            assertEquals(namedCertificateHolder.getValue(), retrieved);
        });
    }

    @Test
    public void testAddMultipleNamedCertificates()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                Iterator<X509Certificate> it = certificates.iterator();

                X509Certificate certificate = it.next();
                X509Certificate otherCertificate = it.next();

                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                NamedCertificate namedCertificate2 = new NamedCertificateImpl("test2", certificate);

                NamedCertificate namedCertificate3 = new NamedCertificateImpl("test2", otherCertificate);

                assertEquals(0, userPreferences.getNamedCertificates().size());

                userPreferences.getNamedCertificates().add(namedCertificate);
                userPreferences.getNamedCertificates().add(namedCertificate2);
                userPreferences.getNamedCertificates().add(namedCertificate3);

                assertEquals(3, userPreferences.getNamedCertificates().size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager("category");

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("test");

            assertNotNull(userPreferences);

            assertEquals(3, userPreferences.getNamedCertificates().size());
        });
    }

    @Test
    public void testAddDuplicateNamedCertificateSingleCollection()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category");

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("test");

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                X509Certificate certificate = certificates.iterator().next();

                NamedCertificate namedCertificate = new NamedCertificateImpl("test", certificate);

                certificates = keyAndCertStore.getCertificates(null);

                X509Certificate duplicateCertificate = certificates.iterator().next();

                NamedCertificate duplicateNamedCertificate = new NamedCertificateImpl("test", duplicateCertificate);

                Set<NamedCertificate> namedCertificates = userPreferences.getNamedCertificates();

                assertEquals(0, namedCertificates.size());

                namedCertificates.add(namedCertificate);
                namedCertificates.add(duplicateNamedCertificate);
                assertTrue(namedCertificates.contains(namedCertificate));
                assertTrue(namedCertificates.contains(duplicateNamedCertificate));

                assertEquals(1, namedCertificates.size());
            }
            catch (CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddSameCertificateOtherPreferences()
    {
        String category = "category";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("pref1");

            File file = new File(TestUtils.getTestDataDir(), "certificates/equifax.cer");

            X509Certificate certificate = TestUtils.loadCertificate(file);

            userPreferences.getCertificates().add(certificate);

            UserPreferences userPreferences2 = userPreferencesManager.addUserPreferences("pref2");

            userPreferences2.getCertificates().add(certificate);
        });
    }

    @Test
    public void testInUse()
    {
        String category = "category-2";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

            assertFalse(userPreferencesManager.isInUse(userPreferences));
            assertFalse(userPreferencesManager.isInUse(inheritedPreferences));

            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("main prefs");

            UserPreferences inheritedPreferences = userPreferencesManager.getUserPreferences("inherited prefs");

            assertFalse(userPreferencesManager.isInUse(userPreferences));
            assertTrue(userPreferencesManager.isInUse(inheritedPreferences));
        });
    }

    @Test
    public void testInheritedOrder()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager.getUserPreferences("test1"));

            UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager.getUserPreferences("test2"));

            UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");
            assertNotNull(prefs3);
            assertNotNull(userPreferencesManager.getUserPreferences("test3"));

            UserPreferences prefs4 = userPreferencesManager.addUserPreferences("test4");
            assertNotNull(prefs4);
            assertNotNull(userPreferencesManager.getUserPreferences("test4"));

            prefs1.getInheritedUserPreferences().add(prefs2);
            prefs1.getInheritedUserPreferences().add(prefs3);
            prefs1.getInheritedUserPreferences().add(prefs4);

            assertEquals(3, prefs1.getInheritedUserPreferences().size());
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences prefs1 = userPreferencesManager.getUserPreferences("test1");

            Set<UserPreferences> inherited = prefs1.getInheritedUserPreferences();

            Iterator<UserPreferences> prefsIt = inherited.iterator();

            assertEquals("test2", prefsIt.next().getName());
            assertEquals("test3", prefsIt.next().getName());
            assertEquals("test4", prefsIt.next().getName());
        });
        transactionOperations.executeWithoutResult(status ->
        {
            // now reorder
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences prefs1 = userPreferencesManager.getUserPreferences("test1");

            Set<UserPreferences> inherited = prefs1.getInheritedUserPreferences();

            Iterator<UserPreferences> prefsIt = inherited.iterator();

            UserPreferences prefs2 = prefsIt.next();
            UserPreferences prefs3 = prefsIt.next();
            UserPreferences prefs4 = prefsIt.next();

            prefs1.getInheritedUserPreferences().clear();

            prefs1.getInheritedUserPreferences().add(prefs4);
            prefs1.getInheritedUserPreferences().add(prefs3);
            prefs1.getInheritedUserPreferences().add(prefs2);
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences prefs1 = userPreferencesManager.getUserPreferences("test1");

            Set<UserPreferences> inherited = prefs1.getInheritedUserPreferences();

            Iterator<UserPreferences> prefsIt = inherited.iterator();

            assertEquals("test4", prefsIt.next().getName());
            assertEquals("test3", prefsIt.next().getName());
            assertEquals("test2", prefsIt.next().getName());
        });
    }

    @Test
    public void testCategoryIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager1 = userPreferencesCategoryManager.
                    getUserPreferencesManager("category-1");

            UserPreferences prefs1 = userPreferencesManager1.addUserPreferences("test1");
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager1.getUserPreferences("test1"));
            assertEquals("category-1", prefs1.getCategory());

            UserPreferences prefs2 = userPreferencesManager1.addUserPreferences("test2");
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager1.getUserPreferences("test2"));

            UserPreferencesManager userPreferencesManager2 = userPreferencesCategoryManager.
                    getUserPreferencesManager("category-2");

            prefs1 = userPreferencesManager2.addUserPreferences("test1");
            assertNotNull(prefs1);
            assertNotNull(userPreferencesManager2.getUserPreferences("test1"));

            prefs2 = userPreferencesManager2.addUserPreferences("test2");
            assertNotNull(prefs2);
            assertNotNull(userPreferencesManager2.getUserPreferences("test2"));
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                List<String> names = new LinkedList<>();

                CloseableIterator<String> nameIterator = userPreferencesCategoryManager.getCategoryIterator();

                try {
                    while (nameIterator.hasNext())
                    {
                        names.add(nameIterator.next());
                    }
                }
                finally {
                    nameIterator.close();
                }

                assertEquals(2, names.size());

                names.clear();

                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager("category-1");

                nameIterator = userPreferencesManager.getNameIterator();

                try {
                    while (nameIterator.hasNext())
                    {
                        names.add(nameIterator.next());
                    }
                }
                finally {
                    nameIterator.close();
                }

                assertEquals(2, names.size());
            }
            catch(CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUserPreferencesLoop()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");
                assertNotNull(prefs1);
                assertNotNull(userPreferencesManager.getUserPreferences("test1"));

                UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");
                assertNotNull(prefs2);
                assertNotNull(userPreferencesManager.getUserPreferences("test2"));

                UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");
                assertNotNull(prefs3);
                assertNotNull(userPreferencesManager.getUserPreferences("test3"));

                prefs1.getInheritedUserPreferences().add(prefs2);
                prefs2.getInheritedUserPreferences().add(prefs3);
                prefs3.getInheritedUserPreferences().add(prefs1);

                assertEquals(1, prefs1.getInheritedUserPreferences().size());

                prefs2.getProperties().setProperty("prop", "123");
                prefs3.getProperties().setProperty("prop", "456");

                assertEquals("123", prefs1.getProperties().getProperty("prop"));

                assertEquals(0, prefs1.getInheritedCertificates().size());

                assertEquals(0, prefs1.getInheritedKeyAndCertificates().size());
                assertEquals(0, prefs1.getInheritedNamedCertificates().size());

                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected calling #getProperties (token: 1category-1:test2)"));
                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected calling #getProperties (token: 1category-1:test3)"));
                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected calling #getProperties (token: 1category-1:test1)"));
                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected calling #getInheritedCertificates (token: 2category-1:test1)"));
                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected with token: 3category-1:test1"));
                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Loop detected calling #getInheritedNamedCertificates (token: 4category-1:test1)"));
            }
            catch(HierarchicalPropertiesException | CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testUserPreferencesLoopDirect()
    {
        String category = "category-2";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");
                assertNotNull(prefs1);
                assertNotNull(userPreferencesManager.getUserPreferences("test1"));

                prefs1.getInheritedUserPreferences().add(prefs1);

                assertEquals(1, prefs1.getInheritedUserPreferences().size());

                assertEquals(0, prefs1.getInheritedCertificates().size());
                assertEquals(0, prefs1.getCertificates().size());

                assertEquals(0, prefs1.getInheritedKeyAndCertificates().size());
            }
            catch(CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testReorderUserPreferences()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences prefs1 = userPreferencesManager.addUserPreferences("test1");
                assertNotNull(prefs1);
                assertNotNull(userPreferencesManager.getUserPreferences("test1"));

                UserPreferences prefs2 = userPreferencesManager.addUserPreferences("test2");
                assertNotNull(prefs2);
                assertNotNull(userPreferencesManager.getUserPreferences("test2"));

                UserPreferences prefs3 = userPreferencesManager.addUserPreferences("test3");
                assertNotNull(prefs3);
                assertNotNull(userPreferencesManager.getUserPreferences("test3"));

                prefs1.getInheritedUserPreferences().add(prefs2);
                prefs1.getInheritedUserPreferences().add(prefs3);

                assertEquals(2, prefs1.getInheritedUserPreferences().size());

                prefs2.getProperties().setProperty("prop", "123");
                prefs3.getProperties().setProperty("prop", "456");

                assertEquals("456", prefs1.getProperties().getProperty("prop"));
                assertTrue(prefs1.getProperties().isInherited("prop"));

                // now change order
                prefs1.getInheritedUserPreferences().clear();
                prefs1.getInheritedUserPreferences().add(prefs3);
                prefs1.getInheritedUserPreferences().add(prefs2);

                assertEquals("123", prefs1.getProperties().getProperty("prop"));
                assertTrue(prefs1.getProperties().isInherited("prop"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddInheritedProperties()
    {
        String category = "category-2";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

                UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

                UserPreferences inheritedInheritedPreferences = userPreferencesManager.
                        addUserPreferences("inherited inherited prefs");

                userPreferences.getInheritedUserPreferences().add(inheritedPreferences);

                inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);

                assertEquals(1, userPreferences.getInheritedUserPreferences().size());

                assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());

                userPreferences.getProperties().setProperty("prop", "123");
                inheritedPreferences.getProperties().setProperty("inherited prop", "456");
                inheritedInheritedPreferences.getProperties().setProperty("inherited inherited prop", "789");

                assertEquals("123", userPreferences.getProperties().getProperty("prop"));
                assertEquals("456", userPreferences.getProperties().getProperty("inherited prop"));
                assertEquals("789", userPreferences.getProperties().getProperty("inherited inherited prop"));

                assertFalse(userPreferences.getProperties().isInherited("prop"));
                assertTrue(userPreferences.getProperties().isInherited("inherited prop"));
                assertTrue(userPreferences.getProperties().isInherited("inherited inherited prop"));
                assertTrue(userPreferences.getProperties().isInherited("non existing property"));

                assertFalse(inheritedPreferences.getProperties().isInherited("inherited prop"));
                assertTrue(inheritedPreferences.getProperties().isInherited("inherited inherited prop"));

                assertFalse(inheritedInheritedPreferences.getProperties().isInherited("inherited inherited prop"));

                // set property with same name on inherited
                inheritedInheritedPreferences.getProperties().setProperty("prop", "ABC");
                assertEquals("123", userPreferences.getProperties().getProperty("prop"));
                assertFalse(userPreferences.getProperties().isInherited("prop"));

                userPreferences.getProperties().deleteProperty("prop");
                assertEquals("ABC", userPreferences.getProperties().getProperty("prop"));
                assertTrue(userPreferences.getProperties().isInherited("prop"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetExplicitlySetProperties()
    {
        String category = "category-2";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

                UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

                userPreferences.getInheritedUserPreferences().add(inheritedPreferences);

                // set a property on the user preferences
                userPreferences.getProperties().setProperty("p1", "v1");

                // set a property on the inherited
                inheritedPreferences.getProperties().setProperty("p2", "v2");

                // both properties should be available when asking for the properties
                assertEquals("v1", userPreferences.getProperties().getProperty("p1"));
                assertEquals("v2", userPreferences.getProperties().getProperty("p2"));

                // p1 should be available in the explicitly set properties
                assertEquals("v1", userPreferences.getProperties().getProperty("p1"));

                // but not p2
                assertNull(userPreferences.getExplicitlySetProperties().getProperty("p2"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddInheritedCertificates()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

            File file = new File(TestUtils.getTestDataDir(), "certificates/equifax.cer");

            X509Certificate certificate = TestUtils.loadCertificate(file);

            userPreferences.getCertificates().add(certificate);

            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

            file = new File(TestUtils.getTestDataDir(), "certificates/rim.cer");

            X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);

            inheritedPreferences.getCertificates().add(inheritedCertificate);

            UserPreferences inheritedInheritedPreferences = userPreferencesManager.
                    addUserPreferences("inherited inherited prefs");

            file = new File(TestUtils.getTestDataDir(), "certificates/intel-crl-test.cer");

            X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);

            inheritedInheritedPreferences.getCertificates().add(inheritedInheritedCertificate);

            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);

            inheritedPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);

            assertEquals(1, userPreferences.getInheritedUserPreferences().size());

            assertEquals(1, inheritedPreferences.getInheritedUserPreferences().size());

            assertTrue(userPreferences.getCertificates().contains(certificate));
            assertEquals(1, userPreferences.getCertificates().size());

            assertTrue(inheritedPreferences.getCertificates().contains(inheritedCertificate));
            assertEquals(1, inheritedPreferences.getCertificates().size());

            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedCertificate));
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedInheritedCertificate));
            assertEquals(2, userPreferences.getInheritedCertificates().size());
        });
    }

    /*
     * almost equal to testAddInheritedCertificates but now inherit at a different level
     */
    @Test
    public void testAddInheritedCertificates2()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main prefs");

            File file = new File(TestUtils.getTestDataDir(), "certificates/equifax.cer");

            X509Certificate certificate = TestUtils.loadCertificate(file);

            userPreferences.getCertificates().add(certificate);

            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited prefs");

            file = new File(TestUtils.getTestDataDir(), "certificates/rim.cer");

            X509Certificate inheritedCertificate = TestUtils.loadCertificate(file);

            inheritedPreferences.getCertificates().add(inheritedCertificate);

            UserPreferences inheritedInheritedPreferences = userPreferencesManager.
                    addUserPreferences("inherited inherited prefs");

            file = new File(TestUtils.getTestDataDir(), "certificates/intel-crl-test.cer");

            X509Certificate inheritedInheritedCertificate = TestUtils.loadCertificate(file);

            inheritedInheritedPreferences.getCertificates().add(inheritedInheritedCertificate);

            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);

            userPreferences.getInheritedUserPreferences().add(inheritedInheritedPreferences);

            assertEquals(2, userPreferences.getInheritedUserPreferences().size());

            assertEquals(0, inheritedPreferences.getInheritedUserPreferences().size());

            assertTrue(userPreferences.getCertificates().contains(certificate));
            assertEquals(1, userPreferences.getCertificates().size());

            assertTrue(inheritedPreferences.getCertificates().contains(inheritedCertificate));
            assertEquals(1, inheritedPreferences.getCertificates().size());

            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedCertificate));
            assertTrue(userPreferences.getInheritedCertificates().contains(inheritedInheritedCertificate));
            assertEquals(2, userPreferences.getInheritedCertificates().size());
        });
    }

    @Test
    public void testSetProperty()
    {
        String category = "category-1";
        String preferencesName = "Test";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                userPreferences.getProperties().setProperty("testProperty", "123");

                assertEquals("123", userPreferences.getProperties().getProperty("testProperty"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                assertEquals("123", userPreferences.getProperties().getProperty("testProperty"));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetPropertyNullValue()
    {
        String preferencesName = "Test";
        String category = "category-1";
        String propertyName = "testProperty";

        transactionOperations.executeWithoutResult(status ->
        {
            try {

                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                userPreferences.getProperties().setProperty(propertyName, "123");

                assertEquals("123", userPreferences.getProperties().getProperty(propertyName));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                assertEquals("123", userPreferences.getProperties().getProperty(propertyName));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                userPreferences.getProperties().setProperty(propertyName, null);

                assertNull(userPreferences.getProperties().getProperty(propertyName));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                assertNull(userPreferences.getProperties().getProperty(propertyName));
            }
            catch(HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddUserPreferences()
    {
        String preferencesName = "Test";
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                assertNull(userPreferences.getKeyAndCertificate());
                assertNotNull(userPreferences.getProperties());
                assertEquals(userPreferences.toString(), userPreferences.getProperties().getCategory());
                assertEquals(0, userPreferences.getProperties().getProperyNames(false).size());
                assertEquals(0, userPreferences.getProperties().getProperyNames(true).size());
                assertEquals(0, userPreferences.getCertificates().size());
                assertEquals(0, userPreferences.getInheritedCertificates().size());
                assertEquals(0, userPreferences.getInheritedKeyAndCertificates().size());

                assertNotNull(userPreferencesManager.getUserPreferences(preferencesName));

                // because the name iterator uses a stateless session we need to commit the transaction otherwise
                // the changes are not noticed by the iterator. This is kind of a pain but we need to use a
                // stateless session to make things scalable
            }
            catch(HierarchicalPropertiesException | CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();

                int count = 0;

                while (nameIterator.hasNext())
                {
                    String name = nameIterator.next();

                    assert(name != null);

                    count++;
                }

                assertEquals(1, count);
            }
            catch(CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddMultipleUserPreferences()
    {
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            userPreferencesManager.addUserPreferences("test1");

            assertNotNull(userPreferencesManager.getUserPreferences("test1"));

            userPreferencesManager.addUserPreferences("test2");

            assertNotNull(userPreferencesManager.getUserPreferences("test2"));

            // because the name iterator uses a stateless session we need to commit the transaction otherwise
            // the changes are not noticed by the iterator. This is kind of a pain but we need to use a
            // stateless session to make things scalable
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                CloseableIterator<String> nameIterator = userPreferencesManager.getNameIterator();

                int count = 0;

                while (nameIterator.hasNext())
                {
                    String name = nameIterator.next();

                    assert(name != null);

                    count++;
                }

                assertEquals(2, count);
            }
            catch(CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddCertificates()
    {
        String preferencesName = "Test";
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                Collection<X509Certificate> certificates = keyAndCertStore.getCertificates(null);

                assertEquals(22, certificates.size());

                for (X509Certificate certificate : certificates) {
                    userPreferences.getCertificates().add(certificate);
                }

                assertEquals(22, userPreferences.getCertificates().size());

                // remove the first certificate from the user
                userPreferences.getCertificates().remove(certificates.iterator().next());

                assertEquals(21, userPreferences.getCertificates().size());
            }
            catch(CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
        // check if the changes are persistent
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

            assertEquals(21, userPreferences.getCertificates().size());
        });
    }

    @Test
    public void testAddNewCertificate()
    {
        String preferencesName = "Test";
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                File file = new File(TestUtils.getTestDataDir(), "certificates/rim.cer");

                X509Certificate certificate = TestUtils.loadCertificate(file);

                assertFalse(keyAndCertStore.contains(certificate));

                userPreferences.getCertificates().add(certificate);

                assertEquals(1, userPreferences.getCertificates().size());

                assertTrue(keyAndCertStore.contains(certificate));
            }
            catch(CertStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddNamedBlobs()
    {
        String preferencesName = "Test";
        String category = "category";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

            Set<NamedBlob> blobs = userPreferences.getNamedBlobs();

            assertEquals(0, blobs.size());

            NamedBlob blob1 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);

            blobs.add(blob1);

            assertEquals(1, blobs.size());

            NamedBlob blob2 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);

            blobs.add(blob2);
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

            Set<NamedBlob> blobs = userPreferences.getNamedBlobs();

            assertEquals(2, blobs.size());

            NamedBlob blob1 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);
            NamedBlob blob2 = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);

            assertTrue(blobs.contains(blob1));
            assertTrue(blobs.contains(blob2));
        });
    }

    @Test
    public void testAddInheritedNamedBlobs()
    {
        String category = "category";

        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.addUserPreferences("main");

            NamedBlob blob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);

            userPreferences.getNamedBlobs().add(blob);

            UserPreferences inheritedPreferences = userPreferencesManager.addUserPreferences("inherited");

            NamedBlob inheritedBlob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);

            inheritedPreferences.getNamedBlobs().add(inheritedBlob);

            userPreferences.getInheritedUserPreferences().add(inheritedPreferences);
        });
        transactionOperations.executeWithoutResult(status ->
        {
            UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                    getUserPreferencesManager(category);

            UserPreferences userPreferences = userPreferencesManager.getUserPreferences("main");

            Set<NamedBlob> blobs = userPreferences.getNamedBlobs();

            assertEquals(1, blobs.size());

            NamedBlob blob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_1);

            assertEquals(blob, blobs.iterator().next());

            assertEquals(1, userPreferences.getInheritedUserPreferences().size());

            Set<NamedBlob> inheritedBlobs = userPreferences.getInheritedUserPreferences().iterator().next()
                    .getNamedBlobs();

            assertEquals(1, inheritedBlobs.size());

            NamedBlob inheritedBlob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_2);

            assertEquals(inheritedBlob, inheritedBlobs.iterator().next());
        });
    }

    @Test
    public void testAddkeyAndCertificate()
    {
        String preferencesName = "Test";
        String category = "category-1";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.addUserPreferences(preferencesName);

                String thumbprint = "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69" +
                            "FB45FF52A34049AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B";

                X509Certificate certificate = keyAndCertStore.getByThumbprint(thumbprint).getCertificate();

                X509CertStoreEntry certStoreEntry = keyAndCertStore.getByCertificate(certificate);

                KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(certStoreEntry);

                assertNotNull(keyAndCertificate);

                userPreferences.setKeyAndCertificate(keyAndCertificate);
            }
            catch(CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                String thumbprint = "610E02A5857CD9CB95A50FFA725173EE8B997E5D2B3443F4CA43CB1B72DF69" +
                        "FB45FF52A34049AD2ECA6D48BCD0F10DF1A882AC065A9E262046F43DBD2AE2C66B";

                X509Certificate certificate = keyAndCertStore.getByThumbprint(thumbprint).getCertificate();

                assertNotNull(userPreferences);
                assertNotNull(userPreferences.getKeyAndCertificate());
                assertEquals(certificate, userPreferences.getKeyAndCertificate().getCertificate());
                assertNotNull(userPreferences.getKeyAndCertificate().getPrivateKey());

                userPreferences.setKeyAndCertificate(null);
            }
            catch(CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferencesManager userPreferencesManager = userPreferencesCategoryManager.
                        getUserPreferencesManager(category);

                UserPreferences userPreferences = userPreferencesManager.getUserPreferences(preferencesName);

                assertNotNull(userPreferences);
                assertNull(userPreferences.getKeyAndCertificate());
            }
            catch(CertStoreException | KeyStoreException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
