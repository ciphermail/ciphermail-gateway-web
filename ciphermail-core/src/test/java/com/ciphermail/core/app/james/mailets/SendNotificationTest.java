/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.ciphermail.core.test.service.MockNotificationService;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SendNotificationTest
{
    @Autowired
    private AbstractApplicationContext applicationContext;

    @Autowired
    private MockNotificationService notificationService;

    @Before
    public void setup() {
        notificationService.getNotifications().clear();
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mail createMail()
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setFrom(new InternetAddress("from@example.com"));

        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SendNotification();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testDefaultValues()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "test 123")
                .build();

        Mail mail = createMail();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, notificationService.getNotifications().size());
        assertEquals("test 123", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(0).getFacility());
    }

    @Test
    public void testStaticValues()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "test 123")
                .setProperty("severity", "CRITICAL")
                .setProperty("facility", "qwerty")
                .build();

        Mail mail = createMail();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, notificationService.getNotifications().size());
        assertEquals("test 123", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("CRITICAL", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("qwerty", notificationService.getNotifications().get(0).getFacility());
    }

    @Test
    public void testAttributes()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "test 123")
                .setProperty("messageAttribute", "message")
                .setProperty("severityAttribute", "severity")
                .setProperty("facilityAttribute", "facility")
                .build();

        Mail mail = createMail();

        mail.setAttribute(Attribute.convertToAttribute("message", "message from attribute"));
        mail.setAttribute(Attribute.convertToAttribute("severity", "EMERGENCY"));
        mail.setAttribute(Attribute.convertToAttribute("facility", "facility from attribute"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, notificationService.getNotifications().size());
        assertEquals("message from attribute", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("EMERGENCY", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("facility from attribute", notificationService.getNotifications().get(0).getFacility());
    }

    @Test
    public void testCollectionOfMessages()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "static message")
                .setProperty("messageAttribute", "message")
                .setProperty("severityAttribute", "severity")
                .setProperty("facilityAttribute", "facility")
                .build();

        Mail mail = createMail();

        // we use MailAttributes mailet to add multiple messages
        Mailet mailAttributesMailet = new MailAttributes();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailAttributesMailet);

        mailAttributesMailet.init(FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("add",
                        "[" +
                        "{\"name\": \"message\", \"type\":\"STRING\",\"value\":\"message 1\"}," +
                        "{\"name\": \"message\", \"type\":\"STRING\",\"value\":\"message 2\"}," +
                        "{\"name\": \"message\", \"type\":\"STRING\",\"value\":\"message 3\"}" +
                        "]")
                .build());

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailAttributesMailet.service(mail);

        checkLogsForErrorsOrExceptions();

        List<String> messages = MailAttributesUtils.getAttributeValueAsStringList(mail, "message");

        assertEquals(3, messages.size());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(3, notificationService.getNotifications().size());

        assertEquals("message 1", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(0).getFacility());

        assertEquals("message 2", notificationService.getNotifications().get(1).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(1).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(1).getFacility());

        assertEquals("message 3", notificationService.getNotifications().get(2).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(2).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(2).getFacility());
    }

    @Test
    public void testInvalidAttributeValues()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "test")
                .setProperty("messageAttribute", "message")
                .setProperty("severityAttribute", "severity")
                .build();

        Mail mail = createMail();

        mail.setAttribute(Attribute.convertToAttribute("message", ""));
        mail.setAttribute(Attribute.convertToAttribute("severity", "INVALID_VALUE"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, notificationService.getNotifications().size());
        assertEquals("test", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(0).getFacility());
    }

    @Test
    public void testNullMessageAttributeValue()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder().mailetContext(FakeMailContext.defaultContext())
                .setProperty("message", "test")
                .setProperty("messageAttribute", "message")
                .build();

        Mail mail = createMail();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, notificationService.getNotifications().size());
        assertEquals("test", notificationService.getNotifications().get(0).getMessage().getMessage());
        assertEquals("NOTICE", notificationService.getNotifications().get(0).getSeverity().toString());
        assertEquals("info", notificationService.getNotifications().get(0).getFacility());
    }
}
