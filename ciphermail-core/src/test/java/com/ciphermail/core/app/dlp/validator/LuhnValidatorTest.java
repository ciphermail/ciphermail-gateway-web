/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp.validator;

import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.validator.LuhnValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class LuhnValidatorTest
{
    @Test
    void test()
    throws IOException
    {
        Validator validator = new LuhnValidator();

        Assertions.assertTrue(validator.isValid("79927398713"));
        Assertions.assertTrue(validator.isValid(" 7  9 9 2 7  \n  398713"));
        Assertions.assertTrue(validator.isValid("7-9-9-2-7-398713"));

        Assertions.assertFalse(validator.isValid("79927398712"));
        Assertions.assertFalse(validator.isValid("A79927398713"));
        Assertions.assertFalse(validator.isValid("7/9927398713"));
        Assertions.assertFalse(validator.isValid(""));
        Assertions.assertFalse(validator.isValid(null));

        // Visa CC
        Assertions.assertTrue(validator.isValid("4111 1111 1111 1111"));
        Assertions.assertFalse(validator.isValid("4111 1111 1111 2111"));

        // MasterCard CC
        Assertions.assertTrue(validator.isValid("5500 0000 0000 0004"));
        Assertions.assertFalse(validator.isValid("5500 0001 0000 0004"));

        // American Express
        Assertions.assertTrue(validator.isValid("3400 0000 0000 009"));
        Assertions.assertFalse(validator.isValid("3400 0000 2000 009"));

        // Diner's Club
        Assertions.assertTrue(validator.isValid("3000 0000 0000 04"));
        Assertions.assertFalse(validator.isValid("3000 2000 0000 04"));

        // Canadian SSN
        Assertions.assertTrue(validator.isValid("046 454 286"));
        Assertions.assertFalse(validator.isValid("047 454 286"));
    }
}
