/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.CipherMailApplication;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.impl.DefaultGlobalPreferencesManager;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MessageParser;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import com.lowagie.text.pdf.PdfReader;
import org.apache.commons.codec.net.QuotedPrintableCodec;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PageMode;
import org.apache.pdfbox.pdmodel.encryption.PDEncryption;
import org.apache.pdfbox.pdmodel.encryption.SecurityHandler;
import org.apache.pdfbox.pdmodel.interactive.viewerpreferences.PDViewerPreferences;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PDFEncryptTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private UserPreferencesCategoryManager userPreferencesCategoryManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        Configurator.setLevel(PDFEncrypt.class, Level.DEBUG);
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @Before
    public void setup()
    {
        deleteAllUsers();
        deleteGlobalProperties();
    }

    private void deleteAllUsers()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void deleteGlobalProperties()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userPreferencesCategoryManager.getUserPreferencesManager(UserPreferencesCategory.GLOBAL.name()).
                    deleteUserPreferences(new DefaultGlobalPreferencesManager(userPreferencesCategoryManager,
                            null).getGlobalUserPreferences());
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserProperties userProperties = user.getUserPreferences().getProperties();

                userProperties.setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setGlobalProperty(String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = globalPreferencesManager.getGlobalUserPreferences();

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void checkEncryption(MimeMessage message, String password, boolean hasReplyLink)
    throws Exception
    {
        checkEncryption(message, password, hasReplyLink, null);
    }

    private void checkEncryption(final MimeMessage message, final String password, boolean hasReplyLink,
            String expectedSubject)
    throws Exception
    {
        if (expectedSubject == null) {
            expectedSubject = message.getSubject();
        }

        // The message should be a mime multipart mixed with two parts. The first part should be readable text
        // and the second part should be the encrypted PDF
        assertTrue(message.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) message.getContent();

        assertEquals(2, mp.getCount());

        BodyPart textPart = mp.getBodyPart(0);

        assertTrue(textPart.isMimeType("text/plain"));

        BodyPart pdfPart = mp.getBodyPart(1);

        assertTrue(pdfPart.isMimeType("application/pdf"));

        // Decrypt with IText
        PdfReader reader = new PdfReader(pdfPart.getInputStream(), password.getBytes(StandardCharsets.UTF_8));

        String firstPageContent = new String(reader.getPageContent(1), StandardCharsets.UTF_8);

        // We just check whether the raw content contains (Reply) or /img1 (which is the image when the reply button
        // is used).
        if (hasReplyLink) {
            assertTrue(firstPageContent.contains("(Reply)") || firstPageContent.contains("/img"));
        }
        else {
            assertFalse(firstPageContent.contains("(Reply)"));
            assertFalse(firstPageContent.contains("/img"));
        }

        // Decrypt with PDFBox
        try (PDDocument document = PDDocument.load(pdfPart.getInputStream(), password)) {
            PDEncryption encryption = document.getEncryption();
            SecurityHandler securityHandler = encryption.getSecurityHandler();

            // Check if AES is enabled
            assertTrue(securityHandler.isAES());

            //Check meta info
            PDDocumentCatalog documentCatalog = document.getDocumentCatalog();

            assertEquals(PageMode.USE_ATTACHMENTS, documentCatalog.getPageMode());

            PDViewerPreferences viewerPreferences = documentCatalog.getViewerPreferences();

            assertTrue(viewerPreferences.displayDocTitle());

            PDDocumentInformation documentInformation = document.getDocumentInformation();

            assertEquals(expectedSubject, documentInformation.getSubject());
            assertEquals(expectedSubject, documentInformation.getTitle());

            assertEquals(StringUtils.join(EmailAddressUtils.addressesToStrings(
                            EmailAddressUtils.getFromNonStrict(message), true /* mime decode */), ", "),
                    documentInformation.getAuthor());

            assertEquals(CipherMailApplication.getName(), documentInformation.getProducer());
            assertEquals(CipherMailApplication.getName(), documentInformation.getCreator());
            assertNull(documentInformation.getModificationDate());
        }
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PDFEncrypt();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testReplyInvalidFrom()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("log", "Log at start")
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("&*^&*^&*", false));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("m.bRINKERs@pobox.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("m.brinkers@pobox.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("Log at start"));
        assertThat(memoryLogAppender.getLogOutput(), containsString("Reply-To of the message is an " +
                "invalid email address. It's not possible to reply to the PDF."));

        assertEquals(Mail.DEFAULT, mail.getState());

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("encryptedProcessor", sentMail.getState());
        assertNull(sentMail.getSender());
        assertEquals("m.bRINKERs@pobox.com", StringUtils.join(sentMail.getRecipients(), ","));
        checkEncryption(sentMail.getMsg(), "test", false);
    }

    @Test
    public void testEncrypt()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .setProperty("passThroughProcessor", "passthrough")
                .setProperty("retainMessageID", "true")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("test@example.com", false));
        message.setHeader("Message-Id", "fixed-message-id");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("M.BRINKERS@pobox.com", "123@example.com")
                .sender("sender@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();

        mail.setAttribute(Attribute.convertToAttribute("testAttribute", "value"));

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setUserProperty("test@EXAMPLE.com", "pdfTemplate", template);

        setUserProperty("test@example.com", "server-secret", "123");
        setUserProperty("test@example.com", "pdf-reply-enabled", "true");
        setUserProperty("test@example.com", "pdf-reply-url", "http://127.0.0.1");

        setUserProperty("m.brinkers@pobox.com", "pdf-reply-enabled", "true");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test1");
        container.setPasswordID("test ID");

        passwords.put("m.brinkers@pobox.com", container);

        container = new PasswordContainer();
        container.setPassword("test2");
        container.setPasswordID("test ID 2");

        passwords.put("123@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Recipient 123@example.com does not allow reply to PDF"));

        assertEquals("passthrough", mail.getState());

        assertEquals(2, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("encryptedProcessor", sentMail.getState());
        assertEquals("sender@example.com", sentMail.getSender().toString());
        assertEquals("M.BRINKERS@pobox.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertEquals("test", StringUtils.join(sentMail.getMsg().getHeader("X-pdf-template-test")));
        // check if message-id is retained
        assertEquals("fixed-message-id", sentMail.getMsg().getMessageID());
        checkEncryption(sentMail.getMsg(), "test1", true);
        // check if Mail attributes are cloned
        assertEquals("value", sentMail.getAttributes().get(AttributeName.of("testAttribute"))
                .getValue().valueAs(String.class).get());

        sentMail = mailContext.getSentMails().get(1);

        assertEquals("encryptedProcessor", sentMail.getState());
        assertEquals("sender@example.com", sentMail.getSender().toString());
        assertEquals("123@example.com", StringUtils.join(sentMail.getRecipients(), ","));
        assertEquals("test", StringUtils.join(sentMail.getMsg().getHeader("X-pdf-template-test")));
        // check if message-id is retained
        assertEquals("fixed-message-id", sentMail.getMsg().getMessageID());
        checkEncryption(sentMail.getMsg(), "test2", false);
        // check if Mail attributes are cloned
        assertEquals("value", sentMail.getAttributes().get(AttributeName.of("testAttribute"))
                .getValue().valueAs(String.class).get());
    }

    /**
     * Test the encryption of a message with a large body text (640K)
     */
    @Test
    public void testPerformanceLargeAttachment()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .setProperty("passThrough", "false")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/multiple-inline-attachments.eml");
        message.setFrom(new InternetAddress("from@example.com", false));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("test@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        int repeat = 100;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            // reset log output since we are only interested in the logs produced while handling not during init
            memoryLogAppender.reset();

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertEquals(Mail.GHOST, mail.getState());

            assertEquals(1, mailContext.getSentMails().size());

            checkEncryption(mailContext.getSentMails().get(0).getMsg(), "test", true);

            // reset to save memory
            mailContext.resetSentMails();
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("PDF ecryptions/sec: " + perSecond);

        /*
         * NOTE: !!! can fail on a slower system
         */
        assertTrue("PDF encryption too slow. !!! this can fail on a slower system !!!", perSecond > 4);
    }

    /**
     * Test the encryption of a message with a large body text (640K). A large body text is relative slow compared
     * to binary attachments
     */
    @Test
    public void testPerformanceLargeBodyText()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/test-640K.eml");
        message.setFrom(new InternetAddress("from@example.com", false));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("test@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        int repeat = 100;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            // reset log output since we are only interested in the logs produced while handling not during init
            memoryLogAppender.reset();

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertEquals(1, mailContext.getSentMails().size());

            checkEncryption(mailContext.getSentMails().get(0).getMsg(), "test", true);

            // reset to save memory
            mailContext.resetSentMails();
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("PDF ecryptions/sec: " + perSecond);

        // NOTE: !!! can fail on a slower system
        assertTrue("PDF encryption too slow. !!! this can fail on a slower system !!!", perSecond > 4);
    }

    @Test
    public void testEmailDisplayNameUTF8()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("test@example.com", "=?UTF-8?B?w6TDtsO8IMOEw5bDnA==?="));
        message.setHeader("Message-Id", "fixed-message-id");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("recipient@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        MimeMessage encrypted = sentMail.getMsg();

        // by default, the message-id is not retained
        assertNotEquals("fixed-message-id", encrypted.getMessageID());

        checkEncryption(encrypted, "test", true);

        assertTrue(encrypted.isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) encrypted.getContent();

        assertEquals(2, mp.getCount());

        BodyPart textPart = mp.getBodyPart(0);
        BodyPart pdfPart = mp.getBodyPart(1);

        assertTrue(textPart.isMimeType("text/plain"));
        assertTrue(pdfPart.isMimeType("application/pdf"));

        // check if the body contains äöü ÄÖÜ (which is the decoded from personal name)
        String text = (String) textPart.getContent();

        // check if the body contains äöü ÄÖÜ (which is the decoded from personal name)
        assertTrue(text.contains("äöü ÄÖÜ"));
    }

    @Test
    public void testInvalidMIMEEncoding()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .setProperty("passThrough", "false")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/unknown-content-transfer-encoding.eml");

        message.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state(Mail.DEFAULT)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Unknown encoding: xxx. " +
                "Retrying (retry count: 1)"));

        assertEquals(0, mailContext.getSentMails().size());

        // even though passThrough is false, because of an error, the email is passed
        assertEquals(Mail.DEFAULT, mail.getState());
    }

    @Test
    public void testMissingPassword()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient1@example.com", "recipient2@example.com", "recipient3@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient1@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        // every recipient gets their own email
        assertEquals(3, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("encryptedProcessor", sentMail.getState());
        assertEquals("recipient1@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        checkEncryption(sentMail.getMsg(), "test", true);

        sentMail = mailContext.getSentMails().get(1);
        assertEquals("notEncryptedProcessor", sentMail.getState());
        assertEquals("recipient2@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));

        sentMail = mailContext.getSentMails().get(2);
        assertEquals("notEncryptedProcessor", sentMail.getState());
        assertEquals("recipient3@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));
    }

    @Test
    public void testMissingPasswordAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient1@example.com", "recipient2@example.com", "recipient3@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("PDF passwords not found. " +
                "Sending message unencrypted"));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("notEncryptedProcessor", sentMail.getState());
        assertEquals("recipient1@example.com,recipient2@example.com,recipient3@example.com",
                StringUtils.join(sentMail.getRecipients(), ","));
    }

    /**
     * Test with a message which has an invalid content-type
     */
    @Test
    public void testTemplateInvalidContentType()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/pdf-attachment-faulty-content-type.eml");
        message.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("Unable to infer MimeType from content " +
                "type. Fallback to application/octet-stream. Content-Type: application/pdf;\r\n" +
                " charset=\"Windows-1252\" name=\"postcard2010.pdf\""));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("encryptedProcessor", sentMail.getState());
        assertEquals("recipient@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        checkEncryption(sentMail.getMsg(), "test", true);
    }

    /**
     * Test whether every email gets its own password ID
     */
    @Test
    public void testUniquePasswordID()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/normal-message-with-attach.eml");
        message.setFrom(new InternetAddress("test@example.com"));

        int recipientCount = 10;

        Passwords passwords = new Passwords();

        List<MailAddress> recipients = new LinkedList<>();

        for (int i = 0; i < recipientCount; i++)
        {
            String recipient = "test" + i + "@example.com";

            PasswordContainer container = new PasswordContainer();
            container.setPassword("test" + i);
            container.setPasswordID("ID" + i);

            passwords.put(recipient, container);

            recipients.add(new MailAddress(recipient));
        }

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/encrypted-pdf-otp-bug38.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(recipientCount, mailContext.getSentMails().size());

        for (int i = 0; i < recipientCount; i++)
        {
            FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(i);

            Collection<MailAddress> sentMailRecipients = sentMail.getRecipients();

            // every recipient should get a unique email
            assertEquals(1, sentMailRecipients.size());

            String pdfRecipient = sentMailRecipients.iterator().next().toString();

            assertEquals("test" + i + "@example.com", pdfRecipient);

            MimeMessage pdfMessage = sentMail.getMsg();

            MessageParser messageParser = new MessageParser(false);

            messageParser.parseMessage(pdfMessage);

            assertEquals(1, messageParser.getAttachments().size());
            assertEquals(1, messageParser.getInlineTextParts().size());

            String body = (String) messageParser.getInlineTextParts().get(0).getContent();

            assertTrue(body.contains("Recipient: test" + i + "%40example.com"));
            assertTrue(body.contains("Recipient: test" + i + "%40example.com"));

            // Foo attr should be part of the Mail attr added by the template. This way we will check whether
            // the template is run on it's own instance of Mail
            assertEquals("ID" + i, sentMail.getAttributes().get(AttributeName.of("foo"))
                    .getValue().valueAs(String.class).get());
        }
    }

    @Test
    public void testUnsafeChars()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/unsafe-chars-headers.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        MimeMessage encrypted = sentMail.getMsg();

        assertEquals("encryptedProcessor", sentMail.getState());
        assertEquals("recipient@example.com", StringUtils.join(sentMail.getRecipients(), ","));

        MimeMultipart mp = (MimeMultipart) encrypted.getContent();

        assertEquals(2, mp.getCount());

        BodyPart textPart = mp.getBodyPart(0);

        assertEquals("TEST\n" +
                "\n" +
                "\n" +
                "                               test ID\n" +
                "\n" +
                "\n" +
                     new QuotedPrintableCodec().decode(
                             "=00=01=02=03=04=05=06=07=08=09=0A=0B=0C=0D=0E=3D") +
                "\n", textPart.getContent());

        checkEncryption(encrypted, "test", true, "\t\n\r " +
                "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~");
    }

    @Test
    public void testNullFromAddresses()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("template", "encrypted-pdf.ftl")
                .setProperty("templateProperty", "pdfTemplate")
                .setProperty("encryptedProcessor", "encryptedProcessor")
                .setProperty("notEncryptedProcessor", "notEncryptedProcessor")
                .build();

        MimeMessage message = TestUtils.loadTestMessage("mail/test-empty-addresses.eml");

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .build();

        String template = FileUtils.readFileToString(
                new File("src/test/resources/templates/pdf-attachment.ftl"),
                StandardCharsets.UTF_8);

        setGlobalProperty("pdfTemplate", template);
        setGlobalProperty("server-secret", "123");
        setGlobalProperty("pdf-reply-enabled", "true");
        setGlobalProperty("portal-base-url", "http://127.0.0.1");

        Mailet mailet = createMailet(mailetConfig);

        Passwords passwords = new Passwords();

        PasswordContainer container = new PasswordContainer();
        container.setPassword("test");
        container.setPasswordID("test ID");

        passwords.put("recipient@example.com", container);

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString("Reply-To of the message is an " +
                "invalid email address. It's not possible to reply to the PDF."));

        assertEquals(1, mailContext.getSentMails().size());

        FakeMailContext.SentMail sentMail = mailContext.getSentMails().get(0);

        assertEquals("encryptedProcessor", sentMail.getState());
        checkEncryption(sentMail.getMsg(), "test", false);
    }
}
