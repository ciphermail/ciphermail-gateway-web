/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RoleDAOTest
{
    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup() {
        createDAO().deleteAllRoles();
    }

    private RoleDAO createDAO() {
        return new RoleDAO(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Test
    public void testGetAuthority()
    {
        RoleDAO dao = createDAO();

        assertNull(dao.getRole("role"));

        createDAO().persist(new Role("role"));

        assertNotNull(dao.getRole("role"));
    }

    @Test
    public void testDeleteAuthority()
    {
        RoleDAO dao = createDAO();

        createDAO().persist(new Role("role"));

        Role entity = dao.getRole("role");

        assertNotNull(entity);

        dao.delete(entity);

        entity = dao.getRole("role");

        assertNull(entity);
    }

    @Test
    public void testDeleteAll()
    {
        RoleDAO dao = createDAO();

        createDAO().persist(new Role("role1"));
        createDAO().persist(new Role("role2"));
        createDAO().persist(new Role("role3"));

        assertNotNull(dao.getRole("role1"));
        assertNotNull(dao.getRole("role2"));
        assertNotNull(dao.getRole("role3"));

        dao.deleteAllRoles();

        assertNull(dao.getRole("role1"));
        assertNull(dao.getRole("role2"));
        assertNull(dao.getRole("role3"));
    }

    @Test
    public void testGetAuthorites()
    {
        RoleDAO dao = createDAO();

        List<Role> authorities= dao.getRoles(null, null, null);

        assertEquals(0, authorities.size());

        createDAO().persist(new Role("role2"));
        createDAO().persist(new Role("role1"));
        createDAO().persist(new Role("role3"));

        authorities = dao.getRoles(null, null, null);

        assertEquals(3, authorities.size());

        authorities = dao.getRoles(null, null, SortDirection.ASC);
        assertEquals(3, authorities.size());
        assertEquals("role1", authorities.get(0).getName());
        assertEquals("role2", authorities.get(1).getName());
        assertEquals("role3", authorities.get(2).getName());

        authorities = dao.getRoles(null, null, SortDirection.DESC);
        assertEquals(3, authorities.size());
        assertEquals("role1", authorities.get(2).getName());
        assertEquals("role2", authorities.get(1).getName());
        assertEquals("role3", authorities.get(0).getName());

        authorities = dao.getRoles(1, null, SortDirection.ASC);
        assertEquals(2, authorities.size());
        assertEquals("role2", authorities.get(0).getName());
        assertEquals("role3", authorities.get(1).getName());

        authorities = dao.getRoles(1, 1, SortDirection.ASC);
        assertEquals(1, authorities.size());
        assertEquals("role2", authorities.get(0).getName());

        authorities = dao.getRoles(1, 2, SortDirection.DESC);
        assertEquals(2, authorities.size());
        assertEquals("role2", authorities.get(0).getName());
        assertEquals("role1", authorities.get(1).getName());
    }

    @Test
    public void testGetAuthorityCount()
    {
        RoleDAO dao = createDAO();

        assertEquals(0, dao.getRoleCount());

        createDAO().persist(new Role("role1"));
        assertEquals(1, dao.getRoleCount());

        createDAO().persist(new Role("role2"));
        assertEquals(2, dao.getRoleCount());
    }
}
