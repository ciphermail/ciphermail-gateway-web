/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.StringUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.OutputStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class FilterSubjectTest
{
    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new FilterSubject();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testStaticFilter()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/e$/ %^&*")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test simple messag%^&*", mail.getMessage().getSubject());
    }

    @Test
    public void testAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attribute", "attribute")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mail.setAttribute(Attribute.convertToAttribute("attribute", "/(simple)/ *$1$1*"));

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Subject filtered. Old subject: test " +
                "simple message, new subject: test *simplesimple* message"));

        assertEquals("test *simplesimple* message", mail.getMessage().getSubject());
    }

    @Test
    public void testEncodedSubject()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/(^(.*)$)/ $1 !@#\\$%^&*()")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/encoded-subject.eml"))
                .build();

        assertEquals("test encoded subject äöü ÄÖÜ", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test encoded subject äöü ÄÖÜ !@#$%^&*()", mail.getMessage().getSubject());

        // Check if subject is encoded
        assertEquals("=?UTF-8?Q?test_encoded_subjec?=\r\n =?UTF-8?Q?t_=C3=A4=C3=B6=C3=BC_=C3=84=C3=96=C3=9C_!@#$%^&*()?=",
                StringUtils.join(mail.getMessage().getHeader("subject"), ','));
    }

    @Test
    public void testMissingSubject()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/(^(.*)$)/ test")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/test-5K.eml"))
                .build();

        assertNull(mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertNull(mail.getMessage().getSubject());
    }

    @Test
    public void testNullFilter()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test simple message", mail.getMessage().getSubject());
    }

    @Test
    public void testInvalidFilter()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attribute", "attribute")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mail.setAttribute(Attribute.convertToAttribute("attribute", "invalid"));

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Filter invalid is not a valid filter"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test simple message", mail.getMessage().getSubject());
    }

    @Test
    public void testEmptyReplace()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/simple/")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test  message", mail.getMessage().getSubject());
    }

    @Test
    public void testInvalidRegExp()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("attribute", "attribute")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mail.setAttribute(Attribute.convertToAttribute("attribute", "/[x/ test"));

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("[x is not a valid regular expression"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test simple message", mail.getMessage().getSubject());
    }

    @Test
    public void testComplexRegEx()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/\\[(decrypted|signed|signed by:.*|invalid signature!)\\]/")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/security-subject-tags.eml"))
                .build();

        assertEquals("test [decrypted] [signed]", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Old subject: test [decrypted] " +
                "[signed], new subject: test"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("test", mail.getMessage().getSubject());
    }

    @Test
    public void testInvalidMessageNoErrorProcessor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/simple/")
                .build();

        MimeMessage message = new MimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
        {
            @Override
            public void writeTo(OutputStream os) throws IOException {
                throw new IOException("failed on purpose");
            }
        };

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Message with message-id null and " +
                "MailID null is not a valid MIME message. The subject can therefore not be filtered."));

        assertEquals("test simple message", mail.getMessage().getSubject());

        assertEquals("initial", mail.getState());
    }

    @Test
    public void testInvalidMessageErrorProcessor()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filter", "/simple/")
                .setProperty("errorProcessor", "error")
                .build();

        MimeMessage message = new MimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
        {
            @Override
            public void writeTo(OutputStream os) throws IOException {
                throw new IOException("failed on purpose");
            }
        };

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(message)
                .state("initial")
                .build();

        assertEquals("test simple message", mail.getMessage().getSubject());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("test simple message", mail.getMessage().getSubject());

        assertEquals("error", mail.getState());
    }
}
