/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UpdateableWordSkipperImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private NamedBlobManager namedBlobManager;

    private UpdateableWordSkipper updateableWordSkipper;

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status -> namedBlobManager.deleteAll());

        updateableWordSkipper = new UpdateableWordSkipperImpl(
                namedBlobManager,
                new ClassPathResource("conf/dlp-skip-words.txt"),
                transactionOperations);
    }

    @Test
    public void testSkip()
    {
        assertTrue(updateableWordSkipper.isSkip("the"));
        assertTrue(updateableWordSkipper.isSkip("us"));
        assertFalse(updateableWordSkipper.isSkip("nonexistingword"));
        // Check case insensitivity
        assertTrue(updateableWordSkipper.isSkip("US"));
        // Input is not trimmed
        assertFalse(updateableWordSkipper.isSkip(" us "));
    }

    @Test
    public void testGetSkipList()
    {
        String wordList = updateableWordSkipper.getSkipList();

        assertTrue(wordList.startsWith("the is be to "));
        assertTrue(wordList.endsWith("give day most us"));
    }

    @Test
    public void testSetSkipList()
    {
        transactionOperations.executeWithoutResult(tx ->
                updateableWordSkipper.setSkipList("aap NOOT mies"));

        assertTrue(updateableWordSkipper.isSkip("aAp"));
        assertFalse(updateableWordSkipper.isSkip("schapen"));
        assertTrue(updateableWordSkipper.isSkip("mies"));

        String wordList = updateableWordSkipper.getSkipList();
        assertEquals("aap noot mies", wordList);
    }
}
