/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.ctl.CTL;
import com.ciphermail.core.common.security.ctl.CTLDAO;
import com.ciphermail.core.common.security.ctl.CTLEntry;
import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.core.common.security.ctl.CTLException;
import com.ciphermail.core.common.security.ctl.CTLManager;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class RecipientHasCertificatesTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private SessionManager sessionManager;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private CTLManager ctlManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                domainManager.deleteAllDomains();

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                // Clean all CTLs
                CTLDAO.deleteAllEntries(SessionAdapterFactory.create(sessionManager.getSession()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        importKeyStore(keyAndCertificateWorkflow, new File(TestUtils.getTestDataDir(),
                        "keys/testCertificates.p12"), "test");

        importCertificates(rootStore, new File(TestUtils.getTestDataDir(), "certificates/mitm-test-root.cer"));

        addUserCertificates("m.brinkers@pobox.com", CertificateUtils.readX509Certificates(new File(
                TestUtils.getTestDataDir(),
                "certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer")));
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyStore(KeyAndCertificateWorkflow keyAndCertificateWorkflow, File pfxFile, String password)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");
                keyStore.load(new FileInputStream(pfxFile), password.toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void importCertificates(X509CertStoreExt certStore, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(certificateFile);

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        certStore.addCertificate((X509Certificate) certificate);
                    }
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUserCertificates(String email, Collection<X509Certificate> certificates)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                UserPreferences userPreferences = user.getUserPreferences();

                for (X509Certificate certificate : certificates) {
                    userPreferences.getCertificates().add(certificate);
                }

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private long getUserCount() {
        return transactionOperations.execute(status -> userWorkflow.getUserCount()).longValue();
    }

    private void setDomainProperty(String domain, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences preferences = domainManager.getDomainPreferences(domain);

                if (preferences == null) {
                    preferences = domainManager.addDomain(domain);
                }

                UserProperties properties = preferences.getProperties();

                properties.setProperty(propertyName, value);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addToCTL(String thumbprint, CTLEntryStatus ctlStatus, boolean allowExpired)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CTL ctl = ctlManager.getCTL(CTLManager.DEFAULT_CTL);

                CTLEntry ctlEntry = ctl.createEntry(thumbprint);

                ctlEntry.setStatus(ctlStatus);
                ctlEntry.setAllowExpired(allowExpired);

                ctl.addEntry(ctlEntry);
            }
            catch (CTLException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new RecipientHasCertificates();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testSingleRecipient()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(13, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }

    @Test
    public void testMultipleRecipientsOneMatch()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "nomatch@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(13, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }

    @Test
    public void testMultipleRecipientsBlackListed()
    throws Exception
    {
        addToCTL("E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673"
                + "E99E3B12294BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4",
                CTLEntryStatus.BLACKLISTED, false /* allow expired */);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "m.brinkers@pobox.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(12, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }

    @Test
    public void testMultipleRecipientsMultipleMatchesWhiteListed()
    throws Exception
    {
        addToCTL(X509CertificateInspector.getThumbprint(TestUtils.loadCertificate(new File(
                TestUtils.getTestDataDir(),
                        "certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer"))),
                CTLEntryStatus.WHITELISTED, true /* allow expired */);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "m.brinkers@pobox.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));
        assertTrue(result.contains(new MailAddress("m.brinkers@pobox.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(14, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }

    @Test
    public void testMultipleRecipientsMultipleMatchesWhiteListedExpiredNotAllowed()
    throws Exception
    {
        addToCTL(X509CertificateInspector.getThumbprint(TestUtils.loadCertificate(new File(
                TestUtils.getTestDataDir(),
                "certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer"))),
                CTLEntryStatus.WHITELISTED, false /* not allow expired */);

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("test@example.com", "m.brinkers@pobox.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(13, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }

    @Test
    public void testSingleRecipientNoMatch()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        Mail mail = createMail("nomatch@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
        assertEquals(1, getUserCount());

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNull(certificates);
    }

    @Test
    public void testRetryOnError()
    throws Exception
    {
        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false")
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        RecipientHasCertificates matcher = new RecipientHasCertificates()
        {
            protected boolean hasMatch(User user)
            throws MessagingException
            {
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }

                return super.hasMatch(user);
            }
        };

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        Mail mail = createMail("test@example.com");

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertTrue(constraintViolationThrown.booleanValue());

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("test@example.com")));

        Certificates certificates = CoreApplicationMailAttributes.getCertificates(mail);

        assertNotNull(certificates);

        assertEquals(13, certificates.getCertificates().size());

        assertEquals(1, getUserCount());
    }
}
