/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserWorkflowImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                domainManager.deleteAllDomains();

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddUser()
    {
        String email = "test@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                assertFalse(userWorkflow.isPersistent(user));

                assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());

                assertNotNull(user.getUserPreferences().getProperties().getDateCreated());

                userWorkflow.makePersistent(user);

                assertTrue(userWorkflow.isPersistent(user));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        assertEquals(Boolean.TRUE, transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                assertNotNull(user);

                assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());

                UserPreferences pref = user.getUserPreferences().getInheritedUserPreferences().iterator().next();

                assertEquals("GLOBAL", pref.getCategory());
                assertEquals("preferences", pref.getName());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddUserNotPersistent()
    {
        String email = "test@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                assertEquals(1, user.getUserPreferences().getInheritedUserPreferences().size());

                UserPreferences pref = user.getUserPreferences().getInheritedUserPreferences().iterator().next();

                assertEquals("GLOBAL", pref.getCategory());
                assertEquals("preferences", pref.getName());

                assertFalse(userWorkflow.isPersistent(user));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        assertNotEquals(Boolean.TRUE, transactionOperations.execute(status ->
        {
            try {
                return userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST) != null;
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        }));
    }

    @Test
    public void testAddUserAndDomainPrefs()
    {
        String email = "test@example.com";
        String propertyName = "test";

        /*
         * Add example.com domain
         */
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences domainPreferences = domainManager.addDomain("example.com");

                domainPreferences.getProperties().setProperty(propertyName, "domain-value");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Add test@example.com user
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.addUser(email);

                assertFalse(userWorkflow.isPersistent(user));

                assertEquals(2, user.getUserPreferences().getInheritedUserPreferences().size());

                assertNotNull(user.getUserPreferences().getProperties().getDateCreated());

                userWorkflow.makePersistent(user);

                assertTrue(userWorkflow.isPersistent(user));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Check if test@example.com inherits from the global settings and from the domain
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                assertNotNull(user);

                assertEquals(2, user.getUserPreferences().getInheritedUserPreferences().size());

                Iterator<UserPreferences> it = user.getUserPreferences().getInheritedUserPreferences().iterator();

                UserPreferences pref = it.next();

                assertEquals("GLOBAL", pref.getCategory());
                assertEquals("preferences", pref.getName());

                pref = it.next();

                assertEquals("DOMAIN", pref.getCategory());
                assertEquals("example.com", pref.getName());

                assertEquals("domain-value", user.getUserPreferences().getProperties().getProperty(propertyName));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Add wild-card domain
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                UserPreferences domainPreferences = domainManager.addDomain("*.example.com");

                domainPreferences.getProperties().setProperty(propertyName, "wild-card-value");
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Check that the user inherits from the domain and not from the wild-card domain
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                assertNotNull(user);

                assertEquals(3, user.getUserPreferences().getInheritedUserPreferences().size());

                Iterator<UserPreferences> it = user.getUserPreferences().getInheritedUserPreferences().iterator();

                UserPreferences pref = it.next();

                assertEquals("GLOBAL", pref.getCategory());
                assertEquals("preferences", pref.getName());

                pref = it.next();

                assertEquals("DOMAIN", pref.getCategory());
                assertEquals("*.example.com", pref.getName());

                pref = it.next();

                assertEquals("DOMAIN", pref.getCategory());
                assertEquals("example.com", pref.getName());

                assertEquals("domain-value", user.getUserPreferences().getProperties().getProperty(propertyName));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
