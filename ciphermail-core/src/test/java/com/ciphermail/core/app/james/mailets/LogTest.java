/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.DefaultMessageOriginatorIdentifier;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.Instant;
import java.util.Date;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class LogTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mail createMail(String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test subject");
        message.setFrom(new InternetAddress("from@example.com"));
        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(new MimeMessageWithID(message, "some message-id"))
                .state(Mail.DEFAULT)
                .build();
    }

    private Log createMailet()
    {
        Log mailet = new Log();

        mailet.setMessageOriginatorIdentifier(new DefaultMessageOriginatorIdentifier());

        return mailet;
    }

    @Test
    public void testDefaultSettingsNullMailID()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        Log mailet = createMailet();

        mailet.init(mailetConfig);

        Mail mail = createMail("test@example.com");

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "MailID: ; Recipients: [test@example.com]"));
    }

    @Test
    public void testFull()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("comment", "some comment")
                .setProperty("logDetail", "FULL")
                .build();

        Log mailet = createMailet();

        mailet.init(mailetConfig);

        Mail mail = createMail("test2@example.com");

        mail.setErrorMessage("some error message");
        mail.setLastUpdated(Date.from(Instant.ofEpochMilli(1692603287270L)));

        CoreApplicationMailAttributes.setMailID(mail, "some mailid");

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "some comment; MailID: some mailid; Recipients: [test2@example.com]; " +
                "Originator: from@example.com; Sender: sender@example.com; " +
                "Remote address: 127.0.0.1; Subject: test subject; " +
                "Message-ID: some message-id; Last updated: Mon Aug 21 09:34:47 CEST 2023; " +
                "Attribute names: AttributeName{name=ciphermail-mail-id}"));
    }
}