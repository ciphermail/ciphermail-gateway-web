/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import org.junit.Test;
import org.springframework.context.annotation.Profile;
import org.springframework.mock.env.MockEnvironment;

import java.lang.annotation.Annotation;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class UserPropertyRegistryImplTest
{
    private static final UserPropertiesType mockUserPropertiesType = new UserPropertiesType()
    {
        @Override
        public String name() {
            return "mock";
        }

        @Override
        public String parent() {
            return "";
        }

        @Override
        public Type type() {
            return Type.PROPERTIES;
        }

        @Override
        public boolean visible() {
            return true;
        }

        @Override
        public String displayName() {
            return "";
        }

        @Override
        public String displayNameKey() {
            return "";
        }

        @Override
        public String descriptionKey() {
            return "";
        }

        @Override
        public int order() {
            return 10;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return UserPropertiesType.class;
        }
    };

    @Profile("test")
    @UserPropertiesType(
            name = "test",
            displayNameKey = UserPropertiesType.BASE_KEY + "test" + UserPropertiesType.DISPLAY_NAME_KEY,
            descriptionKey = UserPropertiesType.BASE_KEY + "test" + UserPropertiesType.DESCRIPTION_KEY
    )
    public static class TestProperties
    {
        @UserProperty(
                name = "test"
        )
        public void setValue(String value) {
            // empty on purpose
        }

        @UserProperty(name = "test")
        public String getValue() {
            return "";
        }

        @UserProperty(
                name = "test2",
                user = false, domain = false, global = false
        )
        public boolean isReadOnly() {
            return false;
        }
    }

    @Profile("someOther")
    @UserPropertiesType(name = "someOther")
    public static class SomeOtherProperties
    {
        @UserProperty(name = "someOtherTest")
        public void setValue(String value) {
            // empty on purpose
        }

        @UserProperty(name = "someOtherTest")
        public String getValue() {
            return "";
        }
    }

    public static class AnnotationOnSetterOnly
    {
        @UserProperty(name = "test")
        public void setValue(String value) {
            // empty on purpose
        }
    }

    public static class NoProperties
    {
        @UserProperty(name = "test")
        public void test()
        {
            // empty on purpose
        }
    }

    public static class NoBeanMethod
    {
        @UserProperty(name = "test1")
        public boolean isReadOnly() {
            return false;
        }

        @UserProperty(name = "test2")
        public void test() {
            // empty on purpose
        }
    }

    public static class DuplicateMethod
    {
        @UserProperty(name = "test")
        public boolean isSet1() {
            return false;
        }

        @UserProperty(name = "test")
        public boolean isReadOnly2() {
            return false;
        }
    }

    public static class DuplicateInvalidMethod
    {
        @UserProperty(name = "test")
        public boolean isSet1() {
            return false;
        }

        @UserProperty(name = "test")
        public void test() {}
    }

    public static class PrivateMethod
    {
        @UserProperty(name = "test1")
        public boolean isReadOnly() {
            return false;
        }

        @UserProperty(name = "test2")
        private boolean isSet1() {
            return false;
        }
    }

    public static class NoSetterAnnotation
    {
        @UserProperty(name = "test")
        public String getValue() {
            return "";
        }

        public void setValue(String v) {
            // empty on purpose
        }
    }

    public static class PropertyNameMismatch
    {
        @UserProperty(name = "test")
        public String getValue() {
            return "";
        }

        @UserProperty(name = "other")
        public void setValue(String v) {
            // empty on purpose
        }
    }

    public static class DisplayKeyMismatch
    {
        @UserProperty(name = "test",
                displayNameKey = UserProperty.BASE_KEY + "test" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "test" + UserProperty.DESCRIPTION_KEY
        )
        public String getValue() {
            return "";
        }

        @UserProperty(
                name = "test",
                displayNameKey = UserProperty.BASE_KEY + "other" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "test" + UserProperty.DESCRIPTION_KEY
        )
        public void setValue(String v) {
            // empty on purpose
        }
    }

    public static class DescriptionKeyMismatch
    {
        @UserProperty(
                name = "test",
                displayNameKey = UserProperty.BASE_KEY + "test" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "test" + UserProperty.DESCRIPTION_KEY
        )
        public String getValue() {
            return "";
        }

        @UserProperty(
                name = "test",
                displayNameKey = UserProperty.BASE_KEY + "test" + UserProperty.DISPLAY_NAME_KEY,
                descriptionKey = UserProperty.BASE_KEY + "other" + UserProperty.DESCRIPTION_KEY
        )
        public void setValue(String v) {
            // empty on purpose
        }
    }

    @Test
    public void testScanPackages()
    {
        MockEnvironment environment = new MockEnvironment();

        environment.setActiveProfiles("test");

        UserPropertyRegistry userPropertyRegistry = new UserPropertyRegistryImpl(environment,
                "com.ciphermail.core.app.properties");

        // someOther profile should not match
        assertNull(userPropertyRegistry.getGroupedProperties().get(SomeOtherProperties.class.getAnnotation(
                UserPropertiesType.class)));

        UserPropertyRegistry.UserPropertyDescriptor userProperty = userPropertyRegistry.getProperties().get("test");

        assertNotNull(userProperty);
        assertEquals("", userProperty.getter().descriptionKey());
        assertEquals("", userProperty.getter().displayNameKey());
        assertTrue(userProperty.getter().user());
        assertTrue(userProperty.getter().domain());
        assertTrue(userProperty.getter().global());
        assertEquals(String.class, userProperty.propertyDescriptor().getPropertyType());

        userProperty = userPropertyRegistry.getProperties().get("test2");
        assertFalse(userProperty.getter().user());
        assertFalse(userProperty.getter().domain());
        assertFalse(userProperty.getter().global());
        assertEquals(boolean.class, userProperty.propertyDescriptor().getPropertyType());

        List<UserPropertyRegistry.UserPropertyDescriptor> descriptors = userPropertyRegistry.getGroupedProperties()
                .get(TestProperties.class.getAnnotation(UserPropertiesType.class));

        assertEquals(2, descriptors.size());
    }

    @Test
    public void testAnnotationOnSetterOnly()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, AnnotationOnSetterOnly.class));

        assertEquals("Method public void com.ciphermail.core.app.properties.UserPropertyRegistryImplTest" +
                     "$AnnotationOnSetterOnly.setValue(java.lang.String) of class class " +
                     "com.ciphermail.core.app.properties.UserPropertyRegistryImplTest$AnnotationOnSetterOnly " +
                     "is not a valid public bean method or is only a setter property", e.getMessage());
    }

    @Test
    public void testNoProperties()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, NoProperties.class));

        assertEquals("PropertyDescriptor's for class class com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$NoProperties not found", e.getMessage());
    }

    @Test
    public void testNoBeanMethod()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, NoBeanMethod.class));

        assertEquals("Method public void com.ciphermail.core.app.properties.UserPropertyRegistryImplTest" +
                     "$NoBeanMethod.test() of class class com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$NoBeanMethod is not a valid public bean method " +
                     "or is only a setter property", e.getMessage());
    }

    @Test
    public void testDuplicateMethod()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, DuplicateMethod.class));

        assertEquals("There is already a property named test (public boolean " +
                     "com.ciphermail.core.app.properties.UserPropertyRegistryImplTest$DuplicateMethod.isReadOnly2())",
                e.getMessage());
    }

    @Test
    public void testDuplicateInvalidMethod()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, DuplicateInvalidMethod.class));

        assertEquals("Method public void com.ciphermail.core.app.properties.UserPropertyRegistryImplTest$" +
                     "DuplicateInvalidMethod.test() of class class com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$DuplicateInvalidMethod a contains a duplicate property",
                e.getMessage());
    }

    @Test
    public void testPrivateMethod()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, PrivateMethod.class));

        assertEquals("Method private boolean com.ciphermail.core.app.properties.UserPropertyRegistryImplTest" +
                     "$PrivateMethod.isSet1() of class class com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$PrivateMethod is not a valid public bean method or is only a " +
                     "setter property",
                e.getMessage());
    }

    @Test
    public void testNoSetterAnnotation()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, NoSetterAnnotation.class));

        assertEquals("The setter does not contain a UserProperty annotation (public void com.ciphermail." +
                     "core.app.properties.UserPropertyRegistryImplTest$NoSetterAnnotation.setValue(java.lang.String))",
                e.getMessage());
    }

    @Test
    public void testPropertyNameMismatch()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, PropertyNameMismatch.class));

        assertEquals("Name of the setter (public void com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$PropertyNameMismatch.setValue(java.lang.String)) does not match " +
                     "the name of the getter (public java.lang.String com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$PropertyNameMismatch.getValue())",
                e.getMessage());
    }

    @Test
    public void testDisplayKeyMismatch()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, DisplayKeyMismatch.class));

        assertEquals("displayNameKey of the setter (public void com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$DisplayKeyMismatch.setValue(java.lang.String)) does not match " +
                     "the displayNameKey of the getter (public java.lang.String com.ciphermail.core.app." +
                     "properties.UserPropertyRegistryImplTest$DisplayKeyMismatch.getValue())",
                e.getMessage());
    }

    @Test
    public void testDescriptionKeyMismatch()
    {
        UserPropertyRegistryImpl userPropertyRegistry = new UserPropertyRegistryImpl(new MockEnvironment());

        IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () ->
                userPropertyRegistry.addClasses(mockUserPropertiesType, DescriptionKeyMismatch.class));

        assertEquals("descriptionKey of the setter (public void com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$DescriptionKeyMismatch.setValue(java.lang.String)) does not match " +
                     "the descriptionKey of the getter (public java.lang.String com.ciphermail.core.app.properties." +
                     "UserPropertyRegistryImplTest$DescriptionKeyMismatch.getValue())",
                e.getMessage());
    }
}