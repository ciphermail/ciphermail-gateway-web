/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.openpgp.PGPKeyRing;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporterImpl;
import com.ciphermail.core.common.security.openpgp.PGPRecursiveValidatingMIMEHandler;
import com.ciphermail.core.common.security.openpgp.PGPTestUtils;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListEntry;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListStatus;
import com.ciphermail.core.common.security.password.StaticPasswordProvider;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.bouncycastle.openpgp.PGPException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class PGPMIMESignTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PGPKeyRing keyRing;

    @Autowired
    private PGPTrustList trustList;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Before
    public void before()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                keyRing.deleteAll();
                trustList.deleteAll();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void importKeyRing(File file, String password)
    {
        transactionOperations.execute(status ->
        {
            try {
                return new PGPKeyRingImporterImpl(keyRing).importKeyRing(FileUtils.openInputStream(file),
                        new StaticPasswordProvider(password)).size();
            }
            catch (IOException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void trustKey(long keyID) {
        trustKey(keyID, false);
    }

    private void trustKey(long keyID, boolean includeSubKeys)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PGPKeyRingEntry keyEntry = CloseableIteratorUtils.toList(keyRing.getByKeyID(keyID)).get(0);

                PGPTrustListEntry trustEntry = trustList.createEntry(keyEntry.getPublicKey());

                trustEntry.setStatus(PGPTrustListStatus.TRUSTED);

                trustList.setEntry(trustEntry, includeSubKeys);
            }
            catch (IOException | PGPTrustListException | CloseableIteratorException | PGPException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean verifyWithBC(MimeMessage signedMessage)
    throws Exception
    {
        // Clone message to make sure it has been written
        signedMessage = MailUtils.cloneMessage(signedMessage);

        boolean verified = false;

        PGPRecursiveValidatingMIMEHandler handler = PGPTestUtils.createPGPHandler(
                new File(TestUtils.getTestDataDir(),
                        "pgp/test@example.com.gpg.asc"), null, null);

        MimeMessage handled = handler.handleMessage(signedMessage);

        if (handled != null) {
            verified = "True".equals(handled.getHeader("X-CipherMail-Info-PGP-Signature-Valid", ","));
        }

        return verified;
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PGPMIMESign();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testTextSignature()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        assertNull(mail.getState());

        assertTrue(signedMessage.isMimeType("multipart/signed"));
        assertEquals(sourceMessage.getMessageID(), signedMessage.getMessageID());

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                        "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void testBinarySignature()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signatureType", "binary")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        assertNull(mail.getState());

        assertTrue(signedMessage.isMimeType("multipart/signed"));
        assertEquals(sourceMessage.getMessageID(), signedMessage.getMessageID());

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: binary signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void testMissingSigningKey()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signatureType", "binary")
                .setProperty("signedProcessor", "signed")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("initial", mail.getState());

        assertSame(sourceMessage, mail.getMessage());
    }

    @Test
    public void testSetSignAlgorithmAttribute()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "pgp.signing-algorithm")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mail.setAttribute(Attribute.convertToAttribute("pgp.signing-algorithm", "SHA512"));

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA512"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA512"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void testOverrideDefaultSigningAlgorithm()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("signingAlgorithmAttribute", "non-existing-attribute")
                .setProperty("signingAlgorithm", "RIPEMD160")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: RIPE-MD/160"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm RIPEMD160"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void testNotRetainMessageId()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("retainMessageID", "false")
                .setProperty("signedProcessor", "signed")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/message-with-attach-encoded-filename.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        assertEquals("<111>", sourceMessage.getMessageID());

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        assertNotEquals("<111>", signedMessage.getMessageID());

        assertEquals("signed", mail.getState());

        MailUtils.validateMessage(signedMessage);

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    /*
     * Check if Message is signed even though the content-transfer-encoding is invalid
     */
    @Test
    public void testInvalidContentTransferEncoding()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-content-transfer-encoding.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        assertEquals("xxx", sourceMessage.getHeader("content-transfer-encoding", ","));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void test8BitMultipart()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-multipart.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        String mime = MailUtils.partToMimeString(signedMessage);

        // Check if 8bit was converted to 7bit QP
        assertThat(mime, containsString("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertThat(mime, containsString("This is a test with unlauts: Sch=C3=B6n"));
        // a part was set to 7bit but the content was 8bit. Some chars therefore are changed into ?
        assertThat(mime, containsString("Sch??n"));

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void test8BitTextOnlyPart()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-text.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        String mime = MailUtils.partToMimeString(signedMessage);

        // Check if 8bit was converted to 7bit QP
        assertThat(mime, containsString("X-MIME-Autoconverted: from 8bit to 7bit by CipherMail"));
        assertThat(mime, containsString("This is a test with unlauts: Sch=C3=B6n"));

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String output = outputWriter.toString();
        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }

    @Test
    public void testRetryOnFailure()
    throws Exception
    {
        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        importKeyRing(new File(TestUtils.getTestDataDir(),
                "pgp/test@example.com.gpg.key"), "test");

        trustKey(PGPUtils.getKeyIDFromHex("4EC4E8813E11A9A0"));

        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/8bit-text.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        PGPMIMESign mailet = new PGPMIMESign()
        {
            @Override
            protected void serviceMailTransacted(Mail mail)
            throws MessagingException, IOException
            {
                super.serviceMailTransacted(mail);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Some error occurred. Error Message: Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was PGP/MIME signed. Hash algorithm: SHA256"));

        assertTrue(constraintViolationThrown.booleanValue());

        MimeMessage signedMessage = mail.getMessage();

        MailUtils.validateMessage(signedMessage);

        String mime = MailUtils.partToMimeString(signedMessage);

        // The message should be signed only once
        assertEquals(1, StringUtils.countMatches(mime, "-----BEGIN PGP SIGNATURE-----"));

        Writer outputWriter = new StringWriter();
        Writer errorWriter = new StringWriter();

        assertTrue(PGPTestUtils.verifyPGPMIME(signedMessage, outputWriter, errorWriter));

        String error = errorWriter.toString();

        assertThat(error, containsString("gpg: Good signature from \"test key djigzo (this is a test key) " +
                                         "<test@example.com>\""));
        assertThat(error, containsString("gpg: textmode signature, digest algorithm SHA256"));

        assertTrue(verifyWithBC(signedMessage));
        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP/MIME signature was valid"));
    }
}
