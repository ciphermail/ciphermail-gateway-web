/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.KeyIdentifier;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.cms.KeyTransRecipientIdImpl;
import com.ciphermail.core.common.security.cms.RecipientInfo;
import com.ciphermail.core.common.security.keystore.KeyStoreKeyProvider;
import com.ciphermail.core.common.security.keystore.MockBasicKeyStore;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.common.security.smime.SMIMEType;
import com.ciphermail.core.common.util.WordIterator;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.OpenSSL;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.BodyPart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SMIMEEncryptTest
{
    @Autowired
    private AbstractApplicationContext applicationContext;

    private static Certificates certificates;

    @BeforeClass
    public static void setUpBeforeClass()
    throws Exception
    {
        Collection<X509Certificate> x509Certificates = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/testCertificates.p7b"));

        certificates = new Certificates(new HashSet<>(x509Certificates));
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private static KeyStore loadKeyStore(File file, String password)
    throws Exception
    {
        KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        keyStore.load(new FileInputStream(file), password.toCharArray());

        return keyStore;
    }

    private static String as1Dump(MimeMessage message)
    throws Exception
    {
        OpenSSL openssl = new OpenSSL();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream error = new ByteArrayOutputStream();

        File file = TestUtils.createTempFile(".eml");

        try {
            MailUtils.writeMessage(message, file);

            openssl.cmsASN1Dump(file, output, error);
        }
        finally {
            IOUtils.closeQuietly(output);
            IOUtils.closeQuietly(error);
            FileUtils.delete(file);
        }

        String asn1 = output.toString(StandardCharsets.UTF_8);

        // Print before normalization
        System.out.println(asn1);

        // Normalize the asn1 dump to make it easier to match
        WordIterator wi = new WordIterator(new StringReader(asn1));

        StringWriter sw = new StringWriter();

        String word;

        while ((word = wi.nextWord()) != null)
        {
            word = StringUtils.trimToNull(word);

            if (word != null) {
                sw.append(word.toLowerCase()).append(' ');
            }
        }

        return sw.toString();
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SMIMEEncrypt();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    /*
     * Encrypt large message and test the performance
     */
    @Test
    public void testPerformanceTest()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/test-5120K.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        int repeat = 10;

        long start = System.currentTimeMillis();

        for (int i = 0; i < repeat; i++)
        {
            FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                    .recipients("test@example.com")
                    .sender("test@example.com")
                    .mimeMessage(sourceMessage)
                    .build();

            // reset log output since we are only interested in the logs produced while handling not during init
            memoryLogAppender.reset();

            CoreApplicationMailAttributes.setCertificates(mail, certificates);

            mailet.service(mail);

            checkLogsForErrorsOrExceptions();

            assertThat(memoryLogAppender.getLogOutput(), containsString(
                    "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                    "Encryption Scheme: RSAES_PKCS1_V1_5"));

            MailUtils.validateMessage(mail.getMessage());

            SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                    SecurityFactoryBouncyCastle.PROVIDER_NAME);

            assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
            assertEquals(SMIMEEncryptionAlgorithm.AES128_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                    getEncryptionAlgorithmOID());
            assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());
        }

        long diff = System.currentTimeMillis() - start;

        double perSecond = repeat * 1000.0 / diff;

        System.out.println("S/MIME ecryptions/sec: " + perSecond);

        float exected = 1;

        // NOTE: !!! can fail on a slower system
        assertThat("S/MIME encryption too slow. Expected " + exected + " but was" + perSecond,
                perSecond > exected);
    }

    /*
     * Test what happens when an invalid message (a message that fails on writeTo) is encrypted.
     */
    @Test
    public void testCorruptMessage()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = new MimeMessage(MailSession.getDefaultSession());

        MimeBodyPart emptyPart = new MimeBodyPart();

        sourceMessage.setContent(emptyPart, "text/plain");

        sourceMessage.saveChanges();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        assertSame(sourceMessage, mail.getMessage());

        try {
            MailUtils.validateMessage(mail.getMessage());

            fail();
        }
        catch(IOException e)
        {
            // expected. The message should be corrupt
            assertThat(memoryLogAppender.getLogOutput(), containsString(
                    "Error reading the message"));
        }
    }

    /*
     * sometimes multipart messages contain a Content-Transfer-Encoding different from 7bit. This is strictly speaking
     * not allowed (according to RFCs). Javamail (until 1.4.4) does not ignore the Content-Transfer-Encoding like it
     * should. This test tests if the Content-Transfer-Encoding is ignored. This test should fail with Javamail <= 1.4.3
     * and succeed with Javamail >= 1.4.4
     */
    @Test
    public void testMultipartWithIllegalContentTransferEncoding()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/quoted-printable-multipart.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertTrue(sourceMessage.isMimeType("multipart/mixed"));

        MimeMultipart mp = (MimeMultipart) sourceMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart part = mp.getBodyPart(0);

        assertTrue(part.isMimeType("text/plain"));
        assertEquals("==", part.getContent());

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(sourceMessage.getMessageID(), mail.getMessage().getMessageID());

        KeyStoreKeyProvider keyStore = new KeyStoreKeyProvider(loadKeyStore(
                new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"), "test"),
                "test");

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), keyStore,
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES128_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        decrypted.saveChanges();

        assertNotNull(decrypted);

        assertTrue(decrypted.isMimeType("multipart/mixed"));

        mp = (MimeMultipart) decrypted.getContent();

        assertEquals(2, mp.getCount());

        part = mp.getBodyPart(0);

        assertTrue(part.isMimeType("text/plain"));

        // The body should not be changed to =3D=3D because the body should not be quoted-printable encoded
        // again
        assertEquals("==", part.getContent());
    }

    @Test
    public void testNoRetainMessageId()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES256_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA384")
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        assertEquals("<123456>", sourceMessage.getMessageID());

        // For AES256 and SHA384, we need at least a 2048 bits key
        Collection<X509Certificate> x509Certificates = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/RSA-2048.cer"));

        CoreApplicationMailAttributes.setCertificates(mail, new Certificates(new HashSet<>(x509Certificates)));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES256_CBC; Key size: 256; " +
                "Encryption Scheme: RSAES_OAEP_SHA384"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, mail.getMessage().getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES256_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(1, inspector.getEnvelopedInspector().getRecipients().size());

        assertNotEquals("<123456>", mail.getMessage().getMessageID());
        assertTrue(mail.getMessage().getMessageID().contains("JavaMail"));
    }

    /*
     * This test tests whether an email message with unknown content-transfer-encoding can be encrypted. Javamail
     * prior to 1.4.4 (i.e. <= 1.4.3) would throw exceptions when handling unknown content-transfer-encoding's. Since
     * 1.4.4 Javamail silently ignores the unknown encoding.
     *
     * This test has been modified to reflect the new behavior since 1.4.4
     */
    @Test
    public void testInvalidEncoding()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES192_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA256")
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/unknown-content-transfer-encoding.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        assertEquals("xxx", sourceMessage.getEncoding());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES192_CBC; Key size: 192; " +
                "Encryption Scheme: RSAES_OAEP_SHA256"));

        MailUtils.validateMessage(mail.getMessage());

        assertNotEquals("xxx", mail.getMessage().getEncoding());

        assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, mail.getMessage().getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES192_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());
    }

    @Test
    public void testDeprecatedContentType()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES256_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA1")
                .setProperty("useDeprecatedContentTypes", "true")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES256_CBC; Key size: 256; " +
                "Encryption Scheme: RSAES_OAEP_SHA1"));

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(SMIMEHeader.DEPRECATED_ENCRYPTED_CONTENT_TYPE, mail.getMessage().getContentType());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES256_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());
    }

    @Test
    public void testOnlyAttachment()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/only-attachment.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MailUtils.validateMessage(mail.getMessage());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES128_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());
    }

    @Test
    public void testDefaultSettings()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MailUtils.validateMessage(mail.getMessage());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(SMIMEEncryptionAlgorithm.AES128_CBC.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());

        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());


        int issuerSerialCount = 0;
        int subjectKeyIdentifierCount = 0;

        for (RecipientInfo recipientInfo : inspector.getEnvelopedInspector().getRecipients())
        {
            KeyIdentifier keyIdentifier = recipientInfo.getRecipientId();

            assertThat(keyIdentifier, instanceOf(KeyTransRecipientIdImpl.class));

            KeyTransRecipientIdImpl keyTransRecipientIdImpl = (KeyTransRecipientIdImpl) keyIdentifier;

            if (keyTransRecipientIdImpl.getSubjectKeyIdentifier() != null)
            {
                assertNull(keyTransRecipientIdImpl.getIssuer());
                assertNull(keyTransRecipientIdImpl.getSerialNumber());
                subjectKeyIdentifierCount = subjectKeyIdentifierCount + 1;
            }
            else {
                assertNotNull(keyTransRecipientIdImpl.getIssuer());
                assertNotNull(keyTransRecipientIdImpl.getSerialNumber());
                issuerSerialCount = issuerSerialCount + 1;
            }
        }

        assertEquals(20, issuerSerialCount);
        assertEquals(0, subjectKeyIdentifierCount);

        assertNull(mail.getState());

        String asn1 = as1Dump(mail.getMessage());

        /*
         * The result might be different for different openssl versions. We therefore use a regex
         */
        assertTrue(Pattern.compile(
            "cms_contentinfo: contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: version: (<absent>|0) originatorinfo: (<absent>|0) recipientinfos: d.ktri: " +
            "version: (<absent>|0) " +
            "d.issuerandserialnumber: issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443394451216042738351345741452842864 " +
            "keyencryptionalgorithm: algorithm: rsaencryption \\(1.2.840.113549.1.1.1\\) parameter: null")
            .matcher(asn1).find());

        assertTrue(asn1.contains(
            "encryptedcontentinfo: contenttype: pkcs7-data (1.2.840.113549.1.7.1) contentencryptionalgorithm: " +
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRecipientModeBoth()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES256_CBC")
                .setProperty("recipientMode", "BOTH")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES256_CBC; Key size: 256; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MailUtils.validateMessage(mail.getMessage());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), new MockBasicKeyStore(),
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        // there are 10 certificates with a subjectKeyIdentifier so 20 + 10 = 30
        assertEquals(30, inspector.getEnvelopedInspector().getRecipients().size());

        int issuerSerialCount = 0;
        int subjectKeyIdentifierCount = 0;

        for (RecipientInfo recipientInfo : inspector.getEnvelopedInspector().getRecipients())
        {
            KeyIdentifier keyIdentifier = recipientInfo.getRecipientId();

            assertThat(keyIdentifier, instanceOf(KeyTransRecipientIdImpl.class));

            KeyTransRecipientIdImpl keyTransRecipientIdImpl = (KeyTransRecipientIdImpl) keyIdentifier;

            if (keyTransRecipientIdImpl.getSubjectKeyIdentifier() != null)
            {
                assertNull(keyTransRecipientIdImpl.getIssuer());
                assertNull(keyTransRecipientIdImpl.getSerialNumber());
                subjectKeyIdentifierCount = subjectKeyIdentifierCount + 1;
            }
            else {
                assertNotNull(keyTransRecipientIdImpl.getIssuer());
                assertNotNull(keyTransRecipientIdImpl.getSerialNumber());
                issuerSerialCount = issuerSerialCount + 1;
            }
        }

        assertEquals(20, issuerSerialCount);
        assertEquals(10, subjectKeyIdentifierCount);
    }

    @Test
    public void testProtectedHeadersStatic()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("retainMessageID", "false")
                .setProperty("protectedHeader", "from, message-id")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        assertEquals("<123456>", sourceMessage.getMessageID());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MimeMessage encrypted = mail.getMessage();

        // Change from to see whether it was protected
        encrypted.setFrom(new InternetAddress("changed@changed.example.com"));

        assertEquals("changed@changed.example.com", StringUtils.join(encrypted.getFrom()));

        MailUtils.validateMessage(encrypted);

        assertNotEquals("<123456>", encrypted.getMessageID());

        assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, encrypted.getContentType());

        KeyStoreKeyProvider keyStore = new KeyStoreKeyProvider(loadKeyStore(
                new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"), "test"),
                "test");

        SMIMEInspector inspector = new SMIMEInspectorImpl(encrypted, keyStore,
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // now decrypt and check whether message-id and from was protected
        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        inspector = new SMIMEInspectorImpl(decrypted, keyStore, SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("<123456>", decrypted.getMessageID());
        assertEquals("from@example.com", StringUtils.join(decrypted.getFrom()));
    }

    @Test
    public void testProtectedHeadersMailAttributes()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("retainMessageID", "false")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        assertEquals("<123456>", sourceMessage.getMessageID());

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        CoreApplicationMailAttributes.setProtectedHeaders(mail, List.of("from", "message-id"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        MimeMessage encrypted = mail.getMessage();

        // Change from to see whether it was protected
        encrypted.setFrom(new InternetAddress("changed@changed.example.com"));

        assertEquals("changed@changed.example.com", StringUtils.join(encrypted.getFrom()));

        MailUtils.validateMessage(encrypted);

        assertNotEquals("<123456>", encrypted.getMessageID());

        assertEquals(SMIMEHeader.ENCRYPTED_CONTENT_TYPE, encrypted.getContentType());

        KeyStoreKeyProvider keyStore = new KeyStoreKeyProvider(loadKeyStore(
                new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"), "test"),
                "test");

        SMIMEInspector inspector = new SMIMEInspectorImpl(encrypted, keyStore,
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.ENCRYPTED, inspector.getSMIMEType());

        // now decrypt and check whether message-id and from was protected
        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        inspector = new SMIMEInspectorImpl(decrypted, keyStore, SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(SMIMEType.NONE, inspector.getSMIMEType());

        assertEquals("<123456>", decrypted.getMessageID());
        assertEquals("from@example.com", StringUtils.join(decrypted.getFrom()));
    }

    /*
     * This is not a unit test but a helper method for testAlgorithmAttribute unit test
     */
    private void encrypt(SMIMEEncryptionAlgorithm algorithm, KeyStoreKeyProvider keyStore)
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", algorithm.name())
                .setProperty("algorithmAttribute", "algorithm")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        assertNotNull(algorithm);

        mail.setAttribute(Attribute.convertToAttribute("algorithm", algorithm.name()));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: " + algorithm.name()));

        MailUtils.validateMessage(mail.getMessage());

        SMIMEInspector inspector = new SMIMEInspectorImpl(mail.getMessage(), keyStore,
                SecurityFactoryBouncyCastle.PROVIDER_NAME);

        assertEquals(algorithm.isAuth() ? SMIMEType.AUTH_ENCRYPTED : SMIMEType.ENCRYPTED, inspector.getSMIMEType());
        assertEquals(algorithm.getOID().toString(), inspector.getEnvelopedInspector().
                getEncryptionAlgorithmOID());
        assertEquals(20, inspector.getEnvelopedInspector().getRecipients().size());

        MimeMessage decrypted = inspector.getContentAsMimeMessage();

        assertEquals("test", ((String)decrypted.getContent()).trim());
    }

    @Test
    public void testAlgorithmAttribute()
    throws Exception
    {
        KeyStoreKeyProvider keyStore = new KeyStoreKeyProvider(loadKeyStore(
                new File(TestUtils.getTestDataDir(), "keys/testCertificates.p12"), "test"),
                "test");

        for (SMIMEEncryptionAlgorithm algorithm : SMIMEEncryptionAlgorithm.values())
        {
            System.out.println("Using algorithm " + algorithm);

            encrypt(algorithm, keyStore);
        }
    }

    @Test
    public void testEncryptedProcessor()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_PKCS1_V1_5"));

        assertEquals("encrypted", mail.getState());
    }

    @Test
    public void testEncryptedProcessorNoEncryption()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptedProcessor", "encrypted")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .state("initial")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Certificates attribute not found."));

        assertEquals("initial", mail.getState());
    }

    @Test
    public void testRSAES_OAEP_SHA1()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA1")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_OAEP_SHA1"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
            "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: " +
            "version: (<absent>|0) " +
            "originatorinfo: (<absent>|0) " +
            "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
            "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443394451216042738351345741452842864 " +
            "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 0 cons: sequence " +
            "encryptedkey:"
            )
            .matcher(asn1).find());

        assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: "+
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRSAES_OAEP_SHA1Attribute()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptionSchemeAttribute", "encryptionScheme")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        mail.setAttribute(Attribute.convertToAttribute("encryptionScheme", "RSAES_OAEP_SHA1"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_OAEP_SHA1"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
                        "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
                        "d.envelopeddata: " +
                        "version: (<absent>|0) " +
                        "originatorinfo: (<absent>|0) " +
                        "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
                        "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                        "serialnumber: 1443394451216042738351345741452842864 " +
                        "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
                        "parameter: sequence: " +
                        "0:d=0 hl=2 l= 0 cons: sequence " +
                        "encryptedkey:"
                )
                .matcher(asn1).find());

        assertTrue(asn1.contains(
                "encryptedcontentinfo: " +
                "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
                "contentencryptionalgorithm: "+
                "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRSAES_OAEP_SHA256()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA256")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_OAEP_SHA256"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
            "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: " +
            "version: (<absent>|0) " +
            "originatorinfo: (<absent>|0) " +
            "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
            "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443394451216042738351345741452842864 " +
            "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 47 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont \\[ 0 ] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha256 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont \\[ 1 ] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha256 " +
            "47:d=4 hl=2 l= 0 prim: null"
            )
            .matcher(asn1).find());

        assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: "+
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRSAES_OAEP_SHA384()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA384")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        CoreApplicationMailAttributes.setCertificates(mail, certificates);

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_OAEP_SHA384"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
            "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
            "d.envelopeddata: " +
            "version: (<absent>|0) " +
            "originatorinfo: (<absent>|0) " +
            "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
            "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
            "serialnumber: 1443394451216042738351345741452842864 " +
            "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
            "parameter: sequence: " +
            "0:d=0 hl=2 l= 47 cons: sequence " +
            "2:d=1 hl=2 l= 15 cons: cont \\[ 0 ] " +
            "4:d=2 hl=2 l= 13 cons: sequence " +
            "6:d=3 hl=2 l= 9 prim: object :sha384 " +
            "17:d=3 hl=2 l= 0 prim: null " +
            "19:d=1 hl=2 l= 28 cons: cont \\[ 1 ] " +
            "21:d=2 hl=2 l= 26 cons: sequence " +
            "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
            "34:d=3 hl=2 l= 13 cons: sequence " +
            "36:d=4 hl=2 l= 9 prim: object :sha384 " +
            "47:d=4 hl=2 l= 0 prim: null"
            )
            .matcher(asn1).find());

        assertTrue(asn1.contains(
            "encryptedcontentinfo: " +
            "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
            "contentencryptionalgorithm: "+
            "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRSAES_OAEP_SHA512()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES128_CBC")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA512")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // For SHA512, we need a 2048 bits key
        Collection<X509Certificate> x509Certificates = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/RSA-2048.cer"));

        CoreApplicationMailAttributes.setCertificates(mail, new Certificates(new HashSet<>(x509Certificates)));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES128_CBC; Key size: 128; " +
                "Encryption Scheme: RSAES_OAEP_SHA512"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
                        "contenttype: pkcs7-envelopeddata \\(1.2.840.113549.1.7.3\\) " +
                        "d.envelopeddata: " +
                        "version: (<absent>|0) " +
                        "originatorinfo: (<absent>|0) " +
                        "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
                        "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                        "serialnumber: 1443399424402921266693579916096623720 " +
                        "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
                        "parameter: sequence: " +
                        "0:d=0 hl=2 l= 47 cons: sequence " +
                        "2:d=1 hl=2 l= 15 cons: cont \\[ 0 ] " +
                        "4:d=2 hl=2 l= 13 cons: sequence " +
                        "6:d=3 hl=2 l= 9 prim: object :sha512 " +
                        "17:d=3 hl=2 l= 0 prim: null " +
                        "19:d=1 hl=2 l= 28 cons: cont \\[ 1 ] " +
                        "21:d=2 hl=2 l= 26 cons: sequence " +
                        "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
                        "34:d=3 hl=2 l= 13 cons: sequence " +
                        "36:d=4 hl=2 l= 9 prim: object :sha512 " +
                        "47:d=4 hl=2 l= 0 prim: null"
                )
                .matcher(asn1).find());

        assertTrue(asn1.contains(
                "encryptedcontentinfo: " +
                "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
                "contentencryptionalgorithm: "+
                "algorithm: aes-128-cbc (2.16.840.1.101.3.4.1.2)"
        ));
    }

    @Test
    public void testRSAES256_GCM_OAEP_SHA512()
    throws Exception
    {
        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .setProperty("algorithm", "AES256_GCM")
                .setProperty("encryptionScheme", "RSAES_OAEP_SHA512")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/simple-text-message-with-id.eml");

        sourceMessage.setFrom(new InternetAddress("from@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("test@example.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        // For SHA512, we need a 2048 bits key
        Collection<X509Certificate> x509Certificates = CertificateUtils.readX509Certificates(
                new File(TestUtils.getTestDataDir(), "certificates/RSA-2048.cer"));

        CoreApplicationMailAttributes.setCertificates(mail, new Certificates(new HashSet<>(x509Certificates)));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Message was S/MIME encrypted. Encryption algorithm: AES256_GCM; Key size: 256; " +
                "Encryption Scheme: RSAES_OAEP_SHA512"));

        String asn1 = as1Dump(mail.getMessage());

        // The result might be different for different openssl versions. We therefore use a regex
        assertTrue(Pattern.compile(
                        "contenttype: id-smime-ct-authenvelopeddata \\(1.2.840.113549.1.9.16.1.23\\) " +
                        "d.authenvelopeddata: " +
                        "version: (<absent>|0) " +
                        "originatorinfo: (<absent>|0) " +
                        "recipientinfos: d.ktri: version: (<absent>|0) d.issuerandserialnumber: " +
                        "issuer: c=nl, st=nh, l=amsterdam, cn=mitm test ca/emailaddress=ca@example.com " +
                        "serialnumber: 1443399424402921266693579916096623720 " +
                        "keyencryptionalgorithm: algorithm: rsaesoaep \\(1.2.840.113549.1.1.7\\) " +
                        "parameter: sequence: " +
                        "0:d=0 hl=2 l= 47 cons: sequence " +
                        "2:d=1 hl=2 l= 15 cons: cont \\[ 0 ] " +
                        "4:d=2 hl=2 l= 13 cons: sequence " +
                        "6:d=3 hl=2 l= 9 prim: object :sha512 " +
                        "17:d=3 hl=2 l= 0 prim: null " +
                        "19:d=1 hl=2 l= 28 cons: cont \\[ 1 ] " +
                        "21:d=2 hl=2 l= 26 cons: sequence " +
                        "23:d=3 hl=2 l= 9 prim: object :mgf1 " +
                        "34:d=3 hl=2 l= 13 cons: sequence " +
                        "36:d=4 hl=2 l= 9 prim: object :sha512 " +
                        "47:d=4 hl=2 l= 0 prim: null"
                )
                .matcher(asn1).find());

        assertTrue(asn1.contains(
                "authencryptedcontentinfo: " +
                "contenttype: pkcs7-data (1.2.840.113549.1.7.1) " +
                "contentencryptionalgorithm: "+
                "algorithm: aes-256-gcm (2.16.840.1.101.3.4.1.46)"
        ));
    }
}
