/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.NamedCertificate;
import com.ciphermail.core.test.TestUtils;
import org.junit.Test;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class NamedCertificateUtilsTest
{
    private static final File CERT_BASE = new File(TestUtils.getTestDataDir(), "certificates/");

    @Test
    public void testGetByName()
    {
        Set<NamedCertificate> namedCertificates = new HashSet<>();

        X509Certificate certificate1 = TestUtils.loadCertificate(new File(CERT_BASE, "equifax.cer"));

        X509Certificate certificate2 = TestUtils.loadCertificate(new File(CERT_BASE, "testcertificate.cer"));

        X509Certificate certificate3 = TestUtils.loadCertificate(new File(CERT_BASE, "special-extensions.cer"));

        namedCertificates.add(new NamedCertificateImpl("name", certificate2));
        namedCertificates.add(new NamedCertificateImpl("name", certificate3));
        namedCertificates.add(new NamedCertificateImpl("other", certificate1));
        namedCertificates.add(new NamedCertificateImpl("other", certificate2));
        namedCertificates.add(new NamedCertificateImpl("other", certificate3));

        // Add a duplicate (to test the equals and hash)
        namedCertificates.add(new NamedCertificateImpl("name", certificate2));

        assertEquals(5, namedCertificates.size());

        Set<X509Certificate> matching = NamedCertificateUtils.getByName("name", namedCertificates);

        assertEquals(2, matching.size());

        assertFalse(matching.contains(certificate1));
        assertTrue(matching.contains(certificate2));
        assertTrue(matching.contains(certificate3));

        matching = NamedCertificateUtils.getByName("other", namedCertificates);

        assertEquals(3, matching.size());

        assertTrue(matching.contains(certificate1));
        assertTrue(matching.contains(certificate2));
        assertTrue(matching.contains(certificate3));

        matching = NamedCertificateUtils.getByName("no-match", namedCertificates);

        assertEquals(0, matching.size());
    }

    @Test
    public void testReplaceNamedCertificates()
    {
        Set<NamedCertificate> namedCertificates = new HashSet<>();

        X509Certificate certificate1 = TestUtils.loadCertificate(new File(CERT_BASE, "equifax.cer"));

        X509Certificate certificate2 = TestUtils.loadCertificate(new File(CERT_BASE, "testcertificate.cer"));

        X509Certificate certificate3 = TestUtils.loadCertificate(new File(CERT_BASE, "special-extensions.cer"));

        X509Certificate certificate4 = TestUtils.loadCertificate(new File(CERT_BASE, "certwithldapcrl.cer"));

        namedCertificates.add(new NamedCertificateImpl("name", certificate2));
        namedCertificates.add(new NamedCertificateImpl("name", certificate3));
        namedCertificates.add(new NamedCertificateImpl("other", certificate1));
        namedCertificates.add(new NamedCertificateImpl("other", certificate2));
        namedCertificates.add(new NamedCertificateImpl("other", certificate3));

        assertEquals(5, namedCertificates.size());

        NamedCertificateUtils.replaceNamedCertificates(namedCertificates, "other", Collections.singleton(certificate4));

        assertEquals(3, namedCertificates.size());

        Set<X509Certificate> matching = NamedCertificateUtils.getByName("name", namedCertificates);

        assertEquals(2, matching.size());

        matching = NamedCertificateUtils.getByName("other", namedCertificates);

        assertEquals(1, matching.size());
    }

    @Test
    public void testGetByNameNullParams()
    {
        Set<X509Certificate> matching = NamedCertificateUtils.getByName(null, null);

        assertEquals(0, matching.size());
    }
}
