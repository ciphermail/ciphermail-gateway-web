/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.hibernate.NamedBlobDAO;
import com.ciphermail.core.common.properties.hibernate.NamedBlobEntity;
import com.ciphermail.core.common.security.certstore.dao.X509CertStoreDAO;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.security.cert.X509Certificate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@Transactional(rollbackFor = Exception.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@EnableTransactionManagement
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPreferencesDAOTest
{
    private static final File TEST_BASE = TestUtils.getTestDataDir();

    @Autowired
    private SessionManager sessionManager;

    @Before
    public void setup()
    {
        UserPreferencesDAO dao = createDAO();

        // Delete all users
        for (UserPreferencesEntity entity : dao.findAll(UserPreferencesEntity.class)) {
            dao.delete(entity);
        }
    }

    private UserPreferencesDAO createDAO() {
        return UserPreferencesDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private X509CertStoreDAO createX509CertStoreDAO() {
        return X509CertStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), "test");
    }

    private NamedBlobDAO createNamedBlobDAO() {
        return NamedBlobDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Test
    public void testGetPreferences()
    {
        UserPreferencesDAO dao = createDAO();

        assertNull(dao.getPreferences("cat1", "name1"));

        dao.addPreferences("cat2", "name1");
        assertNull(dao.getPreferences("cat1", "name1"));

        dao.addPreferences("cat1", "name2");
        assertNull(dao.getPreferences("cat1", "name1"));

        dao.addPreferences("cat1", "name1");

        assertNotNull(dao.getPreferences("cat1", "name1"));
    }

    @Test
    public void testStandardObjectMethods()
    {
        UserPreferencesDAO dao = createDAO();

        UserPreferencesEntity p1 = dao.addPreferences("cat1", "name1");
        UserPreferencesEntity p2 = dao.addPreferences("cat1", "name2");
        UserPreferencesEntity p3 = dao.addPreferences("cat2", "name1");

        assertEquals("cat1:name1", p1.toString());
        assertEquals("cat1:name2", p2.toString());
        assertEquals("cat2:name1", p3.toString());

        assertEquals(p1, p1);
        assertEquals(p1.hashCode(), p1.hashCode());
        assertNotEquals(p1, p2);
        assertNotEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1, p3);
    }

    @Test
    public void testGetPreferencesIterator()
    throws CloseableIteratorException
    {
        UserPreferencesDAO dao = createDAO();

        List<String> preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat1"));

        assertEquals(0, preferences.size());

        dao.addPreferences("cat1", "name1");
        dao.addPreferences("cat1", "name2");

        dao.addPreferences("cat2", "name3");
        dao.addPreferences("cat2", "name4");

        preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat1"));

        assertEquals(2, preferences.size());

        preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat1", null, null, null));

        assertEquals(2, preferences.size());

        preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat2", 1, 1, SortDirection.ASC));

        assertEquals(1, preferences.size());
        assertEquals("name4", preferences.get(0));

        preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat2", 1, 1, SortDirection.DESC));

        assertEquals(1, preferences.size());
        assertEquals("name3", preferences.get(0));

        preferences = CloseableIteratorUtils.toList(dao.getPreferencesIterator("cat1", 1, null, SortDirection.DESC));
        assertEquals(1, preferences.size());
    }

    @Test
    public void testGetCategoryIterator()
    throws CloseableIteratorException
    {
        UserPreferencesDAO dao = createDAO();

        Set<String> categories = new HashSet<>(CloseableIteratorUtils.toList(dao.getCategoryIterator()));

        assertEquals(0, categories.size());

        dao.addPreferences("cat1", "name1");
        dao.addPreferences("cat2", "name1");
        dao.addPreferences("cat3", "name1");

        categories = new HashSet<>(CloseableIteratorUtils.toList(dao.getCategoryIterator()));

        assertEquals(3, categories.size());
        assertTrue(categories.contains("cat1"));
        assertTrue(categories.contains("cat2"));
        assertTrue(categories.contains("cat3"));
    }

    @Test
    public void testDeletePreferences()
    {
        UserPreferencesDAO dao = createDAO();

        dao.addPreferences("cat1", "name1");
        dao.addPreferences("cat1", "name2");

        assertNotNull(dao.getPreferences("cat1", "name1"));
        assertNotNull(dao.getPreferences("cat1", "name2"));

        dao.delete(dao.getPreferences("cat1", "name1"));

        assertNull(dao.getPreferences("cat1", "name1"));
        assertNotNull(dao.getPreferences("cat1", "name2"));

        dao.delete(dao.getPreferences("cat1", "name2"));

        assertNull(dao.getPreferences("cat1", "name1"));
        assertNull(dao.getPreferences("cat1", "name2"));
    }

    @Test
    public void testIsInherited()
    {
        UserPreferencesDAO dao = createDAO();

        UserPreferencesEntity entity1 = dao.addPreferences("cat1", "name1");
        UserPreferencesEntity entity2 = dao.addPreferences("cat1", "name2");

        assertFalse(dao.isInherited(entity1));
        assertFalse(dao.isInherited(entity2));

        entity1.getInheritedPreferences().add(new InheritedUserPreferences(entity2));

        InheritedUserPreferences inheritedUserPreferences = entity1.getInheritedPreferences().iterator().next();

        assertEquals("Inherited: cat1:name2", inheritedUserPreferences.toString());
        assertEquals(0, inheritedUserPreferences.getIndex());
        assertEquals(inheritedUserPreferences, entity1.getInheritedPreferences().iterator().next());

        assertFalse(dao.isInherited(entity1));
        assertTrue(dao.isInherited(entity2));
    }

    @Test
    public void testReferencingFromCertificatesCount()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        assertEquals(0, dao.referencingFromCertificatesCount(certStoreEntry1));

        UserPreferencesEntity entity1 = dao.addPreferences("cat1", "name1");

        entity1.getCertificates().add(certStoreEntry1);

        assertEquals(1, dao.referencingFromCertificatesCount(certStoreEntry1));

        certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/missingsmimekeyusage.cer"));

        X509CertStoreEntity certStoreEntry2 = certStoreDAO.addCertificate(certificate, null);
        entity1.getCertificates().add(certStoreEntry2);

        assertEquals(1, dao.referencingFromCertificatesCount(certStoreEntry1));
        assertEquals(1, dao.referencingFromCertificatesCount(certStoreEntry2));

        UserPreferencesEntity entity2 = dao.addPreferences("cat1", "name2");

        entity2.getCertificates().add(certStoreEntry1);

        assertEquals(2, dao.referencingFromCertificatesCount(certStoreEntry1));
        assertEquals(1, dao.referencingFromCertificatesCount(certStoreEntry2));
    }

    @Test
    public void testReferencingFromCertificates()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        List<UserPreferencesEntity> entities = dao.getReferencingFromCertificates(certStoreEntry1, null, null);

        assertEquals(0, entities.size());

        assertFalse(dao.isReferencing(certStoreEntry1));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.getCertificates().add(certStoreEntry1);

        entities = dao.getReferencingFromCertificates(certStoreEntry1, null, null);

        assertEquals(1, entities.size());

        assertTrue(dao.isReferencing(certStoreEntry1));

        entity = dao.addPreferences("cat1", "name2");

        entity.getCertificates().add(certStoreEntry1);

        entities = dao.getReferencingFromCertificates(certStoreEntry1, null, null);

        assertEquals(2, entities.size());

        entities = dao.getReferencingFromCertificates(certStoreEntry1, 1, 1);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromCertificates(certStoreEntry1, 1, Integer.MAX_VALUE);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromCertificates(certStoreEntry1, 1, 0);

        assertEquals(0, entities.size());

        entities = dao.getReferencingFromCertificates(certStoreEntry1, 1, null);
        assertEquals(1, entities.size());
    }

    @Test
    public void testReferencingFromNamedCertificatesCount()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        assertEquals(0, dao.referencingFromNamedCertificatesCount(certStoreEntry1));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.getNamedCertificates().add(new NamedCertificateHibernate("cert1", certStoreEntry1));

        assertEquals(1, dao.referencingFromNamedCertificatesCount(certStoreEntry1));

        entity = dao.addPreferences("cat1", "name2");

        entity.getNamedCertificates().add(new NamedCertificateHibernate("cert2", certStoreEntry1));

        assertEquals(2, dao.referencingFromNamedCertificatesCount(certStoreEntry1));
    }

    @Test
    public void testReferencingFromNamedCertificates()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        List<UserPreferencesEntity> entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, null, null);

        assertEquals(0, entities.size());

        assertFalse(dao.isReferencing(certStoreEntry1));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.getNamedCertificates().add(new NamedCertificateHibernate("cert1", certStoreEntry1));

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, null, null);

        assertEquals(1, entities.size());

        assertTrue(dao.isReferencing(certStoreEntry1));

        entity = dao.addPreferences("cat1", "name2");

        entity.getNamedCertificates().add(new NamedCertificateHibernate("cert2", certStoreEntry1));

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, null, null);

        assertEquals(2, entities.size());

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, 1, 1);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, 1, Integer.MAX_VALUE);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, 1, 0);

        assertEquals(0, entities.size());

        entities = dao.getReferencingFromNamedCertificates(certStoreEntry1, 1, null);
        assertEquals(1, entities.size());
    }

    @Test
    public void testReferencingFromKeyAndCertificateCount()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        assertEquals(0, dao.referencingFromKeyAndCertificateCount(certStoreEntry1));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.setKeyAndCertificate(certStoreEntry1);

        assertEquals(1, dao.referencingFromKeyAndCertificateCount(certStoreEntry1));

        entity = dao.addPreferences("cat1", "name2");

        entity.setKeyAndCertificate(certStoreEntry1);

        assertEquals(2, dao.referencingFromKeyAndCertificateCount(certStoreEntry1));
    }

    @Test
    public void testReferencingFromKeyAndCertificate()
    throws Exception
    {
        UserPreferencesDAO dao = createDAO();

        X509CertStoreDAO certStoreDAO = createX509CertStoreDAO();

        X509Certificate certificate = TestUtils.loadCertificate(new File(TEST_BASE,
                "certificates/valid_certificate_mitm_test_ca.cer"));

        X509CertStoreEntity certStoreEntry1 = certStoreDAO.addCertificate(certificate, null);

        List<UserPreferencesEntity> entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, null, null);

        assertEquals(0, entities.size());

        assertFalse(dao.isReferencing(certStoreEntry1));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.setKeyAndCertificate(certStoreEntry1);

        entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, null, null);

        assertEquals(1, entities.size());

        assertTrue(dao.isReferencing(certStoreEntry1));

        entity = dao.addPreferences("cat1", "name2");

        entity.setKeyAndCertificate(certStoreEntry1);

        entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, null, null);

        assertEquals(2, entities.size());

        entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, 1, Integer.MAX_VALUE);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, 1, 0);

        assertEquals(0, entities.size());

        entities = dao.getReferencingFromKeyAndCertificate(certStoreEntry1, 1, null);
        assertEquals(1, entities.size());
    }

    @Test
    public void testUserPreferencesCount()
    {
        UserPreferencesDAO dao = createDAO();

        assertEquals(0, dao.getUserPreferencesCount("cat1"));

        dao.addPreferences("catX", "name1");

        dao.addPreferences("cat1", "name1");

        assertEquals(1, dao.getUserPreferencesCount("cat1"));

        dao.addPreferences("cat1", "name2");

        assertEquals(2, dao.getUserPreferencesCount("cat1"));
    }

    @Test
    public void testReferencingFromNamedBlobsCount()
    {
        UserPreferencesDAO dao = createDAO();

        NamedBlobDAO namedBlobDAO = createNamedBlobDAO();

        NamedBlobEntity namedBlobEntity = new NamedBlobEntity("cat1", "name1");

        namedBlobDAO.persist(namedBlobEntity);

        assertEquals(0, dao.referencingFromNamedBlobsCount(namedBlobEntity));

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.getNamedBlobs().add(namedBlobEntity);

        assertEquals(1, dao.referencingFromNamedBlobsCount(namedBlobEntity));

        entity = dao.addPreferences("cat1", "name2");
        entity.getNamedBlobs().add(namedBlobEntity);

        assertEquals(2, dao.referencingFromNamedBlobsCount(namedBlobEntity));
    }

    @Test
    public void testReferencingFromNamedBlobs()
    {
        UserPreferencesDAO dao = createDAO();

        NamedBlobDAO namedBlobDAO = createNamedBlobDAO();

        NamedBlobEntity namedBlobEntity = new NamedBlobEntity("cat1", "name1");

        namedBlobDAO.persist(namedBlobEntity);

        List<UserPreferencesEntity> entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, null, null);

        assertEquals(0, entities.size());

        UserPreferencesEntity entity = dao.addPreferences("cat1", "name1");

        entity.getNamedBlobs().add(namedBlobEntity);

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, null, null);

        assertEquals(1, entities.size());

        entity = dao.addPreferences("cat1", "name2");
        entity.getNamedBlobs().add(namedBlobEntity);

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, null, null);

        assertEquals(2, entities.size());

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, 1, 1);

        assertEquals(1, entities.size());

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, 1, 0);

        assertEquals(0, entities.size());

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, 10, null);

        assertEquals(0, entities.size());

        entities = dao.getReferencingFromNamedBlobs(namedBlobEntity, 1, null);
        assertEquals(1, entities.size());
    }
}
