/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class PGPPartitionedFixupTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new PGPPartitionedFixup();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testPGPDesktopHTMLPartitioned()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-desktop-partitioned-HTML-decrypted.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP partitioned message was fixed-up"));

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertNotSame(fixedUpMessage, sourceMessage);

        assertEquals("multipart/alternative;\r\n\tboundary=\""
                     + "_000_5011D045B6C5A947B21BAF12DDBF724464B33B3996WINUQ50EVO2Q9_\"",
                fixedUpMessage.getContentType());

        Multipart mp = (Multipart) fixedUpMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("text/plain; charset=ISO-8859-1", bp.getContentType());
        assertEquals("rrrr", ((String)bp.getContent()).trim());

        bp = mp.getBodyPart(1);

        assertEquals("text/html; charset=us-ascii", bp.getContentType());
        assertThat(((String)bp.getContent()), containsString("<p class=MsoNormal>rrrr<o:p></o:p></p>"));
    }

    @Test
    public void testPGPUniversalHTMLPartitionedWithAttachments()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/pgp-universal-html-partitioned-with-attachments-decrypted.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP partitioned message was fixed-up"));

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertEquals("multipart/mixed;\r\n boundary=\"------------050102060901020803040602\"",
                fixedUpMessage.getContentType());

        Multipart outerMP = (Multipart) fixedUpMessage.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart bp = outerMP.getBodyPart(0);

        assertTrue(bp.isMimeType("multipart/alternative"));

        Multipart innerMP = (Multipart) bp.getContent();

        assertEquals(2, innerMP.getCount());

        bp = innerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", bp.getContentType());
        assertEquals("_*test*_", ((String)bp.getContent()).trim());

        bp = innerMP.getBodyPart(1);

        assertEquals("text/html; charset=utf-8", bp.getContentType());
        assertThat(((String)bp.getContent()), containsString("<u><b>test</b></u>"));

        bp = outerMP.getBodyPart(1);

        assertEquals("application/octet-stream; name=Ciphermail_logo_test.pdf", bp.getContentType());
        assertEquals("attachment; filename=Ciphermail_logo_test.pdf",
                StringUtils.join(bp.getHeader("Content-Disposition")));
    }

    @Test
    public void testPGPUniversalHTMLPartitioned()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/pgp-universal-html-partitioned-decrypted.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP partitioned message was fixed-up"));

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertTrue(fixedUpMessage.isMimeType("multipart/alternative"));

        Multipart outerMP = (Multipart) fixedUpMessage.getContent();

        assertEquals(2, outerMP.getCount());

        BodyPart bp = outerMP.getBodyPart(0);

        assertEquals("text/plain; charset=UTF-8", bp.getContentType());
        assertEquals("_*test*_", ((String)bp.getContent()).trim());

        bp = outerMP.getBodyPart(1);

        assertEquals("text/html; charset=utf-8", bp.getContentType());
        assertThat(((String)bp.getContent()), containsString("<u><b>test</b></u>"));
    }

    @Test
    public void testPGPDesktopNotDecrypted()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/html-symantec-encryption-desktop.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertSame(fixedUpMessage, sourceMessage);
    }

    @Test
    public void testPGPDesktopUnicodeFilename()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-desktop-partitioned-HTML-with-" +
                "inline-image-and-attachment-with-unicode-filename-decrypted.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "PGP partitioned message was fixed-up"));

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertEquals("multipart/mixed;\r\n\tboundary=\"_007_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50EVO2Q9_\"",
                fixedUpMessage.getContentType());

        Multipart mp = (Multipart) fixedUpMessage.getContent();

        assertEquals(2, mp.getCount());

        BodyPart bp = mp.getBodyPart(0);

        assertEquals("multipart/related;\r\n\tboundary=\"_006_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50EVO2Q9_"
                     + "\";\r\n\ttype=\"multipart/alternative\"", bp.getContentType());

        Multipart inner = (Multipart) bp.getContent();

        assertEquals(2, inner.getCount());

        bp = inner.getBodyPart(0);

        assertEquals("multipart/alternative;\r\n\tboundary=\"_000_5011D045B6C5A947B21BAF12DDBF724464B3BD60A1WINUQ50"
                     + "EVO2Q9_\"", bp.getContentType());

        Multipart alternative = (Multipart) bp.getContent();

        assertEquals(2, alternative.getCount());

        bp = alternative.getBodyPart(0);

        assertEquals("text/plain; charset=iso-8859-1", bp.getContentType());
        assertEquals("CipherMail Logo", ((String)bp.getContent()).trim());

        bp = alternative.getBodyPart(1);

        assertEquals("text/html; charset=us-ascii", bp.getContentType());
        assertThat(((String)bp.getContent()), containsString("<span style='color:#92D050'>CipherMail</span>"));

        bp = inner.getBodyPart(1);
        assertEquals("image/png; name=image001.png", bp.getContentType());
        assertEquals("<image001.png@01D0663D.64D84E40>", StringUtils.join(bp.getHeader("Content-ID")));

        bp = mp.getBodyPart(1);

        assertEquals("application/octet-stream; \r\n\tname=\"=?UTF-8?B?w6TDtsO8IMOEw5bDnC56aXA=?=\"",
                bp.getContentType());
        assertEquals("=?UTF-8?B?w6TDtsO8IMOEw5bDnC56aXA=?=", bp.getFileName());
    }

    @Test
    public void testMaxMimeDepth()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("maxMimeDepth", "1")
                .build();

        MimeMessage sourceMessage = TestUtils.loadTestMessage(
                "mail/PGP-desktop-partitioned-HTML-decrypted.eml");

        sourceMessage.setFrom(new InternetAddress("test@example.com"));

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .recipients("martijn@djigzo.com")
                .sender("test@example.com")
                .mimeMessage(sourceMessage)
                .build();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage fixedUpMessage = mail.getMessage();

        MailUtils.validateMessage(fixedUpMessage);

        assertSame(fixedUpMessage, sourceMessage);
    }
}
