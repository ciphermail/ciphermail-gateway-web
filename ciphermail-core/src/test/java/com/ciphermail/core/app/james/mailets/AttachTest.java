/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.Multipart;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class AttachTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new Attach();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    /*
     * Tests with a message which is not a multipart
     */
    @Test
    public void testSinglePart()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/signed-opaque-validcertificate.eml"))
                .build();

        assertFalse(mail.getMessage().isMimeType("multipart/mixed"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(1, mp.getCount());
   }

   /*
    * Tests whether an existing multipart message is supported
    */
    @Test
    public void testMultipart()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/clear-signed-validcertificate.eml"))
                .build();

        assertTrue(mail.getMessage().isMimeType("multipart/signed"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(1, mp.getCount());
    }

    /*
     * Tests whether the filename of the attached part can be configured
     */
    @Test
    public void testSinglePartWithFilename()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("filename", "test.txt")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/test-5K.eml"))
                .build();

        assertFalse(mail.getMessage().isMimeType("multipart/*"));

        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", mail.getMessage().getMessageID());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(1, mp.getCount());

        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", mail.getMessage().getMessageID());

        assertEquals("test.txt", mail.getMessage().getFileName());
    }

    /*
     * Tests whether the messageID is not retained when retainMessageID is set to false
     */
    @Test
    public void testDoNotRetainMessageID()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("retainMessageID", "false")
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("test@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/test-5K.eml"))
                .build();

        assertFalse(mail.getMessage().isMimeType("multipart/*"));

        assertEquals("<6d5a.0003.fffffffd@martijn-desktop>", mail.getMessage().getMessageID());

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertTrue(mail.getMessage().isMimeType("multipart/mixed"));

        Multipart mp = (Multipart) mail.getMessage().getContent();

        assertEquals(1, mp.getCount());

        assertNotEquals("<6d5a.0003.fffffffd@martijn-desktop>", mail.getMessage().getMessageID());
    }
}
