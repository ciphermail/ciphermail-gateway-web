/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.UnhandledException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class KeyAndCertificateWorkflowImplTest
{
    private static final Logger logger = LoggerFactory.getLogger(KeyAndCertificateWorkflowImplTest.class);

    private static final File TEST_BASE_DIR = TestUtils.getTestDataDir();

    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERTIFICATE_WORKFLOW_SERVICE_NAME)
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    @Qualifier(CipherMailSystemServices.KEY_AND_ROOT_CERT_STORE_SERVICE_NAME)
    private KeyAndCertStore keyAndRootStore;

    @Autowired
    private CertificatePathBuilderFactory certificatePathBuilderFactory;

    @Autowired
    private SessionManager sessionManager;

    private KeyStore keyStore;

    @Before
    public void setup()
    throws Exception
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Clean key/root store
                keyAndCertStore.removeAllEntries();
                keyAndRootStore.removeAllEntries();
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

      keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

      keyStore.load(new FileInputStream(new File(TEST_BASE_DIR, "keys/testCertificates.p12")),
              "test".toCharArray());
    }

    private Collection<X509Certificate> getAllCerts(X509CertStoreExt store)
    {
        return transactionOperations.execute(status ->
        {
            try {
                return store.getCertificates(null);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addCertificates(X509CertStoreExt store, File certificateFile)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                for (Certificate certificate : CertificateUtils.readCertificates(certificateFile)) {
                    store.addCertificate((X509Certificate) certificate);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testImportKeyStore()
    throws KeyStoreException
    {
        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());
    }

    @Test
    public void testImportKeyStoreSkipMissingKey()
    throws KeyStoreException
    {
        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());
    }

    @Test
    public void testImportKeyStoreRepeat()
    throws KeyStoreException
    {
        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());

        imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

        // everything was already imported but the already imported entries are returned as well
        assertEquals(22, imported.size());
    }

    /*
     * Test if a PFX can be retrieved when a chain cannot be build
     */
    @Test
    public void testCopyKeysToPKCS12OutputstreamTransactedNoChain()
    throws Exception
    {
        char[] password = "test".toCharArray();

        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());

        Collection<X509Certificate> certificates = getAllCerts(keyAndCertStore);

        assertEquals(22, certificates.size());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        keyAndCertificateWorkflow.copyKeysToPKCS12OutputstreamTransacted(certificates, password, true, bos);
    }

    @Test
    public void testCopyKeysToPKCS12OutputstreamTransacted()
    throws Exception
    {
        addCertificates(keyAndRootStore, new File(TEST_BASE_DIR, "certificates/mitm-test-root.cer"));

        char[] password = "test".toCharArray();

        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());

        Collection<X509Certificate> certificates = getAllCerts(keyAndCertStore);

        assertEquals(22, certificates.size());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        keyAndCertificateWorkflow.copyKeysToPKCS12OutputstreamTransacted(certificates, password, true, bos);

        KeyStore localKeyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

        localKeyStore.load(new ByteArrayInputStream(bos.toByteArray()), "test".toCharArray());

        assertEquals(22, localKeyStore.size());

        KeyStore.PasswordProtection passwd = new KeyStore.PasswordProtection(password);

        for (X509Certificate certificate : certificates)
        {
            String alias = X509CertificateInspector.getThumbprint(certificate);

            if (localKeyStore.isKeyEntry(alias))
            {
                KeyStore.Entry entry = localKeyStore.getEntry(alias, passwd);

                assertTrue(entry instanceof PrivateKeyEntry);

                PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) entry;

                assertNotNull(privateKeyEntry.getPrivateKey());
                assertNotNull(privateKeyEntry.getCertificate());
                assertEquals(certificate, privateKeyEntry.getCertificate());
            }
        }

        Enumeration<String> aliases = localKeyStore.aliases();

        int keyCount = 0;
        int certCount = 0;

        while (aliases.hasMoreElements())
        {
            String alias = aliases.nextElement();

            if (localKeyStore.isKeyEntry(alias))
            {
                keyCount++;

                PrivateKeyEntry entry = (PrivateKeyEntry) localKeyStore.getEntry(alias, passwd);

                Certificate[] chain = entry.getCertificateChain();

                assertEquals(3, chain.length);
            }
            else {
                certCount++;
            }
        }

        assertEquals(20, keyCount);
        assertEquals(2, certCount);
    }

    @Test
    public void testCopyKeysToPEMOutputstreamTransacted()
    throws Exception
    {
        addCertificates(keyAndRootStore, new File(TEST_BASE_DIR, "certificates/mitm-test-root.cer"));

        char[] password = "test".toCharArray();

        List<X509CertStoreEntry> imported = keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY);

        assertEquals(22, imported.size());

        Collection<X509Certificate> certificates = getAllCerts(keyAndCertStore);

        assertEquals(22, certificates.size());

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        keyAndCertificateWorkflow.copyKeysToPEMOutputstreamTransacted(certificates, password, true, bos);

        String pem = MiscStringUtils.toStringFromUTF8Bytes(bos.toByteArray());

        assertNotNull(pem);

        System.out.println(pem);

        PEMParser parser = new PEMParser(new StringReader(pem));

        int i = 0;

        try {
            Object object = null;

            while ((object = parser.readObject()) != null)
            {
                i++;

                // There should only be X509CertificateHolder's and PEMEncryptedKeyPair's
                if (! (object instanceof X509CertificateHolder || object instanceof PEMEncryptedKeyPair)) {
                    fail("Unexpected object " + object.getClass());
                }
            }
        }
        finally {
            parser.close();
        }

        // All keys and certificates are separate entries. Because the intermediate and root are added separately,
        // it's 20 (keys) * 2 (certs) + 2 (root/intermediate) = 42
        assertEquals(42, i);
    }

    @Test
    public void testImportKeyStoreMultipleThreads()
    throws Exception
    {
        // Because this test will result in a lot of database "problems" because we update/clear the database at the
        // same time from multiple threads, we need a max larger number of retries
        KeyAndCertificateWorkflowImpl localKeyAndCertificateWorkflow = new KeyAndCertificateWorkflowImpl(
                keyAndCertStore, certificatePathBuilderFactory, sessionManager,
                transactionOperations)
        {
            @Override
            protected RetryTemplate createRetryTemplate()
            {
                return RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder(null)
                        .maxAttempts(100)
                        .withListener(new LoggingRetryListener(logger))
                        .build();
            }
        };

        Collection<Future<?>> futures = new LinkedList<>();

        int repeat = 100;
        AtomicInteger counter = new AtomicInteger();

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        try {
            for (int i = 0; i < repeat; i++)
            {
                final int c = i;

                Callable<Null> callable = new Callable<Null>()
                {
                    @Override
                    public Null call()
                    throws Exception
                    {
                        if (c % 10 == 0)
                        {
                            transactionOperations.executeWithoutResult(status ->
                            {
                                try {
                                    // Clean key store
                                    keyAndCertStore.removeAllEntries();
                                }
                                catch (Exception e) {
                                    // ignore
                                }
                            });
                        }

                        localKeyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                                MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

                        counter.incrementAndGet();

                        return null;
                    }
                };

                futures.add(executorService.submit(callable));
            }

            // Wait for results
            for (Future<?> future : futures) {
                future.get();
            }
        }
        finally {
            executorService.shutdown();
        }

        assertEquals(repeat, counter.intValue());
    }
}
