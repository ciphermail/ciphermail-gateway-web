/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.Inherited;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.logging.log4j.LogManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.AddressException;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserHibernateTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    @Before
    public void setup()
    {
        memoryLogAppender.reset();

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                // Reset global signing certificate
                globalPreferencesManager.getGlobalUserPreferences().setKeyAndCertificate(null);

                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                addUser("test@example.com");
                addUser("m.brinkers@pobox.com");

                SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

                KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

                keyStore.load(new FileInputStream(new File("src/test/resources/testdata/keys/testCertificates.p12")),
                        "test".toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore,
                        MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

                assertEquals(22, keyAndCertStore.size());

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                        new File("src/test/resources/testdata/certificates/mitm-test-root.cer"));

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        rootStore.addCertificate((X509Certificate) certificate);
                    }
                }
                assertEquals(1, rootStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void addUser(final String email)
    throws AddressException, HierarchicalPropertiesException
    {
        User user = userWorkflow.addUser(email);

        assertEquals(email, user.toString());

        userWorkflow.makePersistent(user);
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @Test
    public void testGetSigningKeyAndCertificate()
    {
        String email = "test@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNotNull(keyAndCertificate);
                assertNotNull(keyAndCertificate.getCertificate());
                assertNotNull(keyAndCertificate.getPrivateKey());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSigningKeyAndCertificateUnknownUser()
    {
        String email = "unknownuser@example.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNull(keyAndCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAlwaysUseFreshestSigningCert()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(
                        keyAndCertStore.getByThumbprint(
                        "E37636F264F55EB141B5E3524F66D9E5EFCAC34D1FF85D97ACEA71673E99E3B122" +
                        "94BD0B23B18C2D03FDAA75D3DB9A70700B73B72C557EB7F91D98B62CCB9DC4"));

                assertNotNull(keyAndCertificate);

                User user = userWorkflow.getUser("test@example.com", UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                // disable AlwaysUseFreshestSigningCert
                createSMIMEProperties(user.getUserPreferences().getProperties())
                        .setAlwaysUseFreshestSigningCert(false);

                user.setSigningKeyAndCertificate(keyAndCertificate);

                assertEquals(keyAndCertificate, user.getSigningKeyAndCertificate());

                // enable AlwaysUseFreshestSigningCert and try again
                createSMIMEProperties(user.getUserPreferences().getProperties())
                        .setAlwaysUseFreshestSigningCert(true);

                KeyAndCertificate freshestSigningCert = user.getSigningKeyAndCertificate();

                assertNotEquals(keyAndCertificate, freshestSigningCert);

                // the cert which should be selected is from not after date Nov 21 12:54:35 CET 2027
                assertEquals("7D617F44AFF47CD2390507C98BB28283174B30CAAAF0E8A8E16615786CB82" +
                             "1F42A77A689E28B2DA1623B47379E5EC9D5B729B868D8BF6B564FE6885EF162A5F5",
                        X509CertificateInspector.getThumbprint(freshestSigningCert.getCertificate()));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSetSigningKeyAndCertificate()
    {
        String email = "m.brinkers@pobox.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // There should be no signing certificate set for m.brinkers@pobox.com
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNull(keyAndCertificate);

                // Load the signing certificate from another user so we can use that to set if for m.brinkers@pobox.com
                keyAndCertificate = userWorkflow.getUser("test@example.com",
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNotNull(keyAndCertificate);

                // this test requires that AlwaysUseFreshestSigningCert is disabled
                createSMIMEProperties(userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getUserPreferences().getProperties())
                        .setAlwaysUseFreshestSigningCert(false);

                userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).
                    setSigningKeyAndCertificate(keyAndCertificate);
            }
            catch (HierarchicalPropertiesException | CertStoreException | KeyStoreException | AddressException e) {
                throw new UnhandledException(e);
            }
        });

        // Start a new transaction and check whether it was persisted
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNotNull(keyAndCertificate);
                assertFalse(keyAndCertificate instanceof Inherited);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testInheritSigningKeyAndCertificate()
    {
        String email = "m.brinkers@pobox.com";

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // There should be no signing certificate set for m.brinkers@pobox.com
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNull(keyAndCertificate);

                // Load the signing certificate from another user so we can use that to set if for m.brinkers@pobox.com
                keyAndCertificate = userWorkflow.getUser("test@example.com",
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNotNull(keyAndCertificate);

                // Configure the signing certificate for the global settings
                globalPreferencesManager.getGlobalUserPreferences().setKeyAndCertificate(keyAndCertificate);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Start a new transaction and check whether it was persisted
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                KeyAndCertificate keyAndCertificate = userWorkflow.getUser(email,
                        UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST).getSigningKeyAndCertificate();

                assertNotNull(keyAndCertificate);

                // Should be inherited
                assertTrue(keyAndCertificate instanceof Inherited);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddNewCertificateNotYetInStore()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser("test@example.com", UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                X509Certificate certificate = TestUtils.loadCertificate(new File(TestUtils.getTestDataDir(),
                        "certificates/Martijn_Brinkers_Comodo_Class_Security_Services_CA.pem.cer"));

                user.getUserPreferences().getCertificates().add(certificate);

                assertThat(memoryLogAppender.getLogOutput(), containsString(
                        "Certificate entry not found. Adding it the the certStore."));
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });

        // Start a new transaction and check whether it was persisted
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser("test@example.com", UserWorkflow.UserNotExistResult.NULL_IF_NOT_EXIST);

                assertEquals(1, user.getUserPreferences().getCertificates().size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }
}
