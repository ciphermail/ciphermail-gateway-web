/*
 * Copyright (c) 2011-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PortalSignupDetails;
import com.ciphermail.core.app.james.PortalSignups;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.ThreadUtils;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.TestUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class CreatePortalSignupTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void setClientSecret(String email, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userWorkflow.makePersistent(userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST))
                        .getUserPreferences().getProperties().setClientSecret(value);
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new CreatePortalSignup();

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testCreateInvitation()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .state("somestate")
                .build();

        Mailet mailet = createMailet(mailetConfig);

        setClientSecret("recipient@example.com", "not-a-real-secret");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals("somestate", mail.getState());
        assertEquals(1, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("recipient@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());

        PortalSignups invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNotNull(invitations);
        assertEquals(1, invitations.size());

        PortalSignupDetails invitation = invitations.get("RECIPIENT@example.com");

        assertNotNull(invitation);
        assertNotNull(invitation.getMac());
        assertEquals(32, Base32Utils.base32Decode(invitation.getMac()).length);
    }

    @Test
    public void testCreateInvitationMultipleRecipients()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("r1@example.com", "r2@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        setClientSecret("r1@example.com", "not-a-real-secret-1");
        setClientSecret("r2@example.com", "not-a-real-secret-2");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        assertEquals(2, mail.getRecipients().size());
        assertTrue(mail.getRecipients().contains(new MailAddress("r1@example.com")));
        assertTrue(mail.getRecipients().contains(new MailAddress("r2@example.com")));
        assertEquals(new MailAddress("sender@example.com"), mail.getSender());

        PortalSignups invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNotNull(invitations);
        assertEquals(2, invitations.size());

        PortalSignupDetails invitation = invitations.get("r1@example.com");

        assertNotNull(invitation);
        assertNotNull(invitation.getMac());
        assertEquals(32, Base32Utils.base32Decode(invitation.getMac()).length);

        invitation = invitations.get("r2@example.com");

        assertNotNull(invitation);
        assertNotNull(invitation.getMac());
        assertEquals(32, Base32Utils.base32Decode(invitation.getMac()).length);
    }

    @Test
    public void testCreateInvitationUniqueTimestamp()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        Mailet mailet = createMailet(mailetConfig);

        setClientSecret("recipient@example.com", "not-a-real-secret");

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MailUtils.validateMessage(mail.getMessage());

        PortalSignups invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNotNull(invitations);
        assertEquals(1, invitations.size());

        PortalSignupDetails invitation = invitations.get("recipient@example.com");

        assertNotNull(invitation);
        assertNotNull(invitation.getMac());
        String mac1 = invitation.getMac();
        assertEquals(32, Base32Utils.base32Decode(mac1).length);

        ThreadUtils.sleepQuietly(10);

        mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        mailet = createMailet(mailetConfig);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNotNull(invitations);
        assertEquals(1, invitations.size());

        invitation = invitations.get("recipient@example.com");

        assertNotNull(invitation);
        assertNotNull(invitation.getMac());
        String mac2 = invitation.getMac();
        assertEquals(32, Base32Utils.base32Decode(mac2).length);

        assertNotEquals(mac1, mac2);
    }

    @Test
    public void testCreateInvitationRetry()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        MutableBoolean constraintViolationThrown = new MutableBoolean();

        CreatePortalSignup mailet = new CreatePortalSignup()
        {
            @Override
            protected void onHandleUserEvent(Mail mail, User user)
            throws MessagingException
            {
                super.onHandleUserEvent(mail, user);

                /*
                 * throw ConstraintViolationException for first time a user is handled
                 */
                if (!constraintViolationThrown.booleanValue())
                {
                    ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                            ActivationContext.class);

                    activationContext.getInvitations().put("dummy@example.com", new PortalSignupDetails(
                            "dummy@example.com", 0,"dummy"));

                    constraintViolationThrown.setValue(true);

                    throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                            "fake constraint");
                }
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        assertFalse(constraintViolationThrown.booleanValue());

        setClientSecret("recipient@example.com", "not-a-real-secret");

        mailet.service(mail);

        assertTrue(constraintViolationThrown.booleanValue());

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        PortalSignups invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNotNull(invitations);
        assertEquals(1, invitations.size());
    }

    @Test
    public void testCreateInvitationRetryExhaust()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .build();

        FakeMail mail = FakeMail.builder().name(MailImpl.getId())
                .sender("sender@example.com")
                .recipients("recipient@example.com")
                .mimeMessage(TestUtils.loadTestMessage("mail/simple-text-message.eml"))
                .build();

        CreatePortalSignup mailet = new CreatePortalSignup()
        {
            @Override
            protected void onHandleUserEvent(Mail mail, User user)
            throws MessagingException
            {
                super.onHandleUserEvent(mail, user);

                throw new ConstraintViolationException("Forced exception", new SQLException("Forced"),
                        "fake constraint");
            }
        };

        // auto wire the mailet
        applicationContext.getAutowireCapableBeanFactory().autowireBean(mailet);

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 1)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 2)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString("Some error occurred. Error Message: " +
                "Forced exception. Retrying (retry count: 3)"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "org.hibernate.exception.ConstraintViolationException: Forced exception"));

        PortalSignups invitations = CoreApplicationMailAttributes.getPortalSignups(mail);

        assertNull(invitations);
    }
}
