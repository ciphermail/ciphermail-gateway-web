/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserManager;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.AddressException;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserManagerHibernateTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private KeyAndCertificateWorkflow keyAndCertificateWorkflow;

    @Autowired
    @Qualifier(CipherMailSystemServices.ROOT_STORE_SERVICE_NAME)
    private X509CertStoreExt rootStore;

    @Autowired
    private KeyAndCertStore keyAndCertStore;

    @Autowired
    private UserManager userManager;

    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // Delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }

                keyAndCertStore.removeAllEntries();
                rootStore.removeAllEntries();

                SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

                KeyStore keyStore = securityFactory.createKeyStore("PKCS12");

                keyStore.load(new FileInputStream(new File("src/test/resources/testdata/keys/testCertificates.p12")),
                        "test".toCharArray());

                keyAndCertificateWorkflow.importKeyStoreTransacted(keyStore, MissingKeyAction.ADD_CERTIFICATE_ONLY_ENTRY);

                assertEquals(22, keyAndCertStore.size());

                Collection<? extends Certificate> certificates = CertificateUtils.readCertificates(
                        new File("src/test/resources/testdata/certificates/mitm-test-root.cer"));

                for (Certificate certificate : certificates)
                {
                    if (certificate instanceof X509Certificate) {
                        rootStore.addCertificate((X509Certificate) certificate);
                    }
                }
                assertEquals(1, rootStore.size());
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @Test
    public void testAddUserCaseInsensitive()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("tESt@exAMPle.com");

                assertNull(user);

                user = userManager.addUser("tESt@exAMPle.com", UserManager.AddMode.PERSISTENT);

                assertTrue(userManager.isPersistent(user));
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("Test@examplE.com");

                assertNotNull(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testEquals()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user1 = userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);
                User user2 = userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);

                assertEquals(user1, user1);
                assertEquals(user1.hashCode(), user1.hashCode());
                assertNotEquals(user1, user2);
                assertNotEquals(user1.hashCode(), user2.hashCode());
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAutoSelectEncryptionCertsOff()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.addUser("test@example.com", UserManager.AddMode.PERSISTENT);

                assertTrue(userManager.isPersistent(user));

                Set<X509Certificate> certificates = user.getAutoSelectEncryptionCertificates();

                assertEquals(0, certificates.size());

                createSMIMEProperties(user.getUserPreferences().getProperties())
                        .setAutoSelectEncryptionCerts(true);

                certificates = user.getAutoSelectEncryptionCertificates();

                assertEquals(13, certificates.size());
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("Test@examplE.com");

                assertNotNull(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddInvalidEmail()
    {
        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    userManager.addUser("xxx", UserManager.AddMode.PERSISTENT);
                }
                catch (AddressException e) {
                    throw new UnhandledException(e);
                }
            });

            fail();
        }
        catch(UnhandledException e) {
            assertTrue(e.getCause() instanceof AddressException);
        }
    }

    @Test
    public void testGetInvalidEmail()
    {
        AddressException e = assertThrows(AddressException.class, () -> userManager.getUser("##"));

        assertEquals("## is not a valid email address", e.getMessage());

        e = assertThrows(AddressException.class, () -> userManager.getUser(null));

        assertEquals("null is not a valid email address", e.getMessage());
    }

    @Test
    public void testAddUserNonPersistent()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

                assertFalse(userManager.isPersistent(user));
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("test@example.com");

                assertNull(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddUserNonPersistentMakePersistent()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

                assertFalse(userManager.isPersistent(user));

                userManager.makePersistent(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("test@example.com");

                assertNotNull(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testMakePersistentTwice()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.addUser("test@example.com", UserManager.AddMode.NON_PERSISTENT);

                assertFalse(userManager.isPersistent(user));

                userManager.makePersistent(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("test@example.com");

                assertNotNull(user);

                /*
                 * Should not fail
                 */
                userManager.makePersistent(user);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testAddProperty()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.addUser("test@example.com", UserManager.AddMode.PERSISTENT);

                user.getUserPreferences().getProperties().setProperty("test", "123");
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userManager.getUser("test@example.com");

                assertNotNull(user);

                assertEquals("123", user.getUserPreferences().getProperties().getProperty("test"));
            }
            catch (AddressException | HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetEmailIterator()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<String> it = userManager.getEmailIterator();

                List<String> emails = CloseableIteratorUtils.toList(it);

                assertEquals(4, emails.size());

                assertEquals("test1@example.com", emails.get(0));
                assertEquals("test1@xxx.com", emails.get(1));
                assertEquals("test2@example.com", emails.get(2));
                assertEquals("test2@xxx.com", emails.get(3));

                it = userManager.getEmailIterator(0, 2, SortDirection.ASC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(2, emails.size());

                assertEquals("test1@example.com", emails.get(0));
                assertEquals("test1@xxx.com", emails.get(1));

                it = userManager.getEmailIterator(0, 2, SortDirection.DESC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(2, emails.size());

                assertEquals("test2@xxx.com", emails.get(0));
                assertEquals("test2@example.com", emails.get(1));
            }
            catch (CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testSearchEmail()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<String> it = userManager.searchEmail("%%", 0, 100, SortDirection.ASC);

                List<String> emails = CloseableIteratorUtils.toList(it);

                assertEquals(4, emails.size());

                assertEquals("test1@example.com", emails.get(0));
                assertEquals("test1@xxx.com", emails.get(1));
                assertEquals("test2@example.com", emails.get(2));
                assertEquals("test2@xxx.com", emails.get(3));

                it = userManager.searchEmail("%%", 0, 100, SortDirection.DESC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(4, emails.size());

                assertEquals("test2@xxx.com", emails.get(0));
                assertEquals("test2@example.com", emails.get(1));
                assertEquals("test1@xxx.com", emails.get(2));
                assertEquals("test1@example.com", emails.get(3));

                it = userManager.searchEmail("%%", 1, 2, SortDirection.DESC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(2, emails.size());

                assertEquals("test2@example.com", emails.get(0));
                assertEquals("test1@xxx.com", emails.get(1));

                it = userManager.searchEmail("%xxx%", 0, 100, SortDirection.ASC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(2, emails.size());

                assertEquals("test1@xxx.com", emails.get(0));
                assertEquals("test2@xxx.com", emails.get(1));

                it = userManager.searchEmail("non-existing", 0, 100, SortDirection.ASC);

                emails = CloseableIteratorUtils.toList(it);

                assertEquals(0, emails.size());
            }
            catch (CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testGetSearchEmailCount()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userManager.addUser("test2@example.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test2@xxx.com", UserManager.AddMode.PERSISTENT);
                userManager.addUser("test1@xxx.com", UserManager.AddMode.PERSISTENT);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            long count = userManager.getSearchEmailCount("%%");

            assertEquals(4, count);

            count = userManager.getSearchEmailCount("%xxx%");

            assertEquals(2, count);

            count = userManager.getSearchEmailCount("non-existing");

            assertEquals(0, count);
        });
    }

    @Test
    public void testSearchEmailWithUppercase()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                userManager.addUser("test1@example.com", UserManager.AddMode.PERSISTENT);
            }
            catch (AddressException e) {
                throw new UnhandledException(e);
            }
        });
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                CloseableIterator<String> it = userManager.searchEmail("%Test%", 0, 100, SortDirection.ASC);

                List<String> emails = CloseableIteratorUtils.toList(it);

                assertEquals(1, emails.size());

                assertEquals(1, userManager.getSearchEmailCount("%Test%"));
            }
            catch (CloseableIteratorException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
