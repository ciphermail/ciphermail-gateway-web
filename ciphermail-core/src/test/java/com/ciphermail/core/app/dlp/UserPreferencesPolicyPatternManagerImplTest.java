/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.app.workflow.UserWorkflow.UserNotExistResult;
import com.ciphermail.core.common.dlp.impl.PolicyPatternImpl;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.UnhandledException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.AddressException;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class UserPreferencesPolicyPatternManagerImplTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private PolicyPatternManager policyPatternManager;

    @Autowired
    private UserPreferencesPolicyPatternManager userPreferencesPolicyPatternManager;

    @Autowired
    private GlobalPreferencesManager globalPreferencesManager;

    @Autowired
    private DomainManager domainManager;

    @Autowired
    private UserWorkflow userWorkflow;

    /*
     * We only want to initialize the database once. However we cannot do this in setUpBeforeClass because we
     * need instances injected by Spring. This must be static because of the way Junit works
     */
    private static boolean setUpIsDone = false;

    @Before
    public void setup()
    {
        if (setUpIsDone) {
            return;
        }

        setUpIsDone = true;

        transactionOperations.executeWithoutResult(status ->
        {
            try {
                PolicyPatternNode node1 = policyPatternManager.createPattern("pattern1");
                node1.setPolicyPattern(new PolicyPatternImpl("pattern1", Pattern.compile(".*")));

                PolicyPatternNode node2 = policyPatternManager.createPattern("pattern2");
                node2.setPolicyPattern(new PolicyPatternImpl("pattern2", Pattern.compile("123")));

                node1.getChilds().add(node2);

                PolicyPatternNode node3 = policyPatternManager.createPattern("pattern3");
                node3.setPolicyPattern(new PolicyPatternImpl("pattern3", Pattern.compile("abc.*")));

                PolicyPatternNode node4 = policyPatternManager.createPattern("pattern4");
                node4.setPolicyPattern(new PolicyPatternImpl("pattern4", Pattern.compile("\\d")));

                PolicyPatternNode node5 = policyPatternManager.createPattern("pattern5");
                node5.setPolicyPattern(new PolicyPatternImpl("pattern5", Pattern.compile("5")));

                userPreferencesPolicyPatternManager.setPatterns(globalPreferencesManager.getGlobalUserPreferences(),
                        Collections.singleton(node1));

                Set<PolicyPatternNode> globalPatterns = userPreferencesPolicyPatternManager.getPatterns(
                        globalPreferencesManager.getGlobalUserPreferences());

                globalPatterns.add(node3);

                userPreferencesPolicyPatternManager.setPatterns(globalPreferencesManager.getGlobalUserPreferences(),
                        globalPatterns);

                UserPreferences domainPrefs = domainManager.addDomain("example1.com");

                userPreferencesPolicyPatternManager.setPatterns(domainPrefs, Collections.singleton(node4));

                domainPrefs = domainManager.addDomain("*.sub.example.com");

                userPreferencesPolicyPatternManager.setPatterns(domainPrefs, Collections.singleton(node5));

                domainManager.addDomain("example2.com");

                User user = userWorkflow.addUser("user@example1.com");

                userWorkflow.makePersistent(user);

                user = userWorkflow.addUser("user@example2.com");

                userWorkflow.makePersistent(user);

                user = userWorkflow.addUser("user@example3.com");

                userWorkflow.makePersistent(user);

                user = userWorkflow.addUser("user@example4.com");

                userWorkflow.makePersistent(user);

                userPreferencesPolicyPatternManager.setPatterns(user.getUserPreferences(),
                        Collections.singleton(node5));

                user = userWorkflow.addUser("user@sub.example.com");

                userWorkflow.makePersistent(user);

                user = userWorkflow.addUser("user@sub.sub.example.com");

                userWorkflow.makePersistent(user);
            }
            catch (HierarchicalPropertiesException | CloseableIteratorException | AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private boolean containsPolicyPatternNode(Collection<PolicyPatternNode> patterns, final String name) {
        return CollectionUtils.exists(patterns, obj -> name.equals(((PolicyPatternNode) obj).getName()));
    }

    private PolicyPatternNode getPolicyPatternNode(Collection<PolicyPatternNode> patterns, final String name)
    {
        return (PolicyPatternNode) CollectionUtils.find(patterns,
                obj -> name.equals(((PolicyPatternNode) obj).getName()));
    }

    private boolean isInherited(Collection<PolicyPatternNode> patterns)
    {
        for (PolicyPatternNode pattern : patterns)
        {
            if (!(pattern instanceof UserPolicyPatternNode)) {
                fail("Should be a UserPolicyPatternNode");
            }

            if (!((UserPolicyPatternNode)pattern).isInherited()) {
                return false;
            }
        }

        return true;
    }

    @Test
    public void testGlobalSettings()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                Set<PolicyPatternNode> patterns = userPreferencesPolicyPatternManager.getPatterns(
                        globalPreferencesManager.getGlobalUserPreferences());

                assertNotNull(patterns);
                assertEquals(2, patterns.size());

                assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
                assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
                assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

                assertFalse(isInherited(patterns));
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testDomainSettings()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            /*
             * example1.com has explicitly set policy patterns so not inherited from global
             */
            Set<PolicyPatternNode> patterns = userPreferencesPolicyPatternManager.getPatterns(
                    domainManager.getDomainPreferences("example1.com"));
            assertNotNull(patterns);
            assertEquals(1, patterns.size());
            assertFalse(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern4"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));


            /*
             * example2.com inherits from Global
             */
            patterns = userPreferencesPolicyPatternManager.getPatterns(
                    domainManager.getDomainPreferences("example2.com"));
            assertNotNull(patterns);
            assertEquals(2, patterns.size());
            assertTrue(isInherited(patterns));
            assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
            assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
            assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

            PolicyPatternNode node1 = getPolicyPatternNode(patterns, "pattern1");

            assertNotNull(node1);
            assertEquals(".*", node1.getPolicyPattern().getPattern().pattern());
        });
    }

    @Test
    public void testUserSettings()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                /*
                 * user@example1.com inherits from example1.com
                 */
                User user = userWorkflow.getUser("user@example1.com", UserNotExistResult.NULL_IF_NOT_EXIST);
                assertNotNull(user);

                Set<PolicyPatternNode> patterns = userPreferencesPolicyPatternManager.getPatterns(user.getUserPreferences());

                assertNotNull(patterns);
                assertEquals(1, patterns.size());
                assertTrue(isInherited(patterns));
                assertTrue(containsPolicyPatternNode(patterns, "pattern4"));
                assertFalse(containsPolicyPatternNode(patterns, "pattern2"));

                /*
                 * user@example2.com inherits from example2.com which inherits from global
                 */
                user = userWorkflow.getUser("user@example2.com", UserNotExistResult.NULL_IF_NOT_EXIST);
                assertNotNull(user);

                patterns = userPreferencesPolicyPatternManager.getPatterns(user.getUserPreferences());

                assertEquals(2, patterns.size());
                assertTrue(isInherited(patterns));
                assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
                assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
                assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

                /*
                 * user@example3.com inherits from global
                 */
                user = userWorkflow.getUser("user@example3.com", UserNotExistResult.NULL_IF_NOT_EXIST);
                assertNotNull(user);

                patterns = userPreferencesPolicyPatternManager.getPatterns(user.getUserPreferences());

                assertEquals(2, patterns.size());
                assertTrue(isInherited(patterns));
                assertTrue(containsPolicyPatternNode(patterns, "pattern1"));
                assertFalse(containsPolicyPatternNode(patterns, "pattern2"));
                assertTrue(containsPolicyPatternNode(patterns, "pattern3"));

                /*
                 * user@example4.com has explicitly set policy patterns
                 */
                user = userWorkflow.getUser("user@example4.com", UserNotExistResult.NULL_IF_NOT_EXIST);
                assertNotNull(user);

                patterns = userPreferencesPolicyPatternManager.getPatterns(user.getUserPreferences());

                assertEquals(1, patterns.size());
                assertFalse(isInherited(patterns));
                assertTrue(containsPolicyPatternNode(patterns, "pattern5"));
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Test
    public void testWildcard()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                /*
                 * user@sub.example.com inherits from *.sub.example.com
                 */
                User user = userWorkflow.getUser("user@sub.example.com", UserNotExistResult.NULL_IF_NOT_EXIST);
                assertNotNull(user);

                Set<PolicyPatternNode> patterns = userPreferencesPolicyPatternManager.getPatterns(user.getUserPreferences());

                assertNotNull(patterns);
                assertEquals(1, patterns.size());
                assertTrue(isInherited(patterns));
                assertTrue(containsPolicyPatternNode(patterns, "pattern5"));
            }
            catch (HierarchicalPropertiesException | AddressException e) {
                throw new UnhandledException(e);
            }
        });
    }
}
