/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import com.ciphermail.core.test.service.CipherMailSystemServices;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.james.server.core.MimeMessageUtil;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.mailet.Mail;
import org.apache.mailet.Matcher;
import org.apache.mailet.base.test.FakeMail;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMatcherConfig;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CipherMailSystemServices.class})
public class SenderHeaderTriggerTest
{
    @Autowired
    private TransactionOperations transactionOperations;

    @Autowired
    private UserWorkflow userWorkflow;

    @Autowired
    private AbstractApplicationContext applicationContext;

    @BeforeClass
    public static void setUpBeforeClass() {
        Configurator.setLevel(SenderHeaderTrigger.class, Level.DEBUG);
    }

    @Before
    public void setup()
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                // delete all users
                for (User user : userWorkflow.getUsers(null, null, SortDirection.ASC)) {
                    userWorkflow.deleteUser(user);
                }
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private void setUserProperty(String email, String propertyName, String value)
    {
        transactionOperations.executeWithoutResult(status ->
        {
            try {
                User user = userWorkflow.getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

                user.getUserPreferences().getProperties().setProperty(propertyName, value);

                userWorkflow.makePersistent(user);
            }
            catch (Exception e) {
                throw new UnhandledException(e);
            }
        });
    }

    private Mail createMail(String from, String[] headers, String... recipients)
    throws Exception
    {
        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");
        message.setSubject("test");
        message.setFrom(from != null ? new InternetAddress(from) : null);

        for (String header : headers) {
            message.addHeaderLine(header);
        }

        message.saveChanges();

        return FakeMail.builder().name(MailImpl.getId())
                .recipients(recipients)
                .sender("sender@example.com")
                .mimeMessage(message)
                .size(MimeMessageUtil.getMessageSize(message))
                .build();
    }

    private Matcher createMatcher(FakeMatcherConfig matcherConfig)
    throws Exception
    {
        Matcher matcher = new SenderHeaderTrigger();

        // auto wire the matcher
        applicationContext.getAutowireCapableBeanFactory().autowireBean(matcher);

        matcher.init(matcherConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return matcher;
    }

    @Test
    public void testEmptyTrigger()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "  ");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST: sdfkldfkl  "},
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testInvalidPatternNoMatch()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:(?");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST:  123"}, "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Invalid Trigger value: X-TEST:(?"));

        assertEquals(0, result.size());
    }

    @Test
    public void testMatchNoRegEx()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST: sdfkldfkl  "},
                "someuser@example.com", "other@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertTrue(result.contains(new MailAddress("other@example.com")));
    }

    @Test
    public void testMatchEmptyRegEx()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:      ");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST: sdfkldfkl  "},
                "someuser@example.com", "other@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertTrue(result.contains(new MailAddress("other@example.com")));
    }

    @Test
    public void testMatch()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST:  TrUe  "},
                "someuser@example.com", "other@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(2, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
        assertTrue(result.contains(new MailAddress("other@example.com")));
    }

    @Test
    public void testHeaderNoMatch()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST:  123"}, "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(0, result.size());
    }

    @Test
    public void testMatchEnabled()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST:  TrUe  "},
                "someuser@example.com");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
    }

    @Test
    public void testMatchEnabledLogOnMatchDebugNoMailID()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");
        setUserProperty("test@example.com", "test.enabled", "true");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST:  TrUe  "},
                "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "abc");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "#logOnMatch. MailID: abc"));

        assertEquals(1, result.size());
        assertTrue(result.contains(new MailAddress("someuser@example.com")));
    }

    @Test
    public void testMatchDisabled()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");
        setUserProperty("test@example.com", "test.enabled", "false");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST: true"}, "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "abc");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "#logOnDisabled. MailID: abc"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "#logOnNoMatch. MailID: abc"));

        assertEquals(0, result.size());
    }

    @Test
    public void testMatchDisabledPropNotFoundLogDisabled()
    throws Exception
    {
        setUserProperty("test@example.com", "test.trigger", "X-TEST:^(?i)\\s*true\\s*$");

        FakeMatcherConfig matcherConfig = FakeMatcherConfig.builder()
                .mailetContext(FakeMailContext.defaultContext())
                .matcherName("test")
                .condition("matchOnError=false,{'triggerProperty':'test.trigger', 'enabledProperty':'test.enabled', " +
                           "'logOnDisabled':'Disabled {}'}")
                .build();

        Mail mail = createMail("test@example.com", new String[]{"X-TEST: true"}, "someuser@example.com");

        CoreApplicationMailAttributes.setMailID(mail, "abc");

        Matcher matcher = createMatcher(matcherConfig);

        Collection<MailAddress> result = matcher.match(mail);

        checkLogsForErrorsOrExceptions();

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Disabled abc"));

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "#logOnNoMatch. MailID: abc"));

        assertEquals(0, result.size());
    }
}
