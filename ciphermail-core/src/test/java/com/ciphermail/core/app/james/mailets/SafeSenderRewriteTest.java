/*
 * Copyright (c) 2018-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.test.MemoryLogAppenderResource;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.apache.mailet.Mailet;
import org.apache.mailet.base.test.FakeMailContext;
import org.apache.mailet.base.test.FakeMailetConfig;
import org.junit.Rule;
import org.junit.Test;

import javax.mail.internet.MimeMessage;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class SafeSenderRewriteTest
{
    @Rule
    public MemoryLogAppenderResource memoryLogAppender = new MemoryLogAppenderResource(LogManager.getRootLogger());

    // checks whether the log output contains errors or exceptions
    private void checkLogsForErrorsOrExceptions()
    {
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Exception")));
        assertThat(memoryLogAppender.getLogOutput(), not(containsString("Error")));
    }

    private MailImpl createMail(String sender, String from, String replyTo, String... recipients)
    throws Exception
    {
        MailImpl mail = MailImpl
                .builder()
                .name(MailImpl.getId())
                .addRecipients(recipients)
                .state(Mail.DEFAULT)
                .build();

        mail.setSender(sender != null ? new MailAddress("sender@example.com") : null);

        MimeMessage message = new MimeMessage(MailSession.getDefaultSession());

        message.setContent("test", "text/plain");

        if (from != null) {
            message.setHeader("From", from);
        }

        if (replyTo != null) {
            message.setHeader("Reply-To", replyTo);
        }

        message.saveChanges();

        mail.setMessage(message);

        return mail;
    }

    private Mailet createMailet(FakeMailetConfig mailetConfig)
    throws Exception
    {
        Mailet mailet = new SafeSenderRewrite();

        mailet.init(mailetConfig);

        // reset log output since we are only interested in the logs produced while handling not during init
        memoryLogAppender.reset();

        return mailet;
    }

    @Test
    public void testStaticValuesNoAttributes()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        MimeMessage sourceMessage = mail.getMessage();

        String messageID = sourceMessage.getMessageID();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("\"in name of from@example.com\" <newsender@example.com>",
                newMessage.getHeader("From", ","));
        assertEquals("from@example.com", newMessage.getHeader("Reply-To", ","));
        assertEquals(messageID, newMessage.getMessageID());
        assertNotSame(sourceMessage, newMessage);
    }

    @Test
    public void testStaticValuesNoAttributesEmptyTemplate()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .setProperty("personalTemplate", "")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        MimeMessage sourceMessage = mail.getMessage();

        String messageID = sourceMessage.getMessageID();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("<newsender@example.com>", newMessage.getHeader("From", ","));
        assertEquals("from@example.com", newMessage.getHeader("Reply-To", ","));
        assertEquals(messageID, newMessage.getMessageID());
        assertNotSame(sourceMessage, newMessage);
    }

    @Test
    public void testDisabled()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "false")
                .setProperty("sender", "newsender@example.com")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        MimeMessage sourceMessage = mail.getMessage();

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("sender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("from@example.com", newMessage.getHeader("From", ","));
        assertSame(sourceMessage, newMessage);
    }

    @Test
    public void testNoFrom()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .build();

        Mail mail = createMail("sender@example.com", null, null,
                "recipient@example.com");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertNull(newMessage.getHeader("From"));
        assertNull(newMessage.getHeader("Reply-To"));
    }

    @Test
    public void testReplyToAlreadySet()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", "reply@example.com",
                "recipient@example.com");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("\"in name of from@example.com\" <newsender@example.com>",
                newMessage.getHeader("From", ","));
        assertEquals("reply@example.com", newMessage.getHeader("Reply-To", ","));
    }

    @Test
    public void testAttributes()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enableAttribute", "rewrite.enable")
                .setProperty("senderAttribute", "rewrite.sender")
                .setProperty("personalTemplateAttribute", "rewrite.personalTemplate")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        mail.setAttribute(Attribute.convertToAttribute("rewrite.enable", "true"));
        mail.setAttribute(Attribute.convertToAttribute("rewrite.sender", "newsender@example.com"));
        mail.setAttribute(Attribute.convertToAttribute("rewrite.personalTemplate", "%s blabla"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("\"from@example.com blabla\" <newsender@example.com>", newMessage.getHeader("From", ","));
        assertEquals("from@example.com", newMessage.getHeader("Reply-To", ","));
    }

    @Test
    public void testAttributeInvalidSender()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enableAttribute", "rewrite.enable")
                .setProperty("senderAttribute", "rewrite.sender")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        mail.setAttribute(Attribute.convertToAttribute("rewrite.enable", true));
        mail.setAttribute(Attribute.convertToAttribute("rewrite.sender", "!@#$"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        assertThat(memoryLogAppender.getLogOutput(), containsString(
                "Email address !@#$ is not valid"));

        MimeMessage newMessage = mail.getMessage();

        assertTrue(mail.getMaybeSender().isNullSender());
        assertEquals("\"in name of from@example.com\" <!@#$>", newMessage.getHeader("From", ","));
    }

    @Test
    public void testAttributeNullSender()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enableAttribute", "rewrite.enable")
                .setProperty("senderAttribute", "rewrite.sender")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        mail.setAttribute(Attribute.convertToAttribute("rewrite.enable", true));
        mail.setAttribute(Attribute.convertToAttribute("rewrite.sender", "<>"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertTrue(mail.getMaybeSender().isNullSender());
    }

    @Test
    public void testNullSender()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .build();

        Mail mail = createMail(null, "from@example.com", null,
                "recipient@example.com");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
    }

    @Test
    public void testNoAddReplyTo()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .setProperty("addReplyTo", "false")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("\"in name of from@example.com\" <newsender@example.com>",
                newMessage.getHeader("From", ","));
        assertNull(newMessage.getHeader("Reply-To"));
    }

    @Test
    public void testNoAddReplyToAttribute()
    throws Exception
    {
        FakeMailContext mailContext = FakeMailContext.defaultContext();

        FakeMailetConfig mailetConfig = FakeMailetConfig.builder()
                .mailetContext(mailContext)
                .setProperty("enable", "true")
                .setProperty("sender", "newsender@example.com")
                .setProperty("addReplyToAttribute", "rewrite.addReplyTo")
                .build();

        Mail mail = createMail("sender@example.com", "from@example.com", null,
                "recipient@example.com");

        mail.setAttribute(Attribute.convertToAttribute("rewrite.addReplyTo", "false"));

        Mailet mailet = createMailet(mailetConfig);

        mailet.service(mail);

        checkLogsForErrorsOrExceptions();

        MimeMessage newMessage = mail.getMessage();

        assertEquals("newsender@example.com", mail.getMaybeSender().get().toString());
        assertEquals("\"in name of from@example.com\" <newsender@example.com>",
                newMessage.getHeader("From", ","));
        assertNull(newMessage.getHeader("Reply-To"));
    }
}
