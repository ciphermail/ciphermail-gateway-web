/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.AttributeValue;
import org.apache.mailet.Mail;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


public class MailAttributesUtilsTest
{
    @Test
    public void testGetManagedAttributeValue()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(new Attribute(AttributeName.of("test"), AttributeValue.of("string")));

        assertEquals("string", MailAttributesUtils.getManagedAttributeValue(mail,
                "test", String.class).get());

        mail.setAttribute(new Attribute(AttributeName.of("test"), AttributeValue.of(1)));

        assertEquals(1, (int) MailAttributesUtils.getManagedAttributeValue(mail,
                "test", Integer.class).get());

        mail.setAttribute(new Attribute(AttributeName.of("test"), AttributeValue.of(true)));

        assertEquals(true, MailAttributesUtils.getManagedAttributeValue(mail,
                "test", Boolean.class).get());
    }

    @Test
    public void testGetAttributeValueAsListMultiValue()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(new Attribute(AttributeName.of("test"),
                AttributeValue.of(
                        List.of(
                                AttributeValue.of("value1"),
                                AttributeValue.of(2),
                                AttributeValue.of(true)
                        ))));

        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        List<AttributeValue<?>> values = MailAttributesUtils.getAttributeValueAsList(deserializedMail,
                "test");

        assertEquals(3, values.size());

        assertEquals("value1", values.get(0).valueAs(String.class).get());
        assertEquals(2, (int) values.get(1).valueAs(Integer.class).get());
        assertEquals(true, values.get(2).valueAs(Boolean.class).get());
    }

    @Test
    public void testGetAttributeValueAsListSingleValue()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(new Attribute(AttributeName.of("test"),
            AttributeValue.of("value1")));

        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        List<AttributeValue<?>> values = MailAttributesUtils.getAttributeValueAsList(deserializedMail,
                "test");

        assertEquals(1, values.size());

        assertEquals("value1", values.get(0).valueAs(String.class).get());
    }

    @Test
    public void testGetAttributeValueAsListNoAttribute()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));

        List<AttributeValue<?>> values = MailAttributesUtils.getAttributeValueAsList(deserializedMail,
                "test");

        assertEquals(0, values.size());
    }


    @Test
    public void testGetBoolean()
    {
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(Attribute.convertToAttribute("name", true));

        assertTrue(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        mail.setAttribute(Attribute.convertToAttribute("name", false));

        assertFalse(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        // check default
        assertFalse(MailAttributesUtils.attributeAsBoolean(mail, "other", false));
        assertTrue(MailAttributesUtils.attributeAsBoolean(mail, "other", true));
        assertNull(MailAttributesUtils.attributeAsBoolean(mail, "other", null));

        // check string conversion
        mail.setAttribute(Attribute.convertToAttribute("name", "true"));

        assertTrue(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        mail.setAttribute(Attribute.convertToAttribute("name", "yes"));

        assertTrue(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        mail.setAttribute(Attribute.convertToAttribute("name", "false"));

        assertFalse(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        mail.setAttribute(Attribute.convertToAttribute("name", "no"));

        assertFalse(MailAttributesUtils.attributeAsBoolean(mail, "name", null));

        // check null attribute
        assertNull(MailAttributesUtils.attributeAsBoolean(mail, null, null));
        assertTrue(MailAttributesUtils.attributeAsBoolean(mail, null, true));
        assertFalse(MailAttributesUtils.attributeAsBoolean(mail, null, false));
    }

    @Test
    public void testAttributeAsString()
    {
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(Attribute.convertToAttribute("string", "value"));
        mail.setAttribute(Attribute.convertToAttribute("boolean", true));
        mail.setAttribute(Attribute.convertToAttribute("integer", 1));
        mail.setAttribute(Attribute.convertToAttribute("long", 2L));
        mail.setAttribute(Attribute.convertToAttribute("float", 1.1F));
        mail.setAttribute(Attribute.convertToAttribute("double", 3.14D));

        assertEquals("value", MailAttributesUtils.attributeAsString(mail, "string"));
        assertEquals("true", MailAttributesUtils.attributeAsString(mail, "boolean"));
        assertEquals("1", MailAttributesUtils.attributeAsString(mail, "integer"));
        assertEquals("2", MailAttributesUtils.attributeAsString(mail, "long"));
        assertEquals("1.1", MailAttributesUtils.attributeAsString(mail, "float"));
        assertEquals("3.14", MailAttributesUtils.attributeAsString(mail, "double"));
        assertNull(MailAttributesUtils.attributeAsString(mail, "non-existing"));
        assertEquals("default", MailAttributesUtils.attributeAsString(mail, "non-existing",
                "default"));
    }

    @Test
    public void testGetAttributeValueAsStringList()
    {
        // Because we need to serialize/deserialize, we need to use MailImpl and not Fakemail
        Mail mail = MailImpl.builder().name(MailImpl.getId()).build();

        mail.setAttribute(new Attribute(AttributeName.of("test"),
                AttributeValue.of("value1")));
        Mail deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));
        List<String> values = MailAttributesUtils.getAttributeValueAsStringList(deserializedMail,
                "test");
        assertEquals(1, values.size());
        assertEquals("value1", values.get(0));

        mail.setAttribute(new Attribute(AttributeName.of("test"),
                AttributeValue.of(
                        List.of(
                                AttributeValue.of("value1"),
                                AttributeValue.of(2),
                                AttributeValue.of(true),
                                AttributeValue.of(new byte[]{1,2})
                        ))));
        deserializedMail = SerializationUtils.deserialize(SerializationUtils.serialize(mail));
        values = MailAttributesUtils.getAttributeValueAsStringList(deserializedMail,
                "test");
        // byte[] cannot be converted to sting
        assertEquals(3, values.size());
        assertEquals("value1", values.get(0));
        assertEquals("2", values.get(1));
        assertEquals("true", values.get(2));
    }
}
