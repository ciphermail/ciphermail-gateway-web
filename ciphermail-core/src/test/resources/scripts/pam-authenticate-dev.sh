#!/bin/bash

# NOTE: THIS SCRIPT SHOULD ONLY BE USED DURING DEVELOPMENT WHEN RUNNING THE BACK-END LOCALLY

set -e
set -o pipefail

### BEGIN DJIGZO SCRIPT INFO
# Name: pam-authenticate
# Description: Script for PAM authentication
### END DJIGZO SCRIPT INFO
#
# Copyright 2022, CipherMail.
#

# set a sane/secure path
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
# it's almost certainly already marked for export, but make sure
export PATH

# remove all aliases (start with \ to prevent unalias from being aliased)
\unalias -a

# clean command hash
hash -r

# set a sane/secure IFS (note this is bash & ksh93 syntax only--not portable!)
IFS=$' \t\n'

# the error exit codes 
GENERAL_EXIT_CODE=100

script_name=$(basename "$0")

usage()
{
    echo "Usage: $script_name [option...]" >&2
    echo "" >&2
    echo "    -h                        show usage" >&2
    echo "    --help                    show usage" >&2
    echo "    --username <user>         user to authenticate" >&2
    echo "    --service-name <name>     pam service name" >&2
    
    exit 1
}

exit_with_error()
{
    echo "$1" >&2

    local exit_code="$2"

    if [ -z "$exit_code" ]; then
        exit_code=$GENERAL_EXIT_CODE;
    fi

    exit "$exit_code"
}

debug()
{
    if [[ $debug = true ]]; then
        echo "$1" >&2
    fi
}

[ $# -ne 0 ] || usage

# Note that we use `"$@"' to let each command-line parameter expand to a 
# separate word. The quotes around `$@' are essential!
# We need GETOPT_TEMP as the `eval set --' would nuke the return value of getopt.
GETOPT_TEMP=$(getopt -o h --long "help, debug, username:, service-name:" \
    -n "$script_name" -- "$@")

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around $GETOPT_TEMP: they are essential!
eval set -- "$GETOPT_TEMP"

while true ; do
    case "$1" in
        -h|--help)                      usage ;;
        --debug)                        debug=true ; shift 1 ;;
        --username)                     username="$2" ; shift 2 ;;
        --service-name)                 service_name="$2" ; shift 2 ;;
        --)                             shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

jars="$(find -L ../build/lib ../lib -type f -name \*.jar | tr '\n' ':')"

debug "PWD: $PWD, Jars: $jars"

java -cp "$jars" mitm.common.pam.PAMAuthenticate --username "$username" --service-name "$service_name"

exit 0
