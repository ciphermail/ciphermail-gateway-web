<#--
  This base template will be imported by templates which will be converted into MIME emails.

  The main reason for placing code in this file is that admins can modify the templates while we are flexible
  to add or change functionality to this file (the admin cannot change this file, at least not from the GUI),

  The top part of this template includes variables which are used by most email templates. Template specific
  functionality is added by using Freemarker macro's.

  Notes:
    - All content of a macro is added to the template which calls the macro. Newlines should therefore not be added
      because this will result in invalid MIME.
    - Variables which are cerated with assign can be used by a template and should therefore not be renamed or
      removed because a template might have been overridden by an admin
 -->

<#macro init>
    <#local originatorProperties = (application.getPropertiesInstance(originator, "com.ciphermail.core.app.properties.UserPropertiesImpl"))!>
    <#assign logo = "">
    <#assign footer = (originatorProperties.footer)!"Email encryption with support for S/MIME, OpenPGP, PDF Messenger and Webmail Messenger">
    <#assign footerLink = (originatorProperties.footerLink)!"https://ciphermail.com">
    <#assign footerLinkTitle = (originatorProperties.footerLinkTitle)!"www.ciphermail.com">
    <#assign organizationID = (originatorProperties.organizationID)!"CipherMail">
</#macro>

<#macro otp>
    <#local pdfProperties = (application.getPropertiesInstance(originator, "com.ciphermail.core.app.properties.PDFPropertiesImpl"))!>
    <#assign qrCodeEnabled = (pdfProperties.pdfQrCodeEnabled)!false>
    <#-- passwordContainer variable is added by PDFEncrypt Mailet -->
    <#assign keyID = (passwordContainer.clientSecretID)!>
    <#assign passwordID = (passwordContainer.passwordID)!"not set">
    <#assign passwordLength = (passwordContainer.passwordLength)!0>
    <#local otpJSONCode = "{\"a\":\"otp\",\"k\":\"" + keyID?json_string + "\",\"p\":\"" + passwordID?json_string + "\", \"l\":" + passwordLength?json_string + "}">
    <#-- create a QR code -->
    <#assign otpQRCode = createQR(otpJSONCode, 200, 200, "png")>
    <#-- create a Base64 encoded json value -->
    <#assign otpTextCode = base64Encode(otpJSONCode, true)>
    <#local baseURL = (pdfProperties.OTPURL)!>
    <#if baseURL?has_content>
        <#assign otpURL=baseURL + '?d=' + otpTextCode>
    </#if>
</#macro>

<#macro signup>
    <#assign portalProperties = (application.getPropertiesInstance(originator, "com.ciphermail.core.app.properties.PortalPropertiesImpl"))!>
    <#local signupURL = (portalProperties.signupURL)!>
    <#if signupURL?has_content>
        <#local recipient = recipients[0]>
        <#-- portal signup parameters are stored in the Mail attributes -->
        <#local portalSignups = mailAttributes.getPortalSignups(mail)>
        <#local portalSignup = portalSignups[recipient]>
        <#local jsonSignupCode = "{\"e\":\"" + recipient?json_string + "\",\"t\":" + portalSignup.timestamp?c + ",\"m\":\"" + portalSignup.mac?json_string + "\"}">
        <#assign textSignupCode = base64Encode(jsonSignupCode, true)>
        <#assign portalSignupURL=signupURL + '?d=' + textSignupCode>
    </#if>
</#macro>

<#macro passwordReset>
    <#assign portalProperties = (application.getPropertiesInstance(originator, "com.ciphermail.core.app.properties.PortalPropertiesImpl"))!>
    <#local passwordResetURL = (portalProperties.passwordResetURL)!>
    <#if passwordResetURL?has_content>
        <#local jsonPasswordResetCode = "{\"e\":\"" + recipient?json_string + "\",\"t\":" + timestamp?c + ",\"m\":\"" + mac?json_string + "\"}">
        <#assign textPasswordResetCode = base64Encode(jsonPasswordResetCode, true)>
        <#assign portalPasswordResetURL=passwordResetURL + '?d=' + textPasswordResetCode>
    </#if>
</#macro>

<#macro sms>
    <#assign passwordID = (passwordContainer.passwordID)!"not set">
</#macro>