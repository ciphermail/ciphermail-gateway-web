<#import "macros.ftl" as macros>
<@macros.init/>
From: <${from!"postmaster"}>
<#if to??>
To: <#list to as recipient><${recipient}><#if recipient?has_next>, </#if></#list>
</#if>
<#if replyTo??>
Reply-To: <${replyTo}>
</#if>
Subject: *** DLP quarantine warning ***
In-Reply-To: ${(mail.message.messageID)!""}
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-replied

The message with subject:

${subject!""}

was quarantined because it violated the following DLP policies:

<#if mail??>
<#assign violations = mailAttributes.getPolicyViolations(mail)>
<#list violations as violation>
- ${(violation!"")?truncate(900)}
</#list>
<#assign quarantineID = mailAttributes.getMailRepositoryID(mail)>
</#if>

The message has been quarantined under id: ${quarantineID!"not set"}

<#assign url = .vars["dlp-quarantine-url"]!>
<#if url != "">
For more info see ${url}?id=${quarantineID}
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>