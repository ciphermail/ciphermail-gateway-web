<#import "macros.ftl" as macros>
<@macros.init/>
<#if recipients??>
To: <#list recipients as recipient><${recipient}><#if recipient?has_next>, </#if></#list>
</#if>
In-Reply-To: ${(org.messageID)!""}
Subject: *** The message has been released from quarantine ***
Mime-Version: 1.0
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit
Auto-Submitted: auto-replied

The message with id:

${(org.id)!"not set"}

and subject:

${(org.subject)!""}

has been released from quarantine.

<#if macros.footer?has_content>
---
${macros.footer!} <#if macros.footerLink?has_content>(${macros.footerLink!})</#if>
</#if>