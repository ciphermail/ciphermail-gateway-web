<#import "macros.ftl" as macros>
<@macros.init/>
<@macros.signup/>
Content-Type: multipart/alternative; boundary="${boundary!"boundary-not-set"}"

--${boundary!"boundary-not-set"}
Content-Type: text/plain; charset=UTF-8; format=flowed
Content-Transfer-Encoding: 8bit

You have received a secure message

${from!"unknown sender"} of ${macros.organizationID!"unknown organization"} sent you a secure message

The new message can be read by logging into the CipherMail Messenger portal.

You can read the message by following these steps:

- Click the link below.
- Choose a password, and confirm it by typing it again.
- Log in to the portal with the password you just chose.

The next time you receive a secure message, you can log in with your password.

${macros.portalSignupURL!"URL not set"}

<#if (from.personal)?has_content>
Kind regards,
${from.personal}
</#if>

<#if macros.footer?has_content>
---
${macros.footer!} <#if footerLink?has_content>(${macros.footerLink!})</#if>
</#if>

--${boundary!"boundary-not-set"}
Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: 8bit

<!--suppress XmlUnusedNamespaceDeclaration -->
<!doctype html>
<html lang="en" dir="auto" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <title>You have received a secure message</title><!--[if !mso]><!-->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a { padding:0; }
    body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
    table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
    img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
    p { display:block;margin:13px 0; }
  </style><!--[if mso]>
  <noscript>
    <xml>
      <o:OfficeDocumentSettings>
        <o:AllowPNG/>
        <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml>
  </noscript>
  <![endif]--> <!--[if lte mso 11]>
  <style type="text/css">
    .mj-outlook-group-fix { width:100% !important; }
  </style>
  <![endif]-->
  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-33-333333333333336 { width:33.333333333333336% !important; max-width: 33.333333333333336%; }
      .mj-column-per-100 { width:100% !important; max-width: 100%; }
    }
  </style>
  <style media="screen and (min-width:480px)">
    .moz-text-html .mj-column-per-33-333333333333336 { width:33.333333333333336% !important; max-width: 33.333333333333336%; }
    .moz-text-html .mj-column-per-100 { width:100% !important; max-width: 100%; }
  </style>
  <style type="text/css">

    @media only screen and (max-width:479px) {
      table.mj-full-width-mobile { width: 100% !important; }
      td.mj-full-width-mobile { width: auto !important; }
    }

  </style>
</head>
<body style="word-spacing:normal;">
<div style="" lang="en" dir="auto">
  <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                    <tr>
                      <td style="width:150px;"><img alt="logo" src="cid:logo.cid" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="150" height="auto"></td>
                    </tr>
                    </tbody>
                  </table></td>
              </tr>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td><td class="" style="vertical-align:top;width:200px;" ><![endif]-->
          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div><!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0;padding-top:0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <p style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:100%;"></p><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]--></td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;padding-top:40px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color:#556C8E;">
                    <h2 style="color: #00154A;font-weight: bold;line-height: 150%; font-size: 24px;">You have received a secure message</h2>
                    <h3 style="color: #00154A;font-weight: bold;line-height: 150%; font-size: 20px;">Invitation for signup at CipherMail Portal</h3>
                    <p style="line-height: 160%;color: #556C8E;">${from!"unknown sender"?html} of ${macros.organizationID!"unknown organization"?html} sent you a secure message. You can read this message by logging into the CipherMail Portal <b>by following these steps:</b></p>
                    <ol style="line-height: 160%;color: #556C8E;">
                      <li style="padding-bottom: 0.875em;">Click the link below. This will take you to the portal signup page.</li>
                      <li style="padding-bottom: 0.875em;">Signup by choosing a password. After pressing the "Signup" button, you will be transfered to the Portal login page.</li>
                      <li style="padding-bottom: 0.875em;">Log in to the portal with the password you just chose.</li>
                    </ol>
                    <p style="line-height: 160%;color: #556C8E;">Next time you receive a secure message, you can log in with your password.</p>
                    <div class="pretty-box" style="border: 1px solid #E7EDF8;background-color: #F8F9FC;border-radius: 4px;padding: 20px;margin-bottom: 32px;">
                      <a href="${macros.portalSignupURL!}" style="text-decoration: none;color: #39BB8C;font-weight: bold;"> ${macros.portalSignupURL!"URL not set"} </a>
                    </div>
                  </div></td>
              </tr>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div><!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <!-- <#if (from.personal)?has_content> -->
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <p style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:100%;"></p><!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 2px #E7EDF8;font-size:1px;margin:0px auto;width:550px;" role="presentation" width="550px" ><tr><td style="height:0;line-height:0;"> &nbsp;
</td></tr></table><![endif]--></td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:16px;line-height:160%;text-align:left;color:#556C8E;">
                    <p>Kind regards,<br>
                      ${from.personal?html}</p>
                  </div></td>
              </tr><!-- </#if> -->
              </tbody>
            </table>
          </div><!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div><!--[if mso | IE]></td></tr></table><![endif]--> <!-- <#if macros.footer?has_content> --> <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#F8F9FC" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
  <div style="background:#F8F9FC;background-color:#F8F9FC;margin:0px auto;max-width:600px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#F8F9FC;background-color:#F8F9FC;width:100%;">
      <tbody>
      <tr>
        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;"><!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tbody>
              <tr>
                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div style="font-family:helvetica;font-size:11px;line-height:1;text-align:center;color:#9B9B9B;">
                    ${macros.footer?html!} <!-- <#if macros.footerLink?has_content> --> <a href="${macros.footerLink!}" style="color: #8095B5;display: inline-block;margin: 0 6px;text-decoration: none;"> ${macros.footerLinkTitle?html!} </a> <!-- </#if> -->
                  </div></td>
              </tr>
              </tbody>
            </table>
          </div><!--[if mso | IE]></td></tr></table><![endif]--></td>
      </tr>
      </tbody>
    </table>
  </div><!--[if mso | IE]></td></tr></table><![endif]--> <!-- </#if> -->
</div>
</body>
</html>
--${boundary!"boundary-not-set"}--
