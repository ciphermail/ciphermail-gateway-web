/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import java.io.InputStream;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

/**
 * A MimeMessage extension which allows you to specify the message-id
 *
 * @author Martijn Brinkers
 *
 */
public class MimeMessageWithID extends MimeMessage
{
    /*
     * The message ID of the message
     */
    private final String messageID;

    public MimeMessageWithID(Session session, String messageID)
    {
        super(session);

        this.messageID = messageID;
    }

    public MimeMessageWithID(MimeMessage source, String messageID)
    throws MessagingException
    {
        super(source);

        this.messageID = messageID;

        if (messageID != null) {
            updateMessageID();
        }
    }

    public MimeMessageWithID(Session session, InputStream input, String messageID)
    throws MessagingException
    {
        super(session, input);

        this.messageID = messageID;

        if (messageID != null) {
            updateMessageID();
        }
    }

    @Override
    protected void updateMessageID()
    throws MessagingException
    {
        if (messageID != null) {
            setHeader("Message-ID", messageID);
        }
        else {
            super.updateMessageID();
        }
    }
}
