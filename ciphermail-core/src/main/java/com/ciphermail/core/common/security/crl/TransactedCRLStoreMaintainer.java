/*
 * Copyright (c) 2017-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import org.apache.commons.lang.UnhandledException;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.security.cert.CRL;
import java.util.Objects;

/**
 * Extension of CRLStoreMaintainerImpl which wraps certain calls in a database transaction
 *
 * @author Martijn Brinkers
 *
 */
public class TransactedCRLStoreMaintainer extends CRLStoreMaintainerImpl
{
    /*
     * Helper for executing methods wrapped in a database transaction
     */
    private final TransactionOperations transactionOperations;

    public TransactedCRLStoreMaintainer(@Nonnull X509CRLStoreExt crlStore, @Nonnull CRLPathBuilderFactory pathBuilderFactory,
            @Nonnull TransactionOperations transactionOperations)
    {
        super(crlStore, pathBuilderFactory);

        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    public TransactedCRLStoreMaintainer(@Nonnull X509CRLStoreExt crlStore, @Nonnull CRLPathBuilderFactory pathBuilderFactory,
            @Nonnull TransactionOperations transactionOperations, boolean checkTrust)
    {
        super(crlStore, pathBuilderFactory, checkTrust);

        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    @Override
    protected boolean internalAddCRL(@Nonnull final CRL crl)
    throws CRLStoreException
    {
        // Add the CRL in its own session/transaction. We do not want all downloaded CRLs to be added in one large
        // transaction because CRLs can be extremely large
        try {
            return Boolean.TRUE.equals(transactionOperations.execute(status ->
            {
                try {
                    return TransactedCRLStoreMaintainer.super.internalAddCRL(crl);
                }
                catch (CRLStoreException | NoX509CRLException e) {
                    throw new UnhandledException(e);
                }
            }));
        }
        catch (UnhandledException e) {
            throw new CRLStoreException(e);
        }
    }
}
