/*
 * Copyright (c) 2013-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

/**
 * Implementation of PGPSignatureValidatorResult
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSignatureValidatorResultImpl implements PGPSignatureValidatorResult
{
    /*
     * True if signature is valid
     */
    private final boolean valid;

    /*
     * If the signature is not valid, this will provide the reason
     */
    private final String errorMessage;

    /*
     * True if the signature was invalid because the hash was not correct
     */
    private final boolean tampered;

    /*
     * The signer (can be null if signer cannot be found)
     */
    private final PGPKeyRingEntry signer;

    public PGPSignatureValidatorResultImpl(boolean valid, String errorMessage, boolean tampered, PGPKeyRingEntry signer)
    {
        this.valid = valid;
        this.errorMessage = errorMessage;
        this.tampered = tampered;
        this.signer = signer;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public String getFailureMessage() {
        return errorMessage;
    }

    @Override
    public boolean isTampered() {
        return tampered;
    }

    @Override
    public PGPKeyRingEntry getSigner() {
        return signer;
    }
}
