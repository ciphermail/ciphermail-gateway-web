/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * A named binary larged object can be used to store binary data. A NamedBlob can contain other NamedBlob (childs).
 *
 * @author Martijn Brinkers
 *
 */
public interface NamedBlob
{
    /**
     * The category this blob belongs to.
     */
    @Nonnull String getCategory();

    /**
     * The name of this blob.
     */
    void setName(@Nonnull String name);
    @Nonnull String getName();

    /**
     * The actual content.
     */
    void setBlob(byte[] blob);
    byte[] getBlob();

    /**
     * A NamedBlob can reference other NamedBlob's. This can be used to create NamedBlob groups. The returned
     * Set of NamedBlob's can be a live Set (i.e. if changed, the changes will be persisted).
     */
    @Nonnull Set<NamedBlob> getNamedBlobs();
}
