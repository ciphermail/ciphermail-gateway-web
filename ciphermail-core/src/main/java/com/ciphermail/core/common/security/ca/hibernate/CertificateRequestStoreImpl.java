/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * CertificateRequestStore implementation that stores the requests in the database. This implementation requires that
 * the CertificateRequest is-a CertificateRequestEnity (see CertificateRequestDAO for more info).
 *
 * @author Martijn Brinkers
 *
 */
public class CertificateRequestStoreImpl implements CertificateRequestStore
{
    private static final Logger logger = LoggerFactory.getLogger(CertificateRequestStoreImpl.class);

    /*
     * Handles the database session state.
     */
    private final SessionManager sessionManager;

    /*
     * The registry with all available CertificateRequestHandler's
     */
    private final CertificateRequestHandlerRegistry certificateRequestHandlerRegistry;

    public CertificateRequestStoreImpl(@Nonnull SessionManager sessionManager,
            @Nonnull CertificateRequestHandlerRegistry certificateRequestHandlerRegistry)
    {
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.certificateRequestHandlerRegistry = Objects.requireNonNull(certificateRequestHandlerRegistry);
    }

    protected CertificateRequest getNextRequest(Date date) {
        return getDAO().getNextRequest(date);
    }

    @Override
    public CertificateRequest getNextRequest() {
        return getNextRequest(new Date());
    }

    @Override
    public void addRequest(@Nonnull CertificateRequest request) {
        getDAO().addRequest(request);
    }

    @Override
    public CertificateRequest getRequest(@Nonnull UUID id) {
        return getDAO().findById(id, CertificateRequestEntity.class);
    }

    @Override
    public void deleteRequest(@Nonnull UUID id)
    {
        CertificateRequestDAO dao = getDAO();

        CertificateRequest request = dao.findById(id, CertificateRequestEntity.class);

        if (request != null)
        {
            CertificateRequestHandler requestHandler = getCertificateRequestHandler(request);

            if (requestHandler != null)
            {
                try {
                    requestHandler.cleanup(request);
                }
                catch (Exception e) {
                    logger.error("There was an error cleaning up the request", e);
                }
            }
            else {
                logger.warn("CertificateRequestHandler with name {} is no longer available.",
                        request.getCertificateHandlerName());
            }

            getDAO().deleteRequest(request);
        }
    }

    @Override
    public List<? extends CertificateRequest> getRequestsByEmail(@Nonnull String email, Match match,
            Integer firstResult, Integer maxResults)
    {
        return getDAO().getRequestsByEmail(email, match, firstResult, maxResults);
    }

    @Override
    public List<? extends CertificateRequest> getAllRequests(Integer firstResult, Integer maxResults) {
        return getDAO().getAllRequests(firstResult, maxResults);
    }

    @Override
    public CloseableIterator<? extends CertificateRequest> getAllRequests() {
        return getDAO().getAllRequests();
    }

    @Override
    public long getSize() {
        return getDAO().getSize();
    }

    @Override
    public long getSizeByEmail(@Nonnull String email, Match match) {
        return getDAO().getSizeByEmail(email, match);
    }

    @Override
    public CertificateRequestHandler getCertificateRequestHandler(@Nonnull CertificateRequest request)
    {
        CertificateRequestHandler handler = certificateRequestHandlerRegistry.getHandler(
                request.getCertificateHandlerName());

        if (handler == null) {
            logger.warn("Certificate request handler with name {} not found", request.getCertificateHandlerName());
        }

        return handler;
    }

    private CertificateRequestDAO getDAO() {
        return CertificateRequestDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }
}
