/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringTokenizer;
import org.apache.commons.text.matcher.StringMatcherFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Helper class for running external applications.
 */
public class ProcessRunner
{
    private static final Logger logger = LoggerFactory.getLogger(ProcessRunner.class);

    public static final String SUDO_COMMAND;

    static {
        SUDO_COMMAND = System.getProperty("com.ciphermail.core.common.util.processrunner.sudo-command", "sudo");
    }

    public static String getSudoCommand() {
        return SUDO_COMMAND;
    }

    /*
     * Thread class which will be used to copy and input stream to an output
     * stream in its own class. This is used to handle standard output and error
     * without blocking.
     */
    private static class StreamCopier extends Thread
    {
        private final InputStream input;
        private final OutputStream output;
        private final String identifier;
        private final boolean closeInput;
        private final boolean closeOutput;

        public StreamCopier(@Nonnull InputStream input, @Nonnull OutputStream output, String identifier,
                boolean closeInput, boolean closeOutput)
        {
            this.input = input;
            this.output = output;
            this.identifier = identifier;
            this.closeInput = closeInput;
            this.closeOutput = closeOutput;
        }

        @Override
        public void run()
        {
            logger.debug("Start thread {}", identifier);

            try {
                IOUtils.copy(input, output);
            }
            catch (IOException e) {
                // ignore
            }
            finally {
                if (closeInput) {
                    IOUtils.closeQuietly(input);
                }

                if (closeOutput) {
                    IOUtils.closeQuietly(output);
                }
            }

            logger.debug("end thread {}", identifier);
        }
    }

    /*
     * The input will be written to the standard input stream of the process
     */
    private InputStream input;

    /*
     * The standard output of the process will be written to this stream.
     */
    private OutputStream output;

    /*
     * The standard error of the process will be written to this stream.
     */
    private OutputStream error;

    /*
     * The process exit code
     */
    private int exitCode;

    /*
     * The process exit code for success
     */
    private int successExitCode;

    /*
     * If true and the exit code of the process is not equal to successExitCode and
     * exception will be thrown.
     */
    private boolean throwExceptionOnErrorExitCode = true;

    /*
     * The number of milliseconds a process may run before it's terminated.
     */
    private long timeout = Long.MAX_VALUE;

    /*
     * If true the complete cmd line will be logged (in exception etc.). You might want to
     * set this to false when one of the arguments contain sensitive data
     */
    private boolean logArguments = true;

    /**
     * If true, the command will be run with sudo
     */
    private boolean requireSudo;

    /**
     * Stores additional environment variables that should be set for the process execution.
     */
    private final Map<String, String> additionalEnvironmentVariables = new HashMap<>();

    public int run(@Nonnull List<String> cmd)
    throws IOException
    {
        if (cmd.isEmpty()) {
            throw new IllegalArgumentException("cmd is invalid.");
        }

        if (requireSudo)
        {
            cmd.add(0, getSudoCommand());
            cmd.add(1, "--non-interactive");
        }

        // For debugging purposes
        String cmdLine = logArguments ? StringUtils.join(cmd, ",") : cmd.get(0);

        logger.debug("Starting application. cmdLine: {}", cmdLine);

        ProcessBuilder processBuilder = new ProcessBuilder(cmd);

        processBuilder.environment().putAll(additionalEnvironmentVariables);

        Process process = processBuilder.start();

        Thread inputThread = null;
        Thread outputThread = null;
        Thread errorThread = null;

        if (input != null)
        {
            inputThread = new StreamCopier(input, process.getOutputStream(), "input", false, true);
            inputThread.start();
        }

        if (output != null)
        {
            outputThread = new StreamCopier(process.getInputStream(), output, "output", true, false);
            outputThread.start();
        }

        if (error != null)
        {
            errorThread = new StreamCopier(process.getErrorStream(), error, "error", true, false);
            errorThread.start();
        }

        try {
            if (!process.waitFor(timeout, TimeUnit.MILLISECONDS)) {
                // Timeout
                throw new IOException(String.format("Timeout running [%s]", cmdLine));
            }

            exitCode = process.exitValue();

            // Need to wait for the threads to finish because otherwise there is
            // a chance we will start using the output before all the output has been
            // written/read.
            if (inputThread != null) {
                inputThread.join();
            }

            if (outputThread != null) {
                outputThread.join();
            }

            if (errorThread != null) {
                errorThread.join();
            }
        }
        catch (InterruptedException e)
        {
            //  Restore interrupted state
            Thread.currentThread().interrupt();

            throw new IOException("Process was interrupted [" + cmdLine + "]", e);
        }
        finally {
            // MUST close the standard streams otherwise some "FIFO pipes" won't be closed until
            // the garbage collector is run. If there are too many open "FIFO pipes", the following
            // exception might occur:
            //
            // java.io.IOException: error=24, Too many open files
            //
            // Closing the standard streams makes sure that the pipes are closed.
            //
            // Note: The Javadoc for Process does not mention that you should close the standard streams
            // even if not used. Perhaps they rely on the GC?
            //
            // Note2: The number of FIFI pipes can be counted with:
            //
            // lsof | grep java | grep pipe | wc -l
            IOUtils.closeQuietly(process.getInputStream());
            IOUtils.closeQuietly(process.getOutputStream());
            IOUtils.closeQuietly(process.getErrorStream());
        }

        if (throwExceptionOnErrorExitCode && exitCode != successExitCode) {
            throw new ProcessException(String.format("Error running [%s]. exit code: %s", cmdLine, exitCode), exitCode);
        }

        return exitCode;
    }

    public InputStream getInput() {
        return input;
    }

    public ProcessRunner setInput(InputStream input)
    {
        this.input = input;
        return this;
    }

    public OutputStream getOutput() {
        return output;
    }

    public ProcessRunner setOutput(OutputStream output)
    {
        this.output = output;
        return this;
    }

    public OutputStream getError() {
        return error;
    }

    public ProcessRunner setError(OutputStream error)
    {
        this.error = error;
        return this;
    }

    public int getExitCode() {
        return exitCode;
    }

    public ProcessRunner setExitCode(int exitCode)
    {
        this.exitCode = exitCode;
        return this;
    }

    public int getSuccessExitCode() {
        return successExitCode;
    }

    public ProcessRunner setSuccessExitCode(int successExitCode)
    {
        this.successExitCode = successExitCode;
        return this;
    }

    public boolean isThrowExceptionOnErrorExitCode() {
        return throwExceptionOnErrorExitCode;
    }

    public ProcessRunner setThrowExceptionOnErrorExitCode(boolean throwExceptionOnErrorExitCode)
    {
        this.throwExceptionOnErrorExitCode = throwExceptionOnErrorExitCode;
        return this;
    }

    public long getTimeout() {
        return timeout;
    }

    public ProcessRunner setTimeout(long timeout)
    {
        this.timeout = timeout;
        return this;
    }

    public ProcessRunner setLogArguments(boolean logArguments)
    {
        this.logArguments = logArguments;
        return this;
    }

    public boolean isLogArguments() {
        return logArguments;
    }

    public boolean isRequireSudo() {
        return requireSudo;
    }

    public ProcessRunner setRequireSudo(boolean requireSudo)
    {
        this.requireSudo = requireSudo;
        return this;
    }

    public Map<String, String> additionalEnvironmentVariables() {
        return additionalEnvironmentVariables;
    }

    /*
     * Splits a command line into parts. The parts are split at spaces unless a part is quoted
     */
    public static List<String> splitCommandLine(@Nonnull String commandLine)
    {
        StringTokenizer tokenizer = new StringTokenizer(commandLine, ' ');

        tokenizer.setIgnoreEmptyTokens(false);
        tokenizer.setQuoteMatcher(StringMatcherFactory.INSTANCE.charSetMatcher('"', '\''));

        return tokenizer.getTokenList();
    }
}
