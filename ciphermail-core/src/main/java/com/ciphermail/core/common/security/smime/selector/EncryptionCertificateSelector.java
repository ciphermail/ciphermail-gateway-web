/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.selector;

import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.validator.CertificateValidatorChain;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMEEncryption;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * EncryptionCertificateSelector is a CertificateSelector implementation that selects certificates
 * used for encryption for a specific email address. The certificate is checked to see if the
 * certificate is valid, trusted and can be used for email encryption.
 *
 * @author Martijn Brinkers
 *
 */
public class EncryptionCertificateSelector implements CertificateSelector
{
    private static final Logger logger = LoggerFactory.getLogger(EncryptionCertificateSelector.class);

    private final PKISecurityServices pKISecurityServices;
    private final EmailSelector emailSelector;

    public EncryptionCertificateSelector(@Nonnull PKISecurityServices pKISecurityServices)
    {
        this.pKISecurityServices = Objects.requireNonNull(pKISecurityServices);

        EmailSelector.MatchListener matchListener = EncryptionCertificateSelector.this::match;

        emailSelector = new EmailSelector(pKISecurityServices.getKeyAndCertStore(), matchListener,
                MissingKeyAlias.ALLOWED);
    }

    public void setMaxCertificates(int maxCertificates) {
        emailSelector.setMaxMatch(maxCertificates);
    }

    /*
     * Called by EmailSelector to see whether the certificate is acceptable or not.
     */
    private boolean match(X509CertStoreEntry certStoreEntry)
    {
        boolean match = false;

        if (certStoreEntry != null && certStoreEntry.getCertificate() != null)
        {
            X509Certificate certificate = certStoreEntry.getCertificate();

            CertificateValidatorChain chain = new CertificateValidatorChain();

            // Add the following CertificateValidators to the chain so we can check if the certificate is
            // trusted, not revoked and can be used for S/MIME encryption.
            chain.addValidators(new IsValidForSMIMEEncryption());
            chain.addValidators(pKISecurityServices.getPKITrustCheckCertificateValidatorFactory().
                    createValidator(null));

            try {
                match = chain.isValid(certificate);
            }
            catch (CertificateException e) {
                logger.error("Error validating certificate.", e);
            }
        }

        return match;
    }

    @Override
    public @Nonnull Set<X509Certificate> getMatchingCertificates(String email)
    {
        Set<X509Certificate> certificates = new LinkedHashSet<>();

        Set<X509CertStoreEntry> matchingEntries = emailSelector.getMatchingEntries(email);

        for (X509CertStoreEntry certStoreEntry : matchingEntries)
        {
            X509Certificate certificate = certStoreEntry.getCertificate();

            if (certificate != null) {
                certificates.add(certificate);
            }
        }

        return certificates;
    }
}
