/*
 * Copyright (c) 2016-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Helper class to generate QR codes compatible with Google authenticator
 *
 * @author Martijn Brinkers
 *
 */
public class GoogleAuthenticatorUtils
{
    private GoogleAuthenticatorUtils() {
        // empty on purpose
    }

    private static String encodeQRCodeParameter(String input)
    {
        // The issuer and account should not contain ":" so we will replace it with *
        return URLEncoder.encode(StringUtils.replaceChars(input, ':', '*'), StandardCharsets.UTF_8);
    }

    /**
     * Generates a QR code
     *
     * @param secret the base32 encoded OTP secret
     * @param issuer the issuer of the QR
     * @param account the account of the QR (typically an email address of the user)
     * @param imageFormat the image format (png, jpg)
     * @param width the width of the generated QR code image
     * @param height the height of the generated QR code image
     * @return the image
     * @throws IOException
     */
    public static byte[] generateQRCode(String secret, String issuer, String account, String imageFormat,
        int width, int height)
    throws IOException
    {
        if (secret == null) {
            return null;
        }

        StrBuilder uriBuilder = new StrBuilder();

        // For Google authenticator URI format see:
        //
        // https://code.google.com/p/google-authenticator/wiki/KeyUriFormat
        uriBuilder.
        append("otpauth://totp/").
        append(encodeQRCodeParameter(issuer)).
        append(":").
        append(encodeQRCodeParameter(account)).
        append("?secret=").append(secret).
        append("&issuer=").append(issuer);

        QRBuilder qrBuilder = new QRBuilder();

        qrBuilder.setImageFormat(imageFormat);
        qrBuilder.setWidth(width);
        qrBuilder.setHeight(height);

        return qrBuilder.buildQRCode(uriBuilder.toString());
    }
}
