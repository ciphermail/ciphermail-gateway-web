/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

/**
 * URI helper functions for the validation of URI's, converting strings to URI etc.
 *
 * @author Martijn Brinkers
 *
 */
public class URIUtils
{
    public enum URIType {BASE, RELATIVE, FULL, ALL}

    private URIUtils() {
        // empty on purpose
    }

    /**
     * Checks whether the given identifier is a valid URI. If type is null, a FULL check will be done.
     * If identifier is null, false will be returned.
     */
    public static boolean isValidURI(String identifier, URIType type)
    {
        if (identifier == null) {
            return false;
        }

        if (type == null) {
            type = URIType.FULL;
        }

        boolean valid = false;

        try {
            URI uri = new URI(identifier);

            if (type == URIType.BASE) {
                // Only accepts URI of the form [scheme:][//authority][path]
                if (StringUtils.isNotEmpty(uri.getScheme()) && StringUtils.isNotEmpty(uri.getHost()) &&
                        StringUtils.isEmpty(uri.getQuery()) && StringUtils.isEmpty(uri.getFragment()) &&
                        !StringUtils.endsWith(uri.toString(), "?"))
                {
                    valid = true;
                }
            }
            else if (type == URIType.RELATIVE) {
                // Only accepts URI of the form [path][?query][#fragment]
                if (StringUtils.isEmpty(uri.getScheme()) && StringUtils.isEmpty(uri.getAuthority()) &&
                        StringUtils.isEmpty(uri.getHost()))
                {
                    valid = true;
                }
            }
            else if (type == URIType.FULL) {
                // Only accepts URI of the form [scheme:][//authority][path][?query][#fragment]
                if (StringUtils.isNotEmpty(uri.getScheme()) && StringUtils.isNotEmpty(uri.getHost()))
                {
                    valid = true;
                }
            }
            else {
                valid = true;
            }

        }
        catch (URISyntaxException e) {
            // ignore
        }

        return valid;
    }

    /**
     * Returns the base of the URI.
     * <p>
     * For example:
     * <p>
     * <a href="https://martijn@www.example.com:8443/test/123?email=xxx#99">...</a> -> <a href="https://martijn@www.example.com:8443">...</a>
     */
    public static URI getBaseURI(@Nonnull URI uri)
    throws URISyntaxException
    {
        String scheme = uri.getScheme();

        if (StringUtils.isEmpty(scheme)) {
            scheme = "http";
        }

        return new URI(scheme, uri.getAuthority(), null, null, null);
    }

    /**
     * Returns the base of the URI.
     * <p>
     * For example:
     * <p>
     * <a href="https://martijn@www.example.com:8443/test/123?email=xxx#99">...</a> -> <a href="https://martijn@www.example.com:8443">...</a>
     */
    public static URI getBaseURI(@Nonnull String uri)
    throws URISyntaxException
    {
        return getBaseURI(new URI(uri));
    }

    /**
     * Parses a query string into a map of parameter names to their values.
     *
     * @param query The query string to parse.
     * @return A map of parameter names to their values.
     */
    public static Map<String, List<String>> parseQuery(@Nonnull String query)
    {
        return Pattern.compile("&")
                .splitAsStream(query)
                .map(s -> Arrays.copyOf(s.split("=", 2), 2))
                .collect(groupingBy(s -> urlDecode(s[0]), mapping(s -> urlDecode(s[1]), toList())));
    }

    private static String urlDecode(String encoded)
    {
        return Optional.ofNullable(encoded)
                .map(e -> URLDecoder.decode(e, StandardCharsets.UTF_8))
                .orElse(null);
    }
}
