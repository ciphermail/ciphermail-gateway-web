/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Reader extension that delegated to a list of readers. This can be used when you have multiple readers
 * (for example multiple versions, because of rotating logs, of a log file) but want just one reader
 * that encapsulates all other readers.
 *
 * MetaReader does not support mark ie markSupported always returns false.
 *
 * @author Martijn Brinkers
 *
 */
public class MetaReader extends Reader
{
    /*
     * The readers to delegate to
     */
    private final List<Reader> delegates;

    public MetaReader(@Nonnull Collection<Reader> delegates)
    {
        if (delegates.isEmpty()) {
            throw new IllegalArgumentException("delegates should contain at least one Reader.");
        }

        this.delegates = new LinkedList<>(delegates);
    }

    @Override
    public void close()
    throws IOException
    {
        IOException ioe = null;

        for (Reader reader : delegates)
        {
            try {
                reader.close();
            }
            catch(IOException e) {
                ioe = e;
            }
        }

        if (ioe != null) {
            throw ioe;
        }
    }

    @Override
    public int read(@Nonnull char[] buf, int off, int len)
    throws IOException
    {
        int currentOffset = off;
        int itemsLeft = len;

        int totalRead = 0;

        boolean somethingRead = false;

        for (Reader reader : delegates)
        {
            int subRead = reader.read(buf, currentOffset, itemsLeft);

            if (subRead == -1) {
                continue;
            }

            somethingRead = true;

            totalRead = totalRead + subRead;

            currentOffset = currentOffset + subRead;
            itemsLeft = itemsLeft - subRead;

            if (itemsLeft == 0) {
                break;
            }
        }

        // We need to return -1 if none of the delegates returned something
        if (!somethingRead) {
            totalRead = -1;
        }

        return totalRead;
    }
}
