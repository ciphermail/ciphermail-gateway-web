/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.dao;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.X509CRLUserType;
import com.ciphermail.core.common.security.certificate.X500PrincipalInspector;
import com.ciphermail.core.common.security.crl.X509CRLInspector;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.hibernate.X509CRLStoreEntity;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.security.cert.X509CRLSelector;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class X509CRLStoreDAO extends GenericHibernateDAO
{
    private static final Logger logger = LoggerFactory.getLogger(X509CRLStoreDAO.class);

    /*
     * Name of the CRL Store. This can be used to have multiple distinct CRL stores in the database.
     */
    private final String storeName;

    private X509CRLStoreDAO(@Nonnull SessionAdapter session, @Nonnull String storeName)
    {
        super(session);

        this.storeName = Objects.requireNonNull(storeName);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static X509CRLStoreDAO getInstance(@Nonnull SessionAdapter session, @Nonnull String storeName) {
        return new X509CRLStoreDAO(session, storeName);
    }

    public void addCRL(@Nonnull X509CRL crl) {
        persist(new X509CRLStoreEntity(crl, storeName, new Date()));
    }

    public X509CRL getCRL(@Nonnull String thumbprint)
    {
        X509CRL crl = null;

        X509CRLStoreEntity entry = getEntry(thumbprint);

        if (entry != null) {
            crl = entry.getCRL();
        }

        return crl;
    }

    public void remove(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        X509CRLStoreEntity entry = getEntry(crl);

        if (entry != null) {
            delete(entry);
        }
    }

    public void replace(@Nonnull X509CRL oldCRL, @Nonnull X509CRL newCRL)
    throws CRLStoreException
    {
        try {
            if (ObjectUtils.equals(X509CRLInspector.getThumbprint(oldCRL),
                    X509CRLInspector.getThumbprint(newCRL)))
            {
                logger.warn("oldCRL is equal to newCRL so won't replace.");

                return;
            }
        }
        catch(NoSuchAlgorithmException | CRLException e) {
            throw new CRLStoreException(e);
        }
        catch (NoSuchProviderException e) {
            throw new UnhandledException(e);
        }

        // We need to add the new one before removing the old one otherwise there is a small
        // time-gap ('race condition') where there is no CRL.
        //
        // We need to check if the newCRL is not already in the store. This can happen when
        // a CRL is added by a user (ie not automatically downloaded) because if it was added
        // by a user it did not yet replace the old one because replacing is done by CRL store
        // maintainer.
        if (!contains(newCRL))
        {
            logger.debug("newCRL is already in the CRL store.");

            addCRL(newCRL);
        }

        remove(oldCRL);
    }

    public boolean contains(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        return getEntry(crl) != null;
    }

    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults) {
        return new X509CRLStoreCRLIterator(getEntriesScrollable(crlSelector, firstResult, maxResults), crlSelector);
    }

    public CloseableIterator<X509CRLStoreEntity> getCRLStoreIterator(CRLSelector crlSelector,
            Integer firstResult, Integer maxResults)
    {
        return new X509CRLStoreEntryIterator(getEntriesScrollable(crlSelector, firstResult, maxResults), crlSelector);
    }

    public void removeAllEntries()
    throws CRLStoreException
    {
        // I tried using HQL and delete but that did not work because of a constraint violation.
        // Using delete seems not to cascade to the certificate_email table.
        CloseableIterator<X509CRLStoreEntity> iterator = getCRLStoreIterator(null, null, null);

        try {
            try {
                while (iterator.hasNext())
                {
                    X509CRLStoreEntity entry = iterator.next();

                    // Need to flush before evict to prevent the following exception when deleting while adding
                    // CRLs at the same time. See https://forum.hibernate.org/viewtopic.php?p=2424890
                    //
                    // java.util.concurrent.ExecutionException: org.hibernate.AssertionFailure: possible
                    // nonthreadsafe access to session
                    this.flush();
                    // evict the entry to save memory
                    this.evict(entry);
                    this.delete(entry);
                }
            }
            finally {
                // we must close the iterator
                iterator.close();
            }
        }
        catch (CloseableIteratorException e) {
            throw new CRLStoreException(e);
        }
    }

    public long size()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<X509CRLStoreEntity> rootEntity = criteriaQuery.from(X509CRLStoreEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(X509CRLStoreEntity.STORE_NAME_COLUMN),
                storeName));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private X509CRLStoreEntity getEntry(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        try {
            return getEntry(X509CRLInspector.getThumbprint(crl));
        }
        catch (NoSuchAlgorithmException | CRLException e) {
            throw new CRLStoreException(e);
        }
        catch (NoSuchProviderException e) {
            throw new UnhandledException(e);
        }
    }

    private X509CRLStoreEntity getEntry(@Nonnull String thumbprint)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CRLStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CRLStoreEntity.class);

        Root<X509CRLStoreEntity> rootEntity = criteriaQuery.from(X509CRLStoreEntity.class);

        Path<X509CRLUserType> crlPath = rootEntity.get(X509CRLStoreEntity.CRL_USER_TYPE_NAME);

        criteriaQuery.where(
                criteriaBuilder.equal(rootEntity.get(X509CRLStoreEntity.STORE_NAME_COLUMN), storeName),
                criteriaBuilder.equal(criteriaBuilder.upper(crlPath.get(X509CRLStoreEntity.THUMBPRINT_COLUMN)),
                        StringUtils.upperCase(thumbprint)));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }


    private List<Predicate> createRestrictionsForCertSelector(@Nonnull CriteriaBuilder criteriaBuilder,
            @Nonnull Path<X509CRLUserType> crlPath, @Nonnull X509CRLSelector crlSelector)
    {
        List<Predicate> restrictions = new LinkedList<>();

        Collection<X500Principal> crlSelectorIssuers = crlSelector.getIssuers();

        if (crlSelectorIssuers != null && !crlSelectorIssuers.isEmpty())
        {
            List<Predicate> issuerRestrictions = new LinkedList<>();

            for (X500Principal issuer : crlSelectorIssuers)
            {
                issuerRestrictions.add(criteriaBuilder.equal(crlPath.get(X509CRLStoreEntity.ISSUER_COLUMN),
                        X500PrincipalInspector.getCanonical(issuer)));
            }

            if (!issuerRestrictions.isEmpty())
            {
                restrictions.add(issuerRestrictions.size() > 1 ?
                            criteriaBuilder.or(CollectionUtils.toArray(issuerRestrictions, Predicate.class)) :
                            issuerRestrictions.get(0));
            }
        }

        Date dateAndTime = crlSelector.getDateAndTime();

        if (dateAndTime != null)
        {
            // According to @{X509CRLSelector} #setDateAndTime
            //
            // Sets the dateAndTime criterion. The specified date must be
            // equal to or later than the value of the thisUpdate component
            // of the {@code X509CRL} and earlier than the value of the
            // nextUpdate component. There is no match if the {@code X509CRL}
            // does not contain a nextUpdate component.
            // If {@code null}, no dateAndTime check will be done.
            restrictions.add(criteriaBuilder.lessThan(crlPath.get(X509CRLStoreEntity.THIS_UPDATE_COLUMN),
                    dateAndTime));

            restrictions.add(criteriaBuilder.greaterThan(crlPath.get(X509CRLStoreEntity.NEXT_UPDATE_COLUMN),
                    dateAndTime));
        }

        return restrictions;
    }

    private ScrollableResults<X509CRLStoreEntity> getEntriesScrollable(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<X509CRLStoreEntity> criteriaQuery = criteriaBuilder.createQuery(
                X509CRLStoreEntity.class);

        Root<X509CRLStoreEntity> rootEntity = criteriaQuery.from(X509CRLStoreEntity.class);

        Path<X509CRLUserType> crlPath = rootEntity.get(X509CRLStoreEntity.CRL_USER_TYPE_NAME);

        List<Predicate> restrictions = new LinkedList<>();

        restrictions.add(criteriaBuilder.equal(rootEntity.get(X509CRLStoreEntity.STORE_NAME_COLUMN),
                storeName));

        if (crlSelector instanceof X509CRLSelector x509CRLSelector)
        {
            // we can do some optimizations using SQL
            restrictions.addAll(createRestrictionsForCertSelector(criteriaBuilder, crlPath, x509CRLSelector));
        }

        criteriaQuery.where(CollectionUtils.toArray(restrictions, Predicate.class));

        criteriaQuery.orderBy(criteriaBuilder.desc(crlPath.get(X509CRLStoreEntity.THIS_UPDATE_COLUMN)));

        Query<X509CRLStoreEntity> query = createQuery(criteriaQuery);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.scroll(ScrollMode.FORWARD_ONLY);
    }
}
