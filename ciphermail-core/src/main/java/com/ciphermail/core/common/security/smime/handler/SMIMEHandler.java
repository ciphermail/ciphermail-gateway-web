/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.cms.CryptoMessageSyntaxException;
import com.ciphermail.core.common.security.cms.KeyNotFoundException;
import com.ciphermail.core.common.security.smime.SMIMEDecryptionAbortedException;
import com.ciphermail.core.common.security.smime.SMIMEHeader;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspectorException;
import com.ciphermail.core.common.security.smime.SMIMEInspectorImpl;
import com.ciphermail.core.common.security.smime.SMIMEType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.Objects;

/**
 * SMIMEHandler handles S/MIME encrypted, signed or compressed messages. Encrypted messages will be decrypted,
 * compressed message will be decompressed and certificates will be extracted from signed messages. Depending
 * on certain settings signatures from signed message can be removed and security info, like if the message
 * was signed and if so by who, can be added to a message. SMIMEHandler only handles one S/MIME layer, i.e.,
 * if a message is signed and then encrypted SMIMEHandler will only remove the encryption layer. SMIMEHandler
 * should be used multiple times to handle all S/MIME layers. See RecursiveSMIMEHandler for a handler that
 * recursively handles all S/MIME layers.
 *
 * Note: This class is not thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class SMIMEHandler
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMEHandler.class);

    private final PKISecurityServices securityServices;

    /*
     * Factory that creates security related instances
     */
    private final SecurityFactory securityFactory;

    /*
     * If true and the message is signed the signature will be removed
     */
    private boolean removeSignature;

    /*
     * True if the message was decrypted
     */
    private boolean decrypted;

    /*
     * True if the message was encrypted but the decryption key could not be found
     */
    private boolean decryptionKeyNotFound;

    /*
     * True if the message was decrypted and the decrypted message contains characters normally not valid for
     * S/MIME
     */
    private boolean messageContainsInvalid7BitChars;

    /*
     * S/MIME type of the message (signed, encrypted etc.)
     */
    private SMIMEType smimeType;

    /*
     * If true and the message is encrypted the message will be decrypted
     */
    private boolean decrypt = true;

    /*
     * If true and the message is compressed the message will be decompressed
     */
    private boolean decompress = true;

    /*
     * If true the message will be checked for illegal characters after decryption (to prevent the EFAIL attack)
     */
    private boolean checkInvalid7BitChars;

    /*
     * If true and illegal characters are detected in the decrypted message (see checkInvalid7BitChars), decryption
     * will be aborted
     */
    private boolean abortDecryptionOnInvalid7BitChars;

    /*
     * If headers are signed or encrypted, which headers should be copied to the outer message
     */
    private String[] protectedHeaders = ProtectedContentHeaderNameMatcher.DEFAULT_PROTECTED_CONTENT_HEADERS;

    /*
     * The SMIMEInspector can be used to inspect the S/MIME message
     */
    private SMIMEInspector inspector;

    /*
     * Listener that gets called when certificate are extracted from a message.
     */
    private CertificateCollectionEvent certificatesEventListener;

    /*
     * The mail ID of the message (if set). This is purely used for logging
     */
    private String mailID;

    public SMIMEHandler(@Nonnull PKISecurityServices securityServices)
    {
        this.securityServices = Objects.requireNonNull(securityServices);

        securityFactory = SecurityFactoryFactory.getSecurityFactory();
    }

    private void copyNonContentHeaders(@Nonnull Part source, @Nonnull Part target)
    throws MessagingException
    {
        HeaderMatcher contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeaders);

        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

        // do not allow the protected blob to add unknown headers to the message.
        // therefore, remove all headers accept content headers and some protected
        // headers (like subject, to etc.) from the target.
        HeaderUtils.removeHeaders(target, nonContentMatcher);

        // we only want to copy some protected headers from source to target if the target does not
        // have these headers.
        @SuppressWarnings("unchecked")
        String[] protectedHeadersToCopy = HeaderUtils.getMatchingHeaderNames(target.getAllHeaders(),
                protectedHeaders);

        contentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeadersToCopy);

        nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

        // copy all non-content headers from source message to the new message
        HeaderUtils.copyHeaders(source, target, nonContentMatcher);
    }

    private MimeMessage handleSigned(@Nonnull Part source, @Nonnull SMIMEInspector inspector)
    throws MessagingException, CryptoMessageSyntaxException, IOException, CertificateException
    {
        logger.debug("Handling signed message.");

        if (inspector.getSMIMEType() != SMIMEType.SIGNED) {
            throw new IllegalStateException("SMIMEType.SIGNED expected");
        }

        MimeMessage newMessage = null;

        if (removeSignature)
        {
            newMessage = inspector.getContentAsMimeMessage();

            copyNonContentHeaders(source, newMessage);
        }

        if (certificatesEventListener != null) {
            certificatesEventListener.certificatesEvent(inspector.getSignedInspector().getCertificates());
        }

        if (newMessage == null)
        {
            //  make a clone of the source message since we always want the return a new instance.
            newMessage = BodyPartUtils.toMessage(source, true);

            // we may have to 'repair' the content type if it's not the correct type for S/MIME
            SMIMEHeader.Type sMIMEType = SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES);

            if (sMIMEType != SMIMEHeader.Type.OPAQUE_SIGNED && !newMessage.isMimeType("multipart/*"))
            {
                newMessage.setHeader("Content-Type", SMIMEHeader.ENCAPSULATED_SIGNED_CONTENT_TYPE);
                newMessage.setHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
                newMessage.setHeader("Content-Description", "S/MIME Cryptographic Signed Data");
            }
        }

        return newMessage;
    }

    private MimeMessage handleEncrypted(@Nonnull Part source, @Nonnull SMIMEInspector inspector)
    throws MessagingException, CryptoMessageSyntaxException, IOException
    {
        logger.debug("Handling encrypted message.");

        if (inspector.getSMIMEType() != SMIMEType.ENCRYPTED && inspector.getSMIMEType() != SMIMEType.AUTH_ENCRYPTED) {
            throw new IllegalStateException("ENCRYPTED or AUTH_ENCRYPTED expected");
        }

        MimeMessage newMessage = null;

        try {
            newMessage = inspector.getContentAsMimeMessage();

            if (newMessage != null && checkInvalid7BitChars)
            {
                // Check for character which are not 7bit or are control chars normally not used with email.
                // This is mainly to protect against EFAIL. This option however can lead to false positives
                // if the MIME part was not converted to 7bit before encryption.
                if (MailUtils.containsInvalid7BitChars(newMessage))
                {
                    logger.warn("Illegal characters detected in decrypted message; MailID: {};", mailID);

                    messageContainsInvalid7BitChars = true;

                    if (abortDecryptionOnInvalid7BitChars) {
                        throw new SMIMEDecryptionAbortedException("Illegal characters detected in decrypted message");
                    }
                }
            }

            if (newMessage != null)
            {
                copyNonContentHeaders(source, newMessage);

                decrypted = true;
            }
        }
        catch (SMIMEDecryptionAbortedException e)
        {
            logger.warn("Decryption aborted. Illegal characters detected in decrypted message; MailID: {};", mailID);

            // Because we abort the decryption, we need to reset newMessage
            newMessage = null;
        }
        catch (KeyNotFoundException e)
        {
            logger.warn("S/MIME decryption key not found; MailID: {}; Message: {}", mailID, e.getMessage());

            decryptionKeyNotFound = true;
        }

        if (newMessage == null)
        {
            // make a clone of the source message since we always want to return a new instance.
            newMessage = BodyPartUtils.toMessage(source, true);

            // we may have to 'repair' the content type if it's not the correct type for S/MIME
            if (inspector.getSMIMEType() == SMIMEType.ENCRYPTED && SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES) != SMIMEHeader.Type.ENCRYPTED)
            {
                newMessage.setHeader("Content-Type", SMIMEHeader.ENCRYPTED_CONTENT_TYPE);
                newMessage.setHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
                newMessage.setHeader("Content-Description", "S/MIME Encrypted Message");
            }
            else if (inspector.getSMIMEType() == SMIMEType.AUTH_ENCRYPTED && SMIMEHeader.getSMIMEContentType(newMessage, SMIMEHeader.Strict.YES) != SMIMEHeader.Type.ENCRYPTED_AUTH)
            {
                newMessage.setHeader("Content-Type", SMIMEHeader.ENCRYPTED_AUTH_CONTENT_TYPE);
                newMessage.setHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
                newMessage.setHeader("Content-Description", "S/MIME Encrypted Message");
            }
        }

        return newMessage;
    }

    private MimeMessage handleCompressed(Part source, SMIMEInspector inspector)
    throws MessagingException, CryptoMessageSyntaxException
    {
        logger.debug("Handling compressed message.");

        if (inspector.getSMIMEType() != SMIMEType.COMPRESSED) {
            throw new IllegalStateException("COMPRESSED expected");
        }

        MimeMessage newMessage = inspector.getContentAsMimeMessage();

        copyNonContentHeaders(source, newMessage);

        return newMessage;
    }

    /**
     * The message will be decrypted if encrypted. The message will be decompressed if compressed.
     * The signature will be removed if signed and removeSignature is true. If the message is not
     * a S/MIME message null is returned. If the message is encrypted but a suitable decryption
     * key could not be found the message will not be decrypted but returned encrypted.
     */
    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException
    {
        try {
            inspector = new SMIMEInspectorImpl(source, securityServices.getKeyAndCertStore(),
                    securityFactory.getNonSensitiveProvider(),
                    securityFactory.getSensitiveProvider());

            smimeType = inspector.getSMIMEType();

            if (smimeType == SMIMEType.NONE) {
                return null;
            }

            return switch (smimeType) {
                case SIGNED          -> handleSigned(source, inspector);
                case ENCRYPTED       -> decrypt ? handleEncrypted(source, inspector) : null;
                case AUTH_ENCRYPTED  -> decrypt ? handleEncrypted(source, inspector) : null;
                case COMPRESSED      -> decompress ? handleCompressed(source, inspector) : null;
                default -> throw new IllegalArgumentException("Unknown S/MIME type " + smimeType);
            };
        }
        catch (CryptoMessageSyntaxException | CertificateException | MessagingException | IOException |
                SMIMEInspectorException e)
        {
            throw new SMIMEHandlerException(e);
        }
    }

    /**
     * If true and the message was signed the signature will be removed.
     */
    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRemoveSignature() {
        return removeSignature;
    }

    /**
     * Sets the listener that gets called when certificate are extracted from a message.
     */
    public void setCertificatesEventListener(CertificateCollectionEvent event) {
        this.certificatesEventListener = event;
    }

    /**
     * True if the message was encrypted  and the message was successfully decrypted.
     * This property is only valid when smimeType is ENCRYPTED.
     */
    public boolean isDecrypted() {
        return decrypted;
    }

    public SMIMEType getSMIMEType() {
        return smimeType;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public boolean isDecompress() {
        return decompress;
    }

    public void setDecompress(boolean decompress) {
        this.decompress = decompress;
    }

    public boolean isCheckInvalid7BitChars() {
        return checkInvalid7BitChars;
    }

    public void setCheckInvalid7BitChars(boolean checkInvalid7BitChars) {
        this.checkInvalid7BitChars = checkInvalid7BitChars;
    }

    public boolean isAbortDecryptionOnInvalid7BitChars() {
        return abortDecryptionOnInvalid7BitChars;
    }

    public void setAbortDecryptionOnInvalid7BitChars(boolean abortDecryptionOnInvalid7BitChars) {
        this.abortDecryptionOnInvalid7BitChars = abortDecryptionOnInvalid7BitChars;
    }

    public boolean isDecryptionKeyNotFound() {
        return decryptionKeyNotFound;
    }

    public boolean isMessageContainsInvalid7BitChars() {
        return messageContainsInvalid7BitChars;
    }

    public SMIMEInspector getSMIMEInspector() {
        return inspector;
    }

    public String[] getProtectedHeaders() {
        return protectedHeaders;
    }

    public void setProtectedHeaders(String... protectedHeaders) {
        this.protectedHeaders = protectedHeaders;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }
}
