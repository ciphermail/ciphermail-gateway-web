/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties.hibernate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.util.SetBridge;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity(name = NamedBlobEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={NamedBlobEntity.CATEGORY_COLUMN,
        NamedBlobEntity.NAME_COLUMN})}
)
public class NamedBlobEntity implements NamedBlob
{
    static final String ENTITY_NAME = "NamedBlob";

    static final String CATEGORY_COLUMN = "category";
    static final String NAME_COLUMN = "name";

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /*
     * The name will be used to identify multiple named blobs.
     */
    @Column (name = "category", length = SizeUtils.KB, nullable = false)
    private String category;

    /*
     * The name will be used to identify this named blob.
     */
    @Column (name = "name", length = SizeUtils.KB, nullable = false)
    private String name;

    /*
     * The mime message is stored as an entity to allow lazy loading (a byte[] cannot be lazy loaded). Lazy
     * loading is required to support large binary blobs. If not lazy loaded, getting a list of
     * NamedBlobEntity's will result in an Out of Memory.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private NamedBlobDataEntity blobEntity;

    @ManyToMany
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<NamedBlobEntity> namedBlobs = new HashSet<>();

    public NamedBlobEntity() {
        // required by Hibernate
    }

    public NamedBlobEntity(@Nonnull String category, @Nonnull String name)
    {
        this.category = Objects.requireNonNull(category);
        this.name = Objects.requireNonNull(name);
    }

    @Override
    public @Nonnull String getCategory() {
        return category;
    }

    public void setCategory(@Nonnull String category) {
        this.category = Objects.requireNonNull(category);
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    @Override
    public void setName(@Nonnull String name) {
        this.name = Objects.requireNonNull(name);
    }

    @Override
    public byte[] getBlob() {
        return blobEntity != null ? blobEntity.getBlob() : null;
    }

    @Override
    public void setBlob(byte[] blob)
    {
        if (blobEntity == null) {
            blobEntity = new NamedBlobDataEntity();
        }

        blobEntity.setBlob(blob);
    }

    @Override
    public @Nonnull Set<NamedBlob> getNamedBlobs() {
        return new SetBridge<>(namedBlobs, NamedBlobEntity.class);
    }

    @Override
    public String toString() {
        return getCategory() + ":" + getName();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof NamedBlobEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(category, rhs.category)
            .append(name, rhs.name)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(category)
            .append(name)
            .toHashCode();
    }
}
