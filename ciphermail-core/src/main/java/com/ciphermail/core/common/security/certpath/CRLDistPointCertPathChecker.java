/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import com.ciphermail.core.common.security.crl.CRLDistributionPointsInspector;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.Extension;

import java.io.IOException;
import java.security.cert.CRLException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * If a certificate has a critical CRL distribution point extension this
 * PKIXCertPathChecker checks if we can handle the CRL distribution point(s)
 *
 * @author Martijn Brinkers
 *
 */
public class CRLDistPointCertPathChecker extends PKIXCertPathChecker
{
    @Override
    public void check(Certificate certificate, Collection<String> unresolvedCritExts)
    throws CertPathValidatorException
    {
        if (!(certificate instanceof X509Certificate x509Certificate)) {
            throw new CertPathValidatorException("Certificate is not a X509Certificate.");
        }

        if (unresolvedCritExts != null && unresolvedCritExts.contains(Extension.cRLDistributionPoints.getId()))
        {
            try {
                CRLDistPoint distPoint = X509ExtensionInspector.getCRLDistibutionPoints(x509Certificate);

                if (distPoint == null) {
                    throw new CertPathValidatorException("CRLDistributionPoints is critical but CRLDistPoint is null.");
                }

                Set<String> uris = CRLDistributionPointsInspector.getURIDistributionPointNames(distPoint);

                if (uris.isEmpty()) {
                    throw new CertPathValidatorException("CRLDistributionPoints does not contain a supported URI.");
                }

                /*
                 * TODO: check if we can handle the returned uri's
                 */

                // We can handle CRLDistributionPoints so remove from the critical extensions
                unresolvedCritExts.remove(Extension.cRLDistributionPoints.getId());
            }
            catch (IOException | CRLException e) {
                throw new CertPathValidatorException(e);
            }
        }
    }

    @Override
    public Set<String> getSupportedExtensions()
    {
        Set<String> supported = new HashSet<>();

        supported.add(Extension.cRLDistributionPoints.getId());

        return supported;
    }

    @Override
    public void init(boolean forward)
    throws CertPathValidatorException
    {
        // do nothing
    }

    @Override
    public boolean isForwardCheckingSupported() {
        return true;
    }
}
