/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import javax.annotation.Nonnull;
import javax.crypto.SecretKey;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Collection;

/**
 * SMIMEBuilder is used for building S/MIME encrypted, signed and compressed messages.
 *
 * @author Martijn Brinkers
 *
 */
public interface SMIMEBuilder
{
    /**
     * Adds a new signer. The message will be signed and the signer will be identified by issuer/serial number.
     * The following default signed attributes will be added: SMIME capability, signing time.
     */
    void addSigner(@Nonnull PrivateKey privateKey, @Nonnull X509Certificate signer,
            @Nonnull SMIMESigningAlgorithm algorithm)
    throws SMIMEBuilderException;

    /**
     * Adds a new signer. The message will be signed and the signer will be identified by issuer/serial number.
     * The following default signed attributes will be added: SMIME capability, signing time, encryption key preference,
     * and Outlook specific version of encryption key preference (see
     * {@link SMIMEAttributeUtils#getDefaultSignedAttributes(X509Certificate)})
     */
    void addSigner(@Nonnull PrivateKey privateKey, @Nonnull X509Certificate signer,
            @Nonnull SMIMESigningAlgorithm algorithm, X509Certificate encryptionKeyPreference)
    throws SMIMEBuilderException;

    /**
     * Adds a new signer. The message will be signed and the signer will be identified by the subjectKeyIdentifier.
     * The following default signed attributes will be added: SMIME capability, signing time.
     */
    void addSigner(@Nonnull PrivateKey privateKey, @Nonnull byte[] subjectKeyIdentifier,
            @Nonnull SMIMESigningAlgorithm algorithm)
    throws SMIMEBuilderException;

    /**
     * Adds certificates to the CMS
     */
    void addCertificates(Collection<X509Certificate> certificates)
    throws SMIMEBuilderException;

    /**
     * Adds certificates to the CMS
     */
    void addCertificates(X509Certificate... certificates)
    throws SMIMEBuilderException;

    /**
     * Encrypts the message with the certificate. The recipient will be identified by a KeyTransRecipientInfo.
     * The provided mode determines whether issuer/serial nr and/or Subject key identifier will be used.
     */
    void addRecipient(@Nonnull X509Certificate certificate, @Nonnull SMIMERecipientMode mode,
            @Nonnull SMIMEEncryptionScheme scheme)
    throws SMIMEBuilderException;

    /**
     * Encrypts the message with the certificate. The recipient will be identified by a KeyTransRecipientInfo.
     * The provided mode determines whether issuer/serial nr and/or Subject key identifier will be used. The
     * recipient will be encrypted using the default RSAES_PKCS1_v1_5 scheme
     */
    void addRecipient(@Nonnull X509Certificate certificate, @Nonnull SMIMERecipientMode mode)
    throws SMIMEBuilderException;

    /**
     * Encrypts the message with a secret key. The recipient will be identified by a KEKRecipientInfo.
     */
    void addRecipient(@Nonnull SecretKey secretKey, @Nonnull byte[] keyIdentifier);

    /**
     * Digitally sign the message in clear or opaque mode.
     */
    void sign(@Nonnull SMIMESignMode signMode)
    throws SMIMEBuilderException;

    /**
     * Encrypt the message with the provided algorithm and key strength.
     */
    void encrypt(@Nonnull SMIMEEncryptionAlgorithm algorithm, int keySize)
    throws SMIMEBuilderException;

    /**
     * Encrypt the message with the provided algorithm using the default key strength of the algorithm.
     */
    void encrypt(@Nonnull SMIMEEncryptionAlgorithm algorithm)
    throws SMIMEBuilderException;

    /**
     * Compress the message.
     */
    void compress()
    throws SMIMEBuilderException;

    /**
     * Build the final message.
     */
    MimeMessage buildMessage()
    throws SMIMEBuilderException, MessagingException;

    /**
     * If true, the deprecated S/MIME content-types x-pkcs7* headers will be used.
     */
    void setUseDeprecatedContentTypes(boolean useDeprecatedContentTypes);
    boolean isUseDeprecatedContentTypes();

    /**
     * The micalg version to use for S/MIME signed messages (RFC 3851, RFC 5751)
     */
    void setMicAlgStyle(MicAlgStyle micAlgStyle);
    MicAlgStyle getMicAlgStyle();
}
