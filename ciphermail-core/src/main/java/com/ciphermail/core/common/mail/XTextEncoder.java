/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */

package com.ciphermail.core.common.mail;

/**
 * Encoder/decoder for XTEXT according to RFC 1891
 *
 * "xtext" is formally defined as follows:
 *
 *    xtext = *( xchar / hexchar )
 *
 *    xchar = any ASCII CHAR between "!" (33) and "~" (126) inclusive,
 *         except for "+" and "=".
 *
 * ; "hexchar"s are intended to encode octets that cannot appear
 * ; as ASCII characters within an esmtp-value.
 *
 *    hexchar = ASCII "+" immediately followed by two upper case
 *         hexadecimal digits
 *
 */
public class XTextEncoder
{
    private static final char[] HEX_CHARS = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
        };

    private XTextEncoder() {
        // empty on purpose
    }


    /**
     * XTEXT encodes the given string.
     */
    public static String encode(String s)
    {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);

            if (c >= 128) {
                throw new IllegalArgumentException("Only ASCII characters are supported");
            }

            // check if encoding is required
            if (c < '!' || c > '~' || c == '+' || c == '=')
            {
                sb.append('+').append(HEX_CHARS[c >> 4]).append(HEX_CHARS[c & 0x0f]);
            }
            else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * XTEXT Decodes the given string.
     */
    public static String decode(String s)
    {
        if (s == null) {
            return null;
        }

        StringBuilder decoded = new StringBuilder();

        int i = 0;

        while (i < s.length())
        {
            char c = s.charAt(i);

            if (c == '+' && i + 2 < s.length())
            {
                // Check if it's an encoded character
                try {
                    int decodedChar = Integer.parseInt(s.substring(i + 1, i + 3), 16);

                    decoded.append((char) decodedChar);

                    i += 3; // Skip the encoded characters
                }
                catch (NumberFormatException e) {
                    // If parsing fails, treat it as a regular character
                    decoded.append(c);
                    i++;
                }
            }
            else {
                // Regular character
                decoded.append(c);
                i++;
            }
        }

        return decoded.toString();
    }
}
