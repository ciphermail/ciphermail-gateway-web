/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * MailRepositoryItem embeds a MIME message and some extra meta information.
 *
 * @author Martijn Brinkers
 *
 */
public interface MailRepositoryItem
{
    /**
     * Returns the unique ID of this item
     */
    UUID getID();

    /**
     * The name of the repository this item belongs to
     */
    String getRepository();

    /**
     * Returns the MIME message of this item.
     */
    MimeMessage getMimeMessage()
    throws MessagingException;

    /**
     * Writes the bytes of the MIME message to the output stream
     */
    void writeMessage(@Nonnull OutputStream outputStream)
    throws IOException;

    /**
     * Returns the message-id of the mime message.
     */
    String getMessageID();

    /**
     * Returns the subject of the mime message.
     */
    String getSubject();

    /**
     * The unfiltered (but decoded) FROM header of the message including user part. If the FROM header contains
     * multiple addresses, they will be comma separated.
     * <p>
     * Note: because the FROM header contains the user part and possibly multiple addresses, it cannot be used directly
     * for retrieving User objects. It should only be used for displaying purposes.
     */
    String getFromHeader();

    /**
     * Returns the envelope recipients of the message.
     */
    @Nonnull Collection<InternetAddress> getRecipients()
    throws AddressException;

    void setRecipients(Collection<InternetAddress> recipients)
    throws AddressException;

    /**
     * Gets the envelope sender of the message. Null if the sender is the null sender
     */
    InternetAddress getSender()
    throws AddressException;

    /**
     * Sets the envelope sender of the message. Can be null if the sender is the null sender
     */
    void setSender(InternetAddress sender)
    throws AddressException;

    /**
     * Returns the originator of the message. The originator can be for example the envelope sender or
     * the FROM header. The caller decides which email address is used for the originator.
     */
    InternetAddress getOriginator()
    throws AddressException;

    /**
     * Sets the originator of the message. The originator can be for example the envelope sender or
     * the FROM header. The caller decides which email address is used for the originator.
     */
    void setOriginator(InternetAddress originator)
    throws AddressException;

    /**
     * The remote IP address of the sender, or null if unknown.
     */
    String getRemoteAddress();

    /**
     * Sets the remote IP address of the sender.
     */
    void setRemoteAddress(String remoteAddress);

    /**
     * Date at which this item was created
     */
    Date getCreated();

    /**
     * Gets the date at which this item was last updated
     */
    Date getLastUpdated();

    /**
     * Sets the date at which this item was last updated
     */
    void setLastUpdated(Date date);

    /**
     * Gets additional general purpose storage which can be used to store additional data
     */
    byte[] getAdditionalData();

    /**
     * Sets additional general purpose storage which can be used to store additional data
     */
    void setAdditionalData(byte[] data);
}
