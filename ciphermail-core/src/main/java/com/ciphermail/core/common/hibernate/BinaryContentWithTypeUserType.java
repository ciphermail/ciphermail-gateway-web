/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.util.BinaryContentWithType;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.ValueAccess;
import org.hibernate.usertype.CompositeUserType;

import java.io.Serializable;

/**
 * Hibernate database type for @BinaryContentWithType
 *
 * <a href="https://docs.jboss.org/hibernate/orm/6.3/javadocs/org/hibernate/usertype/CompositeUserType.html#replace(J,J,java.lang.Object)">
 *     Javadoc of CompositeUserType</a>
 *
 */
public class BinaryContentWithTypeUserType implements CompositeUserType<BinaryContentWithType>
{
    /*
     * The column indexes
     */
    private static final int CONTENT_COLUMN_INDEX = 0;
    private static final int CONTENT_TYPE_COLUMN_INDEX = 1;

    public static final String CONTENT_COLUMN_NAME      = "content";
    public static final String CONTENT_TYPE_COLUMN_NAME = "contentType";

    public static class BinaryContentWithTypeMapper {
        // in sorted order
        byte[] content;
        String contentType;
    }

    @Override
    public Object getPropertyValue(BinaryContentWithType binaryContentWithType, int propertyIndex)
    {
        return switch (propertyIndex) {
            case CONTENT_COLUMN_INDEX -> binaryContentWithType.getContent();
            case CONTENT_TYPE_COLUMN_INDEX -> binaryContentWithType.getContentType();
            default -> null;
        };
    }

    @Override
    public BinaryContentWithType instantiate(ValueAccess valueAccess,
            SessionFactoryImplementor sessionFactoryImplementor)
    {
        return new BinaryContentWithType(valueAccess.getValue(
                CONTENT_COLUMN_INDEX, byte[].class),
                valueAccess.getValue(CONTENT_TYPE_COLUMN_INDEX, String.class));
    }

    @Override
    public Class<?> embeddable() {
        return BinaryContentWithTypeMapper.class;
    }

    @Override
    public Class<BinaryContentWithType> returnedClass() {
        return BinaryContentWithType.class;
    }

    @Override
    public boolean equals(BinaryContentWithType binaryContentWithType1, BinaryContentWithType binaryContentWithType2)
    {
        if (binaryContentWithType1 == binaryContentWithType2) {
            return true;
        }
        if (binaryContentWithType1 == null || binaryContentWithType2 == null) {
            return false;
        }

        return binaryContentWithType1.equals(binaryContentWithType2);
    }

    @Override
    public int hashCode(BinaryContentWithType binaryContentWithType) {
        return binaryContentWithType.hashCode();
    }

    @Override
    public BinaryContentWithType deepCopy(BinaryContentWithType binaryContentWithType) {
        return binaryContentWithType != null ? binaryContentWithType.clone() : null;
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(BinaryContentWithType binaryContentWithType) {
        return deepCopy(binaryContentWithType);
    }

    @Override
    public BinaryContentWithType assemble(Serializable cached, Object owner) {
        return deepCopy((BinaryContentWithType) cached);
    }

    @Override
    public BinaryContentWithType replace(BinaryContentWithType detached, BinaryContentWithType managed,
            Object owner)
    {
        // according to javadoc https://docs.jboss.org/hibernate/orm/6.3/javadocs/org/hibernate/usertype/CompositeUserType.html#replace(J,J,java.lang.Object)
        // ... for mutable objects, it is safe to return a copy of the first parameter
        return deepCopy(detached);
    }
}
