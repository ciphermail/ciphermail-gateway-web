/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.impl;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.properties.SMSProperties;
import com.ciphermail.core.app.properties.SMSPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.sms.SMS;
import com.ciphermail.core.common.sms.SMSExpiredListener;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.sms.SMSTransport;
import com.ciphermail.core.common.sms.SMSTransportFactory;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import com.ciphermail.core.common.sms.SortColumn;
import com.ciphermail.core.common.util.DateTimeUtils;
import com.ciphermail.core.common.util.MutableString;
import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.text.TextStringBuilder;
import org.hibernate.LockMode;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Implementation of the SMSGateway which uses a database to keep a queue of outgoing SMS.
 *
 * @author Martijn Brinkers
 *
 */
public class SMSGatewayImpl implements SMSGateway
{
    private static final Logger logger = LoggerFactory.getLogger(SMSGatewayImpl.class);

    /*
     * The default name of the transport thread.
     */
    private static final String DEFAULT_THREAD_NAME = "SMS transport";

    /*
     * The registry of all available SMS transports.
     */
    private final SMSTransportFactoryRegistry transportFactoryRegistry;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * Provides the global preferences
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Manager for Hibernate sessions
     */
    private final SessionManager sessionManager;

    /*
     * Used for executing database actions within a transaction
     */
    private final TransactionOperations transactionOperations;

    /*
     * Listener that's being called when a SMS has expired.
     */
    private final SMSExpiredListener expiredListener;

    /*
     * Name of the background thread
     */
    private String threadName = DEFAULT_THREAD_NAME;

    /*
     * Time (in milliseconds) the thread will wait at startup before the thread becomes active
     */
    private long startupDelayTime = DateUtils.MILLIS_PER_SECOND * 30;

    /*
     * Time (in milliseconds) the thread will sleep if there is no SMS to sent.
     */
    private long sleepTime = DateUtils.MILLIS_PER_MINUTE;

    /*
     * Time (in milliseconds) the thread will sleep if there was an error in the SMS background thread not caused
     * by an SMS transport.
     */
    private long sleepTimeOnError = DateUtils.MILLIS_PER_SECOND * 30;

    /*
     * How often (in milliseconds) should we try to send a failed SMS.
     */
    private long updateInterval = DateUtils.MILLIS_PER_MINUTE;

    /*
     * How long (in milliseconds) is an SMS valid.
     */
    private long expirationTime = DateUtils.MILLIS_PER_HOUR * 24;

    /*
     * The thread that will send the SMS messages.
     */
    private TransportThread transportThread;

    /*
     * Object used to notify the thread that it should wake-up.
     */
    private final Object wakeup = new Object();

    /*
     * If true, the thread will be asked to stop
     */
    private final AtomicBoolean stop = new AtomicBoolean(false);

    public SMSGatewayImpl(
            @Nonnull SMSTransportFactoryRegistry transportFactoryRegistry,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull SessionManager sessionManager,
            @Nonnull TransactionOperations transactionOperations,
            SMSExpiredListener expiredListener)
    {
        this.transportFactoryRegistry = Objects.requireNonNull(transportFactoryRegistry);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
        this.globalPreferencesManager = Objects.requireNonNull(globalPreferencesManager);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
        this.expiredListener = expiredListener;
    }

    public SMSGatewayImpl(
            @Nonnull SMSTransportFactoryRegistry transportFactoryRegistry,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry,
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull SessionManager sessionManager,
            @Nonnull TransactionOperations transactionOperations)
    {
        this(transportFactoryRegistry, userPropertiesFactoryRegistry, globalPreferencesManager, sessionManager,
                transactionOperations, null);
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(@Nonnull String threadName) {
        this.threadName = threadName;
    }

    /*
     * Time (in milliseconds) the thread will sleep if there was an error in the SMS background thread not caused
     * by an SMS transport.
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setStartupDelayTime(long startupDelayTime) {
        this.startupDelayTime = startupDelayTime;
    }

    /**
     * Sets the sleepTime (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    /**
     * Sets the sleepTimeOnError (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setSleepTimeOnError(long sleepTimeOnError) {
        this.sleepTimeOnError = sleepTimeOnError;
    }

    /**
     * Sets the expirationTime (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setExpirationTime(long expirationTime) {
        this.expirationTime = expirationTime;
    }

    /**
     * Sets the updateInterval (in milliseconds).
     * <p>
     * Note: should not be called after start has been called.
     */
    public void setUpdateInterval(long updateInterval) {
        this.updateInterval = updateInterval;
    }

    @Override
    public synchronized void start()
    {
        if (transportThread == null)
        {
            stop.set(false);

            transportThread = new TransportThread(threadName);

            transportThread.setDaemon(true);
            transportThread.start();
        }
    }

    @Override
    public synchronized void stop(long waitTime)
    throws InterruptedException
    {
        if (transportThread != null)
        {
            stop.set(true);

            synchronized (wakeup) {
                wakeup.notifyAll();
            }

            transportThread.join(waitTime);

            transportThread = null;
        }
    }

    private SMSGatewayDAO getDAO() {
        return new SMSGatewayDAO(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    @Override
    public SMS sendSMS(@Nonnull String phoneNumber, @Nonnull String message)
    {
        SMS newSMS = transactionOperations.execute(status -> getDAO().addSMS(phoneNumber, message));

        // Notify the transport thread that there is a new SMS so it can wake up if asleep.
        synchronized (wakeup) {
            wakeup.notifyAll();
        }

        return newSMS;
    }

    @Override
    public long getCount() {
        return Objects.requireNonNullElse(transactionOperations.execute(status -> getDAO()
                .getAvailableCount(null)), 0L);
    }

    @Override
    public SMS delete(UUID id)
    {
        return transactionOperations.execute(status ->
        {
            SMSGatewayDAO dao = getDAO();

            SMSGatewayEntity entity = dao.findById(id, SMSGatewayEntity.class);

            if (entity != null) {
                dao.delete(entity);
            }

            return entity;
        });
    }

    @Override
    public void deleteAll() {
        transactionOperations.executeWithoutResult(status -> getDAO().deleteAll());
    }

    @Override
    public List<SMS> getAll(final Integer firstResult, final Integer maxResults,
            final SortColumn sortColumn, final SortDirection sortDirection)
    {
        return transactionOperations.execute(status -> getDAO().getAll(
                firstResult, maxResults, sortColumn, sortDirection));
    }

    private class TransportThread extends Thread
    {
        public TransportThread(String threadName) {
            super(threadName);
        }

        @Override
        public void run()
        {
            // wait some time before starting the background-thread
            ThreadUtils.sleepQuietly(startupDelayTime);

            logger.info("SMS gateway thread started. Expiration time: {}, Sleep time: {}, Update interval: {}",
                    expirationTime, sleepTime, updateInterval);

            do {
                try {
                    if (stop.get()) {
                        break;
                    }

                    sendNextSMS();

                    handleExpiredSMS();

                    synchronized (wakeup)
                    {
                        // Sleep for some time if there is nothing to do.
                        if (getAvailableCount() == 0)
                        {
                            try {
                                wakeup.wait(sleepTime);
                            }
                            catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    logger.error("Error in SMS transport thread.", e);

                    // Sleep some time to prevent the logs from filling up
                    ThreadUtils.sleepQuietly(sleepTimeOnError);
                }
            }
            while(true);

            logger.info("SMS gateway thread stopped");
        }

        /*
         * Returns the date after which entries are not yet ready for sending (because they have been tried recently)
         */
        private Date getNotAfter() {
            return DateTimeUtils.addMilliseconds(new Date(), -updateInterval);
        }

        private SMSTransport getTransport()
        throws IOException
        {
            String defaultSMSTransportName;

            try {
                SMSProperties smsProperties = userPropertiesFactoryRegistry.getFactoryForClass(SMSPropertiesImpl.class)
                        .createInstance(globalPreferencesManager.getGlobalUserPreferences()
                                .getProperties());

                defaultSMSTransportName = smsProperties.getActiveSMSTransportName();
            }
            catch (HierarchicalPropertiesException e) {
                throw new IOException(e);
            }

            SMSTransportFactory factory = transportFactoryRegistry.getFactory(defaultSMSTransportName);

            if (factory == null)
            {
                throw new IOException("A transport factory with the name " +
                        defaultSMSTransportName + " cannot be found.");
            }

            return factory.createSMSTransport();
        }

        /*
         * If a clustered database is used, it can happen that multiple nodes try to send the same SMS at the same time.
         * To prevent this we need to get an SMS and set date last try to current date/time, the commit the transaction
         * and then try to send the SMS. And only if the SMS was successfully sent, delete the SMS.
         */
        private void sendNextSMS()
        {
            MutableString tagWrapper = new MutableString();

            SMSGatewayEntity sms = transactionOperations.execute(status ->
            {
                try {
                    Date notAfter = getNotAfter();

                    SMSGatewayEntity nextAvailable = getDAO().getNextAvailable(notAfter);

                    // Set nextAvailable to make sure another node will not try to send the same SMS
                    if (nextAvailable != null)
                    {
                        // Create a unique tag so we can detect whether the SMS entity was updated concurrently on another node
                        tagWrapper.setValue(UUID.randomUUID().toString());

                        nextAvailable.setDateLastTry(new Date());
                        nextAvailable.setTag(tagWrapper.getValue());

                        logger.debug("Next SMS available. ID: {}, Phone number: {}, Tag: {}",
                                nextAvailable.getID(), nextAvailable.getPhoneNumber(), tagWrapper.getValue());
                    }

                    return nextAvailable;
                }
                catch (Exception e) {
                    throw new UnhandledException(e);
                }
            });

            if (sms != null)
            {
                transactionOperations.executeWithoutResult(status ->
                {
                    SMSTransport transport = null;

                    SMSGatewayEntity reloadedSMS = null;

                    try {
                        SMSGatewayDAO dao = getDAO();

                        // Check if the SMS is still available
                        reloadedSMS = dao.findById(sms.getID(), SMSGatewayEntity.class);

                        if (reloadedSMS != null)
                        {
                            // Prevent the entity from being updated
                            sessionManager.getSession().lock(reloadedSMS, LockMode.PESSIMISTIC_WRITE);

                            logger.debug("SMS reloaded. ID: {}, Phone number: {}, Tag: {}",
                                    reloadedSMS.getID(), reloadedSMS.getPhoneNumber(), tagWrapper.getValue());

                            // Only send the SMS if the tag was not changed, i,e, if the SMS was not already
                            // handled by another node
                            if (tagWrapper.getValue().equals(reloadedSMS.getTag()))
                            {
                                transport = getTransport();

                                if (transport == null) {
                                    throw new IOException("SMS transport is not available");
                                }

                                transport.sendSMS(reloadedSMS.getPhoneNumber(), reloadedSMS.getMessage());

                                // The SMS has been sent and can be removed
                                TextStringBuilder sb = new TextStringBuilder(256);

                                sb.append("Successfully sent SMS with transport '");
                                sb.append(getTransport().getName());
                                sb.append("' to phone number '");
                                sb.append(reloadedSMS.getPhoneNumber());
                                sb.append("'");

                                logger.info("{}", sb);

                                getDAO().delete(reloadedSMS);
                            }
                            else {
                                logger.warn("Tag changed. The SMS was already handled by another node.");
                            }
                        }
                    }
                    catch (IOException | JSONException e)
                    {
                        TextStringBuilder sb = new TextStringBuilder(256);

                        sb.append("Error sending SMS with transport '");
                        sb.append(transport != null ? transport.getName() : "<unknown>");
                        sb.append("' to phone number '");
                        sb.append(sms.getPhoneNumber());
                        sb.append("'");

                        logger.error(sb.toString(), e);

                        if (reloadedSMS != null) {
                            reloadedSMS.setLastError(ExceptionUtils.getRootCauseMessage(e));
                        }
                    }
                });
            }
        }

        /*
         * Returns the number of SMS that are ready to be sent.
         */
        private long getAvailableCount()
        {
            return Objects.requireNonNullElse(transactionOperations.execute(status -> getDAO()
                    .getAvailableCount(getNotAfter())), 0L);
        }

        private void handleExpiredSMS()
        {
            Date now = new Date();

            Date olderThan = DateTimeUtils.addMilliseconds(now, -expirationTime);

            transactionOperations.executeWithoutResult(status ->
            {
                SMSGatewayDAO dao = getDAO();

                List<SMSGatewayEntity> expired = dao.getExpired(olderThan);

                if (!expired.isEmpty())
                {
                    for (SMSGatewayEntity smsEntity : expired)
                    {
                        TextStringBuilder sb = new TextStringBuilder(256);

                        sb.append("The SMS to phone number '");
                        sb.append(smsEntity.getPhoneNumber());
                        sb.append("' is expired");

                        logger.warn("{}", sb);

                        if (expiredListener != null) {
                            expiredListener.expired(SMSGatewayDAO.entityToSMS(smsEntity));
                        }

                        dao.delete(smsEntity);
                    }
                }
            });
        }
    }
}
