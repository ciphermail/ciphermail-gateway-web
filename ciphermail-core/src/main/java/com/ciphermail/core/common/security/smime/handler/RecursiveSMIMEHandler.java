/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.smime.SMIMEType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.util.Objects;

/**
 * RecursiveSMIMEHandler recursively handles S/MIME messages (i.e., it handles message with multiple S/MIME layers).
 *
 * @author Martijn Brinkers
 *
 */
public class RecursiveSMIMEHandler
{
    private static final Logger logger = LoggerFactory.getLogger(RecursiveSMIMEHandler.class);

    /*
     * Provides PKI related functionality
     */
    private final PKISecurityServices securityServices;

    /*
     * If true security info (signed and/or encrypted) will be added to the headers
     */
    private boolean removeSignature;

    /*
     * If true security info (signed and/or encrypted) will be added to the headers
     */
    private boolean addInfo = true;

    /*
     * sanity check to stop processing when a message contains too many parts
     */
    private int maxRecursion = 256;

    /*
     * If true and the message is encrypted the message will be decrypted
     */
    private boolean decrypt = true;

    /*
     * If true and the message is compressed the message will be decompressed
     */
    private boolean decompress = true;

    /*
     * If true the message will be checked for illegal characters after decryption (to prevent the EFAIL attack)
     */
    private boolean checkInvalid7BitChars;

    /*
     * If true and illegal characters are detected in the decrypted message (see checkInvalid7BitChars), decryption
     * will be aborted
     */
    private boolean abortDecryptionOnInvalid7BitChars;

    /*
     * Determines which headers will be encrypted and/or signed
     */
    private String[] protectedHeaders = ProtectedContentHeaderNameMatcher.DEFAULT_PROTECTED_CONTENT_HEADERS;

    /*
     * Listener which will be called with the certificates contained in the message
     */
    private CertificateCollectionEvent certificatesEventListener;

    /*
     * SMIMEInfoHandler will be used to add meta information about the S/MIME layers to the message
     */
    private SMIMEInfoHandler smimeInfoHandler;

    /*
     * The mail ID of the message (if set). This is purely used for logging
     */
    private String mailID;

    public RecursiveSMIMEHandler(@Nonnull PKISecurityServices securityServices)
    {
        this.securityServices = Objects.requireNonNull(securityServices);

        this.smimeInfoHandler = new SMIMEInfoHandlerImpl(securityServices);
    }

    /**
     * If true and the message was signed the signature will be removed.
     */
    public void setRemoveSignature(boolean removeSignature) {
        this.removeSignature = removeSignature;
    }

    public boolean isRemoveSignature() {
        return removeSignature;
    }

    public void setMaxRecursion(int maxRecursion) {
        this.maxRecursion = maxRecursion;
    }

    /**
     * If true S/MIME security info will be added to the resulting message.
     */
    public void setAddInfo(boolean addInfo) {
        this.addInfo = addInfo;
    }

    public boolean isAddInfo() {
        return addInfo;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    /**
     * If true and the message is encrypted the message will be decrypted
     */
    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public boolean isDecompress() {
        return decompress;
    }

    /**
     * If true and the message is compressed the message will be decompressed
     */
    public void setDecompress(boolean decompress) {
        this.decompress = decompress;
    }


    public boolean isCheckInvalid7BitChars() {
        return checkInvalid7BitChars;
    }

    /**
     * If true the message will be checked for illegal characters after decryption (to prevent the EFAIL attack)
     */
    public void setCheckInvalid7BitChars(boolean checkInvalid7BitChars) {
        this.checkInvalid7BitChars = checkInvalid7BitChars;
    }

    /**
     * If true and illegal characters are detected in the decrypted message (see checkInvalid7BitChars), decryption
     * will be aborted
     */
    public boolean isAbortDecryptionOnInvalid7BitChars() {
        return abortDecryptionOnInvalid7BitChars;
    }

    public void setAbortDecryptionOnInvalid7BitChars(boolean abortDecryptionOnInvalid7BitChars) {
        this.abortDecryptionOnInvalid7BitChars = abortDecryptionOnInvalid7BitChars;
    }

    /**
     * Listener which will be called with the certificates contained in the message
     */
    public void setCertificatesEventListener(CertificateCollectionEvent event) {
        this.certificatesEventListener = event;
    }

    public CertificateCollectionEvent getCertificatesEventListener() {
        return certificatesEventListener;
    }

    private SMIMEHandler createSMIMEHandler()
    {
        SMIMEHandler sMIMEHandler = new SMIMEHandler(securityServices);

        sMIMEHandler.setDecrypt(isDecrypt());
        sMIMEHandler.setDecompress(isDecompress());
        sMIMEHandler.setRemoveSignature(isRemoveSignature());
        sMIMEHandler.setCheckInvalid7BitChars(isCheckInvalid7BitChars());
        sMIMEHandler.setAbortDecryptionOnInvalid7BitChars(isAbortDecryptionOnInvalid7BitChars());
        sMIMEHandler.setCertificatesEventListener(getCertificatesEventListener());
        sMIMEHandler.setProtectedHeaders(getProtectedHeaders());
        sMIMEHandler.setMailID(getMailID());

        return sMIMEHandler;
    }

    private AttachedSMIMEHandler.PartHandler createAttachedPartHandler(int recursionCount)
    {
        return new AttachedPartHandler(recursionCount);
    }

    private MimeMessage internalHandlePart(Part source, int level, int recursionCount)
    throws SMIMEHandlerException
    {
        logger.debug("internalHandlePart");

        level++;
        recursionCount++;

        if (recursionCount > maxRecursion)
        {
            logger.warn("Maximum recursion depth reached.");

            return null;
        }

        SMIMEHandler sMIMEHandler = createSMIMEHandler();

        MimeMessage newMessage = sMIMEHandler.handlePart(source);

        if (newMessage != null)
        {
            if (addInfo)
            {
                // sanity check
                Objects.requireNonNull(smimeInfoHandler, "smimeInfoHandler");

                newMessage = smimeInfoHandler.handle(newMessage, sMIMEHandler,
                        level - 1 /* -1 because level starts with 1 */);
            }

            // at this point, sMIMEHandler.getSMIMEType() should never return NONE
            // because SMIMEHandler#handlePart only returns a MimeMessage if SMIMEType != NONE
            // i.e., the message was an S/MIME message (signed or compressed or encrypted)

            // check whether we need recursively handle the message. If the message
            // is signed but the signature is not removed we do not need to continue because
            // we cannot change the message. If the message was encrypted but the message
            // could not be decrypted there is no need to continue.
            if ((sMIMEHandler.getSMIMEType() == SMIMEType.ENCRYPTED && sMIMEHandler.isDecrypted()) ||
                (sMIMEHandler.getSMIMEType() == SMIMEType.AUTH_ENCRYPTED && sMIMEHandler.isDecrypted()) ||
                (sMIMEHandler.getSMIMEType() == SMIMEType.SIGNED && removeSignature) ||
                (sMIMEHandler.getSMIMEType() == SMIMEType.COMPRESSED))
            {
                MimeMessage recursiveMessage = internalHandlePart(newMessage, level, recursionCount);

                if (recursiveMessage != null)
                {
                    logger.debug("Recursive S/MIME message.");

                    newMessage = recursiveMessage;
                }
            }
        }
        else {
            // it's not an S/MIME message however, it can still contain S/MIME message if it's multipart/mixed
            // or rfc822 message
            try {
                if (source.isMimeType("multipart/mixed") || source.isMimeType("message/rfc822"))
                {
                    AttachedSMIMEHandler attachedHandler = new AttachedSMIMEHandler(createAttachedPartHandler(recursionCount));

                    newMessage = attachedHandler.handlePart(source);
                }
            }
            catch (MessagingException e) {
                throw new SMIMEHandlerException(e);
            }
        }

        return newMessage;
    }

    public MimeMessage handlePart(Part source)
    throws SMIMEHandlerException
    {
        return internalHandlePart(source, 0, 0);
    }

    private class AttachedPartHandler implements AttachedSMIMEHandler.PartHandler
    {
        private final int recursionCount;

        public AttachedPartHandler(int recursionCount) {
            this.recursionCount = recursionCount;
        }

        @Override
        public MimeMessage handlePart(Part source)
        throws SMIMEHandlerException
        {
            // nested S/MIME message should start with level 0
            return internalHandlePart(source, 0, recursionCount);
        }
    }

    public String[] getProtectedHeaders() {
        return protectedHeaders;
    }

    public void setProtectedHeaders(String[] protectedHeaders) {
        this.protectedHeaders = protectedHeaders;
    }

    public SMIMEInfoHandler getSmimeInfoHandler() {
        return smimeInfoHandler;
    }

    public void setSmimeInfoHandler(SMIMEInfoHandler smimeInfoHandler) {
        this.smimeInfoHandler = smimeInfoHandler;
    }

    public String getMailID() {
        return mailID;
    }

    public void setMailID(String mailID) {
        this.mailID = mailID;
    }
}
