/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import com.google.common.base.Splitter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MiscStringUtils
{
    private MiscStringUtils() {
        // empty on purpose
    }

    /**
     * returns true if the char c is not ascii
     */
    public static boolean isNotPrintableAscii(char c) {
        return c >= 127 || (c < 32 && c != '\r' && c != '\n' && c != '\t');
    }

    /**
     * returns true if the char c is ascii or CR/LF/TAB.
     */
    public static boolean isPrintableAscii(char c) {
        return !isNotPrintableAscii(c);
    }

    /**
     * returns true if all the characters in the string are ascii or CR/LF/TAB.
     */
    public static boolean isPrintableAscii(String s)
    {
        if (StringUtils.isEmpty(s)) {
            return true;
        }

        for (int i = 0; i < s.length(); i++)
        {
            if (isNotPrintableAscii(s.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Converts the input to US-ASCII
     */
    public static String convertToASCII(String input)
    {
        byte[] b = getBytesASCII(input);

        return b != null ? new String(b, StandardCharsets.US_ASCII) : null;
    }

    /**
     * Converts the specified string to byte array of ASCII characters.
     *
     * @param data the string to be encoded
     * @return byte array containing the encoded ascii string.
     */
    public static byte[] getBytesASCII(String data)
    {
        if (data == null) {
            return null;
        }

        return data.getBytes(StandardCharsets.US_ASCII);
    }

    /**
     * Converts the byte array of ASCII characters to a string.
     *
     * @param data the byte array to be encoded
     * @return The string containing ascii characters
     */
    public static String toStringFromASCIIBytes(byte[] data)
    {
        if (data == null) {
            return null;
        }

        return toStringFromASCIIBytes(data, 0, data.length);
    }

    /**
     * Converts the byte array of ASCII characters to a string
     *
     * @param data the byte array to be encoded
     * @param offset the first byte to encode
     * @param length bytes to encode
     * @return The string converted string
     */
    public static String toStringFromASCIIBytes(byte[] data, int offset, int length)
    {

        if (data == null) {
            return null;
        }

        return new String(data, offset, length, StandardCharsets.US_ASCII);
    }

    /**
     * Converts the specified string to byte array of UTF-8 characters.
     *
     * @param data the string to be encoded
     * @return byte array containing the encoded UTF-8 string.
     */
    public static byte[] getBytesUTF8(String data)
    {
        if (data == null) {
            return null;
        }

        return data.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Converts the byte array with UTF-8 encoded string to a string.
     *
     * @param data the byte array to be encoded
     * @return The string containing ascii characters
     */
    public static String toStringFromUTF8Bytes(byte[] data)
    {
        if (data == null) {
            return null;
        }

        return toStringFromUTF8Bytes(data, 0, data.length);
    }

    /**
     * Converts the byte array with UTF-8 encoded string to a string.
     *
     * @param data the byte array to be encoded
     * @param offset the first byte to encode
     * @param length bytes to encode
     * @return The string converted string
     */
    public static String toStringFromUTF8Bytes(byte[] data, int offset, int length)
    {

        if (data == null) {
            return null;
        }

        return new String(data, offset, length, StandardCharsets.UTF_8);
    }

    /**
     * Removes control characters from the input string
     */
    public static String removeControlChars(String input)
    {
        if (input == null) {
            return null;
        }

        char[] chars = new char[input.length()];

        int index = 0;

        for (int i = 0; i < input.length(); i++)
        {
            char c = input.charAt(i);

            if (c >= ' ')
            {
                chars[index] = c;

                index++;
            }
        }

        return new String(chars, 0, index);
    }

    /**
     * If the input string starts and ends with quoteChar the starting and ending
     * quoteChar will be removed. The input string is not trimmed so any leading
     * or ending white spaces will result in the string not to be unquoted.
     * @param input the String to unquote
     * @param quoteChar the quote chat
     * @return the unquoted string
     */
    public static String unquote(String input, char quoteChar)
    {
        if (input == null) {
            return null;
        }

        // if length < 2 the input cannot be quoted
        if (input.length() < 2) {
            return input;
        }

        String unquoted = input;

        if (isQuoted(input, quoteChar)) {
            unquoted = input.substring(1, input.length() - 1);
        }

        return unquoted;
    }

    public static boolean isQuoted(String input, char quoteChar)
    {
        if (input == null) {
            return false;
        }

        // if length < 2 the input cannot be quoted
        if (input.length() < 2) {
            return false;
        }

        return input.charAt(0) == quoteChar && input.charAt(input.length() - 1) == quoteChar;
    }

    /**
     * Restricts the length of the input string to a maximum length.
     */
    public static String restrictLength(String input, int maxLength) {
        return restrictLength(input, maxLength, false);
    }

    /**
     * Restricts the length of the input string to a maximum length. If addDots is true ... will be added if the string
     * is too long. The final length will always be smaller or equal to maxLength (ie the dots do not make it any longer)
     */
    public static String restrictLength(String input, int maxLength, boolean addDots, String dots)
    {
        if (input == null) {
            return null;
        }

        if (maxLength < 0) {
            maxLength = 0;
        }

        if (input.length() > maxLength)
        {
            // reduce length if we add the dots
            if (addDots)
            {
                maxLength = maxLength - StringUtils.length(dots);

                if (maxLength < 0) {
                    maxLength = 0;
                }
            }

            input = input.substring(0, maxLength);

            if (addDots) {
                input = input + StringUtils.defaultString(dots);
            }
        }

        return input;
    }

    /**
     * Restricts the length of the input string to a maximum length. If addDots is true ... will be added if the string
     * is too long. The final length will always be smaller or equal to maxLength (ie the dots do not make it any longer)
     * There is one exception when the size < 3 and addDots is true.
     */
    public static String restrictLength(String input, int maxLength, boolean addDots) {
        return restrictLength(input, maxLength, addDots, "...");
    }

    /**
     * If input does not end with endsWith it will append endsWith and return the result. If input does end with
     * endsWith input will be returned.
     */
    public static String ensureEndsWith(String input, String endsWith)
    {
        return StringUtils.endsWith(input, endsWith) ? StringUtils.defaultString(input) :
            StringUtils.defaultString(input) + StringUtils.defaultString(endsWith);
    }

    /**
     * Replaces the last chars with replaceCharWith
     *
     * Example:
     *
     * replaceLastChars("abc123", 3, "mn") returns "abcmnmnmn"
     */
    public static String replaceLastChars(String input, int lastChars, String replaceCharWith)
    {
        input = StringUtils.defaultString(input);

        int maxLength = input.length() - lastChars;

        if (maxLength < 0) {
            maxLength = 0;
        }

        String dots = StringUtils.repeat(StringUtils.defaultString(replaceCharWith),
                input.length() - maxLength);

        return StringUtils.substring(input, 0, maxLength) + dots;
    }

    /**
     * Returns true if at least one of the lines contains the string.
     */
    public static boolean contains(String str, Collection<String> lines) {
        return containsIndex(str, lines) != -1;
    }

    /**
     * Returns true if at least one of the lines contains the string.
     */
    public static boolean contains(String str, String[] lines) {
        return containsIndex(str, lines) != -1;
    }

    /**
     * Returns the index if the line if the line contains str. Returns -1 if no line contains the str
     */
    public static int containsIndex(String str, Collection<String> lines)
    {
        if (lines == null) {
            return -1;
        }

        int i = 0;

        for (String line : lines)
        {
            if (StringUtils.contains(line, str)) {
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Returns the index if the line if the line contains str. Returns -1 if no line contains the str
     */
    public static int containsIndex(String str, String[] lines)
    {
        if (lines == null) {
            return -1;
        }

        int i = 0;

        for (String line : lines)
        {
            if (StringUtils.contains(line, str)) {
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Returns true if at least one line matches the regular expression.
     */
    public static boolean containsRegEx(@Nonnull String regex, Collection<String> lines) {
        return containsRegExIndex(regex, lines) != -1;
    }

    /**
     * Returns true if at least one line matches the regular expression.
     */
    public static boolean containsRegEx(@Nonnull String regex, String[] lines) {
        return containsRegExIndex(regex, lines) != -1;
    }

    /**
     * Returns the index if the line if the line matches the regular expression. Returns -1 if no line matches
     * the regular expression
     */
    public static int containsRegExIndex(@Nonnull String regex, Collection<String> lines)
    {
        if (lines == null) {
            return -1;
        }

        Pattern pattern = Pattern.compile(regex);

        int i = 0;

        for (String line : lines)
        {
            Matcher m = pattern.matcher(line);

            if (m.find()) {
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Returns the index if the line if the line matches the regular expression. Returns -1 if no line matches
     * the regular expression
     */
    public static int containsRegExIndex(@Nonnull String regex, String[] lines)
    {
        if (lines == null) {
            return -1;
        }

        Pattern pattern = Pattern.compile(regex);

        int i = 0;

        for (String line : lines)
        {
            Matcher m = pattern.matcher(line);

            if (m.find()) {
                return i;
            }
            i++;
        }

        return -1;
    }

    /**
     * Splits the string into chunks of strings of max length separated by the provided separator. The separator
     * is added to the chunk after chunking, i.e., the length of a section is the length of the chunk size plus the
     * length of the separator.
     *
     * Note: existing CR/LF characters are not removed prior to chunking, i.e., a CR and LF characters are also
     * counted.
     */
    public static String splitStringIntoFixedSizeChunks(String input, int chunkSize, String separator)
    {
        if (input == null) {
            return null;
        }

        Iterable<String> chunks = Splitter.fixedLength(chunkSize).split(input);

        StrBuilder sb = new StrBuilder();

        for (String chunk : chunks) {
            sb.appendSeparator(separator).append(chunk);
        }

        return sb.toString();
    }

    /**
     * Splits the string into chunks of strings of max length separated by LF. The separator
     * is added to the chunk after chunking, i.e., the length of a section is the length of the chunk size plus the
     * length of the separator.
     *
     * Note: existing CR/LF characters are not removed prior to chunking, i.e., a CR and LF characters are also
     * counted.
     */
    public static String splitStringIntoFixedSizeChunks(String input, int chunkSize) {
        return splitStringIntoFixedSizeChunks(input, chunkSize, "\n");
    }

    /**
     * Splits the string into chunks of strings of max length 76 separated by LF. The separator
     * is added to the chunk after chunking, i.e., the length of a section is the length of the chunk size plus the
     * length of the separator.
     *
     * Note: existing CR/LF characters are not removed prior to chunking, i.e., a CR and LF characters are also
     * counted.
     */
    public static String splitStringIntoFixedSizeChunks(String input) {
        return splitStringIntoFixedSizeChunks(input, 76);
    }
}
