/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import com.ciphermail.core.app.properties.UserPropertyRegistry;
import org.apache.commons.lang.UnhandledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Global registry of {@link DefaultPropertyProvider}'s
 */
public class DefaultPropertyProviderRegistryImpl implements DefaultPropertyProviderRegistry
{
    private static final Logger logger = LoggerFactory.getLogger(DefaultPropertyProviderRegistryImpl.class);

    /*
     * All the registered DefaultPropertyProvider's
     */
    private final List<DefaultPropertyProvider> providers = new LinkedList<>();

    /*
     * Set of properties for which a null default value is allowed
     */
    private final Set<String> allowedNullDefaultValue = new HashSet<>();

    public DefaultPropertyProviderRegistryImpl(
            @Nonnull ApplicationContext applicationContext,
            @Nonnull Environment environment,
            @Nonnull List<String> packages,
            @Nonnull UserPropertyRegistry userPropertyRegistry)
    {
        try {
            HashSet<Class<?>> defaultPropertyProviderClasses = new HashSet<>();

            for (String basePackage : packages) {
                defaultPropertyProviderClasses.addAll(findDefaultPropertyProviderClasses(environment, basePackage));
            }

            for (Class<?> clazz : defaultPropertyProviderClasses)
            {
                DefaultPropertyProvider defaultPropertyProvider = (DefaultPropertyProvider) clazz
                        .getDeclaredConstructor().newInstance();

                // inject dependencies
                applicationContext.getAutowireCapableBeanFactory().autowireBean(defaultPropertyProvider);

                providers.add(defaultPropertyProvider);
            }

            // load the list of properties which are allowed to have a null factory property
            Collection<UserPropertyRegistry.UserPropertyDescriptor> propertyDescriptors =
                    userPropertyRegistry.getProperties().values();

            for (UserPropertyRegistry.UserPropertyDescriptor propertyDescriptor : propertyDescriptors)
            {
                if (propertyDescriptor.getter().allowNull()) {
                    allowedNullDefaultValue.add(propertyDescriptor.getter().name());
                }
                // allowNull on the setter will be ignored,
                else if (propertyDescriptor.setter().allowNull()) {
                    logger.warn("allowNull on a setter is ignored");
                }
            }
        }
        catch (IOException | NoSuchMethodException | InstantiationException | IllegalAccessException |
               InvocationTargetException e)
        {
            throw new UnhandledException(e);
        }
    }

    private HashSet<Class<?>> findDefaultPropertyProviderClasses(Environment environment, String basePackage)
    throws IOException
    {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

        HashSet<Class<?>> defaultPropertyProviderClasses = new HashSet<>();

        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                                   resolveBasePackage(basePackage) + "/**/*.class";

        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);

        for (Resource resource : resources)
        {
            if (resource.isReadable())
            {
                try {
                    MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);

                    if (isDefaultPropertyProvider(environment, metadataReader))
                    {
                        logger.info("Adding DefaultPropertyProvider {}",
                                metadataReader.getClassMetadata().getClassName());

                        defaultPropertyProviderClasses.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                    }
                }
                catch (Exception e) {
                    logger.error("Error getting class", e);
                }
            }
        }

        return defaultPropertyProviderClasses;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    private boolean isDefaultPropertyProvider(Environment environment, MetadataReader metadataReader)
    {
        try {
            ClassMetadata classMetadata = metadataReader.getClassMetadata();

            if (!classMetadata.isConcrete()) {
                // Only concrete implementations can be used
                return false;
            }

            Class<?> c = Class.forName(classMetadata.getClassName());

            // Only accept if it is a DefaultPropertyProvider
            if (DefaultPropertyProvider.class.isAssignableFrom(c))
            {
                // now check if there is a Profile annotation and if so, whether it contains the active profiles
                Profile profile = c.getAnnotation(Profile.class);

                return profile == null || environment.acceptsProfiles(Profiles.of(profile.value()));
            }
        }
        catch(Exception e) {
            logger.error("Error checking whether class is-a DefaultPropertyProvider", e);
        }

        return false;
    }

    /**
     * Returns a non-null value if there is a default value for the property. Returns null
     * if there is no default value
     */
    @Override
    public String getDefaultValue(@Nonnull HierarchicalProperties properties, @Nonnull String property)
    throws HierarchicalPropertiesException
    {
        String value = null;

        for (DefaultPropertyProvider provider : providers)
        {
            value = provider.getDefaultValue(properties, property);

            if (value != null) {
                break;
            }
        }

        return value;
    }

    @Override
    public boolean isNullDefaultValueAllowed(@Nonnull String property) {
        return allowedNullDefaultValue.contains(property);
    }
}
