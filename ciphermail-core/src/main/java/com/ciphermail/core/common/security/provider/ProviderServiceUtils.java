/*
 * Copyright (c) 2012-2019, CipherMail.
 *
 * This file is part of CipherMail.
 */
package com.ciphermail.core.common.security.provider;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Provider;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Crypto providers do not allow additional aliases to be added since it's implemented with services.
 * Aliases added with put will not be used since the service takes precedence. We will therefore
 * need to add the aliases to the service. This however is not allowed since the required methods are private.
 * This helper class will allow aliases to be set using introspection.
 *
 * @author Martijn Brinkers
 *
 */
public class ProviderServiceUtils
{
    private static final Logger logger = LoggerFactory.getLogger(ProviderServiceUtils.class);

    /*
     * Pattern that parses the alias input. Example: Mac.OID.1.3.6.1.5.5.8.1.1=HmacMD5 is split into groups
     * Mac, OID.1.3.6.1.5.5.8.1.1 and HmacMD5.
     */
    private static final Pattern PATTERN = Pattern.compile("([^.]+)\\.([^=]+)=(.+)");

    /*
     * Method for adding an alias to a Service
     */
    private final Method addAliasMethod;

    /*
     * Method for putting a service to a Provider
     */
    private final Method putServiceMethod;

    /*
     * The alias of a service type and algorithm
     */
    static class Alias
    {
        /*
         * The type of the service (for example MessageDigest or Signature)
         */
        private final String type;

        /*
         * The algorithm of the service (for example SHA1)
         */
        private final String algorithm;

        /*
         * The alias
         */
        private final String alias;

        Alias(@Nonnull String type, @Nonnull String algorithm, @Nonnull String alias)
        {
            this.type = Objects.requireNonNull(type).trim();
            this.algorithm = Objects.requireNonNull(algorithm).trim();
            this.alias = Objects.requireNonNull(alias).trim();
        }

        public String getType() {
            return type;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public String getAlias() {
            return alias;
        }
    }

    /*
     * The service type and algorithm
     */
    static class Service
    {
        /*
         * The type of the service (for example MessageDigest or Signature)
         */
        private final String type;

        /*
         * The algorithm of the service (for example SHA1)
         */
        private final String algorithm;

        /*
         * The class
         */
        private final String clazz;

        Service(@Nonnull String type, @Nonnull String algorithm, @Nonnull String clazz)
        {
            this.type = Objects.requireNonNull(type).trim();
            this.algorithm = Objects.requireNonNull(algorithm).trim();
            this.clazz = Objects.requireNonNull(clazz).trim();
        }

        public String getType() {
            return type;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public String getClazz() {
            return clazz;
        }
    }

    public ProviderServiceUtils()
    {
        try {
            //  Provider.Service.addAlias() is private, make it accessible through reflection
            addAliasMethod = Provider.Service.class.getDeclaredMethod("addAlias", String.class);
            addAliasMethod.setAccessible(true);

            //  Provider.putService() is private, make it accessible through reflection
            putServiceMethod = Provider.class.getDeclaredMethod("putService", Provider.Service.class);
            putServiceMethod.setAccessible(true);
        }
        catch (SecurityException | NoSuchMethodException e) {
            throw new UnhandledException(e);
        }
    }

    private void addAliases(Provider provider, List<Alias> aliases)
    throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        for (Alias alias : aliases)
        {
            Provider.Service service = provider.getService(alias.getType(), alias.getAlgorithm());

            if (service != null)
            {
                logger.debug("Adding alias {} for type {} with algorithm {}", alias.getAlias(),
                        alias.getType(), alias.getAlgorithm());

                addAliasMethod.invoke(service, alias.getAlias());

                putServiceMethod.invoke(provider, service);
            }
            else {
                logger.warn("Service for type {} and algorithm {}, not found", alias.getType(), alias.getAlgorithm());
            }
        }
    }

    private void addServices(Provider provider, List<Service> services)
    throws IllegalArgumentException, IllegalAccessException, InvocationTargetException
    {
        for (Service service : services)
        {
            logger.debug("Adding service clas {} for type {} with algorithm {}",
                    service.getClazz(), service.getType(), service.getAlgorithm());

            Provider.Service provService = new Provider.Service(provider, service.getType(), service.getAlgorithm(),
                  service.getClazz(), null, null);

            putServiceMethod.invoke(provider, provService);
        }
    }

    private List<Alias> parseAliases(String[] input)
    {
        List<Alias> aliases = new LinkedList<>();

        for (String alias : input)
        {
            if (StringUtils.isBlank(alias)) {
                continue;
            }

            Matcher m = PATTERN.matcher(alias);

            if (!m.matches()) {
                throw new IllegalArgumentException(alias + " is not a valid alias");
            }

            aliases.add(new Alias(
                    m.group(1) /* type */,
                    m.group(3) /* algorithm */,
                    m.group(2) /* alias */ ));
        }

        return aliases;
    }

    private List<Service> parseServices(String[] input)
    {
        List<Service> services = new LinkedList<>();

        for (String service : input)
        {
            if (StringUtils.isBlank(service)) {
                continue;
            }

            Matcher m = PATTERN.matcher(service);

            if (!m.matches()) {
                throw new IllegalArgumentException(service + " is not a valid service");
            }

            services.add(new Service(
                    m.group(1) /* type */,
                    m.group(2) /* algorithm */,
                    m.group(3) /* class */ ));
        }

        return services;
    }

    private void addAliases(Provider provider, String[] input)
    throws IllegalArgumentException
    {
        if (input == null) {
            return;
        }

        List<Alias> aliases = parseAliases(input);

        try {
            addAliases(provider, aliases);
        }
        catch (IllegalAccessException | InvocationTargetException e) {
            throw new UnhandledException(e);
        }
    }

    private void addServices(Provider provider, String[] input)
    throws IllegalArgumentException
    {
        if (input == null) {
            return;
        }

        List<Service> services = parseServices(input);

        try {
            addServices(provider, services);
        }
        catch (IllegalAccessException | InvocationTargetException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * Adds the aliases to the provider.
     */
    public static void addAliasesToProvider(String[] aliases, Provider provider)
    {
        ProviderServiceUtils injector = new ProviderServiceUtils();

        injector.addAliases(provider, aliases);
    }

    /**
     * Adds the aliases to the provider.
     */
    public static void addServicesToProvider(String[] services, Provider provider)
    {
        ProviderServiceUtils injector = new ProviderServiceUtils();

        injector.addServices(provider, services);
    }
}
