/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import java.util.List;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.QuotedPrintableCodec;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateModelException;
import com.ciphermail.core.common.locale.CharacterEncoding;

/**
 * A function for Freemarker that allows you to encode a string to quoted-printable
 *
 * @author Martijn Brinkers
 *
 */
public class QuotedPrintableEncodeMethod extends TemplateMethodModelBase
{
    /*
     * Freemarker method name
     */
    public static final String METHOD_NAME = "qp";

    @Override
    public Object exec(@SuppressWarnings("rawtypes") List arguments)
    throws TemplateModelException
    {
        checkMethodArgCount(arguments, 1, 2);

        try {
            String input = argToString(arguments.get(0));

            String charset = arguments.size() == 2 ? argToString(arguments.get(1)) : CharacterEncoding.UTF_8;

            QuotedPrintableCodec codec = new QuotedPrintableCodec(charset);

            return new SimpleScalar(codec.encode(input));
        }
        catch (EncoderException e) {
            throw new TemplateModelException("Quoted Printable encoding failed", e);
        }
    }
}