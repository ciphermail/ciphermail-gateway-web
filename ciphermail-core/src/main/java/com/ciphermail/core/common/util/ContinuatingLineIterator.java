/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * LineIterator which support continuation lines. Lines that start with whitespace are considered to be a
 * continuation of the previous line
 *
 * @author Martijn Brinkers
 *
 */
public class ContinuatingLineIterator implements Iterator<String>
{
    private final BufferedReader reader;

    /*
     * The last read line
     */
    private String activeLine;

    private StringBuilder cachedLine;

    public ContinuatingLineIterator(@Nonnull Reader reader)
    {
        this.reader =  (Objects.requireNonNull(reader) instanceof BufferedReader bufferedReader) ?
                bufferedReader : new BufferedReader(reader);
    }

    private StringBuilder safeGetCachedLine() {
        return cachedLine != null ? cachedLine : (cachedLine = new StringBuilder());
    }

    @Override
    public boolean hasNext()
    {
        if (activeLine != null) {
            return true;
        }

        try {
            String line;

            do {
                line = reader.readLine();

                if (line != null)
                {
                    if (cachedLine != null && (StringUtils.isBlank(line) ||
                            (line.charAt(0) != ' ' && line.charAt(0) != '\t')))
                    {
                        // We have something cached and this is not a continuation line
                        activeLine = cachedLine.toString();

                        cachedLine = null;
                    }

                    safeGetCachedLine().append(line);
                }
            }
            while (line != null && activeLine == null);

            if (line == null)
            {
                activeLine = cachedLine != null ? cachedLine.toString() : null;

                cachedLine = null;
            }

            return activeLine != null;
        }
        catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public String next() {
        return nextLine();
    }

    public String nextLine()
    {
        if(!hasNext()) {
            throw new NoSuchElementException("No more lines");
        }

        String currentLine = activeLine;

        activeLine = null;

        return currentLine;
    }

    @Override
    public void remove() {
        throw new IllegalStateException("remove is not supported");
    }

    /**
     * Closes the underlying reader
     */
    public void close() {
        IOUtils.closeQuietly(reader);
    }
}
