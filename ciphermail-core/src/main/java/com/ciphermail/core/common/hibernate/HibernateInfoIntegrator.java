/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.spi.BootstrapContext;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;

/**
 * Hibernate helper (spi) which stores some meta information from the hibernate instance which is not available in
 * some other way (we need the Metadata when we want to generate the DDL)
 */
public class HibernateInfoIntegrator implements org.hibernate.integrator.spi.Integrator
{
    private final HibernateInfo hibernateInfo;

    public HibernateInfoIntegrator(HibernateInfo hibernateInfo) {
        this.hibernateInfo = hibernateInfo;
    }

    @Override
    public void integrate(
            @NonNull Metadata metadata,
            @NonNull BootstrapContext bootstrapContext,
            @NonNull SessionFactoryImplementor sessionFactory)
    {
        hibernateInfo.setMetadata(metadata);
    }

    @Override
    public void disintegrate(@NonNull SessionFactoryImplementor sessionFactory,
            @NonNull SessionFactoryServiceRegistry serviceRegistry)
    {
        // Empty on purpose
    }
}
