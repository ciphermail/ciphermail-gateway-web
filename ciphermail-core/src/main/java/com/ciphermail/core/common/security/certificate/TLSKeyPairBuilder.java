/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

public class TLSKeyPairBuilder
{
    public enum Algorithm {
        RSA_2048  ("RSA", 2048, null),
        RSA_3072  ("RSA", 3072, null),
        RSA_4096  ("RSA", 4096, null),
        SECP521R1 ("EC", 521, "secp521r1"),
        SECP384R1 ("EC", 384, "secp384r1"),
        SECP256R1 ("EC", 256, "secp256r1");

        private final String jcaAlgorithm;
        private final int keySize;
        private final String parameterSpec;

        Algorithm(String jcaAlgorithm, int keySize, String parameterSpec)
        {
            this.jcaAlgorithm = jcaAlgorithm;
            this.keySize = keySize;
            this.parameterSpec = parameterSpec;
        }

        public String getJcaAlgorithm() {
            return jcaAlgorithm;
        }

        public int getKeySize() {
            return keySize;
        }

        public String getParameterSpec() {
            return parameterSpec;
        }
    }

    /*
     * The algorithm for the generated keypair
     */
    private Algorithm keyAlgorithm = Algorithm.SECP256R1;

    TLSKeyPairBuilder() {
        // use CSRBuilder#getInstance
    }

    public static TLSKeyPairBuilder getInstance() {
        return new TLSKeyPairBuilder();
    }

    public KeyPair buildKeyPair()
    throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        SecureRandom randomSource = securityFactory.createSecureRandom();

        KeyPair keyPair;

        switch (keyAlgorithm.getJcaAlgorithm()) {
            case "RSA" -> {
                // use provider which allows the key to be exported
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA",
                        securityFactory.getNonSensitiveProvider());

                keyPairGenerator.initialize(keyAlgorithm.getKeySize(), randomSource);

                keyPair = keyPairGenerator.generateKeyPair();
            }
            case "EC" -> {
                ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec(keyAlgorithm.getParameterSpec());

                // use provider which allows the key to be exported
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC",
                        securityFactory.getNonSensitiveProvider());

                keyPairGenerator.initialize(parameterSpec, randomSource);

                keyPair = keyPairGenerator.generateKeyPair();
            }
            default -> throw new IllegalArgumentException("Unsupported algorithm " + keyAlgorithm.getJcaAlgorithm());
        }

        return keyPair;
    }

    public Algorithm getKeyAlgorithm() {
        return keyAlgorithm;
    }

    public TLSKeyPairBuilder setKeyAlgorithm(Algorithm keyAlgorithm) {
        this.keyAlgorithm = keyAlgorithm;
        return this;
    }
}
