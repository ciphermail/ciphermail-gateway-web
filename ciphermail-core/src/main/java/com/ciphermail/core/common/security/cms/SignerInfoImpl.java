/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import com.ciphermail.core.common.security.asn1.ASN1Utils;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.smime.SMIMEAttributeUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.SystemUtils;
import org.bouncycastle.asn1.ASN1Encoding;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.PublicKey;
import java.util.Date;

public class SignerInfoImpl implements SignerInfo
{
    /*
     * DLEncodedSignerInformation returns the signerAttributes with definite length encoding (DL) instead of DER
     * encoded. The reason for this is that some old non-standard S/MIME clients generate signed attributes with
     * DL encoding whereas it should be DER encoding. This is a workaround for those faulty clients.
     */
    private static class DLEncodedSignerInformation extends SignerInformation
    {
        protected DLEncodedSignerInformation(@Nonnull SignerInformation baseInfo) {
            super(baseInfo);
        }

        @Override
        public byte[] getEncodedSignedAttributes()
        throws IOException
        {
            if (signedAttributeSet != null) {
                return signedAttributeSet.getEncoded(ASN1Encoding.DL);
            }

            return null;
        }
    }

    /*
     * If true, the DLEncodedSignerInformation workaround is used
     */
    private static boolean dlEncodeSignedAttributes;

    static
    {
        dlEncodeSignedAttributes = BooleanUtils.toBoolean(
                System.getProperty("ciphermail.cms.dl-encode-signed-attributes"));
    }

    /*
     * SignerInfo block from a CMS Signed message
     */
    private final SignerInformation signerInformation;

    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    public SignerInfoImpl(@Nonnull SignerInformation signerInformation, String nonSensitiveProvider,
            String sensitiveProvider)
    {
        this.signerInformation = dlEncodeSignedAttributes ?
            new DLEncodedSignerInformation(signerInformation) :
            signerInformation;

        this.nonSensitiveProvider = nonSensitiveProvider;

        // Note: nonSensitiveProvider is currently not used
    }

    @Override
    public int getVersion() {
        return signerInformation.getVersion();
    }

    @Override
    public SignerIdentifier getSignerId()
    throws IOException
    {
        SignerId id = signerInformation.getSID();

        return new SignerIdentifierImpl(X500PrincipalUtils.fromX500Name(id.getIssuer()), id.getSerialNumber(),
                id.getSubjectKeyIdentifier());
    }

    @Override
    public String getDigestAlgorithmOID() {
        return signerInformation.getDigestAlgOID();
    }

    @Override
    public byte[] getDigestAlgorithmParams() {
        return signerInformation.getDigestAlgParams();
    }

    @Override
    public String getEncryptionAlgorithmOID() {
        return signerInformation.getEncryptionAlgOID();
    }

    @Override
    public byte[] getEncryptionAlgorithmParams() {
        return signerInformation.getEncryptionAlgParams();
    }

    @Override
    public AttributeTable getSignedAttributes() {
        return signerInformation.getSignedAttributes();
    }

    @Override
    public AttributeTable getUnsignedAttributes() {
        return signerInformation.getUnsignedAttributes();
    }

    @Override
    public Date getSigningTime() {
        return SMIMEAttributeUtils.getSigningTime(getSignedAttributes());
    }

    @Override
    public boolean verify(@Nonnull PublicKey key, String provider)
    throws SignerInfoException
    {
        try {
            JcaSimpleSignerInfoVerifierBuilder verifierBuilder = new JcaSimpleSignerInfoVerifierBuilder();

            verifierBuilder.setProvider(provider);

            return signerInformation.verify(verifierBuilder.build(key));
        }
        catch (CMSException | OperatorCreationException e) {
            throw new SignerInfoException(e);
        }
    }

    @Override
    public boolean verify(@Nonnull PublicKey key)
    throws SignerInfoException
    {
        return verify(key, nonSensitiveProvider);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        String digestName = getDigestAlgorithmOID();

        Digest digest = Digest.fromOID(digestName);

        if (digest != null) {
            digestName = digest.toString();
        }

        Date signingTime = getSigningTime();

        sb.append("CMS Version: ");
        sb.append(getVersion());
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("*** [SignerId] ***");
        sb.append(SystemUtils.LINE_SEPARATOR);

        try {
            sb.append(getSignerId());
        }
        catch (IOException e) {
            sb.append("Error getting signer Id. Message: ").append(e.getMessage());
        }

        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Digest: ");
        sb.append(digestName);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Encryption alg. OID: ");
        sb.append(getEncryptionAlgorithmOID());
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Signing time: ");
        sb.append(ObjectUtils.toString(signingTime));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Signed attributes: ");
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(ASN1Utils.dump(getSignedAttributes()));
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append("Unsigned attributes: ");
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(SystemUtils.LINE_SEPARATOR);
        sb.append(ASN1Utils.dump(getUnsignedAttributes()));

        return sb.toString();
    }

    public static boolean isDlEncodeSignedAttributes() {
        return dlEncodeSignedAttributes;
    }

    public static void setDlEncodeSignedAttributes(boolean dlEncodeSignedAttributes) {
        SignerInfoImpl.dlEncodeSignedAttributes = dlEncodeSignedAttributes;
    }
}
