/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.google.common.annotations.VisibleForTesting;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

public class PGPTrustListDAO extends GenericHibernateDAO
{
    /*
     * The name of the trust list this DAO works for
     */
    private final String name;

    private PGPTrustListDAO(@Nonnull SessionAdapter session, @Nonnull String name)
    {
        super(session);

        this.name = Objects.requireNonNull(name);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static PGPTrustListDAO getInstance(@Nonnull SessionAdapter session, @Nonnull String name) {
        return new PGPTrustListDAO(session, name);
    }

    public List<PGPTrustListEntity> getPGPTrustListEntities(Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<PGPTrustListEntity> criteriaQuery = criteriaBuilder.createQuery(PGPTrustListEntity.class);

        Root<PGPTrustListEntity> rootEntity = criteriaQuery.from(PGPTrustListEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(PGPTrustListEntity.NAME_COLUMN), name));

        Query<PGPTrustListEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    /*
     * Returns all the PGPTrustListEntity's irrespective of the trust list name. Only use this if you really need all
     * keys.
     */
    public static List<PGPTrustListEntity> getAllPGPTrustListEntities(@Nonnull SessionAdapter session,
            Integer firstResult, Integer maxResults)
    {
        PGPTrustListDAO dao = PGPTrustListDAO.getInstance(session, "ignored");

        CriteriaBuilder criteriaBuilder = dao.getCriteriaBuilder();

        CriteriaQuery<PGPTrustListEntity> criteriaQuery = criteriaBuilder.createQuery(PGPTrustListEntity.class);

        criteriaQuery.from(PGPTrustListEntity.class);

        Query<PGPTrustListEntity> query = dao.createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    public long size()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<PGPTrustListEntity> rootEntity = criteriaQuery.from(PGPTrustListEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(PGPTrustListEntity.NAME_COLUMN), name));

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    public PGPTrustListEntity getPGPTrustListEntity(@Nonnull String thumbprint)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<PGPTrustListEntity> criteriaQuery = criteriaBuilder.createQuery(PGPTrustListEntity.class);

        Root<PGPTrustListEntity> rootEntity = criteriaQuery.from(PGPTrustListEntity.class);

        criteriaQuery.where(
                criteriaBuilder.equal(rootEntity.get(PGPTrustListEntity.NAME_COLUMN), name),
                criteriaBuilder.equal(rootEntity.get(PGPTrustListEntity.FINGERPRINT_COLUMN), thumbprint));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    /**
     * Deletes all the entries with the provided name
     */
    public void delete()
    {
        List<PGPTrustListEntity> all = getPGPTrustListEntities(null, null);

        for (PGPTrustListEntity entry : all) {
            delete(entry);
        }
    }

    /**
     * Deletes ALL  entries irrespective of name (used while testing to make
     * sure that all entries are removed)
     */
    @VisibleForTesting
    public static void deleteAll(@Nonnull SessionAdapter session)
    {
        PGPTrustListDAO dao = PGPTrustListDAO.getInstance(session, "ignored");

        List<PGPTrustListEntity> all = dao.findAll(PGPTrustListEntity.class);

        for (PGPTrustListEntity entry : all) {
            dao.delete(entry);
        }
    }

    public String getName() {
        return name;
    }
}
