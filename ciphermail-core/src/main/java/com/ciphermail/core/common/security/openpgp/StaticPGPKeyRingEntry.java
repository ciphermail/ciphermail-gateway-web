/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A PGPKeyRingEntry implementation that directly maps to a PGPPublicKey
 * <p>
 * Note: this implementation is not a full implementation of PGPKeyRingEntry. The main purpose of this class is for
 * testing
 */
public class StaticPGPKeyRingEntry implements PGPKeyRingEntry
{
    UUID id;
    Long keyId;
    Date creationDate;
    Date expirationDate;
    Date insertionDate;
    PGPPublicKey publicKey;
    PGPPrivateKey privateKey;
    String privateKeyAlias;
    String fingerprint;
    String sha256Fingerprint;
    Set<String> userIDs = new LinkedHashSet<>();
    Set<String> email = new LinkedHashSet<>();
    boolean masterKey;
    PGPKeyRingEntry parentKey;
    Set<PGPKeyRingEntry> subKeys = new LinkedHashSet<>();
    String keyringName;

    public StaticPGPKeyRingEntry(UUID id) {
        this.id = id;
    }

    public StaticPGPKeyRingEntry(UUID id, @Nonnull PGPPublicKey publicKey)
    throws IOException
    {
        this.id = id;
        setPublicKey(publicKey);
    }

    @Override
    public UUID getID() {
        return id;
    }

    public void setID(UUID id) {
        this.id = id;
    }

    @Override
    public Long getKeyID() {
        return keyId;
    }

    public void setKeyID(Long keyID) {
        this.keyId = keyID;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public Date getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(Date insertionDate) {
        this.insertionDate = insertionDate;
    }

    @Override
    public void setPublicKey(@Nonnull PGPPublicKey publicKey)
    throws IOException
    {
        try {
            this.publicKey = publicKey;
            this.keyId = publicKey.getKeyID();
            this.fingerprint = PGPPublicKeyInspector.getFingerprintHex(publicKey);
            this.sha256Fingerprint = PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey);
            this.creationDate = publicKey.getCreationTime();
            this.expirationDate = PGPPublicKeyInspector.getExpirationDate(publicKey);
            this.email = new LinkedHashSet<>(PGPUtils.getEmailAddressesFromUserIDs(
                    PGPPublicKeyInspector.getUserIDsAsStrings(publicKey)));
            this.userIDs = new LinkedHashSet<>(PGPPublicKeyInspector.getUserIDsAsStrings(publicKey));
            this.masterKey = publicKey.isMasterKey();
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new IOException(e);
        }
    }

    @Override
    public PGPPublicKey getPublicKey() {
        return publicKey;
    }

    @Override
    public PGPPrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PGPPrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public String getPrivateKeyAlias() {
        return privateKeyAlias;
    }

    public void setPrivateKeyAlias(String privateKeyAlias) {
        this.privateKeyAlias = privateKeyAlias;
    }

    @Override
    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    @Override
    public String getSHA256Fingerprint() {
        return sha256Fingerprint;
    }

    public void setSHA256Fingerprint(String sha256Fingerprint) {
        this.sha256Fingerprint = sha256Fingerprint;
    }

    @Override
    public Set<String> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(Set<String> userIDs) {
        this.userIDs = userIDs != null ? new LinkedHashSet<>() : null;
    }

    @Override
    public Set<String> getEmail() {
        return email;
    }

    @Override
    public void setEmail(Set<String> email) {
        this.email = email != null ? new LinkedHashSet<>(email) : null;
    }

    @Override
    public boolean isMasterKey() {
        return masterKey;
    }

    public void setMasterKey(boolean masterKey) {
        this.masterKey = masterKey;
    }

    @Override
    public PGPKeyRingEntry getParentKey() {
        return parentKey;
    }

    public void setParentKey(PGPKeyRingEntry parentKey) {
        this.parentKey = parentKey;
    }

    @Override
    public Set<PGPKeyRingEntry> getSubkeys() {
        return subKeys;
    }

    public void setSubkeys(Set<PGPKeyRingEntry> subKeys) {
        this.subKeys = subKeys != null ? new LinkedHashSet<>(subKeys) : null;
    }

    @Override
    public String getKeyRingName() {
        return keyringName;
    }

    public void setKeyRingName(String keyRingName) {
        this.keyringName = keyRingName;
    }
}
