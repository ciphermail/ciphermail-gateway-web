/*
 * Copyright (c) 2010-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor;

import com.ciphermail.core.common.extractor.impl.DetectedMimeTypeImpl;
import com.ciphermail.core.common.extractor.impl.TextExtractorContextImpl;
import com.ciphermail.core.common.mail.MimeTypes;
import com.ciphermail.core.common.scheduler.InputStreamTimeoutTask;
import com.ciphermail.core.common.scheduler.Task;
import com.ciphermail.core.common.scheduler.TaskScheduler;
import com.ciphermail.core.common.scheduler.ThreadInterruptTimeoutTask;
import com.ciphermail.core.common.util.RewindableInputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * The AutodetectFullTextExtractor tries to detect the source of the input document and tried to extract
 * all texts. If the input is a document containing other documents (for example a zip file) the text of
 * the embeded documents will be extracted as well.
 *
 * @author Martijn Brinkers
 *
 */
public class AutoDetectFullTextExtractor
{
    private static final Logger logger = LoggerFactory.getLogger(AutoDetectFullTextExtractor.class);

    /*
     * Is used to detect the mime type based on the content
     */
    private final MimeTypeDetector mimeTypeDetector;

    /*
     * Is used to get the TextExtractors
     */
    private final TextExtractorFactoryRegistry extractorFactoryRegistry;

    /*
     * maximum recursive depth for documents (when a document contains another document etc.)
     */
    private int maxDocumentDepth = 16;

    /*
     * The maximum time (in milliseconds) a parser may parse until an attempt is made to 'kill' the parser.
     */
    private long timeout = DateUtils.MILLIS_PER_SECOND * 30;

    /*
     * The maximum size (in bytes) the extracted text can get. If the extracted text exceeds this size an exception
     * will be thrown if exceptionOnMaxSizeExceed is true. The maxSize is used to protect against 'zip bombs'.
     */
    private long maxSize = SizeUtils.MB * 50L;

    /*
     * The maximum number of parts (text and attachments) that is going to be handled. The max must be large
     * enough to handle most zip files containing multuple documents
     */
    private int maxNrOfTextExtractorEvents = 1024;

    /*
     * The size (in bytes) at which the extracted text will be stored on file instead of memory.
     */
    private int threshold = SizeUtils.MB * 10;

    /*
     * If false, and an exception is thrown while parsing a part (or sub document), the part or sub document
     * will be skipped. If true, the exception will be thrown.
     */
    private boolean failOnException;

    /*
     * If true, the fallback text extractor will be tried when failOnException is false and an exception
     * during text extraction has occurred.
     */
    private boolean useFallbackExtractor = true;

    /*
     * The mime type of the TextExtrator if failOnException is false and an exception
     * during text extraction has occurred.
     */
    private String fallbackTextExtractorMimeType = MimeTypes.OCTET_STREAM;

    /*
     * The mime type of the default TextExtrator if a TextExtractor is not found.
     */
    private String defaultTextExtractorMimeType = MimeTypes.OCTET_STREAM;

    protected Logger getLogger() {
        return logger;
    }

    /*
     * Keeps the current part scanner context
     */
    public class PartContext
    {
        private final List<ExtractedPart> textParts = new LinkedList<>();

        /*
         * the running total size (in bytes) of all extracted parts
         */
        private long totalSize;

        /*
         * The current total number of parts (text and attachments) that have been handled.
         */
        private int numberOfParts;

        /*
         * Keeps track of the document depth.
         */
        private int currentDocumentDepth;

        public void update(ExtractedPart part, boolean add)
        throws IOException
        {
            long partSize = part.getByteCount();

            if (partSize != -1) {
                totalSize = totalSize + partSize;
            }

            if (totalSize > maxSize) {
                throw new MaxExceededException("Maximum size " + maxSize + " exceeded.");
            }

            numberOfParts++;

            if (numberOfParts > maxNrOfTextExtractorEvents) {
                throw new MaxExceededException("Maximum number of parts " + maxNrOfTextExtractorEvents + " exceeded.");
            }

            if (add) {
                textParts.add(part);
            }
        }

        public void enter()
        throws MaxExceededException
        {
            currentDocumentDepth++;

            if (currentDocumentDepth > maxDocumentDepth) {
                throw new MaxExceededException("Maximum document depth " + maxDocumentDepth + " exceeded.");
            }
        }

        public void leave() {
            currentDocumentDepth--;
        }

        public List<ExtractedPart> getTextParts() {
            return textParts;
        }
    }

    public AutoDetectFullTextExtractor(@Nonnull MimeTypeDetector detector, @Nonnull TextExtractorFactoryRegistry factories)
    {
        this.mimeTypeDetector = Objects.requireNonNull(detector);
        this.extractorFactoryRegistry = Objects.requireNonNull(factories);
    }

    private void logException(String message, Exception e)
    {
        // Only log exception when debug level is enabled. The reason for this is that
        // when scanning office documents, a log of warnings can be reported and logging
        // the exception will clutter the log too much.
        if (getLogger().isDebugEnabled()) {
            getLogger().warn(message, e);
        }
        else {
            getLogger().warn("{} Message: {}", message, ExceptionUtils.getRootCauseMessage(e));
        }
    }

    private TextExtractorFactory getFallbackTextExtractorFactory()
    {
        TextExtractorFactory factory = extractorFactoryRegistry.getFactory(fallbackTextExtractorMimeType);

        if (factory == null) {
            getLogger().debug("Fallback TextExtractor {} not found.", fallbackTextExtractorMimeType);
        }

        return factory;
    }

    private TextExtractorFactory getDefaultTextExtractorFactory()
    {
        TextExtractorFactory factory = extractorFactoryRegistry.getFactory(defaultTextExtractorMimeType);

        if (factory == null) {
            getLogger().debug("Default TextExtractor {} not found.", defaultTextExtractorMimeType);
        }

        return factory;
    }

    private void tryFallbackExtractor(RewindableInputStream input, TextExtractorContext textExtractorContext,
            TextExtractorEventHandler handler)
    throws AbortExtractionException
    {
        if (!useFallbackExtractor) {
            return;
        }

        TextExtractorFactory factory = getFallbackTextExtractorFactory();

        if (factory != null)
        {
            try {
                factory.createTextExtractor().extract(input, textExtractorContext, handler);
            }
            catch(AbortExtractionException e) {
                // Abort should stop extration
                throw e;
            }
            catch(IOException | RuntimeException nested)
            {
                // Because the fallback will only be tried when failOnException is false, there is
                // no need to check it here because we know that failOnException is false.
                logException("An error occurred while extracting text using fallback extractor. " +
                        "Part will be skipped.", nested);
            }
        }
    }

    private DetectedMimeType extractTextInternal(RewindableInputStream input, String resourceName, final PartContext partContext)
    throws IOException
    {
        long currentPosition = input.getPosition();

        DetectedMimeType detectedMimeType = mimeTypeDetector.detectMimeType(input, resourceName);

        TextExtractorFactory textExtractorFactory;

        // Keeps track of mime types that are already handled. This is done to handle
        // possible loops in mime type super type handling
        Set<DetectedMimeType> handled = null;

        // We need the detectedMimeType later on so use another var for the loop
        DetectedMimeType mimeType = detectedMimeType;

        do
        {
            textExtractorFactory = extractorFactoryRegistry.getFactory(mimeType.toString());

            if (textExtractorFactory == null) {
                // no TextExtractorFactory found for mime type. Try the super type
                mimeType = mimeType.getSuperType();

                if (mimeType == null)
                {
                    // Fallback to default TextExtractor
                    textExtractorFactory = getDefaultTextExtractorFactory();

                    if (textExtractorFactory == null) {
                        break;
                    }
                }

                if (handled == null) {
                    handled = new HashSet<>();
                }

                if (handled.contains(mimeType))
                {
                    getLogger().warn("{} was already tried.", mimeType);

                    break;
                }
                else {
                    handled.add(mimeType);
                }
            }
        }
        while (textExtractorFactory == null);

        if (textExtractorFactory != null)
        {
            TextExtractor textExtractor = textExtractorFactory.createTextExtractor();

            TextExtractorContext textExtractorContext = new TextExtractorContextImpl();

            textExtractorContext.setName(resourceName);
            // PLace the detected mime type in the context. If no mime type detected, use octet-stream
            textExtractorContext.setMimeType(detectedMimeType != null ? detectedMimeType :
                DetectedMimeTypeImpl.DEFAULT_MIME_TYPE);

            // Create handler that will handle the text and attachment part
            TextExtractorEventHandler handler = new TextExtractorEventHandler()
            {
                @Override
                public void attachmentEvent(ExtractedPart attachmentPart)
                throws IOException
                {
                    try {
                        partContext.update(attachmentPart, false);

                        extractTextWithWatchdog(attachmentPart.getContent(), attachmentPart.getContext().getName(),
                                partContext);
                    }
                    finally {
                        attachmentPart.close();
                    }
                }

                @Override
                public void textEvent(ExtractedPart textPart)
                throws IOException
                {
                    partContext.update(textPart, true);
                }
            };

            try {
                textExtractor.extract(input, textExtractorContext, handler);
            }
            catch(AbortExtractionException e) {
                // Abort should stop extration
                throw e;
            }
            catch(IOException | RuntimeException e)
            {
                if (failOnException) {
                    throw e;
                }

                logException("An error occurred while extracting text. Trying fallback extractor.", e);

                // Rewind the input to the position where we started
                input.setPosition(currentPosition);

                tryFallbackExtractor(input, textExtractorContext, handler);
            }
        }

        return detectedMimeType;
    }

    /*
     * extractText method will add some watchdog timers that will try to force 'kill' the extractor if extracting
     * takes too long. A running thread cannot be killed, therefore any closable resource will be closed
     * (like input and writer). There is however not guarantee that a running runaway thread can always be stopped.
     */
    private DetectedMimeType extractTextWithWatchdog(RewindableInputStream input, String resourceName, PartContext partContext)
    throws IOException
    {
        // watchdog which will be used to 'kill' a runaway text extractor
        TaskScheduler watchdog = new TaskScheduler(AutoDetectFullTextExtractor.class.getCanonicalName());

        try {
            partContext.enter();

            // Add watchdog that will interrupt the thread on timeout. we want the input to be closed first so
            // some extra time (50%)
            Task threadWatchdogTask = new ThreadInterruptTimeoutTask(Thread.currentThread(), watchdog.getName());
            watchdog.addTask(threadWatchdogTask, (long) (timeout * 1.5));

            // add a timeout watchdog on the input which will close the input on timeout
            Task inputWatchdogTask = new InputStreamTimeoutTask(input, watchdog.getName());
            watchdog.addTask(inputWatchdogTask, timeout);

            DetectedMimeType detectedMimeType = extractTextInternal(input, resourceName, partContext);

            if (threadWatchdogTask.hasRun() || inputWatchdogTask.hasRun()) {
                getLogger().warn("A timeout detected while extracting text.");
            }

            return detectedMimeType;
        }
        finally {
            // we must cancel all watchdogs tasks
            watchdog.cancel();

            // Clear the interrupted flag, it might have been set by the ThreadInterruptTimeoutTask
            Thread.interrupted();

            partContext.leave();
        }
    }

    protected DetectedMimeType extractText(RewindableInputStream input, String resourceName, PartContext context)
    throws TextExtractorException
    {
        DetectedMimeType detectedMimeType = null;

        try {
            detectedMimeType = extractTextWithWatchdog(input, resourceName, context);
        }
        catch (IOException e)
        {
            if (failOnException) {
                throw new TextExtractorException(e);
            }
            logException("IOException while extracting text.", e);
        }

        if (detectedMimeType == null) {
            detectedMimeType = DetectedMimeTypeImpl.DEFAULT_MIME_TYPE;
        }

        return detectedMimeType;
    }

    public List<ExtractedPart> extractText(RewindableInputStream input, String resourceName)
    throws TextExtractorException
    {
        PartContext context = new PartContext();

        try {
            extractText(input, resourceName, context);
        }
        catch (IOException e)
        {
            if (failOnException) {
                throw new TextExtractorException(e);
            }
            logException("IOException while extracting text.", e);
        }

        return context.textParts;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public boolean isFailOnException() {
        return failOnException;
    }

    public void setFailOnException(boolean failOnException) {
        this.failOnException = failOnException;
    }

    public long getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(long maxSize) {
        this.maxSize = maxSize;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getFallbackTextExtractorMimeType() {
        return fallbackTextExtractorMimeType;
    }

    public void setFallbackTextExtractorMimeType(String fallbackTextExtractorMimeType) {
        this.fallbackTextExtractorMimeType = fallbackTextExtractorMimeType;
    }

    public String getDefaultTextExtractorMimeType() {
        return defaultTextExtractorMimeType;
    }

    public void setDefaultTextExtractorMimeType(String defaultTextExtractorMimeType) {
        this.defaultTextExtractorMimeType = defaultTextExtractorMimeType;
    }

    public int getMaxNrOfTextExtractorEvents() {
        return maxNrOfTextExtractorEvents;
    }

    public void setMaxNrOfTextExtractorEvents(int maxNrOfTextExtractorEvents) {
        this.maxNrOfTextExtractorEvents = maxNrOfTextExtractorEvents;
    }

    public int getMaxDocumentDepth() {
        return maxDocumentDepth;
    }

    public void setMaxDocumentDepth(int maxDocumentDepth) {
        this.maxDocumentDepth = maxDocumentDepth;
    }

    public boolean isUseFallbackExtractor() {
        return useFallbackExtractor;
    }

    public void setUseFallbackExtractor(boolean useFallbackExtractor) {
        this.useFallbackExtractor = useFallbackExtractor;
    }
}
