/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.app.admin.Admin;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Objects;

/**
 * Checks whether a simple select query succeeds
 */
public class DatabaseMonitorImpl implements DatabaseMonitor
{
    private static final Logger logger = LoggerFactory.getLogger(DatabaseMonitorImpl.class);

    /*
     * Manages database sessions
     */
    private final SessionManager sessionManager;

    /*
     * Starts database transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * The entity which will be used to check whether the database can be accessed
     */
    private Class<?> entityClass = Admin.class;

    /*
     * The time in seconds to wait for the database operation used to validate the connection to complete. If the
     * timeout period expires before the operation completes, this method returns false. A value of 0 indicates a
     * timeout is not applied to the database operation.
     */
    private int timeout = 30;

    /*
     * If true, the database is checked using Connection#isValid. If false, a query will be executed
     */
    private boolean checkConnection;

    public DatabaseMonitorImpl(SessionManager sessionManager, TransactionOperations transactionOperations)
    {
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
    }

    @Override
    public boolean isDatabaseActive()
    {
        boolean active = false;

        try {
            active = checkConnection ? isDatabaseActiveConnection() : isDatabaseActiveQuery();
        }
        catch (Exception e) {
            logger.error("Error checking whether database was active", e);
        }

        return active;
    }

    /*
     * Checks if the database is active checking the connection
     */
    private boolean isDatabaseActiveConnection()
    {
        try(StatelessSession session = sessionManager.openStatelessSession()) {
            return session.doReturningWork(connection -> connection.isValid(timeout));
        }
    }

    /*
     * Checks if the database is active by executing a simple query
     */
    private boolean isDatabaseActiveQuery()
    {
        return Boolean.TRUE.equals(transactionOperations.execute(status ->
        {
            Session session = sessionManager.getSession();

            CriteriaBuilder cb = session.getCriteriaBuilder();

            CriteriaQuery<?> criteriaQuery = cb.createQuery(entityClass);

            @SuppressWarnings("rawtypes")
            Root rootEntity = criteriaQuery.from(entityClass);

            //noinspection unchecked
            criteriaQuery.select(rootEntity);

            Query<?> query = session.createQuery(criteriaQuery);

            // Run a select with max one result. We are not interested in the result, only whether the query
            // will succeed or not
            query.setMaxResults(1);
            query.list();

            return true;
        }));
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isCheckConnection() {
        return checkConnection;
    }

    public void setCheckConnection(boolean checkConnection) {
        this.checkConnection = checkConnection;
    }
}
