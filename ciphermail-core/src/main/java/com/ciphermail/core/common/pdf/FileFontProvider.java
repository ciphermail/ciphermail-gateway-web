/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.lowagie.text.Font;
import com.lowagie.text.pdf.BaseFont;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Implementation of FontProvider that reads the .ttf files from the provided font
 * directory at startup.
 *
 * @author Martijn Brinkers
 *
 */
public class FileFontProvider implements FontProvider
{
    private static final Logger logger = LoggerFactory.getLogger(FileFontProvider.class);

    /*
     * The dir to read fonts from
     */
    private final File fontDir;

    /*
     * The fonts read from the font dir
     */
    private Collection<Font> fonts;

    /*
     * Filter for font files (.ttf)
     */
    protected final SuffixFileFilter fileFilter = new SuffixFileFilter(".ttf", IOCase.INSENSITIVE);

    public FileFontProvider(@Nonnull File fontDir)
    {
        this.fontDir = Objects.requireNonNull(fontDir);

        loadFonts();
    }

    protected synchronized void loadFonts()
    {
        String[] fontFiles = fontDir.list(fileFilter);

        if (fontFiles == null) {
            fontFiles = new String[]{};
        }

        fonts = new ArrayList<>(fontFiles.length);

        for (String fontFile : fontFiles)
        {
            logger.info("Adding font file {}", fontFile);

            try {
                fonts.add(new Font(BaseFont.createFont(new File(fontDir, fontFile).getPath(),
                        BaseFont.IDENTITY_H, true)));
            }
            catch (Exception e) {
                logger.error("Error loading font file {}", fontFile, e);
            }
        }
    }

    @Override
    public synchronized Collection<Font> getFonts() {
        // Create a shallow copy
        return new ArrayList<>(fonts);
    }
}
