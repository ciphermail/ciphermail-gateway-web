/*
 * Copyright (c) 2009-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.clickatell;

import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;


/**
 * Implementation of ClickatellParameters that reads and writes it's settings from
 * a HierarchicalProperties object.
 *
 * @author Martijn Brinkers
 *
 */
@UserPropertiesType(name = "clickatell", visible = false)
public class ClickatellPropertiesImpl extends DelegatedHierarchicalProperties implements ClickatellProperties
{
    private static final String APIID_PROPERTY_NAME = "sms-transport-clickatell-legacy-api-id";
    private static final String USER_PROPERTY_NAME = "sms-transport-clickatell-legacy-user";
    private static final String PASSWORD_PROPERTY_NAME = "sms-transport-clickatell-legacy-password";
    private static final String FROM_PROPERTY_NAME = "sms-transport-clickatell-legacy-from";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public ClickatellPropertiesImpl(@Nonnull HierarchicalProperties delegatedProperties) {
        super(delegatedProperties);
    }

    @Override
    @UserProperty(name = APIID_PROPERTY_NAME,
            user = false, domain = false,
            order = 10,
            allowNull = true
    )
    public String getAPIID()
    throws HierarchicalPropertiesException
    {
        return getProperty(APIID_PROPERTY_NAME);
    }

    @Override
    @UserProperty(name = APIID_PROPERTY_NAME,
            user = false, domain = false
    )
    public void setAPIID(String id)
    throws HierarchicalPropertiesException
    {
        setProperty(APIID_PROPERTY_NAME, id);
    }

    @Override
    @UserProperty(name = USER_PROPERTY_NAME,
            user = false, domain = false,
            order = 20,
            allowNull = true
    )
    public String getUser()
    throws HierarchicalPropertiesException
    {
        return getProperty(USER_PROPERTY_NAME);
    }

    @Override
    @UserProperty(name = USER_PROPERTY_NAME,
            user = false, domain = false
    )
    public void setUser(String user)
    throws HierarchicalPropertiesException
    {
        setProperty(USER_PROPERTY_NAME, user);
    }

    @Override
    @UserProperty(name = PASSWORD_PROPERTY_NAME,
            user = false, domain = false,
            order = 30,
            allowNull = true
    )
    public String getPassword()
    throws HierarchicalPropertiesException
    {
        return getProperty(PASSWORD_PROPERTY_NAME);
    }

    @Override
    @UserProperty(name = PASSWORD_PROPERTY_NAME,
            user = false, domain = false
    )
    public void setPassword(String password)
    throws HierarchicalPropertiesException
    {
        setProperty(PASSWORD_PROPERTY_NAME, password);
    }

    @Override
    @UserProperty(name = FROM_PROPERTY_NAME,
            user = false, domain = false,
            order = 40,
            allowNull = true
    )
    public String getFrom()
    throws HierarchicalPropertiesException
    {
        return getProperty(FROM_PROPERTY_NAME);
    }

    @Override
    @UserProperty(name = FROM_PROPERTY_NAME,
            user = false, domain = false
    )
    public void setFrom(String from)
    throws HierarchicalPropertiesException
    {
        setProperty(FROM_PROPERTY_NAME, from);
    }

    @Override
    public void copyTo(ClickatellProperties other)
    throws HierarchicalPropertiesException
    {
        other.setAPIID(getAPIID());
        other.setUser(getUser());
        other.setPassword(getPassword());
        other.setFrom(getFrom());
    }
}
