/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.operator.PGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Implementation of PGPSignatureValidator
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSignatureValidatorImpl implements PGPSignatureValidator
{
    private static PGPContentVerifierBuilderProvider getPGPContentVerifierBuilderProvider()
    {
        JcaPGPContentVerifierBuilderProvider builderProvider = new JcaPGPContentVerifierBuilderProvider();

        builderProvider.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        return builderProvider;
    }

    @Override
    public boolean validateUserIDSignature(@Nonnull PGPPublicKey publicKey, @Nonnull byte[] userID,
            @Nonnull PGPSignature signature)
    throws IOException, PGPException
    {
        int signatureType = signature.getSignatureType();

        if (signatureType != PGPSignature.DEFAULT_CERTIFICATION &&
            signatureType != PGPSignature.NO_CERTIFICATION &&
            signatureType != PGPSignature.CASUAL_CERTIFICATION &&
            signatureType != PGPSignature.POSITIVE_CERTIFICATION)
        {
            throw new PGPException("Signature type " + signatureType + " is not a User-ID certification signature.");
        }

        boolean valid = false;

        // Currently only self signatures are supported
        if (signature.getKeyID() == publicKey.getKeyID())
        {
            signature.init(getPGPContentVerifierBuilderProvider(), publicKey);

            if (signature.getVersion() == 3) {
                // Version 3 User ID signatures need to be calculated differently
                byte[] keyBytes = publicKey.getPublicKeyPacket().getEncodedContents();

                signature.update((byte)0x99);
                signature.update((byte)(keyBytes.length >> 8));
                signature.update((byte)(keyBytes.length));
                signature.update(keyBytes);
                signature.update(userID);

                valid = signature.verify();
            }
            else {
                valid = signature.verifyCertification(userID, publicKey);
            }
        }
        else {
            throw new PGPNotSelfSignedException("Signature is not a self signature. It was signed by " +
                    PGPUtils.getKeyIDHex(signature.getKeyID()) + " and not by " +
                    PGPUtils.getKeyIDHex(publicKey.getKeyID()));
        }

        return valid;
    }

    @Override
    public boolean validateUserIDRevocationSignature(@Nonnull PGPPublicKey publicKey, @Nonnull byte[] userID,
            @Nonnull PGPSignature signature)
    throws PGPException
    {
        int signatureType = signature.getSignatureType();

        if (signatureType != PGPSignature.CERTIFICATION_REVOCATION)
        {
            throw new PGPException("Signature type " + signatureType + " is not a certification revocation signature.");
        }

        boolean valid = false;

        // Currently only self signatures are supported
        if (signature.getKeyID() == publicKey.getKeyID())
        {
            signature.init(getPGPContentVerifierBuilderProvider(), publicKey);

            valid = signature.verifyCertification(userID, publicKey);
        }
        else {
            throw new PGPNotSelfSignedException("Signature is not a self signature. It was signed by " +
                    PGPUtils.getKeyIDHex(signature.getKeyID()) + " and not by " +
                    PGPUtils.getKeyIDHex(publicKey.getKeyID()));
        }

        return valid;
    }

    @Override
    public boolean validateSubkeyBindingSignature(@Nonnull PGPPublicKey masterKey, @Nonnull PGPPublicKey subKey,
            @Nonnull PGPSignature signature)
    throws PGPException
    {
        if (signature.getSignatureType() != PGPSignature.SUBKEY_BINDING)
        {
            throw new PGPException("Signature type " + signature.getSignatureType() +
                    " is not a subkey binding signature.");
        }

        if (!masterKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(masterKey.getKeyID()) +
                    " is not a master key but a sub key.");
        }

        if (subKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(subKey.getKeyID()) +
                    " is not a sub key but a master key.");
        }

        boolean valid = false;

        // Currently only self signatures are supported
        if (signature.getKeyID() == masterKey.getKeyID())
        {
            signature.init(getPGPContentVerifierBuilderProvider(), masterKey);

            valid = signature.verifyCertification(masterKey, subKey);
        }
        else {
            throw new PGPNotSelfSignedException("Signature is not a self signature. It was signed by " +
                    PGPUtils.getKeyIDHex(signature.getKeyID()) + " and not by " +
                    PGPUtils.getKeyIDHex(masterKey.getKeyID()));
        }

        return valid;
    }

    @Override
    public boolean validatePrimaryKeyBindingSignature(@Nonnull PGPPublicKey masterKey, @Nonnull PGPPublicKey subKey,
            @Nonnull PGPSignature signature)
    throws PGPException
    {
        if (signature.getSignatureType() != PGPSignature.PRIMARYKEY_BINDING)
        {
            throw new PGPException("Signature type " + signature.getSignatureType() +
                    " is not a primary key binding signature.");
        }

        if (!masterKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(masterKey.getKeyID()) +
                    " is not a master key but a sub key.");
        }

        if (subKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(subKey.getKeyID()) +
                    " is not a sub key but a master key.");
        }

        boolean valid = false;

        // Currently only self signatures are supported.
        //
        // Note: A primary key binding signature must be issued by the sub key
        if (signature.getKeyID() == subKey.getKeyID())
        {
            signature.init(getPGPContentVerifierBuilderProvider(), subKey);

            valid = signature.verifyCertification(masterKey, subKey);
        }
        else {
            throw new PGPNotSelfSignedException("Signature is not a self signature. It was signed by " +
                    PGPUtils.getKeyIDHex(signature.getKeyID()) + " and not by " +
                    PGPUtils.getKeyIDHex(masterKey.getKeyID()));
        }

        return valid;
    }

    @Override
    public boolean validateKeyRevocationSignature(@Nonnull PGPPublicKey masterKey, PGPPublicKey subKey,
            @Nonnull PGPSignature signature)
    throws PGPException
    {
        int signatureType = signature.getSignatureType();

        if (signatureType != PGPSignature.KEY_REVOCATION && signatureType != PGPSignature.SUBKEY_REVOCATION) {
            throw new PGPException("Signature type " + signatureType + " is not a revocation signature.");
        }

        if (signatureType == PGPSignature.SUBKEY_REVOCATION && subKey == null) {
            throw new PGPException("Signature is a sub key revocation but sub key is not set.");
        }

        if (!masterKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(masterKey.getKeyID()) +
                    " is not a master key but a sub key.");
        }

        if (subKey != null && subKey.isMasterKey())
        {
            throw new PGPException("key with key id " + PGPUtils.getKeyIDHex(subKey.getKeyID()) +
                    " is not a sub key but a master key.");
        }

        boolean valid = false;

        // Currently only self signatures are supported
        if (signature.getKeyID() == masterKey.getKeyID())
        {
            signature.init(getPGPContentVerifierBuilderProvider(), masterKey);

            // Validating a master key's revocation only requires the key itself. For subkey's we need the master and
            // sub key
            //
            // RFC 4880 is not correct. See errata http://www.rfc-editor.org/errata_search.php?rfc=4880&eid=3298
            valid = signatureType == PGPSignature.KEY_REVOCATION ? signature.verifyCertification(masterKey) :
                signature.verifyCertification(masterKey, subKey);
        }
        else {
            throw new PGPNotSelfSignedException("Signature is not a self signature. It was signed by " +
                    PGPUtils.getKeyIDHex(signature.getKeyID()) + " and not by " +
                    PGPUtils.getKeyIDHex(masterKey.getKeyID()));
        }

        return valid;
    }
}
