/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;

import javax.annotation.Nonnull;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of CMSSignedInspector
 *
 * @author Martijn Brinkers
 *
 */
public class CMSSignedInspectorImpl implements CMSSignedInspector
{
    private final CMSSignedDataAdapter signedDataAdapter;

    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    /*
     * The provider for sensitive operations
     */
    private final String sensitiveProvider;

    /*
     * The signers
     */
    private List<SignerInfo> signers;

    /*
     * The certificates from the CMS blob
     */
    private List<X509Certificate> x509certificates;

    /*
     * The CRLs from the CMS blob
     */
    private List<X509CRL> x509CRLs;

    public CMSSignedInspectorImpl(@Nonnull CMSSignedDataAdapter signedDataAdapter, String nonSensitiveProvider,
            String sensitiveProvider)
    {
        this.signedDataAdapter = Objects.requireNonNull(signedDataAdapter);
        this.nonSensitiveProvider = nonSensitiveProvider;
        this.sensitiveProvider = sensitiveProvider;
    }

    @Override
    public List<SignerInfo> getSigners()
    throws CryptoMessageSyntaxException
    {
        if (signers == null)
        {
            signers = new LinkedList<>();

            try {
                SignerInformationStore signerStore = signedDataAdapter.getSignerInfos();

                Collection<SignerInformation> cmsSigners = signerStore.getSigners();

                for (SignerInformation signerInformation : cmsSigners) {
                    signers.add(new SignerInfoImpl(signerInformation, nonSensitiveProvider, sensitiveProvider));
                }
            }
            catch (CMSException e) {
                throw new CryptoMessageSyntaxException(e);
            }
        }

        return Collections.unmodifiableList(signers);
    }

    @Override
    public int getVersion() {
        return signedDataAdapter.getVersion();
    }

    @Override
    public List<X509Certificate> getCertificates()
    throws CryptoMessageSyntaxException
    {
        if (x509certificates == null)
        {
            try {
                x509certificates = signedDataAdapter.getCertificates(nonSensitiveProvider);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException | CMSException e) {
                throw new CryptoMessageSyntaxException(e);
            }
        }

        return Collections.unmodifiableList(x509certificates);
    }

    @Override
    public List<X509CRL> getCRLs()
    throws CryptoMessageSyntaxException
    {
        if (x509CRLs == null)
        {
            try {
                x509CRLs = signedDataAdapter.getCRLs(nonSensitiveProvider);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException | CMSException e) {
                throw new CryptoMessageSyntaxException(e);
            }
        }

        return Collections.unmodifiableList(x509CRLs);
    }
}
