/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.notification;

import com.ciphermail.core.common.util.ThreadUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of NotificationService
 *
 * @author Martijn Brinkers
 *
 */
public class NotificationServiceImpl implements NotificationService
{
    private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);

    /*
     * The name of the background thread.
     */
    private static final String THREAD_NAME = "NotificationService";

    /*
     * The registered listeners
     */
    private final List<NotificationListener> notificationListeners = new LinkedList<>();

    /*
     * Time (in milliseconds) the thread will sleep until the next invocation of the registered
     * NotificationWakeupListener's
     */
    private long updateInterval = DateUtils.MILLIS_PER_MINUTE;

    /*
     * Time (in milliseconds) to settle down at start before the maintainer thread will start.
     */
    private long settleTime = DateUtils.MILLIS_PER_SECOND * 30;

    /*
     * The background thread that periodically wakes-up the registered NotificationWakeupListener's
     */
    private NotificationThread notificationThread;

    /*
     * True if the thread should stop
     */
    private boolean stop;

    @Override
    public synchronized void sendNotification(String facility, NotificationSeverity severity, NotificationMessage message)
    {
        for (NotificationListener listener : notificationListeners)
        {
            try {
                listener.notificationEvent(facility, severity, message);
            }
            catch (Exception e) {
                logger.error("Error in listener#notificationEvent for listener " + listener.getName(), e);
            }
        }
    }

    @Override
    public synchronized void registerNotificationListener(@Nonnull NotificationListener listener) {
        notificationListeners.add(Objects.requireNonNull(listener));
    }

    /**
     * Starts the background thread
     */
    public synchronized void start()
    {
        if (notificationThread == null)
        {
            notificationThread = new NotificationThread();

            notificationThread.setDaemon(true);
            notificationThread.start();
        }
    }

    /**
     * Flags the background thread to stop
     */
    public void stop() {
        stop = true;
    }

    private synchronized void wakeupNotificationWakeupListeners()
    {
        logger.debug("Waking up NotificationWakeupListener's");

        for (NotificationListener listener : notificationListeners)
        {
            try {
                if (listener instanceof NotificationWakeupListener notificationWakeupListener)
                {
                    logger.debug("Waking up NotificationWakeupListener {}", listener.getName());

                    notificationWakeupListener.wakeup();
                }
            }
            catch (Exception e) {
                logger.error("Error in NotificationWakeupListener#wakeup for listener " + listener.getName(), e);
            }
        }
    }

    public long getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(long updateInterval) {
        this.updateInterval = updateInterval;
    }

    public long getSettleTime() {
        return settleTime;
    }

    public void setSettleTime(long settleTime) {
        this.settleTime = settleTime;
    }

    /*
     * Background thread that periodically wakes-up the registered NotificationWakeupListener's
     */
    private class NotificationThread extends Thread
    {
        NotificationThread() {
            super(THREAD_NAME);
        }

        @Override
        public void run()
        {
            logger.info("Starting thread {}. Settle time: {} Update interval: {}", THREAD_NAME, settleTime,
                    updateInterval);

            ThreadUtils.sleepQuietly(settleTime);

            do {
                try {
                    wakeupNotificationWakeupListeners();

                    try {
                        logger.debug("Thread will sleep.");

                        Thread.sleep(updateInterval);
                    }
                    catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
                catch(Exception e) {
                    logger.error("Error in thread {}.", THREAD_NAME, e);
                }
            }
            while(!stop);
        }
    }
}
