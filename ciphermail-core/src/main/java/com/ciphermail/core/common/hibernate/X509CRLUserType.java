/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.crl.X509CRLInspector;
import com.ciphermail.core.common.util.BigIntegerUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.ValueAccess;
import org.hibernate.usertype.CompositeUserType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.util.Date;

/**
 * Hibernate CompositeUserType that can be used to persist {@link X509CRL}.
 *
 * @author Martijn Brinkers
 *
 */
public class X509CRLUserType implements CompositeUserType<X509CRL>
{
    private static final int CRL_COLUMN_INDEX = 0;
    private static final int CRL_NUMBER_COLUMN_INDEX = 1;
    private static final int ISSUER_COLUMN_INDEX = 2;
    private static final int NEXT_UPDATE_COLUMN_INDEX = 3;
    private static final int THIS_UPDATE_COLUMN_INDEX = 4;
    private static final int THUMBPRINT_COLUMN_INDEX = 5;

    public static final String CRL_COLUMN_NAME = "crl";
    public static final String CRL_NUMBER_COLUMN_NAME = "crlNumber";
    public static final String ISSUER_COLUMN_NAME = "issuer";
    public static final String NEXT_UPDATE_COLUMN_NAME = "nextUpdate";
    public static final String THIS_UPDATE_COLUMN_NAME = "thisUpdate";
    public static final String THUMBPRINT_COLUMN_NAME = "thumbprint";

    public static class X509CRLMapper {
        // in sorted order
        byte[] crl;
        String crlNumber;
        String issuer;
        Date nextUpdate;
        Date thisUpdate;
        String thumbprint;
    }

    @Override
    public Object getPropertyValue(X509CRL crl, int propertyIndex)
    throws HibernateException
    {
        try {
            byte[] encodedCRL = null;

            // Encoding the CRL can be time and memory consuming. We will therefore
            // encode it once and use it for thumbprint as well
            if (propertyIndex == CRL_COLUMN_INDEX || propertyIndex == THUMBPRINT_COLUMN_INDEX) {
                encodedCRL = crl.getEncoded();
            }

            return switch (propertyIndex) {
                case CRL_COLUMN_INDEX         -> encodedCRL;
                case CRL_NUMBER_COLUMN_INDEX  -> BigIntegerUtils.hexEncode(X509CRLInspector.getCRLNumber(crl));
                case ISSUER_COLUMN_INDEX      -> X509CRLInspector.getIssuerCanonical(crl);
                // Some CRLs have a nextUpdate value which exceeds the maximum date value accepted by MySQL which
                // results in the exception:
                //
                // Incorrect datetime value: '10000-01-01 00:59:59' for column `ciphermail-gateway`.`cm_crl`.`cm_next_update` at row 1
                //
                // This CRL has been found "in the wild". We will maximize the date values to make sure the CRL can be
                // imported
                case NEXT_UPDATE_COLUMN_INDEX -> DatabaseUtils.limitDate(crl.getNextUpdate());
                case THIS_UPDATE_COLUMN_INDEX -> DatabaseUtils.limitDate(crl.getThisUpdate());
                case THUMBPRINT_COLUMN_INDEX  -> X509CRLInspector.getThumbprint(encodedCRL);
                default -> null;
            };
        }
        catch (CRLException | NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
            throw new HibernateException(e);
        }
    }

    private static X509CRL decodeCRL(byte[] encodedCRL)
    {
        X509CRL crl = null;

        if (encodedCRL != null)
        {
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory("X.509");

                crl = (X509CRL) factory.generateCRL(new ByteArrayInputStream(encodedCRL));
            }
            catch (CRLException | CertificateException | NoSuchProviderException e) {
                throw new HibernateException(e);
            }
        }

        return crl;
    }

    @Override
    public X509CRL instantiate(ValueAccess valueAccess, SessionFactoryImplementor sessionFactoryImplementor) {
        return decodeCRL(valueAccess.getValue(CRL_COLUMN_INDEX, byte[].class));
    }

    @Override
    public Class<?> embeddable() {
        return X509CRLMapper.class;
    }

    @Override
    public Class<X509CRL> returnedClass() {
        return X509CRL.class;
    }

    @Override
    public boolean equals(X509CRL crl1, X509CRL crl2)
    {
        if (crl1 == crl2) {
            return true;
        }

        if (crl1 == null || crl2 == null) {
            return false;
        }

        return crl1.equals(crl2);
    }

    @Override
    public int hashCode(X509CRL crl)
    {
        return crl.hashCode();
    }

    @Override
    public X509CRL deepCopy(X509CRL crl)
    {
        // According to javadoc:
        // It is not necessary to copy immutable objects, or null values, in which case it is safe to simply return the
        // argument.
        return crl;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(X509CRL crl)
    {
        try {
            return crl != null ? crl.getEncoded() : null;
        }
        catch (CRLException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public X509CRL assemble(Serializable cached, Object owner) {
        return decodeCRL((byte[]) cached);
    }

    @Override
    public X509CRL replace(X509CRL detached, X509CRL managed, Object owner)
    {
        // According to javadoc:
        // For immutable objects, or null values, it is safe to simply return the first parameter.
        return detached;
    }
}
