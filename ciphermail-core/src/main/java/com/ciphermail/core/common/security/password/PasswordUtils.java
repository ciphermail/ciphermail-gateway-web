/*
 * Copyright (c) 2008-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import com.ciphermail.core.common.security.digest.Digest;
import com.ciphermail.core.common.security.digest.Digests;
import com.ciphermail.core.common.util.MiscStringUtils;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;

/**
 * General utils for handling passwords.
 *
 * @author Martijn Brinkers
 *
 */
public class PasswordUtils
{
    private PasswordUtils() {
        // empty on purpose
    }

    /**
     * Compares the passwords in a way to prevent a timing attack. The password is converted to a secure hash.
     * The hash is compared and not the passwords directly. This will prevent any time attack on password comparison
     */
    public static boolean isEqualDigest(String one, String other, Digest digest)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        if (one == null && other == null) {
            return true;
        }

        if (one == null || other == null) {
            return false;
        }

        byte[] oneHash = Digests.digest(MiscStringUtils.getBytesUTF8(one), digest);
        byte[] otherHash = Digests.digest(MiscStringUtils.getBytesUTF8(other), digest);

        return Arrays.equals(oneHash, otherHash);
    }

    /**
     * Overwrites the password with 0 values
     */
    public static void clearPassword(char[] password)
    {
        if (password != null)
        {
            for (int i = 0; i < password.length; i++) {
                password[i] = 0;
            }
        }
    }


    /**
     * Compares two passwords and returns true if the passwords match
     */
    public static boolean isEqual(String one, String other)
    {
        // First check with digesting the passwords to stop any timing attack on password lengths
        try {
            if (!isEqualDigest(one, other, Digest.SHA1)) {
                return false;
            }
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("SHA1 should always be available", e);
        }
        catch (NoSuchProviderException e) {
            throw new IllegalArgumentException("The provider should always be available", e);
        }

        return isEqualByteForByte(one, other);
    }

    /*
     * Checks whether the passwords are equal byte for byte.
     */
    static boolean isEqualByteForByte(String one, String other)
    {
        // Now check all bytes. It's highly unlikely that the password will not be equal since the SHA1 was equal.
        // It can however be that the passwords were encoded with a more secure algorithm so we will check byte for byte
        byte[] oneBytes = MiscStringUtils.getBytesUTF8(one);
        byte[] otherBytes = MiscStringUtils.getBytesUTF8(other);

        int expectedLength = oneBytes == null ? -1 : oneBytes.length;
        int actualLength = otherBytes == null ? -1 : otherBytes.length;

        if (expectedLength != actualLength) {
            return false;
        }

        int result = 0;

        for (int i = 0; i < expectedLength; i++) {
            result |= oneBytes[i] ^ otherBytes[i];
        }

        return result == 0;
    }
}
