/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.hibernate;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.util.SizeUtils;
import com.google.common.annotations.VisibleForTesting;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Entity(name = CertificateRequestEntity.ENTITY_NAME)
@Table(
indexes = {
    @Index(name = "certificaterequest_next_update_index", columnList = CertificateRequestEntity.NEXT_UPDATE_COLUMN_NAME),
    @Index(name = "certificaterequest_created_index", columnList = CertificateRequestEntity.CREATED_COLUMN_NAME),
    @Index(name = "certificaterequest_email_index", columnList = CertificateRequestEntity.EMAIL_COLUMN_NAME)
})
public class CertificateRequestEntity implements CertificateRequest
{
    static final String ENTITY_NAME = "CertificateRequest";

    static final String ID_COLUMN_NAME = "id";
    static final String EMAIL_COLUMN_NAME = "email";
    static final String CREATED_COLUMN_NAME = "created";
    static final String NEXT_UPDATE_COLUMN_NAME = "nextUpdate";

    /**
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255;

    @Id
    @Column(name = ID_COLUMN_NAME)
    @UuidGenerator
    private UUID id;

    @Column(name = CREATED_COLUMN_NAME, nullable = false)
    private Date created;

    @Column(length = SizeUtils.KB * 32)
    private X500Principal subject;

    @Column (name = EMAIL_COLUMN_NAME, length = MAX_EMAIL_LENGTH)
    private String email;

    @Column
    private int validity;

    @Column(length = SizeUtils.KB)
    private String signatureAlgorithm;

    @Column
    private int keyLength;

    @Column(length = SizeUtils.KB * 64)
    private String crlDistPoint;

    @Column(length = 255)
    private String certificateHandlerName;

    @Column(length = SizeUtils.KB)
    private String info;

    @Column(length = SizeUtils.MB * 10)
    private byte[] data;

    @Column
    private int iteration;

    @Column
    private Date lastUpdated;

    @Column(name = NEXT_UPDATE_COLUMN_NAME)
    private Date nextUpdate;

    @Column(length = SizeUtils.KB * 32)
    private String lastMessage;

    protected CertificateRequestEntity() {
        // required by Hibernate
    }

    public CertificateRequestEntity(@Nonnull String certificateHandlerName) {
        this(certificateHandlerName, new Date());
    }

    @VisibleForTesting
    protected CertificateRequestEntity(@Nonnull String certificateHandlerName, @Nonnull Date created)
    {
        this.certificateHandlerName = Objects.requireNonNull(certificateHandlerName);
        this.created = Objects.requireNonNull(created);
    }

    @Override
    public UUID getID() {
        return id;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setSubject(X500Principal subject) {
        this.subject = subject;
    }

    @Override
    public X500Principal getSubject() {
        return subject;
    }

    @Override
    public void setEmail(String email) {
        this.email = EmailAddressUtils.canonicalize(email);
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setValidity(int days) {
        this.validity = days;
    }
    @Override
    public int getValidity() {
        return validity;
    }

    @Override
    public void setSignatureAlgorithm(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    @Override
    public String getSignatureAlgorithm() {
        return signatureAlgorithm;
    }

    @Override
    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    @Override
    public int getKeyLength() {
        return keyLength;
    }

    @Override
    public void setCRLDistributionPoint(String crlDistPoint) {
        this.crlDistPoint = crlDistPoint;
    }

    @Override
    public String getCRLDistributionPoint() {
        return crlDistPoint;
    }

    @Override
    public String getCertificateHandlerName() {
        return certificateHandlerName;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public byte[] getData() {
        return data;
    }

    @Override
    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    @Override
    public int getIteration() {
        return iteration;
    }

    @Override
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setNextUpdate(Date nextUpdate) {
        this.nextUpdate = nextUpdate;
    }

    @Override
    public Date getNextUpdate() {
        return nextUpdate;
    }

    @Override
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    @Override
    public String getLastMessage() {
        return lastMessage;
    }
}
