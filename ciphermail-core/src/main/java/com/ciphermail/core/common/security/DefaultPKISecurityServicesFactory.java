/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.certificate.validator.PKITrustCheckCertificateValidatorFactory;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certpath.TrustAnchorBuilder;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.crl.CRLPathBuilderFactory;
import com.ciphermail.core.common.security.crl.RevocationChecker;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;

import javax.annotation.Nonnull;
import java.util.Objects;

public class DefaultPKISecurityServicesFactory implements PKISecurityServicesFactory
{
    private final KeyAndCertStore keyAndCertStore;
    private final X509CertStoreExt rootStore;
    private final X509CRLStoreExt crlStore;
    private final TrustAnchorBuilder trustAnchorBuilder;
    private final RevocationChecker revocationChecker;
    private final CertificatePathBuilderFactory certificatePathBuilderFactory;
    private final CRLPathBuilderFactory crlPathBuilderFactory;
    private final PKITrustCheckCertificateValidatorFactory certificateValidatorFactory;

    public DefaultPKISecurityServicesFactory(
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull X509CertStoreExt rootStore,
            @Nonnull X509CRLStoreExt crlStore,
            @Nonnull TrustAnchorBuilder trustAnchorBuilder,
            @Nonnull RevocationChecker revocationChecker,
            @Nonnull CertificatePathBuilderFactory certificatePathBuilderFactory,
            @Nonnull CRLPathBuilderFactory crlPathBuilderFactory,
            @Nonnull PKITrustCheckCertificateValidatorFactory certificateValidatorFactory)
    {
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.rootStore = Objects.requireNonNull(rootStore);
        this.crlStore = Objects.requireNonNull(crlStore);
        this.trustAnchorBuilder = Objects.requireNonNull(trustAnchorBuilder);
        this.revocationChecker = Objects.requireNonNull(revocationChecker);
        this.certificatePathBuilderFactory = Objects.requireNonNull(certificatePathBuilderFactory);
        this.crlPathBuilderFactory = Objects.requireNonNull(crlPathBuilderFactory);
        this.certificateValidatorFactory = Objects.requireNonNull(certificateValidatorFactory);
    }

    @Override
    public PKISecurityServices createPKISecurityServices() {
        return new PKISecurityServicesImpl();
    }

    private class PKISecurityServicesImpl implements PKISecurityServices
    {
        @Override
        public KeyAndCertStore getKeyAndCertStore() {
            return keyAndCertStore;
        }

        @Override
        public X509CertStoreExt getRootStore() {
            return rootStore;
        }

        @Override
        public X509CRLStoreExt getCRLStore() {
            return crlStore;
        }

        @Override
        public CertificatePathBuilderFactory getCertificatePathBuilderFactory()
        {
            return certificatePathBuilderFactory;
        }

        @Override
        public CRLPathBuilderFactory getCRLPathBuilderFactory() {
            return crlPathBuilderFactory;
        }

        @Override
        public PKITrustCheckCertificateValidatorFactory getPKITrustCheckCertificateValidatorFactory() {
            return certificateValidatorFactory;
        }

        @Override
        public RevocationChecker getCRLStoreRevocationChecker() {
            return revocationChecker;
        }

        @Override
        public TrustAnchorBuilder getTrustAnchorBuilder() {
            return trustAnchorBuilder;
        }
    }
}
