/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.ValueAccess;
import org.hibernate.usertype.CompositeUserType;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

/**
 * Hibernate CompositeUserType that can be used to persist {@link Certificate}.
 *
 * @author Martijn Brinkers
 *
 */
public class CertificateUserType implements CompositeUserType<Certificate>
{
    private static final int CERTIFICATE_COLUMN_INDEX = 0;
    private static final int CERTIFICATE_TYPE_COLUMN_INDEX = 1;
    private static final int THUMBPRINT_COLUMN_INDEX = 2;

    public static final String CERTIFICATE_COLUMN_NAME = "certificate";
    public static final String CERTIFICATE_TYPE_COLUMN_NAME = "certificateType";
    public static final String THUMBPRINT_COLUMN_NAME = "thumbprint";

    public static class CertificateMapper {
        // in sorted order
        byte[] certificate;
        String certificateType;
        String thumbprint;
    }

    /*
     * This is used for serializing/deserializing Certificate
     */
    public static class EncodedCertificateAndType implements Serializable
    {
        EncodedCertificateAndType(byte[] encodedCertificate, String certificateType)
        {
            this.encodedCertificate = encodedCertificate;
            this.certificateType = certificateType;
        }

        private final byte[] encodedCertificate;
        private final String certificateType;
    }

    @Override
    public Object getPropertyValue(Certificate certificate, int propertyIndex)
    throws HibernateException
    {
        try {
            return switch (propertyIndex) {
                case CERTIFICATE_COLUMN_INDEX -> certificate.getEncoded();
                case CERTIFICATE_TYPE_COLUMN_INDEX -> certificate.getType();
                case THUMBPRINT_COLUMN_INDEX -> X509CertificateInspector.getThumbprint(certificate);
                default -> null;
            };
        }
        catch (CertificateEncodingException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new HibernateException(e);
        }
    }

    private static Certificate decodeCertificate(byte[] encodedCertificate, String certificateType)
    {
        Certificate certificate = null;

        if (encodedCertificate != null)
        {
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory(certificateType);

                certificate = factory.generateCertificate(
                        new ByteArrayInputStream(encodedCertificate));
            }
            catch (CertificateException | NoSuchProviderException e) {
                throw new HibernateException(e);
            }
        }

        return certificate;
    }

    @Override
    public Certificate instantiate(ValueAccess valueAccess, SessionFactoryImplementor sessionFactoryImplementor)
    {
        return decodeCertificate(
                valueAccess.getValue(CERTIFICATE_COLUMN_INDEX, byte[].class),
                valueAccess.getValue(CERTIFICATE_TYPE_COLUMN_INDEX, String.class));
    }

    @Override
    public Class<?> embeddable() {
        return CertificateMapper.class;
    }

    @Override
    public Class<Certificate> returnedClass() {
        return Certificate.class;
    }

    @Override
    public boolean equals(Certificate certificate1, Certificate certificate2)
    {
        if (certificate1 == certificate2) {
            return true;
        }

        if (certificate1 == null || certificate2 == null) {
            return false;
        }

        return certificate1.equals(certificate2);
    }

    @Override
    public int hashCode(Certificate certificate) {
        return certificate.hashCode();
    }

    @Override
    public Certificate deepCopy(Certificate certificate) {
        // According to javadoc:
        // It is not necessary to copy immutable objects, or null values, in which case it is safe to simply return the
        // argument.
        return certificate;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Certificate certificate)
    {
        try {
            return certificate != null ? new EncodedCertificateAndType(certificate.getEncoded(), certificate.getType())
                    : null;
        }
        catch (CertificateEncodingException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public Certificate assemble(Serializable cached, Object owner)
    {
        EncodedCertificateAndType encodedCertificateAndType = (EncodedCertificateAndType) cached;

        return decodeCertificate(encodedCertificateAndType.encodedCertificate,
                encodedCertificateAndType.certificateType);
    }

    @Override
    public Certificate replace(Certificate detached, Certificate managed, Object owner)
    {
        // According to javadoc:
        // For immutable objects, or null values, it is safe to simply return the first parameter.
        return detached;
    }
}
