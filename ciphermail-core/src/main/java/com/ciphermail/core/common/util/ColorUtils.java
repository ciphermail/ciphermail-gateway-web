/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Some general color utility methods.
 *
 * @author Martijn Brinkers
 *
 */
public class ColorUtils
{
    /*
     * Matcher that matches comma separated RGB colors (ex: 111,222,000)
     */
    private static final Pattern RGB_PATTERN = Pattern.compile(
            "([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})");

    private ColorUtils() {
        // empty on purpose
    }

    /**
     * Converts the command separated RGB color to Color. Returns null if rgb is an empty string.
     *
     * @throws IllegalArgumentException if the input is not valid
     */
    public static Color toColor(String rgb)
    {
        Color color = null;

        rgb = StringUtils.trimToNull(rgb);

        if (rgb != null)
        {
            Matcher matcher = RGB_PATTERN.matcher(rgb);

            if (!matcher.matches()) {
                throw new IllegalArgumentException("input is not a valid RGB value " + rgb);
            }

            color = new Color(
                    Integer.parseInt(matcher.group(1)),  /* R */
                    Integer.parseInt(matcher.group(2)),  /* G */
                    Integer.parseInt(matcher.group(3))); /* B */
        }

        return color;
    }

    /**
     * Converts the color to r,g,b. If color is null, null is returned.
     */
    public static String toString(Color color)
    {
        if (color == null) {
            return null;
        }

        return color.getRed() + "," + color.getGreen() + "," + color.getBlue();
    }
}
