/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.hibernate;

import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.Match;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.hibernate.ScrollMode;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CertificateRequestDAO extends GenericHibernateDAO
{
    private CertificateRequestDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static CertificateRequestDAO getInstance(@Nonnull SessionAdapter session) {
        return new CertificateRequestDAO(session);
    }

    // Note: We assume that this DAO will only work with CertificateRequest's that are instances of
    // CertificateRequestEntity's. This ties the implementation to a specific implementation of CertificateRequest.
    // If the DAO should be made more general all relevant data should be copied from the CertificateRequest. This
    // however requires some minor changes to the CertificateRequest interface to allow access to the raw encoded
    // public and private key.
    private CertificateRequestEntity toEntity(@Nonnull CertificateRequest request)
    {
        if (!(request instanceof CertificateRequestEntity certificateRequestEntity)) {
            throw new IllegalArgumentException("request is-not-a CertificateRequestEntity.");
        }

        return certificateRequestEntity;
    }

    public CertificateRequestEntity addRequest(@Nonnull CertificateRequest request) {
        return persist(toEntity(request));
    }

    public void deleteRequest(@Nonnull CertificateRequest request) {
        delete(toEntity(request));
    }

    public CertificateRequestEntity getNextRequest(Date notAfter)
    {
        // Finding the next request to handle will be a two-step process. First requests which are never handled
        // before are tried, i.e., request for which "next update" is null. If there are multiple requests for which
        // "next update" is not set, the first added (i.e., oldest created date) will be tried. If there are no
        // requests available with an empty "next update", a request for which the "next update" is older than the
        // provided date (notAfter) will be returned. If multiple requests match, the entry with the oldest
        // "next update" will be returned.
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CertificateRequestEntity> criteriaQuery = criteriaBuilder.createQuery(
                CertificateRequestEntity.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.where(criteriaBuilder.isNull(rootEntity.get(CertificateRequestEntity.NEXT_UPDATE_COLUMN_NAME)));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(CertificateRequestEntity.CREATED_COLUMN_NAME)));

        Query<CertificateRequestEntity> query = createQuery(criteriaQuery);

        query.setMaxResults(1);

        Optional<CertificateRequestEntity> optionalResult = query.uniqueResultOptional();

        CertificateRequestEntity next = null;

        if (optionalResult.isPresent()) {
            next = optionalResult.get();
        }
        else {
            // Try to find the next to be tried
            criteriaQuery = criteriaBuilder.createQuery(CertificateRequestEntity.class);

            rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

            // Only select items which have a next update time which is before not after
            criteriaQuery.where(criteriaBuilder.lessThan(
                    rootEntity.get(CertificateRequestEntity.NEXT_UPDATE_COLUMN_NAME), notAfter));

            criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(
                    CertificateRequestEntity.NEXT_UPDATE_COLUMN_NAME)));

            query = createQuery(criteriaQuery);

            query.setMaxResults(1);

            optionalResult = query.uniqueResultOptional();

            if (optionalResult.isPresent()) {
                next = optionalResult.get();
            }
        }

        return next;
    }

    public long getSize()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private Predicate createEmailRestriction(@Nonnull CriteriaBuilder bld, @Nonnull Root<CertificateRequestEntity> root,
            @Nonnull String email, Match match)
    {
        email = EmailAddressUtils.canonicalize(email);

        if (match == null) {
            match = Match.EXACT;
        }

        return switch (match) {
            case EXACT -> bld.equal(root.get(CertificateRequestEntity.EMAIL_COLUMN_NAME), email);
            case LIKE  -> bld.like(bld.lower(root.get(CertificateRequestEntity.EMAIL_COLUMN_NAME)), email);
            default -> throw new IllegalArgumentException("Unknown Match. " + match);
        };
    }

    public long getSizeByEmail(@Nonnull String email, Match match)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        criteriaQuery.where(createEmailRestriction(criteriaBuilder, rootEntity, email, match));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private Order createSortOnCreationDateOrder(@Nonnull CriteriaBuilder bld,
            @Nonnull Root<CertificateRequestEntity> root)
    {
        // sort on creation date
        return bld.asc(root.get(CertificateRequestEntity.CREATED_COLUMN_NAME));
    }

    public List<CertificateRequestEntity> getAllRequests(Integer firstResult, Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CertificateRequestEntity> criteriaQuery = criteriaBuilder.createQuery(
                CertificateRequestEntity.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.orderBy(createSortOnCreationDateOrder(criteriaBuilder, rootEntity));

        Query<CertificateRequestEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    public CloseableIterator<CertificateRequestEntity> getAllRequests()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CertificateRequestEntity> criteriaQuery = criteriaBuilder.createQuery(
                CertificateRequestEntity.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.orderBy(createSortOnCreationDateOrder(criteriaBuilder, rootEntity));

        return new CertificateRequestIterator(createQuery(criteriaQuery).scroll(ScrollMode.FORWARD_ONLY));
    }

    public List<CertificateRequestEntity> getRequestsByEmail(@Nonnull String email, Match match, Integer firstResult,
            Integer maxResults)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<CertificateRequestEntity> criteriaQuery = criteriaBuilder.createQuery(
                CertificateRequestEntity.class);

        Root<CertificateRequestEntity> rootEntity = criteriaQuery.from(CertificateRequestEntity.class);

        criteriaQuery.orderBy(createSortOnCreationDateOrder(criteriaBuilder, rootEntity));

        criteriaQuery.where(createEmailRestriction(criteriaBuilder, rootEntity, email, match));

        Query<CertificateRequestEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }
}
