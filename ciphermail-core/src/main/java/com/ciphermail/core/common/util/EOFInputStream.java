/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * InputStream wrapper that sets an EOF flag when EOF is reached
 *
 * @author Martijn Brinkers
 *
 */
public class EOFInputStream extends InputStream
{
    /*
     * The wrapped InpuStream
     */
    private final InputStream delegate;

    /*
     * True if eof is reached
     */
    private boolean eof;

    public EOFInputStream(@Nonnull InputStream delegate) {
        this.delegate = Objects.requireNonNull(delegate);
    }

    @Override
    public int read()
    throws IOException
    {
        int c = delegate.read();

        if (c == -1) {
            eof = true;
        }

        return c;
    }

    @Override
    public int read(@Nonnull byte[] b)
    throws IOException
    {
        int c = delegate.read(b);

        if (c == -1) {
            eof = true;
        }

        return c;
    }

    @Override
    public int read(@Nonnull byte[] b, int off, int len)
    throws IOException
    {
        int c = delegate.read(b, off, len);

        if (c == -1) {
            eof = true;
        }

        return c;
    }

    @Override
    public long skip(long n)
    throws IOException
    {
        return delegate.skip(n);
    }

    @Override
    public int available()
    throws IOException
    {
        return delegate.available();
    }

    @Override
    public void close()
    throws IOException
    {
        delegate.close();
    }

    @Override
    public void mark(int i)
    {
        delegate.mark(i);
    }

    @Override
    public synchronized void reset()
    throws IOException
    {
        delegate.reset();
    }

    @Override
    public boolean markSupported() {
        return delegate.markSupported();
    }

    public boolean isEOF() {
        return eof;
    }
}
