/*
 * Copyright (c) 2008-2018, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.clickatell;

/**
 * Clickatell error codes.
 *
 * @author Martijn Brinkers
 *
 */
public enum ErrorCode
{
    AUTHENTICATION_FAILED             ("001", "Authentication failed"),
    UNKNOWN_USERNAME_OR_PASSWORD      ("002", "Unknown username or password"),
    SESSION_ID_EXPIRED                ("003", "Session ID expired"),
    ACCOUNT_FROZEN                    ("004", "Account frozen"),
    MISSING_SESSION_ID                ("005", "Missing session ID"),
    IP_LOCKDOWN_VIOLATION             ("007", "IP Lockdown violation"),
    INVALID_OR_MISSING_PARAMETERS     ("101", "Invalid or missing parameters"),
    INVALID_USER_DATA_HEADER          ("102", "Invalid user data header"),
    UNKNOWN_API_MESSAGE_ID            ("103", "Unknown API message ID"),
    UNKNOWN_MESSAGE_CLIENT_ID         ("104", "Unknown client message ID"),
    INVALID_DESTINATION_ADDRESS       ("105", "Invalid destination address"),
    INVALID_SOURCE_ADDRESS            ("106", "Invalid source address"),
    EMPTY_MESSAGE                     ("107", "Empty message"),
    INVALID_OR_MISSING_API_ID         ("108", "Invalid or missing API ID"),
    MISSING_MESSAGE_ID                ("109", "Missing message ID"),
    ERROR_WITH_EMAIL_MESSAGE          ("110", "Error with email message"),
    INVALID_PROTOCOL                  ("111", "Invalid protocol"),
    INVALID_MESSAGE_TYPE              ("112", "Invalid message type"),
    MAXIMUM_MESSAGE_PARTS_EXCEEDED    ("113", "Maximum message parts exceeded"),
    CANNOT_ROUTE_MESSAGE              ("114", "Cannot route message"),
    MESSAGE_EXPIRED                   ("115", "Message expired"),
    INVALID_UNICODE_DATA              ("116", "Invalid Unicode data"),
    INVALID_DELIVERY_TIME             ("120", "Invalid delivery time"),
    DESTINATION_MOBILE_NUMBER_BLOCKED ("121", "Destination mobile number blocked"),
    DESTINATION_MOBILE_OPTED_OUT      ("122", "Destination mobile opted out"),
    INVALID_BATCH_ID                  ("201", "Invalid batch ID"),
    NO_BATCH_TEMPLATE                 ("202", "No batch template"),
    NO_CREDIT_LEFT                    ("301", "No credit left"),
    MAX_ALLOWED_CREDIT                ("302", "Max allowed credit"),
    UNKNOWN                           ("UNKNOWN", "Unknown error");


    private final String errorNumber;
    private final String description;

    ErrorCode(String errorNumber, String description)
    {
        this.errorNumber = errorNumber;
        this.description = description;
    }

    public String getErrorNumber() {
        return errorNumber;
    }

    public String getDescription() {
        return description;
    }

    public static ErrorCode getErrorCode(String errorNumber)
    {
        for (ErrorCode code : ErrorCode.values())
        {
            if (code.getErrorNumber().equalsIgnoreCase(errorNumber)) {
                return code;
            }
        }

        return ErrorCode.UNKNOWN;
    }
}
