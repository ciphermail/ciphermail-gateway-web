/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.validator;

import org.apache.commons.lang.CharSet;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;


/**
 * Validator implementation that checks whether the input is a valid DEA number (DEA Registration Number)
 * <p>
 * <a href="https://en.wikipedia.org/wiki/DEA_number">...</a>
 *
 */
public class DEAValidator extends AbstractBaseValidator
{
    public DEAValidator() {
        super("DEA", "Checks whether the input is a valid DEA Registration Number");
    }

    private int getInt(String digits, int position) {
        return digits.charAt(position) - '0';
    }

    @Override
    public boolean isValid(String input)
    {
        input = StringUtils.deleteWhitespace(input);

        if (StringUtils.isEmpty(input) || input.length() != 9) {
            return false;
        }

        // First character should be a registrant type (first letter of DEA Number). Only certain characters are
        // allowed but it might be that new ones will be added in the future so we allow everything a-z
        if (!CharSet.ASCII_ALPHA.contains(input.charAt(0))) {
            return false;
        }

        // Second char is the first letter of the registrant's last name (but number 9 is also allowed)
        if (!CharSet.ASCII_ALPHA.contains(input.charAt(1)) && '9' != input.charAt(1)) {
            return false;
        }

        // The next seven chars should be digits and the last should be a checksum
        String digits = StringUtils.substring(input, 2, 8);

        char checksumDigit = input.charAt(8);

        if (!NumberUtils.isDigits(digits)) {
            return false;
        }

        if (!Character.isDigit(checksumDigit)) {
            return false;
        }

        // Calculate checksum and compare
        int calc135 = getInt(digits, 0) + getInt(digits, 2) + getInt(digits, 4);
        int calc246 = 2 * (getInt(digits, 1) + getInt(digits, 3) + getInt(digits, 5));
        int check = calc135 + calc246;

        String checkDigits = Integer.toString(check);

        if (checkDigits.charAt(checkDigits.length() - 1) != checksumDigit) {
            // Checksum failure
            return false;
        }

        return true;
    }
}
