/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import com.ciphermail.core.common.extractor.ExtractedPart;
import com.ciphermail.core.common.extractor.TextExtractorContext;
import com.ciphermail.core.common.extractor.TextExtractorEventHandler;
import com.ciphermail.core.common.util.FileConstants;
import com.ciphermail.core.common.util.MiscIOUtils;
import com.ciphermail.core.common.util.RewindableInputStream;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.DeferredFileOutputStream;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

/**
 * General helper methods for TextExtractor's
 *
 * @author Martijn Brinkers
 *
 */
public class TextExtractorUtils
{
    private TextExtractorUtils() {
        // Empty on purpose
    }

    /**
     * Closes the ExtractedPart without throwing an exception
     */
    public static void closeQuitely(ExtractedPart part)
    {
        if (part == null) {
            return;
        }

        try {
            part.close();
        }
        catch (IOException e) {
            // ignore
        }
    }

    /**
     * Closes all the ExtractedPart's without throwing an exception
     */
    public static void closeQuitely(Collection<? extends ExtractedPart> parts)
    {
        if (parts == null) {
            return;
        }

        for (ExtractedPart part : parts) {
            closeQuitely(part);
        }
    }

    public interface TextExtractorWriterHandler
    {
        void write(Writer writer)
        throws IOException;
    }

    public interface TextExtractorAttachmentHandler
    {
        void write(OutputStream output)
        throws IOException;
    }

    /**
     * Creates a name for an embedded document based on the parent name and the name of the
     * embedded document (can be a word, excel, image etc.)
     */
    public static String createEmbeddedName(String parentName, String childName)
    {
        return StringUtils.isNotEmpty(parentName) ? parentName + "/" + StringUtils.defaultString(childName) :
                StringUtils.defaultString(childName);
    }

    /**
     * Convenience method that writes data and fires a
     * {@link TextExtractorEventHandler#textEvent(mitm.common.extractor.ExtractedPart)} event. The Data is written
     * to disk if total bytes exceed threshold. If total bytes written exceeds maxSize, an IOException will
     * be thrown.
     */
    public static void fireTextEvent(@Nonnull TextExtractorEventHandler handler, @Nonnull TextExtractorContext context,
            @Nonnull TextExtractorWriterHandler writerHandler, int threshold, long maxSize)
    throws IOException
    {
        DeferredFileOutputStream output = DeferredFileOutputStream.builder().setThreshold(threshold)
                .setPrefix(FileConstants.TEMP_FILE_PREFIX).get();

        RewindableInputStream content = null;

        try {
            // Wrap the output in a buffered stream and protect against 'zip bombs'.
            SizeLimitedOutputStream buffered = new SizeLimitedOutputStream(
                    new BufferedOutputStream(output), maxSize);

            Writer writer = new OutputStreamWriter(buffered, StandardCharsets.UTF_8);

            try {
                writerHandler.write(writer);
            }
            finally {
                // Must close to make sure all data is written.
                //
                // Note: if DeferredFileOutputStream uses a file, the file should NOT
                // be deleted here because it will be deleted when the returned
                // DeferredBufferedInputStream is closed.
                writer.close();
            }

            content = new RewindableInputStream(MiscIOUtils.toInputStream(output), threshold);

            handler.textEvent(new ExtractedPartImpl(context, content, buffered.getByteCount()));
        }
        catch(IOException | RuntimeException e)
        {
            // Must close the RewindableInputStream to prevent a possible temp file leak
            IOUtils.closeQuietly(content);

            // Delete the tempfile (if) used by DeferredFileOutputStream.
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }
    }

    /**
     * Convenience method that writes data and fires a
     * {@link TextExtractorEventHandler#textEvent(mitm.common.extractor.ExtractedPart)} event. The Data is written
     * to disk if total bytes exceed threshold. If total bytes written exceeds maxSize, an IOException will
     * be thrown.
     *
     */
    public static void fireAttachmentEvent(@Nonnull TextExtractorEventHandler handler,
            @Nonnull TextExtractorContext context, @Nonnull TextExtractorAttachmentHandler attachmentHandler,
            int threshold, long maxSize)
    throws IOException
    {
        DeferredFileOutputStream output = DeferredFileOutputStream.builder().setThreshold(threshold)
                .setPrefix(FileConstants.TEMP_FILE_PREFIX).get();

        RewindableInputStream content = null;

        try {
            // Wrap the output in a buffered stream and protect against 'zip bombs'.
            SizeLimitedOutputStream buffered = new SizeLimitedOutputStream(
                    new BufferedOutputStream(output), maxSize);

            try {
                attachmentHandler.write(buffered);
            }
            finally {
                // Must close to make sure all data is written.
                //
                // Note: if DeferredFileOutputStream uses a file, the file should NOT
                // be deleted here because it will be deleted when the returned
                // DeferredBufferedInputStream is closed.
                buffered.close();
            }

            content = new RewindableInputStream(MiscIOUtils.toInputStream(output), threshold);

            handler.attachmentEvent(new ExtractedPartImpl(context, content, buffered.getByteCount()));
        }
        catch(IOException | RuntimeException e)
        {
            // Must close the RewindableInputStream to prevent a possible temp file leak
            IOUtils.closeQuietly(content);

            // Delete the tempfile (if) used by DeferredFileOutputStream.
            IOUtils.closeQuietly(output);
            FileUtils.deleteQuietly(output.getFile());

            throw e;
        }
    }
}
