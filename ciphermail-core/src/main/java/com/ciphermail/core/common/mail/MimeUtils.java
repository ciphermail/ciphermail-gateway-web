/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.locale.CharacterEncoding;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.SizeLimitedInputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.annotation.Nonnull;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeUtility;
import javax.mail.internet.ParseException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

/**
 * General MIME utility functions
 *
 * @author Martijn Brinkers
 *
 */
public class MimeUtils
{
    private static final Logger logger = LoggerFactory.getLogger(MimeUtils.class);

    private MimeUtils() {
        // empty on purpose
    }

    /**
     * Returns the filename of the part. If getting the filename results in a
     * MessagingException the exception is handled and null is returned.
     * <p>
     * The reason this function is used is that sometimes mail contains an invalid
     * Content-Disposition header. This results in an exception being thrown when
     * accessing the filename.
     */
    public static String getFilenameQuietly(@Nonnull Part part)
    {
        String filename = null;

        try {
            filename = part.getFileName();
        }
        catch (MessagingException e) {
            logger.debug("Error while getting the filename.", e);
        }

        return filename;
    }

    /**
     * Returns true if the part does not have a Content-Disposition header or if the Content-Disposition header is
     * different than attachment
     */
    public static boolean isInline(@Nonnull Part part)
    throws MessagingException
    {
        return !Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition());
    }

    /**
     * Returns true if the part does not have a Content-Disposition header or if the Content-Disposition header is
     * different than attachment. If the disposition header is invalid, the default value will be returned
     */
    public static boolean isInline(@Nonnull Part part, boolean defaultIfInvalid)
    {
        boolean inline;

        try {
            inline = isInline(part);
        }
        catch (MessagingException e)
        {
            logger.debug("Error part#getDisposition()", e);

            inline = defaultIfInvalid;
        }

        return inline;
    }

    /**
     * Returns true if the part has a Content-Disposition header and is equal to attachment.
     */
    public static boolean isAttachment(@Nonnull Part part)
    throws MessagingException
    {
        return Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition());
    }

    /**
     * Returns true if the part has a Content-Disposition header and is equal to attachment. If the disposition header
     * is invalid, the default value will be returned
     */
    public static boolean isAttachment(@Nonnull Part part, boolean defaultIfInvalid)
    {
        boolean attachment;

        try {
            attachment = Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition());
        }
        catch (MessagingException e)
        {
            logger.debug("Error part#getDisposition()", e);

            attachment = defaultIfInvalid;
        }

        return attachment;
    }

    /**
     * Returns the first content-id header value if available. Null if there is content-id header
     */
    public static String getContentID(@Nonnull Part part)
    throws MessagingException
    {
        String result = null;

        String[] values = part.getHeader("content-id");

        if (values != null && values.length > 0) {
            result = HeaderUtils.removeAngleBrackets(values[0]);
        }

        return result;
    }

    /**
     * Returns the content type text/subtype for the text. The charset is ascii if text only contains ascii characters.
     * If text contains non-ascii characters the encoding will be charsetIfNotAscii.
     */
    public static String getContentTypeForText(String text, String charsetIfNotAscii, @Nonnull String subtype)
    {
        String charset = CharacterEncoding.US_ASCII;

        if(!MiscStringUtils.isPrintableAscii(text)) {
            charset = charsetIfNotAscii;
        }

        return "text/" + subtype + "; charset=" + MimeUtility.quote(charset, "()<>@,;:\\\"\t []/?=");
    }

    /**
     * Parses the contentType and returns the Java Charset found in the content type. Null if charset
     * parameter is not found.
     */
    public static String getCharsetFromContentType(@Nonnull String contentType)
    throws MimeTypeParseException
    {
        MimeType mimeType = new MimeType(contentType);

        String charset = mimeType.getParameter("charset");

        return StringUtils.isNotEmpty(charset) ? MimeUtility.javaCharset(charset) : null;
    }

    /**
     * Tries to detect whether the input is MIME by reading max 128K chars (Postfix default max header length is 102400)
     * and testing whether an empty line is found and whether all lined before the empty line are valid headers.
     * It is assumed that the input is ASCII.
     */
    public static boolean isMIME(InputStream input)
    throws IOException
    {
        return isMIME(input, SizeUtils.KB * 128);
    }

    private static boolean hasHeader(@Nonnull InternetHeaders headers, @Nonnull String... names)
    {
        for (String name : names)
        {
            if (headers.getHeader(name) != null) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the Content-Transfer-Encoding header. Null
     * @throws MessagingException
     */
    public static String getContentTransferEncoding(@Nonnull Part part)
    throws MessagingException
    {
        String encoding = null;

        String[] headers = part.getHeader("Content-Transfer-Encoding");

        if (headers != null && headers.length > 0) {
            encoding = StringUtils.trimToNull(headers[0]);
        }

        return encoding;
    }

    /**
     * Tries to detect whether the input is MIME by reading max chars and testing whether an empty line is found
     * and whether all lined before the empty line are valid headers. It is assumed that the input is ASCII.
     */
    public static boolean isMIME(@Nonnull InputStream input, int maxSize)
    throws IOException
    {
        boolean result = false;

        SizeLimitedInputStream limitedInput = new SizeLimitedInputStream(input, maxSize);

        try {
            InternetHeaders headers = new InternetHeaders(limitedInput);

            /*
             * Validate all the headers
             */
            Enumeration<?> headerEnum = headers.getAllHeaders();

            boolean hasHeader = false;

            while (headerEnum.hasMoreElements())
            {
                hasHeader = true;

                Header header = (Header) headerEnum.nextElement();

                if (StringUtils.isEmpty(header.getName())) {
                    // Fast fail. Header name should not be empty
                    logger.debug("header name should not be empty");

                    return false;
                }

                if (!MiscStringUtils.isPrintableAscii(header.getName())) {
                    // Fast fail. Header should be printable ASCII
                    logger.debug("header name should only contain printable characters");

                    return false;
                }
            }

            // The mime should at least contain one of these headers
            if (!hasHeader(headers, "Content-Type", "Message-ID", "Subject", "MIME-Version", "Date", "From", "To",
                    "CC", "Received"))
            {
                // Fast fail.
                logger.debug("MIME contains no standard header");

                return false;
            }

            if (hasHeader) {
                result = true;
            }
        }
        catch (MessagingException e) {
            // Some problem with the input or more than the max size bytes were read before the header/body separator
            logger.debug("Error reading headers", e);
        }

        return result;
    }

    /**
     * Returns true if the part is of the given MIME type
     */
    public static boolean isMimeType(@Nonnull Part part, String mimeType)
    throws MessagingException
    {
        try {
            ContentType ct = new ContentType(part.getContentType());

            return ct.match(mimeType);
        }
        catch (ParseException ex) {
            return part.getContentType().equalsIgnoreCase(mimeType);
        }
    }

    /**
     * Returns true if the multipart is of the given MIME type
     */
    public static boolean isMimeType(@Nonnull Multipart part, String mimeType)
    throws MessagingException
    {
        try {
            ContentType ct = new ContentType(part.getContentType());

            return ct.match(mimeType);
        }
        catch (ParseException ex) {
            return part.getContentType().equalsIgnoreCase(mimeType);
        }
    }

    /**
     * Converts the java charset name to MIME charset name
     */
    public static String fromJavaCharsetToMimeCharset(String javaCharset)
    {
        if (javaCharset == null) {
            return null;
        }

        String mimeCharset;

        if ("UTF8".equalsIgnoreCase(javaCharset)) {
            mimeCharset = "UTF-8";
        }
        else {
            mimeCharset = MimeUtility.mimeCharset(javaCharset);
        }

        if (mimeCharset == null) {
            mimeCharset = javaCharset;
        }

        return mimeCharset;
    }
}
