/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UuidGenerator;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity(name = PGPKeyRingEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={PGPKeyRingEntity.KEY_RING_NAME_COLUMN,
        PGPKeyRingEntity.SHA256_FINGERPRINT_COLUMN})}
)
public class PGPKeyRingEntity
{
    /*
     * The name of the Hibernate entity
     */
    static final String ENTITY_NAME = "PgpKeyRing";

    /**
     * Name of columns which are used more than once
     */
    static final String KEY_RING_NAME_COLUMN = "keyRingName";
    static final String SHA256_FINGERPRINT_COLUMN = "sha256Fingerprint";

    /*
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255;

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /*
     * The store name will be used to store multiple certificate stores in the database
     */
    @Column (name = KEY_RING_NAME_COLUMN, length = 255, nullable = false)
    private String keyRingName;

    /*
     * Identifies a key. Implementations SHOULD NOT assume that Key IDs are unique.
     */
    @Column (name = "keyId", length = SizeUtils.KB, nullable = false)
    private Long keyID;

    /*
     * The key ID in hex format (for searching purposes)
     */
    @Column (name = "keyIdHex", length = SizeUtils.KB * 2, nullable = false)
    private String keyIDHex;

    /*
     * The key ID of the master key. Null if this key is a master key.
     */
    @Column (name = "parentKeyId", length = SizeUtils.KB)
    private Long parentKeyID;

    /*
     * The creation date of the key
     */
    @Column (name = "creationDate")
    private Date creationDate;

    /*
     * The expiration date of the key. Null means no expiration.
     */
    @Column (name = "expirationDate")
    private Date expirationDate;

    /*
     * The date this entity was inserted
     */
    @Column (name = "insertionDate")
    private Date insertionDate;

    /*
     * The encoded public key
     */
    @Column (name = "publicKey", length = SizeUtils.MB * 32, nullable = false)
    private byte[] encodedPublicKey;

    /*
     * The alias of the private key or null if there is no associated private key.
     */
    @Column (name = "privateKeyAlias", length = 255)
    private String privateKeyAlias;

    /*
     * The fingerprint (hash) of the key. The fingerprint algorithm used depends on whether the key is a V3 of V4 key.
     */
    @Column (name = "fingerprint", length = 255, nullable = false)
    private String fingerprint;

    /*
     * The SHA256 fingerprint (hash) of the key.
     */
    @Column (name = SHA256_FINGERPRINT_COLUMN, length = 255, nullable = false)
    private String sha256Fingerprint;

    /*
     * A collection of user IDs
     */
    @ElementCollection
    @JoinTable(name = "pgpKeyRingUserId",
            joinColumns = @JoinColumn(name = "keyRingId"))
    @Column(name = "userId", length = MAX_EMAIL_LENGTH)
    private Set<String> userID = new HashSet<>();

    /*
     * A collection of email addresses
     */
    @ElementCollection
    @JoinTable(name = "keyRingEmail",
            joinColumns = @JoinColumn(name = "keyRingId"))
    @Column(name = "email", length = MAX_EMAIL_LENGTH)
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<String> email = new HashSet<>();

    /*
     * True if the key is a master key
     */
    @Column (name = "master", unique = false, nullable = false)
    private boolean master;

    @ManyToOne
    @JoinColumn(name = "parentId")
    @NotFound(action = NotFoundAction.IGNORE)
    private PGPKeyRingEntity parent;

    @OneToMany(mappedBy = "parent")
    @Cascade(CascadeType.ALL)
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<PGPKeyRingEntity> subkeys = new LinkedHashSet<>();

    protected PGPKeyRingEntity() {
        // Hibernate requires a default constructor
    }

    public PGPKeyRingEntity(PGPPublicKey publicKey, String keyRingName)
    throws PGPException
    {
        this.keyRingName = Objects.requireNonNull(keyRingName);

        setPGPPublicKey(Objects.requireNonNull(publicKey));
    }

    public void setPGPPublicKey(PGPPublicKey publicKey)
    throws PGPException
    {
        try {
            this.keyID = publicKey.getKeyID();
            this.keyIDHex = PGPUtils.getKeyIDHex(publicKey.getKeyID());

            // Note: the sub key binding signature is not checked for validity. Sub key binding signature checking
            // requires additional steps.
            // Use @mitm.common.security.openpgp.validator.ExpirationValidator.SubKeyBindingSignatureValidator for
            // validating the sub key binding
            Long subkeyBindingSignatureKeyID = PGPPublicKeyInspector.getSubkeyBindingSignatureKeyID(publicKey);

            // If the sub key binding sig points to the key itself, do not set parent
            this.parentKeyID = !ObjectUtils.equals(this.keyID, subkeyBindingSignatureKeyID) ?
                subkeyBindingSignatureKeyID : null;

            this.creationDate = publicKey.getCreationTime();
            // Note: This expiration date should only be used as an indication for the end user. The signatures of the
            // User IDs are not checked. Whether or not the key is really expired requires additional checks
            // (see @mitm.common.security.openpgp.validator.ExpirationValidator)
            this.expirationDate = PGPPublicKeyInspector.getExpirationDate(publicKey);
            this.insertionDate = new Date();
            this.encodedPublicKey = publicKey.getEncoded();
            this.fingerprint = PGPPublicKeyInspector.getFingerprintHex(publicKey);
            this.sha256Fingerprint = PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey);
            this.userID = PGPPublicKeyInspector.getUserIDsAsStrings(publicKey);
            this.master = publicKey.isMasterKey();
        }
        catch (IOException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new PGPException("Error creating PGPKeyRingEntity", e);
        }
    }

    public UUID getID() {
        return id;
    }

    public String getKeyRingName() {
        return keyRingName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getInsertionDate() {
        return insertionDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public Long getKeyID() {
        return keyID;
    }

    public Long getParentKeyID() {
        return parentKeyID;
    }

    public byte[] getEncodedPublicKey() {
        return encodedPublicKey;
    }

    public void setPrivateKeyAlias(String privateKeyAlias) {
        this.privateKeyAlias = privateKeyAlias;
    }

    public String getPrivateKeyAlias() {
        return privateKeyAlias;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public String getSHA256Fingerprint() {
        return sha256Fingerprint;
    }

    public Set<String> getUserIDs() {
        return userID;
    }

    public Set<String> getEmail() {
        return email;
    }

    public boolean isMasterKey() {
        return master;
    }

    public void setParentKey(PGPKeyRingEntity  parent) {
        this.parent= parent;
    }

    public PGPKeyRingEntity getParentKey() {
        return parent;
    }

    public Set<PGPKeyRingEntity> getSubkeys() {
        return subkeys;
    }

    @Override
    public int hashCode()
    {
        HashCodeBuilder builder = new HashCodeBuilder();

        builder.append(sha256Fingerprint);
        builder.append(keyRingName);

        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }

        if (!(obj instanceof PGPKeyRingEntity rhs)) {
            return false;
        }

        return new EqualsBuilder()
            .append(sha256Fingerprint, rhs.sha256Fingerprint)
            .append(keyRingName, rhs.keyRingName)
            .isEquals();
    }
}
