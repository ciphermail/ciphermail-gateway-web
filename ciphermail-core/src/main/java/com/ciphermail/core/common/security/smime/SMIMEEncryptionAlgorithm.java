/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The encryption algorithms that are supported by the s/mime modules.
 */
public enum SMIMEEncryptionAlgorithm
{
    // Note: algorithms are ordered from strongest to weakest since this oder will also be used
    // with S/MIME capabilities
    AES256_GCM      (NISTObjectIdentifiers.id_aes256_GCM, "AES256_GCM",  true,  256, true),
    AES192_GCM      (NISTObjectIdentifiers.id_aes192_GCM, "AES192_GCM",  true,  192, true),
    AES128_GCM      (NISTObjectIdentifiers.id_aes128_GCM, "AES128_GCM",  true,  128, true),
    AES256_CBC      (NISTObjectIdentifiers.id_aes256_CBC, "AES256",      true,  256, false),
    AES192_CBC      (NISTObjectIdentifiers.id_aes192_CBC, "AES192",      true,  192, false),
    AES128_CBC      (NISTObjectIdentifiers.id_aes128_CBC, "AES128",      true,  128, false),
    DES_EDE3_CBC    (PKCSObjectIdentifiers.des_EDE3_CBC,  "3DES",        true,  168, false);

    private static final Logger logger = LoggerFactory.getLogger(SMIMEEncryptionAlgorithm.class);

    private final ASN1ObjectIdentifier oid;
    private final String algorithm;
    private final String friendlyName;
    private final boolean fixedSize;
    private final int defaultKeySize;
    private final boolean auth;

    SMIMEEncryptionAlgorithm(
            ASN1ObjectIdentifier oid,
            String friendlyName,
            boolean fixedSize,
            int defaultKeySize,
            boolean auth)
    {
        this.oid = oid;
        this.algorithm = oid.getId();
        this.friendlyName = friendlyName;
        this.fixedSize = fixedSize;
        this.defaultKeySize = defaultKeySize;
        this.auth = auth;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public ASN1ObjectIdentifier getOID() {
        return oid;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public boolean isFixedSize() {
        return fixedSize;
    }

    public int defaultKeySize() {
        return defaultKeySize;
    }

    public boolean isAuth() {
        return auth;
    }

    public static SMIMEEncryptionAlgorithm fromOID(String oid)
    {
        for (SMIMEEncryptionAlgorithm algorithm : SMIMEEncryptionAlgorithm.values())
        {
            if (algorithm.oid.getId().equals(oid)) {
                return algorithm;
            }
        }

        return null;
    }

    public static SMIMEEncryptionAlgorithm fromName(String name)
    {
        name = StringUtils.trimToNull(name);

        if (name != null)
        {
            for (SMIMEEncryptionAlgorithm algorithm : SMIMEEncryptionAlgorithm.values())
            {
                if (algorithm.name().equalsIgnoreCase(name)) {
                    return algorithm;
                }
            }
        }

        return null;
    }

    /**
     * Returns the effective key size (in bits) of the given algorithm. Returns -1 if the key size
     * cannot be determined.
     * @param algorithm
     * @return
     */
    public static int getKeySize(SMIMEEncryptionAlgorithm algorithm)
    {
        int strength = -1;

        if (algorithm == null) {
            return strength;
        }

        if (algorithm.fixedSize) {
            strength = algorithm.defaultKeySize();
        }

        return strength;
    }
}
