/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.hibernate;

import com.ciphermail.core.common.hibernate.CloseSessionOnCloseIterator;
import com.ciphermail.core.common.hibernate.HibernateUtils;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.crlstore.CRLStoreException;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreExt;
import com.ciphermail.core.common.security.crlstore.dao.X509CRLStoreDAO;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.jdbc.ReturningWork;

import javax.annotation.Nonnull;
import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Objects;

public class X509CRLStoreExtHibernate implements X509CRLStoreExt
{
    /*
     * Name of the CRL Store. This can be used to have multiple distinct CRL stores in the database.
     */
    private final String storeName;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    public X509CRLStoreExtHibernate(@Nonnull String storeName, @Nonnull SessionManager sessionManager)
    {
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.storeName = Objects.requireNonNull(storeName);
    }

    @Override
    public void addCRL(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        getDAO().addCRL(crl);
    }

    @Override
    public X509CRL getCRL(@Nonnull String thumbprint)
    throws CRLStoreException
    {
        return getDAO().getCRL(thumbprint);
    }

    @Override
    public void remove(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        getDAO().remove(crl);
    }

    @Override
    public void replace(@Nonnull X509CRL oldCRL, @Nonnull X509CRL newCRL)
    throws CRLStoreException
    {
        getDAO().replace(oldCRL, newCRL);
    }

    @Override
    public boolean contains(@Nonnull X509CRL crl)
    throws CRLStoreException
    {
        return getDAO().contains(crl);
    }

    @Override
    public Collection<X509CRL> getCRLs(CRLSelector crlSelector)
    throws CRLStoreException
    {
        return getCRLs(crlSelector, null, null);
    }

    /*
     * A call to getCertificates can result in a lot of records being returned which can result in an
     * out of memory because the first level cache grows with every object. There are a lot of possible
     * solutions but we will use a StatelessSession because we are only interested in the certificate
     * and not in the entry itself (StatelessSession does not load collection etc.).
     */
    @Override
    public Collection<X509CRL> getCRLs(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        try {
            return session.doReturningWork((ReturningWork<Collection<X509CRL>>) connection ->
            {
                StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

                try {
                    return CloseableIteratorUtils.toList(getStatelessDAO(statelessSession).getCRLIterator(
                            crlSelector, firstResult, maxResults));
                }
                catch (CloseableIteratorException e) {
                    throw new SQLException(e);
                }
                finally {
                    HibernateUtils.closeSessionQuietly(statelessSession);
                }
            });
        }
        catch (HibernateException e) {
            throw new CRLStoreException(e);
        }
    }

    @Override
    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector)
    throws CRLStoreException
    {
        return getCRLIterator(crlSelector, null, null);
    }

    /*
     * We want to use a StatelessSession for performance and memory reasons. The stateless session will be wrappped
     * into a wrapper iterator that will commit close the session when the iterator is closed.
     * We need to support a large number of certificates (10th of thousands) and we therefore have to work with
     * StatelessSessions and iterators.
     */
    @Override
    public CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        try {
            return session.doReturningWork((ReturningWork<CloseableIterator<X509CRL>>) connection ->
            {
                StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

                try {
                    return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).getCRLIterator(
                                crlSelector, firstResult, maxResults),
                            SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
                }
                catch (Exception e)
                {
                    HibernateUtils.closeSessionQuietly(statelessSession);

                    throw new SQLException(e);
                }
            });
        }
        catch (HibernateException e) {
            throw new CRLStoreException(e);
        }
    }

    @Override
    public CloseableIterator<X509CRLStoreEntity> getCRLStoreIterator(CRLSelector crlSelector)
    throws CRLStoreException
    {
        return getCRLStoreIterator(crlSelector, null, null);
    }

    @Override
    public CloseableIterator<X509CRLStoreEntity> getCRLStoreIterator(CRLSelector crlSelector,
            Integer firstResult, Integer maxResults)
    throws CRLStoreException
    {
        return getDAO().getCRLStoreIterator(crlSelector, firstResult, maxResults);
    }

    @Override
    public void removeAllEntries()
    throws CRLStoreException
    {
        getDAO().removeAllEntries();
    }

    @Override
    public long size() {
        return getDAO().size();
    }

    private X509CRLStoreDAO getDAO() {
        return X509CRLStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    private X509CRLStoreDAO getStatelessDAO(@Nonnull StatelessSession statelessSession)
    {
        return X509CRLStoreDAO.getInstance(SessionAdapterFactory.create(
                statelessSession, sessionManager.getSessionFactory()), storeName);
    }
}
