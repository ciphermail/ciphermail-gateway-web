/*
 * Copyright (c) 2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.operator.PGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation of PGPKeyRingEntryRevoker
 *
 * @author Martijn Brinkers
 *
 */
public class PGPKeyRingEntryRevokerImpl implements PGPKeyRingEntryRevoker
{
    /*
     * The hash algorithm for the signature
     */
    private PGPHashAlgorithm hashAlgorithm = PGPHashAlgorithm.SHA1;

    /*
     * The Key Ring from which the entries will be read
     */
    private final PGPKeyRing keyRing;

    public PGPKeyRingEntryRevokerImpl(@Nonnull PGPKeyRing keyRing) {
        this.keyRing = Objects.requireNonNull(keyRing);
    }

    private PGPContentSignerBuilder getPGPContentSignerBuilder(PGPPublicKey publicKey)
    {
        JcaPGPContentSignerBuilder builder = new JcaPGPContentSignerBuilder(publicKey.getAlgorithm(),
                hashAlgorithm.getTag());

        builder.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        return builder;
    }

    @Override
    public void revokeKeyRingEntry(@Nonnull UUID id)
    throws PGPException, IOException
    {
        PGPKeyRingEntry entry = keyRing.getByID(id);

        if (entry == null) {
            throw new PGPException("There is no entry with id " + id);
        }

        PGPPublicKey publicKey = entry.getPublicKey();

        if (publicKey.isMasterKey())
        {
            PGPPrivateKey privateKey = entry.getPrivateKey();

            if (privateKey == null) {
                throw new PGPException("Entry with id " + id + " does not have a private key.");
            }

            PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(
                    getPGPContentSignerBuilder(publicKey));

            signatureGenerator.init(PGPSignature.KEY_REVOCATION, privateKey);

            publicKey = PGPPublicKey.addCertification(publicKey,
                    signatureGenerator.generateCertification(publicKey));

            entry.setPublicKey(publicKey);
        }
        else {
            // lookup the master entry because we need the master private key to sign
            PGPKeyRingEntry masterEntry = entry.getParentKey();

            if (masterEntry == null) {
                throw new PGPException("Entry with id " + id + " does not have master entry.");
            }

            PGPPrivateKey privateKey = masterEntry.getPrivateKey();

            if (privateKey == null) {
                throw new PGPException("Master entry with id " + id + " does not have a private key.");
            }

            PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(
                    getPGPContentSignerBuilder(publicKey));

            signatureGenerator.init(PGPSignature.SUBKEY_REVOCATION, privateKey);

            publicKey = PGPPublicKey.addCertification(publicKey,
                    signatureGenerator.generateCertification(masterEntry.getPublicKey(), publicKey));

            entry.setPublicKey(publicKey);
        }
    }

    public PGPHashAlgorithm getHashAlgorithm() {
        return hashAlgorithm;
    }

    public void setHashAlgorithm(PGPHashAlgorithm hashAlgorithm) {
        this.hashAlgorithm = hashAlgorithm;
    }
}
