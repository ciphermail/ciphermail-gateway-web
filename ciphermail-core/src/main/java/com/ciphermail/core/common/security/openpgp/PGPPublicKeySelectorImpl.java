/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorResult;
import com.ciphermail.core.common.security.openpgp.validator.StandardValidatorContextObjects;
import com.ciphermail.core.common.util.CloseableIteratorException;
import com.ciphermail.core.common.util.CloseableIteratorUtils;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.core.common.util.DomainUtils.DomainType;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * PGPPublicKeySelector implementation that returns all the valid PGP public keys for an email address
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPublicKeySelectorImpl implements PGPPublicKeySelector
{
    private static final int MAX_MATCH = 100;

    /*
     * The Key ring that contains all PGP keys
     */
    private final PGPKeyRing keyRing;

    /*
     * Validator for validating the public key
     */
    private final PGPPublicKeyValidator publicKeyValidator;

    public PGPPublicKeySelectorImpl(@Nonnull PGPKeyRing keyRing, @Nonnull PGPPublicKeyValidator publicKeyValidator)
    {
        this.keyRing = Objects.requireNonNull(keyRing);
        this.publicKeyValidator = Objects.requireNonNull(publicKeyValidator);
    }

    private boolean isValid(PGPKeyRingEntry keyRingEntry)
    throws PGPException, IOException
    {
        // The parent (master key) should be placed in the context for the validator
        PGPKeyRingEntry parent = keyRingEntry.getParentKey();

        PGPPublicKey parentKey = parent != null ? parent.getPublicKey() : null;

        Context context = new ContextImpl();

        context.set(StandardValidatorContextObjects.MASTER_KEY, parentKey);

        PGPPublicKey publicKey = keyRingEntry.getPublicKey();

        PGPPublicKeyValidatorResult validationResult = publicKeyValidator.validate(publicKey, context);

        return validationResult.isValid();
    }

    protected PGPSearchParameters getSearchParameters()
    {
        PGPSearchParameters searchParameters = new PGPSearchParameters();

        searchParameters.setMatch(Match.EXACT);
        // allow "expired" keys. The expiration entry stored in the database is not accurate and should
        // only be used for GUI options to show which keys (might) be expired. PGPPublicKeySelectorImpl will correctly
        // check whether a returned key is expired or not.
        searchParameters.setExpired(Expired.MATCH_ALL);
        searchParameters.setKeyType(PGPKeyType.ALL);
        searchParameters.setMissingKeyAlias(MissingKeyAlias.ALLOWED);

        return searchParameters;
    }

    private void addKeys(String email, Set<PGPKeyRingEntry> keys)
    throws PGPException, IOException
    {
        try {

            List<PGPKeyRingEntry> matchingEntries = CloseableIteratorUtils.toList(keyRing.search(PGPSearchField.EMAIL,
                    email, getSearchParameters(), 0, MAX_MATCH));

            for (PGPKeyRingEntry entry : matchingEntries)
            {
                if (isValid(entry)) {
                    keys.add(entry);
                }

                // If this key is a master key, add all valid sub keys
                if (entry.isMasterKey())
                {
                    for (PGPKeyRingEntry subkeyEntry : entry.getSubkeys())
                    {
                        if (isValid(subkeyEntry)) {
                            keys.add(subkeyEntry);
                        }
                    }
                }
            }
        }
        catch (CloseableIteratorException e) {
            throw new IOException(e);
        }
    }

    @Override
    public Set<PGPKeyRingEntry> select(String email)
    throws IOException, PGPException
    {
        Set<PGPKeyRingEntry> selectedKeys = new LinkedHashSet<>();

        String validated = EmailAddressUtils.canonicalizeAndValidate(email, true);

        boolean isDomain = false;

        if (validated == null) {
            // Email can be a domain
            validated = DomainUtils.canonicalizeAndValidate(email, DomainType.FULLY_QUALIFIED);

            isDomain = true;
        }

        if (validated != null)
        {
            addKeys(validated, selectedKeys);

            // Check if the input was not a domain since if it was we can skip checking the domain
            if (!isDomain) {
                // Now also add keys that match the domain of the email address
                String domain = EmailAddressUtils.getDomain(validated);

                if (domain != null) {
                    addKeys(domain, selectedKeys);
                }
            }
        }

        return selectedKeys;
    }
}
