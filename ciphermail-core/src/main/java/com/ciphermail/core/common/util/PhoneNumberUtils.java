/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * Phone number related helpers like validation of phone numbers etc.
 *
 * @author Martijn Brinkers
 *
 */
public class PhoneNumberUtils
{
    private static final Logger logger = LoggerFactory.getLogger(PhoneNumberUtils.class);

    private PhoneNumberUtils() {
        // empty on purpose
    }

    /**
     * Checks whether the input is a valid phone number and returns a normalized number. The input should be a
     * number including country code.
     */
    public static String normalizeAndValidatePhoneNumber(String numberRaw)
    {
        numberRaw = StringUtils.trimToNull(numberRaw);

        if (numberRaw == null) {
            return null;
        }

        if (!StringUtils.startsWith(numberRaw, "+")) {
            numberRaw = "+" + numberRaw;
        }

        PhoneNumberUtil util = PhoneNumberUtil.getInstance();

        PhoneNumber phoneNumber;

        String filtered = null;

        try {
            phoneNumber = util.parse(numberRaw, null);

            if (util.isPossibleNumber(phoneNumber)) {
                filtered = util.format(phoneNumber, PhoneNumberFormat.E164);
            }
            else {
                logger.debug("Number {} is not a valid telephone number", numberRaw);
            }
        }
        catch (NumberParseException e) {
            logger.debug("Number {} is not a valid telephone number", numberRaw, e);
        }

        return filtered;
    }

    /**
     * Returns true if the number is a valid number.
     * <p>
     * Note: the number if normalized before it is validated
     */
    public static boolean isValidPhoneNumber(String number) {
        return normalizeAndValidatePhoneNumber(number) != null;
    }

    /**
     * Adds the country code if phone number starts with 0. Returns null if phone
     * number is null.
     */
    public static String addCountryCode(String phoneNumber, String countryCode)
    {
        if (phoneNumber == null) {
            return null;
        }

        phoneNumber = phoneNumber.trim();

        if (StringUtils.isNotBlank(countryCode) && phoneNumber.startsWith("0")) {
            phoneNumber = StringUtils.defaultString(countryCode) + phoneNumber.substring(1);
        }

        return phoneNumber;
    }

    /**
     * Masks the number by replacing all characters except the last nrOfCharsVisible by #.
     * <p>
     * It is assumed that the number is already normalized
     */
    public static String maskPhoneNumber(String phoneNumber, int nrOfCharsVisible)
    {
        if (phoneNumber == null) {
            return null;
        }

        int nrOfCharsToReplace = phoneNumber.length() - nrOfCharsVisible;

        if (nrOfCharsToReplace <= 0) {
            return phoneNumber;
        }

        return StringUtils.repeat("#", nrOfCharsToReplace) + StringUtils.substring(phoneNumber, nrOfCharsToReplace);
    }
}
