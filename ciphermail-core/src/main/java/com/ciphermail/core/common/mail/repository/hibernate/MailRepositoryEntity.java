/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail.repository.hibernate;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import com.ciphermail.core.common.util.SizeUtils;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.UuidGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Hibernate entity for storing MailRepositoryItem in the database.
 *
 * @author Martijn Brinkers
 *
 */
@Entity(name = MailRepositoryEntity.ENTITY_NAME)
@Table
public class MailRepositoryEntity implements MailRepositoryItem
{
    private static final Logger logger = LoggerFactory.getLogger(MailRepositoryEntity.class);

    static final String ENTITY_NAME = "MailRepository";

    static final String ID_COLUMN_NAME = "id";
    static final String REPOSITORY_COLUMN_NAME = "repository";
    static final String MESSAGE_ID_COLUMN_NAME = "messageID";
    static final String SUBJECT_COLUMN_NAME = "subject";
    static final String RECIPIENTS_COLUMN_NAME = "recipients";
    static final String SENDER_COLUMN_NAME = "sender";
    static final String ORIGINATOR_COLUMN_NAME = "originator";
    static final String FROM_HEADER_COLUMN_NAME = "fromHeader";
    static final String REMOTE_ADDRESS_COLUMN_NAME = "remoteAddress";
    static final String CREATED_COLUMN_NAME = "created";
    static final String LAST_UPDATED_COLUMN_NAME = "lastUpdated";
    static final String ADDITIONAL_DATA_COLUMN_NAME = "data";

    private static final int MESSAGE_ID_MAX_LENGTH = 1024;
    private static final int SUBJECT_MAX_LENGTH = 1024;
    private static final int SENDER_MAX_LENGTH = 1024;
    private static final int ORIGINATOR_MAX_LENGTH = 1024;
    private static final int RECIPIENT_MAX_LENGTH = 1024;
    private static final int FROM_MAX_LENGTH = 1024;
    private static final int REMOTE_ADDRESS_MAX_LENGTH = 1024;

    /*
     * The id of this item.
     */
    @Id
    @Column(name = ID_COLUMN_NAME)
    @UuidGenerator
    private UUID id;

    /*
     * The repository this item belongs to.
     */
    @Column(name = REPOSITORY_COLUMN_NAME, updatable = false)
    private String repository;

    /*
     * message-ID of the message
     */
    @Column(name = MESSAGE_ID_COLUMN_NAME, length = MESSAGE_ID_MAX_LENGTH, updatable = false)
    private String messageID;

    /*
     * Decoded subject of the message
     */
    @Column(name = SUBJECT_COLUMN_NAME, length = SUBJECT_MAX_LENGTH, updatable = false)
    private String subject;

    /*
     * The FROM header of the message
     */
    @Column(name = FROM_HEADER_COLUMN_NAME, length = FROM_MAX_LENGTH, updatable = false)
    private String fromHeader;

    /*
     * Envelope recipients of the message
     */
    @ElementCollection
    @JoinTable(name = "mail_repository_recipients",
            joinColumns = @JoinColumn(name = ID_COLUMN_NAME))
    @Column(name = RECIPIENTS_COLUMN_NAME, length = RECIPIENT_MAX_LENGTH)
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private List<String> recipients = new LinkedList<>();

    /*
     * The envelope sender of the message
     */
    @Column(name = SENDER_COLUMN_NAME, length = SENDER_MAX_LENGTH)
    private String sender;

    /*
     * The originator of the message
     */
    @Column(name = ORIGINATOR_COLUMN_NAME, length = ORIGINATOR_MAX_LENGTH)
    private String originator;

    /*
     * The IP address of the sender
     */
    @Column(name = REMOTE_ADDRESS_COLUMN_NAME, length = REMOTE_ADDRESS_MAX_LENGTH)
    private String remoteAddress;

    /*
     * The date this item was created
     */
    @Column(name = CREATED_COLUMN_NAME)
    private Date created;

    /*
     * The date this item was last updated
     */
    @Column(name = LAST_UPDATED_COLUMN_NAME)
    private Date lastUpdated;

    /*
     * General purpose data
     */
    @Column(name = ADDITIONAL_DATA_COLUMN_NAME, length = SizeUtils.GB)
    private byte[] additionalData;

    /*
     * The mime message is stored as an entity to allow lazy loading (a byte[] cannot be lazy loaded). Lazy
     * loading is required because the message can be very large which will result in an Out of Memory when
     * a list of entities is loaded.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private MailRepositoryMimeEntity mime;

    protected MailRepositoryEntity() {
        // required by Hibernate
    }

    public MailRepositoryEntity(@Nonnull String repository, @Nonnull MimeMessage message, Date created)
    throws MessagingException, IOException
    {
        this.repository = Objects.requireNonNull(repository);
        this.created = created;

        parseMessage(message);
    }

    public MailRepositoryEntity(String repository, MimeMessage message)
    throws MessagingException, IOException
    {
        this(repository, message, new Date());
    }

    private void initMessageID(MimeMessage message)
    {
        try {
            messageID = StringUtils.abbreviate(message.getMessageID(), MESSAGE_ID_MAX_LENGTH);
        }
        catch (MessagingException e) {
            logger.debug("Error getting messageID.", e);
        }
    }

    private void initSubject(MimeMessage message)
    {
        try {
            try {
                subject = MailUtils.getSafeSubject(message);
            }
            catch (MessagingException e) {
                // Fallback to raw headers
                subject = message.getHeader("subject", ", ");
            }

            subject = StringUtils.abbreviate(subject, SUBJECT_MAX_LENGTH);
        }
        catch (MessagingException e) {
            logger.debug("Error getting subject.", e);
        }
    }

    private void initFrom(MimeMessage message)
    {
        try {
            String decodedFrom;

            try {
                decodedFrom = StringUtils.join(EmailAddressUtils.addressesToStrings(message.getFrom(),
                        true /* mime decode */), ", ");
            }
            catch (MessagingException e) {
                // Fallback to raw headers
                decodedFrom = message.getHeader("from", ", ");
            }

            fromHeader = StringUtils.abbreviate(decodedFrom, FROM_MAX_LENGTH);
        }
        catch (MessagingException e) {
            logger.debug("Error getting from.", e);
        }
    }

    private void parseMessage(MimeMessage message)
    throws IOException, MessagingException
    {
        initMessageID(message);
        initSubject(message);
        initFrom(message);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        MailUtils.writeMessage(message, bos);

        mime = new MailRepositoryMimeEntity(bos.toByteArray());
    }

    @Override
    public UUID getID() {
        return id;
    }

    @Override
    public String getRepository() {
        return repository;
    }

    @Override
    public MimeMessage getMimeMessage()
    throws MessagingException
    {
        try(InputStream mimeInput = mime.getInputStream()) {
            return MailUtils.loadMessage(mimeInput);
        }
        catch (SQLException | IOException e) {
            throw new MessagingException("Error loading message", e);
        }
    }

    @Override
    public void writeMessage(@Nonnull OutputStream outputStream)
    throws IOException
    {
        try(InputStream mimeInput = mime.getInputStream()) {
            IOUtils.copy(mimeInput, outputStream);
        }
        catch (SQLException e) {
            throw new IOException(e);
        }
    }

    @Override
    public String getMessageID() {
        return messageID;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public String getFromHeader() {
        return fromHeader;
    }

    @Override
    @Nonnull public Collection<InternetAddress> getRecipients()
    throws AddressException
    {
        List<InternetAddress> addresses = new ArrayList<>(recipients.size());

        for (String recipient : recipients) {
            addresses.add(new InternetAddress(recipient, false));
        }

        return addresses;
    }

    @Override
    public void setRecipients(Collection<InternetAddress> recipients)
    throws AddressException
    {
        this.recipients.clear();

        if (recipients != null)
        {
            for (InternetAddress recipient : recipients) {
                this.recipients.add(recipient.getAddress());
            }
        }
    }

    @Override
    public InternetAddress getSender()
    throws AddressException
    {
        return sender != null ? new InternetAddress(sender, false) : null;
    }

    @Override
    public void setSender(InternetAddress sender)
    throws AddressException
    {
        this.sender = sender != null ? sender.getAddress() : null;
    }

    @Override
    public InternetAddress getOriginator()
    throws AddressException
    {
        return originator != null ? new InternetAddress(originator, false) : null;
    }

    @Override
    public void setOriginator(InternetAddress originator) {
        this.originator = originator != null ? originator.getAddress() : null;
    }

    @Override
    public String getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public Date getLastUpdated() {
        return lastUpdated;
    }

    @Override
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public byte[] getAdditionalData() {
        return additionalData;
    }

    @Override
    public void setAdditionalData(byte[] data) {
        this.additionalData = data;
    }
}
