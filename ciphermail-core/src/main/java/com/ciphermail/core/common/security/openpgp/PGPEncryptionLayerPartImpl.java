/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;

/**
 * Implementation of PGPEncryptionLayerPart
 *
 * @author Martijn Brinkers
 *
 */
public class PGPEncryptionLayerPartImpl extends PGPLayerPartImpl implements PGPEncryptionLayerPart
{
    /*
     * The holder for a list of PGP encryption method packets.
     */
    private final PGPEncryptedDataList encryptedDataList;

    /*
     * The public key encrypted data object with which the packet was decrypted.
     */
    private final PGPPublicKeyEncryptedData publicKeyEncryptedData;

    /*
     * The symmetric encryption algorithm used
     */
    private final PGPEncryptionAlgorithm encryptionAlgorithm;

    public PGPEncryptionLayerPartImpl(String[] armorHeaders, PGPEncryptedDataList encryptedDataList,
        PGPPublicKeyEncryptedData publicKeyEncryptedData, PGPEncryptionAlgorithm encryptionAlgorithm)
    {
        super(armorHeaders);

        this.encryptedDataList = encryptedDataList;
        this.publicKeyEncryptedData = publicKeyEncryptedData;
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    @Override
    public PGPEncryptedDataList getPGPEncryptedDataList() {
        return encryptedDataList;
    }

    @Override
    public PGPPublicKeyEncryptedData getPublicKeyEncryptedData() {
        return publicKeyEncryptedData;
    }

    @Override
    public PGPEncryptionAlgorithm getEncryptionAlgorithm() {
        return encryptionAlgorithm;
    }

    @Override
    public String toString() {
        return "[PGP encrypted]";
    }
}
