/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import com.ciphermail.core.common.util.Resource;
import com.ciphermail.core.common.util.ResourceDataSource;
import org.apache.commons.io.IOUtils;

import javax.activation.DataHandler;
import javax.annotation.Nonnull;
import javax.mail.Address;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.Closeable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class MimeMessageBuilder implements Closeable
{
    private String subject;
    private InternetAddress from;
    private String textBody;
    private String htmlBody;
    private final List<Address> to = new LinkedList<>();
    private final List<Address> cc = new LinkedList<>();
    private final List<Resource> attachments = new LinkedList<>();
    private final List<Header> headers = new LinkedList<>();

    /*
     * list of closables which should be closed when #close() is called
     */
    private final List<Closeable> closeables = new LinkedList<>();

    private MimeMessageBuilder() {
        // empty on purpose
    }

    public static MimeMessageBuilder createInstance() {
        return new MimeMessageBuilder();
    }

    public MimeMessage build()
    throws MessagingException
    {
        MimeMessage message = new MimeMessage((Session) null);

        message.setSubject(subject);
        message.setRecipients(Message.RecipientType.TO, to.toArray(new Address[]{}));
        message.setRecipients(Message.RecipientType.CC, cc.toArray(new Address[]{}));

        if (from != null) {
            message.setFrom(from);
        }

        for (Header header : headers) {
            message.setHeader(header.getName(), header.getValue());
        }

        if (!attachments.isEmpty())
        {
            MimeMultipart mixedPart = new MimeMultipart("mixed");

            if (textBody != null && htmlBody != null)
            {
                MimeBodyPart alternativeBody = new MimeBodyPart();
                alternativeBody.setContent(createAlternativeBodyPart());

                mixedPart.addBodyPart(alternativeBody);
            }
            else if (textBody != null) {
                mixedPart.addBodyPart(createTextPart());
            }
            else if (htmlBody != null) {
                mixedPart.addBodyPart(createHTMLPart());
            }

            for (Resource attachment : attachments)
            {
                MimeBodyPart attachmentPart = new MimeBodyPart();

                attachmentPart.setFileName(attachment.getFilename());

                ResourceDataSource dataSource = new ResourceDataSource(attachment);

                closeables.add(dataSource);

                attachmentPart.setDataHandler(new DataHandler(dataSource));

                mixedPart.addBodyPart(attachmentPart);
            }

            message.setContent(mixedPart);
        }
        else {
            if (textBody != null && htmlBody != null) {
                message.setContent(createAlternativeBodyPart());
            }
            else if (textBody != null) {
                message.setContent(textBody, "text/plain");
            }
            else if (htmlBody != null) {
                message.setContent(htmlBody, "text/html");
            }
            else {
                throw new IllegalArgumentException("There should be a text body and/or an " +
                                                   "html body and/or an attachment");
            }
        }

        message.saveChanges();

        return message;
    }

    private MimeBodyPart createTextPart()
    throws MessagingException
    {
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setText(textBody);

        return textPart;
    }

    private MimeBodyPart createHTMLPart()
    throws MessagingException
    {
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(htmlBody, "text/html");

        return htmlPart;
    }

    private MimeMultipart createAlternativeBodyPart()
    throws MessagingException
    {
        MimeMultipart alternativePart = new MimeMultipart("alternative");

        alternativePart.addBodyPart(createTextPart());
        alternativePart.addBodyPart(createHTMLPart());

        return alternativePart;
    }

    public MimeMessageBuilder setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public MimeMessageBuilder setFrom(String from)
    throws AddressException
    {
        this.from = new InternetAddress(from);
        return this;
    }

    public MimeMessageBuilder addHeader(@Nonnull String name, String value) {
        this.headers.add(new Header(name, value));
        return this;
    }

    public MimeMessageBuilder setTextBody(String textBody) {
        this.textBody = textBody;
        return this;
    }

    public MimeMessageBuilder setHtmlBody(String htmlBody) {
        this.htmlBody = htmlBody;
        return this;
    }

    public MimeMessageBuilder addTo(Collection<String> addresses)
    throws AddressException
    {
        if (addresses != null) {
            for (String address : addresses) {
                to.addAll(List.of(InternetAddress.parse(address, true)));
            }
        }

        return this;
    }

    public MimeMessageBuilder addTo(@Nonnull String... addresses)
    throws AddressException
    {
        return addTo(List.of(addresses));
    }

    public MimeMessageBuilder addCc(Collection<String> addresses)
    throws AddressException
    {
        if (addresses != null) {
            for (String address : addresses) {
                cc.addAll(List.of(InternetAddress.parse(address, true)));
            }
        }

        return this;
    }

    public MimeMessageBuilder addCc(@Nonnull String... addresses)
    throws AddressException
    {
        return addCc(List.of(addresses));
    }

    public MimeMessageBuilder addAttachment(@Nonnull Resource resource)
    {
        attachments.add(resource);
        return this;
    }

    @Override
    public void close()
    {
        attachments.forEach(attachment -> {
            if (attachment instanceof Closeable closeable) {
                IOUtils.closeQuietly(closeable);
            }
        });

        closeables.forEach(IOUtils::closeQuietly);
        closeables.clear();
    }
}
