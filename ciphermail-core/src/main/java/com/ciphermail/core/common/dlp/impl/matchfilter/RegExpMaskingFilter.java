/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl.matchfilter;

import com.ciphermail.core.common.dlp.MatchFilter;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Match filter that replaces part of the input with * chars based on a regular expression.
 *
 * @author Martijn Brinkers
 *
 */
public class RegExpMaskingFilter implements MatchFilter
{
    /*
     * Name of the filter
     */
    private final String name;

    /*
     * Description of the filter
     */
    private final String description;

    /*
     * The reg exp pattern
     */
    private final Pattern pattern;

    /*
     * The mask that will replace the matched items
     */
    private final String mask;

    public RegExpMaskingFilter(@Nonnull String name, @Nonnull String description, @Nonnull String regExp,
            @Nonnull String mask)
    {
        this.name = Objects.requireNonNull(name);
        this.description = Objects.requireNonNull(description);
        this.pattern = Pattern.compile(Objects.requireNonNull(regExp));
        this.mask = Objects.requireNonNull(mask);
    }

    @Override
    public String filter(String input)
    {
        if (input != null) {
            input = pattern.matcher(input).replaceAll(mask);
        }

        return input;
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    @Override
    public @Nonnull String getDescription() {
        return description;
    }
}
