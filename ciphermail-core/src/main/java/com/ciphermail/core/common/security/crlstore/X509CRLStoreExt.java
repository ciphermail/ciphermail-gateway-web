/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore;

import com.ciphermail.core.common.util.CloseableIterator;

import javax.annotation.Nonnull;
import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;
import java.util.Collection;

public interface X509CRLStoreExt extends X509BasicCRLStore
{
    /**
     * Adds the CRL to the store. If the store already contains the CRL an
     * CRLStoreException is thrown.
     */
    void addCRL(@Nonnull X509CRL crl)
    throws CRLStoreException;

    /**
     * Returns the CRL with the given thumbprint, null if no CRL with thumbprint is found.
     */
    X509CRL getCRL(@Nonnull String thumbprint)
    throws CRLStoreException;

    /**
     * Returns true if the CRL is in the store.
     */
    boolean contains(@Nonnull X509CRL crl)
    throws CRLStoreException;

    /**
     * Removes the entry with the given CRL
     */
    void remove(@Nonnull X509CRL crl)
    throws CRLStoreException;

    /**
     * Replaces one CRL by the other CRL
     */
    void replace(@Nonnull X509CRL oldCRL, @Nonnull X509CRL newCRL)
    throws CRLStoreException;

    /**
     * Returns a collection of CRLs matching the selector.
     */
    @Override
    Collection<X509CRL> getCRLs(CRLSelector crlSelector)
    throws CRLStoreException;

    /**
     * Returns a bounded collection of CRLs matching the selector.
     */
    Collection<X509CRL> getCRLs(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException;

    /**
     * Returns an iterator of CRLs matching the selector.
     */
    @Override
    CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector)
    throws CRLStoreException;

    /**
     * Returns a bounded iterator of CRLs matching the selector.
     */
    CloseableIterator<X509CRL> getCRLIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException;

    /**
     * Returns an iterator of CRL store entries matching the selector.
     */
    CloseableIterator<? extends X509CRLStoreEntry> getCRLStoreIterator(CRLSelector crlSelector)
    throws CRLStoreException;

    /**
     * Returns a bounded iterator of CRL store entries matching the selector.
     */
    CloseableIterator<? extends X509CRLStoreEntry> getCRLStoreIterator(CRLSelector crlSelector, Integer firstResult, Integer maxResults)
    throws CRLStoreException;

    /**
     * Removes all entries.
     */
    void removeAllEntries()
    throws CRLStoreException;

    /**
     * Returns the number of CRLs in the store
     * @return
     */
    long size();
}
