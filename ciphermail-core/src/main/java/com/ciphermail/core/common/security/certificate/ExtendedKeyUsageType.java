/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import org.bouncycastle.asn1.x509.KeyPurposeId;

public enum ExtendedKeyUsageType
{
    /*
     * See http://www.ietf.org/rfc/rfc3280.txt
     *
     *  anyExtendedKeyUsage OBJECT IDENTIFIER ::= { id-ce-extKeyUsage 0 }
     *
     *  id-kp OBJECT IDENTIFIER ::= { id-pkix 3 }
     *
     *  id-kp-serverAuth             OBJECT IDENTIFIER ::= { id-kp 1 }
     *  -- TLS WWW server authentication
     *  -- Key usage bits that may be consistent: digitalSignature,
     *  -- keyEncipherment or keyAgreement
     *
     *  id-kp-clientAuth             OBJECT IDENTIFIER ::= { id-kp 2 }
     *  -- TLS WWW client authentication
     *  -- Key usage bits that may be consistent: digitalSignature
     *  -- and/or keyAgreement
     *
     *  id-kp-codeSigning             OBJECT IDENTIFIER ::= { id-kp 3 }
     *  -- Signing of downloadable executable code
     *  -- Key usage bits that may be consistent: digitalSignature
     *
     *  id-kp-emailProtection         OBJECT IDENTIFIER ::= { id-kp 4 }
     *  -- E-mail protection
     *  -- Key usage bits that may be consistent: digitalSignature,
     *  -- nonRepudiation, and/or (keyEncipherment or keyAgreement)
     *
     *  id-kp-timeStamping            OBJECT IDENTIFIER ::= { id-kp 8 }
     *  -- Binding the hash of an object to a time
     *  -- Key usage bits that may be consistent: digitalSignature
     *  -- and/or nonRepudiation
     *
     *  id-kp-OCSPSigning            OBJECT IDENTIFIER ::= { id-kp 9 }
     *  -- Signing OCSP responses
     *  -- Key usage bits that may be consistent: digitalSignature
     *  -- and/or nonRepudiation
     */

    ANYKEYUSAGE     (KeyPurposeId.anyExtendedKeyUsage,   "anyKeyUsage"),
    SERVERAUTH      (KeyPurposeId.id_kp_serverAuth,      "serverAuth"),
    CLIENTAUTH      (KeyPurposeId.id_kp_clientAuth,      "clientAuth"),
    CODESIGNING     (KeyPurposeId.id_kp_codeSigning,     "codeSigning"),
    EMAILPROTECTION (KeyPurposeId.id_kp_emailProtection, "emailProtection"),
    TIMESTAMPING    (KeyPurposeId.id_kp_timeStamping,    "timeStamping"),
    OCSPSIGNING     (KeyPurposeId.id_kp_OCSPSigning,     "OCSPSigning"),
    IPSECENDSYSTEM  (KeyPurposeId.id_kp_ipsecEndSystem,  "IPSecEndSystem"),
    IPSECUSER       (KeyPurposeId.id_kp_ipsecUser,       "IPSecUser"),
    IPSECTUNNEL     (KeyPurposeId.id_kp_ipsecTunnel,     "IPSecTunnel"),
    SMARTCARDLOGIN  (KeyPurposeId.id_kp_smartcardlogon,  "smartcardLogin");

    private final KeyPurposeId keyPurposeId;
    private final String friendlyName;

    ExtendedKeyUsageType(KeyPurposeId keyPurposeId, String friendlyName)
    {
        this.keyPurposeId = keyPurposeId;
        this.friendlyName = friendlyName;
    }

    /**
     * Returns the KeyPurposeId of the extended key
     */
    public KeyPurposeId getKeyPurposeId() {
        return keyPurposeId;
    }

    /**
     * Returns the ExtendedKeyUsageType with the given OID
     */
    public static ExtendedKeyUsageType fromOID(String oid)
    {
        for (ExtendedKeyUsageType keyUsage : ExtendedKeyUsageType.values())
        {
            if (keyUsage.keyPurposeId.getId().equals(oid)) {
                return keyUsage;
            }
        }

        return null;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
