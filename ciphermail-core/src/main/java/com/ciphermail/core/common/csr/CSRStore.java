/*
 * Copyright (c) 2015, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.csr;

import com.ciphermail.core.common.security.certificate.CSRBuilder;
import org.bouncycastle.asn1.x500.X500Name;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.List;

public interface CSRStore
{
    /**
     * Returns all CSR entries
     */
    List<CSRStoreEntry> getCSRs()
    throws IOException;

    /**
     * Returns the CSR with the given name
     */
    CSRStoreEntry getCSR(@Nonnull String id)
    throws IOException;

    /**
     * Deletes the CSR with the given name
     */
    void deleteCSR(@Nonnull String id);

    /**
     * Generates a new CSR
     */
    CSRStoreEntry generateCSR(@Nonnull List<String> domains, @Nonnull X500Name subject,
            @Nonnull CSRBuilder.Algorithm keyAlgorithm)
    throws IOException;

    /**
     * Tries to find CSR which matches the certificate.
     */
    CSRStoreEntry findCSR(@Nonnull X509Certificate certificate)
    throws IOException;

    /**
     * Tries to find CSR which matches the certificate. If a match is found, the certificate is imported. If a
     * matching CSR is not found, null will be returned.
     *
     * @param chain The chain including the end-user certificate. The first certificate must be the end-user certificate
     * @return the CSR if found
     * @throws IOException
     */
    CSRStoreEntry importCertificate(@Nonnull X509Certificate... chain)
    throws IOException;
}
