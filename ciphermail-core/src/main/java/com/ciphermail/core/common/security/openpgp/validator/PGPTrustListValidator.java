/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.validator;

import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustList;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListException;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListValidity;
import com.ciphermail.core.common.security.openpgp.trustlist.PGPTrustListValidityResult;
import com.ciphermail.core.common.util.Context;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * PGPPublicKeyValidator implementation that checks whether the public key is trusted by the PGP trust list
 *
 * @author Martijn Brinkers
 *
 */
public class PGPTrustListValidator implements PGPPublicKeyValidator
{
    private static final Logger logger = LoggerFactory.getLogger(PGPTrustListValidator.class);

    /*
     * The PGP trust list
     */
    private final PGPTrustList trustList;

    public PGPTrustListValidator(@Nonnull PGPTrustList trustList) {
        this.trustList = Objects.requireNonNull(trustList);
    }

    @Override
    public @Nonnull PGPPublicKeyValidatorResult validate(@Nonnull PGPPublicKey publicKey, @Nonnull Context context)
    {
        boolean valid = false;
        String failureMessage = null;
        PGPPublicKeyValidatorFailureSeverity severity = null;

        try {
            PGPTrustListValidityResult result = trustList.checkValidity(publicKey);

            if (result.getValidity() == PGPTrustListValidity.VALID) {
                valid = true;
            }
            else if (result.getValidity() == PGPTrustListValidity.NOT_LISTED)
            {
                failureMessage = result.getMessage();
                severity = PGPPublicKeyValidatorFailureSeverity.MINOR;
            }
            else if (result.getValidity() == PGPTrustListValidity.INVALID)
            {
                failureMessage = result.getMessage();
                severity = PGPPublicKeyValidatorFailureSeverity.CRITICAL;
            }
            else {
                failureMessage = "Unknown status";
            }
        }
        catch (PGPTrustListException e)
        {
            logger.error("Error PGPTrustList#getEntry ", e);

            failureMessage = "There was an error getting the PGP trust list entry.";
        }

        return new PGPPublicKeyValidatorResultImpl(this, valid, severity, failureMessage);
    }
}
