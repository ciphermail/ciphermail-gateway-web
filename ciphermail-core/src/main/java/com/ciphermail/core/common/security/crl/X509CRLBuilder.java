/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.security.KeyAndCertificate;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.security.cert.CRLException;
import java.security.cert.X509CRL;
import java.util.Date;

public interface X509CRLBuilder
{
    /**
     * Sets the date this CRL is issued
     */
    void setThisUpdate(Date date);

    /**
     * Sets the date the next update of the CRL is available
     */
    void setNextUpdate(Date date);

    /**
     * Set the signature algorithm. This can be either a name or an OID, names
     * are treated as case-insensitive.
     */
    void setSignatureAlgorithm(String signatureAlgorithm);

    /**
     * Reason being as indicated by CRLReason, i.e. CRLReason.keyCompromise
     * or 0 if CRLReason is not to be used
     **/
    void addCRLEntry(@Nonnull BigInteger serialNumber, Date revocationDate, int reason);

    /**
     * Add the CRLEntry objects contained in a previous CRL.
     */
    void addCRL(@Nonnull X509CRL other)
    throws CRLException;

    /**
     * Generates the CRL issued by the issuer
     */
    @Nonnull X509CRL generateCRL(KeyAndCertificate issuer)
    throws CRLException;
}
