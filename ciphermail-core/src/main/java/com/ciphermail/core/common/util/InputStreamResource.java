/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class InputStreamResource implements Resource, Closeable
{
    /*
     * The wrapped input stream
     */
    private final InputStream inputStream;

    /*
     * The content type of the data
     */
    private final String contentType;

    /*
     * The filename associated with the data
     */
    private final String filename;

    /*
     * The input stream will be stored in a buffer to alow the creation of new InputStreams
     */
    private final ReadableOutputStreamBuffer buffer;


    public InputStreamResource(@Nonnull InputStream inputStream, @Nonnull String contentType, String filename)
    throws IOException
    {
        this(inputStream, contentType, filename, SizeUtils.MB);
    }

    /**
     * Creates a new InputStreamResource with the provided input stream, content type, filename, and threshold.
     * The input stream is full read and stored in a buffer. This allows the caller to get multiple input streams
     * from the same input.
     * <p>
     * Note: the "wrapped" input stream is closed when InputStreamResource is closed
     *
     * @param inputStream The input stream containing the data for the resource (must not be null).
     * @param contentType The content type of the data (must not be null).
     * @param filename The filename associated with the data (can be null).
     * @param threshold The threshold value in bytes, after which the data will be written to disk (must be non-negative).
     * @throws IOException If an I/O error occurs while reading the input stream or closing it.
     */
    public InputStreamResource(@Nonnull InputStream inputStream, @Nonnull String contentType, String filename,
            int threshold)
    throws IOException
    {
        this.inputStream = Objects.requireNonNull(inputStream);
        this.contentType = Objects.requireNonNull(contentType);
        this.filename = filename;

        this.buffer = new ReadableOutputStreamBuffer(threshold);

        IOUtils.copy(inputStream, buffer);
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public byte[] getContentAsByteArray()
    throws IOException
    {
        return IOUtils.toByteArray(getInputStream());
    }

    @Override
    public InputStream getInputStream()
    throws IOException
    {
        return buffer.getInputStream();
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public void close()
    throws IOException
    {
        IOUtils.closeQuietly(inputStream);
        buffer.close();
    }
}
