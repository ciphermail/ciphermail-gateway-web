/*
 * Copyright (c) 2011-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.codec.binary.Base64;

/**
 * Base64Utils provides utility methods for encoding and decoding data using Base64 encoding.
 */
public class Base64Utils
{
    /**
     * Base64 encodes the input
     */
    public static String encode(byte[] data) {
        return encode(data, false);
    }

    /**
     * Base64 encodes the input.
     * urlSafe – if true this encoder will emit - and _ instead of the usual + and / characters.
     * Note: no padding is added when encoding using the URL-safe alphabet.
     */
    public static String encode(byte[] data, boolean urlSafe) {
        return MiscStringUtils.toStringFromASCIIBytes(Base64.encodeBase64(data, false, urlSafe));
    }

    private Base64Utils() {
        // empty on purpose
    }

    /**
     * Base64 encodes the input. Lines longer than 76 chars will be split
     */
    public static String encodeChunked(byte[] data) {
        return MiscStringUtils.toStringFromASCIIBytes(Base64.encodeBase64Chunked(data));
    }

    /**
     * Base64 decodes the input
     */
    public static byte[] decode(String base64) {
        return Base64.decodeBase64(MiscStringUtils.getBytesASCII(base64));
    }
}
