/*
 * Copyright (c) 2009-2016, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.RequiredByJames;

import javax.annotation.Nonnull;
import java.security.cert.X509Certificate;

@RequiredByJames
public interface CA
{
    record RequestResponse(@Nonnull CertificateRequest request, X509CertStoreEntry certStoreEntry){}

    /**
     * Creates a key and certificate issued by the CA. If the certificate is immediately issued, the certificate is
     * immediately returned, i.e., the keyAndCertificate is set to a non-null value. If not, the certificate request
     * is stored in the certificate request store and the returned RequestResponse will have the field
     * keyAndCertificate set to null.
     */
    @Nonnull RequestResponse requestCertificate(@Nonnull RequestParameters parameters)
    throws CAException;

    /**
     * Checks whether there are outstanding requests which will be finished when the certificate is uploaded. This is
     * used with CAs that issue a certificate from a CSR. If a pending request was handled, the new KeyAndCertificate
     * will be returned. If there is no pending request for a certificate, null is
     * returned.
     */
    KeyAndCertificate handleRequest(@Nonnull X509Certificate certificate)
    throws CAException;
}
