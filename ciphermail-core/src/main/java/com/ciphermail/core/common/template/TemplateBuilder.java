/*
 * Copyright (c) 2009-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.template;

import freemarker.template.Template;
import freemarker.template.TemplateHashModel;
import com.ciphermail.core.common.util.RequiredByJames;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;

/**
 * TemplateBuilder is service to process and build Freemarker templates.
 *
 * @author Martijn Brinkers
 *
 */
@RequiredByJames
public interface TemplateBuilder
{
    /**
     * Returns the template with the given name. The template is loaded by the configured template loaders.
     */
    Template getTemplateByName(String name)
    throws IOException;

    /**
     * Creates a template from the template contents read from the Reader.
     */
    Template createTemplate(Reader reader)
    throws IOException;

    /**
     * Creates a template from the templateSource and processes the template. The result in stored
     * in the output.
     * <p>
     * Note: this is a convenience method which uses {{@link #createTemplate(Reader)}
     */
    void buildTemplate(String templateSource, TemplateHashModel properties, OutputStream output)
    throws TemplateBuilderException;

    /**
     * Creates a template from the templateSource and processes the template. The result is returned.
     * <p>
     * Note: this is a convenience method which uses {{@link #createTemplate(Reader)}
     */
    String buildTemplate(String templateSource, TemplateHashModel properties)
    throws TemplateBuilderException;
}
