/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.app.CipherMailApplication;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cryptlib.CryptlibObjectIdentifiers;
import org.bouncycastle.asn1.gnu.GNUObjectIdentifiers;
import org.bouncycastle.bcpg.BCPGKey;
import org.bouncycastle.bcpg.ECPublicBCPGKey;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.RSAPublicBCPGKey;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * General PGP related utility methods
 *
 * @author Martijn Brinkers
 *
 */
public class PGPUtils
{
    private static final Logger logger = LoggerFactory.getLogger(PGPUtils.class);

    /*
     * Extension used by for PGP encrypted attachments
     */
    private static final String[] PGP_EXTENSIONS = new String[]{"pgp", "asc", "gpg", "sig"};

    /*
     * MIME type for PGP public keys
     */
    public static final String PGP_PUBLIC_KEYS_MIME_TYPE = "application/pgp-keys";

    /*
     * The headers for the ArmoredOutputStream.
     *
     * Note: BC requires Hashtable for ArmoredOutputStream
     */
    private static final Hashtable<String, String> headers; // NOSONAR

    private PGPUtils() {
        // empty on purpose
    }

    static {
        headers = new Hashtable<>();

        headers.put("Version", CipherMailApplication.getName() + " (" + CipherMailApplication.getFullVersion() + ")");
    }

    /**
     * Returns a header Hashtable with the version information to be used with ArmoredOutputStream
     */
    @SuppressWarnings("unchecked")
    public static Hashtable<String, String> createArmorHeaders() { // NOSONAR
        return (Hashtable<String, String>) headers.clone();
    }

    /**
     * Returns the input with trailing WS (space, tab and CR/LF) removed.
     *
     * This is used to remove any trailing whitespace
     *
     * From RFC 4880 7.1:
     *
     * "Also, any trailing whitespace -- spaces (0x20) and tabs (0x09) -- at
     * the end of any line is removed when the cleartext signature is
     * generated."
     *
     */
    public static int getLengthWithoutTrailingWhitespace(byte[] input)
    {
        if (input == null) {
            return 0;
        }

        int i = input.length - 1;

        while (i >= 0 && isWhitespace(input[i])) {
            i--;
        }

        return i + 1;
    }

    private static boolean isWhitespace(byte b) {
        return b == '\r' || b == '\n' || b == ' ' || b == '\t';
    }

    /**
     * Removes CR/LF pairs from the end if there is more then one CRLF pair.
     *
     * Note: it is assumed that CRLF come in pairs!
     */
    public static byte[] trimRightExcessiveNewlines(byte[] input)
    {
        if (input == null) {
            return null;
        }

        int i = input.length - 1;

        for (; i >= 0; i--)
        {
            byte b = input[i];

            if (b != '\n' && b != '\r') {
                break;
            }
        }

        return ArrayUtils.subarray(input, 0, i + 3);
    }

    /**
     * Returns the key id in hex form
     */
    public static String getKeyIDHex(Long keyID)
    {
        String hex = null;

        if (keyID != null)
        {
            // If the long starts with zero's, it must be padded to 16 chars
            hex = StringUtils.leftPad(StringUtils.upperCase(Long.toHexString(keyID)), 16, "0");
        }

        return hex;
    }

    /**
     * Returns the key id from hex form
     */
    public static long getKeyIDFromHex(@Nonnull String hex) {
        return new BigInteger(hex, 16).longValue();
    }

    /**
     * Return a short ID version of the long key id
     */
    public static String getShortKeyIDHex(Long keyID) {
        return StringUtils.right(getKeyIDHex(keyID), 8);
    }

    /**
     * removes the .pgp extension from the filename
     */
    public static String removePGPFilenameExtension(String filename)
    {
        for (String extension : PGP_EXTENSIONS)
        {
            if (extension.equalsIgnoreCase(FilenameUtils.getExtension(filename)))
            {
                filename = FilenameUtils.removeExtension(filename);

                break;
            }
        }

        return filename;
    }

    /**
     * Returns true if the filename has a PGP extension
     */
    public static boolean isPGPFilenameExtension(String filename)
    {
        for (String extension : PGP_EXTENSIONS)
        {
            if (extension.equalsIgnoreCase(FilenameUtils.getExtension(filename))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the email addresses from the UserID
     */
    public static Set<String> getEmailAddressesFromUserID(String userID)
    {
        Set<String> emailAddresses = new LinkedHashSet<>();

        if (StringUtils.isNotEmpty(userID))
        {
            // assume that email addresses are placed within <>
            String[] addresses = StringUtils.substringsBetween(userID, "<", ">");

            // If there are no strings between <>, assume it's a bare email address
            // Fix for: "The email address of a PGP key with an email address which is not enclosed in angle brackets
            // ("<" and ">") is ignored" https://gitlab.com/ciphermail/ciphermail/-/issues/168
            if (addresses == null) {
                addresses = new String[]{userID};
            }

            for (String address : addresses)
            {
                String validated = EmailAddressUtils.canonicalizeAndValidate(StringUtils.trim(address), true);

                if (validated != null) {
                    emailAddresses.add(validated);
                }
            }
        }

        return emailAddresses;
    }

    /**
     * Returns all the email addresses from the list of UserIDs
     */
    public static Set<String> getEmailAddressesFromUserIDs(Collection<String> userIDs)
    {
        Set<String> emailAddresses = new LinkedHashSet<>();

        if (userIDs != null)
        {
            for (String userID : userIDs) {
                emailAddresses.addAll(getEmailAddressesFromUserID(userID));
            }
        }

        return emailAddresses;
    }

    /**
     * returns a string version of the User ID bytes
     */
    public static String userIDToString(byte[] rawUserID)
    {
        String userID = null;

        try {
            if (rawUserID != null) {
                userID = Strings.fromUTF8ByteArray(rawUserID);
            }
        }
        catch(Exception e) {
            logger.error("User ID contains invalid UTF8 encoding");
        }

        return userID;
    }

    /**
     * returns the bytes from a User ID
     */
    public static byte[] userIDToByteArray(String userID) {
        return userID != null ? Strings.toUTF8ByteArray(userID) : null;
    }

    /**
     * Return the minimal requires hash algo for the given public key
     */
    public static int getMinRequiredHashAlgorithm(@Nonnull PGPPublicKey publicKey)
    {
        int hashAlgorithm = HashAlgorithmTags.SHA512;

        BCPGKey bcPGPKey = publicKey.getPublicKeyPacket().getKey();

        int bitStrength = publicKey.getBitStrength();

        if (bcPGPKey instanceof RSAPublicBCPGKey) {
            hashAlgorithm = HashAlgorithmTags.SHA256;
        }
        else if (bcPGPKey instanceof ECPublicBCPGKey ecPublicBCPGKey)
        {
            ASN1ObjectIdentifier curveOID = ecPublicBCPGKey.getCurveOID();

            if (curveOID != null && (GNUObjectIdentifiers.Ed25519.equals(curveOID) ||
                    CryptlibObjectIdentifiers.curvey25519.equals(curveOID)))
            {
                hashAlgorithm = HashAlgorithmTags.SHA256;
            }
            else if (bitStrength > 0 && bitStrength <= 128) {
                hashAlgorithm = HashAlgorithmTags.SHA1;
            }
            else if (bitStrength <= 256) {
                hashAlgorithm = HashAlgorithmTags.SHA256;
            }
            else if (bitStrength <= 384) {
                hashAlgorithm = HashAlgorithmTags.SHA384;
            }
        }

        return hashAlgorithm;
    }

    /**
     * Return the minimal requires Symmetric Key Algorithm strength for the given public key
     */
    public static int getMinRequiredSymmetricKeyAlgorithm(PGPPublicKey publicKey)
    {
        int algorithm = SymmetricKeyAlgorithmTags.AES_256;

        BCPGKey bcPGPKey = publicKey.getPublicKeyPacket().getKey();

        int bitStrength = publicKey.getBitStrength();

        if (bcPGPKey instanceof RSAPublicBCPGKey) {
            algorithm = SymmetricKeyAlgorithmTags.AES_128;
        }
        else if (bcPGPKey instanceof ECPublicBCPGKey ecPublicBCPGKey)
        {
            ASN1ObjectIdentifier curveOID = ecPublicBCPGKey.getCurveOID();

            if (curveOID != null && (GNUObjectIdentifiers.Ed25519.equals(curveOID) ||
                    CryptlibObjectIdentifiers.curvey25519.equals(curveOID)))
            {
                algorithm = SymmetricKeyAlgorithmTags.AES_128;
            }
            else if (bitStrength > 0 && bitStrength <= 128) {
                algorithm = SymmetricKeyAlgorithmTags.AES_128;
            }
            else if (bitStrength <= 256) {
                algorithm = SymmetricKeyAlgorithmTags.AES_128;
            }
            else if (bitStrength <= 384) {
                algorithm = SymmetricKeyAlgorithmTags.AES_192;
            }
        }

        return algorithm;
    }
}
