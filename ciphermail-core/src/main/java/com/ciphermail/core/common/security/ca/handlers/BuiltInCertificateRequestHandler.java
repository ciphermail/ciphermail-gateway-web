/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.handlers;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CAProperties;
import com.ciphermail.core.common.security.ca.CAPropertiesProvider;
import com.ciphermail.core.common.security.ca.CertificateRequest;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.KeyAndCertificateIssuer;
import com.ciphermail.core.common.security.ca.RequestParameters;
import com.ciphermail.core.common.security.ca.RequestParametersImpl;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Objects;

/**
 * A CertificateRequestHandler implementation that uses the built-in CA server to request a new certificate.
 *
 * @author Martijn Brinkers
 *
 */
public class BuiltInCertificateRequestHandler implements CertificateRequestHandler
{
    private static final Logger logger = LoggerFactory.getLogger(BuiltInCertificateRequestHandler.class);

    public static final String NAME = "Local";

    /*
     * Used for getting the signing certificate
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * Used to get the CASettings
     */
    private final CAPropertiesProvider propertiesProvider;

    /*
     * Used for issuing certificates
     */
    private final KeyAndCertificateIssuer keyAndCertificateIssuer;

    public BuiltInCertificateRequestHandler(
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull CAPropertiesProvider propertiesProvider,
            @Nonnull KeyAndCertificateIssuer keyAndCertificateIssuer)
    {
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.propertiesProvider = Objects.requireNonNull(propertiesProvider);
        this.keyAndCertificateIssuer = Objects.requireNonNull(keyAndCertificateIssuer);
    }

    @Override
    public boolean isEnabled()
    {
        boolean enabled = false;

        try {
            enabled = getSigner() != null;
        }
        catch (HierarchicalPropertiesException | CertStoreException | KeyStoreException | CAException e) {
            logger.debug("Error getting signer", e);
        }

        return enabled;
    }

    @Override
    public boolean isInstantlyIssued() {
        return true;
    }

    @Override
    public String getCertificateHandlerName() {
        return NAME;
    }

    private KeyAndCertificate getSigner()
    throws HierarchicalPropertiesException, CertStoreException, KeyStoreException, CAException
    {
        KeyAndCertificate signer = null;

        CAProperties properties = propertiesProvider.getProperties();

        String thumbprint = properties.getIssuerThumbprint();

        if (StringUtils.isBlank(thumbprint)) {
            throw new CAException("There is no active CA selected");
        }

        X509CertStoreEntry storeEntry = keyAndCertStore.getByThumbprint(thumbprint);

        if (storeEntry != null) {
            signer = keyAndCertStore.getKeyAndCertificate(storeEntry);
        }

        return signer;
    }

    private RequestParameters toRequestParameters(CertificateRequest request)
    {
        RequestParameters requestParameters = new RequestParametersImpl();

        requestParameters.setSubject(request.getSubject());
        requestParameters.setEmail(request.getEmail());
        requestParameters.setValidity(request.getValidity());
        requestParameters.setKeyLength(request.getKeyLength());
        requestParameters.setSignatureAlgorithm(request.getSignatureAlgorithm());
        requestParameters.setCRLDistributionPoint(request.getCRLDistributionPoint());

        return requestParameters;
    }

    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request)
    throws CAException
    {
        try {
            KeyAndCertificate signer = getSigner();

            if (signer == null || signer.getPrivateKey() == null || signer.getCertificate() == null) {
                throw new CAException("CA signer not found");
            }

            return keyAndCertificateIssuer.issueKeyAndCertificate(toRequestParameters(request), signer);
        }
        catch(HierarchicalPropertiesException | CertStoreException | KeyStoreException e) {
            throw new CAException(e);
        }
    }

    @Override
    public KeyAndCertificate handleRequest(CertificateRequest request, X509Certificate certificate)
    {
        // This request type is not supported by this handler
        return null;
    }

    @Override
    public void cleanup(CertificateRequest request) {
        // Nothing to clean up
    }
}
