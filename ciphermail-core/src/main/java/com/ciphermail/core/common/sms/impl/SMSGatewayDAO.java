/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.impl;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.sms.SMS;
import com.ciphermail.core.common.sms.SortColumn;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Helper functions to access the SMS gateway database
 *
 */
public class SMSGatewayDAO extends GenericHibernateDAO
{
    public SMSGatewayDAO(SessionAdapter session) {
        super(session);
    }

    protected SMSGatewayEntity addSMS(@Nonnull String phoneNumber, @Nonnull String message, @Nonnull Date dateCreated) {
        return persist(new SMSGatewayEntity(phoneNumber, message, dateCreated));
    }

    public SMSGatewayEntity addSMS(@Nonnull String phoneNumber, @Nonnull String message) {
        return addSMS(phoneNumber, message, new Date());
    }

    /**
     * Converts the entity to an SMS instance.
     */
    public static SMS entityToSMS(SMSGatewayEntity entity)
    {
        return entity != null ? new SMSImpl(
                    entity.getID(),
                    entity.getPhoneNumber(),
                    entity.getMessage(),
                    entity.getDateCreated(),
                    entity.getDateLastTry(),
                    entity.getLastError()) : null;
    }

    public List<SMS> getAll(Integer firstResult, Integer maxResults, SortColumn sortColumn,
            SortDirection sortDirection)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<SMSGatewayEntity> criteriaQuery = criteriaBuilder.createQuery(SMSGatewayEntity.class);

        Root<SMSGatewayEntity> rootEntity = criteriaQuery.from(SMSGatewayEntity.class);

        if (sortColumn != null)
        {
            Path<?> sortColumnPath = switch (sortColumn) {
                case PHONENUMBER -> rootEntity.get(SMSGatewayEntity.PHONENUMBER_COLUMN_NAME);
                case CREATED     -> rootEntity.get(SMSGatewayEntity.DATE_CREATED_COLUMN_NAME);
                case LAST_TRY    -> rootEntity.get(SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME);
                default -> throw new IllegalArgumentException("Unknown sortColumn");
            };

            // Note: if "date last try" is null, the sorting order of the null values depends on the database.
            // MariaDB will have the null values at the start whereas Postgres will have the null values at the end.
            //
            // By setting the following Hibernate option, we can make the default null ordering the same for all
            // databases
            //
            //      hibernate.order_by.default_null_ordering = first
            criteriaQuery.orderBy(
                    sortDirection == SortDirection.DESC ?
                    criteriaBuilder.desc(sortColumnPath) :
                    criteriaBuilder.asc(sortColumnPath));
        }

        Query<SMSGatewayEntity> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        List<SMSGatewayEntity> allEntities = query.list();

        List<SMS> allSMS = new ArrayList<>(allEntities.size());

        for (SMSGatewayEntity entity : allEntities) {
            allSMS.add(entityToSMS(entity));
        }

        return allSMS;
    }

    public long getAvailableCount(Date notAfter)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<SMSGatewayEntity> rootEntity = criteriaQuery.from(SMSGatewayEntity.class);

        if (notAfter != null)
        {
            // We only want entries that are older than notAfter or have no dateLastTry yet
            criteriaQuery.where(criteriaBuilder.or(
                    criteriaBuilder.lessThan(rootEntity.get(SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME), notAfter),
                    criteriaBuilder.isNull(rootEntity.get(SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME))));
        }

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns the entry that has the oldest try date and not after notAfter. If there is no such entry null
     * is returned.
     */
    public SMSGatewayEntity getNextAvailable(@Nonnull Date notAfter)
    {
        // Finding the next request to handle will be a two-step process. First requests which are never handled
        // before are tried, i.e., request for which "date last try" is null. If there are multiple requests for which
        // "date last try" is null, the first added (i.e., oldest created date) will be tried. If there are no
        // requests available with a null "date last try", a request for which the "date last try" is older than the
        // provided date (notAfter) will be returned. If multiple requests match, the entry with the oldest
        // "date last try" will be returned.
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<SMSGatewayEntity> criteriaQuery = criteriaBuilder.createQuery(SMSGatewayEntity.class);

        Root<SMSGatewayEntity> rootEntity = criteriaQuery.from(SMSGatewayEntity.class);

        criteriaQuery.where(criteriaBuilder.isNull(rootEntity.get(SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME)));

        criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(SMSGatewayEntity.DATE_CREATED_COLUMN_NAME)));

        Query<SMSGatewayEntity> query = createQuery(criteriaQuery);

        query.setMaxResults(1);

        Optional<SMSGatewayEntity> optionalResult = query.uniqueResultOptional();

        SMSGatewayEntity next = null;

        if (optionalResult.isPresent()) {
            next = optionalResult.get();
        }
        else {
            // Try to find the next to be tried
            criteriaQuery = criteriaBuilder.createQuery(SMSGatewayEntity.class);

            rootEntity = criteriaQuery.from(SMSGatewayEntity.class);

            // Only select items which have a next update time which is before not after
            criteriaQuery.where(criteriaBuilder.lessThan(
                    rootEntity.get(SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME), notAfter));

            criteriaQuery.orderBy(criteriaBuilder.asc(rootEntity.get(
                    SMSGatewayEntity.DATE_LAST_TRY_COLUMN_NAME)));

            query = createQuery(criteriaQuery);

            query.setMaxResults(1);

            optionalResult = query.uniqueResultOptional();

            if (optionalResult.isPresent()) {
                next = optionalResult.get();
            }
        }

        return next;
    }

    public void deleteAll()
    {
        List<SMSGatewayEntity> allEntities = findAll(SMSGatewayEntity.class);

        for (SMSGatewayEntity entity : allEntities) {
            delete(entity);
        }
    }

    /**
     * Returns all entries that have a created date older than the given date.
     */
    public List<SMSGatewayEntity> getExpired(@Nonnull Date olderThan)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<SMSGatewayEntity> criteriaQuery = criteriaBuilder.createQuery(SMSGatewayEntity.class);

        Root<SMSGatewayEntity> rootEntity = criteriaQuery.from(SMSGatewayEntity.class);

        criteriaQuery.where(criteriaBuilder.lessThan(rootEntity.get(SMSGatewayEntity.DATE_CREATED_COLUMN_NAME),
                olderThan));

        return createQuery(criteriaQuery).list();
    }
}
