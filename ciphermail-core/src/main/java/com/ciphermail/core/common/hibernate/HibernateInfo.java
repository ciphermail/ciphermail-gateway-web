/*
 * Copyright (c) 2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.hibernate.boot.Metadata;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.MariaDBDialect;
import org.hibernate.dialect.PostgreSQLDialect;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Holder for Hibernate configuration info
 *
 */
public class HibernateInfo
{
    /*
     * Represents the ORM model as determined from all provided mapping sources.
     */
    private Metadata metadata;

    /*
     * The active database type
     */
    private DatabaseType databaseType;

    public void setMetadata(@Nonnull Metadata metadata) {
        this.metadata = Objects.requireNonNull(metadata);
    }

    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * Returns which database is in use
     */
    public synchronized DatabaseType getDatabaseType()
    {
        if (databaseType == null)
        {
            Dialect dialect = metadata.getDatabase().getDialect();

            if (dialect instanceof MariaDBDialect) {
                databaseType = DatabaseType.MYSQL;
            }
            else if (dialect instanceof PostgreSQLDialect) {
                databaseType = DatabaseType.POSTGRES;
            }
            else {
                throw new UnsupportedOperationException("Unsupported database dialect: " + dialect);
            }
        }

        return databaseType;
    }
}
