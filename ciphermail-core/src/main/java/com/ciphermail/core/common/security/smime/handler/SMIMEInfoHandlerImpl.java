/*
 * Copyright (c) 2012-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime.handler;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certificate.validator.CertificateValidatorChain;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMESigning;
import com.ciphermail.core.common.security.cms.CryptoMessageSyntaxException;
import com.ciphermail.core.common.security.cms.RecipientInfo;
import com.ciphermail.core.common.security.cms.SignerIdentifier;
import com.ciphermail.core.common.security.cms.SignerInfo;
import com.ciphermail.core.common.security.cms.SignerInfoException;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEEnvelopedInspector;
import com.ciphermail.core.common.security.smime.SMIMEInspector;
import com.ciphermail.core.common.security.smime.SMIMESecurityInfoHeader;
import com.ciphermail.core.common.security.smime.SMIMESignedInspector;
import com.ciphermail.core.common.util.BigIntegerUtils;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Default implementation of SMIMEInfoHandler that adds security info to the headers (like signed by, recipient IDs etc.)
 *
 * Note: this class is not thread safe
 *
 * @author Martijn Brinkers
 *
 */
public class SMIMEInfoHandlerImpl implements SMIMEInfoHandler
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMEInfoHandlerImpl.class);

    /*
     * Provides PKI related functionality
     */
    private final PKISecurityServices securityServices;

    /*
     * The maximum length of a header. A long header will always be folded. This maximum is the maximum
     * length of the header including folding.
     */
    private int maxHeaderLength = 4096;

    /*
     * The maximum number of encryption recipient headers that will be added to the message
     */
    private int maxRecipients = 50;

    /*
     * If true the certificates from the CMS blob will be added to the CertPathBuilder for certificate validation
     */
    private boolean addCMSCertificates = true;

    /*
     * The max number of certificates extracted from the S/MIME blob to be added to the certificate validator.
     * This is a protection against injecting too many certs and slowing down the verification process
     */
    private int maxCMSCertificates = 20;

    /*
     * If decryption failed for a message because the decryption key for this message, or some attached message,
     * is not available, this will be true.
     *
     * Note: if there are multiple messages, if one cannot be decrypted because the key is not available, this will
     * be true
     */
    private boolean decryptionKeyNotFound;

    /*
     * If the decrypted message contains illegal characters (non 7bit), or some decrypted attached message,
     * this will be true.
     *
     * Note: if there are multiple messages, if one message has illegal characters, this will be true.
     */
    private boolean messageContainsInvalid7BitChars;

    public SMIMEInfoHandlerImpl(@Nonnull PKISecurityServices securityServices) {
        this.securityServices = Objects.requireNonNull(securityServices);
    }

    /*
     * Makes sure the header is encoded when it contains non ASCII characters is folded and has a
     * maximum length.
     */
    private String[] createHeaderNameAndValue(@Nonnull String headerName, String headerValue, int level)
    {
        if (headerValue == null) {
            headerValue = "";
        }

        headerName = headerName + "-" + level;

        headerValue = MiscStringUtils.restrictLength(headerValue, maxHeaderLength);

        try {
            // make sure the header only contains ASCII characters and is folded
            // when necessary
            headerValue = HeaderUtils.encodeHeaderValue(headerName, headerValue);
        }
        catch (UnsupportedEncodingException e) {
            logger.warn("Header value cannot be encoded. Message: {}", e.getMessage());
        }

        return new String[]{headerName, headerValue};
    }

    /*
     * Makes sure the header is encoded when it contains non ASCII characters is folded and has a maximum length and
     * then sets the header (replacing any header with the same name)
     */
    private void setHeader(@Nonnull String headerName, String headerValue, int level, @Nonnull Part part)
    throws MessagingException
    {
        String[] headerNameAndValue = createHeaderNameAndValue(headerName, headerValue, level);

        part.setHeader(headerNameAndValue[0], headerNameAndValue[1]);
    }

    /*
     * Makes sure the header is encoded when it contains non ASCII characters is folded and has a maximum length and
     * then adds the header (i.e., adding a header and not replacing an existing header with same name)
     */
    private void addHeader(@Nonnull String headerName, String headerValue, int level, @Nonnull Part part)
    throws MessagingException
    {
        String[] headerNameAndValue = createHeaderNameAndValue(headerName, headerValue, level);

        part.addHeader(headerNameAndValue[0], headerNameAndValue[1]);
    }

    protected void onSigned(MimeMessage message, SMIMEInspector sMIMEInspector, int level, boolean signatureValid,
            Set<String> signers)
    throws MessagingException
    {
        // do nothing by default
    }

    private MimeMessage handleSigned(@Nonnull MimeMessage message, @Nonnull SMIMEInspector sMIMEInspector, int level)
    throws MessagingException
    {
        // Set a general header (i.e., without the level) to indicate that some part was signed.
        //
        // Note: this header does *not* indicate that the signature was valid
        message.setHeader(SMIMESecurityInfoHeader.SMIME_SIGNED, "True");

        SMIMESignedInspector signedInspector = sMIMEInspector.getSignedInspector();

        Collection<X509Certificate> certificates = null;

        try {
            certificates = signedInspector.getCertificates();
        }
        catch (CryptoMessageSyntaxException e) {
            logger.error("Error getting certificates from signed message.", e);
        }

        if (certificates == null) {
            certificates = Collections.emptyList();
        }

        Set<String> signerEmail = new HashSet<>();

        List<SignerInfo> signers;

        try {
            signers = signedInspector.getSigners();
        }
        catch (CryptoMessageSyntaxException e) {
            throw new MessagingException("Error getting signers.", e);
        }

        Boolean signatureValid = null;

        for (int signerIndex = 0; signerIndex < signers.size(); signerIndex++)
        {
            Boolean thisSignatureValid = null;

            try {
                SignerInfo signer = signers.get(signerIndex);

                SignerIdentifier signerId;

                try {
                    signerId = signer.getSignerId();
                }
                catch (IOException e)
                {
                    logger.error("Error getting signerId", e);

                    // Continue with other signers
                    continue;
                }

                addSignerIdentifierInfo(message, signerIndex, signer, signerId, level);

                // try to get the signing certificate using a certificate selector
                CertSelector certSelector;

                try {
                    certSelector = signerId.getSelector();
                }
                catch (IOException e)
                {
                    logger.error("Error getting selector for signer", e);

                    // Continue with other signers
                    continue;
                }

                // first search through the certificates that are embedded in the CMS blob
                Collection<X509Certificate> signingCerts = CertificateUtils.getMatchingCertificates(certificates, certSelector);

                if (signingCerts.isEmpty() && securityServices.getKeyAndCertStore() != null)
                {
                    // the certificate could not be found in the CMS blob. If the CertStore is
                    // set we will see if the CertStore has a match.
                    try {
                        signingCerts = securityServices.getKeyAndCertStore().getCertificates(certSelector);
                    }
                    catch (CertStoreException e) {
                        logger.error("Error getting certificates from the CertStore.", e);
                    }
                }

                X509Certificate signingCertificate = null;

                if (signingCerts != null && !signingCerts.isEmpty())
                {
                    // there can be more than one match, although in practice this should not happen
                    // often. Get the first one. If the sender is so stupid to send different
                    // certificates with same issuer/serial, subjectKeyId (which is not likely) than
                    // he/she cannot expect the signature to validate correctly. If the sender did
                    // not add a certificate to the CMS blob but the certificate was found in the
                    // CertStore it can happen that the wrong certificate was found. Solving this
                    // requires that we step through all certificates found an verify until we found
                    // the correct one.
                    signingCertificate = signingCerts.iterator().next();
                }

                if (signingCertificate != null)
                {
                    if (!verifySignature(message, signerIndex, signer, signingCertificate, level)) {
                        thisSignatureValid = false;
                    }

                    // we expect that the certificates from the CMS blob were already added to the stores
                    // if the certificate is not in the store the path cannot be build. It is possible to
                    // add them temporarily to the CertPathBuilder but that's not always as fast as adding
                    // them to the global CertStore.
                    if (!verifySigningCertificate(message, signerIndex, signingCertificate, certificates, level)) {
                        thisSignatureValid = false;
                    }

                    if (thisSignatureValid == null) {
                        thisSignatureValid = true;
                    }

                    try {
                        HashSet<String> certificateEmails = new HashSet<>(new X509CertificateInspector(
                                signingCertificate).getEmail());

                        signerEmail.addAll(certificateEmails);

                        // Add header(s) with valid and canonicalized signer email address
                        for (String certificateEmail : certificateEmails)
                        {
                            String canonicalizedEmail = EmailAddressUtils.canonicalizeAndValidate(certificateEmail,
                                    true);

                            if (canonicalizedEmail != null)
                            {
                                addHeader(SMIMESecurityInfoHeader.SIGNER_EMAIL_ADDRESSES + signerIndex,
                                        canonicalizedEmail, level, message);
                            }
                            else {
                                logger.debug("{} is not a valid email address", certificateEmail);
                            }
                        }
                    }
                    catch (Exception e) {
                        logger.error("Error getting email addresses", e);
                    }
                }
                else {
                    String info = "Signing certificate could not be found.";

                    logger.warn(info);

                    setHeader(SMIMESecurityInfoHeader.SIGNER_VERIFIED + signerIndex, "False", level, message);
                    setHeader(SMIMESecurityInfoHeader.SIGNER_VERIFICATION_INFO + signerIndex, info, level, message);
                }
            }
            finally {
                // Only make the overall signatureValid true if it was not already set and if the current
                // signature check is valid
                if (BooleanUtils.isTrue(thisSignatureValid) && signatureValid == null) {
                    signatureValid = true;
                }

                if (BooleanUtils.isFalse(thisSignatureValid)) {
                    signatureValid = false;
                }
            }
        } // end for

        onSigned(message, sMIMEInspector, level, BooleanUtils.isTrue(signatureValid), signerEmail);

        return message;
    }

    private void signingCertificateNotTrusted(@Nonnull Part part, int signerIndex, Throwable t, int level)
    throws MessagingException
    {
        Throwable cause = ExceptionUtils.getRootCause(t);

        if (cause == null) {
            cause = t;
        }

        String info = signingCertificateNotTrusted(part, signerIndex, cause.getMessage(), level);

        logger.error(info, t);
    }

    private String signingCertificateNotTrusted(@Nonnull Part part, int signerIndex, String message, int level)
    throws MessagingException
    {
        String info = "Signing certificate not trusted";

        if (message != null) {
            info = info + ". Message: " + message;
        }

        info = info + ". Timestamp: " + System.currentTimeMillis();

        setHeader(SMIMESecurityInfoHeader.SIGNER_TRUSTED + signerIndex, "False", level, part);
        setHeader(SMIMESecurityInfoHeader.SIGNER_TRUSTED_INFO + signerIndex, info, level, part);

        return info;
    }

    private boolean verifySigningCertificate(@Nonnull Part part, int signerIndex, @Nonnull X509Certificate signingCertificate,
            @Nonnull Collection<? extends Certificate> certificates, int level)
    throws MessagingException
    {
        boolean valid = false;

        CertificateValidatorChain chain = new CertificateValidatorChain();

        List<Certificate> additionalCertificates = new LinkedList<>();

        for (Certificate certificate : certificates)
        {
            additionalCertificates.add(certificate);

            if (additionalCertificates.size() >= maxCMSCertificates)
            {
                logger.warn("Max number {} of additional certificates reached", maxCMSCertificates);

                break;
            }
        }

        chain.addValidators(new IsValidForSMIMESigning());
        chain.addValidators(securityServices.getPKITrustCheckCertificateValidatorFactory().createValidator(
                addCMSCertificates ? additionalCertificates : null));

        try {
            valid = chain.isValid(signingCertificate);

            if (valid) {
                setHeader(SMIMESecurityInfoHeader.SIGNER_TRUSTED + signerIndex, "True", level, part);
            }
            else {
                signingCertificateNotTrusted(part, signerIndex, chain.getFailureMessage(), level);
            }
        }
        catch (CertificateException e) {
            signingCertificateNotTrusted(part, signerIndex, e, level);
        }

        return valid;
    }

    private boolean verifySignature(@Nonnull Part part, int signerIndex, @Nonnull SignerInfo signer,
            @Nonnull X509Certificate signingCertificate, int level)
    throws MessagingException
    {
        boolean valid = false;

        try {
            // check if the message can be verified using the public key of the signer.
            if (!signer.verify(signingCertificate.getPublicKey())) {
                throw new SignerInfoException("Message content cannot be verified with the signers public key.");
            }

            setHeader(SMIMESecurityInfoHeader.SIGNER_VERIFIED + signerIndex, "True", level, part);

            valid = true;
        }
        catch(SignerInfoException e)
        {
            String info = "Signature could not be verified. Message: " + e.getMessage();

            if (logger.isDebugEnabled()) {
                logger.warn(info, e);
            }
            else {
                logger.warn(info);
            }

            setHeader(SMIMESecurityInfoHeader.SIGNER_VERIFIED + signerIndex, "False", level, part);
            setHeader(SMIMESecurityInfoHeader.SIGNER_VERIFICATION_INFO + signerIndex, info, level, part);
        }

        return valid;
    }

    private void addSignerIdentifierInfo(@Nonnull Part part, int signerIndex, @Nonnull SignerInfo signer,
            @Nonnull SignerIdentifier signerId,  int level)
    throws MessagingException
    {
        String headerName = SMIMESecurityInfoHeader.SIGNER_ID + signerIndex;

        String headerValue = ObjectUtils.toString(signerId.getIssuer()) + "/" +
            BigIntegerUtils.hexEncode(signerId.getSerialNumber()) + "/" +
            HexUtils.hexEncode(signerId.getSubjectKeyIdentifier(), "") + "/" +
            signer.getEncryptionAlgorithmOID();

        setHeader(headerName, headerValue, level, part);
    }

    protected void onEncrypted(MimeMessage message, SMIMEInspector sMIMEInspector, int level, boolean decrypted)
    throws MessagingException
    {
        // do nothing by default
    }

    private MimeMessage handleEncrypted(@Nonnull MimeMessage message, @Nonnull SMIMEInspector sMIMEInspector,
            int level, boolean decrypted)
    throws MessagingException
    {
        // Set a general header (i.e., without the level) to indicate that some part was encrypted
        message.setHeader(SMIMESecurityInfoHeader.SMIME_ENCRYPTED, "True");

        SMIMEEnvelopedInspector envelopedInspector = sMIMEInspector.getEnvelopedInspector();

        String encryptionAlgorithm = envelopedInspector.getEncryptionAlgorithmOID();

        SMIMEEncryptionAlgorithm encryptionAlg = SMIMEEncryptionAlgorithm.fromOID(encryptionAlgorithm);

        if (encryptionAlg != null)
        {
            encryptionAlgorithm = encryptionAlg + ", Key size: " +
                    SMIMEEncryptionAlgorithm.getKeySize(encryptionAlg);
        }

        setHeader(SMIMESecurityInfoHeader.ENCRYPTION_ALGORITHM, encryptionAlgorithm, level, message);

        List<RecipientInfo> recipients;

        try {
            recipients = envelopedInspector.getRecipients();
        }
        catch (CryptoMessageSyntaxException e) {
            throw new MessagingException("Error getting encryption recipients.", e);
        }

        int nrOfRecipients = recipients.size();

        if (nrOfRecipients > maxRecipients)
        {
            logger.warn("There are more recipients but the maximum has been reached.");

            nrOfRecipients = maxRecipients;
        }

        for (int recipientIndex = 0; recipientIndex < nrOfRecipients; recipientIndex++)
        {
            RecipientInfo recipient = recipients.get(recipientIndex);

            setHeader(SMIMESecurityInfoHeader.ENCRYPTION_RECIPIENT + recipientIndex, recipient.toString(),
                    level, message);
        }

        onEncrypted(message, sMIMEInspector, level, decrypted);

        return message;
    }

    private MimeMessage handleCompressed(@Nonnull MimeMessage message, int level)
    throws MessagingException
    {
        setHeader(SMIMESecurityInfoHeader.COMPRESSED, "True", level, message);

        return message;
    }

    private MimeMessage handlePart(@Nonnull MimeMessage message, @Nonnull SMIMEInspector sMIMEInspector, int level,
            boolean decrypted)
    throws MessagingException
    {
        return switch (sMIMEInspector.getSMIMEType())
        {
            case SIGNED          -> handleSigned(message, sMIMEInspector, level);
            case ENCRYPTED       -> handleEncrypted(message, sMIMEInspector, level, decrypted);
            case AUTH_ENCRYPTED  -> handleEncrypted(message, sMIMEInspector, level, decrypted);
            case COMPRESSED      -> handleCompressed(message, level);
            default -> message;
        };
    }

    protected MimeMessage handle(@Nonnull MimeMessage message, @Nonnull SMIMEInspector inspector, int level,
            boolean decrypted)
    {
        MimeMessage result = message;

        try {
            Part handledPart = handlePart(message, inspector, level, decrypted);

            if (handledPart != null) {
                result = BodyPartUtils.toMessage(handledPart);
            }
        }
        catch (Exception e) {
            logger.error("Error handling part.", e);
        }

        return result;
    }

    @Override
    public MimeMessage handle(@Nonnull MimeMessage message, @Nonnull SMIMEHandler handler, int level)
    throws SMIMEHandlerException
    {
        message = handle(message, handler.getSMIMEInspector(), level, handler.isDecrypted());

        if (handler.isDecryptionKeyNotFound()) {
            decryptionKeyNotFound = true;
        }

        if (handler.isMessageContainsInvalid7BitChars()) {
            messageContainsInvalid7BitChars = true;
        }

        try {
            addOuterHeaders(message);
        }
        catch (MessagingException e) {
            throw new SMIMEHandlerException(e);
        }

        return message;
    }

    protected void addDecryptionKeyNotFoundHeader(MimeMessage message)
    throws MessagingException
    {
        if (message != null) {
            // Add special header if some part cannot be decrypted
            message.setHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_KEY_NOT_FOUND, "True");
        }
    }

    protected void addMessageContainsInvalid7BitChars(MimeMessage message)
    throws MessagingException
    {
        if (message != null) {
            // Add special header to indicate that the message contains characters not valid for 7-bit S/MIME
            message.setHeader(SMIMESecurityInfoHeader.SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND, "True");
        }
    }

    /**
     * Some headers should be added to the outer message if a message contains embedded S/MIME messages.
     * @throws MessagingException
     */
    public void addOuterHeaders(MimeMessage message)
    throws MessagingException
    {
        if (message != null)
        {
            boolean headersAdded = false;

            if (decryptionKeyNotFound)
            {
                    addDecryptionKeyNotFoundHeader(message);

                    headersAdded = true;
            }

            if (messageContainsInvalid7BitChars)
            {
                    addMessageContainsInvalid7BitChars(message);

                    headersAdded = true;
            }

            if (headersAdded) {
                message.saveChanges();
            }
        }
    }

    /**
     * If true the certificates from the CMS blob will be temporarily added to the  CertPathBuilder.
     * Normally the certificates should already be added to the store because it's faster in most
     * cases because in most cases the database CertStore is faster.
     */
    public boolean isAddCMSCertificates() {
        return addCMSCertificates;
    }

    /**
     * If true the certificates from the CMS blob will be temporarily added to the  CertPathBuilder.
     * Normally the certificates should already be added to the store because it's faster in most
     * cases because in most cases the database CertStore is faster.
     */
    public void setAddCMSCertificates(boolean addCMSCertificates) {
        this.addCMSCertificates = addCMSCertificates;
    }

    /**
     * The max number of certificates extracted from the S/MIME blob to be added to the certificate validator.
     * This is a protection against injecting too many certs and slowing down the verification process
     */
    public int getMaxCMSCertificates() {
        return maxCMSCertificates;
    }

    public void setMaxCMSCertificates(int maxCMSCertificates) {
        this.maxCMSCertificates = maxCMSCertificates;
    }

    public int getMaxHeaderLength() {
        return maxHeaderLength;
    }

    public void setMaxHeaderLength(int maxHeaderLength) {
        this.maxHeaderLength = maxHeaderLength;
    }

    public int getMaxRecipients() {
        return maxRecipients;
    }

    public void setMaxRecipients(int maxRecipients) {
        this.maxRecipients = maxRecipients;
    }

    public boolean isDecryptionKeyNotFound() {
        return decryptionKeyNotFound;
    }

    public boolean isMessageContainsInvalid7BitChars() {
        return messageContainsInvalid7BitChars;
    }
}
