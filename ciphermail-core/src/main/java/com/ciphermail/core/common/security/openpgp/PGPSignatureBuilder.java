/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.util.CRLFInputStream;
import com.ciphermail.core.common.util.DirectAccessByteArrayOutputStream;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.operator.PGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPPrivateKey;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.util.Date;

/**
 * Can be used to create signed documents
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSignatureBuilder
{
    private static final byte[] CRLF = new byte[]{'\r', '\n'};

    /*
     * Factory for JCE instance building
     */
    private final SecurityFactory securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

    /*
     * A PGP signature can be calculated on a canonical text document or on a binary document.
     * Signing a binary document will always result in a detached signature
     */
    private PGPDocumentType documentType;

    /*
     * The hash algorithm for the signature
     */
    private PGPHashAlgorithm hashAlgorithm;

    /*
     * The filename used for the literal data when no compression is used
     */
    private String literalFilename = "";

    /*
     * If true, the signed content will be ASCII armored
     */
    private boolean armor = true;

    /*
     * If true, the binary signature will be a detached signature
     */
    private boolean detached = true;

    /*
     * Create clear sign if true (only used with a text document)
     */
    private boolean clearSign = true;

    private PGPContentSignerBuilder getPGPContentSignerBuilder(@Nonnull PGPPublicKey publicKey)
    {
        JcaPGPContentSignerBuilder builder = new JcaPGPContentSignerBuilder(publicKey.getAlgorithm(),
                hashAlgorithm.getTag());

        builder.setProvider(securityFactory.getSensitiveProvider());
        builder.setDigestProvider(securityFactory.getNonSensitiveProvider());

        return builder;
    }

    private int getSignatureType()
    {
        return switch (documentType) {
            case TEXT -> PGPSignature.CANONICAL_TEXT_DOCUMENT;
            case BINARY -> PGPSignature.BINARY_DOCUMENT;
        };
    }

    private char getLiteralDataType()
    {
        return switch (documentType) {
            case TEXT -> PGPLiteralData.TEXT;
            case BINARY -> PGPLiteralData.BINARY;
        };
    }

    /*
     * Reads a line and writes it to output. CR/LF pairs are not written to output
     */
    private int readLine(InputStream input, OutputStream output)
    throws IOException
    {
        int ch;

        while ((ch = input.read()) >= 0)
        {
            // skip CR (because of CRLFInputStream, every CR should be followed by NL
            if (ch == '\r') {
                continue;
            }

            // stop on LF
            if (ch == '\n') {
                break;
            }

            output.write(ch);
        }

        return ch;
    }

    private void signClearText(PGPSignatureGenerator signatureGenerator, InputStream input,
        OutputStream output)
    throws IOException, PGPException
    {
        OutputStream signedOutput = armor ? new ArmoredOutputStream(output, PGPUtils.createArmorHeaders()) : output;

        if (armor) {
            ((ArmoredOutputStream)signedOutput).beginClearText(hashAlgorithm.getTag());
        }

        PGPTextSignatureUpdater signatureUpdater = new PGPTextSignatureUpdater(signatureGenerator);

        ByteArrayOutputStream lineBuffer = new ByteArrayOutputStream();

        int lastChar = 0;

        while (lastChar != -1)
        {
            lastChar = readLine(input, lineBuffer);

            if (lineBuffer.size() == 0 && lastChar == -1) {
                // The last line can be empty if we have reached EOF without reading a character.
                break;
            }

            byte[] line = lineBuffer.toByteArray();

            signedOutput.write(line);

            // Every line must end with CR/LF. Since readLine removes CR/LF we need to write them for every line
            signedOutput.write(CRLF);

            signatureUpdater.addLine(line);

            lineBuffer.reset();
        }

        if (armor) {
            ((ArmoredOutputStream)signedOutput).endClearText();
        }

        BCPGOutputStream pgpOutput = new BCPGOutputStream(signedOutput);

        try {
            signatureGenerator.generate().encode(pgpOutput);
        }
        finally {
            pgpOutput.close();
        }
    }

    private void sign(PGPSignatureGenerator signatureGenerator, InputStream input, OutputStream output)
    throws PGPException, IOException
    {
        DirectAccessByteArrayOutputStream bufferedInput = new DirectAccessByteArrayOutputStream();

        IOUtils.copy(input, bufferedInput);

        input = bufferedInput.getInputStream();

        OutputStream signedOutput = armor ? new ArmoredOutputStream(output, PGPUtils.createArmorHeaders()) : output;

        BCPGOutputStream pgpOutput = new BCPGOutputStream(signedOutput);

        OutputStream literalOutput = null;

        if (!detached)
        {
            signatureGenerator.generateOnePassVersion(false).encode(pgpOutput);

            PGPLiteralDataGenerator literalGenerator = new PGPLiteralDataGenerator();

            literalOutput = literalGenerator.open(pgpOutput, getLiteralDataType(), literalFilename,
                    input.available(), new Date());
        }

        byte[] buffer = new byte[4096];

        int length;

        while ((length = input.read(buffer)) != -1)
        {
            if (literalOutput != null) {
                literalOutput.write(buffer, 0, length);
            }

            signatureGenerator.update(buffer, 0, length);
        }

        try {
            signatureGenerator.generate().encode(pgpOutput);
        }
        finally {
            pgpOutput.close();
        }

        if (literalOutput != null) {
            literalOutput.close();
        }
    }

    private void signDocument(InputStream input, OutputStream output, PGPPublicKey publicKey,
        PGPPrivateKey privateKey)
    throws PGPException, IOException
    {
        PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(getPGPContentSignerBuilder(publicKey));

        signatureGenerator.init(getSignatureType(), privateKey);

        switch (documentType)
        {
        case TEXT :
            // OpenPGP requires CR/LF endings so filter input
            if (clearSign) {
                signClearText(signatureGenerator, new CRLFInputStream(input), output);
            }
            else {
                sign(signatureGenerator, new CRLFInputStream(input), output);
            }
            break;
        case BINARY :
            sign(signatureGenerator, input, output);
            break;
        default:
            throw new IllegalArgumentException("Unsupported documentType " + documentType);
        }
    }

    public void sign(@Nonnull InputStream input, @Nonnull OutputStream output, @Nonnull PGPPublicKey publicKey,
            @Nonnull PrivateKey privateKey)
    throws IOException, PGPException
    {
        if (documentType == null) {
            throw new IllegalArgumentException("documentType is not set");
        }

        if (hashAlgorithm == null) {
            throw new IllegalArgumentException("hashAlgorithm is not set");
        }

        PGPPrivateKey pgpPrivateKey = new JcaPGPPrivateKey(publicKey, privateKey);

        signDocument(input, output, publicKey, pgpPrivateKey);
    }

    public void setPGPDocumentType(PGPDocumentType type) {
        this.documentType = type;
    }

    public PGPDocumentType getPGPDocumentType() {
        return documentType;
    }

    public void setHashAlgorithm(PGPHashAlgorithm algorithm) {
        this.hashAlgorithm = algorithm;
    }

    public PGPHashAlgorithm getPGPhashAlgorithm() {
        return hashAlgorithm;
    }

    public String getLiteralFilename() {
        return literalFilename;
    }

    public void setLiteralFilename(String literalFilename) {
        this.literalFilename = literalFilename;
    }

    public boolean isArmor() {
        return armor;
    }

    public void setArmor(boolean armor) {
        this.armor = armor;
    }

    public boolean isDetached() {
        return detached;
    }

    public void setDetached(boolean detached) {
        this.detached = detached;
    }

    public boolean isClearSign() {
        return clearSign;
    }

    public void setClearSign(boolean clearSign) {
        this.clearSign = clearSign;
    }
}
