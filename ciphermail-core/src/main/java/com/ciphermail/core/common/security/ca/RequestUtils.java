/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.certificate.X500PrincipalBuilder;
import com.ciphermail.core.common.util.Match;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.security.auth.x500.X500Principal;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Utility class for certificate requests
 *
 * @author Martijn Brinkers
 *
 */
public class RequestUtils
{
    private RequestUtils() {
        // Empty on purpose
    }

    private static X500Principal createX500Principal(String email, String commonName)
    throws IOException
    {
        X500PrincipalBuilder principalBuilder = X500PrincipalBuilder.getInstance();

        principalBuilder.setEmail(email);
        principalBuilder.setCommonName(commonName);

        return principalBuilder.buildPrincipal();
    }

    /**
     * Creates a default RequestParameters instance from the provided email and CASettings.
     */
    public static RequestParameters createRequestParameters(String email, @Nonnull CAProperties settings)
    throws HierarchicalPropertiesException, IOException
    {
        RequestParameters request = new RequestParametersImpl();

        request.setSubject(createX500Principal(email, settings.getDefaultCommonName()));
        request.setEmail(email);
        request.setValidity(settings.getCertificateValidityDays());
        request.setKeyLength(settings.getKeyLength());
        request.setSignatureAlgorithm(settings.getSignatureAlgorithm().getAlgorithmName());
        request.setCRLDistributionPoint(settings.getCRLDistributionPoint());
        request.setCertificateRequestHandler(settings.getDefaultCertificateRequestHandler());

        return request;
    }

    /**
     * Returns the requests with a matching email address.
     */
    public static Collection<RequestParameters> findByEmail(Collection<RequestParameters> requests, String email,
            Match match)
    {
        Collection<RequestParameters> result = new LinkedList<>();

        if (requests == null || StringUtils.isEmpty(email)) {
            return result;
        }

        for (RequestParameters request : requests)
        {
            if (match == null || match == Match.EXACT)
            {
                if (email.equalsIgnoreCase(request.getEmail())) {
                    result.add(request);
                }
            }
            else {
                if (StringUtils.containsIgnoreCase(request.getEmail(), email)) {
                    result.add(request);
                }
            }
        }

        return result;
    }
}
