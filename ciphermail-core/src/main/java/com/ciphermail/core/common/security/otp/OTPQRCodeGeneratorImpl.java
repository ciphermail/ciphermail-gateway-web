/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.otp;

import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.QRBuilder;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;

public class OTPQRCodeGeneratorImpl implements OTPQRCodeGenerator
{
    /*
     * Default "client secret" app name
     */
    private static final String DEFAULT_CLIENT_SECRET_APP = "cs";

    /*
     * Default "otp" app name
     */
    private static final String DEFAULT_OTP_APP = "otp";

    /**
     * The JSON parameters
     */
    private static final String APP_JSON_PARAM = "a";
    private static final String HOST_JSON_PARAM = "h";
    private static final String KEY_ID_JSON_PARAM = "k";
    private static final String SECRET_JSON_PARAM = "s";
    private static final String PASSWORD_ID_JSON_PARAM = "p";
    private static final String PASSWORD_LENGTH_JSON_PARAM = "l";

    /*
     * Generates an identifier which can be used to identify which secret was used when generating the OTP
     */
    private final ClientSecretIdGenerator clientSecretIdGenerator;

    /*
     * The app parameter for the client secret value
     */
    private String clientSecretApp = DEFAULT_CLIENT_SECRET_APP;

    /*
     * The app parameter for the OTP value
     */
    private String otpApp = DEFAULT_OTP_APP;

    /*
     * Width (in pixels) of the QR code image
     */
    private int width = 200;

    /*
     * Height (in pixels) of the QR code image
     */
    private int height = 200;

    /*
     * The image format of the QR code (png, jpg etc.)
     */
    private String imageFormat = "png";

    public OTPQRCodeGeneratorImpl(@Nonnull ClientSecretIdGenerator clientSecretIdGenerator) {
        this.clientSecretIdGenerator = Objects.requireNonNull(clientSecretIdGenerator);
    }

    @Override
    public String createClientSecretJSONCode(@Nonnull byte[] clientSecret, String organizationID)
    throws IOException
    {
        JSONObject json = new JSONObject();

        try {
            json.put(APP_JSON_PARAM, clientSecretApp);
            json.put(HOST_JSON_PARAM, StringUtils.defaultString(organizationID));
            json.put(KEY_ID_JSON_PARAM, clientSecretIdGenerator.generateClientSecretID(clientSecret));
            json.put(SECRET_JSON_PARAM, Base64Utils.encode(clientSecret));
        }
        catch (JSONException e) {
            throw new IOException(e);
        }

        return json.toString();
    }

    @Override
    public byte[] createSecretKeyQRCode(@Nonnull byte[] secretKey, String organizationID)
    throws IOException
    {
        QRBuilder qrBuilder = new QRBuilder();

        qrBuilder.setWidth(width);
        qrBuilder.setHeight(height);
        qrBuilder.setImageFormat(imageFormat);

        return qrBuilder.buildQRCode(createClientSecretJSONCode(secretKey, organizationID));
    }

    @Override
    public String createOTPJSONCode(@Nonnull byte[] secretKey, @Nonnull String passwordID, int passwordLength)
    throws IOException
    {
        JSONObject json = new JSONObject();

        try {
            json.put(APP_JSON_PARAM, otpApp);
            json.put(KEY_ID_JSON_PARAM, clientSecretIdGenerator.generateClientSecretID(secretKey));
            json.put(PASSWORD_ID_JSON_PARAM, passwordID);
            json.put(PASSWORD_LENGTH_JSON_PARAM, passwordLength);
        }
        catch (JSONException e) {
            throw new IOException(e);
        }

        return json.toString();
    }

    @Override
    public byte[] createOTPQRCode(@Nonnull byte[] clientSecret, @Nonnull String passwordID, int passwordLength)
    throws IOException
    {
        QRBuilder qrBuilder = new QRBuilder();

        qrBuilder.setWidth(width);
        qrBuilder.setHeight(height);
        qrBuilder.setImageFormat(imageFormat);

        return qrBuilder.buildQRCode(createOTPJSONCode(clientSecret, passwordID, passwordLength));
    }

    public String getClientSecretApp() {
        return clientSecretApp;
    }

    public OTPQRCodeGeneratorImpl setClientSecretApp(String clientSecretApp)
    {
        this.clientSecretApp = clientSecretApp;
        return this;
    }

    public String getOtpApp() {
        return otpApp;
    }

    public OTPQRCodeGeneratorImpl setOtpApp(String otpApp)
    {
        this.otpApp = otpApp;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public OTPQRCodeGeneratorImpl setWidth(int width)
    {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public OTPQRCodeGeneratorImpl setHeight(int height)
    {
        this.height = height;
        return this;
    }

    public String getImageFormat() {
        return imageFormat;
    }

    public OTPQRCodeGeneratorImpl setImageFormat(String imageFormat)
    {
        this.imageFormat = imageFormat;
        return this;
    }
}
