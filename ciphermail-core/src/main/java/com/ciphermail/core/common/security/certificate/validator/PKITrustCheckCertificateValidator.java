/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate.validator;

import java.security.cert.CertPath;
import java.security.cert.TrustAnchor;
import java.util.Date;

/**
 * This CertificateValidator is used to check the trust of a certificate (ie. if a valid certificate chain
 * can be build) and that the certificate is not revoked.
 *
 * @author Martijn Brinkers
 *
 */
public interface PKITrustCheckCertificateValidator extends CertificateValidator
{
    /**
     * True if the last time isValid(certificate) returned true, false otherwise. The certificate
     * is valid when a complete trusted path can be build up to a trusted root and the
     * certificate is not revoked.
     */
    boolean isValid();

    /**
     * True if a path could be build (ie. up to a trusted root). trusted does not mean the certificate is not
     * revoked, it only means that a complete chain could be build from the certificate to a trusted root.
     */
    boolean isTrusted();

    /**
     * True if one of the certificates in the chain is revoked (based on acceptedRevocationStatus). This value is
     * only valid if isTrusted is true. If isTrusted is false isRevoked will always be false. Revocation checking
     * cannot be done when the chain is incomplete. We will therefore return false when the chain in incomplete
     * because we do not know better.
     */
    boolean isRevoked();

    /**
     * True if one of the certificates in the chain is black listed. Blacklist checking is only done when
     * the certificate is valid.
     */
    boolean isBlackListed();

    /**
     * True if the certificate is whitelisted. A white list check is only done when the certificate is
     * invalid.
     */
    boolean isWhiteListed();

    /**
     * Sets the date used for path building and revocation checking. If not set the current date will be used.
     */
    void setDate(Date date);

    /**
     * The certificate path of the certificate
     */
    CertPath getCertPath();

    /**
     * The TrustAnchor of the certificate
     */
    TrustAnchor getTrustAnchor();
}
