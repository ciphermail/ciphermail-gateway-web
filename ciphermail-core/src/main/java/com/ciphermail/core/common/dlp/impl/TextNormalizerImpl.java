/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.dlp.TextNormalizer;
import com.ciphermail.core.common.dlp.WordSkipper;
import com.ciphermail.core.common.util.WordIterator;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.Normalizer;

/**
 * TextNormalizer implementation that removes all excess whitespace, make all words lowercase, normalizes the
 * words to unicode normalized form and removes words that are should be skipped according to the WordSkipper.
 *
 * @author Martijn Brinkers
 *
 */
public class TextNormalizerImpl implements TextNormalizer
{
    /*
     * Determines which words to skip
     */
    private final WordSkipper wordSkipper;

    public TextNormalizerImpl(WordSkipper wordSkipper) {
        this.wordSkipper = wordSkipper;
    }

    @Override
    public void normalize(@Nonnull Reader input, @Nonnull Writer output)
    throws IOException
    {
        WordIterator wi = new WordIterator(input);

        String word;

        while ((word = wi.nextWord()) != null)
        {
            word = StringUtils.trimToNull(word);

            if (word != null)
            {
                // Unicode normalize the word to make sure the word only has one form
                word = Normalizer.normalize(word.toLowerCase(), Normalizer.Form.NFC);

                if (wordSkipper == null || !wordSkipper.isSkip(word)) {
                    output.append(word).append(' ');
                }
            }
        }
    }
}
