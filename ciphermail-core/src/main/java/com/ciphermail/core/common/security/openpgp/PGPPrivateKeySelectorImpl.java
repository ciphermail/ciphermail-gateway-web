/*
 * Copyright (c) 2013-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.bouncycastle.openpgp.PGPException;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Set;

/**
 * Extension of PGPPublicKeySelectorImpl that returns entries which have a private key
 *
 * @author Martijn Brinkers
 *
 */
public class PGPPrivateKeySelectorImpl implements PGPPrivateKeySelector
{
    private static class Delegate extends PGPPublicKeySelectorImpl
    {
        public Delegate(@Nonnull PGPKeyRing keyRing, @Nonnull PGPPublicKeyValidator publicKeyValidator) {
            super(keyRing, publicKeyValidator);
        }

        @Override
        protected PGPSearchParameters getSearchParameters()
        {
            PGPSearchParameters searchParameters = new PGPSearchParameters();

            searchParameters.setMatch(Match.EXACT);
            // We will allow "expired" keys. The expiration entry stored in the database is not accurate and should
            // only be used for GUI options to show which keys (might) be expired. PGPPublicKeySelectorImpl will correctly
            // check whether a returned key is expired or not.
            searchParameters.setExpired(Expired.MATCH_ALL);
            searchParameters.setKeyType(PGPKeyType.ALL);
            // Only return entries that have an associated key
            searchParameters.setMissingKeyAlias(MissingKeyAlias.NOT_ALLOWED);

            return searchParameters;
        }
    }

    private final Delegate delegate;

    public PGPPrivateKeySelectorImpl(@Nonnull PGPKeyRing keyRing, @Nonnull PGPPublicKeyValidator publicKeyValidator) {
        this.delegate = new Delegate(keyRing, publicKeyValidator);
    }

    @Override
    public Set<PGPKeyRingEntry> select(String key)
    throws IOException, PGPException
    {
        return delegate.select(key);
    }
}
