/*
 * Copyright (c) 2012-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.util.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Base class for CMSSignedDataAdapter implementations
 *
 * @author Martijn Brinkers
 *
 */
public abstract class AbstractCMSSignedDataAdapterImpl implements CMSSignedDataAdapter
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractCMSSignedDataAdapterImpl.class);

    /**
     * Should return the store with the certificates
     */
    protected abstract Store<?> getCertificateStore()
    throws CMSException;

    /**
     * Should return the store with the CRLs
     */
    protected abstract Store<?> getCRLStore()
    throws CMSException;

    @Override
    public List<X509Certificate> getCertificates(String provider)
    throws NoSuchAlgorithmException, NoSuchProviderException, CMSException
    {
        List<X509Certificate> certificates = new LinkedList<>();

        Store<?> store = getCertificateStore();

        if (store != null)
        {
            Collection<?> holders = store.getMatches(null);

            JcaX509CertificateConverter converter = new JcaX509CertificateConverter();

            converter.setProvider(provider);

            for (Object holder : holders)
            {
                if (holder instanceof X509CertificateHolder x509CertificateHolder)
                {
                    try {
                        certificates.add(converter.getCertificate(x509CertificateHolder));
                    }
                    catch (CertificateException e)
                    {
                        if (logger.isDebugEnabled()) {
                            logger.warn("Certificate is not valid.", e);
                        }
                        else {
                            logger.warn("Certificate is not valid.");
                        }
                    }
                }
                else {
                    logger.warn("object not an X509CertificateHolder");
                }
            }
        }

        return certificates;
    }

    @Override
    public List<X509CRL> getCRLs(String provider)
    throws NoSuchAlgorithmException, NoSuchProviderException, CMSException
    {
        List<X509CRL> crls = new LinkedList<>();

        Store<?> store = getCRLStore();

        if (store != null)
        {
            Collection<?> holders = store.getMatches(null);

            JcaX509CRLConverter converter = new JcaX509CRLConverter();

            converter.setProvider(provider);

            for (Object holder : holders)
            {
                if (holder instanceof X509CRLHolder x509CRLHolder)
                {
                    try {
                        crls.add(converter.getCRL(x509CRLHolder));
                    }
                    catch (CRLException e)
                    {
                        if (logger.isDebugEnabled()) {
                            logger.warn("CRL is not valid.", e);
                        }
                        else {
                            logger.warn("CRL is not valid.");
                        }
                    }
                }
                else {
                    logger.warn("object not an X509CRLHolder");
                }
            }
        }

        return crls;
    }
}
