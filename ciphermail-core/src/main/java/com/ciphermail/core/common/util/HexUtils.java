/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

/**
 * Utility functions to convert byte arrays to Hex Strings and vice versa.
 *
 * @author Martijn Brinkers
 *
 */
public class HexUtils
{
    /*
     * Pattern for the detection of hex.
     */
    private static final Pattern HEX_PATTERN = Pattern.compile("\\s*[0-9a-fA-F]+\\s*");

    private HexUtils() {
        // empty on purpose
    }

    /**
     * Returns true if the input is hex. If hex is null or empty false is returned. Pending or trailing
     * white-spaces are ignored.
     */
    public static boolean isHex(String hex)
    {
        if (StringUtils.isBlank(hex)) {
            return false;
        }

        return HEX_PATTERN.matcher(hex).matches();
    }

    public static String hexEncode(byte[] data) {
        return hexEncode(data, null);
    }

    public static String hexEncode(byte[] data, String defaultIfNull)
    {
        if (data == null) {
            return defaultIfNull;
        }

        return String.copyValueOf(Hex.encodeHex(data)).toUpperCase();
    }

    public static byte[] hexDecode(String hex, byte[] defaultIfNull)
    throws DecoderException
    {
        if (hex == null) {
            return defaultIfNull;
        }

        return Hex.decodeHex(hex.toCharArray());
    }

    public static byte[] hexDecode(String hex)
    throws DecoderException
    {
        return hexDecode(hex, null);
    }
}
