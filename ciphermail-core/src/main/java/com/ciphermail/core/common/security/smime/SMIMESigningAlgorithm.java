/*
 * Copyright (c) 2012-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

/**
 * The signing algorithms for S/MIME.
 *
 * @author Martijn Brinkers
 */
public enum SMIMESigningAlgorithm
{
    SHA1_WITH_RSA_ENCRYPTION("SHA1WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1WithRSAEncryption"),
    SHA1_WITH_RSA("SHA1WITHRSA", PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1WithRSA"),
    SHA224_WITH_RSA_ENCRYPTION("SHA224WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WithRSAEncryption"),
    SHA224_WITH_RSA("SHA224WITHRSA", PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WithRSA"),
    SHA256_WITH_RSA_ENCRYPTION("SHA256WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WithRSAEncryption"),
    SHA256_WITH_RSA("SHA256WITHRSA", PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WithRSA"),
    SHA384_WITH_RSA_ENCRYPTION("SHA384WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WithRSAEncryption"),
    SHA384_WITH_RSA("SHA384WITHRSA", PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WithRSA"),
    SHA512_WITH_RSA_ENCRYPTION("SHA512WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WithRSAEncryption"),
    SHA512_WITH_RSA("SHA512WITHRSA", PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WithRSA"),
    SHA1_WITH_RSA_AND_MGF1("SHA1WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS, "SHA1WithRSAAndMGF1"),
    SHA256_WITH_RSA_AND_MGF1("SHA256WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS, "SHA256WithRSAAndMGF1"),
    SHA384_WITH_RSA_AND_MGF1("SHA384WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS, "SHA384WithRSAAndMGF1"),
    SHA512_WITH_RSA_AND_MGF1("SHA512WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS, "SHA512WithRSAAndMGF1");

    private final String algorithm;
    private final ASN1ObjectIdentifier oid;
    private final String friendlyName;

    SMIMESigningAlgorithm(String algorithm, ASN1ObjectIdentifier oid, String friendlyName)
    {
        this.algorithm = algorithm;
        this.oid = oid;
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public ASN1ObjectIdentifier getOID() {
        return oid;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public static SMIMESigningAlgorithm fromOID(String oid)
    {
        for (SMIMESigningAlgorithm algorithm : SMIMESigningAlgorithm.values())
        {
            if (algorithm.oid.getId().equals(oid)) {
                return algorithm;
            }
        }

        return null;
    }

    public static SMIMESigningAlgorithm fromName(String name)
    {
        name = StringUtils.trimToNull(name);

        if (name != null)
        {
            for (SMIMESigningAlgorithm algorithm : SMIMESigningAlgorithm.values())
            {
                if (algorithm.name().equalsIgnoreCase(name)) {
                    return algorithm;
                }
            }
        }

        return null;
    }
}
