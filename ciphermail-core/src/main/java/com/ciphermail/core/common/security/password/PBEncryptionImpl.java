/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.crypto.RandomGenerator;

import javax.annotation.Nonnull;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

/**
 * Class for doing Password Based Encryption (PBE) using SHA256, AES 128bit with salt and iteration count.
 *
 * @author Martijn Brinkers
 *
 */
public class PBEncryptionImpl implements PBEncryption
{
    private static final String DEFAULT_ALGORITHM = "PBEWITHSHA256AND128BITAES-CBC-BC";

    /*
     * larger iteration count improves security because "brute force" password cracking takes
     * longer. Disadvantage, naturally, is that it takes longer to decrypt the key.
     */
    private static final int DEFAULT_ITERATION_COUNT = 2048;

    /*
     * default salt length in bytes (16 = 128 bits/8)
     */
    private static final int DEFAULT_SALT_LENGTH = 16;

    private final SecurityFactory securityFactory;
    private final SecretKeyFactory keyFactory;
    private final String algorithm;
    private final RandomGenerator randomGenerator;
    private final int iterationCount;
    private final int saltLength;

    public PBEncryptionImpl(int iterationCount, int saltLength,
            @Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        this.iterationCount = iterationCount;
        this.saltLength = saltLength;
        this.algorithm = Objects.requireNonNull(algorithm);

        securityFactory = SecurityFactoryFactory.getSecurityFactory();

        keyFactory = securityFactory.createSecretKeyFactory(algorithm);

        randomGenerator = securityFactory.createRandomGenerator();
    }

    public PBEncryptionImpl()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        this(DEFAULT_ITERATION_COUNT, DEFAULT_SALT_LENGTH, DEFAULT_ALGORITHM);
    }

    /*
     * Generates a new secret key using the provided key specs.
     *
     * SecretKeyFactory is not thread safe so synchronize is needed.
     */
    private synchronized Key generateSecretKey(PBEKeySpec keySpec)
    throws InvalidKeySpecException
    {
        return keyFactory.generateSecret(keySpec);
    }

    @Override
    public byte[] encrypt(byte[] data, char[] password)
    throws GeneralSecurityException, IOException
    {
        byte[] salt = randomGenerator.generateRandom(saltLength);

        PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount);

        Key secretKey = generateSecretKey(keySpec);

        Cipher cipher = securityFactory.createCipher(algorithm);

        PBEParameterSpec parameterSpec = new PBEParameterSpec(salt, iterationCount);

        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

        byte[] encrypted = cipher.doFinal(data);

        // wrap all the relevant info which we can convert to byte array
        PBEncryptionParameters wrapper = new PBEncryptionParameters(salt, iterationCount, encrypted);

        return wrapper.getEncoded();
    }

    @Override
    public byte[] decrypt(byte[] data, char[] password)
    throws GeneralSecurityException, IOException
    {
        PBEncryptionParameters wrapper = new PBEncryptionParameters(data);

        PBEKeySpec keySpec = new PBEKeySpec(password, wrapper.getSalt(), wrapper.getIterationCount());

        Key secretKey = generateSecretKey(keySpec);

        Cipher cipher = securityFactory.createCipher(algorithm);

        PBEParameterSpec parameterSpec = new PBEParameterSpec(wrapper.getSalt(), wrapper.getIterationCount());

        cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);

        return cipher.doFinal(wrapper.getEncryptedData());
    }
}
