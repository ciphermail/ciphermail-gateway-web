/*
 * Copyright (c) 2008-2014, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1SequenceParser;
import org.bouncycastle.asn1.ASN1StreamParser;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;


/**
 * The CMSContentTypeClassifier class is responsible for determining the content-type of a CMS (Cryptographic Message Syntax) object.
 * It provides methods to extract the content type from an ASN1SequenceParser, a byte array, or an InputStream.
 * <p>
 * The class is designed to conform to RFC3852 for content types.
 * <p>
 * Usage:
 * <p>
 * CMSContentType contentType = CMSContentTypeClassifier.getContentType(sequenceParser);
 * <p>
 * CMSContentType contentType = CMSContentTypeClassifier.getContentType(data);
 * <p>
 * CMSContentType contentType = CMSContentTypeClassifier.getContentType(input);
 */
public class CMSContentTypeClassifier
{
    private static final Logger logger = LoggerFactory.getLogger(CMSContentTypeClassifier.class);

    private CMSContentTypeClassifier() {
        // empty on purpose
    }

    /**
     * Returns the CMS content type of the provided sequence.
     * <p>
     * See RFC3852 for content types
     *
     * @param sequenceParser
     * @return
     */
    public static CMSContentType getContentType(ASN1SequenceParser sequenceParser)
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;

        try {
            ContentInfoParser contentInfoParser = new ContentInfoParser(sequenceParser);

            ASN1ObjectIdentifier contentTypeOID = contentInfoParser.getContentType();

            if (CMSObjectIdentifiers.data.equals(contentTypeOID)) {
                contentType = CMSContentType.DATA;
            }
            else if (CMSObjectIdentifiers.signedData.equals(contentTypeOID)) {
                contentType = CMSContentType.SIGNED_DATA;
            }
            else if (CMSObjectIdentifiers.envelopedData.equals(contentTypeOID)) {
                contentType = CMSContentType.ENVELOPED_DATA;
            }
            else if (CMSObjectIdentifiers.authEnvelopedData.equals(contentTypeOID)) {
                contentType = CMSContentType.AUTH_ENVELOPED_DATA;
            }
            else if (CMSObjectIdentifiers.signedAndEnvelopedData.equals(contentTypeOID)) {
                contentType = CMSContentType.SIGNED_AND_ENVELOPED_DATA;
            }
            else if (CMSObjectIdentifiers.digestedData.equals(contentTypeOID)) {
                contentType = CMSContentType.DIGESTED_DATA;
            }
            else if (CMSObjectIdentifiers.encryptedData.equals(contentTypeOID)) {
                contentType = CMSContentType.ENCRYPTED_DATA;
            }
            else if (CMSObjectIdentifiers.compressedData.equals(contentTypeOID)) {
                contentType = CMSContentType.COMPRESSED_DATA;
            }
        }
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }
        return contentType;
    }

    /**
     * Returns the CMS content type of the provided byte array
     * <p>
     * See RFC3852 for content types
     *
     * @param data
     * @return
     */
    public static CMSContentType getContentType(byte[] data)
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;

        try {
            ASN1StreamParser streamParser = new ASN1StreamParser(data);

            ASN1SequenceParser sequenceParser = (ASN1SequenceParser) streamParser.readObject();

            contentType = getContentType(sequenceParser);
        }
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }

        return contentType;
    }

    /**
     * Returns the CMS content type of the provided sequence.
     * <p>
     * See RFC3852 for content types
     *
     * @param input
     * @return
     */
    public static CMSContentType getContentType(InputStream input)
    {
        CMSContentType contentType = CMSContentType.UNKNOWN;

        try {
            ASN1StreamParser streamParser = new ASN1StreamParser(input);

            Object object = streamParser.readObject();

            if (object instanceof ASN1SequenceParser sequenceParser) {
                contentType = getContentType(sequenceParser);
            }
            else {
                logger.warn("Object is not a ASN1SequenceParser.");
            }
        }
        catch(IOException e) {
            logger.error("IOException retrieving CMS content type", e);
        }

        return contentType;
    }
}
