/*
 * Copyright (c) 2012-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;

/**
 * CallbackHandler implementation that sets a static password for every callback. It is expected that the callback
 * is-a PasswordCallback. If not, the callback is skipped and a warning is logged.
 *
 * @author Martijn Brinkers
 *
 */
public class StaticPasswordCallbackHandler implements CallbackHandler
{
    private static final Logger logger = LoggerFactory.getLogger(StaticPasswordCallbackHandler.class);
    /*
     * The password which is set in the callback
     */
    private final char[] password;

    public StaticPasswordCallbackHandler(char[] password)
    {
        this.password = password;
    }

    @Override
    public void handle(Callback[] callbacks)
    throws IOException, UnsupportedCallbackException
    {
        if (callbacks != null)
        {
            for (Callback callback : callbacks)
            {
                if (callback == null) {
                    continue;
                }

                if (!(callback instanceof PasswordCallback passwordCallback))
                {
                    logger.warn("PasswordCallback expected but got: {}", callback.getClass());

                    continue;
                }

                logger.info("Password callback. Prompt: {}", passwordCallback.getPrompt());

                passwordCallback.setPassword(password);
            }
        }
    }
}
