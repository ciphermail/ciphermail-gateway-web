/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.logging;

import com.ciphermail.core.common.util.ProcessException;
import com.ciphermail.core.common.util.ProcessRunner;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

public class JournalCtlImpl implements JournalCtl
{
    /*
     * The command to run
     */
    private String command = "journalctl";

    /*
     * The maximum time in milliseconds a command may run after which it is destroyed
     */
    private long timeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * Max output size we are interested in
     */
    private int maxOutputSize = SizeUtils.MB * 100;

    /*
     * Max error output size we are interested in
     */
    private int maxErrorSize = SizeUtils.KB * 10;

    /**
     * If true, the postfix command will be run with sudo
     */
    private boolean requireSudo = true;

    /*
     * The journal output fields to return
     */
    private List<String> outputFields = List.of("MESSAGE" ,"SYSLOG_IDENTIFIER");

    @Override
    public String getLog(
            @Nonnull List<String> units,
            @Nonnull List<String> identifiers,
            boolean reverse,
            String filter,
            Integer lines,
            String since,
            String until)
    throws IOException
    {
        ProcessRunner processRunner = new ProcessRunner();

        // journalctl returns a non-null exit code when there are no results (for example when filtering)
        processRunner.setThrowExceptionOnErrorExitCode(false);
        processRunner.setRequireSudo(isRequireSudo());

        // split command on spaces to support a command with parameters
        List<String> cmd = new LinkedList<>(ProcessRunner.splitCommandLine(getCommand()));

        for (String unit : units) {
            cmd.add("--unit=" + unit);
        }

        for (String identifier : identifiers) {
            cmd.add("--identifier=" + identifier);
        }

        cmd.add("--output=json");
        cmd.add("--output-fields=" + StringUtils.join(outputFields, ","));

        if (reverse) {
            cmd.add("--reverse");
        }

        if (StringUtils.isNotEmpty(filter)) {
            cmd.add("--grep=" + filter);
        }

        if (lines != null) {
            cmd.add("--lines=" + lines);
        }

        if (StringUtils.isNotEmpty(since)) {
            cmd.add("--since=" + since);
        }

        if (StringUtils.isNotEmpty(until)) {
            cmd.add("--until=" + until);
        }

        StringWriter outputWriter = new StringWriter();

        OutputStream outputStream = new SizeLimitedOutputStream(WriterOutputStream.builder()
                .setCharset(StandardCharsets.UTF_8).setWriter(outputWriter).get(),
                getMaxOutputSize(), false);

        StringWriter errorWriter = new StringWriter();

        OutputStream errorStream = new SizeLimitedOutputStream(WriterOutputStream.builder()
                .setCharset(StandardCharsets.UTF_8).setWriter(errorWriter).get(),
                getMaxErrorSize(), false);

        try {
            processRunner.setOutput(outputStream);
            processRunner.setError(errorStream);

            processRunner.run(cmd);
        }
        catch(ProcessException e)
        {
            // need to close before getting the content
            IOUtils.closeQuietly(errorStream);

            throw new IOException(errorWriter.toString(), e);
        }
        finally {
            IOUtils.closeQuietly(errorStream);
            IOUtils.closeQuietly(outputStream);
        }

        return outputWriter.toString();
    }

    public String getCommand() {
        return command;
    }

    public JournalCtlImpl setCommand(String command) {
        this.command = command;
        return this;
    }

    public long getTimeout() {
        return timeout;
    }

    public JournalCtlImpl setTimeout(long timeout)
    {
        this.timeout = timeout;
        return this;
    }

    public int getMaxOutputSize() {
        return maxOutputSize;
    }

    public JournalCtlImpl setMaxOutputSize(int maxOutputSize)
    {
        this.maxOutputSize = maxOutputSize;
        return this;
    }

    public int getMaxErrorSize() {
        return maxErrorSize;
    }

    public JournalCtlImpl setMaxErrorSize(int maxErrorSize)
    {
        this.maxErrorSize = maxErrorSize;
        return this;
    }

    public boolean isRequireSudo() {
        return requireSudo;
    }

    public JournalCtlImpl setRequireSudo(boolean requireSudo)
    {
        this.requireSudo = requireSudo;
        return this;
    }

    public List<String> getOutputFields() {
        return outputFields;
    }

    public JournalCtlImpl setOutputFields(List<String> outputFields)
    {
        this.outputFields = outputFields;
        return this;
    }
}
