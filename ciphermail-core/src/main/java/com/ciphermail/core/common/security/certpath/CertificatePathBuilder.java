/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certpath;

import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.PKIXCertPathChecker;
import java.security.cert.TrustAnchor;
import java.util.Date;
import java.util.Set;

public interface CertificatePathBuilder extends BasicCertificatePathBuilder
{
    /**
     * Sets the set of trust anchors (trusted roots) used for path building.
     */
    void setTrustAnchors(Set<TrustAnchor> trustAnchors);

    /**
     * Returns the set of trust anchors (trusted roots) used for path building.
     */
    Set<TrustAnchor> getTrustAnchors()
    throws CertStoreException;

    /**
     * Adds the store to the set of stores used for path building.
     */
    void addCertStore(CertStore store);

    /**
     * Returns the set of stores used for path building.
     */
    Set<CertStore> getCertStores();

    /**
     * add the certPathChecker to the list of certPathCheckers used during path validation.
     */
    void addCertPathChecker(PKIXCertPathChecker certPathChecker);

    /**
     * If true the built in revocation checking of the PathBuilder will be used. Set false if an external
     * revocation mechanism is used.
     */
    void setRevocationEnabled(boolean enabled);

    /**
     * Sets the 'current' date. If not set the real current date will be used.
     */
    void setDate(Date date);
}
