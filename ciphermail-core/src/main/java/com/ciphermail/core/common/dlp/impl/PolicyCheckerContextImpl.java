/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.dlp.PolicyCheckerContext;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.util.ThreadSafeContextImpl;
import org.apache.commons.lang.BooleanUtils;

import java.util.Collection;

/**
 * Implementation of PolicyCheckerContext.
 *
 * @author Martijn Brinkers
 *
 */
public class PolicyCheckerContextImpl extends ThreadSafeContextImpl implements PolicyCheckerContext
{
    /*
     * The content key (for example the content of email). The content should be of type String.
     */
    private static final String CONTENT_CONTEXT_KEY = "dlp.content";

    /*
     * The key used for partial data
     */
    private static final String PARTIAL_CONTEXT_KEY = "dlp.partial";

    /*
     * regular expression patterns. The content should be of type Collection&lt;PolicyPattern&gt;
     */
    private static final String PATTERNS_CONTEXT_KEY = "dlp.patterns";

    @Override
    public String getContent() {
        return get(CONTENT_CONTEXT_KEY, String.class);
    }

    @Override
    public void setContent(String content) {
        set(CONTENT_CONTEXT_KEY, content);
    }

    @Override
    public void setPartial(boolean partial) {
        set(PARTIAL_CONTEXT_KEY, partial);
    }

    @Override
    public boolean isPartial() {
        return BooleanUtils.toBoolean(get(PARTIAL_CONTEXT_KEY, Boolean.class));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<PolicyPattern> getPatterns() {
        return get(PATTERNS_CONTEXT_KEY, Collection.class);
    }

    @Override
    public void setPatterns(Collection<PolicyPattern> patterns) {
        set(PATTERNS_CONTEXT_KEY, patterns);
    }
}
