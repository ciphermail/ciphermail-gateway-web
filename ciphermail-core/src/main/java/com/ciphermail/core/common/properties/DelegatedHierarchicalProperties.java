/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * HierarchicalProperties implementation that delegates all calls to another instance of HierarchicalProperties.
 *
 * @author Martijn Brinkers
 *
 */
public class DelegatedHierarchicalProperties implements HierarchicalProperties
{
    /*
     * The properties to delegate to
     */
    private final HierarchicalProperties source;

    /*
     * The parent properties to delegate to if the property is not found
     * on the source properties (can be null)
     */
    private final HierarchicalProperties parent;

    public DelegatedHierarchicalProperties(@Nonnull HierarchicalProperties source, HierarchicalProperties parent)
    {
        this.source = Objects.requireNonNull(source);
        this.parent = parent;
    }

    public DelegatedHierarchicalProperties(@Nonnull HierarchicalProperties source) {
        this(source, null);
    }

    @Override
    public void deleteAll()
    throws HierarchicalPropertiesException
    {
        source.deleteAll();
    }

    @Override
    public void deleteProperty(@Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        source.deleteProperty(propertyName);
    }

    @Override
    public String getCategory() {
        return source.getCategory();
    }

    public HierarchicalProperties getParent() {
        return parent;
    }

    private String internalGetProperty(String propertyName, Parent checkParent)
    throws HierarchicalPropertiesException
    {
        String value = source.getProperty(propertyName, checkParent);

        if (value == null && parent != null && checkParent == Parent.CHECK_PARENT) {
            value = parent.getProperty(propertyName, checkParent);
        }

        return value;
    }

    @Override
    public String getProperty(@Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        return internalGetProperty(propertyName, Parent.CHECK_PARENT);
    }

    @Override
    public String getProperty(@Nonnull String propertyName, @Nonnull Parent checkParent)
    throws HierarchicalPropertiesException
    {
        return internalGetProperty(propertyName, checkParent);
    }

    @Override
    public void setProperty(@Nonnull String propertyName, String value)
    throws HierarchicalPropertiesException
    {
        source.setProperty(propertyName, value);
    }

    @Override
    public boolean isInherited(@Nonnull String propertyName)
    throws HierarchicalPropertiesException
    {
        return source.isInherited(propertyName);
    }

    @Override
    public Set<String> getProperyNames(boolean recursive)
    throws HierarchicalPropertiesException
    {
        Set<String> names = source.getProperyNames(recursive);

        if (recursive && parent != null) {
            names.addAll(parent.getProperyNames(recursive));
        }

        return Collections.unmodifiableSet(names);
    }

    public HierarchicalProperties getSourceProperties() {
        return source;
    }
}
