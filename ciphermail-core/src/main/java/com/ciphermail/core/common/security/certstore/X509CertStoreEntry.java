/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore;

import java.security.cert.CertPath;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;

/**
 * An entry in the X509 certificate store
 *
 * @author Martijn Brinkers
 *
 */
public interface X509CertStoreEntry
{
    /**
     *
     * The Certificate associated with this entry
     */
    X509Certificate getCertificate();

    /**
     * The chain of the certificate
     */
    void setCertificatePath(CertPath chain);
    CertPath getCertificatePath();

    /**
     * The date the path was updated
     */
    void setDatePathUpdated(Date date);
    Date getDatePathUpdated();

    /**
     * The date this entry was created
     */
    void setCreationDate(Date creationDate);
    Date getCreationDate();

    /**
     * The email addresses associated with the certificate
     */
    Set<String> getEmail();

    /**
     * The key alias associated with the certificate
     */
    void setKeyAlias(String keyAlias);
    String getKeyAlias();
}
