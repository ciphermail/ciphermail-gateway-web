/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.rmi.server.RMIServerSocketFactory;

import javax.net.ServerSocketFactory;

/**
 * RMIServerSocketFactory implementation that binds to a specific address. This can for example be used to make sure
 * that the RMI server is only bound to localhost.
 *
 * @author Martijn Brinkers
 *
 */
public class RMIServerSocketFactoryImpl implements RMIServerSocketFactory
{
    /*
     * The address to bind to
     */
    private final InetAddress bindAddress;

    public RMIServerSocketFactoryImpl(InetAddress bindAddress) {
        this.bindAddress = bindAddress;
    }

    public RMIServerSocketFactoryImpl(String bindAddress)
    throws UnknownHostException
    {
        this.bindAddress = InetAddress.getByName(bindAddress);
    }

    @Override
    public ServerSocket createServerSocket(int port)
    throws IOException
    {
        return ServerSocketFactory.getDefault().createServerSocket(port, 10, bindAddress);
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == null) {
            return false;
        }

        if (other == this) {
            return true;
        }

        return other.getClass().equals(getClass());
    }

    @Override
    public int hashCode() {
        return this.getClass().hashCode();
    }
}
