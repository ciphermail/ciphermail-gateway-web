/*
 * Copyright (c) 2016-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.sms.transport.twilio;

import com.ciphermail.core.common.http.AbstractVoidBinResponseConsumer;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactory;
import com.ciphermail.core.common.http.CloseableHttpAsyncClientFactoryImpl;
import com.ciphermail.core.common.http.HttpClientContextFactory;
import com.ciphermail.core.common.http.HttpClientContextFactoryImpl;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.sms.SMSTransport;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.PhoneNumberUtils;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.auth.AuthCache;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.entity.EntityBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.auth.BasicAuthCache;
import org.apache.hc.client5.http.impl.auth.BasicScheme;
import org.apache.hc.client5.http.impl.auth.SystemDefaultCredentialsProvider;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.core5.http.ConnectionClosedException;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.apache.hc.core5.http.nio.entity.BasicAsyncEntityProducer;
import org.apache.hc.core5.http.nio.support.BasicRequestProducer;
import org.apache.hc.core5.net.URIBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.security.GeneralSecurityException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * SMSTransport implementation for Twilio (<a href="https://www.twilio.com">...</a>)
 *
 * @author Martijn Brinkers
 *
 */
public class TwilioSMSTransport implements SMSTransport
{
    private static final Logger logger = LoggerFactory.getLogger(TwilioSMSTransport.class);

    private static final String TIMEOUT_ERROR = "A timeout has occurred connecting to: ";
    private static final String MAX_RESPONSE_SIZE_EXCEEDED_ERROR = "Max HTTP response size exceeded.";

    /*
     * The JSON parameters supported by the Twilio API
     */
    private static final String JSON_ERROR_CODE = "code";
    private static final String JSON_ERROR_MESSAGE = "message";
    private static final String JSON_TO = "to";
    private static final String JSON_SID = "sid";

    /*
     * Maximum allowed size of a response from the API.
     */
    private int maxResponseSize = SizeUtils.KB * 100;

    /*
     * Max time the request may take
     */
    private long totalTimeout = DateUtils.MILLIS_PER_MINUTE;

    /*
     * Creates CloseableHttpAsyncClient instances
     */
    private final CloseableHttpAsyncClientFactory httpClientFactory;

    /*
     * Creates HttpClientContextFactory instances
     */
    private final HttpClientContextFactory httpClientContextFactory;

    /*
     * Provides the Twilio API settings
     */
    private final TwilioPropertiesProvider propertiesProvider;

    /*
     * The HTTP client instance responsible for HTTP(s) communication
     */
    private CloseableHttpAsyncClient sharedHttpClient;

    /*
     * The URL for sending SMS messages. The URL should have the following form:
     *
     * https://api.twilio.com/2010-04-01/Accounts/{AccountSid}/Messages.json
     */
    private String url = "https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json";

    public TwilioSMSTransport(
            @Nonnull CloseableHttpAsyncClientFactory httpClientFactory,
            @Nonnull HttpClientContextFactory httpClientContextFactory,
            @Nonnull TwilioPropertiesProvider propertiesProvider)
    {
        this.httpClientFactory = Objects.requireNonNull(httpClientFactory);
        this.httpClientContextFactory = Objects.requireNonNull(httpClientContextFactory);
        this.propertiesProvider = Objects.requireNonNull(propertiesProvider);
    }

    @Override
    public String getName() {
        return "Twilio";
    }

    private synchronized CloseableHttpAsyncClient getHTTPClient()
    throws IOException, GeneralSecurityException
    {
        if (sharedHttpClient == null) {
            sharedHttpClient = httpClientFactory.createClient();
        }

        return sharedHttpClient;
    }

    protected String getQualifiedURL(String accountSid) {
        return String.format(url, accountSid);
    }

    private void throwTransportExceptionUnknownError(int httpStatusCode, String httpStatusReasonPhrase, Throwable t)
    throws IOException
    {
        logger.error("Error sending SMS. Status: {}, Reason: {}", httpStatusCode,
                httpStatusReasonPhrase, t);

        throw new IOException("Error sending SMS. " +
                "Status: " + httpStatusCode + ", " +
                "Reason: " + httpStatusReasonPhrase);
    }

    private void parseResponse(byte[] response, AbstractVoidBinResponseConsumer responseConsumer)
    throws IOException
    {
        int httpStatusCode = responseConsumer.getHttpStatusCode();
        String httpStatusReasonPhrase = responseConsumer.getHttpStatusReasonPhrase();

        String responseBody = MiscStringUtils.toStringFromUTF8Bytes(response);

        logger.debug("{} response: {}", getName(), responseBody);

        if (StringUtils.isNotEmpty(responseBody))
        {
            try {
                // Try to load the response as JSON
                JSONObject json = new JSONObject(responseBody);

                if (httpStatusCode >= HttpStatus.SC_CLIENT_ERROR)
                {
                    logger.error("Error sending SMS. Status: {}, Response: {}", httpStatusCode,
                            StringUtils.abbreviate(responseBody, 100));

                    String errorCode = StringUtils.defaultIfEmpty(ObjectUtils.toString(json.opt(
                                JSON_ERROR_CODE)), "unknown");

                    String errorMessage = StringUtils.defaultIfEmpty(ObjectUtils.toString(json.opt(
                                JSON_ERROR_MESSAGE)), "unknown");

                    throw new IOException("Error sending SMS. " +
                            "Status: " + httpStatusCode + ", " +
                            "Error Code: " + errorCode + ", " +
                            "Error message: " + errorMessage);
                }

                String to = StringUtils.defaultIfEmpty(ObjectUtils.toString(json.opt(
                        JSON_TO)), "unknown");

                String sid = StringUtils.defaultIfEmpty(ObjectUtils.toString(json.opt(
                        JSON_SID)), "unknown");

                logger.info("SMS successfully delivered to {} using {}. Message sid: {}", to, getName(), sid);
            }
            catch (JSONException e) {
                throwTransportExceptionUnknownError(httpStatusCode, httpStatusReasonPhrase, e);
            }
        }
        else {
            throw new IOException("Response body is empty. Status: " + httpStatusCode);
        }
    }

    @Override
    public void sendSMS(String phoneNumber, String message)
    throws IOException
    {
        if (StringUtils.isEmpty(phoneNumber)) {
            throw new IOException("phoneNumber is not set");
        }

        if (StringUtils.isEmpty(message)) {
            throw new IOException("message is not set");
        }

        phoneNumber = PhoneNumberUtils.normalizeAndValidatePhoneNumber(phoneNumber);

        if (StringUtils.isEmpty(phoneNumber)) {
            throw new IOException("phoneNumber is not valid");
        }

        try {
            TwilioProperties properties = propertiesProvider.getProperties();

            String accountSid = properties.getAccountSid();

            if (StringUtils.isEmpty(accountSid)) {
                throw new IOException("Account Sid is not set");
            }

            String from = properties.getFrom();

            if (StringUtils.isEmpty(from)) {
                throw new IOException("from is not set");
            }

            String username;
            String password;

            String apiKeySid = properties.getAPIKeySid();
            String apiSecretKey = properties.getAPISecretKey();
            String authToken = properties.getAuthToken();

            // Either the API key with secret must be set or the AuthToken must be set
            if (StringUtils.isNotEmpty(apiKeySid))
            {
                if (StringUtils.isEmpty(apiSecretKey)) {
                    throw new IOException("API Secret Key is not set");
                }

                username = apiKeySid;
                password = apiSecretKey;
            }
            else {
                if (StringUtils.isEmpty(authToken)) {
                    throw new IOException("AuthToken is not set");
                }

                username = accountSid;
                password = authToken;
            }

            URIBuilder uriBuilder = new URIBuilder(getQualifiedURL(accountSid));

            // The port must be explicitly set for basic auth to work. For some reason AuthCache does not match
            // when the port is not explicitly set (if not set, the port will be set to -1)
            if (uriBuilder.getPort() == -1)
            {
                if ("https".equalsIgnoreCase(uriBuilder.getScheme())) {
                    uriBuilder.setPort(443);
                }
                else if ("http".equalsIgnoreCase(uriBuilder.getScheme())) {
                    uriBuilder.setPort(80);
                }
            }

            URI uri = uriBuilder.build();

            SimpleHttpRequest request = SimpleRequestBuilder.post(uri).build();

            final EntityBuilder entityBuilder = EntityBuilder.create();

            entityBuilder.setParameters(
                    new BasicNameValuePair("To", phoneNumber),
                    new BasicNameValuePair("From", from),
                    new BasicNameValuePair("Body", message));

            CloseableHttpAsyncClient httpClient = getHTTPClient();

            HttpClientContext clientContext = httpClientContextFactory.createHttpClientContext();

            // Enable preemptive basic auth
            BasicScheme basicScheme = new BasicScheme();
            basicScheme.initPreemptive(new UsernamePasswordCredentials(username, password.toCharArray()));

            AuthCache authCache = new BasicAuthCache();
            authCache.put(new HttpHost(uri.getScheme(), uri.getHost(), uri.getPort()), basicScheme);

            clientContext.setAuthCache(authCache);

            ByteArrayOutputStream response = new ByteArrayOutputStream();

            AbstractVoidBinResponseConsumer consumer;

            try(
                WritableByteChannel outputChannel = Channels.newChannel(new SizeLimitedOutputStream(response,
                    maxResponseSize));

                HttpEntity contentEntity = entityBuilder.build();
            )
            {
                consumer = new AbstractVoidBinResponseConsumer()
                {
                    @Override
                    protected void data(ByteBuffer src, boolean endOfStream)
                    throws IOException
                    {
                        try {
                            outputChannel.write(src);
                        }
                        catch (LimitReachedException e) {
                            // If the limit was reached, we do not want HTTPClient to retry. The
                            // @DefaultHttpRequestRetryStrategy will not retry if the exception is a
                            // ConnectionClosedException
                            throw new ConnectionClosedException(MAX_RESPONSE_SIZE_EXCEEDED_ERROR, e);
                        }
                    }
                };

                Future<Void> future = httpClient.execute(
                        new BasicRequestProducer(request, new BasicAsyncEntityProducer(IOUtils.toByteArray(
                                contentEntity.getContent()), ContentType.APPLICATION_FORM_URLENCODED)),
                        consumer, clientContext, null);

                future.get(totalTimeout, TimeUnit.MILLISECONDS);
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();

                throw new IOException(e);
            }
            catch (ExecutionException e) {
                throw new IOException(ExceptionUtils.getRootCause(e));
            }
            catch (TimeoutException e) {
                throw new IOException(TIMEOUT_ERROR + url);
            }

            parseResponse(response.toByteArray(), consumer);
        }
        catch (HierarchicalPropertiesException | URISyntaxException | GeneralSecurityException e) {
            throw new IOException(getName() + " error sending SMS. " + e.getMessage(), e);
        }
    }

    public int getMaxResponseSize() {
        return maxResponseSize;
    }

    public void setMaxResponseSize(int maxResponseSize) {
        this.maxResponseSize = maxResponseSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static void main(String[] args)
    throws Exception
    {
        StaticTwilioPropertiesProvider propertiesProvider = new StaticTwilioPropertiesProvider();

        propertiesProvider.setAccountSid("***");
        //propertiesProvider.setAuthToken("***");
        propertiesProvider.setApiKeySid("***");
        propertiesProvider.setApiKeySecret("***");
        //propertiesProvider.setFrom("+15005550006");
        propertiesProvider.setFrom("+12019879823");

        TwilioSMSTransport transport = new TwilioSMSTransport(
                new CloseableHttpAsyncClientFactoryImpl(null),
                new HttpClientContextFactoryImpl(new SystemDefaultCredentialsProvider()),
                propertiesProvider);

        transport.sendSMS("+316***", "test");
    }
}
