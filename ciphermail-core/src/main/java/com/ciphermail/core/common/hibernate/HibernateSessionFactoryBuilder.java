/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.Properties;

/**
 * Builds a Hibernate SessionFactory
 */
public class HibernateSessionFactoryBuilder
{
    /*
     * The Hibernate properties
     */
    private Properties properties;

    /*
     * The database PhysicalNamingStrategy to use
     */
    private PhysicalNamingStrategy physicalNamingStrategy = new PrefixPhysicalNamingStrategy();

    /*
     * Packages to scan for entities
     */
    private String[] packagesToScan;

    /*
     * Entiry classes
     */
    private Class<?>[] annotatedClasses;

    /*
     * Holder for Hibernate configuration info
     */
    private HibernateInfo hibernateInfo;

    public static HibernateSessionFactoryBuilder createInstance() {
        return new HibernateSessionFactoryBuilder();
    }

    public HibernateSessionFactoryBuilder setProperties(Properties properties)
    {
        this.properties = properties;

        return this;
    }

    public HibernateSessionFactoryBuilder setPhysicalNamingStrategy(PhysicalNamingStrategy physicalNamingStrategy)
    {
        this.physicalNamingStrategy = physicalNamingStrategy;

        return this;
    }

    public HibernateSessionFactoryBuilder setPackagesToScan(String... packagesToScan)
    {
        this.packagesToScan = packagesToScan;

        return this;
    }

    public HibernateSessionFactoryBuilder addAnnotatedClasses (Class<?>... annotatedClasses)
    {
        this.annotatedClasses = annotatedClasses;

        return this;
    }

    public HibernateSessionFactoryBuilder setHibernateInfo(HibernateInfo hibernateInfo)
    {
        this.hibernateInfo = hibernateInfo;

        return this;
    }

    public HibernateSessionFactoryBuilder setPhysicalNamingStrategyClassName(String physicalNamingStrategyClassName)
    {
        try {
            if (StringUtils.isNotEmpty(physicalNamingStrategyClassName))
            {
                this.physicalNamingStrategy = (PhysicalNamingStrategy) Class.forName(physicalNamingStrategyClassName).
                        getDeclaredConstructor().newInstance();
            }
        }
        catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException |
               ClassNotFoundException e)
        {
            throw new UnhandledException(e);
        }

        return this;
    }

    public SessionFactory build()
    {
        Objects.requireNonNull(properties, "properties is missing");
        Objects.requireNonNull(hibernateInfo, "hibernateInfo is missing");

        BootstrapServiceRegistryBuilder bootstrapServiceRegistryBuilder = new BootstrapServiceRegistryBuilder();

        bootstrapServiceRegistryBuilder.applyIntegrator(new HibernateInfoIntegrator(hibernateInfo));

        MetadataSources metadataSources = new MetadataSources(
                bootstrapServiceRegistryBuilder.build());

        ResourceLoader resourceLoader = new PathMatchingResourcePatternResolver();

        LocalSessionFactoryBuilder sessionFactoryBuilder = new LocalSessionFactoryBuilder(null,
                resourceLoader, metadataSources);

        // scan packages for entities
        if (packagesToScan != null) {
            sessionFactoryBuilder.scanPackages(packagesToScan);
        }

        if (annotatedClasses != null) {
            sessionFactoryBuilder.addAnnotatedClasses(annotatedClasses);
        }

        if (physicalNamingStrategy != null) {
            sessionFactoryBuilder.setPhysicalNamingStrategy(physicalNamingStrategy);
        }

        sessionFactoryBuilder.addProperties(properties);

        return sessionFactoryBuilder.buildSessionFactory();
    }
}
