/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.cms.CMSAuthEnvelopedDataStreamGenerator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.RecipientInfoGenerator;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMEStreamingProcessor;
import org.bouncycastle.operator.OutputAEADEncryptor;
import org.bouncycastle.operator.OutputEncryptor;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.OutputStream;

/**
 * The SMIMEAuthEnvelopedGenerator class is a subclass of SMIMEEnvelopedGenerator that is used to generate S/MIME
 * enveloped objects with an additional layer of authentication. It provides methods for adding recipient information
 * generators, setting the BER encoding for recipient information, and generating the enveloped object.
 * <p>
 * Note:  SMIMEEnvelopedGenerator cannot be easily extended because a lot of methods and utility classes are private.
 *        Some methods are therefore duplicated and modified where required
 *
 * @see SMIMEEnvelopedGenerator
 */
public class SMIMEAuthEnvelopedGenerator extends SMIMEEnvelopedGenerator
{
    private final AuthEnvelopedGenerator envelopedGenerator;

    public SMIMEAuthEnvelopedGenerator() {
        envelopedGenerator = new AuthEnvelopedGenerator();
    }

    /**
     * Adds a recipient information generator to the SMIMEAuthEnvelopedGenerator.
     *
     * @param recipientInfoGen the recipient information generator to add
     */
    @Override
    public void addRecipientInfoGenerator(RecipientInfoGenerator recipientInfoGen) {
        envelopedGenerator.addRecipientInfoGenerator(recipientInfoGen);
    }

    /**
     * Sets whether to use a BER Set to store the recipient information in the SMIMEAuthEnvelopedGenerator.
     *
     * @param berEncodeRecipientSet true to use BER encoding, false otherwise
     */
    @Override
    public void setBerEncodeRecipients(boolean berEncodeRecipientSet)  {
        envelopedGenerator.setBEREncodeRecipients(berEncodeRecipientSet);
    }

    private MimeBodyPart makeAuthBodyPart(MimeBodyPart content, OutputEncryptor encryptor)
    throws SMIMEException
    {
        try {
            MimeBodyPart data = new MimeBodyPart();

            if (!(encryptor instanceof OutputAEADEncryptor outputAEADEncryptor)) {
                throw new SMIMEException("encryptor is not a AEAD encryptor");
            }

            data.setContent(new ContentEncryptor(content, outputAEADEncryptor), SMIMEHeader.ENCRYPTED_AUTH_CONTENT_TYPE);
            data.addHeader("Content-Type", SMIMEHeader.ENCRYPTED_AUTH_CONTENT_TYPE);
            data.addHeader("Content-Disposition", "attachment; filename=\"smime.p7m\"");
            data.addHeader("Content-Description", "S/MIME Encrypted Message");
            data.addHeader("Content-Transfer-Encoding", encoding);

            return data;
        }
        catch (MessagingException e) {
            throw new SMIMEException("exception putting multi-part together.", e);
        }
    }

    /**
     * Override because the body part should already have been encoded
     */
    @Override
    protected MimeBodyPart makeContentBodyPart(MimeBodyPart content)
    {
        // we do not need to do any encoding because we already did that
        return content;
    }

    /**
     * Generates an enveloped object that contains an SMIME Enveloped object using the given content encryptor.
     *
     * @param content   the content to be encrypted and included in the enveloped object
     * @param encryptor the encryptor to be used for encryption
     * @return the generated enveloped object as a MimeBodyPart
     * @throws SMIMEException if an error occurs during the generation process
     */
    @Override
    public MimeBodyPart generate(MimeBodyPart content, OutputEncryptor encryptor)
    throws SMIMEException
    {
        return makeAuthBodyPart(makeContentBodyPart(content), encryptor);
    }

    /**
     * Generates an enveloped object that contains an SMIME Enveloped object using the given provider from the contents
     * of the passed in message.
     *
     * @param message the MimeMessage to be encrypted and included in the enveloped object
     * @param encryptor the OutputEncryptor to be used for encryption
     * @return the generated enveloped object as a MimeBodyPart
     * @throws SMIMEException if an error occurs during the generation process
     */
    @Override
    public MimeBodyPart generate(MimeMessage message, OutputEncryptor  encryptor)
    throws SMIMEException
    {
        return makeAuthBodyPart(makeContentBodyPart(message), encryptor);
    }

    private class ContentEncryptor implements SMIMEStreamingProcessor
    {
        private final MimeBodyPart content;
        private final OutputAEADEncryptor encryptor;

        private boolean firstTime = true;

        ContentEncryptor(MimeBodyPart content, OutputAEADEncryptor encryptor)
        {
            this.content = content;
            this.encryptor = encryptor;
        }

        @Override
        public void write(OutputStream out)
        throws IOException
        {
            OutputStream encrypted;

            try {
                if (firstTime) {
                    encrypted = envelopedGenerator.open(out, encryptor);

                    firstTime = false;
                }
                else {
                    encrypted = envelopedGenerator.regenerate(out, encryptor);
                }

                content.writeTo(encrypted);

                encrypted.close();
            }
            catch (MessagingException | CMSException e) {
                throw new IOException(e);
            }
        }
    }

    private static class AuthEnvelopedGenerator extends CMSAuthEnvelopedDataStreamGenerator
    {
        private ASN1ObjectIdentifier dataType;
        private ASN1EncodableVector recipientInfos;

        @Override
        protected OutputStream open(
                ASN1ObjectIdentifier dataType,
                OutputStream out,
                ASN1EncodableVector recipientInfos,
                OutputAEADEncryptor encryptor)
        throws IOException
        {
            this.dataType = dataType;
            this.recipientInfos = recipientInfos;

            return super.open(dataType, out, recipientInfos, encryptor);
        }

        OutputStream regenerate(OutputStream out, OutputAEADEncryptor encryptor)
        throws IOException
        {
            return super.open(dataType, out, recipientInfos, encryptor);
        }
    }
}
