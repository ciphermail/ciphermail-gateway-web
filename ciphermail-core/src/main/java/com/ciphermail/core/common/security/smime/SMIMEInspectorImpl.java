/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.security.cms.CryptoMessageSyntaxException;
import com.ciphermail.core.common.security.keystore.BasicKeyStore;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Objects;

/**
 * The SMIMEInspectorImpl class is designed to inspect and process different types of S/MIME messages
 * (signed, encrypted, authenticated encrypted, compressed, or none). It delegates the actual inspection tasks to
 * type-specific inspector classes and provides methods to retrieve the content of the S/MIME message as either a
 * MimeBodyPart or a MimeMessage.
 */
public class SMIMEInspectorImpl implements SMIMEInspector
{
    private SMIMESignedInspector signedInspector;
    private SMIMEEnvelopedInspector envelopedInspector;
    private SMIMECompressedInspector compressedInspector;

    private final Part part;
    private final SMIMEType type;

    public SMIMEInspectorImpl(@Nonnull Part part, @Nonnull BasicKeyStore basicKeyStore, String provider)
    throws MessagingException, IOException, SMIMEInspectorException
    {
        this (part, basicKeyStore, provider, provider);
    }

    public SMIMEInspectorImpl(@Nonnull Part part, @Nonnull BasicKeyStore basicKeyStore, String nonSensitiveProvider,
            String sensitiveProvider)
    throws MessagingException, IOException, SMIMEInspectorException
    {
        this.part = Objects.requireNonNull(part);

        type = SMIMEUtils.getSMIMEType(part);

        switch (type) {
            case NONE            -> { /* do nothing */ }
            case SIGNED          -> signedInspector = new SMIMESignedInspectorImpl(part, nonSensitiveProvider,
                                        sensitiveProvider);
            case ENCRYPTED       -> envelopedInspector = new SMIMEEnvelopedInspectorImpl(part, basicKeyStore,
                                        nonSensitiveProvider, sensitiveProvider);
            case AUTH_ENCRYPTED  -> envelopedInspector = new SMIMEAuthEnvelopedInspectorImpl(part, basicKeyStore,
                                        nonSensitiveProvider, sensitiveProvider);
            case COMPRESSED      -> compressedInspector = new SMIMECompressedInspectorImpl(part);
        }
    }

    @Override
    public SMIMEType getSMIMEType() {
        return type;
    }

    @Override
    public SMIMESignedInspector getSignedInspector() {
        return signedInspector;
    }

    @Override
    public SMIMEEnvelopedInspector getEnvelopedInspector() {
        return envelopedInspector;
    }

    @Override
    public SMIMECompressedInspector getCompressedInspector() {
        return compressedInspector;
    }

    /**
     * Returns the content as a MimeBodyPart. The content depends on the S/MIME type.
     */
    @Override
    public MimeBodyPart getContentAsMimeBodyPart()
    throws MessagingException, CryptoMessageSyntaxException
    {
        try {
            return switch (getSMIMEType()) {
                case SIGNED          -> getSignedInspector().getContent();
                case ENCRYPTED       -> getEnvelopedInspector().getContentAsMimeBodyPart();
                case AUTH_ENCRYPTED  -> getEnvelopedInspector().getContentAsMimeBodyPart();
                case COMPRESSED      -> getCompressedInspector().getContentAsMimeBodyPart();
                case NONE            -> BodyPartUtils.toMimeBodyPart(part);
                default -> throw new IllegalStateException("Unknown S/MIME type.");
            };
        }
        catch (IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }

    /**
     * Returns the content as a MimeMessage. The content depends on the S/MIME type.
     */
    @Override
    public MimeMessage getContentAsMimeMessage()
    throws MessagingException, CryptoMessageSyntaxException
    {
        try {
            return switch (getSMIMEType()) {
                case SIGNED          -> getSignedInspector().getContentAsMimeMessage();
                case ENCRYPTED       -> getEnvelopedInspector().getContentAsMimeMessage();
                case AUTH_ENCRYPTED  -> getEnvelopedInspector().getContentAsMimeMessage();
                case COMPRESSED      -> getCompressedInspector().getContentAsMimeMessage();
                case NONE            -> BodyPartUtils.toMessage(part);
                default -> throw new IllegalStateException("Unknown S/MIME type.");
            };
        }
        catch (IOException e) {
            throw new CryptoMessageSyntaxException(e);
        }
    }
}
