/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.util.SizeLimitedInputStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPMarker;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.bouncycastle.util.io.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class PGPHandler
{
    private static final Logger logger = LoggerFactory.getLogger(PGPHandler.class);

    /*
     * Provides PGP key pair based on key id
     */
    private final PGPKeyPairProvider keyPairProvider;

    /*
     * For decrypting PGPPublicKeyEncryptedData packets
     */
    private final JcePublicKeyDataDecryptorFactoryBuilder decryptorFactoryBuilder;

    /*
     * The PGP layers
     */
    private final List<PGPLayer> pgpLayers = new LinkedList<>();

    /*
     * Listener which is called when a PGP keyring is found
     */
    private PGPPublicKeyRingListener publicKeyRingListener;

    /*
     * If true (the default), the PGP blob will be decrypted if it is encrypted and a suitable key is available
     */
    private boolean decrypt = true;

    /*
     * The maximum number of bytes read for decompression. This is done to protect against "ZIP bombs".
     * The default value will be set to 10% of Runtime.getRuntime().maxMemory()
     */
    private int decompressionUpperlimit = (int) (Runtime.getRuntime().maxMemory() * 0.1);

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * The nr of PGP objects that are read
     */
    private int nrOfObjectsRead;

    /*
     * The filename of the part. This is used when the part is a detached signature so we can lookup
     * the part for validating the signature
     */
    private String filename;

    public PGPHandler(@Nonnull PGPKeyPairProvider keyPairProvider)
    {
        this.keyPairProvider = Objects.requireNonNull(keyPairProvider);

        SecurityFactory securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

        this.decryptorFactoryBuilder = new JcePublicKeyDataDecryptorFactoryBuilder();

        this.decryptorFactoryBuilder.setProvider(securityFactory.getSensitiveProvider());
        this.decryptorFactoryBuilder.setContentProvider(securityFactory.getNonSensitiveProvider());
    }

    public void handle(@Nonnull InputStream input, @Nonnull OutputStream output)
    throws IOException, PGPException
    {
        nrOfObjectsRead = 0;

        handle(input, output, -1);
    }

    private void handle(@Nonnull InputStream input, @Nonnull OutputStream output, int level)
    throws IOException, PGPException
    {
        level++;

        pgpLayers.add(new PGPLayer(level));

        InputStream decoderStream = PGPUtil.getDecoderStream(input);

        String[] armorHeaders = null;

        if (decoderStream instanceof ArmoredInputStream armoredInputStream) {
            armorHeaders = armoredInputStream.getArmorHeaders();
        }

        PGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);

        Object streamObject = null;

        while ((streamObject = objectFactory.nextObject()) != null)
        {
            logger.debug("next PGP object. level: {}, Type: {}", level, streamObject.getClass());

            nrOfObjectsRead++;

            if (nrOfObjectsRead > maxObjects) {
                throw new PGPException("Maximum number of PGP objects read.");
            }

            if (streamObject instanceof PGPMarker) {
                // Ignore markers
                continue;
            }
            else if (streamObject instanceof PGPEncryptedDataList pgpEncryptedDataList) {
                handleEncryptedDataList(pgpEncryptedDataList, output, armorHeaders, level);
            }
            else if (streamObject instanceof PGPCompressedData pgpCompressedData) {
                handleCompressedData(pgpCompressedData, output, armorHeaders, level);
            }
            else if (streamObject instanceof PGPOnePassSignatureList pgpOnePassSignatureList) {
                handleOnePassSignatureList(pgpOnePassSignatureList, output, armorHeaders, level);
            }
            else if (streamObject instanceof PGPSignatureList pgpSignatureList) {
                handleSignatureList(pgpSignatureList, output, armorHeaders, level);
            }
            else if (streamObject instanceof PGPLiteralData pgpLiteralData) {
                output = handleLiteralData(pgpLiteralData, output, armorHeaders, level);
            }
            else if (streamObject instanceof PGPPublicKeyRing pgpPublicKeyRing)
            {
                if (publicKeyRingListener != null) {
                    publicKeyRingListener.onPGPPublicKeyRing(pgpPublicKeyRing);
                }
            }
            else {
                logger.warn("Unsupported PGP object {}", streamObject.getClass());
            }
        }
    }

    private void handleEncryptedDataList(PGPEncryptedDataList encryptedDataList, OutputStream output,
        String[] armorHeaders, int level)
    throws IOException, PGPException
    {
        Iterator<?> iterator = encryptedDataList.getEncryptedDataObjects();

        PGPPublicKeyEncryptedData encryptedData = null;

        List<PGPKeyRingPair> keyPairs = null;

        // The key id's (in hex form) for which the message was encrypted. This is for logging purposes
        Set<String> keyIDsHex = new HashSet<>();

        while (iterator.hasNext())
        {
            Object object = iterator.next();

            // We only support PGPPublicKeyEncryptedData for now
            if (object instanceof PGPPublicKeyEncryptedData)
            {
                encryptedData = (PGPPublicKeyEncryptedData) object;

                String keyIDHex = PGPUtils.getKeyIDHex(encryptedData.getKeyID());

                logger.debug("PGPPublicKeyEncryptedData with key id {}", keyIDHex);

                keyIDsHex.add(keyIDHex);

                // Check if we have a private key for encrypted data
                keyPairs = keyPairProvider.getByKeyID(encryptedData.getKeyID());

                if (CollectionUtils.isNotEmpty(keyPairs))
                {
                    logger.debug("Key pair(s) for key id {} found", keyIDHex);

                    break;
                }
            }
            else {
                logger.debug("Unsupported EncryptedDataObject. Class: {}", object.getClass());
            }
        }

        if (!decrypt) {
            throw new PGPDecryptionDisabledException(keyIDsHex);
        }

        if (CollectionUtils.isEmpty(keyPairs)) {
            throw new PGPDecryptionKeyNotFoundException(keyIDsHex);
        }

        // always use the first KeyPair
        //
        // Note: in practice only one key pair will match the long key id
        PGPKeyRingPair keypair = keyPairs.get(0);

        PublicKeyDataDecryptorFactory decryptorFactory =  decryptorFactoryBuilder.build(keypair.getPrivateKey());

        InputStream decryptedInput = encryptedData.getDataStream(decryptorFactory);

        PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.fromTag(
                encryptedData.getSymmetricAlgorithm(decryptorFactory));

        pgpLayers.get(level).addPart(new PGPEncryptionLayerPartImpl(armorHeaders, encryptedDataList,
                encryptedData, encryptionAlgorithm));

        // Recursively handle the decrypted data
        handle(decryptedInput, output, level);
    }

    private void handleCompressedData(PGPCompressedData compressedData, OutputStream output, String[] armorHeaders,
        int level)
    throws IOException, PGPException
    {
        pgpLayers.get(level).addPart(new PGPCompressionLayerPartImpl(armorHeaders, compressedData));

        // Recursively handle decompressed data.
        //
        // Note: Wrap in a SizeLimitedInputStream to protect against "zip bombs"
        handle(new SizeLimitedInputStream(compressedData.getDataStream(), decompressionUpperlimit), output, level);
    }

    private void handleOnePassSignatureList(PGPOnePassSignatureList signatureList, OutputStream output,
        String[] armorHeaders, int level)
    {
        // Not supported
    }

    private void handleSignatureList(PGPSignatureList signatureList, OutputStream output,
        String[] armorHeaders, int level)
    {
        pgpLayers.get(level).addPart(new PGPSignatureLayerPartImpl(armorHeaders, signatureList));
    }

    private OutputStream handleLiteralData(PGPLiteralData literalData, OutputStream output, String[] armorHeaders,
        int level)
    throws IOException
    {
        ByteArrayOutputStream literalContent = new ByteArrayOutputStream();

        output = new TeeOutputStream(output, literalContent);

        pgpLayers.get(level).addPart(new PGPLiteralLayerPartImpl(armorHeaders, literalData, literalContent));

        IOUtils.copy(literalData.getDataStream(), output);

        return output;
    }

    public boolean isDecrypt() {
        return decrypt;
    }

    public void setDecrypt(boolean decrypt) {
        this.decrypt = decrypt;
    }

    public int getDecompressionUpperlimit() {
        return decompressionUpperlimit;
    }

    public void setDecompressionUpperlimit(int decompressionUpperlimit) {
        this.decompressionUpperlimit = decompressionUpperlimit;
    }

    public int getMaxObjects() {
        return maxObjects;
    }

    public void setMaxObjects(int maxObjects) {
        this.maxObjects = maxObjects;
    }

    public List<PGPLayer> getPGPLayers() {
        return pgpLayers;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public PGPPublicKeyRingListener getPublicKeyRingListener() {
        return publicKeyRingListener;
    }

    public void setPublicKeyRingListener(PGPPublicKeyRingListener publicKeyRingListener) {
        this.publicKeyRingListener = publicKeyRingListener;
    }
}
