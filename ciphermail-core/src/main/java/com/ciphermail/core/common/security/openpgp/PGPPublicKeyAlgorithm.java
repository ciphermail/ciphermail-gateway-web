/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

/**
 * The OpenPGP public key algorithms as defined in section "9.1.  Public-Key Algorithms" of RFC 4880
 *
 * @author Martijn Brinkers
 *
 */
public enum PGPPublicKeyAlgorithm
{
    RSA             (1, "RSA"),
    RSA_ENCRYPT     (2, "RSA encrypt only"),
    RSA_SIGN        (3, "RSA sign only"),
    ELGAMAL         (16, "Elgamal encrypt only"),
    DSA             (17, "DSA"),
    ECDH            (18, "Elliptic Curve"),
    ECDSA           (19, "Elliptic Curve DSA"),
    ELGAMAL_GENERAL (20, "Elgamal"),
    DIFFIE_HELLMAN  (21, "Diffie-Hellman"),
    EDDSA           (22, "EdDSA"),
    EXPERIMENTAL_1  (100, "Experimental 1"),
    EXPERIMENTAL_2  (101, "Experimental 2"),
    EXPERIMENTAL_3  (102, "Experimental 3"),
    EXPERIMENTAL_4  (103, "Experimental 4"),
    EXPERIMENTAL_5  (104, "Experimental 5"),
    EXPERIMENTAL_6  (105, "Experimental 6"),
    EXPERIMENTAL_7  (106, "Experimental 7"),
    EXPERIMENTAL_8  (107, "Experimental 8"),
    EXPERIMENTAL_9  (108, "Experimental 9"),
    EXPERIMENTAL_10 (109, "Experimental 10"),
    EXPERIMENTAL_11 (110, "Experimental 11");

    /*
     * The tag value
     */
    private final int tag;

    /*
     * The friendly name
     */
    private final String friendlyName;

    PGPPublicKeyAlgorithm(int tag, String friendlyName)
    {
        this.tag = tag;
        this.friendlyName = friendlyName;
    }

    public int getTag() {
        return tag;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public static PGPPublicKeyAlgorithm fromTag(int tag)
    {
        for (PGPPublicKeyAlgorithm algorithm : PGPPublicKeyAlgorithm.values())
        {
            if (tag == algorithm.tag) {
                return algorithm;
            }
        }

        return null;
    }
}
