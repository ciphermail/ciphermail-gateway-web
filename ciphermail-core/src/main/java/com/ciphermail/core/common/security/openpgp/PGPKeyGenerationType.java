/*
 * Copyright (c) 2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.apache.commons.lang.StringUtils;

/**
 * List of algorithms and key strength (if applicable) which can be selected when generating a private key
 */
public enum PGPKeyGenerationType
{
    RSA_1024                ("RSA-1024"),
    RSA_2048                ("RSA-2048"),
    RSA_3072                ("RSA-3072"),
    RSA_4096                ("RSA-4096"),
    ECC_NIST_P_256          ("NIST-P-256"),
    ECC_NIST_P_384          ("NIST-P-384"),
    ECC_NIST_P_521          ("NIST-P-521"),
    ECC_BRAINPOOL_P_256     ("Brainpool256"),
    ECC_BRAINPOOL_P_384     ("Brainpool384"),
    ECC_BRAINPOOL_P_512     ("Brainpool512");

    private final String friendlyName;

    PGPKeyGenerationType(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public static PGPKeyGenerationType fromName(String name)
    {
        name = StringUtils.trimToNull(name);

        for (PGPKeyGenerationType type : PGPKeyGenerationType.values())
        {
            if (type.name().equalsIgnoreCase(name)) {
                return type;
            }
        }

        return null;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
