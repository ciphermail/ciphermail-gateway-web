/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * CurrentDateProvider can be used to override the "current" time. This is for example used for S/MIME and PGP unit
 * tests which use test resources which are only valid within a certain date range (for example an X509Certificate)
 * <p>
 * Note: the date override is only set for the calling thread. DON'T FORGET TO REMOVE THE OVERRIDE AFTERWARDS
 */
public class CurrentDateProvider
{
    private static final Logger logger = LoggerFactory.getLogger(CurrentDateProvider.class);

    private static final ThreadLocal<Date> dateOverride = new ThreadLocal<>();

    private CurrentDateProvider() {
        // empty on purpose
    }

    public static Date getNow()
    {
        Date now = dateOverride.get();

        if (now != null)
        {
            // log to std out and logger to make sure the override cannot be missed
            System.out.println("******* DATE OVERRIDE IS SET FOR CURRENT THREAD ******");
            logger.warn("******* DATE OVERRIDE IS SET FOR CURRENT THREAD ******");

            return now;
        }

        return new Date();
    }

    public static void setDateOverrideForCurrentThread(Date date) {
        dateOverride.set(date);
    }

    public static void removeDateOverrideForCurrentThread() {
        dateOverride.remove();
    }
}
