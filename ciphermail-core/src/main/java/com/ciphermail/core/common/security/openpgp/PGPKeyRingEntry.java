/*
 * Copyright (c) 2013-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * PGPKeyRingEntry is an item from a PGPKeyRing
 *
 * @author Martijn Brinkers
 *
 */
public interface PGPKeyRingEntry
{
    /**
     * The unique ID of this key. The ID is only locally unique and should not be used externally. Use the
     * KeyID or fingerprint to globally identify a key.
     */
    UUID getID();

    /**
     * Identifies a key. Implementations SHOULD NOT assume that Key IDs are unique.
     */
    Long getKeyID();

    /**
     * The date the key was created
     */
    Date getCreationDate();

    /**
     * The date the key expires
     */
    Date getExpirationDate();

    /**
     * The date the key was added to the key ring
     */
    Date getInsertionDate();

    /**
     * Sets the public key
     */
    void setPublicKey(@Nonnull PGPPublicKey publicKey)
    throws PGPException, IOException;

    /**
     * Returns the public key of this key.
     */
    PGPPublicKey getPublicKey()
    throws PGPException, IOException;

    /**
     * Returns the private key associated with this key. Null if there is no key associated.
     */
    PGPPrivateKey getPrivateKey()
    throws PGPException, IOException;

    /**
     * Returns the key alias if there is a private key associated with this entry
     */
    String getPrivateKeyAlias();

    /**
     * The fingerprint (hash) of the key. The fingerprint algorithm used depends on whether the key is a V3 of V4 key.
     */
    String getFingerprint();

    /**
     * The SHA256 fingerprint of the key.
     */
    String getSHA256Fingerprint();

    /**
     * The userIDs (in most cases, an email address) associated with the key.
     */
    Set<String> getUserIDs();

    /**
     * The email addresses associated with the key. The email addresses must be explicitly added, i.e., they
     * are not automatically taken from the UserIDs.
     */
    Set<String> getEmail();
    void setEmail(Set<String> email);

    /**
     * True if this key is a master key.
     */
    boolean isMasterKey();

    /**
     * The parent entry. Null if this is a master key.
     */
    PGPKeyRingEntry getParentKey();

    /**
     * The sub key entries.
     */
    Set<PGPKeyRingEntry> getSubkeys();

    /**
     * The key ring name will be used to support multiple key rings
     */
    String getKeyRingName();
}
