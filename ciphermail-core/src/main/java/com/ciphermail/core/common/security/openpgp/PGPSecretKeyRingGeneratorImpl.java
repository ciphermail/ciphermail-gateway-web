/*
 * Copyright (c) 2014-2020, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.SecurityFactory;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.PublicKeyAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPDigestCalculatorProviderBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyEncryptorBuilder;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Generates a secret keyring with a master key and and encryption subkey
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSecretKeyRingGeneratorImpl implements PGPSecretKeyRingGenerator
{
    /*
     * The factory that creates the KeyPairGenerator, RandomGenerator etc.
     */
    private final SecurityFactory securityFactory = PGPSecurityFactoryFactory.getSecurityFactory();

    /*
     * Holder for master keypair and encryption sub keypair
     */
    private static class KeyPairs
    {
        private PGPKeyPair masterKeyPair;
        private PGPKeyPair encryptionKeyPair;
    }

    private KeyPairs generateKeyPairs(@Nonnull PGPKeyGenerationType keyType)
    throws NoSuchAlgorithmException, NoSuchProviderException, PGPException, InvalidAlgorithmParameterException
    {
        KeyPairs keyPairs = new KeyPairs();

        Date now = new Date();

        KeyPairGenerator keyPairGenerator;

        if (PGPKeyGenerationType.RSA_1024.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
            keyPairGenerator.initialize(1024);

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.RSA_2048.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
            keyPairGenerator.initialize(2048);

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.RSA_3072.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
            keyPairGenerator.initialize(3072);

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.RSA_4096.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("RSA");
            keyPairGenerator.initialize(4096);

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.RSA_GENERAL,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_NIST_P_256.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("P-256"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_NIST_P_384.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("P-384"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_NIST_P_521.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("P-521"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_BRAINPOOL_P_256.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("brainpoolP256r1"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_BRAINPOOL_P_384.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("brainpoolP384r1"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else if (PGPKeyGenerationType.ECC_BRAINPOOL_P_512.equals(keyType))
        {
            keyPairGenerator = securityFactory.createKeyPairGenerator("ECDH");
            keyPairGenerator.initialize(new ECGenParameterSpec("brainpoolP512r1"));

            keyPairs.masterKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDSA,
                    keyPairGenerator.generateKeyPair(), now);

            keyPairs.encryptionKeyPair = new HSMSafePGPKeyPair(PublicKeyAlgorithmTags.ECDH,
                    keyPairGenerator.generateKeyPair(), now);
        }
        else {
            throw new IllegalArgumentException("Unsupported PGPKeyGenerationType " + keyType);
        }

        return keyPairs;
    }

    @Override
    public PrivateKeysAndPublicKeyRing generateKeyRing(@Nonnull PGPKeyGenerationType keyType, @Nonnull String userID)
    throws PGPException, IOException
    {
        try {
            KeyPairs keyPairs = generateKeyPairs(keyType);

            PGPKeyRingGenerator keyRingGenerator = createKeyRingGenerator(keyPairs.masterKeyPair, userID, null);

            addEncryptionSubkey(keyRingGenerator, keyPairs.encryptionKeyPair);

            return new PrivateKeysAndPublicKeyRingImpl(keyRingGenerator.generatePublicKeyRing(), keyPairs.masterKeyPair,
                    keyPairs.encryptionKeyPair);
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new IOException(e);
        }
    }

    @Override
    public PGPKeyRingGenerator createKeyRingGenerator(@Nonnull PGPKeyPair masterKeyPair, @Nonnull String userID,
            char[] password)
    throws PGPException, IOException
    {
        try {
            PGPSignatureSubpacketGenerator sigSubpacketGenerator = new PGPSignatureSubpacketGenerator();

            sigSubpacketGenerator.setKeyFlags(false /* non critical */, KeyFlags.CERTIFY_OTHER | KeyFlags.SIGN_DATA);

            sigSubpacketGenerator.setFeature(false /* non critical */, Features.FEATURE_MODIFICATION_DETECTION);

            sigSubpacketGenerator.setPreferredSymmetricAlgorithms(false, new int[] {
                    SymmetricKeyAlgorithmTags.AES_256,
                    SymmetricKeyAlgorithmTags.AES_192,
                    SymmetricKeyAlgorithmTags.AES_128,
                    SymmetricKeyAlgorithmTags.CAST5,
                    SymmetricKeyAlgorithmTags.TWOFISH,
                    SymmetricKeyAlgorithmTags.TRIPLE_DES });

            sigSubpacketGenerator.setPreferredHashAlgorithms(false, new int[] {
                    HashAlgorithmTags.SHA256,
                    HashAlgorithmTags.SHA384,
                    HashAlgorithmTags.SHA512,
                    HashAlgorithmTags.SHA1,
                    HashAlgorithmTags.RIPEMD160});

            sigSubpacketGenerator.setPreferredCompressionAlgorithms(false, new int[] {
                    CompressionAlgorithmTags.ZLIB,
                    CompressionAlgorithmTags.BZIP2,
                    CompressionAlgorithmTags.ZIP});

            sigSubpacketGenerator.setPrimaryUserID(false /* non critical */, true /* is primary user id */ );

            int hashAlgorithm = PGPUtils.getMinRequiredHashAlgorithm(masterKeyPair.getPublicKey());

            PGPDigestCalculator digestCalculator = new JcaPGPDigestCalculatorProviderBuilder().
                    setProvider(securityFactory.getNonSensitiveProvider()).build().get(
                    hashAlgorithm);

            JcaPGPContentSignerBuilder keySignerBuilder = new JcaPGPContentSignerBuilder(
                    masterKeyPair.getPublicKey().getAlgorithm(), hashAlgorithm);

            keySignerBuilder.setProvider(securityFactory.getSensitiveProvider());
            keySignerBuilder.setSecureRandom(securityFactory.createSecureRandom());

            int encryptionAlgorithm = PGPUtils.getMinRequiredSymmetricKeyAlgorithm(masterKeyPair.getPublicKey());

            JcePBESecretKeyEncryptorBuilder secretKeyEncryptorBuilder = new JcePBESecretKeyEncryptorBuilder(
                    password != null ? encryptionAlgorithm : SymmetricKeyAlgorithmTags.NULL);

            secretKeyEncryptorBuilder.setProvider(securityFactory.getNonSensitiveProvider());
            secretKeyEncryptorBuilder.setSecureRandom(securityFactory.createSecureRandom());

            return new PGPKeyRingGenerator(PGPSignature.DEFAULT_CERTIFICATION, masterKeyPair, userID, digestCalculator,
                    sigSubpacketGenerator.generate(), null, keySignerBuilder,
                    secretKeyEncryptorBuilder.build(password != null ? password : new char[]{}));
        }
        catch (NoSuchProviderException | NoSuchAlgorithmException e) {
            throw new IOException(e);
        }
    }

    private void addEncryptionSubkey(PGPKeyRingGenerator keyRingGenerator, PGPKeyPair keyPair)
    throws PGPException
    {
        PGPSignatureSubpacketGenerator sigSubpacketGenerator = new PGPSignatureSubpacketGenerator();

        sigSubpacketGenerator.setKeyFlags(false /* not critical */, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);

        keyRingGenerator.addSubKey(keyPair, sigSubpacketGenerator.generate(), null);
    }

    /*
     * Implementation of PrivateKeysAndPubliKeyRing
     */
    static class PrivateKeysAndPublicKeyRingImpl implements PrivateKeysAndPublicKeyRing
    {
        /*
         * The generated PGPPublicKeyRing
         */
        final PGPPublicKeyRing publicKeyRing;

        /*
         * Mapping from sha256 fingerprint of the PGP public key associated with the private key and the private key
         */
        final Map<String, PGPPrivateKey> privateKeys = new LinkedHashMap<>();

        private PrivateKeysAndPublicKeyRingImpl(PGPPublicKeyRing publicKeyRing, PGPKeyPair... keyPairs)
        throws IOException
        {
            this.publicKeyRing = publicKeyRing;

            for (PGPKeyPair keyPair : keyPairs) {
                privateKeys.put(getMapKey(keyPair.getPublicKey()), keyPair.getPrivateKey());
            }
        }

        private String getMapKey(PGPPublicKey publicKey)
        throws IOException
        {
            try {
                return PGPPublicKeyInspector.getSHA256FingerprintHex(publicKey);
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                throw new IOException(e);
            }
        }

        @Override
        public PGPPublicKeyRing getPublicKeyRing() {
            return publicKeyRing;
        }

        @Override
        public PGPPrivateKey getPrivateKey(PGPPublicKey publicKey)
        throws IOException
        {
            if (publicKey == null) {
                return null;
            }

            return privateKeys.get(getMapKey(publicKey));
        }
    }
}
