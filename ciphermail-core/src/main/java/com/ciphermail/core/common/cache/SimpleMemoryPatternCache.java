/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.cache;

import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * A basic PatternCache implementation that stores all the patterns in memory. Because this class is mainly used for
 * the DLP functionality, storing Regular Expressions in memory should not be an issue since the number of DLP rules
 * is limited.
 */
public class SimpleMemoryPatternCache implements PatternCache
{
    private static final Logger logger = LoggerFactory.getLogger(SimpleMemoryPatternCache.class);

    /*
     * Mapping from RegEx to compiled Pattern.
     */
    private final Map<String, Pattern> patterns = new HashMap<>();

    @Override
    public synchronized Pattern getPattern(@Nonnull String regExpr) {
        return getPattern(regExpr, 0);
    }

    @Override
    public synchronized Pattern getPattern(@Nonnull String regExpr, int flags)
    {
        // Store the pattern under a key with the flags pattern as a prefix to make it unique
        String key = flags + ":" + regExpr;

        return patterns.computeIfAbsent(key, k ->
        {
            logger.debug("Adding pattern {} to cache.", regExpr);

            Pattern pattern = null;

            try {
                pattern = Pattern.compile(regExpr, flags);
            }
            catch(PatternSyntaxException e) {
                logger.warn("{} is not a valid regular expression.", regExpr);
            }

            return pattern;
        });
    }

    @VisibleForTesting
    Map<String, Pattern> getPatterns() {
        return patterns;
    }
}
