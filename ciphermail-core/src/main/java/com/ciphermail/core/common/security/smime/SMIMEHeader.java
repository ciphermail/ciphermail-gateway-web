/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;


/**
 * The SMIMEHeader class represents a set of static methods for determining the S/MIME content type based on message headers.
 * <p>
 * The S/MIME content headers include:
 * - DETACHED_SIGNATURE_TYPE: used for detached signatures
 * - ENCAPSULATED_SIGNED_CONTENT_TYPE: used for encapsulated signed data
 * - ENCRYPTED_CONTENT_TYPE: used for enveloped data (encrypted)
 * - ENCRYPTED_AUTH_CONTENT_TYPE: used for auth enveloped data (authenticated and encrypted)
 * - COMPRESSED_CONTENT_TYPE: used for compressed data
 * <p>
 * The deprecated S/MIME content headers include:
 * - DEPRECATED_DETACHED_SIGNATURE_TYPE: used for deprecated detached signatures
 * - DEPRECATED_ENCAPSULATED_SIGNED_CONTENT_TYPE: used for deprecated encapsulated signed data
 * - DEPRECATED_ENCRYPTED_CONTENT_TYPE: used for deprecated enveloped data (encrypted)
 * - DEPRECATED_COMPRESSED_CONTENT_TYPE: used for deprecated compressed data
 * <p>
 * The Type enum represents the content type of the S/MIME blob and has the following possible values:
 * - NO_SMIME: no S/MIME content type
 * - CLEAR_SIGNED: clear-signed data
 * - OPAQUE_SIGNED: opaque-signed (signed-data) but the protocol could not be determined
 * - UNKNOWN_CLEAR_SIGNED: signed data but the protocol is unknown (could be PGP or S/MIME clear signed)
 * - ENCRYPTED: encrypted data
 * - ENCRYPTED_AUTH: authenticated and encrypted data
 * - COMPRESSED: compressed data
 * - ENVELOPED: encrypted or opaque-signed data (could not be determined from the headers)
 * - CERTS_ONLY: only certificates, no content
 *
 * The Strict enum represents the strict checking mode for the S/MIME content type and has two possible values:
 * - NO: strict checking is off, allowing octet-stream as a valid S/MIME type
 * - YES: strict checking is on, requiring 'complete' S/MIME headers
 * <p>
 * The class includes methods for getting the S/MIME content type of a given Part object:
 * - getSMIMEContentType(Part part, Strict strict): gets the S/MIME content type of the given Part object with strict checking mode
 * - getSMIMEContentType(Part part): gets the S/MIME content type of the given Part object with strict checking mode off
 * <p>
 * The class also includes a method for getting the S/MIME content type based on a content type header string and filename:
 * - getSMIMEContentType(String contentTypeHeader, String filename, Strict strict): gets the S/MIME content type based on the provided content type header, filename, and strict checking
 *  mode
 *
 * This class should be used by developers working with S/MIME content to determine the content type and perform appropriate actions.
 */
public class SMIMEHeader
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMEHeader.class);

    /*
     * The S/MIME content-headers
     */
    public static final String DETACHED_SIGNATURE_TYPE = "application/pkcs7-signature; name=smime.p7s; smime-type=signed-data";
    public static final String ENCAPSULATED_SIGNED_CONTENT_TYPE = "application/pkcs7-mime; name=smime.p7m; smime-type=signed-data";
    public static final String ENCRYPTED_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data";
    public static final String ENCRYPTED_AUTH_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=authEnveloped-data";
    public static final String COMPRESSED_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data";

    /*
     * The deprecated S/MIME content-headers
     */
    public static final String DEPRECATED_DETACHED_SIGNATURE_TYPE = "application/x-pkcs7-signature; name=smime.p7s; smime-type=signed-data";
    public static final String DEPRECATED_ENCAPSULATED_SIGNED_CONTENT_TYPE = "application/x-pkcs7-mime; name=smime.p7m; smime-type=signed-data";
    public static final String DEPRECATED_ENCRYPTED_CONTENT_TYPE = "application/x-pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data";
    public static final String DEPRECATED_COMPRESSED_CONTENT_TYPE = "application/x-pkcs7-mime; name=\"smime.p7z\"; smime-type=compressed-data";

    /**
     * The content type of the s/mime blob which is determined using the message headers.
     * <p>
     * ENVELOPED: encrypted or opaque signed (could not be determined from the headers).
     * UNKNOWN_SIGNED: Signed but protocol could not be determined from headers
     * (could be PGP or s/mime clear signed).
     */
    public enum Type {NO_SMIME, CLEAR_SIGNED, OPAQUE_SIGNED, UNKNOWN_CLEAR_SIGNED, ENCRYPTED, ENCRYPTED_AUTH, COMPRESSED, ENVELOPED, CERTS_ONLY}

    public enum Strict {NO, YES}

    /**
     * gets the s/mime type of the given part. If strict is true only 'complete' s/mime headers are
     * acceptable. ie. octet-stream is not accepted as valid s/mime.
     *
     * @param part
     * @param strict
     * @return
     * @throws MessagingException
     */
    public static Type getSMIMEContentType(@Nonnull Part part, Strict strict)
    throws MessagingException
    {
        return getSMIMEContentType(part.getContentType(), HeaderUtils.decodeTextQuietly(
                MimeUtils.getFilenameQuietly(part)), strict);
    }

    /**
     * gets the s/mime type of the given part. Strict checking is off so octet-stream is also accepted.
     *
     * @param part
     * @return
     * @throws MessagingException
     */
    public static Type getSMIMEContentType(@Nonnull Part part)
    throws MessagingException
    {
        return getSMIMEContentType(part, Strict.NO);
    }

    /**
     * gets the s/mime type of the given part. If strict is true only 'complete' s/mime headers are
     * acceptable. ie. octet-stream is not accepted as valid s/mime. The filename is the name of
     * the attachment which is being checked.
     *
     * @param contentTypeHeader
     * @param filename
     * @param strict
     * @return
     */
    public static Type getSMIMEContentType(@Nonnull String contentTypeHeader, String filename, Strict strict)
    {
        Type type = Type.NO_SMIME;

        try {
            ContentType contentType = new ContentType(contentTypeHeader);

            String primaryType = contentType.getPrimaryType();
            String subType  = contentType.getSubType();

            if ("multipart".equalsIgnoreCase(primaryType) &&
                    "signed".equalsIgnoreCase(subType))
            {
                // could be non-S/MIME signed so check the protocol
                String protocol = contentType.getParameter("protocol");

                if ("application/pkcs7-signature".equalsIgnoreCase(protocol) ||
                    "application/x-pkcs7-signature".equalsIgnoreCase(protocol))
                {
                    type = Type.CLEAR_SIGNED;
                }
                else {
                    // it is signed but the protocol is unknown
                    type = Type.UNKNOWN_CLEAR_SIGNED;
                }
            }
            else {
                if ("application".equalsIgnoreCase(primaryType))
                {
                    if (("pkcs7-mime".equalsIgnoreCase(subType) || "x-pkcs7-mime".equalsIgnoreCase(subType)))
                    {
                        type = checkSMIMETypeParameter(contentType, Type.NO_SMIME);

                        // no match on smime-type. Check filename
                        if (type == Type.NO_SMIME && strict == Strict.NO) {
                            type = checkFilename(contentType, filename);
                        }
                    }
                    else if ("pkcs7-signature".equalsIgnoreCase(subType) ||
                             "x-pkcs7-signature".equalsIgnoreCase(subType))
                    {
                        type = Type.CLEAR_SIGNED;
                    }
                    else if (strict == Strict.NO && "octet-stream".equalsIgnoreCase(subType))
                    {
                        // check file name. See rfc2633 3.8 Identifying an S/MIME Message
                        type = checkFilename(contentType, filename);

                        if (type != Type.NO_SMIME) {
                            // check if smime-type gives us more info
                            type = checkSMIMETypeParameter(contentType, type);
                        }
                    }
                }
            }
        }
        catch(ParseException e) {
            logger.debug("Parsing exception while parsing Content-Type.", e);
        }

        return type;
    }

    private static Type checkSMIMETypeParameter(@Nonnull ContentType contentType, Type defaultType)
    {
        String parameter = contentType.getParameter("smime-type");

        Type type = defaultType;

        if ("signed-data".equalsIgnoreCase(parameter)) {
            type = Type.OPAQUE_SIGNED;
        }
        else if ("enveloped-data".equalsIgnoreCase(parameter)) {
            type = Type.ENCRYPTED;
        }
        else if ("authEnveloped-data".equalsIgnoreCase(parameter)) {
            type = Type.ENCRYPTED_AUTH;
        }
        else if ("compressed-data".equalsIgnoreCase(parameter)) {
            type = Type.COMPRESSED;
        }
        else if ("certs-only".equalsIgnoreCase(parameter)) {
            type = Type.CERTS_ONLY;
        }

        return type;
    }

    private static Type checkFilename(@Nonnull ContentType contentType, String filename)
    {
        if (filename != null)
        {
            if (filename.toLowerCase().endsWith(".p7m"))
            {
                // might be signed or encrypted
                return Type.ENVELOPED;
            }
            if (filename.toLowerCase().endsWith(".p7z")) {
                return Type.COMPRESSED;
            }
            if (filename.toLowerCase().endsWith(".p7s")) {
                return Type.CLEAR_SIGNED;
            }
            if (filename.toLowerCase().endsWith(".p7c")) {
                return Type.CERTS_ONLY;
            }
        }

        String name = contentType.getParameter("name");

        if (name != null)
        {
            if (name.toLowerCase().endsWith(".p7m"))
            {
                // might be signed or encrypted
                return Type.ENVELOPED;
            }
            if (name.toLowerCase().endsWith(".p7z")) {
                return Type.COMPRESSED;
            }
            if (name.toLowerCase().endsWith(".p7s")) {
                return Type.CLEAR_SIGNED;
            }
            if (name.toLowerCase().endsWith(".p7c")) {
                return Type.CERTS_ONLY;
            }
        }

        return Type.NO_SMIME;
    }
}
