/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.PublicKeyInspector;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.sec.SECObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;

public class TLSKeyPairBuilderUtils
{
    private static final Logger logger = LoggerFactory.getLogger(TLSKeyPairBuilderUtils.class);

    private TLSKeyPairBuilderUtils() {
        // empty on purpose
    }

    public static TLSKeyPairBuilder.Algorithm getPublicKeyAlgorithm(@Nonnull PublicKey publicKey)
    {
        if (publicKey instanceof ECPublicKey ecPublicKey)
        {
            SubjectPublicKeyInfo spki = SubjectPublicKeyInfo.getInstance(ASN1Sequence.getInstance(
                    ecPublicKey.getEncoded()));

            AlgorithmIdentifier algid = spki.getAlgorithm();

            ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier) algid.getParameters();

            if (SECObjectIdentifiers.secp521r1.equals(oid)) {
                return TLSKeyPairBuilder.Algorithm.SECP521R1;
            }
            else if (SECObjectIdentifiers.secp384r1.equals(oid)) {
                return TLSKeyPairBuilder.Algorithm.SECP384R1;
            }
            else if (SECObjectIdentifiers.secp256r1.equals(oid)) {
                return TLSKeyPairBuilder.Algorithm.SECP256R1;
            }

            logger.warn("Unsupported EC curve {}", oid);
        }
        else if (publicKey instanceof RSAPublicKey rsaPublicKey)
        {
            int keyLength = PublicKeyInspector.getKeyLength(rsaPublicKey);

            if (keyLength == TLSKeyPairBuilder.Algorithm.RSA_2048.getKeySize()) {
                return TLSKeyPairBuilder.Algorithm.RSA_2048;
            }
            else if (keyLength == TLSKeyPairBuilder.Algorithm.RSA_3072.getKeySize()) {
                return TLSKeyPairBuilder.Algorithm.RSA_3072;
            }
            else if (keyLength == TLSKeyPairBuilder.Algorithm.RSA_4096.getKeySize()) {
                return TLSKeyPairBuilder.Algorithm.RSA_4096;
            }

            logger.warn("Unsupported RSA key length {}", keyLength);
        }
        else {
            logger.warn("Unsupported public key algorithm {}", publicKey.getAlgorithm());
        }

        return null;
    }
}
