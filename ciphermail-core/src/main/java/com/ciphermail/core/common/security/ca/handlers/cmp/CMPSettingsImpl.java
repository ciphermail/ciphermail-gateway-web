/*
 * Copyright (c) 2021-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca.handlers.cmp;

import com.ciphermail.core.app.properties.CAHandlersCategory;
import com.ciphermail.core.app.properties.UserPropertiesType;
import com.ciphermail.core.app.properties.UserProperty;
import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

import javax.annotation.Nonnull;

@UserPropertiesType(name = "cmp", displayName = "CMP", parent = CAHandlersCategory.NAME, order = 10)
public class CMPSettingsImpl extends DelegatedHierarchicalProperties implements CMPSettings
{
    /*
     * Property names
     */
    private static final String ISSUER_DN = "request-handler-cmp-issuer-dn";
    private static final String SENDER_KID = "request-handler-cmp.sender-kid";
    private static final String MESSAGE_PROTECTION_PASSWORD = "request-handler-cmp-message-protection-password";
    private static final String MESSAGE_PROTECTION_DIGEST_ALGORITHM = "request-handler-cmp-message-protection-digest-algorithm";
    private static final String MESSAGE_PROTECTION_HMAC_ALGORITHM = "request-handler-cmp-message-protection-hmac-algorithm";
    private static final String CMP_SERVICE_URL = "request-handler-cmp-service-url";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public CMPSettingsImpl(@Nonnull HierarchicalProperties delegatedProperties) {
        super(delegatedProperties);
    }

    public static CMPSettingsImpl getInstance(@Nonnull HierarchicalProperties delegatedProperties) {
        return new CMPSettingsImpl(delegatedProperties);
    }

	@Override
    @UserProperty(name = ISSUER_DN,
            user = false, domain = false
    )
	public void setIssuerDN(String dn)
	{
        try {
            setProperty(ISSUER_DN, dn);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = ISSUER_DN,
            user = false, domain = false,
            order = 10,
            allowNull = true
    )
	public String getIssuerDN()
	{
        try {
            return getProperty(ISSUER_DN);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = SENDER_KID, user = false, domain = false)
	public void setSenderKID(String senderKID)
	{
        try {
            setProperty(SENDER_KID, senderKID);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = SENDER_KID,
            user = false, domain = false,
            order = 20,
            allowNull = true
    )
	public String getSenderKID()
	{
        try {
            return getProperty(SENDER_KID);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_PASSWORD,
            user = false, domain = false
    )
	public void setMessageProtectionPassword(String password)
	{
        try {
            setProperty(MESSAGE_PROTECTION_PASSWORD, password);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_PASSWORD,
            user = false, domain = false,
            order = 30,
            allowNull = true
    )
	public String getMessageProtectionPassword()
	{
        try {
            return getProperty(MESSAGE_PROTECTION_PASSWORD);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_DIGEST_ALGORITHM,
            user = false, domain = false
    )
	public void setMessageProtectionDigestAlgorithm(String oid)
	{
        try {
            setProperty(MESSAGE_PROTECTION_DIGEST_ALGORITHM, oid);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_DIGEST_ALGORITHM,
            user = false, domain = false,
            order = 40,
            allowNull = true
    )
	public String getMessageProtectionDigestAlgorithm()
	{
        try {
            String algorithm = StringUtils.trimToNull(getProperty(MESSAGE_PROTECTION_DIGEST_ALGORITHM));

            if (algorithm == null) {
                algorithm = NISTObjectIdentifiers.id_sha256.getId();
            }

            return algorithm;
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_HMAC_ALGORITHM,
            user = false, domain = false
    )
	public void setMessageProtectionHMACAlgorithm(String oid)
	{
        try {
            setProperty(MESSAGE_PROTECTION_HMAC_ALGORITHM, oid);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = MESSAGE_PROTECTION_HMAC_ALGORITHM,
            user = false, domain = false,
            order = 50,
            allowNull = true
    )
	public String getMessageProtectionHMACAlgorithm()
	{
        try {
            String algorithm = StringUtils.trimToNull(getProperty(MESSAGE_PROTECTION_HMAC_ALGORITHM));

            if (algorithm == null) {
                algorithm = PKCSObjectIdentifiers.id_hmacWithSHA256.getId();
            }

            return algorithm;
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = CMP_SERVICE_URL,
            user = false, domain = false
    )
	public void setCMPServiceURL(String url)
	{
        try {
            setProperty(CMP_SERVICE_URL, url);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}

	@Override
    @UserProperty(name = CMP_SERVICE_URL,
            user = false, domain = false,
            order = 60,
            allowNull = true
    )
	public String getCMPServiceURL()
	{
        try {
            return getProperty(CMP_SERVICE_URL);
        }
        catch (HierarchicalPropertiesException e) {
            throw new IllegalArgumentException(e);
        }
	}
}
