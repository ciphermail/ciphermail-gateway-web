/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

/**
 * The support OpenPGP key flags (see RFC 4880 section 5.2.3.21 Key Flags)
 *
 * @author Martijn Brinkers
 *
 */
public enum PGPKeyFlags
{
    CERTIFY_OTHER   (0x01, "Certify other"),
    SIGN_DATA       (0x02, "Sign data"),
    ENCRYPT_COMMS   (0x04, "Encrypt communications"),
    ENCRYPT_STORAGE (0x08, "Encrypt storage"),
    SPLIT           (0x10, "split"),
    AUTHENTICATION  (0x20, "Authentication"),
    SHARED          (0x80, "Shared");

    /*
     * The tag value
     */
    private final int tag;

    /*
     * The friendly name
     */
    private final String friendlyName;

    PGPKeyFlags(int tag, String friendlyName)
    {
        this.tag = tag;
        this.friendlyName = friendlyName;
    }

    public int getTag() {
        return tag;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public static PGPKeyFlags fromTag(int tag)
    {
        for (PGPKeyFlags flag : PGPKeyFlags.values())
        {
            if (tag == flag.tag) {
                return flag;
            }
        }

        return null;
    }

    public static int getMask(PGPKeyFlags... flags)
    {
        int mask = 0;

        if (flags != null)
        {
            for (PGPKeyFlags flag : flags) {
                mask = mask | flag.getTag();
            }
        }

        return mask;
    }
}
