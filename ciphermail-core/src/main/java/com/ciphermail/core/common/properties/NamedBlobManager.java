/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * NamedBlobManager can be used to add, delete and search for NamedBlob's.
 *
 * @author Martijn Brinkers
 *
 */
public interface NamedBlobManager
{
    /**
     * Creates and adds a new NamedBlob instance. The combination category/name should be unique.
     */
    @Nonnull NamedBlob createNamedBlob(@Nonnull String category, @Nonnull String name);

    /**
     * Returns the named blob from the given category. Null if there is no such element.
     */
    NamedBlob getNamedBlob(@Nonnull String category, @Nonnull String name);

    /**
     * Removes the named blob from the given category.
     */
    void deleteNamedBlob(@Nonnull String category, @Nonnull String name);

    /**
     * Deletes ALL named blobs
     */
    void deleteAll();

    /**
     * Returns an immutable List of all named blobs from the given category
     */
    List<? extends NamedBlob> getByCategory(@Nonnull String category, Integer firstResult, Integer maxResults);

    /**
     * Returns the number of named blobs of the given category
     */
    long getByCategoryCount(@Nonnull String category);

    /**
     * A NamedBlob can reference other NamedBlob's. This method returns the number
     * of times the NamedBlob is referenced by other NamedBlob's.
     * <p>
     * Note: this only returns the number of NamedBlob's that references the provided NamedBlob. A NamedBlob
     * can also be referenced by other classes/entities.
     */
    long getReferencedByCount(@Nonnull NamedBlob namedBlob);

    /**
     * A NamedBlob can reference other NamedBlob's. This method returns all the NamedBlob's
     * that references the NamedBlob.
     *
     * Note: this only returns the NamedBlob's that references the provided NamedBlob. A NamedBlob
     * can also be referenced by other classes/entities.
     */
    List<? extends NamedBlob> getReferencedBy(@Nonnull NamedBlob namedBlob, Integer firstResult,  Integer maxResults);
}
