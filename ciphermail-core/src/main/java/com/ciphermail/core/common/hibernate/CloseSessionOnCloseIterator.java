/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;

/**
 * Wraps a {@link CloseableIterator} to make sure that the session is closed when the iterator is closed.
 *
 * @author Martijn Brinkers
 *
 * @param <T>
 */
public class CloseSessionOnCloseIterator<T> implements CloseableIterator<T>
{
    private static final Logger logger = LoggerFactory.getLogger(CloseSessionOnCloseIterator.class);

    private final CloseableIterator<T> delegateIterator;
    private final SessionAdapter session;

    private boolean closed;

    public CloseSessionOnCloseIterator(CloseableIterator<T> delegateIterator, SessionAdapter session)
    {
        this.delegateIterator = delegateIterator;
        this.session = session;
    }

    @Override
    public boolean hasNext()
    throws CloseableIteratorException
    {
        boolean hasNext = delegateIterator.hasNext();

        /*
         * childIterator.next() can result in closing of the child iterator. If that happens we also
         * need to close this iterator.
         */
        if (delegateIterator.isClosed()) {
            this.close();
        }

        return hasNext;
    }

    @Override
    public T next()
    throws CloseableIteratorException
    {
        T next = delegateIterator.next();

        /*
         * childIterator.next() can result in closing of the child iterator. If that happens we also
         * need to close this iterator.
         */
        if (delegateIterator.isClosed()) {
            this.close();
        }

        return next;
    }

    @Override
    public void close()
    throws CloseableIteratorException
    {
        if (!closed)
        {
            logger.debug("Closing iterator.");

            closed = true;

            try {
                delegateIterator.close();
            }
            catch(Exception e)
            {
                if (!(e instanceof CloseableIteratorException)) {
                    throw new CloseableIteratorException(e);
                }
                else {
                    throw (CloseableIteratorException) e;
                }
            }
            finally {
                session.close();
            }
        }
    }

    @Override
    public boolean isClosed()
    throws CloseableIteratorException
    {
        boolean childClosed = delegateIterator.isClosed();

        if (childClosed && !closed)
        {
            logger.warn("Child is closed while this iterator is not closed. Closing this iterator.");
            close();
        }

        return childClosed;
    }
}
