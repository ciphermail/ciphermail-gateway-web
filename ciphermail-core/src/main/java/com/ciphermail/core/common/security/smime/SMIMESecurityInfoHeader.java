/*
 * Copyright (c) 2008-2018, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.MailConstants;

/**
 * Headers added to a message if the message was S/MIME and handled by the S/MIME handler
 *
 * @author Martijn Brinkers
 *
 */
public class SMIMESecurityInfoHeader
{
    public static final String BASE = MailConstants.CIPHERMAIL_HEADER_PREFIX + "-Info-";

    public static final String SIGNER_ID                            = BASE + "Signer-ID-";
    public static final String SIGNER_EMAIL_ADDRESSES               = BASE + "Signer-Email-";
    public static final String SIGNER_VERIFIED                      = BASE + "Signer-Verified-";
    public static final String SIGNER_VERIFICATION_INFO             = BASE + "Signer-Verification-Info-";
    public static final String SIGNER_TRUSTED                       = BASE + "Signer-Trusted-";
    public static final String SIGNER_TRUSTED_INFO                  = BASE + "Signer-Trusted-Info-";

    public static final String ENCRYPTION_ALGORITHM                 = BASE + "Encryption-Algorithm";
    public static final String ENCRYPTION_RECIPIENT                 = BASE + "Encryption-Recipient-";

    public static final String SMIME_ENCRYPTED                      = BASE + "SMIME-Encrypted";
    public static final String SMIME_SIGNED                         = BASE + "SMIME-Signed";
    public static final String SMIME_DECRYPTION_KEY_NOT_FOUND       = BASE + "SMIME-Decryption-Key-Not-Found";
    public static final String SMIME_DECRYPTION_ILLEGAL_CHARS_FOUND = BASE + "SMIME-Illegal-Chars-Found";


    public static final String COMPRESSED                           = BASE + "Compressed";
}
