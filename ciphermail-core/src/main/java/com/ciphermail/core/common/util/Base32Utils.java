/*
 * Copyright (c) 2016, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang.StringUtils;

/**
 * Base32 helper class
 *
 * @author Martijn Brinkers
 *
 */
public class Base32Utils
{
    private static final Base32 base32 = new Base32();

    private Base32Utils() {
        // empty on purpose
    }

    /**
     * Returns true if the input is base32 encoded.
     */
    public static boolean isBase32(String input)
    {
        if (StringUtils.isEmpty(input)) {
            return false;
        }

        return input.matches("[a-zA-Z2-7]*");
    }

    /**
     * Base32 encodes the input. The base32 encoded string is converted to lowercase and any padding is stripped to
     * allow the base32 encoded string to be used as a password.
     */
    public static String base32Encode(byte[] input) {
        return StringUtils.lowerCase(StringUtils.stripEnd(base32.encodeAsString(input), "="));
    }

    /**
     * Base32 decodes the input.
     */
    public static byte[] base32Decode(String base32Input) {
        return base32.decode(base32Input);
    }
}
