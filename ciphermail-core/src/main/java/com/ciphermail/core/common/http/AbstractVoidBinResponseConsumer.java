/*
 * Copyright (c) 2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.http;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.hc.client5.http.async.methods.AbstractBinResponseConsumer;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.ProtocolVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Base implementation of AbstractBinResponseConsumer
 *
 */
public abstract class AbstractVoidBinResponseConsumer extends AbstractBinResponseConsumer<Void>
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractVoidBinResponseConsumer.class);

    /*
     * The HTTP status code
     */
    private final AtomicInteger httpStatusCode = new AtomicInteger();

    /*
     * The HTTP status line
     */
    private final AtomicReference<String> httpStatusReasonPhrase = new AtomicReference<>();

    /*
     * The HTTP version of the connection
     */
    private final AtomicReference<ProtocolVersion> protocolVersion = new AtomicReference<>();

    @Override
    public void releaseResources() {
        logger.debug("Release resources");
    }

    @Override
    protected void start(HttpResponse httpResponse, ContentType contentType)
    {
        logger.debug("HttpResponse: {}", httpResponse);

        httpStatusCode.set(httpResponse.getCode());
        httpStatusReasonPhrase.set(httpResponse.getReasonPhrase());
        protocolVersion.set(httpResponse.getVersion());
    }

    @Override
    protected Void buildResult()
    {
        logger.debug("Build result");

        return null;
    }

    @Override
    protected int capacityIncrement() {
        return Integer.MAX_VALUE;
    }

    public int getHttpStatusCode() {
        return httpStatusCode.get();
    }

    public String getHttpStatusReasonPhrase() {
        return httpStatusReasonPhrase.get();
    }

    public ProtocolVersion getProtocolVersion() {
        return protocolVersion.get();
    }
}
