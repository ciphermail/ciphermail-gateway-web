/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp.trustlist;

import com.ciphermail.core.common.util.SizeUtils;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Entity(name = PGPTrustListEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={PGPTrustListEntity.NAME_COLUMN,
        PGPTrustListEntity.FINGERPRINT_COLUMN})}
)
public class PGPTrustListEntity implements PGPTrustListEntry
{
    static final String ENTITY_NAME = "PgpTrustList";

    /*
     * Property names of properties stored in nameValues
     */
    private static final String STATUS_PROPERTY_NAME = "status";

    /**
     * Name of columns which are referenced from other classes
     */
    public static final String NAME_COLUMN = "name";
    public static final String FINGERPRINT_COLUMN = "fingerprint";

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    @Column (name = NAME_COLUMN, length = 255, unique = false, nullable = true)
    private String name;

    @Column (name = FINGERPRINT_COLUMN, length = 255, unique = false, nullable = false)
    private String fingerprint;

    /*
     * We will store some properties as name/value pairs. The advantage
     * is that we do not need to change the database structure when
     * we want to add new properties. Although this probably is not
     * "how it should be done" with a relational database I prefer this
     * approach because database changes are a pain.
     */
    @ElementCollection
    @Column(name = "value", length = SizeUtils.KB * 32)
    @MapKeyColumn(name = "name")
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Map<String, String> nameValues = new HashMap<>();

    protected PGPTrustListEntity() {
        // Hibernate requires a default constructor
    }

    public PGPTrustListEntity(@Nonnull String fingerprint) {
        this.fingerprint = Objects.requireNonNull(fingerprint);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public @Nonnull String getSHA256Fingerprint() {
        return fingerprint;
    }

    @Override
    public @Nonnull PGPTrustListStatus getStatus()
    {
        PGPTrustListStatus status = PGPTrustListStatus.fromName(nameValues.get(STATUS_PROPERTY_NAME));

        return status != null ? status : PGPTrustListStatus.UNTRUSTED;
    }

    @Override
    public void setStatus(PGPTrustListStatus status) {
        nameValues.put(STATUS_PROPERTY_NAME, ObjectUtils.toString(status, null));
    }

    public Map<String, String> getProperties() {
        return nameValues;
    }

    /**
     * Creates a clone of this instance with fingerprint set to newFingerprint. Note: the id is note cloned
     */
    public PGPTrustListEntity clone(@Nonnull String newFingerprint)
    {
        PGPTrustListEntity clone = new PGPTrustListEntity(newFingerprint);

        clone.setName(name);
        clone.nameValues.putAll(nameValues);

        return clone;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PGPTrustListEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, rhs.name)
            .append(fingerprint, rhs.fingerprint)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .append(fingerprint)
            .toHashCode();
    }
}
