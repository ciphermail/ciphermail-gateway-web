/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.smime;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailSession;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.ProtectedContentHeaderNameMatcher;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.bouncycastle.X509CertificateHolderStore;
import com.ciphermail.core.common.security.certificate.X509ExtensionInspector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.DefaultSignedAttributeTableGenerator;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.SignerInfoGeneratorBuilder;
import org.bouncycastle.cms.SimpleAttributeTableGenerator;
import org.bouncycastle.cms.jcajce.JceCMSContentEncryptorBuilder;
import org.bouncycastle.cms.jcajce.JceKEKRecipientInfoGenerator;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientInfoGenerator;
import org.bouncycastle.cms.jcajce.ZlibCompressor;
import org.bouncycastle.mail.smime.SMIMECompressedGenerator;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
import org.bouncycastle.mail.smime.SMIMEException;
import org.bouncycastle.mail.smime.SMIMESignedGenerator;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaAlgorithmParametersConverter;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;

import javax.annotation.Nonnull;
import javax.crypto.SecretKey;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.spec.MGF1ParameterSpec;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 *
 * This class is not thread safe.
 *
 * @author Martijn Brinkers
 *
 */
public class SMIMEBuilderImpl implements SMIMEBuilder
{
    /*
     * The source mime message
     */
    private final MimeMessage sourceMessage;

    /*
     * header name matcher uses to copy header names from source part to destination part
     */
    private final HeaderMatcher protectedContentMatcher;

    /*
     * contains the active body part
     */
    private MimeBodyPart bodyPart;

    /*
     * generator for encrypting messages
     */
    private SMIMEEnvelopedGenerator envelopedGenerator;

    /*
     * generator for signing of messages
     */
    private SMIMESignedGenerator signedGenerator;

    /*
     * Generator for compression of messages
     */
    private SMIMECompressedGenerator compressedGenerator;

    /*
     * If true the old x-pkcs7-* content types will be used
     */
    private boolean useDeprecatedContentTypes;

    /*
     * The micalg version to use for S/MIME signed messages
     */
    private MicAlgStyle micAlgStyle = MicAlgStyle.RFC5751;

    /*
     * The provider for non sensitive operations
     */
    private String nonSensitiveProvider;

    /*
     * The provider for sensitive operations
     */
    private String sensitiveProvider;

    /*
     * If true and the message contains 8bit parts, the message will be converted
     * to 7bit prior to signing.
     */
    private boolean convertTo7Bit = true;

    public SMIMEBuilderImpl(@Nonnull MimeMessage message)
    throws MessagingException, IOException
    {
        this(message, ProtectedContentHeaderNameMatcher.DEFAULT_PROTECTED_CONTENT_HEADERS);
    }

    public SMIMEBuilderImpl(@Nonnull MimeMessage message, String... protectedHeaders)
    throws MessagingException, IOException
    {
        this.protectedContentMatcher = new ProtectedContentHeaderNameMatcher(protectedHeaders);

        this.sourceMessage = Objects.requireNonNull(message);

        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        this.nonSensitiveProvider = securityFactory.getNonSensitiveProvider();
        this.sensitiveProvider = securityFactory.getSensitiveProvider();

        this.bodyPart = BodyPartUtils.makeContentBodyPart(message, protectedContentMatcher);
    }

    @Override
    public void addCertificates(Collection<X509Certificate> certificates)
    throws SMIMEBuilderException
    {
        if (certificates == null) {
            return;
        }

        getSignedGenerator().addCertificates(new X509CertificateHolderStore(certificates));
    }

    @Override
    public void addCertificates(X509Certificate... certificates)
    throws SMIMEBuilderException
    {
        if (certificates == null) {
            return;
        }

        addCertificates(Arrays.asList(certificates));
    }

    @Override
    public void addRecipient(@Nonnull X509Certificate certificate, @Nonnull SMIMERecipientMode mode)
    throws SMIMEBuilderException
    {
        addRecipient(certificate, mode, SMIMEEncryptionScheme.RSAES_PKCS1_V1_5);
    }

    private String getOAEPDIgestAlgo(@Nonnull SMIMEEncryptionScheme scheme)
    {
        return switch (scheme) {
            case RSAES_OAEP_SHA1   -> "SHA1";
            case RSAES_OAEP_SHA256 -> "SHA-256";
            case RSAES_OAEP_SHA384 -> "SHA-384";
            case RSAES_OAEP_SHA512 -> "SHA-512";
            default -> throw new IllegalArgumentException(scheme + " is not a OAEP scheme");
        };
    }

    @Override
    public void addRecipient(@Nonnull X509Certificate certificate, @Nonnull SMIMERecipientMode mode,
            @Nonnull SMIMEEncryptionScheme scheme)
    throws SMIMEBuilderException
    {
        try {
            boolean rsAesOAEP = (scheme == SMIMEEncryptionScheme.RSAES_OAEP_SHA1) ||
                    (scheme == SMIMEEncryptionScheme.RSAES_OAEP_SHA256) ||
                    (scheme == SMIMEEncryptionScheme.RSAES_OAEP_SHA384) ||
                    (scheme == SMIMEEncryptionScheme.RSAES_OAEP_SHA512);

            AlgorithmIdentifier algorithmIdentifier = null;

            if (rsAesOAEP)
            {
                JcaAlgorithmParametersConverter paramsConverter = new JcaAlgorithmParametersConverter();

                String digestAlgo = getOAEPDIgestAlgo(scheme);

                OAEPParameterSpec oaepSpec = new OAEPParameterSpec(digestAlgo, "MGF1",
                        new MGF1ParameterSpec(digestAlgo), PSource.PSpecified.DEFAULT);

                algorithmIdentifier = paramsConverter.getAlgorithmIdentifier(PKCSObjectIdentifiers.id_RSAES_OAEP,
                        oaepSpec);
            }

            byte[] subjectKeyIdentifier = X509ExtensionInspector.getSubjectKeyIdentifier(certificate);

            if ((mode == SMIMERecipientMode.SUBJECT_KEY_ID_IF_AVAILABLE || mode == SMIMERecipientMode.BOTH) &&
                    subjectKeyIdentifier != null)
            {
                PublicKey publicKey = certificate.getPublicKey();

                JceKeyTransRecipientInfoGenerator recipientInfoGenerator  = algorithmIdentifier != null ?
                    new JceKeyTransRecipientInfoGenerator(subjectKeyIdentifier, algorithmIdentifier, publicKey) :
                    new JceKeyTransRecipientInfoGenerator(subjectKeyIdentifier, publicKey);

                recipientInfoGenerator.setProvider(nonSensitiveProvider);

                getEnvelopedGenerator().addRecipientInfoGenerator(recipientInfoGenerator);
            }

            if (mode == SMIMERecipientMode.ISSUER_SERIAL || mode == SMIMERecipientMode.BOTH ||
                    subjectKeyIdentifier == null)
            {
                JceKeyTransRecipientInfoGenerator recipientInfoGenerator = algorithmIdentifier != null ?
                    new JceKeyTransRecipientInfoGenerator(certificate, algorithmIdentifier) :
                    new JceKeyTransRecipientInfoGenerator(certificate);

                recipientInfoGenerator.setProvider(nonSensitiveProvider);

                getEnvelopedGenerator().addRecipientInfoGenerator(recipientInfoGenerator);
            }
        }
        catch (CertificateEncodingException | InvalidAlgorithmParameterException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    @Override
    public void addRecipient(@Nonnull SecretKey secretKey, @Nonnull byte[] keyIdentifier)
    {
        JceKEKRecipientInfoGenerator recipientInfoGenerator = new JceKEKRecipientInfoGenerator(
                keyIdentifier, secretKey);

        getEnvelopedGenerator().addRecipientInfoGenerator(recipientInfoGenerator);
    }

    private void addSigner(@Nonnull PrivateKey privateKey, @Nonnull X509Certificate signer,
            @Nonnull SMIMESigningAlgorithm algorithm, AttributeTable signedAttr, AttributeTable unsignedAttr)
    throws SMIMEBuilderException
    {
        try {
            JcaDigestCalculatorProviderBuilder digestBuilder = new JcaDigestCalculatorProviderBuilder();

            digestBuilder.setProvider(nonSensitiveProvider);

            SignerInfoGeneratorBuilder signerInfoBuilder = new SignerInfoGeneratorBuilder(digestBuilder.build());

            if (signedAttr != null) {
                signerInfoBuilder.setSignedAttributeGenerator(new DefaultSignedAttributeTableGenerator(signedAttr));
            }

            if (unsignedAttr != null) {
                signerInfoBuilder.setUnsignedAttributeGenerator(new SimpleAttributeTableGenerator(unsignedAttr));
            }

            JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(algorithm.getAlgorithm());

            contentSignerBuilder.setProvider(sensitiveProvider);

            SignerInfoGenerator signerInfoGenerator = signerInfoBuilder.build(contentSignerBuilder.build(privateKey),
                    new JcaX509CertificateHolder(signer));

            getSignedGenerator().addSignerInfoGenerator(signerInfoGenerator);
        }
        catch (OperatorCreationException | CertificateEncodingException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    @Override
    public void addSigner(@Nonnull PrivateKey privateKey, @Nonnull X509Certificate signer,
            @Nonnull SMIMESigningAlgorithm algorithm)
    throws SMIMEBuilderException
    {
        addSigner(privateKey, signer, algorithm, SMIMEAttributeUtils.getDefaultSignedAttributes(), null);
    }

    @Override
    public void addSigner(@Nonnull PrivateKey privateKey, @Nonnull X509Certificate signer, @Nonnull
    SMIMESigningAlgorithm algorithm, X509Certificate encryptionKeyPreference)
    throws SMIMEBuilderException
    {
        addSigner(privateKey, signer, algorithm, SMIMEAttributeUtils.getDefaultSignedAttributes(
                encryptionKeyPreference), null);
    }

    public void addSigner(@Nonnull PrivateKey privateKey, @Nonnull byte[] subjectKeyIdentifier,
            @Nonnull SMIMESigningAlgorithm algorithm, AttributeTable signedAttr, AttributeTable unsignedAttr)
    throws SMIMEBuilderException
    {
        try {
            JcaDigestCalculatorProviderBuilder digestBuilder = new JcaDigestCalculatorProviderBuilder();

            digestBuilder.setProvider(nonSensitiveProvider);

            SignerInfoGeneratorBuilder signerInfoBuilder = new SignerInfoGeneratorBuilder(digestBuilder.build());

            if (signedAttr != null) {
                signerInfoBuilder.setSignedAttributeGenerator(new DefaultSignedAttributeTableGenerator(signedAttr));
            }

            if (unsignedAttr != null) {
                signerInfoBuilder.setUnsignedAttributeGenerator(new SimpleAttributeTableGenerator(unsignedAttr));
            }

            JcaContentSignerBuilder contentSignerBuilder = new JcaContentSignerBuilder(algorithm.getAlgorithm());

            contentSignerBuilder.setProvider(sensitiveProvider);

            SignerInfoGenerator signerInfoGenerator = signerInfoBuilder.build(contentSignerBuilder.build(privateKey),
                    subjectKeyIdentifier);

            getSignedGenerator().addSignerInfoGenerator(signerInfoGenerator);
        }
        catch (OperatorCreationException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    @Override
    public void addSigner(@Nonnull PrivateKey privateKey, @Nonnull byte[] subjectKeyIdentifier,
            @Nonnull SMIMESigningAlgorithm algorithm)
    throws SMIMEBuilderException
    {
        addSigner(privateKey, subjectKeyIdentifier, algorithm, SMIMEAttributeUtils.getDefaultSignedAttributes(), null);
    }

    @Override
    public MimeMessage buildMessage()
    throws SMIMEBuilderException, MessagingException
    {
        try {
            MimeMessage newMessage = new MimeMessage(MailSession.getDefaultSession());

            newMessage.setContent(bodyPart.getContent(), bodyPart.getContentType());

            HeaderMatcher contentMatcher = new ContentHeaderNameMatcher();

            // copy all content headers from body to new message
            HeaderUtils.copyHeaders(bodyPart, newMessage, contentMatcher);

            // create a matcher that matches on everything expect content-*
            HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(contentMatcher);

            // copy all non-content headers from source message to the new message
            HeaderUtils.copyHeaders(sourceMessage, newMessage, nonContentMatcher);

            newMessage.saveChanges();

            return newMessage;
        }
        catch (IOException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    @Override
    public void compress()
    throws SMIMEBuilderException
    {
        try {
            bodyPart = getCompressedGenerator().generate(bodyPart, new ZlibCompressor());

            // use the deprecated content-type if required. We do this by changing the content-type
            // header. I wish I could specify the content-type for the envelopedGenerator but that's not
            // possible without completely re-implementing envelopedGenerator. This is a workaround until
            // BC allows me to set the content-type
            if (useDeprecatedContentTypes) {
                bodyPart.setHeader("Content-Type", SMIMEHeader.DEPRECATED_COMPRESSED_CONTENT_TYPE);
            }

            // Reset compressedGenerator. A new instance will be created if needed.
            compressedGenerator = null;
        }
        catch (SMIMEException | MessagingException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    protected void validateSMIMEEncryptionAlgorithm(@Nonnull SMIMEEncryptionAlgorithm algorithm)
    {
        if (algorithm.isAuth()) {
            throw new IllegalArgumentException(algorithm + " requires an SMIMEBuilder with Auth support");
        }
    }

    @Override
    public void encrypt(@Nonnull SMIMEEncryptionAlgorithm algorithm, int keySize)
    throws SMIMEBuilderException
    {
        validateSMIMEEncryptionAlgorithm(algorithm);

        try {
            JceCMSContentEncryptorBuilder encryptorBuilder = new JceCMSContentEncryptorBuilder(algorithm.getOID(),
                    keySize);

            encryptorBuilder.setProvider(nonSensitiveProvider);

            bodyPart = getEnvelopedGenerator().generate(bodyPart, encryptorBuilder.build());

            // use the deprecated content-type if required. We do this by changing the content-type
            // header. I wish I could specify the content-type for the envelopedGenerator but that's not
            // possible without completely re-implementing envelopedGenerator. This is a workaround until
            // BC allows me to set the content-type
            if (useDeprecatedContentTypes) {
                bodyPart.setHeader("Content-Type", SMIMEHeader.DEPRECATED_ENCRYPTED_CONTENT_TYPE);
            }

            // Reset envelopedGenerator. A new instance will be created if needed.
            envelopedGenerator = null;
        }
        catch (SMIMEException | MessagingException | CMSException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    @Override
    public void encrypt(@Nonnull SMIMEEncryptionAlgorithm algorithm)
    throws SMIMEBuilderException
    {
        encrypt(algorithm, algorithm.defaultKeySize());
    }

    @Override
    public void sign(@Nonnull SMIMESignMode signMode)
    throws SMIMEBuilderException
    {
        try {
            if (convertTo7Bit) {
                MailUtils.convertTo7Bit(bodyPart);
            }

            if (signMode == SMIMESignMode.CLEAR)
            {
                Multipart signed = getSignedGenerator().generate(bodyPart);

                // use the deprecated content-type if required. We do this by changing the content-type
                // header. I wish I could specify the content-type for the signedGenerator but that's not
                // possible without completely re-implementing signedGenerator. This is a workaround until
                // BC allows me to set the content-type
                if (useDeprecatedContentTypes)
                {
                    // I don't know how to change the protocol content-type parameter of the Multipart
                    // so we will only change the signature part content type
                    BodyPart[] parts = SMIMEUtils.dissectSigned(signed);

                    if (parts != null)
                    {
                        BodyPart signaturePart = parts[1];

                        signaturePart.setHeader("Content-Type", SMIMEHeader.DEPRECATED_DETACHED_SIGNATURE_TYPE);
                    }
                }

                MimeMessage signedMessage = new MimeMessage(MailSession.getDefaultSession());

                signedMessage.setContent(signed);
                signedMessage.saveChanges();

                bodyPart = BodyPartUtils.makeContentBodyPart(signedMessage, protectedContentMatcher);
            }
            else if (signMode == SMIMESignMode.OPAQUE)
            {
                bodyPart = getSignedGenerator().generateEncapsulated(bodyPart);

                // use the deprecated content-type if required. We do this by changing the content-type
                // header. I wish I could specify the content-type for the signedGenerator but that's not
                // possible without completely re-implementing signedGenerator. This is a workaround until
                // BC allows me to set the content-type
                if (useDeprecatedContentTypes) {
                    bodyPart.setHeader("Content-Type", SMIMEHeader.DEPRECATED_ENCAPSULATED_SIGNED_CONTENT_TYPE);
                }
            }
            else {
                throw new IllegalArgumentException("Unknown signMode.");
            }

            // Reset signedGenerator. A new instance will be created if needed.
            this.signedGenerator = null;
        }
        catch (SMIMEException | MessagingException | IOException e) {
            throw new SMIMEBuilderException(e);
        }
    }

    protected SMIMEEnvelopedGenerator getEnvelopedGenerator()
    {
        if (envelopedGenerator == null) {
            envelopedGenerator = new PrivateSMIMEEnvelopedGenerator();
        }

        return envelopedGenerator;
    }

    private SMIMESignedGenerator getSignedGenerator()
    {
        if (signedGenerator == null) {
            signedGenerator = new PrivateSMIMESignedGenerator(getMicAlgs());
        }

        return signedGenerator;
    }

    private SMIMECompressedGenerator getCompressedGenerator()
    {
        if (compressedGenerator == null) {
            compressedGenerator = new PrivateSMIMECompressedGenerator();
        }

        return compressedGenerator;
    }

    private Map<?, ?> getMicAlgs()
    {
        if (micAlgStyle != null)
        {
            return switch (micAlgStyle) {
                case RFC3851 -> SMIMESignedGenerator.RFC3851_MICALGS;
                case RFC5751 -> SMIMESignedGenerator.RFC5751_MICALGS;
            };
        }

        return SMIMESignedGenerator.RFC5751_MICALGS;
    }

    /*
     * Create our own private SMIMEEnvelopedGenerator so we can use our own makeContentBodyPart
     */
    private static class PrivateSMIMEEnvelopedGenerator extends SMIMEEnvelopedGenerator
    {
        @Override
        protected MimeBodyPart makeContentBodyPart(MimeBodyPart content)
        {
            // we do not need to do any encoding because we already did that
            return content;
        }
    }

    /*
     * Create our own private SMIMESignedGenerator so we can use our own makeContentBodyPart
     */
    private static class PrivateSMIMESignedGenerator extends SMIMESignedGenerator
    {
        PrivateSMIMESignedGenerator(Map<?, ?> micAlgs) {
            super(micAlgs);
        }

        @Override
        protected MimeBodyPart makeContentBodyPart(MimeBodyPart content)
        {
            // we do not need to do any encoding because we already did that
            return content;
        }
    }

    /*
     * Create our own private SMIMECompressedGenerator so we can use our own makeContentBodyPart
     */
    private static class PrivateSMIMECompressedGenerator extends SMIMECompressedGenerator
    {
        @Override
        protected MimeBodyPart makeContentBodyPart(MimeBodyPart content)
        {
            // we do not need to do any encoding because we already did that
            return content;
        }
    }

    @Override
    public void setUseDeprecatedContentTypes(boolean useDeprecatedContentTypes) {
        this.useDeprecatedContentTypes = useDeprecatedContentTypes;
    }

    @Override
    public boolean isUseDeprecatedContentTypes() {
        return useDeprecatedContentTypes;
    }

    @Override
    public void setMicAlgStyle(MicAlgStyle micAlgStyle) {
        this.micAlgStyle = micAlgStyle;
    }

    @Override
    public MicAlgStyle getMicAlgStyle() {
        return micAlgStyle;
    }

    public String getNonSensitiveProvider() {
        return nonSensitiveProvider;
    }

    public void setNonSensitiveProvider(String nonSensitiveProvider) {
        this.nonSensitiveProvider = nonSensitiveProvider;
    }

    public String getSensitiveProvider() {
        return sensitiveProvider;
    }

    public void setSensitiveProvider(String sensitiveProvider) {
        this.sensitiveProvider = sensitiveProvider;
    }

    public boolean isConvertTo7Bit() {
        return convertTo7Bit;
    }

    public void setConvertTo7Bit(boolean convertTo7Bit) {
        this.convertTo7Bit = convertTo7Bit;
    }
}
