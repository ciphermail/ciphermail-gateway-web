/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.http;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class HTTPUtils
{
    private HTTPUtils() {
        // empty on purpose
    }

    /**
     * The default HTTP user agent
     */
    public static final String HTTP_USER_AGENT = "Mozilla/4.0 (compatible; CipherMail email encryption; " +
    		"www.ciphermail.com)";

    /**
     * Parses the provided URL query and returns a map with all names to values. The values are URL decoded.
     * The keys of the map will be converted to lowercase.
     * <p>
     * Example:
     * test=123&value=%27test%27&value=abc maps to test -> [123] and value -> ["test", abc]
     */
    public static Map<String, String[]> parseQuery(String query)
    {
        return parseQuery(query, true);
    }

    /**
     * Parses the provided URL query and returns a map with all names to values. The values are URL decoded. If
     * toLowercase is true the keys of the map will be converted to lowecase.
     * <p>
     * Example:
     * test=123&value=%27test%27&value=abc maps to test -> [123] and value -> ["test", abc]
     */
    public static Map<String, String[]> parseQuery(String query, boolean toLowercase)
    {
        Map<String, String[]> map = new HashMap<>();

        if (query == null) {
            return map;
        }

        String[] elements = StringUtils.split(query, '&');

        for (String element : elements)
        {
            element = element.trim();

            if (StringUtils.isEmpty(element)) {
                continue;
            }

            String name;
            String value;

            int i = element.indexOf('=');

            if (i > -1)
            {
                name = StringUtils.substring(element, 0, i);
                value = StringUtils.substring(element, i + 1);
            }
            else {
                name = element;
                value = "";
            }

            name = StringUtils.trimToEmpty(name);
            value = StringUtils.trimToEmpty(value);

            if (toLowercase) {
                name = name.toLowerCase();
            }

            value = URLDecoder.decode(value, StandardCharsets.UTF_8);

            String[] updated = (String[]) ArrayUtils.add(map.get(name), value);

            map.put(name, updated);
        }

        return map;
    }
}
