/*
 * Copyright (c) 2016-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Command line tool to test a JDBC connection.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings({"squid:S106","squid:S1148"})
public class JDBCConnectionTester
{
    private static final String COMMAND_NAME = JDBCConnectionTester.class.getName();

    /*
     * JDBC username option
     */
    private static final String USERNAME_OPTION = "username";

    /*
     * JDBC password option
     */
    private static final String PASSWORD_OPTION = "password";

    /*
     * JDBC url option
     */
    private static final String URL_OPTION = "url";

    /*
     * JDBC Drive classname
     */
    private static final String DRIVER_CLASS_OPTION = "driver-class";

    /*
     * Connection timeout option
     */
    private static final String TIMEOUT_OPTION = "timeout";

    private Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder().longOpt(USERNAME_OPTION).argName("username").hasArg().required().
                desc("JDBC usernamne").build());

        options.addOption(Option.builder().longOpt(PASSWORD_OPTION).argName("password").hasArg().required().
                desc("JDBC password").build());

        options.addOption(Option.builder().longOpt(URL_OPTION).argName("url").hasArg().required().
                desc("JDBC URL").build());

        options.addOption(Option.builder().longOpt(DRIVER_CLASS_OPTION).argName("class").hasArg().required().
                desc("JDBC Driver class").build());

        options.addOption(Option.builder().longOpt(TIMEOUT_OPTION).argName("seconds").hasArg().
                desc("JDBC connection timeout").build());

        return options;
    }

    private void testConnection(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        String driverClass = null;

        if (commandLine.hasOption(DRIVER_CLASS_OPTION)) {
            driverClass = commandLine.getOptionValue(DRIVER_CLASS_OPTION);
        }

        if (driverClass != null) {
            Class.forName(driverClass);
        }

        System.out.println("Trying to connect to database");

        try(Connection connection = DriverManager.getConnection(commandLine.getOptionValue(URL_OPTION),
                commandLine.getOptionValue(USERNAME_OPTION),
                commandLine.getOptionValue(PASSWORD_OPTION)))

        {
            int timeout = 30;

            if (commandLine.hasOption(TIMEOUT_OPTION)) {
                timeout = Integer.parseInt(commandLine.getOptionValue(TIMEOUT_OPTION));
            }

            if (connection.isValid(timeout)) {
                System.out.println("Connection is valid");
            }
            else {
                System.err.println("Connection is not valid");
            }
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        JDBCConnectionTester main = new JDBCConnectionTester();

        try {
            main.testConnection(args);
        }
        catch(IllegalArgumentException e)
        {
            System.err.println(e.getMessage());

            System.exit(1);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e);

            System.exit(4);
        }
    }
}
