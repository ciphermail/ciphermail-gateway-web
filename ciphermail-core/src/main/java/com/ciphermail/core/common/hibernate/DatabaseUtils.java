/*
 * Copyright (c) 2020-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import org.apache.commons.lang.UnhandledException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * General purpose database utils
 */
public class DatabaseUtils
{
    private static final Logger logger = LoggerFactory.getLogger(DatabaseUtils.class);

    private DatabaseUtils() {
        // Empty on purpose
    }

    /**
     * MySQL/MariaDB date value supported range is '1000-01-01 00:00:00' to '9999-12-31 23:59:59'
     *
     * For more info see: <a href="https://dev.mysql.com/doc/refman/8.0/en/datetime.html">...</a>
     */
    protected static final Date MYSQL_MAX_DATE;
    protected static final Date MYSQL_MIN_DATE;

    static {
        try {
            MYSQL_MAX_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("9999-12-31 23:59:59");
            MYSQL_MIN_DATE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("1000-01-01 00:00:00");
        }
        catch (ParseException e) {
            throw new UnhandledException(e);
        }
    }

    /**
     * MySQL/MariaDB date value supported range is '1000-01-01 00:00:00' to '9999-12-31 23:59:59'. If a date is
     * out of this range, the date will be limited
     *
     * For more info see: <a href="https://dev.mysql.com/doc/refman/8.0/en/datetime.html">...</a>
     */
    public static Date limitDate(Date date)
    {
        if (date == null) {
            return date;
        }

        if (MYSQL_MAX_DATE != null && date.after(MYSQL_MAX_DATE))
        {
            logger.warn("Date too large: {}. Date will be limited to: {}", date, MYSQL_MAX_DATE);

            date = MYSQL_MAX_DATE;
        }

        if (MYSQL_MIN_DATE != null && date.before(MYSQL_MIN_DATE))
        {
            logger.warn("Date too large: {}. Date will be limited to: {}", date, MYSQL_MIN_DATE);

            date = MYSQL_MIN_DATE;
        }

        return date;
    }
}
