/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.extractor.impl;

import com.ciphermail.core.common.extractor.ExtractedPart;
import com.ciphermail.core.common.extractor.TextExtractorContext;
import com.ciphermail.core.common.util.RewindableInputStream;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Objects;

/**
 * Default implementation of {@link ExtractedPart}
 *
 * @author Martijn Brinkers
 *
 */
public class ExtractedPartImpl implements ExtractedPart
{
    /*
     * Meta information about the extracted part.
     */
    private final TextExtractorContext context;

    /*
     * The stream containing the content
     */
    private final RewindableInputStream input;

    /**
     * The number of bytes of the content. -1 if the size is unknown
     */
    private final long byteCount;

    public ExtractedPartImpl(@Nonnull TextExtractorContext context, @Nonnull RewindableInputStream input,
            long byteCount)
    {
        this.context = Objects.requireNonNull(context);
        this.input = Objects.requireNonNull(input);
        this.byteCount = byteCount;
    }

    @Override
    public TextExtractorContext getContext() {
        return context;
    }

    @Override
    public RewindableInputStream getContent() {
        return input;
    }

    @Override
    public long getByteCount() {
        return byteCount;
    }

    @Override
    public void close()
    throws IOException
    {
        input.close();
    }
}
