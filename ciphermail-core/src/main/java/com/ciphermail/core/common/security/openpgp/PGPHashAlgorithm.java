/*
 * Copyright (c) 2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

/**
 * The OpenPGP hash algorithms as defined in section "9.4. Hash Algorithms" of RFC 4880
 *
 * @author Martijn Brinkers
 *
 */
public enum PGPHashAlgorithm
{
    MD5         (1,  "md5",              "pgp-md5"),
    SHA1        (2,  "SHA-1",            "pgp-sha1"),
    RIPEMD160   (3,  "RIPE-MD/160",      "pgp-ripemd160"),
    DOUBLE_SHA  (4,  "double-width SHA", "pgp-dw-sha1"),
    MD2         (5,  "MD2",              "pgp-md2" ),
    TIGER_192   (6,  "TIGER/192",        "pgp-tiger192"),
    HAVAL_5_160 (7,  "HAVAL",            "pgp-haval-5-160"),
    SHA256      (8,  "SHA256",           "pgp-sha256"),
    SHA384      (9,  "SHA384",           "pgp-sha384"),
    SHA512      (10, "SHA512",           "pgp-sha512"),
    SHA224      (11, "SHA224",           "pgp-sha224");

    /*
     * The tag value from section "9.4. Hash Algorithms"
     */
    private final int tag;

    /*
     * The friendly name of the hash algorithm
     */
    private final String friendlyName;

    /*
     * The "micalg" parameter for the "application/pgp-signature" protocol. See RFC 3156 section 5 "OpenPGP signed data"
     */
    private final String micAlg;

    PGPHashAlgorithm(int tag, String friendlyName, String micAlg)
    {
        this.tag = tag;
        this.friendlyName = friendlyName;
        this.micAlg = micAlg;
    }

    public int getTag() {
        return tag;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public String getMicAlg() {
        return micAlg;
    }

    public static PGPHashAlgorithm fromTag(int tag)
    {
        for (PGPHashAlgorithm algorithm : PGPHashAlgorithm.values())
        {
            if (tag == algorithm.tag) {
                return algorithm;
            }
        }

        return null;
    }

    public static PGPHashAlgorithm fromName(String name)
    {
        for (PGPHashAlgorithm algorithm : PGPHashAlgorithm.values())
        {
            if (algorithm.name().equalsIgnoreCase(name)) {
                return algorithm;
            }
        }

        return null;
    }
}
