/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certificate;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.asn1.ASN1Encoder;
import com.ciphermail.core.common.security.asn1.DERUtils;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import org.bouncycastle.asn1.ASN1EncodableVector;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CertificateEncoder
{
    private CertificateEncoder() {
        // empty on purpose
    }

    /*
     * Converts to Certificate to a byte array
     */
    public static byte[] encode(@Nonnull Certificate certificate, @Nonnull ObjectEncoding encoding)
    throws CertificateEncodingException, IOException
    {
        return switch (encoding) {
            case DER -> certificate.getEncoded();
            case PEM -> ASN1Encoder.encodePEM(Collections.singletonList(certificate));
            default -> throw new IllegalArgumentException("Unknown encoding: " + encoding);
        };
    }

    /**
     * Convert the collection of certificates to a byte array using the specified encoding
     * @param certificates
     * @param encoding
     * @return
     * @throws CertificateEncodingException
     * @throws IOException
     */
    public static byte[] encode(@Nonnull Collection<? extends Certificate> certificates,
            @Nonnull ObjectEncoding encoding)
    throws CertificateEncodingException, IOException
    {
        return switch (encoding) {
            case DER -> encodePKCS7(certificates);
            case PEM -> ASN1Encoder.encodePEM(certificates);
            default -> throw new IllegalArgumentException("Unknown encoding: " + encoding);
        };
    }

    /**
     * Convert the collection of certificates to a byte array. This method uses the crypto provider CertPath to do the
     * conversion. The Bouncycastle version of CertPath is slow so the other method should be used if speed is
     * important. The supported encodings depend on the actual implementation of CertPath.
     * @param certificates
     * @param encoding
     * @return
     * @throws IOException
     * @throws NoSuchProviderException
     * @throws CertificateException
     */
    public static byte[] encode(@Nonnull Collection<? extends Certificate> certificates, @Nonnull String encoding)
    throws CertificateException, NoSuchProviderException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        CertificateFactory certificateFactory = securityFactory.createCertificateFactory("X.509");

        List<? extends Certificate> certificateList = new LinkedList<>(certificates);

        CertPath certPath = certificateFactory.generateCertPath(certificateList);

        return certPath.getEncoded(encoding);
    }

    private static byte[] encodePKCS7(@Nonnull Collection<? extends Certificate> certificates)
    throws IOException, CertificateEncodingException
    {
        ASN1EncodableVector asn1Certificates = new ASN1EncodableVector();

        Iterator<? extends Certificate> certificateIt = certificates.iterator();

        while (certificateIt.hasNext())
        {
            Certificate certificate = certificateIt.next();

            asn1Certificates.add(DERUtils.toDERObject(certificate));
        }

        return ASN1Encoder.encodePKCS7(asn1Certificates, null /* no CRLs */);
    }
}
