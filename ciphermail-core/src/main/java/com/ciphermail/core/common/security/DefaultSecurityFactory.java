/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.security.bouncycastle.SecurityFactoryBouncyCastle;
import com.ciphermail.core.common.security.certificate.X509CertificateBuilder;
import com.ciphermail.core.common.security.crl.X509CRLBuilder;
import com.ciphermail.core.common.security.crl.X509CRLBuilderImpl;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.provider.CipherMailProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.TrustAnchor;
import java.util.Set;

public class DefaultSecurityFactory implements SecurityFactory
{
    private static final Logger logger = LoggerFactory.getLogger(DefaultSecurityFactory.class);

    /*
     * The majority of factory functions will be delegated to the Bouncycastle factory
     */
    private final SecurityFactory bouncyCastleSecurityFactory;

    public DefaultSecurityFactory()
    throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        bouncyCastleSecurityFactory = new SecurityFactoryBouncyCastle();

        // The Provider must be initialized
        CipherMailProvider.initialize(null);
    }

    @Override
    public CertPathBuilder createCertPathBuilder(@Nonnull String algorithm)
    throws NoSuchProviderException, NoSuchAlgorithmException
    {
        return bouncyCastleSecurityFactory.createCertPathBuilder(algorithm);
    }

    @Override
    public CertStore createCertStore(@Nonnull String certStoreType, CertStoreParameters certStoreParameters)
    throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createCertStore(certStoreType, certStoreParameters);
    }


    @Override
    public CertificateFactory createCertificateFactory(@Nonnull String certificateType)
    throws CertificateException, NoSuchProviderException
    {
        // Use a different CertificateFactory for PGP keys
        return CipherMailProvider.PGP_CERTIFICATE_TYPE.equalsIgnoreCase(certificateType) ?
            CertificateFactory.getInstance(certificateType, CipherMailProvider.PROVIDER) :
            bouncyCastleSecurityFactory.createCertificateFactory(certificateType);
    }

    @Override
    public Cipher createCipher(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException
    {
        return bouncyCastleSecurityFactory.createCipher(algorithm);
    }

    @Override
    public RandomGenerator createRandomGenerator()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createRandomGenerator();
    }

    @Override
    public KeyFactory createKeyFactory(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createKeyFactory(algorithm);
    }

    @Override
    public KeyPairGenerator createKeyPairGenerator(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createKeyPairGenerator(algorithm);
    }

    @Override
    public KeyStore createKeyStore(@Nonnull String keyStoreType)
    throws KeyStoreException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createKeyStore(keyStoreType);
    }

    @Override
    public MessageDigest createMessageDigest(@Nonnull String digestType)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createMessageDigest(digestType);
    }

    @Override
    public PKIXBuilderParameters createPKIXBuilderParameters(@Nonnull Set<TrustAnchor> trustAnchors,
            CertSelector targetConstraints)
    throws InvalidAlgorithmParameterException
    {
        return bouncyCastleSecurityFactory.createPKIXBuilderParameters(trustAnchors, targetConstraints);
    }

    @Override
    public SecretKeyFactory createSecretKeyFactory(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createSecretKeyFactory(algorithm);
    }

    @Override
    public SecureRandom createSecureRandom(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        SecureRandom random = bouncyCastleSecurityFactory.createSecureRandom(algorithm);

        logger.debug("SecureRandom instance created. Provider: {}, Algorithm: {}", random.getProvider(),
                random.getAlgorithm());

        return random;
    }

    @Override
    public SecureRandom createSecureRandom()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        SecureRandom random = bouncyCastleSecurityFactory.createSecureRandom();

        logger.debug("SecureRandom instance created. Provider: {}, Algorithm: {}", random.getProvider(),
                random.getAlgorithm());

        return random;
    }

    @Override
    public X509CertificateBuilder createX509CertificateBuilder()
    {
        return bouncyCastleSecurityFactory.createX509CertificateBuilder();
    }

    @Override
    public Mac createMAC(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createMAC(algorithm);
    }

    @Override
    public Signature createSignature(@Nonnull String algorithm)
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return bouncyCastleSecurityFactory.createSignature(algorithm);
    }
    @Override
    public X509CRLBuilder createX509CRLBuilder()
    {
        return new X509CRLBuilderImpl(SecurityFactoryBouncyCastle.PROVIDER_NAME /* signing provider */,
                SecurityFactoryBouncyCastle.PROVIDER_NAME /* CRL provider */);
    }

    @Override
    public String getNonSensitiveProvider() {
        return bouncyCastleSecurityFactory.getNonSensitiveProvider();
    }

    @Override
    public String getSensitiveProvider() {
        return bouncyCastleSecurityFactory.getSensitiveProvider();
    }
}
