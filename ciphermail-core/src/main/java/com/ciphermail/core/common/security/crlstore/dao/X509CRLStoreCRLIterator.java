/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.dao;

import com.ciphermail.core.common.hibernate.AbstractScrollableResultsIterator;
import com.ciphermail.core.common.security.crlstore.hibernate.X509CRLStoreEntity;
import org.apache.commons.lang.IllegalClassException;
import org.hibernate.ScrollableResults;

import java.security.cert.CRLSelector;
import java.security.cert.X509CRL;

public class X509CRLStoreCRLIterator extends AbstractScrollableResultsIterator<X509CRL>
{
    private final CRLSelector crlSelector;

    public X509CRLStoreCRLIterator(ScrollableResults<X509CRLStoreEntity> results, CRLSelector crlSelector)
    {
        super(results);

        this.crlSelector = crlSelector;
    }

    @Override
    protected Object parseElement(Object element)
    {
        if (!(element instanceof X509CRLStoreEntity crlStoreEntry))
        {
            throw new IllegalClassException("The object returned from the ScrollableResults" +
                    " is not a X509CRLStoreEntity.");
        }

        return crlStoreEntry.getCRL();
    }

    @Override
    protected boolean hasMatch(X509CRL crl)
    {
        if (crl == null) {
            return false;
        }

        if (crlSelector == null) {
            return true;
        }

        return crlSelector.match(crl);
    }
}