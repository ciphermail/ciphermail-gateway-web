/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPMarker;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.security.SignatureException;
import java.util.Objects;

/**
 * Implementation of PGPSignatureVerifier.
 *
 * Note: this PGPSignatureVerifier only checks whether the content is signed by the signers key. It does not
 * check whether the key is valid for signing and whether the key is trusted.
 *
 * @author Martijn Brinkers
 *
 */
public class PGPContentSignatureVerifierImpl implements PGPContentSignatureVerifier
{
    private static final Logger logger = LoggerFactory.getLogger(PGPContentSignatureVerifierImpl.class);

    /*
     * Max objects to scan before failing
     */
    private static final int MAX_OBJECTS = 10;

    /*
     * Provides PGPKeyRingEntry's
     */
    private final PGPKeyRingEntryProvider keyRingEntryProvider;

    public PGPContentSignatureVerifierImpl(@Nonnull PGPKeyRingEntryProvider keyRingEntryProvider) {
        this.keyRingEntryProvider = Objects.requireNonNull(keyRingEntryProvider);
    }

    @Override
    public PGPSignatureVerifierResult verify(@Nonnull InputStream contentInput, @Nonnull PGPSignature signature)
    throws IOException, PGPException, SignatureException
    {
        // Lookup the signer key
        PGPKeyRingEntry signerKeyEntry = keyRingEntryProvider.getByKeyID(signature.getKeyID());

        if (signerKeyEntry == null)
        {
            throw new PGPException("Signer's key with key ID " + PGPUtils.getKeyIDHex(signature.getKeyID()) +
                    " not found.");
        }

        signature.init(getPGPContentVerifierBuilderProvider(), signerKeyEntry.getPublicKey());

        int ch;

        while ((ch = contentInput.read()) >= 0) {
            signature.update((byte)ch);
        }

        boolean valid = signature.verify();

        if (!valid) {
            logger.debug("Message has been tampered with.");
        }

        return new PGPSignatureVerifierResultImpl(valid, signerKeyEntry);
    }

    @Override
    public PGPSignatureVerifierResult verify(@Nonnull InputStream contentInput, @Nonnull InputStream signatureInput)
    throws IOException, PGPException, SignatureException
    {
        signatureInput = PGPUtil.getDecoderStream(signatureInput);

        PGPObjectFactory objectFactory = new JcaPGPObjectFactory(signatureInput);

        Object streamObject;

        int objectCount = 0;

        while ((streamObject = objectFactory.nextObject()) != null)
        {
            logger.debug("PGP object class {}", streamObject.getClass());

            objectCount++;

            if (streamObject instanceof PGPMarker) {
                continue;
            }

            if (streamObject instanceof PGPSignatureList) {
                break;
            }

            if (objectCount > MAX_OBJECTS) {
                throw new PGPException("PGPSignatureList not found.");
            }
        }

        PGPSignature signature = PGPSignatureInspector.getFirstSignature((PGPSignatureList) streamObject);

        if (signature == null) {
            throw new PGPException("Signature is not available");
        }

        return verify(contentInput, signature);
    }

    private static PGPContentVerifierBuilderProvider getPGPContentVerifierBuilderProvider()
    {
        JcaPGPContentVerifierBuilderProvider builderProvider = new JcaPGPContentVerifierBuilderProvider();

        builderProvider.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());

        return builderProvider;
    }
}
