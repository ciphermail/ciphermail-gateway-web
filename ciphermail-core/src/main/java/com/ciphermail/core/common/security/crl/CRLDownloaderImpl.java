/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.URI;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class CRLDownloaderImpl implements CRLDownloader
{
    private static final Logger logger = LoggerFactory.getLogger(CRLDownloaderImpl.class);

    private final List<CRLDownloadHandler> downloadHandlers = Collections.synchronizedList(new LinkedList<>());

    public CRLDownloaderImpl(@Nonnull Collection<CRLDownloadHandler> downloadHandlersParam)  {
        this.downloadHandlers.addAll(Objects.requireNonNull(downloadHandlersParam));
    }

    @Override
    public Collection<? extends CRL> downloadCRLs(@Nonnull URI uri)
    throws IOException, CRLException
    {
        // look for a handler that supports the URI
        for (CRLDownloadHandler handler : downloadHandlers)
        {
            if (handler.canHandle(uri))
            {
                logger.info("Trying to download CRL from {}", uri);

                return handler.downloadCRLs(uri);
            }
        }

        logger.debug("Could not find a handler for {}", uri);

        return new LinkedList<>();
    }
}
