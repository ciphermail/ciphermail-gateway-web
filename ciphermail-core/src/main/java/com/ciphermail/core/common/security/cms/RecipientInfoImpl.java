/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.cms;

import com.ciphermail.core.common.security.KeyIdentifier;
import com.ciphermail.core.common.security.certificate.X500PrincipalUtils;
import org.apache.commons.lang.BooleanUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.Recipient;
import org.bouncycastle.cms.RecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.jcajce.JceAlgorithmIdentifierConverter;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.PrivateKey;
import java.util.Objects;

/**
 * Implementation of RecipientInfo
 *
 * @author Martijn Brinkers
 *
 */
public class RecipientInfoImpl implements RecipientInfo
{
    private final Logger logger = LoggerFactory.getLogger(RecipientInfoImpl.class);

    /*
     * Flag that unwrapping must produce a key that will return a meaningful value from a call to Key.getEncoded().
     * This is important if you are using a HSM for unwrapping and using a software based provider for
     * decrypting the content. This is required when using Utimaco HSM but not when using an nCipher HSM.
     * The value is set using a System property (see static initializer)
     */
    private static final boolean MUST_PRODUCE_ENCODABLE_UNWRAPPED_KEY;

    static
    {
        String value = System.getProperty("ciphermail.crypto.cms.mustProduceEncodableUnwrappedKey");

        MUST_PRODUCE_ENCODABLE_UNWRAPPED_KEY = BooleanUtils.toBoolean(value);
    }

    /*
     * Information about the CMS recipient (algorithm used etc.)
     */
    private final RecipientInformation recipientInformation;

    /*
     * The provider for non sensitive operations
     */
    private final String nonSensitiveProvider;

    /*
     * The provider for sensitive operations
     */
    private final String sensitiveProvider;

    /*
     * Identifies the recipient (this is used for finding the correct private key for decryption)
     */
    private KeyIdentifier recipientIdentifier;

    public RecipientInfoImpl(@Nonnull RecipientInformation recipientInformation, String nonSensitiveProvider,
            String sensitiveProvider)
    throws RecipientInfoException
    {
        this.recipientInformation = Objects.requireNonNull(recipientInformation);
        this.nonSensitiveProvider = nonSensitiveProvider;
        this.sensitiveProvider = sensitiveProvider;

        initRecipientId();
    }

    private Recipient getRecipient(@Nonnull Key key)
    throws RecipientInfoException
    {
        // only support Public/Private keys
        if (!(key instanceof PrivateKey)) {
            throw new RecipientInfoException("The key is not a PrivateKey");
        }

        JceKeyTransEnvelopedRecipient recipient = new JceKeyTransEnvelopedRecipient((PrivateKey) key);

        logger.debug("sensitiveProvider: {}, nonSensitiveProvider: {}, mustProduceEncodableUnwrappedKey: {}",
                sensitiveProvider, nonSensitiveProvider, MUST_PRODUCE_ENCODABLE_UNWRAPPED_KEY);

        recipient.setProvider(sensitiveProvider);
        recipient.setContentProvider(nonSensitiveProvider);
        recipient.setMustProduceEncodableUnwrappedKey(MUST_PRODUCE_ENCODABLE_UNWRAPPED_KEY);

        return recipient;
    }

    @Override
    public byte[] getContent(@Nonnull Key key)
    throws RecipientInfoException
    {
        try {
            return recipientInformation.getContent(getRecipient(key));
        }
        catch (CMSException e) {
            throw new RecipientInfoException(e);
        }
    }

    @Override
    public InputStream getContentStream(@Nonnull Key key)
    throws RecipientInfoException
    {
        try {
            return recipientInformation.getContentStream(getRecipient(key)).getContentStream();
        }
        catch (CMSException | IOException e) {
            throw new RecipientInfoException(e);
        }
    }

    @Override
    public String getKeyEncryptionAlgorithmOID()
    {
        return recipientInformation.getKeyEncryptionAlgOID();
    }

    @Override
    public AlgorithmParameters getKeyEncryptionAlgorithmParameters()
    throws RecipientInfoException
    {
        try {
            return new JceAlgorithmIdentifierConverter().setProvider(nonSensitiveProvider).
                    getAlgorithmParameters(recipientInformation.getKeyEncryptionAlgorithm());
        }
        catch (CMSException e) {
            throw new RecipientInfoException(e);
        }
    }

    /*
     * Convert the BC version of RecipientId to our own RecipientId
     */
    private KeyTransRecipientId getKeyTransRecipientId(org.bouncycastle.cms.KeyTransRecipientId otherRecipientId)
    throws RecipientInfoException
    {
        try {
            return new KeyTransRecipientIdImpl(
                    X500PrincipalUtils.fromX500Name(otherRecipientId.getIssuer()),
                    otherRecipientId.getSerialNumber(),
                    otherRecipientId.getSubjectKeyIdentifier());
        }
        catch (IOException e) {
            throw new RecipientInfoException(e);
        }
    }

    private void initRecipientId()
    throws RecipientInfoException
    {
        // We currently only support KeyTransRecipientInformation
        if (recipientInformation instanceof KeyTransRecipientInformation)
        {
            RecipientId recipientId = recipientInformation.getRID();

            if (recipientId instanceof org.bouncycastle.cms.KeyTransRecipientId keyTransRecipientId) {
                recipientIdentifier = getKeyTransRecipientId(keyTransRecipientId);
            }
            else {
                logger.warn("Unsupported recipientId type. Class: {}", recipientId.getClass());
            }
        }
        else {
            logger.warn("Unsupported recipientInformation type. Class: {}", recipientInformation.getClass());
        }
    }

    @Override
    public KeyIdentifier getRecipientId()
    {
        return recipientIdentifier;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(getRecipientId().toString());
        sb.append("/");
        sb.append(getKeyEncryptionAlgorithmOID());

        AlgorithmParameters keyAlgParams;

        try {
            keyAlgParams = getKeyEncryptionAlgorithmParameters();

            sb.append("/");
            sb.append(keyAlgParams);
        }
        catch (Exception e) {
            // can happen if there are no encryption algorithm parameters
            logger.debug("Error getting Encryption Algorithm Parameters", e);
        }

        return sb.toString();
    }
}
