/*
 * Copyright (c) 2015, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.csr;

import com.ciphermail.core.common.jackson.JacksonUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.KeyPair;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Objects;

/*
 * To support future changes, ignore unknown properties
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(
    fieldVisibility = JsonAutoDetect.Visibility.ANY,
    setterVisibility = JsonAutoDetect.Visibility.NONE,
    getterVisibility = JsonAutoDetect.Visibility.NONE,
    isGetterVisibility = JsonAutoDetect.Visibility.NONE,
    creatorVisibility = JsonAutoDetect.Visibility.NONE
)
public class CSRStoreEntryImpl implements CSRStoreEntry
{
    /*
     * ID of this entry
     */
    private String id;

    /*
     * The date the CSR was created
     */
    private Date date;

    /*
     * The PKCS10 certificate signing request
     */
    private PKCS10CertificationRequest csr;

    /*
     * Public/private key pair for the CSR
     */
    private KeyPair keyPair;

    /*
     * The X509 certificate chain
     */
    private X509Certificate[] certificateChain;

    protected CSRStoreEntryImpl() {
        // Jackson requires a default constructor
    }

    public CSRStoreEntryImpl(@Nonnull String id)
    {
        this.id = Objects.requireNonNull(id);
        this.date = new Date();
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public PKCS10CertificationRequest getPKCS10Request() {
        return csr;
    }

    public void setCSR(PKCS10CertificationRequest csr) {
        this.csr = csr;
    }

    @Override
    public KeyPair getKeyPair() {
        return keyPair;
    }

    public void setKeyPair(KeyPair keyPair) {
        this.keyPair = keyPair;
    }

    @Override
    public X509Certificate[] getCertificateChain() {
        return certificateChain;
    }

    public void setCertificateChain(X509Certificate... certificateChain) {
        this.certificateChain = certificateChain;
    }

    /**
     * Serializes this object
     *
     * @return serialized this
     */
    public @Nonnull byte[] serialize()
    throws JsonProcessingException
    {
        return JacksonUtil.getObjectMapper().writeValueAsBytes(this);
    }

    /**
     * Deserialize the serialized CSRStoreEntryImpl object
     *
     * @param serialized the serialized CSRStoreEntryImpl
     * @return a deserialized object
     * @throws UnrecoverableKeyException, IOException
     */
    public static CSRStoreEntryImpl deserialize(@Nonnull byte[] serialized)
    throws UnrecoverableKeyException, IOException
    {
        Object o = JacksonUtil.getObjectMapper().readValue(serialized, CSRStoreEntryImpl.class);

        if (!(o instanceof CSRStoreEntryImpl csrStoreEntry)) {
            throw new UnrecoverableKeyException("The serialized is not the correct type.");
        }

        return csrStoreEntry;
    }
}
