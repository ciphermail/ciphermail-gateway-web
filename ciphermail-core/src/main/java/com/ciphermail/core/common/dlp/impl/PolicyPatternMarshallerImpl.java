/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.cache.PatternCache;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.ciphermail.core.common.jackson.JacksonUtil;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

/**
 * Used to marshall and unmarshall PolicyPatternImpl to/from JSON.
 */
public class PolicyPatternMarshallerImpl implements PolicyPatternMarshaller
{
    /*
     * Used to get cached compiled Pattern's from a reg expr string.
     */
    private final PatternCache patternCache;

    /*
     * Registry used to lookup MatchFilter's based on the name of the MatchFilter.
     */
    private final MatchFilterRegistry matchFilterRegistry;

    /*
     * Registry used to lookup Validator's based on the name of the validator.
     */
    private final ValidatorRegistry validatorRegistry;

    public PolicyPatternMarshallerImpl(
            @Nonnull PatternCache patternCache,
            @Nonnull MatchFilterRegistry matchFilterRegistry,
            @Nonnull ValidatorRegistry validatorRegistry)
    {
        this.patternCache = Objects.requireNonNull(patternCache);
        this.matchFilterRegistry = Objects.requireNonNull(matchFilterRegistry);
        this.validatorRegistry = Objects.requireNonNull(validatorRegistry);
    }

    /**
     * Reads JSON from the input stream and returns an PolicyPatternImpl from the XML content
     */
    @Override
    public PolicyPatternImpl unmarshall(@Nonnull InputStream input)
    throws IOException
    {
        PolicyPatternImpl policyPattern = JacksonUtil.getObjectMapper().readValue(input, PolicyPatternImpl.class);

        initPolicyPattern(policyPattern);

        return policyPattern;
    }

    @Override
    public void initPolicyPattern(@Nonnull PolicyPattern policyPattern)
    {
        if (policyPattern instanceof PolicyPatternImpl policyPatternImpl)
        {
            policyPatternImpl.setPatternCache(patternCache);
            policyPatternImpl.setMatchFilterRegistry(matchFilterRegistry);
            policyPatternImpl.setValidatorRegistry(validatorRegistry);
        }
    }

    /**
     * Writes the PolicyPattern to the output stream to JSON.
     */
    @Override
    public void marshall(@Nonnull PolicyPattern policyPattern, @Nonnull OutputStream output)
    throws IOException
    {
        JacksonUtil.getObjectMapper().writeValue(output, policyPattern);
    }
}