/*
 * Copyright (c) 2009-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import javax.security.auth.x500.X500Principal;
import java.util.Collection;

public interface CABuilderParameters
{
    /**
     * The subject for the root certificate
     */
    void setRootSubject(X500Principal subject);
    X500Principal getRootSubject();

    /**
     * The subject for the intermediate certificate
     */
    void setIntermediateSubject(X500Principal subject);
    X500Principal getIntermediateSubject();

    /**
     * The number of days the root is valid (days > 0).
     */
    void setRootValidity(int days);
    int getRootValidity();

    /**
     * The number of days the intermediate is valid (days > 0).
     */
    void setIntermediateValidity(int days);
    int getIntermediateValidity();

    /**
     * The length of the root key (in bits) to use (>= 1024).
     */
    void setRootKeyLength(int bits);
    int getRootKeyLength();

    /**
     * The length of the intermediate key (in bits) to use (>= 1024).
     */
    void setIntermediateKeyLength(int bits);
    int getIntermediateKeyLength();

    /**
     * The signature algorithm used to sign the certificate
     */
    void setSignatureAlgorithm(String signatureAlgorithm);
    String getSignatureAlgorithm();

    /**
     * Adds the uri's as CRL distribution points (only for CA)
     */
    void setCRLDistributionPoints(Collection<String> uris);
    Collection<String> getCRLDistributionPoints();
}
