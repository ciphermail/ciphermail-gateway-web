/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.util.ProcessRunner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.io.output.WriterOutputStream;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Class that calls GPG
 * <p>
 * !!! DO NOT USE FOR PRODUCTION. ONLY FOR TESTING PURPOSES !!!
 *
 */
public class GPG
{
    private static final Logger logger = LoggerFactory.getLogger(GPG.class);

    /*
     * The GPG binary. We assume that gpg is on the path
     */
    private static final String SYSTEM_GPG_PATH = "gpg";

    /*
     * Dir with PGP config files
     */
    private static final File GPG_FILES_DIR = new File("src/test/resources/testdata/pgp/gnupg");

    /*
     * If true, --ignore-mdc-error will be added when decrypting
     */
    private boolean ignoreMDCError;

    private String getGPGPath()
    {
        /*
         * Use system OpenSSL unless the system property ciphermail.openssl.binary.path is set
         */
        String gpgPath = System.getProperty("ciphermail.gpg.binary.path");

        if (gpgPath == null) {
            gpgPath = SYSTEM_GPG_PATH;
        }

        return gpgPath;
    }

    /*
     * GPG requires that the gpg home dir is owned by the caller and that files have mod 700. To make sure this
     * works for a checked-out git, create a temp home dir and copy the gpg files
     * Note: this is required when running tests on gitlab because gitlab runner will check out the git repo as root
     */
    private File createHomeDir()
    throws IOException
    {
        File tempHomeDir = Files.createTempDirectory("gpg",
                PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-----"))).toFile();

        // copy GPG conf files to temp home
        FileUtils.copyDirectory(GPG_FILES_DIR, tempHomeDir);

        return tempHomeDir;
    }

    public boolean verify(File file)
    throws IOException
    {
        return verify(file, null, null);
    }

    public boolean verify(File file, OutputStream output, OutputStream error)
    throws IOException
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setThrowExceptionOnErrorExitCode(false);

        File homeDir = createHomeDir();

        try {
            List<String> cmd = new LinkedList<>();

            cmd.add(getGPGPath());
            cmd.add("--homedir");
            cmd.add(homeDir.getPath());
            cmd.add("-vv");
            cmd.add("-q");
            cmd.add("--no-tty");
            cmd.add("--verify");
            cmd.add(file.getPath());

            runner.setOutput(output);
            runner.setError(error);

            return runner.run(cmd) == 0;
        }
        finally {
            FileUtils.deleteDirectory(homeDir);
        }
    }

    public boolean verify(File signed, File signature)
    throws IOException
    {
        return verify(signed, signature, null, null);
    }

    public boolean verify(File signed, File signature, OutputStream output, OutputStream error)
    throws IOException
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setThrowExceptionOnErrorExitCode(false);

        File homeDir = createHomeDir();

        try {
            List<String> cmd = new LinkedList<>();

            cmd.add(getGPGPath());
            cmd.add("--homedir");
            cmd.add(homeDir.getPath());
            cmd.add("-vv");
            cmd.add("-q");
            cmd.add("--no-tty");
            cmd.add("--verify");
            cmd.add(signature.getPath());
            cmd.add(signed.getPath());

            runner.setOutput(output);
            runner.setError(error);

            return runner.run(cmd) == 0;
        }
        finally {
            FileUtils.deleteDirectory(homeDir);
        }
    }

    public boolean decrypt(File inputFile, String password, OutputStream output, OutputStream error)
    throws IOException
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setThrowExceptionOnErrorExitCode(false);

        File homeDir = createHomeDir();

        try {
            List<String> cmd = new LinkedList<>();

            cmd.add(getGPGPath());
            cmd.add("--homedir");
            cmd.add(homeDir.getPath());
            cmd.add("-vv");
            cmd.add("-q");
            cmd.add("-d");
            cmd.add("--no-tty");
            cmd.add("--batch");
            cmd.add("--pinentry-mode");
            cmd.add("loopback");
            cmd.add("--passphrase-fd");
            cmd.add("0");

            if (ignoreMDCError) {
                cmd.add("--ignore-mdc-error");
            }

            cmd.add(inputFile.getPath());

            runner.setInput(IOUtils.toInputStream(password, StandardCharsets.US_ASCII.name()));

            runner.setOutput(output);
            runner.setError(error);

            return runner.run(cmd) == 0;
        }
        finally {
            FileUtils.deleteDirectory(homeDir);
        }
    }

    public Date getMasterKeyExpirationDate(File inputFile)
    throws IOException
    {
        ProcessRunner runner = new ProcessRunner();

        runner.setThrowExceptionOnErrorExitCode(false);

        File homeDir = createHomeDir();

        try {
            List<String> cmd = new LinkedList<>();

            cmd.add(getGPGPath());
            cmd.add("--homedir");
            cmd.add(homeDir.getPath());
            cmd.add("--with-colons");
            cmd.add(inputFile.getPath());

            StringWriter outputWriter = new StringWriter();
            StringWriter errorWriter = new StringWriter();

            try(WriterOutputStream output = WriterOutputStream.builder().setCharset(StandardCharsets.UTF_8)
                    .setWriter(outputWriter).get();
                WriterOutputStream error = WriterOutputStream.builder().setCharset(StandardCharsets.UTF_8)
                        .setWriter(errorWriter).get())
            {
                runner.setOutput(output);
                runner.setError(error);

                runner.run(cmd);
            }

            Date expirationDate = null;

            try (LineIterator li = new LineIterator(new StringReader(outputWriter.toString())))
            {
                if (li.hasNext())
                {
                    String masterKeyInfo = li.next();

                    String[] columns = masterKeyInfo.split(":");

                    if (columns.length >= 7) {
                        // Column 6 is the expiration date
                        String value = StringUtils.trimToNull(columns[6]);

                        if (value != null) {
                            expirationDate = new Date(1000 * Long.parseLong(value));
                        }
                        else {
                             // No expiration date set
                            logger.debug("Empty date value");
                        }
                    }
                    else {
                        logger.warn("Columns size < 7");
                    }
                }
                else {
                    throw new IOException("No result. Error output: "+ errorWriter.toString());
                }
            }

            return expirationDate;
        }
        finally {
            FileUtils.deleteDirectory(homeDir);
        }
    }

    public boolean isIgnoreMDCError() {
        return ignoreMDCError;
    }

    public void setIgnoreMDCError(boolean ignoreMDCError) {
        this.ignoreMDCError = ignoreMDCError;
    }
}
