/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.certstore.hibernate;

import com.ciphermail.core.common.hibernate.CloseSessionOnCloseIterator;
import com.ciphermail.core.common.hibernate.HibernateUtils;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.certstore.X509CertStoreExt;
import com.ciphermail.core.common.security.certstore.X509StoreEventListener;
import com.ciphermail.core.common.security.certstore.dao.X509CertStoreDAO;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CurrentDateProvider;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.jdbc.ReturningWork;

import javax.annotation.Nonnull;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class X509CertStoreExtHibernate implements X509CertStoreExt
{
    /*
     * Name of the Certificate Store. This can be used to have multiple distinct certificate stores in the database
     * like for example a certificate store (end-user and intermediate) and a root store
     */
    private final String storeName;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    /*
     * Listener that will be notified when the store changes (add or remove certificates)
     */
    private final AtomicReference<X509StoreEventListener> eventListener = new AtomicReference<>();

    public X509CertStoreExtHibernate(@Nonnull String storeName, @Nonnull SessionManager sessionManager)
    {
        this.storeName = Objects.requireNonNull(storeName);
        this.sessionManager = Objects.requireNonNull(sessionManager);
    }

    @Override
    public @Nonnull X509CertStoreEntity addCertificate(@Nonnull X509Certificate certificate,
            String keyAlias)
    throws CertStoreException
    {
        // we will check whether this certificate is already in the store because a certificate
        // may only be stored once. Adding this check does not guarantee that the certificate will
        // be added when the transaction is committed because a 'race condition' is always
        // possible in which case a constraint violation exception will be thrown
        X509CertStoreEntity entry = getByCertificate(certificate);

        if (entry == null)
        {
            entry = getDAO().addCertificate(certificate, keyAlias);

            fireChangeEvent();
        }

        return entry;
    }

    @Override
    public @Nonnull X509CertStoreEntity addCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return addCertificate(certificate, null);
    }

    @Override
    public void removeCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        getDAO().removeCertificate(certificate);

        fireChangeEvent();
    }

    @Override
    public void removeAllEntries()
    throws CertStoreException
    {
        getDAO().removeAllEntries();

        fireChangeEvent();
    }

    @Override
    public long size() {
        return getDAO().getRowCount();
    }

    @Override
    public long size(Expired expired, MissingKeyAlias missingKeyAlias) {
        return getDAO().getRowCount(expired, missingKeyAlias, CurrentDateProvider.getNow());
    }

    @Override
    public CloseableIterator<? extends X509CertStoreEntity> getCertStoreIterator(Expired expired,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().getCertStoreIterator(expired, missingKeyAlias, CurrentDateProvider.getNow(), firstResult, maxResults);
    }

    @Override
    public X509CertStoreEntity getByCertificate(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return getDAO().getByCertificate(certificate);
    }

    @Override
    public boolean contains(@Nonnull X509Certificate certificate)
    throws CertStoreException
    {
        return getDAO().getByCertificate(certificate) != null;
    }

    @Override
    public CloseableIterator<X509CertStoreEntity> getByEmail(@Nonnull String email, Match match,
            Expired expired, MissingKeyAlias missingKeyAlias)
    throws CertStoreException
    {
        return getByEmail(email, match, expired, missingKeyAlias, null, null);
    }

    @Override
    public CloseableIterator<X509CertStoreEntity> getByEmail(@Nonnull String email, Match match,
            Expired expired, MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().getByEmail(email, match, expired, missingKeyAlias, CurrentDateProvider.getNow(), firstResult, maxResults);
    }

    @Override
    public long getByEmailCount(@Nonnull String email, Match match, Expired expired, MissingKeyAlias missingKeyAlias) {
        return getDAO().getByEmailCount(email, match, expired, missingKeyAlias, CurrentDateProvider.getNow());
    }

    @Override
    public CloseableIterator<X509CertStoreEntity> searchBySubject(@Nonnull String subject,
            Expired expired, MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    {
        return getDAO().searchBySubject(subject, expired, missingKeyAlias, CurrentDateProvider.getNow(), firstResult, maxResults);
    }

    @Override
    public long getSearchBySubjectCount(@Nonnull String subject, Expired expired, MissingKeyAlias missingKeyAlias) {
        return getDAO().getSearchBySubjectCount(subject, expired, missingKeyAlias, CurrentDateProvider.getNow());
    }

    @Override
    public CloseableIterator<X509CertStoreEntity> searchByIssuer(@Nonnull String issuer,
            Expired expired, MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    {
        return getDAO().searchByIssuer(issuer, expired, missingKeyAlias, CurrentDateProvider.getNow(), firstResult, maxResults);
    }

    @Override
    public long getSearchByIssuerCount(@Nonnull String issuer, Expired expired, MissingKeyAlias missingKeyAlias) {
        return getDAO().getSearchByIssuerCount(issuer, expired, missingKeyAlias, CurrentDateProvider.getNow());
    }

    @Override
    public X509CertStoreEntity getByThumbprint(@Nonnull String thumbprint) {
        return getDAO().getByThumbprint(thumbprint);
    }

    @Override
    public CloseableIterator<X509CertStoreEntity> getCertStoreIterator(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        return getDAO().getCertStoreIterator(certSelector, missingKeyAlias, firstResult, maxResults);
    }

    /*
     * A call to getCertificates can result in a lot of records being returned which can result in an
     * out of memory because the first level cache grows with every object. There are a lot of possible
     * solutions but we will use a StatelessSession because we are only interested in the certificate
     * and not in the entry itself (StatelessSession does not load collection etc.). The drawback of
     * this approach is that getCertificates has it's own transaction.
     *
     * Note: Because the statelessSession uses it's own session/transaction it can happen that changes made in other
     * sessions/transactions are not seen until the transactions have been committed.
     */
    public Collection<X509Certificate> getCertificates(CertSelector certSelector, MissingKeyAlias missingKeyAlias,
            Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        try {
            return session.doReturningWork(connection ->
            {
                StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

                try {
                    return getStatelessDAO(statelessSession).getCertificates(certSelector,
                            missingKeyAlias, firstResult, maxResults);
                }
                catch (CertStoreException e) {
                    throw new SQLException(e);
                }
                finally {
                    HibernateUtils.closeSessionQuietly(statelessSession);
                }
            });
        }
        catch (HibernateException e) {
            throw new CertStoreException(e);
        }
    }

    @Override
    public Collection<X509Certificate> getCertificates(CertSelector certSelector)
    throws CertStoreException
    {
        return getCertificates(certSelector, MissingKeyAlias.ALLOWED, null, null);
    }

    /*
     * We want to use a StatelessSession for performance and memory reasons but we do want to wrap it in it's own
     * transaction. We will therefore wrap the transaction and session into a wrapper iterator that will commit
     * the transaction and close the session when the iterator is closed.
     * We need to support a large number of certificates (10th of thousands) and we therefore have to work with
     * StatelessSessions and iterators.
     *
     * Note: Because the statelessSession uses it's own session/transaction it can happen that changes made in other
     * sessions/transactions are not seen until the transactions have been committed.
     */
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector,
            MissingKeyAlias missingKeyAlias, Integer firstResult, Integer maxResults)
    throws CertStoreException
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        try {
            return session.doReturningWork((ReturningWork<CloseableIterator<X509Certificate>>) connection ->
            {
                StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

                try {
                    return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).getCertificateIterator(
                            certSelector, missingKeyAlias, firstResult, maxResults),
                            SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
                }
                catch (Exception e)
                {
                    HibernateUtils.closeSessionQuietly(statelessSession);

                    throw new SQLException(e);
                }
            });
        }
        catch (HibernateException e) {
            throw new CertStoreException(e);
        }
    }

    @Override
    public CloseableIterator<X509Certificate> getCertificateIterator(CertSelector certSelector)
    throws CertStoreException
    {
        return getCertificateIterator(certSelector, MissingKeyAlias.ALLOWED, null, null);
    }

    private X509CertStoreDAO getDAO() {
        return X509CertStoreDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()), storeName);
    }

    private X509CertStoreDAO getStatelessDAO(@Nonnull StatelessSession statelessSession)
    {
        return X509CertStoreDAO.getInstance(SessionAdapterFactory.create(
                statelessSession, sessionManager.getSessionFactory()), storeName);
    }

    @Override
    public void setStoreEventListener(@Nonnull X509StoreEventListener eventListener) {
        this.eventListener.set(Objects.requireNonNull(eventListener));
    }

    private void fireChangeEvent()
    {
        if (eventListener.get() != null) {
            eventListener.get().onChange();
        }
    }
}
