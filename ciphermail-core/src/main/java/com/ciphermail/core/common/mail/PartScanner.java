/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.mail;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import java.io.IOException;

/**
 * Walks through all message parts of a message and notifies the PartListener when a part is found.
 *
 * @author Martijn Brinkers
 *
 */
public class PartScanner
{
    private final PartListener partListener;

    /*
     * maximum recursive depth. Default no max (-1)
     */
    private int maxDepth = -1;

    /*
     * If true, and the maxDepth is reached a MaxDepthReachedException exception will be thrown.
     */
    private boolean exceptionOnMaxDepthReached = false;

    public interface PartListener
    {
        /**
         * If false is returned the scanning is stopped.
         *
         * @param part
         * @return continue scanning.
         * @throws PartException
         */
        boolean onPart(Part parent, Part part, Object context)
        throws PartException;
    }

    public PartScanner(final PartListener partListener, int maxDepth)
    {
        this.partListener = partListener;
        this.maxDepth = maxDepth;
    }

    public PartScanner(PartListener partListener)
    {
        this.partListener = partListener;
    }


    public void scanPart(Part part, Object context)
    throws MessagingException, IOException, PartException
    {
        scanPart(part, part, 0, context);
    }

    public void scanPart(Part part)
    throws MessagingException, IOException, PartException
    {
        scanPart(part, part, 0, null);
    }

    protected boolean scanPart(Part parent, Part part, int depth, Object context)
    throws MessagingException, IOException, PartException
    {
        if (part == null) {
            return false;
        }

        boolean stop = false;

        depth++;

        // check if this part is a multipart. If so we need to check all
        // the child parts
        if (part.isMimeType("multipart/*"))
        {
            if (depth <= maxDepth || maxDepth < 0) {
                stop = scanMultipart(parent, part, depth, context);
            }
            else {
                if (exceptionOnMaxDepthReached) {
                    throw new MaxDepthReachedException(maxDepth);
                }

                // we do not further scan inside the part but we must fire the event
                if (partListener != null) {
                    stop = !partListener.onPart(parent, part, context);
                }
            }
        }
        else {
            if (partListener != null) {
                stop = !partListener.onPart(parent, part, context);
            }
        }

        return stop;
    }

    protected boolean scanMultipart(Part parent, Part part, int depth, Object context)
    throws MessagingException, IOException, PartException
    {
        boolean stop = false;

        Multipart multipart = (Multipart) part.getContent();

        int partCount = multipart.getCount();

        for (int i = 0; i < partCount; i++)
        {
            Part child = multipart.getBodyPart(i);

            // recursively call scanPart
            stop = scanPart(part, child, depth, context);

            if (stop) {
                break;
            }
        }

        return stop;
    }

    public boolean isExceptionOnMaxDepthReached() {
        return exceptionOnMaxDepthReached;
    }

    public void setExceptionOnMaxDepthReached(boolean exceptionOnMaxDepthReached) {
        this.exceptionOnMaxDepthReached = exceptionOnMaxDepthReached;
    }
}
