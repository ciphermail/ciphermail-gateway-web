/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.hibernate;

import com.ciphermail.core.common.security.SecurityConstants;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.util.HexUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.metamodel.spi.ValueAccess;
import org.hibernate.usertype.CompositeUserType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;

/**
 * Hibernate CompositeUserType that can be used to persist {@link X509Certificate}.
 *
 * @author Martijn Brinkers
 *
 */
public class X509CertificateUserType implements CompositeUserType<X509Certificate>
{
    private static final int CERTIFICATE_COLUMN_INDEX = 0;
    private static final int ISSUER_COLUMN_INDEX = 1;
    private static final int ISSUER_FRIENDLY_COLUMN_INDEX = 2;
    private static final int NOT_AFTER_COLUMN_INDEX = 3;
    private static final int NOT_BEFORE_COLUMN_INDEX = 4;
    private static final int SERIAL_NUMBER_COLUMN_INDEX = 5;
    private static final int SUBJECT_COLUMN_INDEX = 6;
    private static final int SUBJECT_FRIENDLY_COLUMN_INDEX = 7;
    private static final int SUBJECT_KEY_IDENT_COLUMN_INDEX = 8;
    private static final int THUMBPRINT_COLUMN_INDEX = 9;

    public static final String CERTIFICATE_COLUMN_NAME = "certificate";
    public static final String ISSUER_COLUMN_NAME = "issuer";
    public static final String ISSUER_FRIENDLY_COLUMN_NAME = "issuerFriendly";
    public static final String NOT_AFTER_COLUMN_NAME = "notAfter";
    public static final String NOT_BEFORE_COLUMN_NAME = "notBefore";
    public static final String SERIAL_NUMBER_COLUMN_NAME = "serial";
    public static final String SUBJECT_COLUMN_NAME = "subject";
    public static final String SUBJECT_FRIENDLY_COLUMN_NAME = "subjectFriendly";
    public static final String SUBJECT_KEY_IDENT_COLUMN_NAME = "subjectKeyIdentifier";
    public static final String THUMBPRINT_COLUMN_NAME = "thumbprint";

    public static class X509CertificateMapper {
        // in sorted order
        byte[] certificate;
        String issuer;
        String issuerFriendly;
        Date notAfter;
        Date notBefore;
        String serial;
        String subject;
        String subjectFriendly;
        String subjectKeyIdentifier;
        String thumbprint;
    }

    @Override
    public Object getPropertyValue(X509Certificate certificate, int propertyIndex)
    throws HibernateException
    {
        try {
            return switch (propertyIndex) {
                case CERTIFICATE_COLUMN_INDEX -> certificate.getEncoded();
                case ISSUER_COLUMN_INDEX -> X509CertificateInspector.getIssuerCanonical(certificate);
                case ISSUER_FRIENDLY_COLUMN_INDEX -> X509CertificateInspector.getIssuerFriendly(certificate);
                case NOT_AFTER_COLUMN_INDEX -> certificate.getNotAfter();
                case NOT_BEFORE_COLUMN_INDEX -> certificate.getNotBefore();
                case SERIAL_NUMBER_COLUMN_INDEX -> X509CertificateInspector.getSerialNumberHex(certificate);
                case SUBJECT_COLUMN_INDEX -> X509CertificateInspector.getSubjectCanonical(certificate);
                case SUBJECT_FRIENDLY_COLUMN_INDEX -> X509CertificateInspector.getSubjectFriendly(certificate);
                case SUBJECT_KEY_IDENT_COLUMN_INDEX -> getSubjectKeyIdentifierHex(certificate);
                case THUMBPRINT_COLUMN_INDEX -> X509CertificateInspector.getThumbprint(certificate);
                default -> null;
            };
        }
        catch (CertificateEncodingException | IOException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new HibernateException(e);
        }
    }

    private static X509Certificate decodeCertificate(byte[] encodedCertificate)
    {
        X509Certificate certificate = null;

        if (encodedCertificate != null)
        {
            try {
                CertificateFactory factory = SecurityFactoryFactory.getSecurityFactory().
                        createCertificateFactory("X.509");

                certificate = (X509Certificate) factory.generateCertificate(
                        new ByteArrayInputStream(encodedCertificate));
            }
            catch (CertificateException | NoSuchProviderException e) {
                throw new HibernateException(e);
            }
        }

        return certificate;
    }

    @Override
    public X509Certificate instantiate(ValueAccess valueAccess, SessionFactoryImplementor sessionFactoryImplementor) {
        return decodeCertificate(valueAccess.getValue(CERTIFICATE_COLUMN_INDEX, byte[].class));
    }

    @Override
    public Class<?> embeddable() {
        return X509CertificateMapper.class;
    }

    @Override
    public Class<X509Certificate> returnedClass() {
        return X509Certificate.class;
    }

    @Override
    public boolean equals(X509Certificate certificate1, X509Certificate certificate2)
    {
        if (certificate1 == certificate2) {
            return true;
        }

        if (certificate1 == null || certificate2 == null) {
            return false;
        }

        return certificate1.equals(certificate2);
    }

    @Override
    public int hashCode(X509Certificate certificate) {
        return certificate.hashCode();
    }

    @Override
    public X509Certificate deepCopy(X509Certificate certificate)
    {
        // According to javadoc:
        // It is not necessary to copy immutable objects, or null values, in which case it is safe to simply return the
        // argument.
        return certificate;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(X509Certificate certificate)
    {
        try {
            return certificate != null ? certificate.getEncoded() : null;
        }
        catch (CertificateEncodingException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public X509Certificate assemble(Serializable cached, Object owner) {
        return decodeCertificate((byte[]) cached);
    }

    @Override
    public X509Certificate replace(X509Certificate detached, X509Certificate managed, Object owner)
    {
        // According to javadoc:
        // For immutable objects, or null values, it is safe to simply return the first parameter.
        return detached;
    }

    /*
     * Helper function to provide a work-around (if enabled) for a Outlook 2010 'bug'. Outlook 2010
     * uses a non-standard way to encode the Subject Key Identifier (SKI) and adds an SKI to a CMS
     * Recipient Identifier even when the certificate used for encryption does not contain a SKI.
     * This is clearly non RFC compliant. A workaround is to always add the SKI to the database
     * when the certificate does not have a SKI.
     *
     * For info why this workaround is needed see: https://bugzilla.mozilla.org/show_bug.cgi?id=559243 and
     * http://www.ietf.org/mail-archive/web/smime/current/msg18730.html
     */
    private String getSubjectKeyIdentifierHex(X509Certificate certificate)
    throws IOException
    {
        String ski = X509CertificateInspector.getSubjectKeyIdentifierHex(certificate);

        if (ski == null && SecurityConstants.isOutlook2010SKIWorkaroundEnabled()) {
            ski = HexUtils.hexEncode(X509CertificateInspector.calculateSubjectKeyIdentifierMicrosoft(certificate));
        }

        return ski;
    }
}
