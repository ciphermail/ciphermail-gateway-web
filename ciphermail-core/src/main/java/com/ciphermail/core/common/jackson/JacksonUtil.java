/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.jackson;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.KeyPair;
import java.security.cert.X509Certificate;

public class JacksonUtil
{
    // this class should be used statically
    private JacksonUtil() {
        // empty on purpose
    }

    private static final ThreadLocal<ObjectMapper> objectMappers = ThreadLocal.withInitial(ObjectMapper::new);

    /**
     * Returns an ObjectMapper for the current thread
     */
    public static ObjectMapper getObjectMapper()
    {
        ObjectMapper objectMapper = objectMappers.get();

        // register our custom serializers/deserializers
        SimpleModule module = new SimpleModule();

        module.addSerializer(KeyPair.class, new KeyPairSerializer());
        module.addDeserializer(KeyPair.class, new KeyPairDeserializer());

        module.addSerializer(PKCS10CertificationRequest.class, new PKCS10CertificationRequestSerializer());
        module.addDeserializer(PKCS10CertificationRequest.class, new PKCS10CertificationRequestDeserializer());

        module.addSerializer(X509Certificate.class, new X509CertificateSerializer());
        module.addDeserializer(X509Certificate.class, new X509CertificateDeserializer());

        objectMapper.registerModule(module);

        return objectMapper;
    }

    /**
     * Pretty prints the json input string
     */
    public static String prettyPrintJSON(@Nonnull String json)
    throws IOException
    {
        // use a default ObjectMapper since we only want raw json
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        JsonParser parser = factory.createParser(json);
        JsonNode node = mapper.readTree(parser);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(node);
    }
}
