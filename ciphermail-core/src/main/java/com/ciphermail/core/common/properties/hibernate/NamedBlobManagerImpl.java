/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties.hibernate;

import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of NamedBlobManager.
 *
 * @author Martijn Brinkers
 *
 */
public class NamedBlobManagerImpl implements NamedBlobManager
{
    /*
     * Manages database sessions
     */
    private final SessionManager sessionManager;

    public NamedBlobManagerImpl(@Nonnull SessionManager sessionManager) {
        this.sessionManager = Objects.requireNonNull(sessionManager);
    }

    @Override
    public @Nonnull NamedBlob createNamedBlob(@Nonnull String category, @Nonnull String name)
    {
        NamedBlobEntity entity = new NamedBlobEntity();

        entity.setCategory(category);
        entity.setName(name);

        return getDAO().persist(entity);
    }

    @Override
    public void deleteNamedBlob(@Nonnull String category, @Nonnull String name) {
        getDAO().deleteNamedBlob(category, name);
    }

    @Override
    public List<? extends NamedBlob> getByCategory(@Nonnull String category, Integer firstResult, Integer maxResults) {
        return Collections.unmodifiableList(getDAO().getByCategory(category, firstResult, maxResults));
    }

    @Override
    public long getByCategoryCount(@Nonnull String category) {
        return getDAO().getByCategoryCount(category);
    }

    @Override
    public NamedBlob getNamedBlob(@Nonnull String category, @Nonnull String name) {
        return getDAO().getNamedBlob(category, name);
    }

    @Override
    public void deleteAll() {
        getDAO().deleteAll();
    }

    @Override
    public long getReferencedByCount(@Nonnull NamedBlob namedBlob)
    {
        if (!(namedBlob instanceof NamedBlobEntity namedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }

        return getDAO().getReferencedByCount(namedBlobEntity);
    }

    @Override
    public List<? extends NamedBlob> getReferencedBy(@Nonnull NamedBlob namedBlob, Integer firstResult,
            Integer maxResults)
    {
        if (!(namedBlob instanceof NamedBlobEntity namedBlobEntity)) {
            throw new IllegalArgumentException("namedBlob is not a NamedBlobEntity");
        }

        return getDAO().getReferencedBy(namedBlobEntity, firstResult, maxResults);
    }

    private NamedBlobDAO getDAO() {
        return NamedBlobDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }
}
