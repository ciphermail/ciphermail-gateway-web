/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crlstore.hibernate;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import com.ciphermail.core.common.hibernate.X509CRLUserType;
import com.ciphermail.core.common.security.crlstore.X509CRLStoreEntry;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.CompositeType;
import org.hibernate.annotations.UuidGenerator;

import java.security.cert.X509CRL;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Entity class for storing a CRL into a database.
 *
 * @author Martijn Brinkers
 *
 */
@Entity(name = X509CRLStoreEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={X509CRLStoreEntity.STORE_NAME_COLUMN,
        X509CRLStoreEntity.THUMBPRINT_COLUMN})},
indexes = {
        @Index(name = "crl_store_name_index", columnList = X509CRLStoreEntity.STORE_NAME_COLUMN),
        @Index(name = "crl_issuer_index",     columnList = X509CRLStoreEntity.ISSUER_COLUMN),
        @Index(name = "crl_crlnumber_index",  columnList = X509CRLStoreEntity.CRL_NUMBER_COLUMN),
        @Index(name = "crl_thumbprint_index", columnList = X509CRLStoreEntity.THUMBPRINT_COLUMN)
}
)
public class X509CRLStoreEntity implements X509CRLStoreEntry
{
    static final String ENTITY_NAME = "CRL";

    /**
     * Name of columns which are referenced from other classes
     */
    public static final String STORE_NAME_COLUMN = "storeName";
    public static final String ISSUER_COLUMN =  X509CRLUserType.ISSUER_COLUMN_NAME;
    public static final String NEXT_UPDATE_COLUMN = X509CRLUserType.NEXT_UPDATE_COLUMN_NAME;
    public static final String THIS_UPDATE_COLUMN = X509CRLUserType.THIS_UPDATE_COLUMN_NAME;
    public static final String CRL_NUMBER_COLUMN = X509CRLUserType.CRL_NUMBER_COLUMN_NAME;
    public static final String THUMBPRINT_COLUMN = X509CRLUserType.THUMBPRINT_COLUMN_NAME;

    /*
     * Name of the X509CRLUserType
     */
    public static final String CRL_USER_TYPE_NAME = "crl";

    protected X509CRLStoreEntity() {
        // Hibernate requires a default constructor
    }

    public X509CRLStoreEntity(X509CRL crl, String storeName, Date creationDate)
    {
        this.crl = Objects.requireNonNull(crl);
        this.storeName = storeName;
        this.creationDate = creationDate;
    }

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /*
     * The store name will be used to store multiple certificate stores in the database
     */
    @Column (name = STORE_NAME_COLUMN, length = 255, unique = false, nullable = true)
    private String storeName;

    @Embedded
    @AttributeOverride(name = X509CRLUserType.CRL_COLUMN_NAME, column = @Column(length = SizeUtils.GB))
    @AttributeOverride(name = X509CRLUserType.ISSUER_COLUMN_NAME, column = @Column(length = SizeUtils.KB * 32))
    // The maximum length of a serial number. Should be at least 20 octets
    // (see http://www.ietf.org/rfc/rfc3280.txt appendix B)
    @AttributeOverride(name = X509CRLUserType.CRL_NUMBER_COLUMN_NAME, column = @Column(length = SizeUtils.KB))
    @AttributeOverride(name = X509CRLUserType.THUMBPRINT_COLUMN_NAME, column = @Column(length = 255))
    @CompositeType(X509CRLUserType.class)
    private X509CRL crl;

    /*
     * Date this entry was created
     */
    @Column (name = "creationDate", unique = false, nullable = true)
    private Date creationDate;

    @Override
    public String getStoreName() {
        return storeName;
    }

    @Override
    public X509CRL getCRL() {
        return crl;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * X509CRLStoreEntry is equal if and only if the CRLs and storeNames are equal
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof X509CRLStoreEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(crl, rhs.crl)
            .append(storeName, rhs.storeName)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(crl)
            .append(storeName)
            .toHashCode();
    }
}
