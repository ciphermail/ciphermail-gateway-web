/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.tools;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.util.HexUtils;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Attribute;
import org.apache.mailet.AttributeName;
import org.apache.mailet.Mail;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Tool which can be used to inspect and manipulate mail FileObjectStore files.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings({"java:S106", "java:S112"})
public class MailUtil
{
    private static final String COMMAND_NAME = MailUtil.class.getName();

    /*
     * File extension for Mail object files
     */
    public static final String MAIL_OBJECT_FILE_EXTENSION = "FileObjectStore";

    /*
     * File extension for Mail MIME stream files
     */
    public static final String MAIL_MIME_FILE_EXTENSION = "FileStreamStore";

    /*
     * The command line parameters
     */
    private CommandLine commandLine;

    /*
     * Object file input
     */
    private static final String FILE_OPTION = "file";

    /*
     * Directory from which files will be read
     */
    private static final String DIRECTORY_OPTION = "directory";

    /*
     * If true, the encoded name of the Mail item will be printed
     */
    private static final String ENCODE_NAME_OPTION = "encode-name";

    /*
     * If true, the input file will be renamed to match the correct encoded name
     */
    private static final String FIX_NAME_OPTION = "fix-name";

    /*
     * If true, the input file will be checked
     */
    private static final String VERIFY_OPTION = "verify";

    /*
     * Option for delete a Mail attribute
     */
    private static final String DELETE_ATTRIBUTE_OPTION = "delete-attribute";

    /*
     * Option for setting the Mail envelope recipients
     */
    private static final String SET_RECIPIENTS_OPTION = "set-recipients";

    /*
     * Option for setting the Mail envelope sender
     */
    private static final String SET_SENDER_OPTION = "set-sender";

    /*
     * Option for setting a String Mail attribute
     */
    private static final String SET_STRING_ATTRIBUTE_OPTION = "set-string-attribute";

    /*
     * Option for setting a Boolean Mail attribute
     */
    private static final String SET_BOOLEAN_ATTRIBUTE_OPTION = "set-boolean-attribute";

    /*
     * Option for setting an Integer Mail attribute
     */
    private static final String SET_INTEGER_ATTRIBUTE_OPTION = "set-integer-attribute";

    /*
     * Option for a value
     */
    private static final String VALUE_OPTION = "value";

    /*
     * The CLI option for showing help
     */
    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";

    /*
     * The input file
     */
    private File inFile;

    /*
     * The directory to read files from
     */
    private File directory;

    private Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(FILE_OPTION).argName("file").hasArg().
                desc("Mail FileObjectStore file").build());

        options.addOption(Option.builder().longOpt(DIRECTORY_OPTION).argName("dir").hasArg().
                desc("Directory from which files will be read").build());

        options.addOption(Option.builder().longOpt(ENCODE_NAME_OPTION).
                desc("If set, the name of the Mail object will be printed in encoded form").build());

        options.addOption(Option.builder().longOpt(FIX_NAME_OPTION).
                desc("If set, the Object and Stream file will be renamed to match the encoded " +
                     "form of the Mail name").build());

        options.addOption(Option.builder().longOpt(VERIFY_OPTION).
                desc("If set, the Object and Stream file will be verified. " +
                     "Only invalid files will be reported").build());

        options.addOption(Option.builder().longOpt(DELETE_ATTRIBUTE_OPTION).argName("attribute-name").hasArg().
                desc("Delete an attribute from the Mail object").build());

        options.addOption(Option.builder().longOpt(SET_RECIPIENTS_OPTION).argName("email").hasArg().
                desc("Sets the envelope recipients").build());

        options.addOption(Option.builder().longOpt(SET_SENDER_OPTION).argName("email").hasArg().
                desc("Sets the envelope sender").build());

        options.addOption(Option.builder().longOpt(SET_STRING_ATTRIBUTE_OPTION).argName("attribute-name").hasArg().
                desc("Sets a String attribute").build());

        options.addOption(Option.builder().longOpt(SET_BOOLEAN_ATTRIBUTE_OPTION).argName("attribute-name").hasArg().
                desc("Sets a Boolean attribute").build());

        options.addOption(Option.builder().longOpt(SET_INTEGER_ATTRIBUTE_OPTION).argName("attribute-name").hasArg().
                desc("Sets an Integer attribute").build());

        options.addOption(Option.builder().longOpt(VALUE_OPTION).argName("attribute-value").hasArg().
                desc("Mail attribute value to set").build());

        return options;
    }

    private void validateInFile()
    {
        if (!inFile.exists()) {
            throw new IllegalArgumentException(inFile + " does not exist.");
        }

        if (!inFile.isFile()) {
            throw new IllegalArgumentException(inFile + " is not a file.");
        }

        if (!inFile.canRead()) {
            throw new IllegalArgumentException(inFile + " cannot be read.");
        }
    }

    private MailImpl loadMail(File file)
    throws IOException
    {
        return (MailImpl) SerializationUtils.deserialize(FileUtils.readFileToByteArray(file));
    }

    private File[] getMailObjectFiles()
    throws MissingArgumentException
    {
        if (directory == null)
        {
            throw new MissingArgumentException("Either --" + FILE_OPTION + " or --" +
                    DIRECTORY_OPTION + " option is required.");
        }

        return directory.listFiles((FilenameFilter) new SuffixFileFilter("." + MAIL_OBJECT_FILE_EXTENSION));
    }

    private void saveMail(Mail mail, File file)
    throws IOException
    {
        File newFile = new File(file.getPath() + ".new");

        OutputStream objectOutputStream = new BufferedOutputStream(new FileOutputStream(newFile));

        try {
            SerializationUtils.serialize(mail, objectOutputStream);
        }
        finally {
            IOUtils.closeQuietly(objectOutputStream);
        }

        Path newPath = Paths.get(newFile.getPath());

        Files.move(newPath, newPath.resolveSibling(file.getName()), StandardCopyOption.ATOMIC_MOVE);
    }

    /*
     * Used by #handleMultipleFiles. Caller should be implemented to call the actual method.
     */
    interface MultipleFilesExecuter
    {
        void execute(File file)
        throws IOException;
    }

    private void handleMultipleFiles(MultipleFilesExecuter executer)
    throws MissingArgumentException
    {
        File[] files = getMailObjectFiles();

        int error = 0;

        if (files != null)
        {
            for (File file : files)
            {
                try {
                    executer.execute(file);
                }
                catch (Exception e)
                {
                    System.err.println("Error handling file " + file.getName() +
                            ". Message: " + ExceptionUtils.getRootCauseMessage(e));

                    error++;
                }
            }
        }

        System.out.println("Total files handled: " + ArrayUtils.getLength(files));
        System.out.println("Total files failed: " + error);

        if (error > 0) {
            System.exit(1);
        }
    }

    private void printDetails(File file)
    throws IOException
    {
        Mail mail = loadMail(file);

        StrBuilder sb = new StrBuilder();

        sb.append("[ENVELOPE]").appendNewLine().appendNewLine();
        sb.append("Name: ").append(mail.getName()).appendNewLine();
        sb.append("Recipients: ").appendWithSeparators(mail.getRecipients(), ", ").appendNewLine();
        sb.append("Sender: ").append(mail.getMaybeSender().asOptional().orElse(null)).appendNewLine();
        sb.append("State: ").append(mail.getState()).appendNewLine();
        sb.append("Remote Addr: ").append(mail.getRemoteAddr()).appendNewLine();
        sb.append("Error message: ").append(mail.getErrorMessage()).appendNewLine();
        sb.append("Last updated: ").append(mail.getLastUpdated()).appendNewLine();
        sb.appendNewLine();
        sb.appendln("[ATTRIBUTES]").appendNewLine();

        List<AttributeName> attributeNames = mail.attributeNames().toList();

        for (AttributeName attributeName : attributeNames)
        {
            Optional<Attribute> attributeValue = mail.getAttribute(attributeName);

            sb.append(attributeName + ": ").append(attributeValue)
                .appendNewLine();
        }

        sb.appendNewLine();

        System.out.print(sb.toString());
    }

    private void printDetails()
    throws IOException, MissingArgumentException
    {
        if (inFile != null) {
            printDetails(inFile);
        }
        else {
            handleMultipleFiles(this::printDetails);
        }
    }

    private void printEncodedName(File file)
    throws IOException
    {
        System.out.println(HexUtils.hexEncode(loadMail(file).getName().getBytes()));
    }

    private void printEncodedName()
    throws IOException, MissingArgumentException
    {
        if (inFile != null) {
            printEncodedName(inFile);
        }
        else {
            handleMultipleFiles(this::printEncodedName);
        }
    }

    private boolean fixName(File file)
    throws IOException
    {
        // Only accept filename if it ends with FileObjectStore
        if (!MAIL_OBJECT_FILE_EXTENSION.equals(FilenameUtils.getExtension(file.getName()))) {
            throw new IllegalArgumentException(file + " must end with ." + MAIL_OBJECT_FILE_EXTENSION);
        }

        Mail mail = loadMail(file);

        String baseFile = FilenameUtils.removeExtension(file.getPath());

        // There should also be a matching FileStreamStore file
        File mimeFile = new File(baseFile + "." + MAIL_MIME_FILE_EXTENSION);

        if (!mimeFile.exists())
        {
            System.err.println("MIME file " + mimeFile + " does not exist");

            return false;
        }

        if (!mimeFile.canRead())
        {
            System.err.println("MIME file " + mimeFile + " cannot be read");

            return false;
        }

        String newName = HexUtils.hexEncode(mail.getName().getBytes());

        if (FilenameUtils.getName(baseFile).equals(newName + ".Repository"))
        {
            System.err.println("Filename already fixed: " + newName);

            return true;
        }

        String newPath = FilenameUtils.getFullPath(baseFile);

        File newObjectFile = new File(newPath, newName + ".Repository." + MAIL_OBJECT_FILE_EXTENSION);

        Path path = Paths.get(file.getPath());

        Files.move(path, path.resolveSibling(newObjectFile.getName()), StandardCopyOption.ATOMIC_MOVE);

        System.out.println(file.getName() + " renamed to " + newObjectFile.getName());

        File newMimeFile = new File(newPath, newName + ".Repository." + MAIL_MIME_FILE_EXTENSION);

        path = Paths.get(mimeFile.getPath());

        Files.move(path, path.resolveSibling(newMimeFile.getName()), StandardCopyOption.ATOMIC_MOVE);

        System.out.println(mimeFile.getName() + " renamed to " + newMimeFile.getName());

        return true;
    }

    private void fixName()
    throws IOException, MissingArgumentException
    {
        if (inFile != null)
        {
            if (!fixName(inFile)) {
                System.exit(1);
            }
        }
        else {
            File[] files = getMailObjectFiles();

            int error = 0;

            if (files != null)
            {
                for (File file : files)
                {
                    try {
                        if (!fixName(file)) {
                            error++;
                        }
                    }
                    catch (Exception e)
                    {
                        System.err.println("Error fixing file. File: " + file.getName() +
                                ". Message: " + ExceptionUtils.getRootCauseMessage(e));

                        error++;
                    }
                }
            }

            System.out.println("Total files handled: " + ArrayUtils.getLength(files));
            System.out.println("Total files failed: " + error);

            if (error > 0) {
                System.exit(1);
            }
        }
    }

    private void deleteAttribute(File file)
    throws IOException
    {
        Mail mail = loadMail(file);

        String attributeName = commandLine.getOptionValue(DELETE_ATTRIBUTE_OPTION);

        mail.removeAttribute(AttributeName.of(attributeName));

        saveMail(mail, file);

        System.out.println("Attribute deleted: " + attributeName + ". File: " + file.getName());
    }

    private void deleteAttribute()
    throws IOException, MissingArgumentException
    {
        if (inFile != null) {
            deleteAttribute(inFile);
        }
        else {
            handleMultipleFiles(this::deleteAttribute);
        }
    }

    private boolean verify(File file)
    throws IOException
    {
        loadMail(file);

        String baseFile = FilenameUtils.removeExtension(file.getPath());

        // There should also be a matching FileStreamStore file
        File mimeFile = new File(baseFile + "." + MAIL_MIME_FILE_EXTENSION);

        if (!mimeFile.exists())
        {
            System.err.println("The MIME file " + mimeFile.getName() + " does not exist");

            return false;
        }

        if (!mimeFile.canRead())
        {
            System.err.println("The MIME file " + mimeFile.getName() + " cannot be read");

            return false;
        }

        try {
            MailUtils.loadMessage(mimeFile);
        }
        catch (Exception e)
        {
            System.err.println("The MIME file cannot be loaded. File " + file.getName() +
                    ". Message: " + ExceptionUtils.getRootCauseMessage(e));

            return false;
        }

        return true;
    }

    private void verify()
    throws IOException, MissingArgumentException
    {
        if (inFile != null) {
            verify(inFile);
        }
        else {
            handleMultipleFiles(this::verify);
        }
    }

    private void setRecipients(File file)
    throws IOException
    {
        Mail mail = loadMail(file);

        List<MailAddress> recipients = new LinkedList<>();

        for (String recipient : StringUtils.split(commandLine.getOptionValue(SET_RECIPIENTS_OPTION), ','))
        {
            recipient = StringUtils.trimToNull(recipient);

            if (!EmailAddressUtils.isValid(recipient)) {
                throw new IllegalArgumentException("Email address " + recipient + " is not a valid email address");
            }

            try {
                recipients.add(new MailAddress(recipient));
            }
            catch (javax.mail.internet.ParseException e) {
                throw new IOException(e);
            }
        }

        if (recipients.isEmpty()) {
            throw new IllegalArgumentException("No recipients were specified");
        }

        mail.setRecipients(recipients);

        saveMail(mail, file);

        System.out.println("Recipients set: " + recipients + ". File: " + file.getName());
    }

    private void setRecipients()
    throws IOException, MissingArgumentException
    {
        if (inFile != null) {
            setRecipients(inFile);
        }
        else {
            handleMultipleFiles(this::setRecipients);
        }
    }

    private void setSender(File file)
    throws IOException
    {
        MailImpl mail = loadMail(file);

        MailAddress sender;

        String email = StringUtils.trimToNull(commandLine.getOptionValue(SET_SENDER_OPTION));

        try {
            sender = email != null ? new MailAddress(email) : null;
        }
        catch (javax.mail.internet.ParseException e) {
            throw new IOException(e);
        }

        mail.setSender(sender);

        saveMail(mail, file);

        System.out.println("Sender set: " + sender + ". File: " + file.getName());
    }

    private void setSender()
    throws IOException, MissingArgumentException
    {
        String email = StringUtils.trimToNull(commandLine.getOptionValue(SET_SENDER_OPTION));

        if (email != null && !EmailAddressUtils.isValid(email)) {
            throw new IllegalArgumentException("Email address " + email + " is not a valid email address");
        }

        if (inFile != null) {
            setSender(inFile);
        }
        else {
            handleMultipleFiles(this::setSender);
        }
    }

    private void setStringAttribute(File file)
    throws IOException
    {
        MailImpl mail = loadMail(file);

        String attribute = StringUtils.trimToNull(commandLine.getOptionValue(SET_STRING_ATTRIBUTE_OPTION));

        String value = commandLine.getOptionValue(VALUE_OPTION);

        mail.setAttribute(attribute, value);

        saveMail(mail, file);

        System.out.println("Attribute set. Attribute: " + attribute + ", Value: " + value + ". File: " + file.getName());
    }

    private void setStringAttribute()
    throws IOException, MissingArgumentException
    {
        if (!commandLine.hasOption(VALUE_OPTION)) {
            throw new MissingArgumentException(VALUE_OPTION);
        }

        if (inFile != null) {
            setStringAttribute(inFile);
        }
        else {
            handleMultipleFiles(this::setStringAttribute);
        }
    }

    private void setBooleanAttribute(File file)
    throws IOException
    {
        MailImpl mail = loadMail(file);

        String attribute = StringUtils.trimToNull(commandLine.getOptionValue(SET_BOOLEAN_ATTRIBUTE_OPTION));

        boolean value = BooleanUtils.toBoolean(commandLine.getOptionValue(VALUE_OPTION));

        mail.setAttribute(attribute, value);

        saveMail(mail, file);

        System.out.println("Attribute set. Attribute: " + attribute + ", Value: " + value + ". File: " + file.getName());
    }

    private void setBooleanAttribute()
    throws IOException, MissingArgumentException
    {
        if (!commandLine.hasOption(VALUE_OPTION)) {
            throw new MissingArgumentException(VALUE_OPTION);
        }

        if (inFile != null) {
            setBooleanAttribute(inFile);
        }
        else {
            handleMultipleFiles(this::setBooleanAttribute);
        }
    }

    private void setIntegerAttribute(File file, Integer value)
    throws IOException
    {
        MailImpl mail = loadMail(file);

        String attribute = StringUtils.trimToNull(commandLine.getOptionValue(SET_INTEGER_ATTRIBUTE_OPTION));

        mail.setAttribute(attribute, value);

        saveMail(mail, file);

        System.out.println("Attribute set. Attribute: " + attribute + ", Value: " + value + ". File: " + file.getName());
    }

    private void setIntegerAttribute()
    throws IOException, MissingArgumentException
    {
        if (!commandLine.hasOption(VALUE_OPTION)) {
            throw new MissingArgumentException(VALUE_OPTION);
        }

        final Integer value = Integer.parseInt(commandLine.getOptionValue(VALUE_OPTION));

        if (inFile != null) {
            setIntegerAttribute(inFile, value);
        }
        else {
            handleMultipleFiles(file -> setIntegerAttribute(file, value));
        }
    }

    private void handleCommandline(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, true);

            throw e;
        }

        if (commandLine.hasOption(FILE_OPTION))
        {
            inFile = new File(commandLine.getOptionValue(FILE_OPTION));

            validateInFile();
        }

        if (commandLine.hasOption(DIRECTORY_OPTION)) {
            directory = new File(commandLine.getOptionValue(DIRECTORY_OPTION));
        }

        if (commandLine.getOptions().length == 0)
        {
            formatter.printHelp(COMMAND_NAME, options, true);

            System.exit(1);
        }
        if (commandLine.hasOption(HELP_OPTION_SHORT) || commandLine.hasOption(HELP_OPTION_LONG)) {
            formatter.printHelp(COMMAND_NAME, options, true);
        }
        else if (commandLine.hasOption(ENCODE_NAME_OPTION)) {
            printEncodedName();
        }
        else if (commandLine.hasOption(FIX_NAME_OPTION)) {
            fixName();
        }
        else if (commandLine.hasOption(DELETE_ATTRIBUTE_OPTION)) {
            deleteAttribute();
        }
        else if (commandLine.hasOption(VERIFY_OPTION)) {
            verify();
        }
        else if (commandLine.hasOption(SET_RECIPIENTS_OPTION)) {
            setRecipients();
        }
        else if (commandLine.hasOption(SET_SENDER_OPTION)) {
            setSender();
        }
        else if (commandLine.hasOption(SET_STRING_ATTRIBUTE_OPTION)) {
            setStringAttribute();
        }
        else if (commandLine.hasOption(SET_BOOLEAN_ATTRIBUTE_OPTION)) {
            setBooleanAttribute();
        }
        else if (commandLine.hasOption(SET_INTEGER_ATTRIBUTE_OPTION)) {
            setIntegerAttribute();
        }
        else {
            printDetails();
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        MailUtil tool = new MailUtil();

        try {
            tool.handleCommandline(args);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
        catch (ParseException e)
        {
            System.err.println("Command line parsing error. " + e.getMessage());

            System.exit(4);
        }
    }
}
