/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

/**
 * A Collection class, wrapping an existing Collection, that sends out events on add and remove.
 *
 * @author Martijn Brinkers
 *
 * @param <E>
 */
public class ObservableCollection<E>  implements Collection<E>
{
    private static final Logger logger = LoggerFactory.getLogger(ObservableCollection.class);

    public interface Event<T> {
        boolean event(T item)
        throws ObservableRuntimeException;
    }

    private final Collection<E> source;

    private Event<E> addEvent;
    private Event<Object> removeEvent;

    public ObservableCollection(@Nonnull Collection<E> sourceCollection) {
        this.source = Objects.requireNonNull(sourceCollection);
    }

    public void setAddEvent(Event<E> event) {
        this.addEvent = event;
    }

    public void setRemoveEvent(Event<Object> event) {
        this.removeEvent = event;
    }

    @Override
    public boolean add(E item)
    {
        boolean added = fireAddEvent(item);

        if (added) {
            added = source.add(item);

            if (!added)
            {
                // Can be a false positive if fireAddEvent always returns true
                logger.debug("Collections possibly out of sync (add).");
            }
        }
        return added;
    }

    @Override
    public boolean addAll(Collection<? extends E> items)
    {
        boolean added = false;

        for (E item : items)
        {
            if (add(item)) {
                added = true;
            }
        }

        return added;
    }

    @Override
    public void clear()
    {
        Iterator<E> it = iterator();

        while (it.hasNext())
        {
            it.next();
            it.remove();
        }
    }

    @Override
    public boolean contains(Object item) {
        return source.contains(item);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> items)
    {
        return source.containsAll(items);
    }

    @Override
    public boolean isEmpty() {
        return source.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return new ItemIterator(source.iterator());
    }

    @Override
    public boolean remove(Object item)
    {
        boolean removed = fireRemoveEvent(item);

        if (removed) {
            removed = source.remove(item);

            if (!removed)
            {
                // Can be a false positive if fireRemoveEvent always returns true
                logger.debug("Collections possibly out of sync (remove).");
            }
        }
        return removed;
    }

    @Override
    public boolean removeAll(@Nonnull Collection<?> items)
    {
        boolean removed = false;

        for (Object item : items)
        {
            if (remove(item)) {
                removed = true;
            }
        }

        return removed;
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> items)
    {
        boolean removed = false;

        Iterator<E> it = iterator();

        while (it.hasNext())
        {
            E item = it.next();

            if (!items.contains(item))
            {
                it.remove();

                removed = true;
            }
        }

        return removed;
    }

    @Override
    public int size() {
        return source.size();
    }

    @Override
    public Object[] toArray() {
        return source.toArray();
    }

    @Override
    public <T> T[] toArray(@Nonnull T[] items) {
        return source.toArray(items);
    }

    private boolean fireAddEvent(E item)
    {
        boolean added = true;

        if (addEvent != null) {
            added = addEvent.event(item);
        }

        return added;
    }

    private boolean fireRemoveEvent(Object item)
    {
        boolean removed = true;

        if (removeEvent != null) {
            removed = removeEvent.event(item);
        }

        return removed;
    }

    private class ItemIterator implements Iterator<E>
    {
        private final Iterator<E> source;

        private E next;

        public ItemIterator(Iterator<E> source) {
            this.source = source;
        }

        @Override
        public boolean hasNext() {
            return source.hasNext();
        }

        @Override
        public E next()
        {
            next = source.next();

            return next;
        }

        @Override
        public void remove()
        {
            if (fireRemoveEvent(next)) {
                source.remove();
            }
        }
    }
}
