/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore.jce;

import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.keystore.KeyStoreRuntimeException;
import com.ciphermail.core.common.security.keystore.dao.KeyStoreDAO;
import com.ciphermail.core.common.security.keystore.hibernate.KeyStoreEntity;
import com.ciphermail.core.common.security.keystore.hibernate.SerializableKeyEntry;
import com.ciphermail.core.common.security.password.PBEncryption;
import org.apache.commons.collections.iterators.IteratorEnumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Objects;

/**
 * KeyStoreSpi implementation which stores the key's in a database.
 *
 * @author Martijn Brinkers
 *
 */
public class KeyStoreHibernate extends KeyStoreSpi
{
    private static final Logger logger = LoggerFactory.getLogger(KeyStoreHibernate.class);

    /*
     * Manager for getting the Hibernate session
     */
    private SessionManager sessionManager;

    /*
     * Is used for encrypting the raw key
     */
    private final PBEncryption encryptor;

    /*
     * Name of this store. Default when the name is not set.
     */
    private String storeName = "default";

    /*
     * The security factory to use to create the key from the raw key material. If null, the default security factory
     * will be used.
     */
    private SecurityFactory securityFactory;

    public KeyStoreHibernate(@Nonnull SessionManager sessionManager, PBEncryption encryptor)
    {
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.encryptor = encryptor;
    }

    private KeyStoreDAO getDAO() {
        return new KeyStoreDAO(storeName, sessionManager.getSession());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Enumeration<String> engineAliases()
    throws KeyStoreRuntimeException
    {
        return new IteratorEnumeration(getDAO().getAliases().iterator());
    }

    @Override
    public boolean engineContainsAlias(final String alias)
    throws KeyStoreRuntimeException
    {
        return getDAO().getEntryByAlias(alias) != null;
    }

    @Override
    public void engineDeleteEntry(final String alias)
    {
        logger.debug("#engineDeleteEntry. Alias: {}", alias);

        KeyStoreDAO dao = getDAO();

        KeyStoreEntity entry = dao.getEntryByAlias(alias);

        if (entry != null) {
            dao.delete(entry);
        }
    }

    @Override
    public Certificate engineGetCertificate(final String alias)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineGetCertificate. Alias: {}", alias);

        KeyStoreEntity entry = getDAO().getEntryByAlias(alias);

        return entry != null ? entry.getCertificate() : null;
    }

    @Override
    public String engineGetCertificateAlias(final Certificate certificate)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineGetCertificateAlias. Certificate: {}", certificate);

        try {
            KeyStoreEntity entry = getDAO().getEntryByCertificate(certificate);

            return entry != null ? entry.getKeyAlias() : null;
        }
        catch (CertificateEncodingException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new KeyStoreRuntimeException(e);
        }
    }

    @Override
    public Certificate[] engineGetCertificateChain(final String alias)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineGetCertificateChain. Alias: {}", alias);

        KeyStoreEntity entry = getDAO().getEntryByAlias(alias);

        return entry != null ? entry.getChain() : null;
    }

    @Override
    public Date engineGetCreationDate(final String alias)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineGetCreationDate. Alias: {}", alias);

        KeyStoreEntity entry = getDAO().getEntryByAlias(alias);

        return entry != null ? entry.getCreationDate() : null;
    }

    @Override
    public Key engineGetKey(final String alias, char[] password)
    throws UnrecoverableKeyException
    {
        logger.debug("#engineGetKey. Alias: {}", alias);

        KeyStoreEntity entry;

        try {
            entry = getDAO().getEntryByAlias(alias);

            if (entry == null) {
                return null;
            }

            SerializableKeyEntry serializableKeyEntry = SerializableKeyEntry.deserialize(entry.getEncodedKey(),
                    securityFactory);

            return password != null ? serializableKeyEntry.getKey(password, encryptor) : serializableKeyEntry.getKey();
        }
        catch (IOException | GeneralSecurityException e)
        {
            UnrecoverableKeyException ex = new UnrecoverableKeyException("Error getting key");

            ex.initCause(e);

            throw ex;
        }
    }

    @Override
    public boolean engineIsCertificateEntry(final String alias)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineIsCertificateEntry. Alias: {}", alias);

        KeyStoreEntity entry = getDAO().getEntryByAlias(alias);

        if (entry == null) {
            return false;
        }

        return entry.getEncodedKey() == null;
    }

    @Override
    public boolean engineIsKeyEntry(final String alias)
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineIsKeyEntry. Alias: {}", alias);

        KeyStoreEntity entry = getDAO().getEntryByAlias(alias);

        if (entry == null) {
            return false;
        }

        return entry.getEncodedKey() != null;
    }

    @Override
    public void engineLoad(InputStream stream, char[] password)
    {
        logger.debug("#engineLoad");

        // not implemented
    }

    @Override
    public void engineLoad(KeyStore.LoadStoreParameter loadStoreParameter)
    throws IOException
    {
        logger.debug("#engineLoad");

        if (loadStoreParameter != null)
        {
            if (!(loadStoreParameter instanceof DatabaseKeyStoreLoadStoreParameter databaseKeyStoreLoadStoreParameter)) {
                throw new IOException("loadStoreParameter must be an DatabaseKeyStoreLoadStoreParameter.");
            }

            this.storeName = databaseKeyStoreLoadStoreParameter.getStoreName();

            if (databaseKeyStoreLoadStoreParameter.getSessionManager() != null) {
                this.sessionManager = databaseKeyStoreLoadStoreParameter.getSessionManager();
            }

            this.securityFactory = databaseKeyStoreLoadStoreParameter.getSecurityFactory();
        }
    }

    @Override
    public void engineSetCertificateEntry(final String alias, final Certificate certificate)
    {
        logger.debug("#engineSetCertificateEntry. Alias: {}, Certificate: {}", alias, certificate);

        KeyStoreDAO dao = getDAO();

        KeyStoreEntity entry = dao.getEntryByAlias(alias);

        if (entry == null)
        {
            entry = new KeyStoreEntity(storeName, alias, new Date());
            dao.persist(entry);
        }

        entry.setEncodedKey(null);
        entry.setChain(null);
        entry.setCertificate(certificate);
    }

    @Override
    public void engineSetKeyEntry(String alias, byte[] key, Certificate[] chain)
    throws KeyStoreException
    {
        logger.debug("#engineSetKeyEntry");

        throw new KeyStoreException("Not supported.");
    }

    @Override
    public void engineSetKeyEntry(final String alias, final Key key, final char[] password,
            final Certificate[] chain)
    throws KeyStoreException
    {
        logger.debug("#engineSetKeyEntry. Alias: {}", alias);

        KeyStoreDAO dao = getDAO();

        SerializableKeyEntry serializable;

        try {
            serializable = SerializableKeyEntry.createInstance(key, password, encryptor);

            serializable.setSecurityFactory(securityFactory);

            Certificate certificate = null;

            if (chain != null && chain.length > 0) {
                certificate = chain[0];
            }

            KeyStoreEntity entry = dao.getEntryByAlias(alias);

            if (entry == null)
            {
                entry = new KeyStoreEntity(storeName, alias, new Date());
                dao.persist(entry);
            }

            entry.setEncodedKey(serializable.serialize());
            entry.setChain(chain);
            entry.setCertificate(certificate);
        }
        catch (GeneralSecurityException | IOException e) {
            throw new KeyStoreException("Error setting key entry", e);
        }
    }

    @Override
    public int engineSize()
    throws KeyStoreRuntimeException
    {
        logger.debug("#engineSize");

        return (int) getDAO().getEntryCount();
    }

    @Override
    public void engineStore(OutputStream stream, char[] password)
    throws IOException
    {
        logger.debug("#engineStore");

        throw new IOException("Not supported.");
    }
}
