/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ctl;

import javax.annotation.Nonnull;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * A Certificate Trust List (CTL) contains entries that determine whether a certificate
 * is valid or not. This can be used as a certificate white- or blacklist. The main
 * difference between a CTL and a CRL is that a CRL is issued by the issuer of a certificate
 * whereas a CTL is 'issued' by the user of a certificate.
 *
 * @author Martijn Brinkers
 *
 */
public interface CTL
{
    /**
     * A CTL has a certain name.
     */
    String getName();

    /**
     * Returns all the CTL entries.
     */
    List<? extends CTLEntry> getEntries(Integer firstResult, Integer maxResults)
    throws CTLException;

    /**
     * Returns the number of CTL entries.
     */
    long size()
    throws CTLException;

    /**
     * Removes all entries
     */
    void deleteAll()
    throws CTLException;

    /**
     * Return the CRL entry with the given thumbprint. Returns null if there is no CTL entry
     * with the given thumbprint.
     */
    CTLEntry getEntry(@Nonnull String thumbprint)
    throws CTLException;

    /**
     * Return the CRL entry with the given certificate thumbprint. Returns null if there is no CTL entry
     * with the given thumbprint.
     *
     * This is a convenience overload of getCTLEntry(String)
     */
    CTLEntry getEntry(@Nonnull X509Certificate certificate)
    throws CTLException;

    /**
     * Creates a new CTL entry. The CRL entry is only created but not added.
     */
    @Nonnull CTLEntry createEntry(@Nonnull String thumbprint)
    throws CTLException;

    /**
     * Creates a new CTL entry. The CRL entry is only created but not added.
     *
     * This is a convenience overload of createCTLEntry(String)
     */
    CTLEntry createEntry(@Nonnull X509Certificate certificate)
    throws CTLException;

    /**
     * Adds the CTL entry.
     */
    void addEntry(@Nonnull CTLEntry entry)
    throws CTLException;

    /**
     * Removes the CTL entry.
     */
    void deleteEntry(@Nonnull CTLEntry entry)
    throws CTLException;

    /**
     * Returns the status of the certificate. Whether a certificate is valid depends
     * on whether a CTL entry is found for the certificate and on the settings of the
     * CRL entry.
     */
    CTLValidityResult checkValidity(@Nonnull X509Certificate certificate)
    throws CTLException;
}
