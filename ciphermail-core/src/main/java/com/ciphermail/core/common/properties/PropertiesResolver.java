/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Helpers for resolving special property values. For example a property value that starts with file: will be
 * replaced by the file content
 *
 * @author Martijn Brinkers
 *
 */
public class PropertiesResolver
{
    /*
     * The classpath schema name
     */
    private static final String CLASSPATH_SCHEMA = "classpath:";

    /*
     * The file schema name
     */
    private static final String FILE_SCHEMA = "file://";

    /*
     * The literal schema name
     */
    private static final String LITERAL_SCHEMA = "literal:";

    /*
     * The base directory used when resolving files
     */
    private File baseDir = null;

    public static class PropertiesResolverException extends Exception {
        public PropertiesResolverException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Resolves special property value types (for example a property that starts with file:// is converted to
     * the actual file content).
     * <p>
     * Currently only file: binding is supported
     */
    public void resolve(Properties properties)
    throws PropertiesResolverException
    {
        Enumeration<?> propertyNamesEnum = properties.propertyNames();

        while (propertyNamesEnum.hasMoreElements())
        {
            Object obj = propertyNamesEnum.nextElement();

            if (!(obj instanceof String key)) {
                continue;
            }

            String value = properties.getProperty(key);

            if (value == null) {
                continue;
            }

            if (value.startsWith(CLASSPATH_SCHEMA))
            {
                try {
                    properties.setProperty(key, new ClassPathResource(StringUtils.substringAfter(
                            value, CLASSPATH_SCHEMA)).getContentAsString(StandardCharsets.UTF_8));
                }
                catch (IOException e) {
                    throw new PropertiesResolverException("Unable to resolve classpath resource " + value, e);
                }
            }
            else if (value.startsWith(FILE_SCHEMA))
            {
                File file = new File(baseDir, StringUtils.substringAfter(value, FILE_SCHEMA));

                try {
                    properties.setProperty(key, FileUtils.readFileToString(file, StandardCharsets.UTF_8));
                }
                catch (IOException e) {
                    throw new PropertiesResolverException("Unable to resolve file resource " + value, e);
                }
            }
            else if (value.startsWith(LITERAL_SCHEMA)) {
                // with literal binding the value after the binding should be taken
                // literal.
                properties.setProperty(key, StringUtils.substringAfter(value, LITERAL_SCHEMA));
            }
        }
    }

    public File getBaseDir() {
        return baseDir;
    }

    public void setBaseDir(File baseDir) {
        this.baseDir = baseDir;
    }
}
