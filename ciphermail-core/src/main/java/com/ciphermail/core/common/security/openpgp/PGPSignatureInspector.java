/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.util.CurrentDateProvider;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.PacketTags;
import org.bouncycastle.bcpg.SignatureSubpacket;
import org.bouncycastle.bcpg.SignatureSubpacketTags;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPSignatureSubpacketVector;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility methods to inspect PGP signatures
 *
 * @author Martijn Brinkers
 *
 */
public class PGPSignatureInspector
{
    /**
     * The User ID certification Signature Types (see RFC 4880 5.2.1. Signature Types)
     */
    protected static final int[] USER_ID_CERTIFICATION_TYPES = new int[]{
        PGPSignature.DEFAULT_CERTIFICATION,
        PGPSignature.NO_CERTIFICATION,
        PGPSignature.CASUAL_CERTIFICATION,
        PGPSignature.POSITIVE_CERTIFICATION};

    /**
     * The User ID certification Signature Types and User ID revocation sig type (see RFC 4880 5.2.1. Signature Types)
     */
    protected static final int[] USER_ID_CERTIFICATION_WITH_REVOCATION_TYPES = new int[]{
        PGPSignature.DEFAULT_CERTIFICATION,
        PGPSignature.NO_CERTIFICATION,
        PGPSignature.CASUAL_CERTIFICATION,
        PGPSignature.POSITIVE_CERTIFICATION,
        PGPSignature.CERTIFICATION_REVOCATION};

    /**
     * The key certification Signature Types (see RFC 4880 5.2.1. Signature Types)
     */
    protected static final int[] KEY_SIGNATURES = new int[]{
        PGPSignature.SUBKEY_BINDING,
        PGPSignature.PRIMARYKEY_BINDING,
        PGPSignature.DIRECT_KEY,
        PGPSignature.KEY_REVOCATION,
        PGPSignature.SUBKEY_REVOCATION};

    private PGPSignatureInspector() {
        // empty on purpose
    }

    /*
     * Returns true if the signature key flag has any of the key flags
     */
    private static boolean hasKeyFlags(PGPSignature signature, PGPKeyFlags... flags)
    {
        if (signature == null || flags == null || flags.length == 0) {
            return false;
        }

        if (signature.hasSubpackets())
        {
            PGPSignatureSubpacketVector subPackets = signature.getHashedSubPackets();

            if (subPackets.hasSubpacket(SignatureSubpacketTags.KEY_FLAGS))
            {
                if ((subPackets.getKeyFlags() & PGPKeyFlags.getMask(flags)) != 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns true if one of the signatures has the key flags
     */
    public static boolean hasKeyFlags(Collection<PGPSignature> signatures, PGPKeyFlags... flags)
    {
        if (flags == null || flags.length == 0) {
            return false;
        }

        if (CollectionUtils.isNotEmpty(signatures))
        {
            for (PGPSignature signature : signatures)
            {
                if (hasKeyFlags(signature, flags)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns true if any of the signatures has a key flags sub packet
     */
    public static boolean hasKeyFlagsSubPacket(Collection<PGPSignature> signatures)
    {
        if (CollectionUtils.isNotEmpty(signatures))
        {
            for (PGPSignature signature : signatures)
            {
                if (signature.hasSubpackets())
                {
                    PGPSignatureSubpacketVector subPackets = signature.getHashedSubPackets();

                    if (subPackets.hasSubpacket(SignatureSubpacketTags.KEY_FLAGS)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Returns the first signature from the list. Null if there is no signature.
     */
    public static PGPSignature getFirstSignature(PGPSignatureList signatureList)
    {
        PGPSignature signature = null;

        if (signatureList != null && !signatureList.isEmpty()) {
            signature = signatureList.get(0);
        }

        return signature;
    }

    /**
     * Returns all the PGPSignature's of the key
     */
    public static List<PGPSignature> getSignatures(@Nonnull PGPPublicKey publicKey)
    {
        List<PGPSignature> signatures = new LinkedList<>();

        Iterator<?> it = publicKey.getSignatures();

        if (it != null)
        {
            while (it.hasNext())
            {
                Object next = it.next();

                if (!(next instanceof PGPSignature signature)) {
                    continue;
                }

                signatures.add(signature);
            }
        }

        return signatures;
    }

    /**
     * Returns a list of PGPSignature's with the given type
     */
    public static List<PGPSignature> getSignaturesOfType(@Nonnull PGPPublicKey publicKey, int... signatureType)
    {
        List<PGPSignature> signatures = new LinkedList<>();

        Iterator<?> it = publicKey.getSignatures();

        if (it != null)
        {
            while (it.hasNext())
            {
                Object next = it.next();

                if (!(next instanceof PGPSignature signature)) {
                    continue;
                }

                if (ArrayUtils.contains(signatureType, signature.getSignatureType())) {
                    signatures.add(signature);
                }
            }
        }

        return signatures;
    }

    /**
     * Returns a list of PGPSignature's for the User ID
     */
    public static List<PGPSignature> getSignaturesForUserID(@Nonnull PGPPublicKey publicKey, @Nonnull byte[] userID)
    {
        List<PGPSignature> signatures = new LinkedList<>();

        Iterator<?> it = publicKey.getSignaturesForID(userID);

        if (it != null)
        {
            while (it.hasNext())
            {
                Object next = it.next();

                if (!(next instanceof PGPSignature signature)) {
                    continue;
                }

                signatures.add(signature);
            }
        }

        return signatures;
    }

    /**
     * Extracts the PGPSignature from the SignatureSubpacket. Returns null if there is no embedded signature either
     * because SignatureSubpacket is not an EMBEDDED_SIGNATURE or because there is not embedded signature
     */
    public static PGPSignature extractEmbeddedSignature(SignatureSubpacket signatureSubpacket)
    throws IOException
    {
        if (signatureSubpacket == null || signatureSubpacket.getType() != SignatureSubpacketTags.EMBEDDED_SIGNATURE) {
            return null;
        }

        /*
         * Most constructors in BC PGP classes are package private. We therefore need to jump through some loops
         * to convert a raw packet into a BC PGP object.
         */
        PGPSignature sigature = null;

        ByteArrayOutputStream encoded = new ByteArrayOutputStream();

        BCPGOutputStream encoder = new BCPGOutputStream(encoded, PacketTags.SIGNATURE);

        try {
            encoder.write(signatureSubpacket.getData());
        }
        finally {
            IOUtils.closeQuietly(encoder);
        }

        PGPObjectFactory factory = new JcaPGPObjectFactory(encoded.toByteArray());

        Object o = factory.nextObject();

        if (o instanceof PGPSignatureList sigs && (sigs.size() > 0)) {
            sigature = sigs.get(0);
        }

        return sigature;
    }

    /**
     * Returns true if the signature expired
     */
    public static boolean isSignatureExpired(@Nonnull PGPSignature signature)
    {
        boolean expired = false;

        // only check hashed sub packets for the expiration time. Storing it in the unhashed sub packets
        // makes no sense since anyone can add them
        PGPSignatureSubpacketVector subPackets = signature.getHashedSubPackets();

        if (subPackets != null)
        {
            long validSeconds = subPackets.getSignatureExpirationTime();

            if (validSeconds > 0)
            {
                if (validSeconds > Integer.MAX_VALUE) {
                    validSeconds = Integer.MAX_VALUE;
                }

                Date signatureCreationTime = subPackets.getSignatureCreationTime();

                if (signatureCreationTime != null && CurrentDateProvider.getNow().after(
                        DateUtils.addSeconds(signatureCreationTime, (int) validSeconds)))
                {
                        expired = true;
                }
            }
        }

        return expired;
    }
}
