/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

/**
 * A PolicyPattern is used to detect regular expression patterns.
 *
 * @author Martijn Brinkers
 *
 */
public interface PolicyPattern
{
    /**
     * The name of the pattern
     */
    @Nonnull String getName();

    /**
     * The description of the policy.
     */
    String getDescription();

    /**
     * User editable background information about the policy
     */
    String getNotes();

    /**
     * The regular expression pattern.
     */
    @Nonnull Pattern getPattern();

    /**
     * Returns a @{Validator} which will do additional checks to see whether the pattern matches. Returns null
     * if there is no Validator
     */
    Validator getValidator();

    /**
     * If the number of times this pattern matches, the policy is violated.
     */
    int getThreshold();

    /**
     * Returns the action associated with this pattern.
     */
    PolicyViolationAction getAction();

    /**
     * If true, the evaluation is delayed when the policy is violated. What delayed evaluation actually means is not
     * defined. It's up to the caller to decided what it means. For example it can mean that the policy is not violated
     * if the message has been encrypted.
     */
    boolean isDelayEvaluation();

    /**
     * Returns a filter that will be used to filter any matching content. Null if no filter is specified.
     */
    MatchFilter getMatchFilter();
}
