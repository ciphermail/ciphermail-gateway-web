/*
 * Copyright (c) 2009-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ctl;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Entity(name = CTLEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames={CTLEntity.NAME_COLUMN, CTLEntity.THUMBPRINT_COLUMN})}
)
public class CTLEntity implements CTLEntry
{
    static final String ENTITY_NAME = "CTL";

    /**
     * Name of columns which are referenced from other classes
     */
    public static final String NAME_COLUMN = "name";
    public static final String THUMBPRINT_COLUMN = "thumbprint";

    /*
     * Property names of properties stored in nameValues
     */
    private static final String STATUS_PROPERTY_NAME = "status";
    private static final String ALLOW_EXPIRED_PROPERTY_NAME = "allowExpired";

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /*
     * A CTLEntry is 'owned' by a named CTL
     */
    @Column (name = NAME_COLUMN, length = 255, unique = false, nullable = true)
    private String name;

    @Column (name = THUMBPRINT_COLUMN, length = 255, unique = false, nullable = false)
    private String thumbprint;

    /*
     * We will store some properties as name/value pairs. The advantage
     * is that we do not need to change the database structure when
     * we want to add new properties. Although this probably is not
     * "how it should be done" with a relational database I prefer this
     * approach because database changes are a pain.
     */
    @ElementCollection
    @Column(name = "value", length = SizeUtils.KB * 32)
    @MapKeyColumn(name = "name")
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Map<String, String> nameValues = new HashMap<>();

    protected CTLEntity() {
        // Hibernate requires a default constructor
    }

    public CTLEntity(@Nonnull String name, @Nonnull String thumbprint)
    {
        this.name = Objects.requireNonNull(name);
        this.thumbprint = Objects.requireNonNull(thumbprint);
    }

    public @Nonnull String getName() {
        return name;
    }

    @Override
    public @Nonnull String getThumbprint() {
        return thumbprint;
    }

    @Override
    public @Nonnull CTLEntryStatus getStatus()
    {
        CTLEntryStatus status = CTLEntryStatus.fromName(nameValues.get(STATUS_PROPERTY_NAME));

        if (status == null) {
            status = CTLEntryStatus.WHITELISTED;
        }

        return status;
    }

    @Override
    public void setStatus(CTLEntryStatus status) {
        nameValues.put(STATUS_PROPERTY_NAME, ObjectUtils.toString(status, null));
    }

    @Override
    public boolean isAllowExpired() {
        return BooleanUtils.toBoolean(nameValues.get(ALLOW_EXPIRED_PROPERTY_NAME));
    }

    @Override
    public void setAllowExpired(boolean allowExpired) {
        nameValues.put(ALLOW_EXPIRED_PROPERTY_NAME, Boolean.toString(allowExpired));
    }

    /**
     * Returns the properties map
     */
    public Map<String, String> getProperties() {
        return nameValues;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof CTLEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, rhs.name)
            .append(thumbprint, rhs.thumbprint)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .append(thumbprint)
            .toHashCode();
    }
}
