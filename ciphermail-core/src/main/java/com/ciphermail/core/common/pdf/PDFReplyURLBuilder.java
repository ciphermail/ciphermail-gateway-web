/*
 * Copyright (c) 2008-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.Base64Utils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * Used for building the reply URL for PDFEncrypt. Can also be used to validate an existing reply.
 * <p>
 * This class is not thread safe.
 */
public class PDFReplyURLBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(PDFReplyURLBuilder.class);

    /**
     * Thrown when the key is null
     */
    public static class NullKeyURLBuilderException extends PDFURLBuilderException
    {
        public NullKeyURLBuilderException(String message) {
            super(message);
        }
    }

    /**
     * Used by PDFReplyURLBuilder#loadFromEnvelope for getting the key used to calculate the hmac. The key can
     * be based upon the enveloped data.
     *
     */
    public interface KeyProvider
    {
        String getKey(PDFReplyURLBuilder builder)
        throws PDFURLBuilderException;
    }

    /*
     * The HTTP parameters names
     */
    public static final String REPLY_ENVELOPE_PARAMETER = "env";
    public static final String HMAC_PARAMETER = "hmac";

    /*
     * JSON parameter names
     */
    private static final String USER_EMAIL_PARAMETER = "u";
    private static final String RECIPIENT_EMAIL_PARAMETER = "r";
    private static final String FROM_EMAIL_PARAMETER = "f";
    private static final String SUBJECT_PARAMETER = "s";
    private static final String MESSAGE_ID_PARAMETER = "mid";
    private static final String TIME_PARAMETER = "t";
    private static final String ID_PARAMETER = "id";

    /*
     * Number of bytes to used for the message ID if it should be generated
     */
    private static final int NONCE_BYTES_RANDOM = 8;

    /*
     * Max allowed URL length
     */
    private static final int MAX_URL_LENGTH = 1900;

    /*
     * Used to generate a message-id
     */
    private final RandomGenerator randomGenerator;

    /*
     * The base URL for the final URL. example: http://www.example.com
     */
    private String baseURL;

    /*
     * The email address of the initiator of the message
     */
    private String user;

    /*
     * The recipient of the reply
     */
    private String recipient;

    /*
     * The sender of the reply
     */
    private String from;

    /*
     * The subject of the message
     */
    private String subject;

    /*
     * The message-id of the email to which will be replied
     */
    private String messageID;

    /*
     * The time the reply was created
     */
    private Long time;

    /*
     * A "unique" id which will be added to the URL.
     */
    private String nonce;

    /*
     * The secret key used for calculating the HMAC
     */
    private String key;

    /*
     * The MAC algorithm to use for protecting the reply URL.
     */
    private String algorithm = "HmacSHA1";

    private PDFReplyURLBuilder()
    {
        try {
            randomGenerator = SecurityFactoryFactory.getSecurityFactory().createRandomGenerator();
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new UnhandledException("Error creating RandomGenerator instance.", e);
        }
    }

    public static PDFReplyURLBuilder createInstance() {
        return new PDFReplyURLBuilder();
    }

    private Mac createMAC()
    throws PDFURLBuilderException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        try {
            Mac mac = securityFactory.createMAC(algorithm);

            SecretKeySpec keySpec = new SecretKeySpec(MiscStringUtils.getBytesUTF8(key), "raw");

            mac.init(keySpec);

            return mac;
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            throw new PDFURLBuilderException(e);
        }
    }

    private String encodeJSON(JSONObject json) {
        return Base64Utils.encode((MiscStringUtils.getBytesUTF8(json.toString())), true);
    }

    /**
     * Builds a PDF reply URL. baseURL, recipient, sender and key must be non-null. If time is not specified the current
     * time in milliseconds will be used. If messageID is not specified a messageID will be created (using a UUID).
     */
    public String buildURL()
    throws PDFURLBuilderException
    {
        if (baseURL == null) {
            throw new PDFURLBuilderException("baseURL is not specified.");
        }

        if (user == null) {
            throw new PDFURLBuilderException("user is not specified.");
        }

        if (recipient == null) {
            throw new PDFURLBuilderException("recipient is not specified.");
        }

        if (from == null) {
            throw new PDFURLBuilderException("from is not specified.");
        }

        if (subject == null) {
            throw new PDFURLBuilderException("subject is not specified.");
        }

        if (key == null) {
            throw new PDFURLBuilderException("key is not specified.");
        }

        if (time == null) {
            time = System.currentTimeMillis();
        }

        if (nonce == null) {
            nonce = Base32Utils.base32Encode(randomGenerator.generateRandom(NONCE_BYTES_RANDOM));
        }

        PDFURLBuilder uRLBuilder = new PDFURLBuilder();

        uRLBuilder.setBaseURL(baseURL);

        JSONObject json = new JSONObject();

        try {
            json.put(USER_EMAIL_PARAMETER, user);
            json.put(RECIPIENT_EMAIL_PARAMETER, recipient);
            json.put(FROM_EMAIL_PARAMETER, from);
            json.put(SUBJECT_PARAMETER, subject);
            json.put(TIME_PARAMETER, time);
            json.put(ID_PARAMETER, nonce);

            // message-id is optional. To make sure the URL will not be too long we will only add it if
            // the final URL is not too large
            if (StringUtils.isNotEmpty(messageID))
            {
                if ((encodeJSON(json).length() + messageID.length()) < MAX_URL_LENGTH) {
                    json.put(MESSAGE_ID_PARAMETER, messageID);
                }
                else {
                    logger.warn("URL length exceeds maximum if Message-ID is added. Message-ID will not be added");
                }
            }
            else {
                logger.debug("Message-ID is not set");
            }
        }
        catch(JSONException e) {
            throw new PDFURLBuilderException(e);
        }

        uRLBuilder.addParameter(REPLY_ENVELOPE_PARAMETER, encodeJSON(json));

        uRLBuilder.addHMAC(HMAC_PARAMETER, createMAC());

        return uRLBuilder.buildURL();
    }

    /**
     * Compares the given hmac with the newly calculated hmac of the base64 envelope to see whether the envelope
     * is not changed and retrieves the values from the envelope (which should be a base64 encoded json object).
     */
    public PDFReplyURLBuilder loadFromEnvelope(@Nonnull String envelope, @Nonnull String hmac, @Nonnull KeyProvider keyProvider)
    throws PDFURLBuilderException
    {
        byte[] envelopeBlob = Base64.decodeBase64(MiscStringUtils.getBytesUTF8(envelope));

        try {
            JSONObject json = new JSONObject(MiscStringUtils.toStringFromUTF8Bytes(envelopeBlob));

            user = json.getString(USER_EMAIL_PARAMETER);
            recipient = json.getString(RECIPIENT_EMAIL_PARAMETER);
            from = json.getString(FROM_EMAIL_PARAMETER);
            subject = json.getString(SUBJECT_PARAMETER);
            time = json.getLong(TIME_PARAMETER);
            nonce = json.getString(ID_PARAMETER);

            // message-id is optional
            if (json.has(MESSAGE_ID_PARAMETER)) {
                messageID = json.getString(MESSAGE_ID_PARAMETER);
            }
        }
        catch (JSONException e) {
            throw new PDFURLBuilderException(e);
        }

        PDFURLBuilder uRLBuilder = new PDFURLBuilder();

        key = keyProvider.getKey(this);

        if (key == null) {
            throw new NullKeyURLBuilderException(user + " has no valid key.");
        }

        uRLBuilder.addParameter(REPLY_ENVELOPE_PARAMETER, envelope);

        String calculated = uRLBuilder.addHMAC("hmac", createMAC());

        if (!calculated.equals(hmac)) {
            throw new PDFURLBuilderException("HMAC is not correct.");
        }

        return this;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public PDFReplyURLBuilder setBaseURL(String baseURL)
    {
        this.baseURL = baseURL;
        return this;
    }

    /**
     * Gets the email address of the initiator of the message, i.e., the sender of the encrypted PDF containing the
     * reply link.
     *
     * @return the email address of the initiator of the message
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the email address of the initiator of the message, i.e., the sender of the encrypted PDF containing the
     * reply link.
     *
     * @param user the email address of the initiator of the message
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setUser(String user)
    {
        this.user = user;
        return this;
    }

    /**
     * Gets the email address of the recipient of the reply message (which in most cases is set to the sender of the
     * original encrypted PDF message)
     *
     * @return the email address of the recipient
     */
    public String getRecipient() {
        return recipient;
    }

    /**
     * Sets the email address of the recipient of the reply message. In most cases, this is set to the sender of the
     * original encrypted PDF message or if the Reply-To header was set on the original encrypted PDF message, the
     * recipient will be set to the address of the Reply-To header
     *
     * @param recipient the email address of the recipient
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setRecipient(String recipient)
    {
        this.recipient = recipient;
        return this;
    }

    /**
     * Gets the email address of the sender of the reply message. In most cases, this is set to the recipient of the
     * original encrypted PDF message.
     *
     * @return the email address of the sender of the reply message
     */
    public String getFrom() {
        return from;
    }

    /**
     * Sets the email address of the sender of the reply message. In most cases, this is set to the recipient of the
     * original encrypted PDF message.
     *
     * @param from the email address of the sender of the reply message
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setFrom(String from)
    {
        this.from = from;
        return this;
    }

    /**
     * Gets the subject of the reply message.
     *
     * @return the subject of the reply message
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Sets the subject of the reply message.
     *
     * @param subject the subject of the reply message
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setSubject(String subject)
    {
        this.subject = subject;
        return this;
    }

    /**
     * Gets the message-id of the original encrypted PDF message. This can be used to set an "In-Reply-To" header
     *
     * @return the message ID
     */
    public String getMessageID() {
        return messageID;
    }

    /**
     * Sets the message-id of the original encrypted PDF message. This can be used to set an "In-Reply-To" header
     *
     * @param messageID the message-id of the reply message
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setMessageID(String messageID)
    {
        this.messageID = messageID;
        return this;
    }

    /**
     * Returns the time the reply link was created. This can be used to limit the time the reply link is valid.
     *
     * @return the time in milliseconds
     */
    public Long getTime() {
        return time;
    }

    /**
     * Sets the time the reply link was created. This can be used to limit the time the reply link is valid.
     *
     * @param time the time in milliseconds
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setTime(Long time)
    {
        this.time = time;
        return this;
    }

    /**
     * Gets the nonce which is a random value added to the URL.
     *
     * @return the nonce value
     */
    public String getNonce() {
        return nonce;
    }

    /**
     * Sets the nonce which is a random value added to the URL.
     *
     * @param nonce the nonce value to set
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setNonce(String nonce)
    {
        this.nonce = nonce;
        return this;
    }

    /**
     * Gets the HMAC key which is used for signing the reply link
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the HMAC key which is used for signing the reply link.
     *
     * @param key the HMAC key
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setKey(String key)
    {
        this.key = key;
        return this;
    }

    /**
     * Gets the HMAC algorithm used for signing the reply link.
     *
     * @return the algorithm as a string
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * Sets the HMAC algorithm used for signing the reply link.
     *
     * @param algorithm the algorithm to set
     * @return the PDFReplyURLBuilder instance
     */
    public PDFReplyURLBuilder setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }
}
