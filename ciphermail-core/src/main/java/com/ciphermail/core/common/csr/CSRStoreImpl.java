/*
 * Copyright (c) 2015-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.common.csr;

import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.security.certificate.CSRBuilder;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Implementation of CSRStore which stores the CSRs in serialized form in the named blobs database
 */
public class CSRStoreImpl implements CSRStore
{
    private static final String NAMED_BLOB_CATEGORY = "com.ciphermail.csr";

    /*
     * The CSRs will be stored in serialized form in a NamedBlob
     */
    private final NamedBlobManager namedBlobManager;

    public CSRStoreImpl(@Nonnull NamedBlobManager namedBlobManager) {
        this.namedBlobManager = Objects.requireNonNull(namedBlobManager);
    }

    @Override
    public List<CSRStoreEntry> getCSRs()
    throws IOException
    {
        List<CSRStoreEntry> csrs = new LinkedList<>();

        List<? extends NamedBlob> blobs = namedBlobManager.getByCategory(NAMED_BLOB_CATEGORY, null, null);

        for (NamedBlob blob : blobs)
        {
            try {
                csrs.add(CSRStoreEntryImpl.deserialize(blob.getBlob()));
            }
            catch (UnrecoverableKeyException e) {
                throw new IOException(e);
            }
        }

        return csrs;
    }

    @Override
    public CSRStoreEntry getCSR(@Nonnull String id)
    throws IOException
    {
        CSRStoreEntry csr = null;

        NamedBlob blob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, id);

        if (blob != null) {
            try {
                csr = CSRStoreEntryImpl.deserialize(blob.getBlob());
            }
            catch (UnrecoverableKeyException e) {
                throw new IOException(e);
            }
        }

        return csr;
    }

    @Override
    public void deleteCSR(@Nonnull String id) {
        namedBlobManager.deleteNamedBlob(NAMED_BLOB_CATEGORY, id);
    }

    @Override
    public CSRStoreEntry generateCSR(@Nonnull List<String> domains, @Nonnull X500Name subject,
            @Nonnull CSRBuilder.Algorithm keyAlgorithm)
    throws IOException
    {
        try {
            String name = UUID.randomUUID().toString();

            CSRStoreEntryImpl csr = new CSRStoreEntryImpl(name);

            CSRBuilder csrBuilder = CSRBuilder.getInstance();

            csrBuilder.setKeyAlgorithm(Objects.requireNonNull(keyAlgorithm));
            csrBuilder.setSubject(Objects.requireNonNull(subject));
            domains.forEach(csrBuilder::addDomain);

            PKCS10CertificationRequest pkcs10 = csrBuilder.buildPKCS10();

            csr.setKeyPair(csrBuilder.getKeyPair());
            csr.setCSR(pkcs10);

            NamedBlob blob = namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, name);

            blob.setBlob(csr.serialize());

            return csr;
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | OperatorCreationException |
               InvalidAlgorithmParameterException e)
        {
            throw new IOException(e);
        }
    }

    @Override
    public CSRStoreEntry findCSR(@Nonnull X509Certificate certificate)
    throws IOException
    {
        List<? extends NamedBlob> blobs = namedBlobManager.getByCategory(NAMED_BLOB_CATEGORY, null, null);

        for (NamedBlob blob : blobs)
        {
            CSRStoreEntryImpl csr;

            try {
                csr = CSRStoreEntryImpl.deserialize(blob.getBlob());
            }
            catch (UnrecoverableKeyException e) {
                throw new IOException(e);
            }

            if (csr.getKeyPair() == null) {
                continue;
            }

            if (certificate.getPublicKey().equals(csr.getKeyPair().getPublic())) {
                // Found a matching CSR
                return csr;
            }
        }

        return null;
    }

    @Override
    public CSRStoreEntry importCertificate(@Nonnull X509Certificate[] chain)
    throws IOException
    {
        if (chain.length == 0) {
            throw new IllegalArgumentException("Chain must not be empty");
        }

        List<? extends NamedBlob> blobs = namedBlobManager.getByCategory(NAMED_BLOB_CATEGORY, null, null);

        for (NamedBlob blob : blobs)
        {
            CSRStoreEntryImpl csr;

            try {
                csr = CSRStoreEntryImpl.deserialize(blob.getBlob());
            }
            catch (UnrecoverableKeyException e) {
                throw new IOException(e);
            }

            if (csr.getKeyPair() == null) {
                continue;
            }

            if (chain[0].getPublicKey().equals(csr.getKeyPair().getPublic()))
            {
                // Found a matching CSR
                csr.setCertificateChain(chain);

                // store the modified entry
                blob.setBlob(csr.serialize());

                return csr;
            }
        }

        return null;
    }
}
