/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of MatchFilterRegistry that stores MatchFilter's in memory.
 *
 * @author Martijn Brinkers
 *
 */
public class MatchFilterRegistryImpl implements MatchFilterRegistry
{
    private final Map<String, MatchFilter> filters = new HashMap<>();

    public MatchFilterRegistryImpl(List<MatchFilter> filters)
    {
        if (filters != null)
        {
            for (MatchFilter filter : filters) {
                addMatchFilter(filter);
            }
        }
    }

    @Override
    public synchronized void addMatchFilter(@Nonnull MatchFilter filter)
    {
        String name = StringUtils.lowerCase(filter.getName());

        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("Filter name is empty.");
        }

        filters.put(name, filter);
    }

    @Override
    public void addMatchFilters(Collection<MatchFilter> filters)
    {
        if (filters != null)
        {
            for (MatchFilter filter : filters)
            {
                if (filter == null) {
                    continue;
                }

                addMatchFilter(filter);
            }
        }
    }

    @Override
    public synchronized MatchFilter getMatchFilter(String name)
    {
        if (name == null) {
            return null;
        }

        return filters.get(StringUtils.lowerCase(name));
    }

    @Override
    public synchronized @Nonnull List<MatchFilter> getMatchFilters() {
        return new LinkedList<>(filters.values());
    }
}
