/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.crl;

import com.ciphermail.core.common.scheduler.AbstractCancelableTask;
import com.ciphermail.core.common.scheduler.Task;
import com.ciphermail.core.common.scheduler.TaskScheduler;
import com.ciphermail.core.common.scheduler.ThreadInterruptTimeoutTask;
import com.ciphermail.core.common.util.CollectionUtils;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.NoSuchProviderException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Properties;

public class LDAPCRLDownloadHandler implements CRLDownloadHandler
{
    private static final Logger logger = LoggerFactory.getLogger(LDAPCRLDownloadHandler.class);

    private static final String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";

    /*
     * The default size at which a downloaded CRL will be streamed to disk
     */
    private static final int DEFAULT_MEM_THRESHOLD = SizeUtils.MB * 5;

    /*
     * The size in bytes at which a downloaded CRL will be streamed to disk
     */
    private int memThreshold = DEFAULT_MEM_THRESHOLD;

    /*
     * The max allowed size in bytes of a downloaded CRL.
     */
    private int maxCRLSize = SizeUtils.MB * 25;

    /*
     * Max time in milliseconds downloading a CRL may take after which the download will be cancelled.
     */
    private long totalTimeout = 15 * DateUtils.MILLIS_PER_MINUTE;

    /*
     * LDAP connection timeout.
     */
    private long connectTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    /*
     * LDAP read timeout.
     */
    private long readTimeout = 30 * DateUtils.MILLIS_PER_SECOND;

    private static class LazyInitializingInitialDirContext extends InitialDirContext
    {
        public LazyInitializingInitialDirContext()
        throws NamingException
        {
            super(true);
        }

        @Override
        public void init(Hashtable<?,?> environment) //NOSONAR
        throws NamingException
        {
            super.init(environment);
        }
    }

    @Override
    public boolean canHandle(@Nonnull URI uri)
    {
        String scheme = uri.getScheme();

        return scheme != null && scheme.equalsIgnoreCase("ldap");
    }

    @Override
    public Collection<? extends CRL> downloadCRLs(@Nonnull URI uri)
    throws IOException, CRLException
    {
        // sanity check
        if (!canHandle(uri)) {
            throw new IllegalArgumentException("The uri cannot be handled by this handler.");
        }

        Collection<CRL> crls = new LinkedList<>();

        Properties properties = new Properties();

        // Info on timeouts: http://java.sun.com/docs/books/tutorial/jndi/newstuff/readtimeout.html
        // basic guide about LDAP http://java.sun.com/products/jndi/tutorial/basics/prepare/initial.html
        properties.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        properties.put(Context.PROVIDER_URL, uri.toString());
        properties.put(Context.SECURITY_AUTHENTICATION, "none"); //NOSONAR
        properties.put(Context.REFERRAL, "follow");
        properties.put("com.sun.jndi.ldap.connect.timeout", Long.toString(connectTimeout));
        properties.put("com.sun.jndi.ldap.read.timeout", Long.toString(readTimeout));

        try {
            final LazyInitializingInitialDirContext dirContext = new LazyInitializingInitialDirContext();

            final String name = LDAPCRLDownloadHandler.class.getCanonicalName();

            // watchdog to prevent runaway downloads
            TaskScheduler watchdog = new TaskScheduler(name);

            try {
                Task closeContextTask = new AbstractCancelableTask() {
                    @Override
                    public void doRun()
                    throws Exception
                    {
                        dirContext.close();
                    }
                };

                watchdog.addTask(closeContextTask, totalTimeout);

                Task threadWatchdogTask = new ThreadInterruptTimeoutTask(Thread.currentThread(), name);

                // we want the close to fire first so add 10%
                watchdog.addTask(threadWatchdogTask, (long) (totalTimeout * 1.1));

                dirContext.init(properties);

                getCRLs(crls, dirContext, new String[]{"certificateRevocationList;binary"});
                getCRLs(crls, dirContext, new String[]{"authorityRevocationList;binary"});
                getCRLs(crls, dirContext, new String[]{"deltaRevocationList;binary"});
            }
            finally
            {
                watchdog.cancel();

                // Clear the interrupted flag, it might have been set by the ThreadInterruptTimeoutTask
                Thread.interrupted();
            }
        }
        catch(NamingException e) {
            throw new IOException(e);
        }

        return crls;
    }

    private void getCRLs(Collection<CRL> crls, DirContext dirContext, String[] attributeIDs)
    throws NamingException, CRLException
    {
        Attributes attributes = dirContext.getAttributes("", attributeIDs);

        NamingEnumeration<? extends Attribute> attributeEnumerator = attributes.getAll();

        while (attributeEnumerator.hasMoreElements())
        {
            Attribute attribute = attributeEnumerator.nextElement();

            if (attribute != null)
            {
                try {
                    Object object = attribute.get();

                    if (object instanceof byte[] bytes)
                    {
                        if (bytes.length > maxCRLSize)
                        {
                            logger.warn("Bytes returned by LDAP server exceed limit.");
                            continue;
                        }

                        InputStream input = new ByteArrayInputStream(bytes);

                        Collection<? extends CRL> found = CRLUtils.readCRLs(input);

                        if (found == null || found.isEmpty()) {
                            logger.debug("No CRLs found in the downloaded stream.");
                        }
                        else {
                            CollectionUtils.copyCollectionFiltered(found, crls, CRL.class);
                        }
                    }
                }
                catch (NamingException e) {
                    logger.error("Error getting attribute value.", e);
                }
                catch (CertificateException | NoSuchProviderException e) {
                    logger.error("Error creating CRL out of byte array.", e);
                }
            }
        }
    }

    public int getMemThreshold() {
        return memThreshold;
    }

    public void setMemThreshold(int memThreshold) {
        this.memThreshold = memThreshold;
    }

    public int getMaxCRLSize() {
        return maxCRLSize;
    }

    public void setMaxCRLSize(int maxCRLSize) {
        this.maxCRLSize = maxCRLSize;
    }

    public long getTotalTimeout() {
        return totalTimeout;
    }

    public void setTotalTimeout(long totalTimeout) {
        this.totalTimeout = totalTimeout;
    }

    public long getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(long connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }
}
