/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.ca;

import javax.security.auth.x500.X500Principal;
import java.util.Date;
import java.util.UUID;

/**
 * A CertificateRequest is a request for signing a certificate. The CertificateRequest will be handled by a
 * CertificateRequestHandler.
 *
 * @author Martijn Brinkers
 *
 */
public interface CertificateRequest
{
    /**
     * The ID uniquely identifies this request. The ID might be null when the request is not yet persistent
     */
    UUID getID();

    /**
     * The date the CertificateRequest was created
     */
    Date getCreated();

    /**
     * The subject for the certificate
     */
    void setSubject(X500Principal subject);
    X500Principal getSubject();

    /**
     * Email will be added as an AltName
     */
    void setEmail(String email);
    String getEmail();

    /**
     * The number of days the certificate should be valid. Must be > 0.
     */
    void setValidity(int validity);
    int getValidity();

    /**
     * The signature algorithm used to sign the certificate
     */
    void setSignatureAlgorithm(String signatureAlgorithm);
    String getSignatureAlgorithm();

    /**
     * The length of the key for the request
     */
    void setKeyLength(int keyLength);
    int getKeyLength();

    /**
     * The CRL distribution point to add
     */
    void setCRLDistributionPoint(String crlDistributionPoint);
    String getCRLDistributionPoint();

    /**
     * Info will be used to provide some user viewable information (free form text) about the request.
     */
    void setInfo(String info);
    String getInfo();

    /**
     * A general data object. Certificate providers can store whatever they like.
     */
    void setData(byte[] data);
    byte[] getData();

    /**
     * A counter that keeps track how many times this request was handled.
     */
    void setIteration(int iteration);
    int getIteration();

    /**
     * The date at which this request was last updated.
     */
    void setLastUpdated(Date lastUpdated);
    Date getLastUpdated();

    /**
     * The date at which this request require an update .
     */
    void setNextUpdate(Date nextUpdate);
    Date getNextUpdate();

    /**
     * The message (for example a warning/error message) since the last update.
     * This can be set by the certificate provider
     */
    void setLastMessage(String message);
    String getLastMessage();

    /**
     * The name of the CertificateHandler which is responsible for handling this request
     */
    String getCertificateHandlerName();
}
