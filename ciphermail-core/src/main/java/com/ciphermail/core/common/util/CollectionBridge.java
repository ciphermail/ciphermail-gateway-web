/*
 * Copyright (c) 2010-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.util;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Iterator;

/**
 * A Collection class, wrapping an existing Collection to bridge from a super type to of a class to the base type
 * without having to use ugly type casts. The Collection makes sure that the item added is the same type as the
 * source collection (i.e. F extends E) and if this is not the case an IllegalArgumentException will be thrown.
 *
 * @author Martijn Brinkers
 *
 */
public class CollectionBridge<E, F extends E>  implements Collection<E>
{
    private final Collection<F> source;
    private final Class<F> clazz;

    public CollectionBridge(Collection<F> sourceCollection, Class<F> clazz)
    {
        this.source = sourceCollection;
        this.clazz = clazz;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean add(E item)
    {
        if (!(clazz.isAssignableFrom(item.getClass()))) {
            throw new IllegalArgumentException("item is-not-a " + clazz.getName());
        }

        return source.add((F) item);
    }

    @Override
    public boolean addAll(Collection<? extends E> items)
    {
        boolean added = false;

        for (E item : items)
        {
            if (add(item)) {
                added = true;
            }
        }

        return added;
    }

    @Override
    public void clear() {
        source.clear();
    }

    @Override
    public boolean contains(Object item) {
        return source.contains(item);
    }

    @Override
    public boolean containsAll(@Nonnull Collection<?> items)  {
        return source.containsAll(items);
    }

    @Override
    public boolean isEmpty() {
        return source.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return new ItemIterator(source.iterator());
    }

    @Override
    public boolean remove(Object item) {
        return source.remove(item);
    }

    @Override
    public boolean removeAll(@Nonnull Collection<?> items) {
        return source.removeAll(items);
    }

    @Override
    public boolean retainAll(@Nonnull Collection<?> items) {
        return source.retainAll(items);
    }

    @Override
    public int size() {
        return source.size();
    }

    @Override
    public Object[] toArray() {
        return source.toArray();
    }

    @Override
    public <T> T[] toArray(@Nonnull T[] items) {
        return source.toArray(items);
    }

    private class ItemIterator implements Iterator<E>
    {
        private final Iterator<F> source;

        public ItemIterator(Iterator<F> source) {
            this.source = source;
        }

        @Override
        public boolean hasNext() {
            return source.hasNext();
        }

        @Override
        public E next() {
            return source.next();
        }

        @Override
        public void remove() {
            source.remove();
        }
    }
}
