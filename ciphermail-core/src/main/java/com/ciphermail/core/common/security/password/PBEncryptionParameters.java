/*
 * Copyright (c) 2008-2013, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.password;

import org.apache.commons.lang.SerializationException;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * The PBEncryptionParameters is used to store the relevant info for the Password Based Encryption (PBE) classes.
 * When something is encrypted using PBE, in order to decrypt we need to have access to the salt and iteration
 * count. This class is used to store the encrypted data and the relevant parameters needed for decryption and
 * allows this data to be serialized.
 *
 * @author Martijn Brinkers
 *
 */
public class PBEncryptionParameters
{
    private static final long serialVersionUID = -7617451983656008567L;

    private byte[] salt;
    private int iterationCount;
    private byte[] encryptedData;

    protected PBEncryptionParameters(@Nonnull byte[] salt, int iterationCount, @Nonnull byte[] encryptedData)
    {
        this.salt = Objects.requireNonNull(salt);
        this.iterationCount = iterationCount;
        this.encryptedData = Objects.requireNonNull(encryptedData);
    }

    /**
     * Creates a PBEncryptionParameters from the encoded form.
     */
    protected PBEncryptionParameters(@Nonnull byte[] encoded)
    throws IOException
    {
        fromByteArray(encoded);
    }


    public byte[] getSalt() {
        return salt;
    }

    public int getIterationCount() {
        return iterationCount;
    }

    public byte[] getEncryptedData() {
        return encryptedData;
    }

    /**
     * Returns the encoded form of PBEncryptionParameters.
     */
    public byte[] getEncoded()
    throws IOException
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        DataOutputStream out = new DataOutputStream(bos);

        out.writeLong(serialVersionUID);

        out.writeInt(salt.length);
        out.write(salt);

        out.writeInt(iterationCount);

        out.writeInt(encryptedData.length);
        out.write(encryptedData);

        return bos.toByteArray();
    }

    private void fromByteArray(@Nonnull byte[] encoded)
    throws IOException
    {
        ByteArrayInputStream bis = new ByteArrayInputStream(encoded);

        DataInputStream in = new DataInputStream(bis);

        long version = in.readLong();

        if (version != serialVersionUID) {
            throw new SerializationException("Version expected '" + serialVersionUID + "' but got '" + version);
        }

        int saltSize = in.readInt();

        this.salt = new byte[saltSize];

        in.readFully(salt);

        this.iterationCount = in.readInt();

        int encryptedSize = in.readInt();

        this.encryptedData = new byte[encryptedSize];

        in.readFully(encryptedData);
    }
}
