/*
 * Copyright (c) 2013-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.openpgp;

import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidator;
import com.ciphermail.core.common.security.openpgp.validator.PGPPublicKeyValidatorResult;
import com.ciphermail.core.common.security.openpgp.validator.StandardValidatorContextObjects;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.InputStream;
import java.util.Objects;

/**
 * Implementation of PGPSignatureValidator
 *
 * @author Martijn Brinkers
 *
 */
public class PGPContentSignatureValidatorImpl implements PGPContentSignatureValidator
{
    private static final Logger logger = LoggerFactory.getLogger(PGPContentSignatureValidatorImpl.class);

    /*
     * Checks whether the content is signed by the public key identified by the key ID from the signature
     */
    private final PGPContentSignatureVerifier signatureVerifier;

    /*
     * Validates the signing key
     */
    private final PGPPublicKeyValidator signingKeyValidator;

    public PGPContentSignatureValidatorImpl(@Nonnull PGPContentSignatureVerifier signatureVerifier,
            @Nonnull PGPPublicKeyValidator signingKeyValidator)
    {
        this.signatureVerifier = Objects.requireNonNull(signatureVerifier);
        this.signingKeyValidator = Objects.requireNonNull(signingKeyValidator);
    }

    @Override
    public PGPSignatureValidatorResult validateSignature(@Nonnull InputStream content, @Nonnull PGPSignature signature)
    {
        PGPSignatureValidatorResult validatorResult;

        try {
            PGPSignatureVerifierResult verifierResult = signatureVerifier.verify(content, signature);

            PGPKeyRingEntry signer = verifierResult.getSigner();

            if (!verifierResult.isValid())
            {
                validatorResult = new PGPSignatureValidatorResultImpl(false,
                        "Message has been tampered with.", true, signer);
            }
            else {
                // Signature could be verified. Now check whether the signer key is valid
                // The parent (master key) should be placed in the context for the validator
                PGPKeyRingEntry parent = signer.getParentKey();

                PGPPublicKey parentKey = parent != null ? parent.getPublicKey() : null;

                Context context = new ContextImpl();

                context.set(StandardValidatorContextObjects.MASTER_KEY, parentKey);

                PGPPublicKey signerPublicKey = signer.getPublicKey();

                PGPPublicKeyValidatorResult validationResult = signingKeyValidator.validate(signerPublicKey, context);

                String failureMessage = null;

                if (!validationResult.isValid()) {
                    failureMessage = validationResult.getFailureMessage();
                }

                validatorResult = new PGPSignatureValidatorResultImpl(validationResult.isValid(), failureMessage,
                        false, signer);
            }
        }
        catch (Exception e)
        {
            logger.debug("Error validating signature", e);

            validatorResult = new PGPSignatureValidatorResultImpl(false, e.getMessage(), false, null);
        }

        return validatorResult;
    }
}
