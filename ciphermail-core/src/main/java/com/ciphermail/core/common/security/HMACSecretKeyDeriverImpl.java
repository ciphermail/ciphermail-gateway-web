/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security;

import com.ciphermail.core.common.util.Base32Utils;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;

public class HMACSecretKeyDeriverImpl implements SecretKeyDeriver
{
    /*
     * The HMAC algorithm
     */
    private String algorithm = "HmacSHA256";

    /*
     * Factory for creating security class instances
     */
    private final SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

    private Mac createMAC(byte[] masterKey)
    throws GeneralSecurityException
    {
        Mac mac = securityFactory.createMAC(algorithm);

        SecretKeySpec keySpec = new SecretKeySpec(masterKey, "raw");

        mac.init(keySpec);

        return mac;
    }

    @Override
    public String deriveKey(@Nonnull String masterSecretKey, @Nonnull String id, @Nonnull String subKey)
    throws GeneralSecurityException
    {
        Mac mac = createMAC(masterSecretKey.getBytes(StandardCharsets.UTF_8));

        mac.update(id.getBytes(StandardCharsets.UTF_8));
        mac.update(subKey.getBytes(StandardCharsets.UTF_8));

        return Base32Utils.base32Encode(mac.doFinal());
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public HMACSecretKeyDeriverImpl setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }
}
