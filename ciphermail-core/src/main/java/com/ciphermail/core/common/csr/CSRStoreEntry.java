/*
 * Copyright (c) 2015, CipherMail.
 *
 * This file is part of CipherMail email encryption pro.
 */
package com.ciphermail.core.common.csr;

import org.bouncycastle.pkcs.PKCS10CertificationRequest;

import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.Date;

public interface CSRStoreEntry
{
    /**
     * The ID of the CSR
     */
    String getID();

    /**
     * The date this CSR was created
     */
    Date getDate();

    /**
     * Returns the certificate signing request of this entry
     */
    PKCS10CertificationRequest getPKCS10Request();

    /**
     * Returns the public/private keypair of this entry
     */
    KeyPair getKeyPair();

    /**
     * The certificate chain associated with this request. If a certificate is not yet imported, null will be returned.
     */
    X509Certificate[] getCertificateChain();
}
