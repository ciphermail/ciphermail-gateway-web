/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of URLBuilder for building HTTP URLs.
 * <p>
 * Example URL
 * <p><a href="
 ">* http://www.example.com?param1=value1&param2=va</a>lue2
 *
 * @author Martijn Brinkers
 *
 */
public class PDFURLBuilder
{
    private static class Parameter
    {
        private final String name;
        private final String value;

        Parameter(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    /*
     * The base URL for the final URL. example: http://www.example.com
     */
    private String baseURL;

    /*
     * The list of parameters
     */
    private final List<Parameter> parameters = new LinkedList<>();

    public void setBaseURL(@Nonnull String baseURL) {
        this.baseURL = StringUtils.removeEnd(baseURL.trim(), "/");
    }

    public void addParameter(@Nonnull String name, String value)
    throws PDFURLBuilderException
    {
        if (StringUtils.isEmpty(name)) {
            throw new PDFURLBuilderException("parameter name cannot be empty.");
        }

        name = name.trim();

        if (value == null) {
            value = "";
        }

        parameters.add(new Parameter(name, value));
    }

    public String addHMAC(String name, @Nonnull Mac mac)
    throws PDFURLBuilderException
    {
        if (parameters.isEmpty()) {
            throw new PDFURLBuilderException("There are no values.");
        }

        for (Parameter parameter : parameters)
        {
            mac.update(MiscStringUtils.getBytesASCII(parameter.getName()));
            mac.update(MiscStringUtils.getBytesASCII(parameter.getValue()));
        }

        byte[] hmac = mac.doFinal();

        String base32 = Base32Utils.base32Encode(hmac);

        parameters.add(new Parameter(name, base32));

        return base32;
    }

    public String buildURL()
    {
        String url = baseURL;

        if (!StringUtils.contains(url, '?') && !parameters.isEmpty()) {
            url = url + '?';
        }

        url = url + buildQueury();

        return url;
    }

    public String buildQueury()
    {
        StrBuilder sb = new StrBuilder(256);

        for (Parameter parameter : parameters)
        {
            sb.appendSeparator("&");
            sb.append(URLEncoder.encode(parameter.getName(), StandardCharsets.UTF_8));
            sb.append("=");
            sb.append(URLEncoder.encode(parameter.getValue(), StandardCharsets.UTF_8));

        }

        return sb.toString();
    }

    public void reset()
    {
        baseURL = null;
        parameters.clear();
    }
}
