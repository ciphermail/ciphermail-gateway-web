/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.security.keystore;

import com.ciphermail.core.common.security.CertSelectorKeyIdentifier;
import com.ciphermail.core.common.security.KeyIdentifier;
import com.ciphermail.core.common.security.SecurityConstants;
import com.ciphermail.core.common.security.certificate.X509CertificateMicrosoftSKI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertSelector;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * Basic KeyProvider that searches the provided KeyStore for matching keys. This
 * KeyProvider is slow because it steps through all entries. Use an optimized KeyProvider
 * for real life scenarios. Only private key entries and KEYTRANSRECIPIENT is supported by
 * this KeyStoreKeyProvider.
 *
 * @author Martijn Brinkers
 *
 */
public class KeyStoreKeyProvider implements BasicKeyStore
{
    private static final Logger logger = LoggerFactory.getLogger(KeyStoreKeyProvider.class);

    /*
     * If true, a non-standard subject key identifier calculation routine will be used to work
     * around a non RFC compliancy 'bug' in OL2010
     *
     * For info why this workaround is needed see: https://bugzilla.mozilla.org/show_bug.cgi?id=559243 and
     * http://www.ietf.org/mail-archive/web/smime/current/msg18730.html
     */
    private boolean useOL2010Workaround = SecurityConstants.isOutlook2010SKIWorkaroundEnabled();

    /*
     * The key store from which keys are retrieved
     */
    private final KeyStore keyStore;

    /*
     * The password protection for the key store
     */
    private final KeyStore.PasswordProtection passwordProtection;

    /**
     * The given keyStore will be used for the key source. All entries must be
     * encrypted with the same password and only KeyStore.PasswordProtection
     * is supported.
     *
     * @param keyStore
     * @param password
     */
    public KeyStoreKeyProvider(KeyStore keyStore, String password)
    {
        this.keyStore = keyStore;
        this.passwordProtection = new KeyStore.PasswordProtection(password.toCharArray());
    }

    /**
     * returns a collection of private keys matching RecipientIdentifier. RecipientIdentifierType.KEYTRANSRECIPIENT
     * is supported. All other RecipientIdentifierTypes will return an empty collection.
     */
    @Override
    public Set<PrivateKey> getMatchingKeys(KeyIdentifier keyIdentifier)
    throws KeyStoreException
    {
        Set<PrivateKey> keys = new HashSet<>();

        if (keyIdentifier instanceof CertSelectorKeyIdentifier certSelectorKeyIdentifier)
        {
            Enumeration<String> aliases = keyStore.aliases();

            while (aliases.hasMoreElements())
            {
                String alias = aliases.nextElement();

                KeyStore.Entry entry;

                try {
                    // only key entries are supported
                    if (!keyStore.isKeyEntry(alias)) {
                        continue;
                    }

                    entry = keyStore.getEntry(alias, passwordProtection);

                    if (!(entry instanceof KeyStore.PrivateKeyEntry privateKeyEntry)) {
                        continue;
                    }

                    Certificate certificate = privateKeyEntry.getCertificate();

                    // only X509Certificates are supported
                    if (!(certificate instanceof X509Certificate x509Certificate)) {
                        continue;
                    }

                    CertSelector selector = certSelectorKeyIdentifier.getSelector();

                    if (useOL2010Workaround) {
                        x509Certificate = new X509CertificateMicrosoftSKI(x509Certificate);
                    }

                    if (selector.match(x509Certificate))
                    {
                        PrivateKey key = privateKeyEntry.getPrivateKey();

                        keys.add(key);
                    }
                }
                catch (NoSuchAlgorithmException | UnrecoverableEntryException | IOException | KeyStoreException e) {
                    logger.error("Key entry could not be retrieved.", e);
                }
            }
        }

        return keys;
    }

    public boolean isUseOL2010Workaround() {
        return useOL2010Workaround;
    }

    public void setUseOL2010Workaround(boolean useOL2010Workaround) {
        this.useOL2010Workaround = useOL2010Workaround;
    }
}
