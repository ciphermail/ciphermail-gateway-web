/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.dlp.impl;

import com.ciphermail.core.common.cache.PatternCache;
import com.ciphermail.core.common.dlp.MatchFilter;
import com.ciphermail.core.common.dlp.MatchFilterRegistry;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyViolationAction;
import com.ciphermail.core.common.dlp.Validator;
import com.ciphermail.core.common.dlp.ValidatorRegistry;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * A PolicyPattern implementation that can be marshalled and unmarshalled to/from JSON
 */
public class PolicyPatternImpl implements PolicyPattern
{
    /*
     * The name of this policy pattern (aka the rule)
     */
    private String name;

    /*
     * The regular expression pattern of this rule
     */
    @JsonIgnore
    private Pattern pattern;

    /*
     * Validator which will do additional checks to see whether the pattern matches.
     */
    @JsonIgnore
    private Validator validator;

    /*
     * filter that will be used to filter any matching content.
     */
    @JsonIgnore
    private MatchFilter matchFilter;

    /*
     * The regular expression pattern for this rule
     */
    private String regExp;

    /*
     * The name of the validator which will do additional checks to see whether the pattern matches.
     */
    private String validatorName;

    /*
     * The name of the filter that will be used to filter any matching content.
     */
    private String matchFilterName;

    /*
     * Explains what this pattern policy does.
     */
    private String description;

    /*
     * User editable background information about the policy
     */
    private String notes;

    /*
     * If the pattern matches the content more than the threshold, the policy is violated.
     */
    private int threshold;

    /*
     * The action associated with this pattern
     */
    private PolicyViolationAction action;

    /*
     * If true, the evaluation is delayed when the policy is violated. What delayed evaluation actually means is not
     * defined. It's up to the caller to decided what it means. For example it can mean that the policy is not violated
     * if the message has been encrypted.
     */
    private boolean delayEvaluation;

    /*
     * Used for caching patterns.
     */
    @JsonIgnore
    private PatternCache patternCache;

    /*
     * Registry used to lookup Validator's based on the name of the validator.
     */
    @JsonIgnore
    private ValidatorRegistry validatorRegistry;

    /*
     * Registry used to lookup MatchFilter's based on the name of the MatchFilter.
     */
    @JsonIgnore
    private MatchFilterRegistry matchFilterRegistry;

    public PolicyPatternImpl(@Nonnull String name, @Nonnull Pattern pattern)
    {
        this.name = Objects.requireNonNull(name);

        setPattern(Objects.requireNonNull(pattern));
    }

    public PolicyPatternImpl() {
        // Jackson requires a default constructor
    }

    @Override
    public @Nonnull String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegExp() {
        return regExp;
    }

    public void setRegExp(String regExp) {
        this.regExp = regExp;
    }

    public String getValidatorName() {
        return validatorName;
    }

    public void setValidatorName(String validatorName) {
        this.validatorName = validatorName;
    }

    public String getMatchFilterName() {
        return matchFilterName;
    }

    public void setMatchFilterName(String matchFilterName) {
        this.matchFilterName = matchFilterName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public PolicyViolationAction getAction() {
        return action;
    }

    public void setAction(PolicyViolationAction action) {
        this.action = action;
    }

    @Override
    public boolean isDelayEvaluation() {
        return delayEvaluation;
    }

    public void setDelayEvaluation(boolean delayEvaluation) {
        this.delayEvaluation = delayEvaluation;
    }

    public void setValidator(Validator validator)
    {
        this.validator = validator;

        validatorName = validator != null ? validator.getName() : null;
    }

    @Override
    public Validator getValidator()
    {
        if (validatorName != null && (validator == null || !validatorName.equals(validator.getName()))) {
            validator = validatorRegistry.getValidator(validatorName);
        }

        return validator;
    }

    public void setMatchFilter(MatchFilter matchFilter)
    {
        this.matchFilter = matchFilter;

        matchFilterName = matchFilter != null ? matchFilter.getName() : null;
    }

    @Override
    public MatchFilter getMatchFilter()
    {
        if (matchFilterName != null && (matchFilter == null || !matchFilterName.equals(matchFilter.getName()))) {
            matchFilter = matchFilterRegistry.getMatchFilter(matchFilterName);
        }

        return matchFilter;
    }

    public void setPattern(@Nonnull Pattern pattern)
    {
        this.pattern = Objects.requireNonNull(pattern);

        regExp = pattern.pattern();
    }

    @Override
    public @Nonnull Pattern getPattern()
    {
        if (regExp != null && (pattern == null || !regExp.equals(pattern.pattern())))
        {
            // Note: we scan with case sensitivity by default since that's about 10 time faster for large
            // documents than if we would use case-insensitive scanning. The extracted text is already
            // normalized to lowercase so the regular expression pattern should be in lower case as well.
            pattern = patternCache.getPattern(regExp);
        }

        return pattern;
    }

    public PatternCache getPatternCache() {
        return patternCache;
    }

    public void setPatternCache(PatternCache patternCache) {
        this.patternCache = patternCache;
    }

    public ValidatorRegistry getValidatorRegistry() {
        return validatorRegistry;
    }

    public void setValidatorRegistry(ValidatorRegistry validatorRegistry) {
        this.validatorRegistry = validatorRegistry;
    }

    public MatchFilterRegistry getMatchFilterRegistry() {
        return matchFilterRegistry;
    }

    public void setMatchFilterRegistry(MatchFilterRegistry matchFilterRegistry) {
        this.matchFilterRegistry = matchFilterRegistry;
    }

    /**
     * Validates the internal state to check whether it's settings are correct. This is for example called when
     * a Policy Pattern is imported from JSON. It might be that a Validator or MatchFilter is not available
     *
     * @throws RuntimeException will be thrown if validation fails
     */
    @SuppressWarnings("java:S2583")
    public void validate()
    {
        //noinspection ConstantValue
        if (getPattern() == null) {
            throw new IllegalStateException("Pattern is null");
        }

        if (StringUtils.isNotEmpty(validatorName) && getValidator() == null) {
            throw new IllegalArgumentException("Validator with name " + validatorName + " does not exist.");
        }

        if (StringUtils.isNotEmpty(matchFilterName) && getMatchFilter() == null) {
            throw new IllegalArgumentException("Match filter with name " + matchFilterName + " does not exist.");
        }
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternImpl rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, rhs.name)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().
            append(name).
            toHashCode();
    }

    /**
     * Creates a new instance of PolicyPatternImpl with the name and copies all public data from
     * the pattern to clone to the new instance.
     */
    public static PolicyPatternImpl clone(PolicyPattern pattern, String name)
    {
        PolicyPatternImpl clone = new PolicyPatternImpl();

        clone.name = name;

        if (pattern != null)
        {
            clone.setDescription(pattern.getDescription());
            clone.setNotes(pattern.getNotes());
            clone.setPattern(pattern.getPattern());
            clone.setValidator(pattern.getValidator());
            clone.setThreshold(pattern.getThreshold());
            clone.setAction(pattern.getAction());
            clone.setDelayEvaluation(pattern.isDelayEvaluation());
            clone.setMatchFilter(pattern.getMatchFilter());
        }

        return clone;
    }
}
