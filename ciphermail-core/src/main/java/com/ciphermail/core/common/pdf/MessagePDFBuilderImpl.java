/*
 * Copyright (c) 2008-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.common.pdf;

import com.ciphermail.core.app.CipherMailApplication;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MessageParser;
import com.ciphermail.core.common.mail.MimeTypes;
import com.ciphermail.core.common.mail.MimeUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.util.HTMLLinkify;
import com.ciphermail.core.common.util.SizeUtils;
import com.ciphermail.core.common.util.StringReplaceUtils;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.FontSelector;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfEncodings;
import com.lowagie.text.pdf.PdfFileSpecification;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import javax.activation.DataHandler;
import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.annotation.Nonnull;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimePart;
import javax.mail.util.ByteArrayDataSource;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Creates a PDF that looks like an email from a MimeMessage. Attachments are included as well.
 *
 * @author Martijn Brinkers
 *
 */
public class MessagePDFBuilderImpl implements MessagePDFBuilder, Cloneable
{
    private static final Logger logger = LoggerFactory.getLogger(MessagePDFBuilderImpl.class);

    /*
     * The text used for the body when there is no body text found. Can happen when the message is HTML only.
     */
    private static final String NO_INLINE_TEXT_BODY = "*** There was no inline text body. Make sure the email is " +
            "sent as text only or is sent as html with alternative text part ***";

    protected static final String FOOTER_TEXT = "Created with " + CipherMailApplication.getName() +
                                                " free community edition.";

    /*
     * The filename to use if the filename cannot be determined but is a plain/text file
     */
    protected static final String UNKNOWN_TEXT_FILENAME = "attachment.txt";

    /*
     * The filename to use if the filename cannot be determined
     */
    protected static final String UNKNOWN_ATTACHMENT_FILENAME = "attachment.bin";

    /*
     * The filename to use if the name of the attached rfc822 message cannot be determined
     */
    protected static final String UNKNOWN_RFC822_FILENAME = "message.pdf";

    /*
     * PDF document creator
     */
    private static final String CREATOR = CipherMailApplication.getName();

    /*
     * Size of the document.
     */
    private float documentWidth = PageSize.A4.getWidth();
    private float documentHeight = PageSize.A4.getHeight();

    /*
     * The margins of the document.
     */
    private float marginLeft   = 10;
    private float marginRight  = 10;
    private float marginTop    = 10;
    private float marginBottom = 10;

    /*
     * Provides extra fonts
     */
    private FontProvider fontProvider;

    /*
     * The viewer preferences (PAGE_MODE_USE_ATTACHMENTS, DISPLAY_DOC_TITLE etc.)
     */
    private int viewerPreferences;

    /*
     * The bidirectional text mode to use for the body text
     */
    private BIDIMode bidiMode = BIDIMode.LTR;

    /*
     * If true everything is compressed (only supported by PDF 1.5).
     */
    private boolean fullCompression = false;

    /*
     * The document background color.
     */
    private Color backgroundColor = new Color(249, 249, 249);

    /*
     * PDF does not have tabs so we need to convert tabs to spaces.
     */
    private int tabWidth;

    /*
     * Size of the font used for the message body
     */
    private float bodyFontSize = 10f;

    /*
     * Size of the font used to the links
     */
    private float linkFontSize = 10f;

    /*
     * Size of the font used to the headers
     */
    private float headerFontSize = 12f;

    /*
     * The raw bitmap of the reply button
     */
    private byte[] replyButtonBitmap;

    /*
     * If an inline text exceeds the maxInlineSize, the text will be treated as an attachment instead of
     * an inline text. There will be no maximum if set to a negative value,
     */
    private int maxInlineSize = SizeUtils.KB * 256;

    /*
     * Inner class for storing meta information of a create PDF message
     */
    protected static class MessageMetaInfo
    {
        private List<String> froms;
        private String subject;
        private Collection<Part> attachments;

        List<String> getFroms() {
            return froms;
        }

        void setFroms(List<String> froms) {
            this.froms = froms;
        }

        String getSubject() {
            return subject;
        }

        void setSubject(String subject) {
            this.subject = subject;
        }

        Collection<Part> getAttachments() {
            return attachments;
        }

        void setAttachments(Collection<Part> attachments) {
            this.attachments = attachments;
        }
    }

    public MessagePDFBuilderImpl()
    {
        // initialize the default ViewerPreferences
        this.viewerPreferences = getViewerPreferencesIntValue(
                ViewerPreference.PAGE_MODE_USE_ATTACHMENTS,
                ViewerPreference.DISPLAY_DOC_TITLE,
                ViewerPreference.PAGE_LAYOUT_ONE_COLUMN);
    }

    public void setViewerPreference(ViewerPreference... viewerPreferences) {
        this.viewerPreferences = getViewerPreferencesIntValue(viewerPreferences);
    }

    protected Document createDocument(Color backgroundColor)
    {
        Rectangle pageSize = new Rectangle(documentWidth, documentHeight);

        pageSize.setBackgroundColor(backgroundColor);

        Document document = new Document(pageSize, marginLeft, marginRight, marginTop, marginBottom);

        document.addCreator(CREATOR);

        return document;
    }

    private int getViewerPreferencesIntValue(ViewerPreference... viewerPreferences)
    {
        int intValue = 0;

        if (viewerPreferences != null)
        {
            for (ViewerPreference preference : viewerPreferences) {
                intValue = intValue | preference.intValue();
            }
        }

        return intValue;
    }

    protected int getviewerPreferencesInt() {
        return viewerPreferences;
    }

    protected PdfWriter createPdfWriter(@Nonnull Document document, @Nonnull OutputStream pdfStream)
    throws DocumentException
    {
        PdfWriter pdfWriter = PdfWriter.getInstance(document, pdfStream);

        if (fullCompression) {
            pdfWriter.setFullCompression();
        }

        pdfWriter.setViewerPreferences(viewerPreferences);

        return pdfWriter;
    }

    protected String getAttachmentFilename(@Nonnull Part part) {
        return getFilename(part, getDefaultAttachmentNameIfUnknown(part));
    }

    private void addAttachment(@Nonnull Part part, @Nonnull PdfWriter pdfWriter)
    throws IOException, MessagingException
    {
        String baseType;

        String contentType = null;

        try {
            contentType = part.getContentType();

            MimeType mimeType = new MimeType(contentType);

            baseType = mimeType.getBaseType();
        }
        catch (MimeTypeParseException e)
        {
            // Can happen when the content-type is not correct. Example with missing ; between charset and name:
            //
            // Content-Type: application/pdf;
            //      charset="Windows-1252" name="postcard2010.pdf"
            logger.warn("Unable to infer MimeType from content type. Fallback to application/octet-stream. " +
                "Content-Type: {}", contentType);

            baseType = MimeTypes.OCTET_STREAM;
        }

        String filename = getAttachmentFilename(part);

        PdfFileSpecification fileSpec = PdfFileSpecification.fileEmbedded(pdfWriter, null,
                filename, IOUtils.toByteArray(part.getInputStream()),
                true /* compress */, baseType, null);

        if (!PdfEncodings.isPdfDocEncoding(filename)) {
            // Use UNICODE encoding if the filename cannot be encoded with PdfDocEncoding
            fileSpec.setUnicodeFileName(filename, true);
        }

        pdfWriter.addFileAttachment(fileSpec);
    }

    protected void addAttachments(@Nonnull PdfWriter pdfWriter, @Nonnull Collection<Part> attachments)
    throws MessagingException, IOException
    {
        for (Part part : attachments) {
            addAttachment(part, pdfWriter);
        }
    }

    private void addTextPart(String text, @Nonnull Phrase bodyPhrase, @Nonnull FontSelector fontSelector)
    {
        if (StringUtils.isNotEmpty(text)) {
            bodyPhrase.add(fontSelector.process(StringUtils.defaultString(text)));
        }
    }

    private void addLinkPart(@Nonnull String link, @Nonnull Phrase bodyPhrase, Font linkFont)
    {
        String linkName = link;

        link = link.trim();

        Chunk anchor = new Chunk(linkName, linkFont);

        // An anchor needs http (or https)
        if (!link.toLowerCase().startsWith("http")) {
            link = "http://" + link;
        }

        anchor.setAnchor(link);

        bodyPhrase.add(anchor);
    }

    private Font createLinkFont()
    {
        // Font for anchors (links)
        Font linkFont = new Font();
        linkFont.setStyle(Font.UNDERLINE);
        linkFont.setColor(Color.BLUE);
        linkFont.setSize(linkFontSize);

        return linkFont;
    }

    private Font createHeaderFont()
    {
        // Font for the headers
        Font headerFont = new Font();
        headerFont.setStyle(Font.BOLD);
        headerFont.setSize(headerFontSize);

        return headerFont;
    }

    private FontSelector createBodyFontSelector() {
        return createFontSelector(FontFactory.HELVETICA, bodyFontSize, Font.NORMAL, Color.BLACK);
    }

    private FontSelector createHeaderFontSelector() {
        return createFontSelector(FontFactory.HELVETICA, headerFontSize, Font.BOLD, Color.BLACK);
    }

    protected FontSelector createFontSelector(String fontName, float size, int style, Color color)
    {
        FontSelector selector = new FontSelector();

        selector.addFont(FontFactory.getFont(fontName, size, style, color));

        // Add extra fonts provided by the FontProvider
        if (fontProvider != null)
        {
            Collection<Font> extraFonts = fontProvider.getFonts();

            for (Font extraFont : extraFonts) {
                selector.addFont(extraFont);
            }
        }

        // Supported CJK (Chinese, Japanese, Korean)
        //
        // his is the list of fonts supported in the iTextAsian.jar:
        // Chinese Simplified:
        //      STSong-Light and STSongStd-Light with the encodings UniGB-UCS2-H and UniGB-UCS2-V
        // Chinese Traditional:
        //      MHei-Medium, MSung-Light and MSungStd-Light with the encodings UniCNS-UCS2-H and UniCNS-UCS2-V
        // Japanese:
        //      HeiseiMin-W3, HeiseiKakuGo-W5 and KozMinPro-Regular with the encodings UniJIS-UCS2-H, UniJIS-UCS2-V,
        //      UniJIS-UCS2-HW-H and UniJIS-UCS2-HW-V
        // Korean:
        //      HYGoThic-Medium, HYSMyeongJo-Medium and HYSMyeongJoStd with the encodings UniKS-UCS2-H and UniKS-UCS2-V
        selector.addFont(FontFactory.getFont("KozMinPro-Regular", "UniJIS-UCS2-H", BaseFont.NOT_EMBEDDED, size, style, color));
        selector.addFont(FontFactory.getFont("MSung-Light", "UniCNS-UCS2-H", BaseFont.NOT_EMBEDDED, size, style, color));
        selector.addFont(FontFactory.getFont("HYGoThic-Medium", "UniKS-UCS2-H", BaseFont.NOT_EMBEDDED, size, style, color));

        return selector;
    }

    protected void addBody(@Nonnull Document document, @Nonnull String bodyText)
    throws DocumentException
    {
        // Body table will contain the body of the message
        PdfPTable bodyTable = new PdfPTable(1);
        bodyTable.setWidthPercentage(100f);

        bodyTable.setSplitLate(false);

        bodyTable.setSpacingBefore(5f);
        bodyTable.setHorizontalAlignment(Element.ALIGN_LEFT);

        // Font for anchors (links)
        Font linkFont = createLinkFont();

        FontSelector bodyFontSelector = createBodyFontSelector();

        PdfPCell bodyCell = new PdfPCell();

        bodyCell.setRunDirection(bidiMode.getRunDirection());

        // Body table will be white
        bodyCell.setGrayFill(1f);

        bodyCell.setPadding(10);

        Phrase bodyPhrase = new Phrase();

        bodyCell.setPhrase(bodyPhrase);

        // Matcher we need to convert links to clickable links
        Matcher urlMatcher = HTMLLinkify.HTTP_LINK_PATTERN.matcher(bodyText);

        String bodyTextPart;

        int currentIndex = 0;

        // Add body and links
        while(urlMatcher.find())
        {
            bodyTextPart = bodyText.substring(currentIndex, urlMatcher.start());

            addTextPart(bodyTextPart, bodyPhrase, bodyFontSelector);

            String linkPart = urlMatcher.group();

            if (linkPart != null)
            {
                addLinkPart(linkPart, bodyPhrase, linkFont);

                currentIndex = urlMatcher.start() + linkPart.length();
            }
        }

        bodyTextPart = bodyText.substring(currentIndex);

        addTextPart(bodyTextPart, bodyPhrase, bodyFontSelector);

        bodyTable.addCell(bodyCell);

        document.add(bodyTable);
    }

    protected String getFilename(@Nonnull Part part, String defaultIfEmpty)
    {
        String filename;

        filename = HeaderUtils.decodeTextQuietly(MimeUtils.getFilenameQuietly(part));

        if (StringUtils.isEmpty(filename))
        {
            try {
                filename = part.getDescription();
            }
            catch (MessagingException e) {
                // ignore
            }

            if (StringUtils.isEmpty(filename)) {
                filename = defaultIfEmpty;
            }
        }

        return filename;
    }

    private String getDefaultAttachmentNameIfUnknown(@Nonnull Part part)
    {
        String name = UNKNOWN_ATTACHMENT_FILENAME;

        try {
            if (part.isMimeType("text/plain")) {
                name = UNKNOWN_TEXT_FILENAME;
            }
        }
        catch (MessagingException e) {
            // ignore
        }

        return name;
    }

    protected String getAttachmentHeaderFilename(@Nonnull Part part) {
        return getFilename(part, getDefaultAttachmentNameIfUnknown(part));
    }

    protected String getAttachmentHeader(@Nonnull Collection<Part> attachments)
    {
        StrBuilder sb = new StrBuilder(256);

        for (Part attachment : attachments)
        {
            String filename = getAttachmentHeaderFilename(attachment);

            sb.append(filename);
            sb.appendSeparator("; ");
        }

        return sb.toString();
    }

    private Part convertRFC822(@Nonnull Part attachment)
    {
        try {
            Object o = attachment.getContent();

            if (o instanceof MimeMessage message)
            {
                // The embedded message will be converted to a PDF as well. We can in principle use the same
                // instance since we do not store any state but to be on the safe side we will make a clone
                // (future versions might introduce state without knowing that the same instance is reused for
                // a new message).
                MessagePDFBuilderImpl pdfBuilder = (MessagePDFBuilderImpl) this.clone();

                ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();

                pdfBuilder.internalBuildPDF(message, null, pdfStream);

                MimePart pdfPart = new MimeBodyPart();

                pdfPart.setDataHandler(new DataHandler(new ByteArrayDataSource(pdfStream.toByteArray(),
                        "application/pdf")));

                String filename = getFilename(attachment, UNKNOWN_RFC822_FILENAME);

                if (!filename.toLowerCase().endsWith(".pdf")) {
                    filename = filename + ".pdf";
                }

                pdfPart.setFileName(filename);

                return pdfPart;
            }
        }
        catch (IOException | MessagingException | CloneNotSupportedException e) {
            logger.error("Error trying to converting to RFC822.");
        }

        return attachment;
    }

    private List<Part> preprocessAttachments(@Nonnull List<Part> attachments)
    {
        List<Part> prepared = new LinkedList<>();

        for (Part attachment : attachments)
        {
            try {
                if (attachment.isMimeType("message/rfc822"))
                {
                    // We need to convert the attached message to a PDF.
                    Part pdfAttachment = convertRFC822(attachment);

                    prepared.add(pdfAttachment);
                }
                else {
                    prepared.add(attachment);
                }
            }
            catch (MessagingException e) {
                prepared.add(attachment);
            }
        }

        return prepared;
    }

    private void addReplyLink(@Nonnull Document document, @Nonnull String replyURL)
    throws DocumentException, IOException
    {
        PdfPTable replyTable = new PdfPTable(1);
        replyTable.setWidthPercentage(100f);

        replyTable.setSplitLate(false);

        Chunk anchor;

        // If there is a reply button image, use it. If no fallback to text
        if (replyButtonBitmap != null)
        {
            // Since we are not 100% certain that Image is thread safe, we will load it everytime
            Image image = Image.getInstance(replyButtonBitmap);

            anchor = new Chunk(image, 0, 0);
        }
        else {
            Font linkFont = new Font();

            linkFont.setStyle(Font.BOLD);
            linkFont.setColor(0, 0, 255);
            linkFont.setSize(headerFontSize);

            anchor = new Chunk("Reply", linkFont);
        }

        anchor.setAnchor(replyURL);

        Phrase phrase = new Phrase();

        phrase.add(anchor);

        PdfPCell cell = new PdfPCell(phrase);
        cell.setBorder(Rectangle.NO_BORDER);

        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        replyTable.addCell(cell);

        document.add(replyTable);
    }

    protected void addMetaInfo(@Nonnull Document document, @Nonnull MessageMetaInfo metaInfo)
    {
        List<String> froms = metaInfo.getFroms();

        if (froms != null)
        {
            for (String from : froms) {
                document.addAuthor(from);
            }
        }

        String subject = metaInfo.getSubject();

        if (subject != null)
        {
            document.addSubject(subject);
            document.addTitle(subject);
        }
    }

    protected void addAttachmentLinks(@Nonnull PdfWriter writer, @Nonnull PdfPTable headerTable,
            @Nonnull FontSelector headerFontSelector, @Nonnull Collection<Part> attachments)
    {

        PdfPCell attachmentsCell = new PdfPCell();

        attachmentsCell.setPhrase(headerFontSelector.process(StringUtils.defaultString(getAttachmentHeader(attachments))));
        // Attachment names can contain bidirectional text
        attachmentsCell.setRunDirection(bidiMode.getRunDirection());
        attachmentsCell.setBorder(Rectangle.NO_BORDER);

        headerTable.addCell(attachmentsCell);
    }

    protected void addAdditionalAttachments(MimeMessage message, Collection<Part> attachments)
    {
        // Empty on purpose
    }

    protected MessageMetaInfo buildMessagePDF(@Nonnull PdfWriter writer, @Nonnull Document document,
            @Nonnull MimeMessage message, MessagePDFBuilderParameters parameters)
    throws DocumentException, MessagingException, IOException
    {
        MessageMetaInfo metaInfo = new MessageMetaInfo();

        try {
            metaInfo.setFroms(EmailAddressUtils.addressesToStrings(EmailAddressUtils.getFromNonStrict(message),
                    true /* mime decode */));
        }
        catch (MessagingException e) {
            logger.warn("From address is not a valid email address.");
        }

        try {
            metaInfo.setSubject(MailUtils.getSafeSubject(message));
        }
        catch (MessagingException e) {
            logger.error("Error getting subject.", e);
        }

        addMetaInfo(document, metaInfo);

        List<String> tos = null;

        try {
            tos = EmailAddressUtils.addressesToStrings(EmailAddressUtils.getRecipientsNonStrict(message,
                    Message.RecipientType.TO), true /* mime decode */);
        }
        catch (MessagingException e) {
            logger.warn("To is not a valid email address.");
        }

        List<String> ccs = null;

        try {
            ccs = EmailAddressUtils.addressesToStrings(EmailAddressUtils.getRecipientsNonStrict(message,
                    Message.RecipientType.CC), true /* mime decode */);
        }
        catch (MessagingException e) {
            logger.warn("CC is not a valid email address.");
        }

        Date sentDate = null;

        try {
            sentDate = message.getSentDate();
        }
        catch (MessagingException e) {
            logger.error("Error getting sent date.", e);
        }

        boolean deepscan = parameters != null && parameters.isDeepScan();

        MessageParser messageParser = new MessageParser(deepscan);

        // Set a limit to the size of an inline body since handling extremely large
        // inline texts is extremely slow
        messageParser.setMaxInlineSize(maxInlineSize);

        try {
            messageParser.parseMessage(message);
        }
        catch (PartException e) {
            throw new MessagingException("Error parsing message", e);
        }

        List<Part> attachments = messageParser.getAttachments();

        attachments = preprocessAttachments(attachments);

        addAdditionalAttachments(message, attachments);

        metaInfo.setAttachments(attachments);

        PdfPTable headerTable = new PdfPTable(2);

        headerTable.setHorizontalAlignment(Element.ALIGN_LEFT);
        headerTable.setWidthPercentage(100);
        headerTable.setWidths(new int[]{1, 6});
        headerTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        Font headerFont = createHeaderFont();

        FontSelector headerFontSelector = createHeaderFontSelector();

        PdfPCell cell = new PdfPCell(new Paragraph("From:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);

        String decodedFroms = StringUtils.defaultString(StringUtils.join(metaInfo.getFroms(), ", "));

        PdfPCell fromCell = new PdfPCell();

        fromCell.setPhrase(headerFontSelector.process(decodedFroms));
        // From can contain bidirectional text
        fromCell.setRunDirection(bidiMode.getRunDirection());
        fromCell.setBorder(Rectangle.NO_BORDER);

        headerTable.addCell(fromCell);

        cell = new PdfPCell(new Paragraph("To:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);
        headerTable.addCell(headerFontSelector.process(StringUtils.defaultString(StringUtils.join(tos, ", "))));

        cell = new PdfPCell(new Paragraph("CC:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);
        headerTable.addCell(headerFontSelector.process(StringUtils.defaultString(StringUtils.join(ccs, ", "))));

        cell = new PdfPCell(new Paragraph("Subject:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);

        PdfPCell subjectCell = new PdfPCell();

        subjectCell.setPhrase(headerFontSelector.process(StringUtils.defaultString(metaInfo.getSubject())));
        // Subject can contain bidirectional text
        subjectCell.setRunDirection(bidiMode.getRunDirection());
        subjectCell.setBorder(Rectangle.NO_BORDER);

        headerTable.addCell(subjectCell);

        cell = new PdfPCell(new Paragraph("Date:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);
        headerTable.addCell(ObjectUtils.toString(sentDate));

        cell = new PdfPCell(new Paragraph("Attachments:", headerFont));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

        headerTable.addCell(cell);

        addAttachmentLinks(writer, headerTable, headerFontSelector, attachments);

        document.add(headerTable);

        String replyURL = parameters != null ? parameters.getReplyURL() : null;

        if (replyURL != null) {
            addReplyLink(document, replyURL);
        }

        if (CollectionUtils.isNotEmpty(messageParser.getInlineTextParts()))
        {
            for (Part bodyPart : messageParser.getInlineTextParts())
            {
                Object content = bodyPart.getContent();

                if (!(content instanceof String)) {
                    throw new MessagingException("Bodypart is not a valid text part but a: " + content.getClass());
                }

                // PDF does not have tab support so, we convert tabs to spaces
                addBody(document, StringReplaceUtils.replaceTabsWithSpaces((String) content, tabWidth));
            }
        }
        else {
            // No inline text body found was found. Use a default text
            addBody(document, NO_INLINE_TEXT_BODY);
        }

        addFooter(writer, document);

        return metaInfo;
    }

    protected void addFooter(@Nonnull PdfWriter writer, @Nonnull Document document)
    {
        Phrase footer = new Phrase(FOOTER_TEXT);

        PdfContentByte cb = writer.getDirectContent();

        ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT, footer, document.right(), document.bottom(), 0);
    }

    protected void internalBuildPDF(@Nonnull MimeMessage message, MessagePDFBuilderParameters parameters,
            @Nonnull OutputStream pdfStream)
    throws MessagingException, IOException
    {

        Document document = null;

        try {
            document = createDocument(backgroundColor);

            PdfWriter writer = createPdfWriter(document, pdfStream);

            document.open();

            MessageMetaInfo metaInfo = buildMessagePDF(writer, document, message, parameters);

            addAttachments(writer, metaInfo.getAttachments());
        }
        catch (DocumentException e) {
            throw new IOException(e);
        }
        finally {
            // Close the document (this will also close the PdfWriter)
            if (document != null) {
                document.close();
            }
        }
    }

    @Override
    public void buildPDF(@Nonnull MimeMessage message, MessagePDFBuilderParameters parameters,
            @Nonnull OutputStream pdfStream)
    throws MessagingException, IOException
    {
        internalBuildPDF(message, parameters, pdfStream);
    }

    public FontProvider getFontProvider() {
        return fontProvider;
    }

    public void setFontProvider(FontProvider fontProvider) {
        this.fontProvider = fontProvider;
    }

    public void setBIDIMode(BIDIMode bidiMode) {
        this.bidiMode = bidiMode;
    }

    public BIDIMode getBIDIMode() {
        return bidiMode;
    }

    public void setReplyButtonResource(Resource imageResource)
    throws IOException
    {
        replyButtonBitmap = imageResource != null ? imageResource.getContentAsByteArray() : null;
    }

    public void setReplyButtonImage(byte[] replyButtonBitmap) {
        this.replyButtonBitmap = replyButtonBitmap;
    }

    public float getDocumentWidth() {
        return documentWidth;
    }

    public void setDocumentWidth(float documentWidth) {
        this.documentWidth = documentWidth;
    }

    public float getDocumentHeight() {
        return documentHeight;
    }

    public void setDocumentHeight(float documentHeight) {
        this.documentHeight = documentHeight;
    }

    public float getMarginLeft() {
        return marginLeft;
    }

    public void setMarginLeft(float marginLeft) {
        this.marginLeft = marginLeft;
    }

    public float getMarginRight() {
        return marginRight;
    }

    public void setMarginRight(float marginRight) {
        this.marginRight = marginRight;
    }

    public float getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(float marginTop) {
        this.marginTop = marginTop;
    }

    public float getMarginBottom() {
        return marginBottom;
    }

    public void setMarginBottom(float marginBottom) {
        this.marginBottom = marginBottom;
    }

    public boolean isFullCompression() {
        return fullCompression;
    }

    public void setFullCompression(boolean fullCompression) {
        this.fullCompression = fullCompression;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getTabWidth() {
        return tabWidth;
    }

    public void setTabWidth(int tabWidth) {
        this.tabWidth = tabWidth;
    }

    public float getBodyFontSize() {
        return bodyFontSize;
    }

    public void setBodyFontSize(float bodyFontSize) {
        this.bodyFontSize = bodyFontSize;
    }

    public float getLinkFontSize() {
        return linkFontSize;
    }

    public void setLinkFontSize(float linkFontSize) {
        this.linkFontSize = linkFontSize;
    }

    public float getHeaderFontSize() {
        return headerFontSize;
    }

    public void setHeaderFontSize(float headerFontSize) {
        this.headerFontSize = headerFontSize;
    }

    public int getMaxInlineSize() {
        return maxInlineSize;
    }

    public void setMaxInlineSize(int maxInlineSize) {
        this.maxInlineSize = maxInlineSize;
    }
}
