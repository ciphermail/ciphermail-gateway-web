/*
 * Copyright (c) 2015-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Publishes stats notifications to all the regstered {@link StatsSubscriber}'s
 *
 * @author Martijn Brinkers
 *
 */
public class StatsPublisherImpl implements StatsPublisher
{
    private static final Logger logger = LoggerFactory.getLogger(StatsPublisherImpl.class);

    /*
     * The registered stats subscribers that listen for stats notifications
     */
    private final List<StatsSubscriber> subscribers = Collections.synchronizedList(new LinkedList<StatsSubscriber>());

    /**
     * Notifies all the registered {@link StatsSubscriber}'s of the stats event
     */
    @Override
    public void publishStats(@Nonnull String name, @Nonnull StatsEvent statsEvent)
    throws IOException
    {
        for (StatsSubscriber subscriber : subscribers) {
            subscriber.notifyStats(name, statsEvent);
        }
    }

    /**
     * Add the StatsSubscriber to the list of registered subscribers
     */
    @Override
    public void addSubscriber(@Nonnull StatsSubscriber subscriber)
    {
        logger.info("Registering StatsSubscriber {}", subscriber.getClass().getName());

        subscribers.add(subscriber);
    }
}
