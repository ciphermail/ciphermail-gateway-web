/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.StringReader;

/**
 * Extension of SendMailTemplate that makes it possible to read the template from the sender's properties
 */
public class SenderTemplatePropertySendMail extends SendMailTemplate
{
    private static final Logger logger = LoggerFactory.getLogger(SenderTemplatePropertySendMail.class);

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        TEMPLATE_PROPERTY ("templateProperty");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }
    /*
     * The name of the property from which the template is read
     */
    private String templateProperty;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailetTransacted()
    throws MessagingException
    {
        super.initMailetTransacted();

        templateProperty = getInitParameter(Parameter.TEMPLATE_PROPERTY.name);

        getLogger().info("templateProperty {}", templateProperty);
    }

    /*
     * Reads the template from the user
     */
    protected Template getTemplateFromUser(User user, SimpleHash root)
    throws MessagingException, HierarchicalPropertiesException, IOException
    {
        Template template = null;

        UserProperties properties = user.getUserPreferences().getProperties();

        // Load the template from the user properties
        String templateSource = properties.getProperty(templateProperty);

        if (templateSource != null)
        {
            StringReader reader = new StringReader(templateSource);

            template = getTemplateBuilder().createTemplate(reader);
        }

        return template;
    }

    /*
     * The template will be retrieved from the user property. If no template is available, the
     * static template specified in the mailet config will be used.
     */
    @Override
    protected Template getTemplate(Mail mail, final SimpleHash root)
    throws IOException, MessagingException
    {
        Template template = null;

        try {
            InternetAddress originator = getMessageOriginatorIdentifier().getOriginator(mail);

            User user = getUserWorkflow().getUser(originator.getAddress(),
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

            if (templateProperty != null) {
                template = getTemplateFromUser(user, root);
            }

            if (template == null)
            {
                getLogger().debug("No template found for {}", originator);
                // The sender was not a valid user or did not have a template. Use the default static
                // template specified in the mailet config.
                template = super.getTemplate(mail, root);
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error getting template.", e);
        }

        return template;
    }
}
