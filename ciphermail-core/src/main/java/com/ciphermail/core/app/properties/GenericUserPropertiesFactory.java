/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalProperties;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.lang.UnhandledException;
import org.springframework.context.ApplicationContext;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

/**
 * UserPropertiesFactory implementation which is responsible for creating property instances which have a public
 * constructor with only one parameter of type HierarchicalProperties. This factory will be used as the default
 * factory unless there is a class specific factory available
 */
public class GenericUserPropertiesFactory<T> implements UserPropertiesFactory
{
    /*
     * Spring ApplicationContext
     */
    private final ApplicationContext applicationContext;

    /*
    * The class for which an instance should be created
     */
    private final Class<T> clazz;

    public GenericUserPropertiesFactory(
            @Nonnull ApplicationContext applicationContext,
            @Nonnull Class<T> clazz)
    {
        this.applicationContext = Objects.requireNonNull(applicationContext);
        this.clazz = Objects.requireNonNull(clazz);
    }

    @Nonnull
    @Override
    public T createInstance(@Nonnull HierarchicalProperties properties) {
        return createInstance(properties, false);
    }

    @Nonnull
    @Override
    public T createInstance(@Nonnull HierarchicalProperties properties, boolean autoWireProperties)
    {
        if (autoWireProperties) {
            applicationContext.getAutowireCapableBeanFactory().autowireBean(properties);
        }

        T object;

        try {
            object = ConstructorUtils.invokeConstructor(clazz, properties);
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new UnhandledException(e);
        }

        applicationContext.getAutowireCapableBeanFactory().autowireBean(object);

        return object;
    }
}
