/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import com.ciphermail.core.common.util.RequiredByJames;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@RequiredByJames
public interface MessageOriginatorIdentifier
{
    /**
     * Returns the email address of the 'sender' of the message. The 'sender' can be for example the
     * envelope sender of the FROM header. It's implementation dependent which email address is returned.
     * Note: if there is no valid originator, for example there is no from or sender, or the originator
     * is an invalid email address, a dummy email address will be returned (EmailAddressUtils.INVALID_EMAIL).
     *
     * @param mail the Mail object from which to extract the originator email address
     * @return the InternetAddress of the message originator, or a dummy email address if no valid originator is found
     * @throws MessagingException if there is an error parsing the email addresses
     */
    @Nonnull InternetAddress getOriginator(@Nonnull Mail mail)
    throws MessagingException;

    /**
     * Returns the email address of the 'sender' of the message. The 'sender' can be for example the
     * envelope sender of the FROM header. It's implementation dependent which email address is returned.
     * Note: if there is no valid originator, for example there is no from or sender, or the originator
     * is an invalid email address, a dummy email address will be returned (EmailAddressUtils.INVALID_EMAIL).
     *
     * @param message the MimeMessage from which to extract the originator email address
     * @return the InternetAddress of the message originator, or a dummy email address if no valid originator is found
     * @throws MessagingException if there is an error parsing the email addresses
     */
    @Nonnull InternetAddress getOriginator(MimeMessage message)
    throws MessagingException;

    /**
     * Identifies and returns the originator email address from the provided array of email addresses.
     * If the array contains multiple email addresses, the first valid one is selected. If no valid
     * email address is found and the dummyIfNull flag is set to true, a dummy email address will be returned.
     *
     * @param fromHeaders an array of email addresses to be evaluated as potential originators
     * @param dummyIfNull a boolean flag indicating whether to return a dummy email address if no valid originator is found
     * @return the InternetAddress of the identified originator, or a dummy email address if specified and no valid address is found
     * @throws MessagingException if there is an error parsing the email addresses
     */
    InternetAddress getOriginator(String[] fromHeaders, boolean dummyIfNull)
    throws MessagingException;
}
