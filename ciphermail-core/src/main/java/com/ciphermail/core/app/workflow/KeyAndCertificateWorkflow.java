/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow;

import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import com.ciphermail.core.common.util.RequiredByJames;

import javax.annotation.Nonnull;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

@RequiredByJames
public interface KeyAndCertificateWorkflow extends CertificateWorkflow
{
    /**
     * Imports all the keys and associated certificates from the key store. Returns the
     * imported entries. Before importing, a new database transaction is started.
     * It will return all entries found in the KeyStore including any existing entry.
     * Chain entries will also be imported and included with the result
     */
    List<X509CertStoreEntry> importKeyStoreTransacted(@Nonnull KeyStore keyStore,
            @Nonnull MissingKeyAction missingKeyAction)
    throws KeyStoreException;

    /**
     * Returns a PKCS#12 encoded file (PFX) containing the certificates and the associated keys (if present).
     * If include root is true, the root certificate, if available, will be included.
     * <p>
     * The PFX will be encrypted with the given password. The password will be cleared (ie all
     * elements of the password will be set to 0). The resulting PFX will be written to the output stream.
     * <p>
     * Before importing, a new database transaction is started.
     */
    void copyKeysToPKCS12OutputstreamTransacted(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException;

    /**
     * Returns a PEM encoded file containing the certificates and the associated keys (if present).
     * If include root is true, the root certificate, if available, will be included.
     * <p>
     * The PEM will be encrypted with the given password. The password will be cleared (ie all
     * elements of the password will be set to 0). The resulting PEM will be written to the output stream.
     * <p>
     *  Before importing, a new database transaction is started.
     */
    void copyKeysToPEMOutputstreamTransacted(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException;
}
