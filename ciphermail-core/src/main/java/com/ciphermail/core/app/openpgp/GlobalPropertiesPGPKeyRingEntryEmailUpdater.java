/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.openpgp;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.properties.PGPProperties;
import com.ciphermail.core.app.properties.PGPPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntryEmailUpdaterImpl;
import com.ciphermail.core.common.security.openpgp.PGPUserIDRevocationChecker;
import com.ciphermail.core.common.security.openpgp.PGPUserIDValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Extension of PGPKeyRingEntryEmailUpdaterImpl which gets some settings from the global properties
 *
 * @author Martijn Brinkers
 *
 */
public class GlobalPropertiesPGPKeyRingEntryEmailUpdater extends PGPKeyRingEntryEmailUpdaterImpl
{
    private static final Logger logger = LoggerFactory.getLogger(GlobalPropertiesPGPKeyRingEntryEmailUpdater.class);

    /*
     * For getting the global properties
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public GlobalPropertiesPGPKeyRingEntryEmailUpdater(
            @Nonnull PGPUserIDValidator userIDValidator,
            @Nonnull PGPUserIDRevocationChecker userIDRevocationChecker,
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        super(userIDValidator, userIDRevocationChecker);

        this.globalPreferencesManager = Objects.requireNonNull(globalPreferencesManager);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public boolean isUpdateEmailAddresses()
    {
        boolean updateEmailAddresses = false;

        try {
            PGPProperties pgpProperties = userPropertiesFactoryRegistry.getFactoryForClass(PGPPropertiesImpl.class)
                    .createInstance(globalPreferencesManager.getGlobalUserPreferences()
                            .getProperties());

            updateEmailAddresses = pgpProperties.getPGPAutoUpdateEmailAddresses();
        }
        catch (Exception e) {
            logger.error("Error in #isUpdateEmailAddresses", e);
        }

        return updateEmailAddresses;
    }
}
