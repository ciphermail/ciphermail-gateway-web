/*
 * Copyright (c) 2011-2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.PortalEmailSigner;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.PortalSignupDetails;
import com.ciphermail.core.app.james.PortalSignups;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.security.GeneralSecurityException;
import java.util.Objects;

/*
 * Mailet that creates a Signup for every recipient.
 */
@SuppressWarnings({"java:S6813"})
public class CreatePortalSignup extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(CreatePortalSignup.class);

    /*
     * The activation context key
     */
    @VisibleForTesting
    protected static final String ACTIVATION_CONTEXT_KEY = "invites";

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ALGORITHM ("algorithm");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The HMAC algorithm used by the PortalInvitationValidator. Leave null for the default value
     */
    private String algorithm;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Callback used to track user objects.
     */
    @VisibleForTesting
    protected static class ActivationContext
    {
        /*
         * The invitations
         */
        private final PortalSignups invitations = new PortalSignups();

        public PortalSignups getInvitations() {
            return invitations;
        }

        public void clear() {
            invitations.clear();
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);

        algorithm = getInitParameter(Parameter.ALGORITHM.name);

        StrBuilder sb = new StrBuilder();

        sb.append("; algorithm: ");
        sb.append(algorithm);

        getLogger().info("{}", sb);
    }

    private long getTimestamp() {
        return System.currentTimeMillis();
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(Mail mail, User user)
    throws MessagingException
    {
        try {
            ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                    ActivationContext.class);

            String clientSecret = user.getUserPreferences().getProperties().getClientSecret();

            if (StringUtils.isNotEmpty(clientSecret))
            {
                long timestamp = getTimestamp();

                PortalEmailSigner emailSigner = new PortalEmailSigner(user.getEmail(), timestamp,
                        PortalEmailSigner.PORTAL_SIGNUP);

                if (algorithm != null) {
                    emailSigner.setAlgorithm(algorithm);
                }

                activationContext.getInvitations().put(user.getEmail(),
                        new PortalSignupDetails(user.getEmail(), timestamp, emailSigner.calculateMAC(clientSecret)));
            }
            else {
                // Should not happen because the client secret should always be created for the user
                getLogger().atWarn().log(createLogLine("Client secret not set for user {}"), user.getEmail());
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error handling user " + user.getEmail(), e);
        }
        catch (GeneralSecurityException e) {
            throw new MessagingException("Error calculating MAC for user " + user.getEmail(), e);
        }
    }

    @Override
    public void serviceMail(final Mail mail)
    {
        try {
            ActivationContext activationContext = new ActivationContext();

            // Place the ActivationContext in the context so onHandleUserEvent can use it
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, activationContext);

            MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                    user -> onHandleUserEvent(mail, user), getLogger());

            RetryListener retryListener = new AbstractRetryListener()
            {
                @Override
                public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                        Throwable throwable)
                {
                    activationContext.clear();
                }
            };

            mailAddressHandler.handleMailAddressesWithRetry(MailAddressUtils.getRecipients(mail), retryListener);

            CoreApplicationMailAttributes.setPortalSignups(mail, activationContext.getInvitations());
        }
        catch(MessagingException e) {
            getLogger().error("Error creating signup.", e);
        }
    }
}
