/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

import javax.annotation.Nonnull;

/**
 * Java interface wrapper around hierarchical properties for the DLP properties
 */
public interface DLPProperties
{
    /**
     * If true, DLP is enabled for the user
     */
    void setEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getEnabled()
    throws HierarchicalPropertiesException;

    /**
     * A comma separated list of email addresses to which DLP notifications
     * (for example warnings) should be sent.
     * <p>
     * Note: the email address must be valid email addresses.
     */
    void setDLPManagers(String recipients)
    throws HierarchicalPropertiesException;

    String getDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, warnings, are send to the originator
     */
    void setSendWarningToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendWarningToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, warnings, are send to the DLP managers
     */
    void setSendWarningToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendWarningToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, Quarantine warnings, are send to the originator
     */
    void setSendQuarantineToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendQuarantineToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, Quarantine warnings, are send to the DLP managers
     */
    void setSendQuarantineToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendQuarantineToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, Block warnings, are send to the originator
     */
    void setSendBlockToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendBlockToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, Block warnings, are send to the DLP managers
     */
    void setSendBlockToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendBlockToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, Error warnings, are send to the originator
     */
    void setSendErrorToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendErrorToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, Error warnings, are send to the DLP managers
     */
    void setSendErrorToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendErrorToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, and an error occurs during DLP, the message will be put in quarantine
     */
    void setQuarantineOnError(Boolean quarantine)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getQuarantineOnError()
    throws HierarchicalPropertiesException;

    /**
     * If true, and a message cannot be encrypted and encryption is mandatory, the message will be quarantined
     */
    void setQuarantineOnFailedEncryption(Boolean quarantine)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getQuarantineOnFailedEncryption()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is sent to the originator when the email is released from quarantine.
     */
    void setSendReleaseNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendReleaseNotifyToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is sent to the DLP managers when the email is released from quarantine.
     */
    void setSendReleaseNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendReleaseNotifyToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is sent to the originator when the email is deleted from quarantine.
     */
    void setSendDeleteNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendDeleteNotifyToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is sent to the DLP managers when the email is deleted from quarantine.
     */
    void setSendDeleteNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendDeleteNotifyToDLPManagers()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is sent to the originator when the email expires from quarantine.
     */
    void setSendExpireNotifyToOriginator(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendExpireNotifyToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification is send to the DLP managers when the email expires from quarantine.
     */
    void setSendExpireNotifyToDLPManagers(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSendExpireNotifyToDLPManagers()
    throws HierarchicalPropertiesException;
}
