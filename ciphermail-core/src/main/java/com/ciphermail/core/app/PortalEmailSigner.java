/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.Base32Utils;
import com.ciphermail.core.common.util.MiscStringUtils;

import javax.annotation.Nonnull;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Objects;

/**
 * The PortalEmailSigner class is used to generate HMAC signatures for email links
 * such as portal signups or password resets. It uses a specified algorithm to
 * calculate the MAC (Message Authentication Code) based on email, timestamp, and a unique ID.
 */
public class PortalEmailSigner
{
    /**
     * The ID for the portal signup link
     */
    public static final byte[] PORTAL_SIGNUP = "portal-signup".getBytes(StandardCharsets.UTF_8);
    /**
     * The ID for the portal forgot password link
     */
    public static final byte[] PORTAL_RESET_PASSWORD = "portal-reset-password".getBytes(StandardCharsets.UTF_8);

    /*
     * The default hmac algorithm.
     */
    private static final String DEFAULT_ALGORITHM = "HmacSHA256";

    /*
     * The HMAC algorithm to use
     */
    private String algorithm = DEFAULT_ALGORITHM;

    /*
     * The email address of the account
     */
    private final String email;

    /*
     * The timestamp (can be used to check whether the link is still valid)
     */
    private final long timestamp;

    /*
     * Some id which can be set to identify a specific portal invitation
     */
    private final byte[] id;

    public PortalEmailSigner(@Nonnull String email, long timestamp, @Nonnull byte[] id)
    {
        this.email = Objects.requireNonNull(EmailAddressUtils.canonicalizeAndValidate(email, true));
        this.timestamp = timestamp;
        this.id = Objects.requireNonNull(id);
    }

    private Mac createMAC(String key)
    throws GeneralSecurityException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        Mac mac = securityFactory.createMAC(algorithm);

        SecretKeySpec keySpec = new SecretKeySpec(MiscStringUtils.getBytesUTF8(key), "raw");

        mac.init(keySpec);

        return mac;
    }

    /**
     * returns the calculated MAC
     */
    public String calculateMAC(@Nonnull String key)
    throws GeneralSecurityException
    {
        Mac mac = createMAC(key);

        mac.update(id);
        mac.update(email.getBytes(StandardCharsets.UTF_8));
        mac.update(Long.toString(timestamp).getBytes(StandardCharsets.US_ASCII));

        return Base32Utils.base32Encode(mac.doFinal());
    }
    public String getAlgorithm() {
        return algorithm;
    }

    public PortalEmailSigner setAlgorithm(String algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }
}
