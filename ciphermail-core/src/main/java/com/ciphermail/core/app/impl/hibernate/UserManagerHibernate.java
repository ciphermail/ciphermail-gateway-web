/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserManager;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.hibernate.CloseSessionOnCloseIterator;
import com.ciphermail.core.common.hibernate.HibernateUtils;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.smime.selector.CertificateSelector;
import com.ciphermail.core.common.util.CloseableIterator;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.jdbc.ReturningWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.sql.SQLException;
import java.util.Objects;

public class UserManagerHibernate implements UserManager
{
    private static final Logger logger = LoggerFactory.getLogger(UserManagerHibernate.class);


    private final PKISecurityServices pKISecurityServices;

    /*
     * Selector that selects encryption certificates for the user.
     */
    private final CertificateSelector encryptionCertificateSelector;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    public UserManagerHibernate(
            @Nonnull PKISecurityServices pKISecurityServices,
            @Nonnull CertificateSelector encryptionCertificateSelector,
            @Nonnull SessionManager sessionManager,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.pKISecurityServices = Objects.requireNonNull(pKISecurityServices);
        this.encryptionCertificateSelector = Objects.requireNonNull(encryptionCertificateSelector);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public @Nonnull User addUser(@Nonnull String email, AddMode addMode)
    throws AddressException
    {
        // only accept valid email addresses. Email addresses need to be normalized as well. The caller is
        // responsible for normalizing and validating the email address so this check is more or less a sanity check.
        String validatedUser = EmailAddressUtils.canonicalizeAndValidate(email, true);

        if (validatedUser == null) {
            throw new AddressException(email + " is not a valid email address.");
        }

        return new UserHibernate(getDAO().addUser(validatedUser, addMode == AddMode.PERSISTENT),
                pKISecurityServices, encryptionCertificateSelector, sessionManager.getSession(),
                userPropertiesFactoryRegistry);
    }

    @Override
    public boolean deleteUser(@Nonnull User user)
    {
        boolean deleted = false;

        if (user instanceof UserHibernate userHibernate)
        {
            getDAO().delete(userHibernate.getUserEntity());

            deleted = true;
        }
        else {
            logger.warn("User cannot be deleted because user is not a UserHibernate.");
        }

        return deleted;
    }

    @Override
    public User getUser(@Nonnull String email)
    throws AddressException
    {
        // only accept valid email addresses. Email addresses need to be normalized as well. The caller is
        // responsible for normalizing and validating the email address so this check is more or less a sanity check.
        String validatedUser = EmailAddressUtils.canonicalizeAndValidate(email, true);

        if (validatedUser == null) {
            throw new AddressException(email + " is not a valid email address");
        }

        User user = null;

        UserEntity entity = getDAO().getUser(validatedUser);

        if (entity != null)
        {
            user = new UserHibernate(entity, pKISecurityServices, encryptionCertificateSelector,
                    sessionManager.getSession(), userPropertiesFactoryRegistry);
        }

        return user;
    }

    @Override
    public long getUserCount() {
        return getDAO().getUserCount();
    }

    @Override
    public boolean isPersistent(@Nonnull User user)
    {
        boolean persisted = false;

        if (user instanceof UserHibernate userHibernate) {
            persisted = (userHibernate.getUserEntity().getId() != null);
        }
        else {
            logger.warn("User cannot be persisted because user is not a UserHibernate.");
        }

        return persisted;
    }

    @Override
    public User makePersistent(@Nonnull User user)
    {
        if (user instanceof UserHibernate userHibernate)
        {
            getDAO().persist(userHibernate.getUserEntity());
        }
        else {
            logger.warn("User cannot be persisted because user is not a UserHibernate.");
        }

        return user;
    }

    @Override
    public @Nonnull CloseableIterator<String> getEmailIterator()
    {
        return getEmailIterator(null, null, SortDirection.ASC);
    }

    @Override
    public @Nonnull CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        return session.doReturningWork((ReturningWork<CloseableIterator<String>>) connection ->
        {
            StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

            try {
                return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).getEmailIterator(
                        firstResult, maxResults, sortDirection),
                        SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
            }
            catch (Exception e) {
                HibernateUtils.closeSessionQuietly(statelessSession);

                throw new SQLException(e);
            }
        });
    }

    @Override
    public @Nonnull CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        return session.doReturningWork((ReturningWork<CloseableIterator<String>>) connection ->
        {
            StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

            try {
                return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).searchEmail(
                            search, firstResult, maxResults, sortDirection),
                        SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
            }
            catch (Exception e)
            {
                HibernateUtils.closeSessionQuietly(statelessSession);

                throw new SQLException(e);
            }
        });
    }

    @Override
    public long getSearchEmailCount(@Nonnull String search)
    {
        StatelessSession statelessSession = sessionManager.openStatelessSession();

        try {
            return getStatelessDAO(statelessSession).getSearchEmailCount(search);
        }
        finally {
            HibernateUtils.closeSessionQuietly(statelessSession);
        }
    }

    private UserDAO getDAO() {
        return UserDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private UserDAO getStatelessDAO(@Nonnull StatelessSession statelessSession) {
        return UserDAO.getInstance(SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
    }
}
