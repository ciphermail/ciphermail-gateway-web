/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.security.openpgp.PGPCompressionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPEncoding;
import com.ciphermail.core.common.security.openpgp.PGPEncryptionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPHashAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;

import javax.annotation.Nonnull;

/**
 * Implementation of PGPProperties
 */
@UserPropertiesType(name = "pgp", displayName = "PGP", order = 40)
public class PGPPropertiesImpl extends DelegatedHierarchicalProperties implements PGPProperties
{
    /*
     * User property names
     */
    private static final String PGP_ENABLED                        = "pgp-enabled";
    private static final String PGP_INCOMING_INLINE_ENABLED        = "pgp-incoming-inline-enabled";
    private static final String PGP_SCAN_HTML_FOR_PGP              = "pgp-scan-html-for-pgp";
    private static final String PGP_SKIP_NON_PGP_EXTENSIONS        = "pgp-skip-non-pgp-extensions";
    private static final String PGP_PARTITIONED_FIXUP              = "pgp-partitioned-fixup";
    private static final String PGP_SIGNING_KEY                    = "pgp-signing-key";
    private static final String PGP_AUTO_SELECT_SIGNING_ALGORITHM  = "pgp-auto-select-signing-algorithm";
    private static final String PGP_SIGNING_ALGORITHM              = "pgp-signing-algorithm";
    private static final String PGP_ENCRYPTION_ALGORITHM           = "pgp-encryption-algorithm";
    private static final String PGP_COMPRESSION_ALGORITHM          = "pgp-compression-algorithm";
    private static final String PGP_MAX_SIZE                       = "pgp-max-size";
    private static final String PGP_CONVERT_HTML_TO_TEXT           = "pgp-convert-html-to-text";
    private static final String PGP_ADD_INTEGRITY_PACKET           = "pgp-add-integrity-packet";
    private static final String PGP_ENCODING                       = "pgp-encoding";
    private static final String PGP_KEY_GENERATION_TYPE            = "pgp-key-generation-type";
    private static final String PGP_AUTO_PUBLISH_KEYS              = "pgp-auto-publish-keys";
    private static final String PGP_AUTO_REQUEST_KEY               = "pgp-auto-request-key";
    private static final String PGP_AUTO_IMPORT_KEYS_FROM_MAIL     = "pgp-auto-import-keys-from-mail";
    private static final String PGP_AUTO_REMOVE_KEYS_FROM_MAIL     = "pgp-auto-remove-keys-from-mail";
    private static final String PGP_REMOVE_SIGNATURE               = "pgp-remove-signature";
    private static final String PGP_AUTO_UPDATE_EMAIL_ADDRESSES    = "pgp-auto-update-email-addresses";
    private static final String PGP_SKIP_SIGN_ONLY                 = "pgp-skip-sign-only";
    private static final String PGP_KEY_SERVERS                    = "pgp-key-servers";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public PGPPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = PGP_ENABLED)
    public void setPGPEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = PGP_ENABLED,
            order = 10
    )
    public Boolean getPGPEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_ENABLED);
    }

    @Override
    @UserProperty(name = PGP_INCOMING_INLINE_ENABLED)
    public void setPGPIncomingInlineEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_INCOMING_INLINE_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = PGP_INCOMING_INLINE_ENABLED,
            order = 20
    )
    public Boolean getPGPIncomingInlineEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_INCOMING_INLINE_ENABLED);
    }

    @Override
    @UserProperty(name = PGP_SCAN_HTML_FOR_PGP)
    public void setPGPScanHTMLForPGP(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_SCAN_HTML_FOR_PGP, enabled);
    }

    @Override
    @UserProperty(name = PGP_SCAN_HTML_FOR_PGP,
            order = 30
    )
    public Boolean getPGPScanHTMLForPGP()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_SCAN_HTML_FOR_PGP);
    }

    @Override
    @UserProperty(name = PGP_SKIP_NON_PGP_EXTENSIONS)
    public void setPGPSkipNonPGPExtensions(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_SKIP_NON_PGP_EXTENSIONS, enabled);
    }

    @Override
    @UserProperty(name = PGP_SKIP_NON_PGP_EXTENSIONS,
            order = 40
    )
    public Boolean getPGPSkipNonPGPExtensions()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_SKIP_NON_PGP_EXTENSIONS);
    }

    @Override
    @UserProperty(name = PGP_PARTITIONED_FIXUP)
    public void setPGPPartitionedFixup(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_PARTITIONED_FIXUP, enabled);
    }

    @Override
    @UserProperty(name = PGP_PARTITIONED_FIXUP,
            visible = false
    )
    public Boolean getPGPPartitionedFixup()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_PARTITIONED_FIXUP);
    }

    @Override
    @UserProperty(name = PGP_SIGNING_KEY)
    public void setPGPSigningKey(String sha256Fingerprint)
    throws HierarchicalPropertiesException
    {
        setProperty(PGP_SIGNING_KEY, sha256Fingerprint);
    }

    @Override
    @UserProperty(name = PGP_SIGNING_KEY,
            visible = false,
            allowNull = true
    )
    public String getPGPSigningKey()
    throws HierarchicalPropertiesException
    {
        return getProperty(PGP_SIGNING_KEY);
    }

    @Override
    @UserProperty(name = PGP_AUTO_SELECT_SIGNING_ALGORITHM)
    public void setPGPAutoSelectSigningAlgorithm(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_SELECT_SIGNING_ALGORITHM, enabled);
    }

    @Override
    @UserProperty(name = PGP_AUTO_SELECT_SIGNING_ALGORITHM,
            order = 70
    )
    public Boolean getPGPAutoSelectSigningAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_SELECT_SIGNING_ALGORITHM);
    }

    @Override
    @UserProperty(name = PGP_SIGNING_ALGORITHM)
    public void setPGPSigningAlgorithm(PGPHashAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, PGP_SIGNING_ALGORITHM, algorithm);
    }

    @Override
    @UserProperty(name = PGP_SIGNING_ALGORITHM,
            order = 80
    )
    public PGPHashAlgorithm getPGPSigningAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, PGP_SIGNING_ALGORITHM,
                PGPHashAlgorithm.class);
    }

    @Override
    @UserProperty(name = PGP_ENCRYPTION_ALGORITHM)
    public void setPGPEncryptionAlgorithm(PGPEncryptionAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, PGP_ENCRYPTION_ALGORITHM, algorithm);
    }

    @Override
    @UserProperty(name = PGP_ENCRYPTION_ALGORITHM,
            order = 90
    )
    public PGPEncryptionAlgorithm getPGPEncryptionAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, PGP_ENCRYPTION_ALGORITHM,
                PGPEncryptionAlgorithm.class);
    }

    @Override
    @UserProperty(name = PGP_COMPRESSION_ALGORITHM)
    public void setPGPCompressionAlgorithm(PGPCompressionAlgorithm algorithm)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, PGP_COMPRESSION_ALGORITHM, algorithm);
    }

    @Override
    @UserProperty(name = PGP_COMPRESSION_ALGORITHM,
            order = 100
    )
    public PGPCompressionAlgorithm getPGPCompressionAlgorithm()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, PGP_COMPRESSION_ALGORITHM,
                PGPCompressionAlgorithm.class);
    }

    @Override
    @UserProperty(name = PGP_MAX_SIZE)
    public void setPGPMaxSize(Long size)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setLong(this, PGP_MAX_SIZE, size);
    }

    @Override
    @UserProperty(name = PGP_MAX_SIZE,
            order = 110
    )
    public Long getPGPMaxSize()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getLong(this, PGP_MAX_SIZE);
    }

    @Override
    @UserProperty(name = PGP_CONVERT_HTML_TO_TEXT)
    public void setPGPConvertHTMLToText(Boolean convertHTMLToText)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_CONVERT_HTML_TO_TEXT, convertHTMLToText);
    }

    @Override
    @UserProperty(name = PGP_CONVERT_HTML_TO_TEXT,
            order = 120
    )
    public Boolean getPGPConvertHTMLToText()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_CONVERT_HTML_TO_TEXT);
    }

    @Override
    @UserProperty(name = PGP_ADD_INTEGRITY_PACKET)
    public void setPGPAddIntegrityPacket(Boolean addIntegrityPacket)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_ADD_INTEGRITY_PACKET, addIntegrityPacket);
    }

    @Override
    @UserProperty(name = PGP_ADD_INTEGRITY_PACKET,
            order = 130
    )
    public Boolean getPGPAddIntegrityPacket()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_ADD_INTEGRITY_PACKET);
    }

    @Override
    @UserProperty(name = PGP_ENCODING)
    public void setPGPEncoding(PGPEncoding encoding)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, PGP_ENCODING, encoding);
    }

    @Override
    @UserProperty(name = PGP_ENCODING,
            order = 140
    )
    public PGPEncoding getPGPEncoding()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, PGP_ENCODING,
                PGPEncoding.class);
    }

    @Override
    @UserProperty(name = PGP_KEY_GENERATION_TYPE)
    public void setPGPKeyGenerationType(PGPKeyGenerationType type)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setEnum(this, PGP_KEY_GENERATION_TYPE, type);
    }

    @Override
    @UserProperty(name = PGP_KEY_GENERATION_TYPE,
            order = 150
    )
    public PGPKeyGenerationType getPGPKeyGenerationType()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getEnum(this, PGP_KEY_GENERATION_TYPE,
                PGPKeyGenerationType.class);
    }

    @Override
    @UserProperty(name = PGP_AUTO_PUBLISH_KEYS)
    public void setPGPAutoPublishKeys(Boolean autoPublishKeys)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_PUBLISH_KEYS, autoPublishKeys);
    }

    @Override
    @UserProperty(name = PGP_AUTO_PUBLISH_KEYS,
            order = 160
    )
    public Boolean getPGPAutoPublishKeys()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_PUBLISH_KEYS);
    }

    @Override
    @UserProperty(name = PGP_AUTO_REQUEST_KEY)
    public void setPGPAutoRequestKey(Boolean autoRequestKey)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_REQUEST_KEY, autoRequestKey);
    }

    @Override
    @UserProperty(name = PGP_AUTO_REQUEST_KEY,
            order = 170
    )
    public Boolean getPGPAutoRequestKey()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_REQUEST_KEY);
    }

    @Override
    @UserProperty(name = PGP_AUTO_IMPORT_KEYS_FROM_MAIL)
    public void setPGPAutoImportKeysFromMail(Boolean autoImport)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_IMPORT_KEYS_FROM_MAIL, autoImport);
    }

    @Override
    @UserProperty(name = PGP_AUTO_IMPORT_KEYS_FROM_MAIL,
            order = 180
    )
    public Boolean getPGPAutoImportKeysFromMail()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_IMPORT_KEYS_FROM_MAIL);
    }

    @Override
    @UserProperty(name = PGP_AUTO_REMOVE_KEYS_FROM_MAIL)
    public void setPGPAutoRemoveKeysFromMail(Boolean autoRemove)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_REMOVE_KEYS_FROM_MAIL, autoRemove);
    }

    @Override
    @UserProperty(name = PGP_AUTO_REMOVE_KEYS_FROM_MAIL,
            order = 190
    )
    public Boolean getPGPAutoRemoveKeysFromMail()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_REMOVE_KEYS_FROM_MAIL);
    }

    @Override
    @UserProperty(name = PGP_REMOVE_SIGNATURE)
    public void setPGPRemoveSignature(Boolean removeSignature)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_REMOVE_SIGNATURE, removeSignature);
    }

    @Override
    @UserProperty(name = PGP_REMOVE_SIGNATURE,
            order = 200
    )
    public Boolean getPGPRemoveSignature()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_REMOVE_SIGNATURE);
    }

    @Override
    @UserProperty(name = PGP_AUTO_UPDATE_EMAIL_ADDRESSES)
    public void setPGPAutoUpdateEmailAddresses(Boolean autoUpdateEmailAddresses)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_AUTO_UPDATE_EMAIL_ADDRESSES, autoUpdateEmailAddresses);
    }

    @Override
    @UserProperty(name = PGP_AUTO_UPDATE_EMAIL_ADDRESSES,
            order = 210
    )
    public Boolean getPGPAutoUpdateEmailAddresses()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_AUTO_UPDATE_EMAIL_ADDRESSES);
    }

    @Override
    @UserProperty(name = PGP_SKIP_SIGN_ONLY)
    public void setPGPSkipSignOnly(Boolean skipSignOnly)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, PGP_SKIP_SIGN_ONLY, skipSignOnly);
    }

    @Override
    @UserProperty(name = PGP_SKIP_SIGN_ONLY,
            order = 220
    )
    public Boolean getPGPSkipSignOnly()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, PGP_SKIP_SIGN_ONLY);
    }

    @Override
    @UserProperty(name = PGP_KEY_SERVERS,
            user = false, domain = false,
            visible = false
    )
    public void setJSONEncodedPGPKeyServers(String jsonEncodedKeyServers)
    throws HierarchicalPropertiesException
    {
        setProperty(PGP_KEY_SERVERS, UserPropertiesValidators.validateJSONEncodePGPKeyServers(PGP_KEY_SERVERS,
                jsonEncodedKeyServers));
    }

    @Override
    @UserProperty(name = PGP_KEY_SERVERS,
            user = false, domain = false,
            visible = false
    )
    public String getJSONEncodedPGPKeyServers()
    throws HierarchicalPropertiesException
    {
        return getProperty(PGP_KEY_SERVERS);
    }
}
