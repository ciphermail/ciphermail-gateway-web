/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.CipherMailApplication;
import com.ciphermail.core.app.CipherMailHeader;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.JamestUtils;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.MessageIDCreator;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.mail.matcher.ContentHeaderNameMatcher;
import com.ciphermail.core.common.mail.matcher.HeaderMatcher;
import com.ciphermail.core.common.mail.matcher.NotHeaderNameMatcher;
import com.ciphermail.core.common.pdf.MessagePDFBuilder;
import com.ciphermail.core.common.pdf.MessagePDFBuilderParametersImpl;
import com.ciphermail.core.common.pdf.PDFReplyURLBuilder;
import com.ciphermail.core.common.pdf.PDFURLBuilderException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PageMode;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.interactive.viewerpreferences.PDViewerPreferences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet that creates and sends new message(s) with the source message converted to PDF, encrypted with a password
 * and attached to the new message. The PDF password(s) to use are extracted from the mail attributes. The PDF will
 * be encrypted using AES 128.
 */
@SuppressWarnings("java:S6813")
public class PDFEncrypt extends SenderTemplatePropertySendMail
{
    private static final Logger logger = LoggerFactory.getLogger(PDFEncrypt.class);

    /*
     * PDFEncrypt Freemarker template parameter names
     */
    protected static final String PASSWORD_CONTAINER_TEMPLATE_PARAM = "passwordContainer";
    protected static final String PASSWORD_ID_TEMPLATE_PARAM = "passwordID";

    /*
     * The key under which the PDFContext will be stored in the activation context
     */
    private static final String PDF_CONTEXT_KEY = "PDFContext";

    /*
     * Processor to use for the encrypted pdf message.
     */
    private String encryptedProcessor;

    /*
     * Processor to use for the non encrypted pdf message.
     */
    private String notEncryptedProcessor;

    /*
     * The key length for PDF encryption (128 or 256)
     *
     * Note: not every PDF reader supports key length of 256
     */
    private int encryptionKeyLength = 128;

    /*
     * Maximum length of the subject for the reply URL. If size exceeds this maximum size the subject
     * will be truncated.
     *
     */
    private int maxSubjectLength = 80;

    /*
     * If true the original Message-ID will be used for the encrypted message.
     */
    private boolean retainMessageID;

    /*
     * If true and a reply-to header is available and valid, the reply to the PDF will be
     * sent to the reply-to
     */
    private boolean useReplyTo = true;

    /*
     * Service that creates a PDF from a message
     */
    @Inject
    private MessagePDFBuilder messagePDFBuilder;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ENCRYPTED_PROCESSOR     ("encryptedProcessor"),
        NOT_ENCRYPTED_PROCESSOR ("notEncryptedProcessor"),
        MAX_SUBJECT_LENGTH      ("maxSubjectLength"),
        RETAIN_MESSAGE_ID       ("retainMessageID"),
        USE_REPLY_TO            ("useReplyTo");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * Helper class for storing some settings in the activation context
     */
    protected static class PDFContext
    {
        /*
         * The subject of the reply (in parameter)
         */
        private final String subject;

        /*
         * The message-id of this message
         */
        private final String messageID;

        /*
         * The recipients of the reply (in parameter)
         */
        private final Collection<MailAddress> recipients;

        /*
         * The reply-to of the source email. Null if no reply-to header was set.
         */
        private final String replyTo;

        /*
         * The reply URL (out parameter)
         */
        private String replyURL;

        /*
         * If true, the complete MIME message is deep scanned for attachments etc.
         */
        private boolean deepscan;

        PDFContext(String subject, Collection<MailAddress> recipients, String replyTo, String messageID)
        {
            this.subject = subject;
            this.recipients = recipients;
            this.replyTo = replyTo;
            this.messageID = messageID;
        }

        public String getReplyURL() {
            return replyURL;
        }

        public void setReplyURL(String replyURL) {
            this.replyURL = replyURL;
        }

        public String getSubject() {
            return subject;
        }

        public Collection<MailAddress> getRecipients() {
            return recipients;
        }

        public String getReplyTo() {
            return replyTo;
        }

        public String getMessageID() {
            return messageID;
        }

        public void setDeepScan(boolean deepscan) {
            this.deepscan = deepscan;
        }

        public boolean isDeepScan() {
            return deepscan;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailetTransacted()
    throws MessagingException
    {
        super.initMailetTransacted();

        Objects.requireNonNull(messagePDFBuilder);

        encryptedProcessor = Objects.requireNonNull(getInitParameter(Parameter.ENCRYPTED_PROCESSOR.name),
                Parameter.ENCRYPTED_PROCESSOR.name + " must be specified");

        notEncryptedProcessor = Objects.requireNonNull(getInitParameter(Parameter.NOT_ENCRYPTED_PROCESSOR.name),
                Parameter.NOT_ENCRYPTED_PROCESSOR.name + " must be specified");

        String param = getInitParameter(Parameter.MAX_SUBJECT_LENGTH.name);

        if (param != null) {
            maxSubjectLength = NumberUtils.toInt(param, maxSubjectLength);
        }

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);
        useReplyTo = getBooleanInitParameter(Parameter.USE_REPLY_TO.name, useReplyTo);

        StrBuilder sb = new StrBuilder();

        sb.append("encryptedProcessor: ");
        sb.append(encryptedProcessor);
        sb.append("; ");
        sb.append("notEncryptedProcessor: ");
        sb.append(notEncryptedProcessor);
        sb.append("; ");
        sb.append("retainMessageID: ");
        sb.append(retainMessageID);
        sb.append("; ");
        sb.append("useReplyTo: ");
        sb.append(useReplyTo);

        getLogger().info("{}", sb);
    }

    /*
     * Called with the originating user
     */
    @Override
    protected Template getTemplateFromUser(User user, SimpleHash root)
    throws MessagingException, HierarchicalPropertiesException, IOException
    {
        PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(user.getUserPreferences().getProperties());

        PDFContext context = getActivationContext().get(PDF_CONTEXT_KEY,
                PDFContext.class);

        if (context != null) {
            context.setDeepScan(pdfProperties.getPdfDeepScan());
        }

        try {
            String baseReplyURL = pdfProperties.getPdfReplyURL();
            String replySecret = pdfProperties.getPdfReplySecretKey();

            boolean senderAllowed = pdfProperties.getPdfReplyEnabled();

            if (context != null)
            {
                if (senderAllowed)
                {
                    // Use the Reply-To of the message if available
                    String replyTo = null;

                    if (useReplyTo) {
                        replyTo = context.getReplyTo();
                    }

                    // Fallback to the user's email address (i.e., the from) if the Reply-To is not
                    // available or not valid.
                    if (StringUtils.isEmpty(replyTo)) {
                        replyTo = user.getEmail();
                    }

                    String replyURL = createReplyURL(baseReplyURL, context.getSubject(),
                            context.getRecipients(), user.getEmail(), replyTo, context.getMessageID(),
                            replySecret);

                    if (replyURL != null) {
                        context.setReplyURL(replyURL);
                    }
                }
                else {
                    getLogger().debug("Sender {} does not allow reply to PDF.", user.getEmail());
                }
            }
            else {
                getLogger().warn("ReplySettings are not set");
            }
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error getting reply pdf properties", e);
        }

        return super.getTemplateFromUser(user, root);
    }

    private void addEncryptedPDF(MimeMessage message, byte[] pdf)
    throws MessagingException
    {
        // Find the existing PDF. It should be a multipart otherwise there cannot be a PDF attachment
        if (!message.isMimeType("multipart/*")) {
            throw new MessagingException("Content-type should have been multipart.");
        }

        Multipart mp;

        try {
            mp = (Multipart) message.getContent();
        }
        catch (IOException e) {
            throw new MessagingException("Error getting message content.", e);
        }

        BodyPart pdfPart = null;

        // Fallback in case the template does not contain a CipherMailHeader.MARKER
        BodyPart fallbackPart = null;

        for (int i=0; i < mp.getCount(); i++)
        {
            BodyPart part = mp.getBodyPart(i);

            if (ArrayUtils.contains(part.getHeader(CipherMailHeader.MARKER), CipherMailHeader.ATTACHMENT_MARKER_VALUE))
            {
                pdfPart = part;

                break;
            }

            // Fallback scanning for application/pdf in case the template does not contain a CipherMailHeader.MARKER
            if (part.isMimeType("application/pdf")) {
                fallbackPart = part;
            }
        }

        if (pdfPart == null)
        {
            if (fallbackPart != null)
            {
                getLogger().info("Marker not found. Using octet-stream instead.");

                // Use the octet-stream part
                pdfPart = fallbackPart;
            }
            else {
                throw new MessagingException("Unable to find the attachment part in the template.");
            }
        }

        pdfPart.setDataHandler(new DataHandler(new ByteArrayDataSource(pdf, "application/pdf")));
    }

    private byte[] encryptPDF(MimeMessage sourceMessage, byte[] unencryptedPDF, String userPassword, String ownerPassword)
    throws IOException
    {
        byte[] encrypted;

        try (PDDocument pdfDocument = PDDocument.load(unencryptedPDF))
        {
            setPDFMetaInfo(sourceMessage, pdfDocument);

            AccessPermission accessPermission = AccessPermission.getOwnerAccessPermission();

            StandardProtectionPolicy protectionPolicy = new StandardProtectionPolicy(ownerPassword, userPassword,
                    accessPermission);

            protectionPolicy.setEncryptionKeyLength(encryptionKeyLength);
            protectionPolicy.setPermissions(accessPermission);
            protectionPolicy.setPreferAES(true);

            pdfDocument.protect(protectionPolicy);

            ByteArrayOutputStream encryptionOutput = new ByteArrayOutputStream();

            pdfDocument.save(encryptionOutput);

            encrypted = encryptionOutput.toByteArray();
        }

        return encrypted;
    }

    private void setPDFMetaInfo(MimeMessage message, PDDocument pdfDocument)
    {
        PDDocumentCatalog documentCatalog = pdfDocument.getDocumentCatalog();

        // Make sure the attachment pane is open when the PDF is opened (only on supported PDF readers)
        documentCatalog.setPageMode(PageMode.USE_ATTACHMENTS);

        PDViewerPreferences viewerPreferences = new PDViewerPreferences(new COSDictionary());

        // Add document title to PDF viewer title bar
        viewerPreferences.setDisplayDocTitle(true);

        documentCatalog.setViewerPreferences(viewerPreferences);

        PDDocumentInformation documentInformation = pdfDocument.getDocumentInformation();

        try {
            documentInformation.setSubject(MailUtils.getSafeSubject(message));
            documentInformation.setTitle(documentInformation.getSubject());
        }
        catch (MessagingException e) {
            getLogger().warn("Invalid Subject", e);
        }

        try {
            documentInformation.setAuthor(StringUtils.join(EmailAddressUtils.addressesToStrings(
                    EmailAddressUtils.getFromNonStrict(message), true /* mime decode */), ", "));
        }
        catch (AddressException e) {
            getLogger().warn("From address is not a valid email address: {}", e.getMessage());
        }
        catch (MessagingException e) {
            getLogger().warn("Error getting From address", e);
        }

        documentInformation.setProducer(CipherMailApplication.getName());
        documentInformation.setCreator(CipherMailApplication.getName());
        documentInformation.setCreationDate(new GregorianCalendar());
        documentInformation.setModificationDate(null);
    }

    /*
     * Checks if the user with email allows replies to PDF
     */
    private boolean isReplyAllowed(String email)
    {
        boolean allowed = false;

        try {
            PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                    .createInstance(getUserWorkflow()
                            .getUser(email, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                            .getUserPreferences().getProperties());

            allowed = pdfProperties.getPdfReplyEnabled();
        }
        catch (AddressException e) {
            getLogger().error("Error parsing email address: {}", email, e);
        }
        catch (HierarchicalPropertiesException e) {
            getLogger().error("Error getting isReplyAllowed property for user {}", email);
        }

        return allowed;
    }

    private String createReplyURL(String baseURL, String subject, Collection<MailAddress> recipients, String sender,
            String replyTo, String messageID, String replySecret)
    throws MessagingException
    {
        String replyURL = null;

        if (EmailAddressUtils.INVALID_EMAIL.equals(replyTo) || StringUtils.isEmpty(replyTo))
        {
            // This happens if the sender of the email is an invalid sender. This will normally only happen if
            // the global settings allow PDF reply.
            getLogger().warn("Reply-To of the message is an invalid email address. " +
                    "It's not possible to reply to the PDF.");
        }
        else if (baseURL != null && replySecret != null)
        {
            // The From of the reply (is equal to the recipient of the encrypted PDF)
            String recipient = null;

            if (recipients.size() == 1)
            {
                String notYetValidatedRecipient = recipients.iterator().next().toString();

                recipient = EmailAddressUtils.canonicalizeAndValidate(notYetValidatedRecipient, true);

                if (recipient == null) {
                    getLogger().warn("{} is not a valid recipient.", notYetValidatedRecipient);
                }
            }
            else {
                getLogger().warn("The mail has multiple recipients. It's not possible to reply to the PDF.");
            }

            if (recipient != null && !isReplyAllowed(recipient))
            {
                getLogger().debug("Recipient {} does not allow reply to PDF.", recipient);

                recipient = null;
            }

            if (sender != null && recipient != null)
            {
                // Make sure the subject is not too long
                subject = StringUtils.abbreviate(StringUtils.trimToEmpty(subject), maxSubjectLength);

                PDFReplyURLBuilder replyBuilder = PDFReplyURLBuilder.createInstance()
                    .setBaseURL(baseURL)
                    .setUser(sender)
                    .setRecipient(replyTo)
                    // The From of the reply message is equal to the original recipient of the PDF (i.e, if the
                    // recipient clicks reply, the sender, aka from, of the reply is set to the recipient of the pdf).
                    .setFrom(recipient)
                    .setSubject(subject)
                    .setMessageID(messageID)
                    .setKey(replySecret);

                try {
                    replyURL = replyBuilder.buildURL();
                }
                catch (PDFURLBuilderException e) {
                    throw new MessagingException("Building reply URL failed.", e);
                }
            }
        }

        return replyURL;
    }

    protected void buildPDF(Mail mail, PDFContext context, OutputStream pdfStream)
    throws MessagingException, IOException
    {
        MessagePDFBuilderParametersImpl parameters = new MessagePDFBuilderParametersImpl(context.getReplyURL());

        parameters.setDeepScan(context.isDeepScan());

        getMessagePDFBuilder().buildPDF(mail.getMessage(), parameters, pdfStream);
    }

    private MimeMessage createMessage(final Mail mail, Collection<MailAddress> recipients,
            PasswordContainer passwordContainer)
    throws MessagingException, TemplateException, IOException
    {
        Objects.requireNonNull(passwordContainer);

        SimpleHash root = getTemplateHashModelBuilder().buildSimpleHash();

        root.put(PASSWORD_CONTAINER_TEMPLATE_PARAM, passwordContainer);
        // Note: although passwordID can be retrieved from passwordContainer we keep passwordID to make sure
        // that existing templates that use passwordID are still working
        root.put(PASSWORD_ID_TEMPLATE_PARAM, passwordContainer.getPasswordID());

        // We should put the real recipient(s) in the Freemarker root. bug https://jira.djigzo.com/browse/GATEWAY-38.
        root.put(RECIPIENTS_TEMPLATE_PARAM, recipients);

        final MimeMessage sourceMessage = mail.getMessage();

        // Get the Reply-To of the message and validate.
        //
        // Note: the Reply-To is not canonicalized because that can result in invalid email addresses. For example
        // "email with space"@example.com is only valid with the quotes.
        String replyTo = EmailAddressUtils.validate(EmailAddressUtils.getEmailAddress(EmailAddressUtils.getAddress(
                EmailAddressUtils.getReplyToQuietly(sourceMessage))));

        // The ReplySettings will be placed in the context since they are needed in #getTemplateFromUser to
        // get the reply URL
        PDFContext replySettings = new PDFContext(MailUtils.getSafeSubject(sourceMessage), recipients, replyTo,
                sourceMessage.getMessageID());

        getActivationContext().set(PDF_CONTEXT_KEY, replySettings);

        // Create a message from a template.
        //
        // Note: Calling createMessage will result in a call to #getTemplateFromUser and should result in the
        // reply URL to be set (if reply is allowed and all required settings are set)
        MimeMessage containerMessage = createMessage(mail, root);

        // Copy all non-content headers from source to notificationMessage.
        HeaderMatcher nonContentMatcher = new NotHeaderNameMatcher(new ContentHeaderNameMatcher());

        HeaderUtils.copyHeaders(sourceMessage, containerMessage, nonContentMatcher);

        // Create PDF from the source message
        ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();

        try {
            buildPDF(mail, replySettings, pdfStream);
        }
        catch (IOException e) {
            throw new MessagingException("Error building PDF.", e);
        }

        byte[] encryptedPdf;

        try {
            String userPassword = passwordContainer.getPassword();

            // Owner and user password will be set to the same value
            encryptedPdf = encryptPDF(sourceMessage, pdfStream.toByteArray(), userPassword, userPassword);
        }
        catch (IOException e) {
            throw new MessagingException("Error encrypting PDF.", e);
        }

        // Now find and replace the pdf inside the notificationMessage with the encrypted pdf
        addEncryptedPDF(containerMessage, encryptedPdf);

        containerMessage.saveChanges();

        String messageID = null;

        if (retainMessageID) {
            messageID = StringUtils.trimToNull(sourceMessage.getMessageID());
        }

        if (messageID == null) {
            messageID = MessageIDCreator.getInstance().createUniqueMessageID();
        }

        containerMessage = new MimeMessageWithID(containerMessage, messageID);

        return containerMessage;
    }

    private void sendNewMessage(MailImpl newMail, Collection<MailAddress> recipients, MimeMessage message,
            String processor)
    throws MessagingException
    {
        newMail.setRecipients(recipients);
        newMail.setMessage(message);
        newMail.setState(processor);

        getMailetContext().sendMail(newMail);
    }

    private void sendNotification(Mail mail, Collection<MailAddress> recipients,
            PasswordContainer passwordContainer)
    throws MessagingException, TemplateException, IOException
    {
        // The new Mail instance will be created to make sure the template is run with the correct instance
        // of the Mail object.
        MailImpl newMail = MailImpl.duplicate(mail);

        try {
            if (passwordContainer != null)
            {
                MimeMessage message = createMessage(newMail, recipients, passwordContainer);

                sendNewMessage(newMail, recipients, message, encryptedProcessor);
            }
            else {
                getLogger().warn("PDF Password not found. Sending message unencrypted.");

                sendNewMessage(newMail, recipients, mail.getMessage(), notEncryptedProcessor);
            }
        }
        finally {
            JamestUtils.dispose(newMail);
        }
    }

    @Override
    protected void sendMail(Mail mail)
    throws MessagingException, TemplateException, IOException
    {
        Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);

        if (passwords != null)
        {
            Collection<MailAddress> recipients = getRecipients(mail);

            for (MailAddress recipient : recipients)
            {
                // We will only accept valid email addresses. If an email address is invalid the message
                // will not be encrypted
                String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(
                        recipient.toString(), true);

                PasswordContainer passwordContainer = (validatedEmail != null ? passwords.get(validatedEmail) : null);

                sendNotification(mail, Collections.singletonList(recipient), passwordContainer);
            }
        }
        else {
            getLogger().warn("PDF passwords not found. Sending message unencrypted.");

            sendNotification(mail, getRecipients(mail), null);
        }
    }

    // Can be used by a subclass to get or override the messagePDFBuilder
    protected MessagePDFBuilder getMessagePDFBuilder() {
        return messagePDFBuilder;
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        requiredProcessors.add(new ProcessingState(encryptedProcessor));
        requiredProcessors.add(new ProcessingState(notEncryptedProcessor));

        return requiredProcessors;
    }
}
