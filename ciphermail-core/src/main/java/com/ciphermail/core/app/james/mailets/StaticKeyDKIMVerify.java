/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.dkim.DKIMMessageHeaders;
import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.common.mail.SkipHeadersOutputStream;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.CRLFOutputStream;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.jdkim.DKIMVerifier;
import org.apache.james.jdkim.api.BodyHasher;
import org.apache.james.jdkim.api.Headers;
import org.apache.james.jdkim.api.PublicKeyRecordRetriever;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.exceptions.FailException;
import org.apache.mailet.Attribute;
import org.apache.mailet.Mail;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.security.PublicKey;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Verifies the digital signature of a DKIM signed message. This implementation works with a static public key i.e.,
 * the public key is not retrieved from DNS. The key is read from the Mailet configuration and should be a PEM
 * encoded public key.
 */
public class StaticKeyDKIMVerify extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(StaticKeyDKIMVerify.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        PUBLIC_KEY            ("publicKey"),
        DKIM_HEADER           ("dkimHeader"),
        RESULT_MAIL_ATTRIBUTE ("resultAttribute");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * Implementation of PublicKeyRecordRetriever that returns a record based on
     * the provided public key
     */
    private static class StaticPublicKeyRecordRetriever implements PublicKeyRecordRetriever
    {
        private final List<String> records;

        private StaticPublicKeyRecordRetriever(@Nonnull PublicKey publicKey)
        {
            records = List.of("p=" + MiscStringUtils.toStringFromASCIIBytes(
                    Base64.encodeBase64(publicKey.getEncoded())));
        }

        @Override
        public List<String> getRecords(CharSequence methodAndOption, CharSequence selector, CharSequence token) {
            return records;
        }
    }

    /*
     * The public key used to verify the signature with
     */
    private PublicKey publicKey;

    /*
     * The name of the Mail attribute to store the result in
     */
    private String resultMailAttribute;

    /*
     * The DKIM header to use (defaults to DKIM-Signature)
     */
    private String dkimHeader;

    @Override
    protected void initMailet()
    throws MessagingException
    {
        dkimHeader = getInitParameter(Parameter.DKIM_HEADER.name, DKIMUtils.DEFAULT_DKIM_HEADER);

        resultMailAttribute = Objects.requireNonNull(StringUtils.trimToNull(
                getInitParameter(Parameter.RESULT_MAIL_ATTRIBUTE.name)),
                Parameter.RESULT_MAIL_ATTRIBUTE.name + " parameter is missing");

        loadPublicKey();
    }

    /*
     * Returns the Crypto provider to use for DKIM signing
     */
    protected String getProvider()
    {
        // Because we will use a PEM for the DKIM key, we need to use the non-sensitive provider
        return SecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider();
    }

    protected static PublicKey parseKey(String pkcs8)
    throws MessagingException
    {
        SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

        JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();

        // Since we are importing PEM, we will use the non-sensitive provider because any private material
        // will not be stored on HSM
        keyConverter.setProvider(securityFactory.getNonSensitiveProvider());

        PublicKey key = null;

        pkcs8 = StringUtils.trimToNull(pkcs8);

        if (pkcs8 != null)
        {
            PEMParser pem = new PEMParser(new StringReader(pkcs8));

            Object o;

            try {
                o = pem.readObject();
            }
            catch (IOException e) {
                throw new MessagingException("Unable to read PEM encoded private key", e);
            }
            finally {
                IOUtils.closeQuietly(pem);
            }

            try {
                if (o instanceof PEMKeyPair pemKeyPair)
                {
                    // We are only interested in private keys and not in "stand-alone" public keys
                    key = keyConverter.getKeyPair(pemKeyPair).getPublic();
                }
                else if (o instanceof SubjectPublicKeyInfo subjectPublicKeyInfo) {
                    key = keyConverter.getPublicKey(subjectPublicKeyInfo);
                }
                else if (o == null) {
                    throw new MessagingException("The PEM encoded blob did not return any object.");
                }
                else {
                    throw new MessagingException("The PEM input is not a PublicKey or KeyPair but a " +
                            o.getClass());
                }
            }
            catch (PEMException e) {
                throw new MessagingException("The PEM input is not a valid PublicKey or KeyPair.", e);
            }
        }

        return key;
    }

    private void loadPublicKey()
    throws MessagingException
    {
        publicKey = parseKey(getInitParameter(Parameter.PUBLIC_KEY.name));
    }

    protected PublicKey getPublicKey(Mail mail)
    throws MessagingException
    {
        return publicKey;
    }

    private void signatureFailure(Mail mail) {
        signatureFailure(mail, null);
    }

    private void signatureFailure(Mail mail, Throwable t)
    {
        if (t != null) {
            getLogger().error("DKIM verification failed due to an error.", t);
        }
        else {
            getLogger().warn("DKIM verification failed.");
        }

        mail.setAttribute(Attribute.convertToAttribute(resultMailAttribute, "failed"));
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            MimeMessage message = mail.getMessage();

            PublicKey localPublicKey = getPublicKey(mail);

            if (localPublicKey != null)
            {
                // Use a static PublicKeyRecordRetriever that does not retrieve the public key from
                // DNS but uses a pre-defined PublicKey.
                PublicKeyRecordRetriever retriever = new StaticPublicKeyRecordRetriever(localPublicKey);

                DKIMVerifier verifier = new DKIMVerifier(retriever);

                // We need to rename non-standard DKIM header to DKIM-Signature because verify assumes the
                // header is set to DKIM-Signature
                Headers headers = new DKIMMessageHeaders(message, Map.of(StringUtils.lowerCase(dkimHeader),
                        DKIMUtils.DEFAULT_DKIM_HEADER));

                BodyHasher bodyHasher = verifier.newBodyHasher(headers);

                if (bodyHasher == null) {
                    throw new MessagingException("No DKIM signature found");
                }

                try {
                    OutputStream os = new CRLFOutputStream(new SkipHeadersOutputStream(
                            bodyHasher.getOutputStream()));

                    message.writeTo(os);
                }
                catch (IOException e) {
                    throw new MessagingException("Exception calculating body hash: " + e.getMessage(), e);
                }
                finally
                {
                    try {
                        bodyHasher.getOutputStream().close();
                    }
                    catch (IOException e) {
                        throw new MessagingException("Exception calculating body hash: " + e.getMessage(), e);
                    }
                }

                List<SignatureRecord> results = verifier.verify(bodyHasher);

                if (CollectionUtils.isEmpty(results)) {
                    signatureFailure(mail);
                }
                else {
                    mail.setAttribute(Attribute.convertToAttribute(resultMailAttribute, "verified"));
                }
            }
            else {
                getLogger().warn("Unable to verify the message because the public key is missing.");
            }
        }
        catch(MessagingException e) {
            signatureFailure(mail, e);
        }
        catch (FailException e) {
            signatureFailure(mail);
        }
    }
}
