/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;

/**
 * There isn't really something like a domain. A domain is nothing more than a UserPreferences object
 * with a specific naming strategy.
 *
 * @author Martijn Brinkers
 */
public interface DomainManager
{
    /**
     * Add the domain and return the preferences object for the domain.
     */
    UserPreferences addDomain(String domain)
    throws HierarchicalPropertiesException, CloseableIteratorException;

    /**
     * Removes the domain.
     */
    boolean deleteDomainPreferences(UserPreferences domainPreferences);

    /**
     * Returns the UserPreferences object for the domain.
     */
    UserPreferences getDomainPreferences(String domain);

    /**
     * Returns true if the domain is in use (and can therefore not be deleted)
     */
    boolean isDomainInUse(String domain);

    /**
     * Returns the number of domains.
     */
    long getDomainCount();

    /**
     * Returns an iterator for the domain names.
     */
    CloseableIterator<String> getDomainIterator();

    /**
     * Returns an iterator that returns all the domain names.
     */
    CloseableIterator<String> getDomainIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection);

    /**
     * Deletes all domains
     */
    void deleteAllDomains()
    throws CloseableIteratorException;
}
