/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.stats.StatsEventImpl;
import com.ciphermail.core.app.stats.StatsPublisher;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Objects;

/**
 * Mailet that sends stats to the registered stats subscribers
 */
@SuppressWarnings({"java:S6813"})
public class Stats extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(Stats.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * Name of the stats
     */
    private String name;

    /*
     * Publishes stats to all the registered StatsSubscriber's
     */
    @Inject
    private StatsPublisher publisher;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        NAME ("name");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    public final void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(publisher);

        name = Objects.requireNonNull(StringUtils.trimToNull(getInitParameter(Parameter.NAME.name)),
                Parameter.NAME.name + " parameter is missing");
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            publisher.publishStats(name, new StatsEventImpl(mail));
        }
        catch (IOException e) {
            logger.error("Error notifying stats subscribers", e);
        }
    }
}
