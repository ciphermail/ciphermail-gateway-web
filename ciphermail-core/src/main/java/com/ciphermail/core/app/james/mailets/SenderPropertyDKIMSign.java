/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.Context;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.mutable.MutableObject;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.security.PrivateKey;
import java.util.Objects;

/**
 * Extension of DKIMSign that reads the private key for DKIM signing from the sender properties. The key from the
 * properties should be a PEM encoded private key without password protection. For better key protection use the
 * DKIM sign mailet which reads the private key from the key store (assuming that the key store is backed by an HSM)
 */
@SuppressWarnings({"java:S6813"})
public class SenderPropertyDKIMSign extends StaticKeyDKIMSign
{
    private static final Logger logger = LoggerFactory.getLogger(SenderPropertyDKIMSign.class);

    /*
     * The user property from which the DKIM key should be read
     */
    private String keyProperty;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        keyProperty = Objects.requireNonNull(getInitParameter("keyProperty"), "keyProperty is missing");
    }

    /*
     * Gets called by MailAddressHandler#handleMailAddresses to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(User user, MutableObject pemKeyPairHolder)
    throws MessagingException
    {
        try {
            pemKeyPairHolder.setValue(user.getUserPreferences().
                    getProperties().getProperty(keyProperty));
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error reading DKIM key pair.", e);
        }
    }

    @Override
    public PrivateKey getPrivateKey(Mail mail, Context context)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        final MutableObject pemKeyPairHolder = new MutableObject();

        MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                user -> onHandleUserEvent(user, pemKeyPairHolder), getLogger());

        mailAddressHandler.handleMailAddressesWithRetry(MailAddressUtils.fromAddressArrayToMailAddressList(originator));

        return parseKey((String) pemKeyPairHolder.getValue());
    }
}
