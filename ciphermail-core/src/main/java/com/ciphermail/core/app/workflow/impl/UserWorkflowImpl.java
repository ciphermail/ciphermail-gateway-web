/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.ClientSecretGenerator;
import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserManager;
import com.ciphermail.core.app.UserPreferenceMerger;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.UserPreferencesManager;
import com.ciphermail.core.app.UserPreferencesSelector;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of UserWorkflow
 *
 * @author Martijn Brinkers
 *
 */
public class UserWorkflowImpl implements UserWorkflow
{
    /*
     * Manages the users (users are the email addresses of receivers and senders)
     */
    private final UserManager userManager;

    /*
     * Manages the global preferences. Every user should inherit from the global preferences.
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Select preferences for the user (for example from a domain)
     */
    private final UserPreferencesSelector userPreferencesSelector;

    /*
     * Merges existing preferences with new preferences (for example from the domain)
     */
    private final UserPreferenceMerger userPreferenceMerger;

    /*
     * This UserPreferencesManager for a user
     */
    private final UserPreferencesManager userPreferencesManager;

    /*
     * Generates the client secret for a new user
     */
    private final ClientSecretGenerator clientSecretGenerator;

    public UserWorkflowImpl(
            @Nonnull UserManager userManager,
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull UserPreferencesSelector userPreferencesSelector,
            @Nonnull UserPreferenceMerger userPreferenceMerger,
            @Nonnull UserPreferencesCategoryManager userPreferencesCategoryManager,
            @Nonnull ClientSecretGenerator clientSecretGenerator)
    {
        this.userManager = Objects.requireNonNull(userManager);
        this.globalPreferencesManager = Objects.requireNonNull(globalPreferencesManager);
        this.userPreferencesSelector = Objects.requireNonNull(userPreferencesSelector);
        this.userPreferenceMerger = Objects.requireNonNull(userPreferenceMerger);
        this.userPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(
                UserPreferencesCategory.USER.name());
        this.clientSecretGenerator = Objects.requireNonNull(clientSecretGenerator);
    }

    /*
     * A user inherits preferences (factory, global, domain)
     */
    private User inheritPreferences(@Nonnull User user)
    {
        // update the user preferences of this user. The strategy used depends on the actual
        // userPreferencesSelector implementation.
        Set<UserPreferences> selectedUserPreferences = userPreferencesSelector.select(user);

        Set<UserPreferences> inheritedPreferences = user.getUserPreferences().getInheritedUserPreferences();

        // merge the selectedUserPreferences with the existing user preferences.
        Set<UserPreferences> merged = userPreferenceMerger.merge(inheritedPreferences, selectedUserPreferences);

        // Replace the inherited user preferences by the new merged preferences
        if (merged != null)
        {
            inheritedPreferences.clear();
            inheritedPreferences.addAll(merged);
        }

        return user;
    }

    @Override
    public User getUser(@Nonnull String email, UserNotExistResult userNotExistResult)
    throws AddressException, HierarchicalPropertiesException
    {
        User user = userManager.getUser(email);

        if (user == null && userNotExistResult == UserNotExistResult.DUMMY_IF_NOT_EXIST) {
            user = addUser(email);
        }

        if (user != null) {
            inheritPreferences(user);
        }

        return user;
    }

    @Override
    public Set<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws AddressException, CloseableIteratorException, HierarchicalPropertiesException
    {
        Set<User> users = new LinkedHashSet<>();

        CloseableIterator<String> emailIterator = userManager.getEmailIterator(firstResult,
                maxResults, sortDirection);

        try {
            while (emailIterator.hasNext())
            {
                String email = emailIterator.next();

                email = EmailAddressUtils.canonicalizeAndValidate(email, false);

                User user = this.getUser(email, UserNotExistResult.DUMMY_IF_NOT_EXIST);

                users.add(user);
            }
        }
        finally {
            emailIterator.close();
        }

        return users;
    }

    @Override
    public long getUserCount() {
        return userManager.getUserCount();
    }

    @Override
    public User addUser(@Nonnull String email)
    throws AddressException, HierarchicalPropertiesException
    {
        User user = userManager.addUser(email, UserManager.AddMode.NON_PERSISTENT);

        UserPreferences preferences = user.getUserPreferences();

        // every user inherits the global preferences
        preferences.getInheritedUserPreferences().add(
                globalPreferencesManager.getGlobalUserPreferences());

        // the user also inherits from a domain (if there is a domain object)
        inheritPreferences(user);

        // Set the date this user was created
        preferences.getProperties().setDateCreated(System.currentTimeMillis());
        // a user should always have a client secret
        preferences.getProperties().setClientSecret(clientSecretGenerator.generateClientSecret());

        return user;
    }

    @Override
    public boolean isInUse(@Nonnull String email)
    throws AddressException
    {
        User user = userManager.getUser(email);

        return user != null && userPreferencesManager.isInUse(user.getUserPreferences());
    }

    @Override
    public boolean deleteUser(@Nonnull User user) {
        return userManager.deleteUser(user);
    }

    @Override
    public User makePersistent(@Nonnull User user) {
        return userManager.makePersistent(user);
    }

    @Override
    public boolean isPersistent(@Nonnull User user) {
        return userManager.isPersistent(user);
    }

    @Override
    public CloseableIterator<String> getEmailIterator() {
        return userManager.getEmailIterator();
    }

    @Override
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        return userManager.getEmailIterator(firstResult, maxResults, sortDirection);
    }

    @Override
    public CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        return userManager.searchEmail(search, firstResult, maxResults, sortDirection);
    }

    @Override
    public long getSearchEmailCount(@Nonnull String search) {
        return userManager.getSearchEmailCount(search);
    }
}
