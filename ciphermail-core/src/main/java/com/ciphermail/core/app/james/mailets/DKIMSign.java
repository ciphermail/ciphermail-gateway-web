/*
 * Copyright (c) 2016-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.properties.DKIMProperties;
import com.ciphermail.core.app.properties.DKIMPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dkim.DKIMPrivateKeyManager;
import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.common.util.Context;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.Objects;

/**
 * Mailet which DKIM signs outgoing email. The private key for signing is retrieved from @DKIMPrivateKeyManager
 */
@SuppressWarnings("java:S6813")
public class DKIMSign extends AbstractDKIMSign
{
    private static final Logger logger = LoggerFactory.getLogger(DKIMSign.class);

    /*
     * The names used to store things in the context
     */
    private static final String KEY_PAIR_CONTEXT_NAME = "privateKey";
    private static final String SIGNATURE_TEMPLATE_CONTEXT_NAME = "signatureTemplate";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Manages the DKIM private keys
     */
    @Inject
    private DKIMPrivateKeyManager privateKeyManager;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(privateKeyManager);
        Objects.requireNonNull(transactionOperations);
    }

    /*
     * Gets called by MailAddressHandler#handleMailAddresses to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(User user, Mail mail, Context context)
    throws MessagingException
    {
        try {
            DKIMProperties properties = userPropertiesFactoryRegistry.getFactoryForClass(DKIMPropertiesImpl.class)
                    .createInstance(user.getUserPreferences().getProperties());

            String keyIdentifier = properties.getKeyIdentifier();

            if (StringUtils.isNotEmpty(keyIdentifier))
            {
                KeyPair keyPair = privateKeyManager.getKey(properties.getKeyIdentifier());

                if (keyPair == null) {
                    logger.atWarn().log(createLogLine(String.format("DKIM key with keyId %s is missing",
                            properties.getKeyIdentifier()), mail, false));
                }

                context.set(KEY_PAIR_CONTEXT_NAME, keyPair);

                String signatureTemplate = properties.getSignatureTemplate();

                // Replace ${} tokens in the template
                signatureTemplate = DKIMUtils.replaceSignatureTemplateTokens(signatureTemplate, user.getEmail());

                context.set(SIGNATURE_TEMPLATE_CONTEXT_NAME, signatureTemplate);
            }
        }
        catch (Exception e) {
            throw new MessagingException("Error retrieving DKIM configuration", e);
        }
    }

    @Override
    protected void initialize(Mail mail, final Context context)
    throws IOException, MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                user -> onHandleUserEvent(user, mail, context), getLogger());

        mailAddressHandler.handleMailAddressesWithRetry(MailAddressUtils.fromAddressArrayToMailAddressList(originator));
    }

    @Override
    protected PrivateKey getPrivateKey(Mail mail, Context context)
    {
        KeyPair keyPair = context.get(KEY_PAIR_CONTEXT_NAME, KeyPair.class);

        return (keyPair != null && keyPair.getPrivate() != null) ? keyPair.getPrivate() : null;
    }

    @Override
    protected String getSignatureTemplate(Mail mail, Context context) {
        return context.get(SIGNATURE_TEMPLATE_CONTEXT_NAME, String.class);
    }
}
