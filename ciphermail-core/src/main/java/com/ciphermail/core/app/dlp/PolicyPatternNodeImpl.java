/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.common.dlp.PolicyPattern;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.util.ObservableSet;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Implementation of PolicyPatternNode that reads/writes the data to a wrapped NamedBlob.
 *
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternNodeImpl implements PolicyPatternNode
{
    private static final Logger logger = LoggerFactory.getLogger(PolicyPatternNodeImpl.class);

    private final NamedBlob namedBlob;

    private final PolicyPatternMarshaller policyPatternMarshaller;

    public PolicyPatternNodeImpl(@Nonnull NamedBlob namedBlob, @Nonnull PolicyPatternMarshaller policyPatternMarshaller)
    {
        this.namedBlob = Objects.requireNonNull(namedBlob);
        this.policyPatternMarshaller = Objects.requireNonNull(policyPatternMarshaller);
    }

    private boolean onAddChild(@Nonnull PolicyPatternNode pattern)
    {
        if (!(pattern instanceof PolicyPatternNodeImpl patternImpl)) {
            throw new IllegalArgumentException("Pattern must be a PolicyPatternNodeImpl.");
        }

        return namedBlob.getNamedBlobs().add(patternImpl.namedBlob);
    }

    private boolean onRemoveChild(@Nonnull Object pattern)
    {
        if (!(pattern instanceof PolicyPatternNodeImpl patternImpl)) {
            throw new IllegalArgumentException("Pattern must be a PolicyPatternNodeImpl.");
        }

        return namedBlob.getNamedBlobs().remove(patternImpl.namedBlob);
    }

    @Override
    public void setName(@Nonnull String name) {
        namedBlob.setName(name);
    }

    @Override
    public @Nonnull String getName() {
        return namedBlob.getName();
    }

    @Override
    public @Nonnull Set<PolicyPatternNode> getChilds()
    {
        Set<NamedBlob> childBlobs = namedBlob.getNamedBlobs();

        Set<PolicyPatternNode> patterns = new LinkedHashSet<>();

        for (NamedBlob childBlob : childBlobs) {
            patterns.add(new PolicyPatternNodeImpl(childBlob, policyPatternMarshaller));
        }

        // create a Set instance that wraps-up the PolicyPatternNode set so we can intercept changes
        // (like add and remove) made to the set. We need this to reflect these
        // changes to the database
        ObservableSet<PolicyPatternNode> observer = new ObservableSet<>(patterns);

        observer.setAddEvent(this::onAddChild);

        observer.setRemoveEvent(this::onRemoveChild);

        return observer;
    }

    @Override
    public PolicyPattern getPolicyPattern()
    {
        PolicyPattern pattern = null;

        byte[] json = namedBlob.getBlob();

        if (json != null)
        {
            try {
                pattern = policyPatternMarshaller.unmarshall(new ByteArrayInputStream(json));
            }
            catch (IOException e) {
                logger.error("Unable to unmarshall pattern.", e);
            }
        }

        return pattern;
    }

    @Override
    public void setPolicyPattern(@Nonnull PolicyPattern pattern)
    {
        byte[] json = null;

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            policyPatternMarshaller.marshall(pattern, bos);

            json = bos.toByteArray();
        }
        catch (IOException e) {
            logger.error("Unable to marshall pattern.", e);
        }

        namedBlob.setBlob(json);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof PolicyPatternNode rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(getName(), rhs.getName())
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(getName())
            .toHashCode();
    }

    public NamedBlob getNamedBlob() {
        return namedBlob;
    }
}
