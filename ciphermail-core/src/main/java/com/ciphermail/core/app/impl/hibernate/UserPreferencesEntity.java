/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.ciphermail.core.common.properties.hibernate.NamedBlobEntity;
import com.ciphermail.core.common.properties.hibernate.PropertyEntity;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;

@Entity(name = UserPreferencesEntity.ENTITY_NAME)
@Table(
uniqueConstraints = {@UniqueConstraint(columnNames = {UserPreferencesEntity.CATEGORY_COLUMN,
        UserPreferencesEntity.NAME_COLUMN})},
indexes = {
    @Index(name = "userpreferences_name_index", columnList = UserPreferencesEntity.NAME_COLUMN),
    @Index(name = "userpreferences_category_index", columnList = UserPreferencesEntity.CATEGORY_COLUMN)
    }
)
public class UserPreferencesEntity
{
    static final String ENTITY_NAME = "UserPreferences";

    static final String NAME_COLUMN = "name";
    static final String CATEGORY_COLUMN = "category";

    /*
     * Use maximum length of an email address as max length
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_NAME_LENGTH = 64 + 1 + 255;

    /*
     * Use maximum length of an email address as max length
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_CATEGORY_LENGTH = 64 + 1 + 255;

    @Id
    @Column(name = "id")
    @UuidGenerator
    private UUID id;

    /**
     * The name of the UserPreferences
     */
    @Column (name = NAME_COLUMN, length = MAX_NAME_LENGTH, unique = false, nullable = false)
    private String name;

    /**
     * The category of the group.
     */
    @Column (name = CATEGORY_COLUMN, length = MAX_CATEGORY_LENGTH, unique = false, nullable = false)
    private String category;

    @ManyToMany
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<X509CertStoreEntity> certificates = new HashSet<>();

    @ElementCollection
    @JoinTable(name = "userPreferencesNamedCertificates")
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<NamedCertificateHibernate> namedCertificates = new HashSet<>();

    @ManyToOne
    private X509CertStoreEntity keyAndCertificateEntry;

    @OneToOne
    @Cascade(CascadeType.ALL)
    private PropertyEntity propertyEntity;

    @ManyToMany
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<NamedBlobEntity> namedBlobs = new HashSet<>();

    @ElementCollection
    @JoinTable(name = "userPreferencesInheritedPreferences")
    @OrderBy("index")
    // Hibernate will replace the instance therefore it should not be final
    @SuppressWarnings("FieldMayBeFinal")
    private Set<InheritedUserPreferences> inheritedPreferences = new LinkedHashSet<>();

    protected UserPreferencesEntity() {
        // Hibernate requires default constructor
    }

    public UserPreferencesEntity(@Nonnull String category, @Nonnull String name)
    {
        this.category = Objects.requireNonNull(category);
        this.name = Objects.requireNonNull(name);

        // #getPropertyEntityKey must be called after setting category and name
        this.propertyEntity = new PropertyEntity(getPropertyEntityKey());
    }

    public @Nonnull String getName() {
        return name;
    }

    public @Nonnull String getCategory() {
        return category;
    }

    public Set<X509CertStoreEntity> getCertificates() {
        return certificates;
    }

    public Set<InheritedUserPreferences> getInheritedPreferences() {
        return inheritedPreferences;
    }

    public Set<NamedCertificateHibernate> getNamedCertificates() {
        return namedCertificates;
    }

    public X509CertStoreEntity getKeyAndCertificateEntry() {
        return keyAndCertificateEntry;
    }

    public void setKeyAndCertificate(X509CertStoreEntity keyAndCertificate) {
        this.keyAndCertificateEntry = keyAndCertificate;
    }

    public PropertyEntity getPropertyEntity() {
        return propertyEntity;
    }

    public Set<NamedBlobEntity> getNamedBlobs() {
        return namedBlobs;
    }

    private String getPropertyEntityKey() {
        return getCategory() + ":" + getName();
    }

    @Override
    public String toString() {
        return getPropertyEntityKey();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserPreferencesEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(name, rhs.name)
            .append(category, rhs.category)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(name)
            .append(category)
            .toHashCode();
    }
}