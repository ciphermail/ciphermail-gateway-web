/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;

import javax.annotation.Nonnull;

/**
 * UserProperties implementation that reads/writes the user property values from/to a  HierarchicalProperties instance.
 */
@UserPropertiesType(name = "security-info", displayName = "Security Info", order = 80)
public class SecurityInfoPropertiesImpl extends DelegatedHierarchicalProperties implements SecurityInfoProperties
{
    private static final String ADD_SECURITY_INFO                 = "security-info-add";
    private static final String SECURITY_INFO_DECRYPTED_TAG       = "security-info-decrypted-tag";
    private static final String SECURITY_INFO_SIGNED_VALID_TAG    = "security-info-signed-valid-tag";
    private static final String SECURITY_INFO_SIGNED_BY_VALID_TAG = "security-info-signed-by-valid-tag";
    private static final String SECURITY_INFO_SIGNED_INVALID_TAG  = "security-info-signed-invalid-tag";
    private static final String SECURITY_INFO_MIXED_CONTENT_TAG   = "security-info-mixed-content-tag";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public SecurityInfoPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = ADD_SECURITY_INFO)
    public void setAddSecurityInfo(Boolean add)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, ADD_SECURITY_INFO, add);
    }

    @Override
    @UserProperty(name = ADD_SECURITY_INFO, order = 10)
    public Boolean getAddSecurityInfo()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, ADD_SECURITY_INFO);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_DECRYPTED_TAG)
    public void setSecurityInfoDecryptedTag(String tag)
    throws HierarchicalPropertiesException
    {
        setProperty(SECURITY_INFO_DECRYPTED_TAG, tag);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_DECRYPTED_TAG, order = 20)
    public String getSecurityInfoDecryptedTag()
    throws HierarchicalPropertiesException
    {
        return getProperty(SECURITY_INFO_DECRYPTED_TAG);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_VALID_TAG)
    public void setSecurityInfoSignedValidTag(String tag)
    throws HierarchicalPropertiesException
    {
        setProperty(SECURITY_INFO_SIGNED_VALID_TAG, tag);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_VALID_TAG, order = 30)
    public String getSecurityInfoSignedValidTag()
    throws HierarchicalPropertiesException
    {
        return getProperty(SECURITY_INFO_SIGNED_VALID_TAG);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_BY_VALID_TAG)
    public void setSecurityInfoSignedByValidTag(String tag)
    throws HierarchicalPropertiesException
    {
        setProperty(SECURITY_INFO_SIGNED_BY_VALID_TAG, tag);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_BY_VALID_TAG, order = 40)
    public String getSecurityInfoSignedByValidTag()
    throws HierarchicalPropertiesException
    {
        return getProperty(SECURITY_INFO_SIGNED_BY_VALID_TAG);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_INVALID_TAG)
    public void setSecurityInfoSignedInvalidTag(String tag)
    throws HierarchicalPropertiesException
    {
        setProperty(SECURITY_INFO_SIGNED_INVALID_TAG, tag);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_SIGNED_INVALID_TAG, order = 50)
    public String getSecurityInfoSignedInvalidTag()
    throws HierarchicalPropertiesException
    {
        return getProperty(SECURITY_INFO_SIGNED_INVALID_TAG);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_MIXED_CONTENT_TAG)
    public void setSecurityInfoMixedContentTag(String tag)
    throws HierarchicalPropertiesException
    {
        setProperty(SECURITY_INFO_MIXED_CONTENT_TAG, tag);
    }

    @Override
    @UserProperty(name = SECURITY_INFO_MIXED_CONTENT_TAG, order = 60)
    public String getSecurityInfoMixedContentTag()
    throws HierarchicalPropertiesException
    {
        return getProperty(SECURITY_INFO_MIXED_CONTENT_TAG);
    }
}
