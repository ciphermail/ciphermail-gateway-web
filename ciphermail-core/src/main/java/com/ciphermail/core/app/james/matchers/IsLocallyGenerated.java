/*
 * Copyright (c) 2018-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.util.AddressUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Matcher that matches all recipients if the message was locally generated, i.e., if the message was sent by
 * localhost. Optionally, a header matcher regular expression can be used to match on a specific header.
 */
public class IsLocallyGenerated extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(IsLocallyGenerated.class);

    /*
     * JSON property names
     */
    private static final String JSON_HEADER_NAME_PARAM = "header";
    private static final String JSON_HEADER_REGEX_PARAM = "regex";

    /*
     * The optional header name to check
     */
    private String headerName;

    /*
     * The header value regex
     */
    private Pattern headerValuePattern;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        String condition = StringUtils.trimToNull(getCondition());

        if (condition != null)
        {
            try {
                getLogger().info("Condition: {}", condition);

                JSONObject parameters = new JSONObject(condition);

                if (parameters.has(JSON_HEADER_NAME_PARAM)) {
                    headerName = StringUtils.trimToNull(parameters.getString(JSON_HEADER_NAME_PARAM));
                }

                if (headerName != null)
                {
                    if (!parameters.has(JSON_HEADER_REGEX_PARAM)) {
                        throw new MessagingException(JSON_HEADER_REGEX_PARAM + " parameter is missing");
                    }

                    String regex = StringUtils.trimToNull(parameters.getString(JSON_HEADER_REGEX_PARAM));

                    if (regex == null) {
                        throw new MessagingException(JSON_HEADER_REGEX_PARAM + " parameter value is empty");
                    }

                    headerValuePattern = Pattern.compile(regex);
                }
            }
            catch (JSONException e) {
                throw new MessagingException("The JSON string is invalid", e);
            }
        }
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        boolean match = false;

        try {
            if (AddressUtils.isLocalIpAddress(InetAddress.getByName(mail.getRemoteAddr())))
            {
                if (headerName != null)
                {
                    String[] headerValues = mail.getMessage().getHeader(headerName);

                    if (headerValues != null)
                    {
                        for (String headerValue : headerValues)
                        {
                            try {
                                headerValue = HeaderUtils.decodeHeaderValue(headerValue);

                                Matcher matcher = headerValuePattern.matcher(headerValue);

                                if (matcher.find())
                                {
                                    match = true;

                                    break;
                                }
                            }
                            catch (UnsupportedEncodingException e) {
                                getLogger().warn("Error decoding header value", e);
                            }
                        }
                    }
                }
                else {
                    match = true;
                }
            }
        }
        catch (UnknownHostException e) {
            getLogger().error("Error getting remote address", e);
        }

        return match ? MailAddressUtils.getRecipients(mail) : Collections.emptyList();
    }
}
