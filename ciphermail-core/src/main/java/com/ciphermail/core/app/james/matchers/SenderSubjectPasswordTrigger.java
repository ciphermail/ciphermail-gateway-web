/*
 * Copyright (c) 2021-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.regex.Matcher;

/**
 * Matcher which matches if the subject matches a regular expression and contains a reg ex group with a password. If
 * a match is found and a password is extracted, a password object will be added (@Passwords) with an entry for every
 * recipient
 */
public class SenderSubjectPasswordTrigger extends UserPropertiesSenderSubjectTrigger
{
    /*
     * JSON property names
     */
    private static final String JSON_PASSWORD_GROUP_NAME_PARAM = "groupName";

    private static final Logger logger = LoggerFactory.getLogger(SenderSubjectPasswordTrigger.class);

    /*
     * The name of the group from which the password will be read
     */
    private String passwordGroupName;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected String[] getSupportedNamedParameters()
    {
        String[] params = super.getSupportedNamedParameters();

        // Add new JSON param
        params = (String[]) ArrayUtils.add(params, JSON_PASSWORD_GROUP_NAME_PARAM);

        // We do not want users to override the RegEx and remove match setting
        params = (String[]) ArrayUtils.removeElement(params, JSON_REMOVE_PATTERN_PROPERTY_NAME_PARAM);

        return params;
    }

    @Override
    protected void init(JSONObject parameters)
    {
        super.init(parameters);

        passwordGroupName = StringUtils.trimToNull(parameters.optString(JSON_PASSWORD_GROUP_NAME_PARAM, "pw"));
    }

    /*
     * The trigger should always be removed because it contains a password
     */
    @Override
    protected boolean getDefaultRemovePattern() {
        return true;
    }

    @Override
    protected Collection<MailAddress> onMatch(Mail mail, Matcher matcher)
    throws MessagingException, IOException
    {
        Collection<MailAddress> result;

        String password = null;

        try {
            password = StringUtils.trimToNull(matcher.group(passwordGroupName));
        }
        catch (IllegalArgumentException e) {
            // Will be thrown if there is no named group
            getLogger().error("There is no group with name {}", passwordGroupName, e);
        }

        result = super.onMatch(mail, matcher);

        Passwords passwords = new Passwords();

        // If no password is extracted from the subject, we will still add the Passwords object but without any
        // passwords. A password validator should then reject the mail. The main reason we do it this way is to
        // make it possible to catch cases where the user uses the correct pattern but leave the password empty.
        // For example use the following subject "test pw=". If this would not match, we cannot warn the user of the
        // mistake
        if (password != null)
        {
            for (MailAddress recipient : result)
            {
                PasswordContainer passwordContainer = new PasswordContainer();
                passwordContainer.setPassword(password);

                passwords.put(recipient.toString(), passwordContainer);
            }
        }
        else {
            getLogger().warn("No password found in subject header");
        }

        CoreApplicationMailAttributes.setPasswords(mail, passwords);

        if (result == null) {
            result = new LinkedList<>();
        }

        return result;
    }
}
