/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.james.UserPersister;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;

/**
 * Mailet that generates User passwords for all recipients. The generated password will be of length passwordSize
 * and will be stored in the user properties and attached to the message as a Mail attribute
 * (see {@link CoreApplicationMailAttributes}). Each generated password will have an associated password ID. The password
 * ID is used to identify the generated password. The password ID is based on the current time in milliseconds.
 * To make the password ID smaller, the current time is subtracted by the baseTime. uniqueID is used to make sure
 * that the password ID will be 100% unique when multiple
 * servers are used.
 */
@SuppressWarnings("java:S6813")
public abstract class AbstractGeneratePassword extends AbstractCipherMailMailet
{
    /*
     * The activation context key
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "passwords";

    /*
     * Used for generating the password IDs.
     *
     * Note: the password IDs are not a secret, they just need to be random.
     */
    private SecureRandom randomGenerator;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    /*
     * The upper bound on the random number to be used for the password ID
     */
    private int randomIDBound = Integer.MAX_VALUE;

    /*
     * Because there can be multiple servers (or multiple instances of this mailet) and the password ID is
     * based on a counter and some time in minutes there should be a way to distinguish between the different
     * instances because otherwise there is a chance that the same password ID is generated when a message
     * is sent to the same user.
     */
    private String uniqueID;

    /*
     * Size in bytes of the passwords.
     */
    private int defaultPasswordLength = 16;

    /*
     * Callback used to track user objects.
     */
    protected static class ActivationContext
    {
        /*
         * The users with a password
         */
        private final Collection<User> users = new HashSet<>();

        /*
         * The passwords
         */
        private final Passwords passwords = new Passwords();

        /*
         * The mail object which is currently handled
         */
        private Mail mail;

        public Collection<User> getUsers() {
            return users;
        }

        public Passwords getPasswords() {
            return passwords;
        }

        public void clear()
        {
            users.clear();
            passwords.clear();
        }

        public Mail getMail() {
            return mail;
        }

        public void setMail(Mail mail) {
            this.mail = mail;
        }
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        RANDOM_ID_BOUND         ("randomIDBound"),
        UNIQUE_ID               ("uniqueID"),
        DEFAULT_PASSWORD_LENGTH ("defaultPasswordLength");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    private void initRandomIDBound()
    {
        String param = getInitParameter(Parameter.RANDOM_ID_BOUND.name);

        if (param != null)
        {
            randomIDBound = NumberUtils.toInt(param, randomIDBound);

            if (randomIDBound <= 0) {
                throw new IllegalArgumentException(Parameter.RANDOM_ID_BOUND.name + " must be > 0");
            }
        }
    }

    private void initUniqueID() {
        uniqueID = getInitParameter(Parameter.UNIQUE_ID.name);
    }

    private void initDefaultPasswordLength()
    {
        String param = getInitParameter(Parameter.DEFAULT_PASSWORD_LENGTH.name);

        if (param != null)
        {
            defaultPasswordLength = NumberUtils.toInt(param, defaultPasswordLength);

            if (defaultPasswordLength <= 0) {
                throw new IllegalArgumentException(Parameter.DEFAULT_PASSWORD_LENGTH.name +  " must be > 0");
            }
        }
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);

        initRandomIDBound();
        initUniqueID();
        initDefaultPasswordLength();

        try {
            randomGenerator = SecurityFactoryFactory.getSecurityFactory().createSecureRandom();
        }
        catch (Exception e) {
            throw new MessagingException("SecureRandom instance could not be created", e);
        }

        StrBuilder sb = new StrBuilder();

        sb.append("randomIDBound: ");
        sb.append(randomIDBound);
        sb.append("; ");
        sb.append("UniqueID: ");
        sb.append(uniqueID);
        sb.append("; ");
        sb.append("defaultPasswordLength: ");
        sb.append(defaultPasswordLength);

        getLogger().info("{}", sb);
    }

    /*
     * Create a "unique" password id based on a random integer
     */
    protected String generatePasswordID()
    {
        String passwordID = Integer.toString(randomGenerator.nextInt(randomIDBound));

        if (uniqueID != null) {
            passwordID = uniqueID + passwordID;
        }

        return passwordID;
    }

    protected int getPasswordLength(UserProperties userProperties)
    throws HierarchicalPropertiesException
    {
        PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(userProperties);

        int passwordLength = pdfProperties.getPasswordLength();

        if (passwordLength <= 0) {
            passwordLength = defaultPasswordLength;
        }

        return passwordLength;
    }

    /**
     * Override to set the password for the container. some fields, "password ID" and "password length", of the password
     * container are pre-configured.
     *
     * @param user The user for which the password should be generated
     * @param passwordContainer The password container.
     */
    protected abstract void generatePassword(User user, PasswordContainer passwordContainer)
    throws MessagingException;

    /**
     * Creates a new instance of a PasswordContainer. subclasses can override this to provide a different type.
     *
     * @return A new PasswordContainer instance
     */
    protected PasswordContainer createPasswordContainer() {
        return new PasswordContainer();
    }

    /**
     * Creates a new instance of ActivationContext. subclasses can override this to provide a different type.
     *
     * @return A new ActivationContext instance
     */
    protected ActivationContext createActivationContext() {
        return new ActivationContext();
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients for each recipient to generate a password for each recipient
     */
    @VisibleForTesting
    protected void onHandleUserEvent(@Nonnull User user)
    throws MessagingException
    {
        ActivationContext passwordActivationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                ActivationContext.class);

        try {
            PasswordContainer passwordContainer = createPasswordContainer();

            UserProperties userProperties = user.getUserPreferences().getProperties();

            passwordContainer.setPasswordID(generatePasswordID());
            passwordContainer.setPasswordLength(getPasswordLength(userProperties));

            generatePassword(user, passwordContainer);

            getLogger().debug("Password container: {} for user: {}", passwordContainer, user.getEmail());

            passwordActivationContext.getPasswords().put(user.getEmail(), passwordContainer);
            passwordActivationContext.getUsers().add(user);
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error generating password for user " + user, e);
        }
    }

    private void makeNonPersistentUsersPersistent(Collection<User> users) {
        UserPersister.getInstance(userWorkflow, transactionOperations).tryToMakeNonPersistentUsersPersistent(users);
    }

    private void addPasswordsAttribute(Mail mail, Passwords passwords) {
        CoreApplicationMailAttributes.setPasswords(mail, passwords);
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            ActivationContext passwordsActivationContext = createActivationContext();

            // The mail object will be store in the context for other mailets
            passwordsActivationContext.setMail(mail);

            // Place the passwordsActivationContext in the context so we can use it in onHandleUserEvent
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, passwordsActivationContext);

            MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                    this::onHandleUserEvent, getLogger());

            RetryListener retryListener = new AbstractRetryListener()
            {
                @Override
                public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                        Throwable throwable)
                {
                    passwordsActivationContext.clear();
                }
            };

            mailAddressHandler.handleMailAddressesWithRetry(MailAddressUtils.getRecipients(mail), retryListener);

            addPasswordsAttribute(mail, passwordsActivationContext.getPasswords());

            makeNonPersistentUsersPersistent(passwordsActivationContext.getUsers());
        }
        catch(MessagingException e) {
            getLogger().error("Error generating passwords.", e);
        }
    }
}
