/*
 * Copyright (c) 2018-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.james.server.core.MailImpl;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;

/**
 * Mailet which sets the sender of the Mail to a new sender.
 */
public class SetSender extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SetSender.class);

    /*
     * The new sender
     */
    private MailAddress sender;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SENDER ("sender");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException
    {
        String rawSender = StringUtils.trimToNull(getInitParameter(Parameter.SENDER.name));

        if (rawSender != null) {
            sender = new MailAddress(rawSender);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        logger.debug("Replacing sender '{}' by '{}'", mail.getMaybeSender() , sender);

        // We can only change the sender if the mail implementation is MailImpl. In practice, this is always
        // the case.
        if (mail instanceof MailImpl mailImpl) {
            mailImpl.setSender(sender);
        }
        else {
            logger.warn("Unable to rewrite sender. Mail is not a MailImpl but a {}", mail.getClass());
        }
    }
}
