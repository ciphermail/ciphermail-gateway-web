/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.DomainManager;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesSelector;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.util.DomainUtils;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * UserPreferencesSelector that selects the UserPreferences based on the domain of the email address.
 *
 * @author Martijn Brinkers
 *
 */
public class DefaultUserPreferencesSelector implements UserPreferencesSelector
{
    private final DomainManager domainManager;

    public DefaultUserPreferencesSelector(@Nonnull DomainManager domainManager) {
        this.domainManager = Objects.requireNonNull(domainManager);
    }

    /**
     * Returns a ordered (possibly empty) set of UserPreferences that match the domain of the user. A domain
     * is first matched against the wildcard domain (eg. *.example.com) and then matched agains the fully
     * qualified domain (example.com).
     */
    @Override
    public Set<UserPreferences> select(@Nonnull User user)
    {
        Set<UserPreferences> selected = new LinkedHashSet<>();

        String domain = EmailAddressUtils.getDomain(user.getEmail());

        if (StringUtils.isNotEmpty(domain))
        {
            UserPreferences wildcardPreferences = domainManager.getDomainPreferences("*." + domain);

            if (wildcardPreferences == null)
            {
                // Check if there is a wildcard for one level up
                String upperDomain = DomainUtils.getUpperLevelDomain(domain);

                if (StringUtils.isNotEmpty(upperDomain)) {
                    wildcardPreferences = domainManager.getDomainPreferences("*." + upperDomain);
                }
            }

            // Get the prefs of the domain itself (if present)
            UserPreferences domainPreferences = domainManager.getDomainPreferences(domain);

            // add the preferences from least specific to most specific. A wildcard domain is less
            // specific than a non wildcard domain.
            if (wildcardPreferences != null) {
                selected.add(wildcardPreferences);
            }

            if (domainPreferences != null) {
                selected.add(domainPreferences);
            }
        }

        return selected;
    }
}
