/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

/**
 * Abstract matcher that matches when a user (if sender or receiver depends on the class extending this class) has skip
 * calendar. The matcher condition determines which user property is checked:
 *
 * - skipSMIME        : checks the isSMIMESkipCalendar user property
 * - skipSMIMESigning : checks the isSMIMESkipSigningCalendar user property
 * - skipSMIMEBoth    : checks the isSMIMESkipCalendar and/or isSMIMESkipSigningCalendar user property
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings("java:S6813")
public abstract class AbstractIsSMIMESkipCalendar extends AbstractContextAwareCipherMailMatcher
{
    private enum SkipParameter
    {
        SKIP_SMIME          ("skipSMIME"),
        SKIP_SMIME_SIGNING  ("skipSMIMESigning"),
        SKIP_SMIME_BOTH     ("skipSMIMEBoth");

        private final String name;

        SkipParameter(String name) {
            this.name = name;
        }

        public static SkipParameter fromString(String name)
        {
            for (SkipParameter parameter : SkipParameter.values())
            {
                if (parameter.name.equals(name)) {
                    return parameter;
                }
            }

            return null;
        }
    }

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    private SkipParameter skipParameter;

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        String condition = getCondition();

        skipParameter = SkipParameter.fromString(condition);

        if (skipParameter == null) {
            throw new IllegalArgumentException(condition + " is not a valid SkipParameter.");
        }

        StrBuilder sb = new StrBuilder();

        sb.append("Skip Parameter: ");
        sb.append(skipParameter);

        getLogger().info("{}", sb);
    }

    protected Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = AbstractIsSMIMESkipCalendar.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses);
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @VisibleForTesting
    protected boolean hasMatch(User user)
    throws MessagingException
    {
        // Return true if user needs to skip a calendar
        try {
            SMIMEProperties smimeProperties = createSMIMEProperties(user.getUserPreferences().getProperties());

            return switch (skipParameter) {
                case SKIP_SMIME         -> smimeProperties.getSMIMESkipCalendar();
                case SKIP_SMIME_SIGNING -> smimeProperties.getSMIMESkipSigningCalendar();
                case SKIP_SMIME_BOTH    -> smimeProperties.getSMIMESkipCalendar() ||
                                           smimeProperties.getSMIMESkipSigningCalendar();
                default -> throw new IllegalArgumentException("Unknown skipParameter: " + skipParameter);
            };
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error in hasMatch.", e);
        }
    }

    protected UserWorkflow getUserWorkflow() {
        return userWorkflow;
    }

    protected MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
        return messageOriginatorIdentifier;
    }
}
