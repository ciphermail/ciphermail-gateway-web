/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertificate;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Set;

/**
 * A user identifies a user by his/her email address.
 *
 * @author Martijn Brinkers
 *
 */
public interface User
{
    /**
     * a user is uniquely identified by his/her email. The email address should be a valid email
     * address.
     * <p>
     * Note: because a user is uniquely identified by the email address an implementor should override equals in such
     * a way that a user is equal iff email is equal.
     */
    String getEmail();

    /**
     * Returns preferences of this user.
     */
    UserPreferences getUserPreferences();

    /**
     * Returns a Set of encryption certificates that are automatically selected.
     * <p>
     * If the property "auto select encryption certificates" is enabled for a user, all certificates which are
     * valid for encryption, trusted, not revoked and for which the email address of the certificate matches the
     * email address of the user, will be returned. Inherited or manually selected certificates or certificates
     * which are PKI wise not valid are not part of the auto selected certificates.
     */
    @Nonnull Set<X509Certificate> getAutoSelectEncryptionCertificates()
    throws HierarchicalPropertiesException;

    /**
     * Sets the key and certificate for signing messages.
     */
    void setSigningKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException;

    /**
     * Returns the signing certificate for a user. A signing certificate always has a private key associated with
     * the certificate, i.e., the key alias is set. If the property "alwaysUseFreshestSigningCert" is enabled for
     * the user, the latest signing certificate will be automatically selected and returned. If
     * "alwaysUseFreshestSigningCert" is not enabled and a signing certificate is manually selected and is still
     * valid the manually selected certificate will be returned. If there is no manually selected certificate or
     * if the manually selected certificate is not valid, the latest signing certificate will be automatically
     * selected. If there is no valid signing certificate, the an empty result will be returned.
     */
    KeyAndCertificate getSigningKeyAndCertificate()
    throws CertStoreException, KeyStoreException, HierarchicalPropertiesException;
}
