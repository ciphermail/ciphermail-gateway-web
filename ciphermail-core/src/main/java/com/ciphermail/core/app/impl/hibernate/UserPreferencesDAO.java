/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.AbstractScrollableResultsIterator;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.hibernate.NamedBlobEntity;
import com.ciphermail.core.common.security.certstore.hibernate.X509CertStoreEntity;
import com.ciphermail.core.common.util.CloseableIterator;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;

public class UserPreferencesDAO extends GenericHibernateDAO
{
    private static final String ENTITY_NAME = UserPreferencesEntity.ENTITY_NAME;

    private UserPreferencesDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static UserPreferencesDAO getInstance(@Nonnull SessionAdapter session) {
        return new UserPreferencesDAO(session);
    }

    public UserPreferencesEntity getPreferences(@Nonnull String category, @Nonnull String name)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<UserPreferencesEntity> criteriaQuery = criteriaBuilder.createQuery(UserPreferencesEntity.class);

        Root<UserPreferencesEntity> rootEntity = criteriaQuery.from(UserPreferencesEntity.class);

        criteriaQuery.where(criteriaBuilder.and(
                criteriaBuilder.equal(rootEntity.get(UserPreferencesEntity.CATEGORY_COLUMN), category),
                criteriaBuilder.equal(rootEntity.get(UserPreferencesEntity.NAME_COLUMN), name)));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    public CloseableIterator<String> getPreferencesIterator(@Nonnull String category)
    {
        // We will use HQL because I do not (yet?) know how to return just a field instead of
        // an object when using the Criteria API.
        String hql = "select p.name from " + ENTITY_NAME + " p where p.category = :category";

        Query<String> query = createQuery(hql, String.class);

        query.setParameter("category", category);

        return new StringIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    public CloseableIterator<String> getPreferencesIterator(@Nonnull String category, Integer firstResult,
            Integer maxResults, SortDirection sortDirection)
    {
        if (sortDirection == null) {
            sortDirection = SortDirection.ASC;
        }

        // We will use HQL because I do not (yet?) know how to return just a field instead of
        // an object when using the Criteria API.
        String hql = "select p.name from " + ENTITY_NAME + " p where p.category = :category order by p.name " +
                sortDirection;

        Query<String> query = createQuery(hql, String.class);

        query.setParameter("category", category);

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new StringIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    public CloseableIterator<String> getCategoryIterator()
    {
        // We will use HQL because I do not (yet?) know how to return just a field instead of
        // an object when using the Criteria API.
        String hql = "select distinct p.category from " + ENTITY_NAME + " p";

        return new StringIterator(createQuery(hql, String.class).scroll(ScrollMode.FORWARD_ONLY));
    }

    public @Nonnull UserPreferencesEntity addPreferences(@Nonnull String category, @Nonnull String name) {
        return persist(new UserPreferencesEntity(category, name));
    }

    public void deletePreferences(@Nonnull UserPreferencesEntity userPreferencesEntity) {
        delete(userPreferencesEntity);
    }

    /**
     * Checks if the given userPreferencesEntity is inherited by another UserPreferencesEntity.
     */
    public boolean isInherited(@Nonnull UserPreferencesEntity userPreferencesEntity)
    {
        String hql = "select count(*) from " + ENTITY_NAME +
            " u where :other in (select ip.inheritedPreferences from u.inheritedPreferences ip)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", userPreferencesEntity);

        return query.uniqueResultOptional().orElse(0L) > 0;
    }

    /**
     * Checks if the given X509CertStoreEntity is in the certificate collection of a
     * UserPreferences instance.
     */
    public long referencingFromCertificatesCount(@Nonnull X509CertStoreEntity certStoreEntry)
    {
        String hql = "select count(*) from " + ENTITY_NAME +
            " u where :other in (select c from u.certificates c)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", certStoreEntry);

        return query.uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns a list of UserPreferencesEntity's that references the certificate entry from the certificates collection.
     */
    public List<UserPreferencesEntity> getReferencingFromCertificates(
            @Nonnull X509CertStoreEntity certStoreEntry, Integer firstResult, Integer maxResults)
    {
        String hql = "select u from " + ENTITY_NAME + " u where :other in (select c from u.certificates c)";

        Query<UserPreferencesEntity> query = createQuery(hql, UserPreferencesEntity.class);

        query.setParameter("other", certStoreEntry);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    /**
     * Checks if the given X509CertStoreEntity is in the named certificate collection of a
     * UserPreferences instance.
     */
    public long referencingFromNamedCertificatesCount(@Nonnull X509CertStoreEntity certStoreEntry)
    {
        String hql = "select count(*) from " + ENTITY_NAME +
            " u where :other in (select c.certificateEntry from u.namedCertificates c)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", certStoreEntry);

        return query.uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns a list of UserPreferencesEntity's that references the certificate entry from the named certificates
     * collection.
     */
    public List<UserPreferencesEntity> getReferencingFromNamedCertificates(
            @Nonnull X509CertStoreEntity certStoreEntry, Integer firstResult, Integer maxResults)
    {
        String hql = "select u from " + ENTITY_NAME +
            " u where :other in (select c.certificateEntry from u.namedCertificates c)";

        Query<UserPreferencesEntity> query = createQuery(hql, UserPreferencesEntity.class);

        query.setParameter("other", certStoreEntry);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    /**
     * Checks if the given X509CertStoreEntity is used as a signing key
     */
    public long referencingFromKeyAndCertificateCount(@Nonnull X509CertStoreEntity certStoreEntry)
    {
        String hql = "select count(*) from " + ENTITY_NAME +
            " u where :other in (select c from u.keyAndCertificateEntry c)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", certStoreEntry);

        return query.uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns a list of UserPreferencesEntity's that references the certificate from the key and certificate.
     */
    public List<UserPreferencesEntity> getReferencingFromKeyAndCertificate(
            @Nonnull X509CertStoreEntity certStoreEntry, Integer firstResult, Integer maxResults)
    {
        String hql = "select u from " + ENTITY_NAME +
            " u where :other in (select c from u.keyAndCertificateEntry c)";

        Query<UserPreferencesEntity> query = createQuery(hql, UserPreferencesEntity.class);

        query.setParameter("other", certStoreEntry);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    /**
     * Checks if the given X509CertStoreEntity is referenced by a UserPreferences instance.
     */
    public boolean isReferencing(@Nonnull X509CertStoreEntity certStoreEntry)
    {
        boolean referencing = referencingFromCertificatesCount(certStoreEntry) > 0;

        if (!referencing) {
            referencing = referencingFromKeyAndCertificateCount(certStoreEntry) > 0;
        }

        if (!referencing) {
            referencing = referencingFromNamedCertificatesCount(certStoreEntry) > 0;
        }

        return referencing;
    }

    public long getUserPreferencesCount(@Nonnull String category)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<UserPreferencesEntity> rootEntity = criteriaQuery.from(UserPreferencesEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(UserPreferencesEntity.CATEGORY_COLUMN), category));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns the number of UserPreferences that references the NamedBlob
     */
    public long referencingFromNamedBlobsCount(@Nonnull NamedBlobEntity namedBlob)
    {
        String hql = "select count(*) from " + ENTITY_NAME +
                " u where :other in (select c from u.namedBlobs c)";

        Query<Long> query = createQuery(hql, Long.class);

        query.setParameter("other", namedBlob);

        return query.uniqueResultOptional().orElse(0L);
    }

    /**
     * Returns the UserPreferences that references the NamedBlob
     */
    public List<UserPreferencesEntity> getReferencingFromNamedBlobs(@Nonnull NamedBlobEntity namedBlob,
            Integer firstResult, Integer maxResults)
    {
        String hql = "select u from " + ENTITY_NAME +
            " u where :other in (select c from u.namedBlobs c)";

        Query<UserPreferencesEntity> query = createQuery(hql, UserPreferencesEntity.class);

        query.setParameter("other", namedBlob);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.list();
    }

    private static class StringIterator extends AbstractScrollableResultsIterator<String>
    {
        public StringIterator(ScrollableResults<String> results) {
            super(results);
        }

        @Override
        protected boolean hasMatch(String element) {
            return element != null;
        }
    }
}
