/*
 * Copyright (c) 2010-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.UserPersister;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.ca.CA;
import com.ciphermail.core.common.security.ca.CAException;
import com.ciphermail.core.common.security.ca.CAProperties;
import com.ciphermail.core.common.security.ca.CAPropertiesProvider;
import com.ciphermail.core.common.security.ca.CancelCertificateRequestException;
import com.ciphermail.core.common.security.ca.CertificateRequestStore;
import com.ciphermail.core.common.security.ca.RequestParameters;
import com.ciphermail.core.common.security.ca.RequestUtils;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.ciphermail.core.common.util.KeyedBarrier;
import com.ciphermail.core.common.util.Match;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * Requests a certificate for the sender.
 */
@SuppressWarnings({"java:S6813"})
public class RequestSenderCertificate extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(RequestSenderCertificate.class);

    /*
     * The activation context key
     */
    private static final String ACTIVATION_CONTEXT_KEY = "activationContext";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Identifies the 'sender' of the message (default version uses from as the identifier)
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used for requesting certificates
     */
    @Inject
    private CA ca;

    /*
     * Store that stores pending certificate requests
     */
    @Inject
    private CertificateRequestStore certificateRequestStore;

    /*
     * Used to get the CA settings
     */
    @Inject
    private CAPropertiesProvider caPropertiesProvider;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * If true a certificate won't be requested if the sender already has a signing certificate or if the certificate
     * request store already contains a pending request for the sender.
     */
    private boolean skipIfAvailable = true;

    /*
     * If true and a certificate is created, the user will be added
     */
    private boolean addUser = true;

    /*
     * If true, requests are synchronized using a KeyedBarrier
     */
    private boolean synchronizeRequests = true;

    /*
     * Used for synchronizing certificate requests to make sure that no certificate is requested
     * multiple times when a message is sent by the same user in a short timeframe
     */
    private final KeyedBarrier<InternetAddress, Null> keyedBarrier = new KeyedBarrier<>();

    /*
     * The maximum time (in msec) a request can be blocked by the KeyedBarrier
     */
    private static final long KEYED_BARRIER_TIMEOUT = DateUtils.MILLIS_PER_SECOND * 60;

    /*
     * The certificate request handler to use. If not specified the certificate request handler
     * specified by the CAProperties will be used
     */
    private String certificateRequestHandler;

    /*
     * used to track user objects.
     */
    private static class ActivationContext
    {
        private User user;

        public void setUser(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public void clear() {
            user = null;
        }
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SKIP_IF_AVAILABLE           ("skipIfAvailable"),
        ADD_USER                    ("addUser"),
        SYNCHRONIZE_REQUESTS        ("synchronizeRequests"),
        CERTIFICATE_REQUEST_HANDLER ("certificateRequestHandler");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(ca);
        Objects.requireNonNull(certificateRequestStore);
        Objects.requireNonNull(caPropertiesProvider);
        Objects.requireNonNull(transactionOperations);

        skipIfAvailable = getBooleanInitParameter(Parameter.SKIP_IF_AVAILABLE.name, skipIfAvailable);

        addUser = getBooleanInitParameter(Parameter.ADD_USER.name, addUser);

        synchronizeRequests = getBooleanInitParameter(Parameter.SYNCHRONIZE_REQUESTS.name, synchronizeRequests);

        certificateRequestHandler = getInitParameter(Parameter.CERTIFICATE_REQUEST_HANDLER.name);

        StrBuilder sb = new StrBuilder();

        sb.append("skipIfAvailable: ");
        sb.append(skipIfAvailable);
        sb.append("; ");
        sb.append("addUser: ");
        sb.append(addUser);
        sb.append("; ");
        sb.append("synchronizeRequests: ");
        sb.append(synchronizeRequests);
        sb.append("; ");
        sb.append("certificateRequestHandler: ");
        sb.append(certificateRequestHandler);

        getLogger().info("{}", sb);
    }

    private void makeNonPersistentUserPersistent(User user)
    {
        if (user != null && addUser)
        {
            UserPersister.getInstance(userWorkflow, transactionOperations).tryToMakeNonPersistentUsersPersistent(
                    Collections.singleton(user));
        }
    }

    private void requestCertificate(User user)
    throws MessagingException
    {
        try {
            CAProperties caProperties = caPropertiesProvider.getProperties();

            RequestParameters request = RequestUtils.createRequestParameters(user.getEmail(), caProperties);

            // Override the certificate request handler from the CAProperties if this mailet overrides it
            if (StringUtils.isNotEmpty(certificateRequestHandler)) {
                request.setCertificateRequestHandler(certificateRequestHandler);
            }

            ca.requestCertificate(request);
        }
        catch (HierarchicalPropertiesException | IOException | CAException e) {
            throw new MessagingException("Error requesting certificate", e);
        }
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(User user)
    throws MessagingException
    {
        try {
            ActivationContext activationContext = Objects.requireNonNull(getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                    ActivationContext.class));

            boolean requestCertificate = true;

            if (skipIfAvailable)
            {
                if (user.getSigningKeyAndCertificate() != null)
                {
                    logger.debug("The user {} already has a valid signing certificate.", user.getEmail());

                    requestCertificate = false;
                }

                if (requestCertificate && certificateRequestStore.getSizeByEmail(user.getEmail(), Match.EXACT) > 0)
                {
                    logger.debug("there is already a pending request for user {}", user.getEmail());

                    requestCertificate = false;
                }
            }

            if (requestCertificate)
            {
                requestCertificate(user);

                activationContext.setUser(user);
            }
        }
        catch(KeyStoreException | CertStoreException | HierarchicalPropertiesException e) {
            throw new MessagingException("Error getting signing key for user: " + user.getEmail(), e);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            ActivationContext activationContext = new ActivationContext();

            // Place the activationContext in the context so we can use it in onHandleUserEvent
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, activationContext);

            MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                    this::onHandleUserEvent, getLogger());

            InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

            RetryListener retryListener = new AbstractRetryListener()
            {
                @Override
                public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                        Throwable throwable)
                {
                    activationContext.clear();
                }
            };

            // If the From is an invalid email address invalid@invalid.tld will be returned. We don't want to request
            // a certificate for that address.
            if (!EmailAddressUtils.isInvalidDummyAddress(originator))
            {
                Callable<Null> callable = () ->
                {
                    mailAddressHandler.handleMailAddressesWithRetry(
                            MailAddressUtils.fromAddressArrayToMailAddressList(originator),
                            retryListener);

                    // activationContext.getUser() will return null if there was no certificate requested for the user
                    makeNonPersistentUserPersistent(activationContext.getUser());

                    return null;
                };

                try {
                    if (synchronizeRequests) {
                        // Synchronize certificate requests to make sure only one certificate is requested for
                        // a user even if multiple messages are sent by the same user at the same time
                        keyedBarrier.execute(originator, callable, KEYED_BARRIER_TIMEOUT);
                    }
                    else {
                        callable.call();
                    }
                }
                catch (Exception e) {
                    throw new MessagingException("Error requesting a certificate", e);
                }
            }
            else {
                logger.debug("Message originator is an invalid email address. Certificate request will be skipped.");
            }
        }
        catch(Exception e)
        {
            Throwable rootCause = ExceptionUtils.getRootCause(e);

            // If the MessagingException is caused by a CancelCertificateRequestException, do not log stacktrace
            if (rootCause instanceof CancelCertificateRequestException) {
                getLogger().warn("Certificate request was canceled. Message: {}", rootCause.getMessage());
            }
            else {
                getLogger().error("Error requesting certificates.", e);
            }
        }
    }
}
