/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.AbstractScrollableResultsIterator;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.util.CloseableIterator;
import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.Optional;

public class UserDAO extends GenericHibernateDAO
{
    private static final String ENTITY_NAME = UserEntity.ENTITY_NAME;

    private UserDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Creates a new DAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static UserDAO getInstance(@Nonnull SessionAdapter session) {
        return new UserDAO(session);
    }

    /**
     * Returns the user with the given email.
     * @param email The email of the user. The email should be a valid email address.
     * @return the UserEntity or null if there is no user with the given email.
     */
    public UserEntity getUser(@Nonnull String email)
    {
        email = Objects.requireNonNull(EmailAddressUtils.canonicalizeAndValidate(email, true),
                email + " is not a valid email address");

        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<UserEntity> criteriaQuery = criteriaBuilder.createQuery(UserEntity.class);

        Root<UserEntity> rootEntity = criteriaQuery.from(UserEntity.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(UserEntity.EMAIL_COLUMN), email));

        Optional<UserEntity> optionalUser = createQuery(criteriaQuery).uniqueResultOptional();

        return optionalUser.orElse(null);
    }

    /**
     * Returns an iterator over the users.
     * @param firstResult the index of the first result. If null, the index start at 0.
     * @param maxResults the maximum number of results to return. If null, all elements will be returned.
     * @param sortDirection if null, sorting will be ASC.
     * @return
     */
    public CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        if (sortDirection == null) {
            sortDirection = SortDirection.ASC;
        }

        // We will use HQL because I do not (yet?) know how to return just a field instead of
        // an object when using the Criteria API.
        String hql = String.format("select n.%s from %s n order by n.%s %s",
                UserEntity.EMAIL_COLUMN, ENTITY_NAME, UserEntity.EMAIL_COLUMN, sortDirection);

        Query<String> query = createQuery(hql, String.class);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new EmailIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /**
     * Returns an iterator over the users which match the search
     * @param search the email address (or part of) to search for
     * @param firstResult the index of the first result. If null, the index start at 0.
     * @param maxResults the maximum number of results to return. If null, all elements will be returned.
     * @param sortDirection if null, sorting will be ASC.
     * @return The email addresses that match the search
     */
    public CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        if (sortDirection == null) {
            sortDirection = SortDirection.ASC;
        }

        // We will use HQL because I do not (yet?) know how to return just a field instead of
        // an object when using the Criteria API.
        //
        // Note: HQL does not support ilike. To make sure that search is done case insensitive we will convert
        // the search to lowercase because email addresses should have been normalized before adding. We will do
        // the same for #getSearchEmailCount to make sure it works the same way.
        String hql = String.format("select n.%s from %s n where n.%s like :search order by n.%s %s",
                UserEntity.EMAIL_COLUMN, ENTITY_NAME, UserEntity.EMAIL_COLUMN, UserEntity.EMAIL_COLUMN, sortDirection);

        Query<String> query = createQuery(hql, String.class);

        query.setParameter("search", StringUtils.lowerCase(search));

        if (firstResult != null)
        {
            query.setFirstResult(firstResult);

            // For some reason, PGSQL does not like that maxResults is set to null if firstResult is not set to null
            // The exception message is:
            //
            // Operation requires a scrollable ResultSet, but this ResultSet is FORWARD_ONLY.
            //
            // If firstResult is therefore set, we will set maxReslts to Integer.MAX_VALUE
            if (maxResults == null) {
                maxResults = Integer.MAX_VALUE;
            }
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return new EmailIterator(query.scroll(ScrollMode.FORWARD_ONLY));
    }

    /**
     * Returns the number of results that will match the search.
     * @param search the email address (or part of) to search for
     * @return The number of matching addresses
     */
    public long getSearchEmailCount(@Nonnull String search)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<UserEntity> rootEntity = criteriaQuery.from(UserEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        criteriaQuery.where(criteriaBuilder.like(rootEntity.get(UserEntity.EMAIL_COLUMN),
                StringUtils.lowerCase(search)));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    /**
     * Create a new user.
     * @param email The user to create
     * @param makePersistent If makePersistent is true, the user will be stored in the database
     * @return The newly created User Entity
     */
    public @Nonnull UserEntity addUser(@Nonnull String email, boolean makePersistent)
    {
        UserEntity userEntity = new UserEntity(email);

        if (makePersistent) {
            userEntity = persist(userEntity);
        }

        return userEntity;
    }

    /**
     * Deletes the user
     * @param userEntity The user to delete
     */
    public void deleteUser(@Nonnull UserEntity userEntity) {
        delete(userEntity);
    }

    /**
     * @return The total count of all users
     */
    public long getUserCount()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<UserEntity> rootEntity = criteriaQuery.from(UserEntity.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }

    private static class EmailIterator extends AbstractScrollableResultsIterator<String>
    {
        public EmailIterator(ScrollableResults<String> results) {
            super(results);
        }

        @Override
        protected boolean hasMatch(String element) {
            return element != null;
        }
    }
}
