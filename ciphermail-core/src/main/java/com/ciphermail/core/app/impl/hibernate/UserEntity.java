/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.UuidGenerator;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.UUID;

@Entity(name = UserEntity.ENTITY_NAME)
@Table
public class UserEntity
{
    static final String ENTITY_NAME = "User";

    static final String ID_COLUMN = "id";
    static final String EMAIL_COLUMN = "email";

    /*
     * Maximum length of an email address.
     * 64 for local part + @ + 255 for domain.
     */
    private static final int MAX_EMAIL_LENGTH = 64 + 1 + 255;

    @Id
    @Column(name = ID_COLUMN)
    @UuidGenerator
    private UUID id;

    @Column (name = EMAIL_COLUMN, length = MAX_EMAIL_LENGTH, unique = true, nullable = false)
    private String email;

    @OneToOne
    @Cascade(CascadeType.ALL)
    private UserPreferencesEntity userPreferencesEntity;

    protected UserEntity() {
        // required by Hibernate
    }

    public UserEntity(@Nonnull String email)
    {
        // Email must be a valid email addresses
        this.email = Objects.requireNonNull(EmailAddressUtils.canonicalizeAndValidate(email, true),
                email + " is not a valid email address");

        // the name of the associated preferences object will be prefixed
        this.userPreferencesEntity = new UserPreferencesEntity(UserPreferencesCategory.USER.name(), email);
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public UserPreferencesEntity getUserPreferencesEntity() {
        return userPreferencesEntity;
    }

    @Override
    public String toString() {
        return getEmail();
    }

    /**
     * User is equals iff email address is equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof UserEntity rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(email, rhs.email)
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(email)
            .toHashCode();
    }
}
