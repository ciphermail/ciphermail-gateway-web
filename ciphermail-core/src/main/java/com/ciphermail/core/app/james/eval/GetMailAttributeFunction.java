/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.eval;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.function.Function;
import net.sourceforge.jeval.function.FunctionConstants;
import net.sourceforge.jeval.function.FunctionException;
import net.sourceforge.jeval.function.FunctionHelper;
import net.sourceforge.jeval.function.FunctionResult;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Objects;

public class GetMailAttributeFunction implements Function
{
    private static final Logger logger = LoggerFactory.getLogger(GetMailAttributeFunction.class);

    private final Mail mail;

    public GetMailAttributeFunction(@Nonnull Mail mail) {
        this.mail = Objects.requireNonNull(mail);
    }

    @Override
    public String getName() {
        return "getMailAttribute";
    }

    @Override
    public @Nonnull FunctionResult execute(Evaluator evaluator, String arguments)
    throws FunctionException
    {
        String attributeName = FunctionHelper.trimAndRemoveQuoteChars(arguments, evaluator.getQuoteCharacter());

        logger.atDebug().log("Getting mail attribute {} for Mail with MailID {}", attributeName,
                CoreApplicationMailAttributes.getMailID(mail));

        String value = MailAttributesUtils.attributeAsString(mail, attributeName);

        if (value == null)
        {
            logger.atDebug().log("Mail attribute {} for Mail with MailID {} is null", attributeName,
                    CoreApplicationMailAttributes.getMailID(mail));

            value = "";
        }

        return new FunctionResult(value, FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
