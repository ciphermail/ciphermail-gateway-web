/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.smtp;

import com.ciphermail.core.common.mail.XTextEncoder;
import com.ciphermail.core.common.util.NameValueIterator;
import org.apache.commons.lang.StringUtils;
import org.apache.james.protocols.api.ProtocolSession;
import org.apache.james.protocols.api.Request;
import org.apache.james.protocols.api.Response;
import org.apache.james.protocols.api.handler.CommandHandler;
import org.apache.james.protocols.smtp.SMTPResponse;
import org.apache.james.protocols.smtp.SMTPRetCode;
import org.apache.james.protocols.smtp.SMTPSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Set;

/**
 * Command handler for XFORWARD SMTP command.
 * <p>
 * See <a href="http://www.postfix.org/XFORWARD_README.html">...</a>.
 */
public class XForwardCmdHandler implements CommandHandler<SMTPSession>
{
    private static final Logger logger = LoggerFactory.getLogger(XForwardCmdHandler.class);

    /*
     * [UNAVAILABLE] string as defined by http://www.postfix.org/XFORWARD_README.html
     */
    private static final String UNAVAILABLE = "[UNAVAILABLE]";

    /*
     * The name of the command handled by the command handler
     */
    private static final Collection<String> COMMANDS = Set.of("XFORWARD");

    /**
     * Name under which the XFORWARDED address will be stored
     */
    public static final ProtocolSession.AttachmentKey<String> XFORWARD_ADDRESS = ProtocolSession.AttachmentKey.of(
            "XFORWARD_ADDRESS", String.class);

    /**
     * Name under which the XFORWARDED host will be stored
     */
    public static final ProtocolSession.AttachmentKey<String> XFORWARD_HOST = ProtocolSession.AttachmentKey.of(
            "XFORWARD_HOST", String.class);

    @Override
    public Collection<String> getImplCommands() {
        return COMMANDS;
    }

    @Override
    public Response onCommand(SMTPSession session, Request request)
    {
        doXFoward(session, request.getArgument());

        return new SMTPResponse(SMTPRetCode.MAIL_OK, "OK");
    }

    private void doXFoward(SMTPSession session, String argument)
    {
        logger.debug("XForward: {}", argument);

        if (argument != null)
        {
            NameValueIterator iterator = new NameValueIterator(argument);

            while (iterator.hasNext())
            {
                NameValueIterator.Entry nameValue = iterator.next();

                XForwardAttribute attribute = XForwardAttribute.fromName(nameValue.getName());

                if (attribute == null) {
                    logger.warn("Unsupported XForward attribute: {}", nameValue.getName());
                }

                // XFORWARD arguments are XText encoded
                String value = StringUtils.trimToNull(XTextEncoder.decode(nameValue.getValue()));

                if (value == null)
                {
                    logger.debug("value is null. ");

                    continue;
                }

                if (attribute != null)
                {
                    switch (attribute) {
                        case NAME   -> handleName(value, session);
                        case ADDR   -> handleAddr(value, session);
                        case PORT   -> handlePort(value);
                        case PROTO  -> handleProto(value);
                        case HELO   -> handleHelo(value);
                        case SOURCE -> handleSource(value);
                        default -> logger.warn("Unknown XForward attribute: {}", nameValue.getValue());
                    }
                }
                else {
                    logger.warn("XForward attribute is null");
                }
            }
        }
    }

    private void handleAddr(String argument, SMTPSession session)
    {
        logger.debug("XForward handleAddr: {}", argument);

        // argument can be an IPV4 address or an IPV6 address. An IPV6 address starts with IPV6
        String address = StringUtils.removeStartIgnoreCase(argument, "IPV6:");

        if (!StringUtils.equalsIgnoreCase(address, UNAVAILABLE)) {
            session.setAttachment(XFORWARD_ADDRESS, address, ProtocolSession.State.Connection);
        }
        else {
            logger.warn("XForward address is unavailable");
        }
    }

    private void handleName(String argument, SMTPSession session)
    {
        logger.debug("XForward handleName: {}", argument);

        if (!StringUtils.equalsIgnoreCase(argument, UNAVAILABLE)) {
            session.setAttachment(XFORWARD_HOST, argument, ProtocolSession.State.Connection);
        }
    }

    private void handlePort(String argument)
    {
        logger.debug("XForward handlePort: {}", argument);

        // not implemented
    }

    private void handleProto(String argument)
    {
        logger.debug("XForward handleProto: {}", argument);

        // not implemented
    }

    private void handleHelo(String argument)
    {
        logger.debug("XForward handleHelo: {}", argument);

        // not implemented
    }

    private void handleSource(String argument)
    {
        logger.debug("XForward handleSource: {}", argument);

        // not implemented
    }
}
