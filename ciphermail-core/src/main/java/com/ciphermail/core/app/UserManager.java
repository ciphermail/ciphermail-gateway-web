/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.util.CloseableIterator;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;

/**
 * manager for users (users are the email addresses of receivers and senders)
 *
 * @author Martijn Brinkers
 *
 */
public interface UserManager
{
    enum AddMode {PERSISTENT, NON_PERSISTENT}

    /**
     * Add a new user. If makePersistent is false the user is not persisted.
     */
    @Nonnull User addUser(@Nonnull String email, AddMode addMode)
    throws AddressException;

    /**
     * Deletes the user. Returns true if user was deleted.
     */
    boolean deleteUser(@Nonnull User user);

    /**
     * Returns the user with given email. Returns null if user does not exist.
     */
    User getUser(@Nonnull String email)
    throws AddressException;

    /**
     * Returns the number of users.
     */
    long getUserCount();

    /**
     * Returns true if the user is a persistent user.
     */
    boolean isPersistent(@Nonnull User user);

    /**
     * Persists the user.
     */
    User makePersistent(@Nonnull User user);

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    @Nonnull CloseableIterator<String> getEmailIterator();

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    @Nonnull CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults, SortDirection sortDirection);

    /**
     * Search for users matching search using LIKE
     */
    @Nonnull CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
            SortDirection sortDirection);

    /**
     * Returns the number of users that will be returned by searchEmail.
     */
    long getSearchEmailCount(@Nonnull String search);
}
