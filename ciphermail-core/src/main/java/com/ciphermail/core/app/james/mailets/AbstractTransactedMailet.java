/*
 * Copyright (c) 2011-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.util.AbstractRetryListener;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import org.apache.commons.lang.UnhandledException;
import org.apache.mailet.Mail;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Objects;

/**
 * Abstract mailet which starts a database transaction when an email is handled.
 */
@SuppressWarnings({"java:S6813"})
public abstract class AbstractTransactedMailet extends AbstractCipherMailMailet
{
    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    private final RetryListener retryListener = new AbstractRetryListener()
    {
        @Override
        public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                Throwable throwable)
        {
            AbstractTransactedMailet.this.retryTransaction();
        }
    };

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener = new LoggingRetryListener(getLogger());

    /**
     * Should be overridden to handle the mail. A database transaction has been started when this
     * method is called.
     */
    protected abstract void serviceMailTransacted(Mail mail)
    throws MessagingException, IOException;

    protected void serviceMailPreTransaction(Mail mail) {
        // Empty on purpose
    }

    protected void serviceMailPostTransaction(Mail mail) {
        // Empty on purpose
    }

    /*
     * Called when the transaction is retried
     */
    protected void retryTransaction() {
        // Empty on purpose
    }

    protected void initMailetTransacted()
    throws MessagingException
    {
        // empty on purpose
    }

    /**
     * Note: method is made final to make sure that extensions of this class do not 'remove' the
     * required initialization. Override {@link #initMailetTransacted()} to add your own
     * initialization code.
     */
    @Override
    protected final void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(transactionOperations);

        initMailetTransacted();
    }

    /**
     * serviceMail is overridden to wrap the serviceMail call in a database transaction.
     *
     * Note: method is made final to make sure that extensions of this class do not 'remove' the
     * transaction mechanism
     */
    @Override
    public final void serviceMail(final Mail mail)
    {
        serviceMailPreTransaction(mail);

        try {
            RetryTemplate retryTemplate = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                    .withListener(loggingRetryListener)
                    .withListener(retryListener)
                    .build();

            retryTemplate.execute(ctx ->
            {
                transactionOperations.executeWithoutResult(status ->
                {
                    try {
                        // Call the super serviceMail method now wrapped in a transaction
                        serviceMailTransacted(mail);
                    }
                    catch (MessagingException | IOException e) {
                        throw new UnhandledException(e);
                    }
                });

                return null;
            });

            serviceMailPostTransaction(mail);
        }
        catch (UnhandledException e) {
            getLogger().error("Error servicing mail.", e);
        }
    }

    protected TransactionOperations getTransactionOperations() {
        return transactionOperations;
    }
}
