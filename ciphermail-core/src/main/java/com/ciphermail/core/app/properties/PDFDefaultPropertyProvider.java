/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DefaultPropertyProvider;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.SecretKeyDeriver;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import java.security.GeneralSecurityException;

/**
 *  DefaultPropertyProvider implementation for PDFProperties
 */
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PDFDefaultPropertyProvider implements DefaultPropertyProvider
{
    /*
     * The default relative URL for the OTP generator
     */
    private static final String DEFAULT_RELATIVE_OTP_URL = "pdf/otp";

    /*
     * The default relative URL for the PDF reply
     */
    private static final String PDF_DEFAULT_RELATIVE_REPLY_URL = "pdf/reply";

    /*
     * Default OTP sub key value
     */
    private static final String DEFAULT_OTP_SUB_KEY = "otp";

    /*
     * Default PDF reply sub key value
     */
    private static final String DEFAULT_PDF_REPLY_SUB_KEY = "reply";

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Autowired
    private SecretKeyDeriver secretKeyDeriver;

    @Override
    public String getName() {
        return PDFDefaultPropertyProvider.class.getName();
    }

    private PortalProperties getPortalProperties(HierarchicalProperties properties)
    {
        return properties instanceof PortalProperties portalProperties ? portalProperties :
                userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class).createInstance(properties);
    }

    private UserProperties getUserProperties(HierarchicalProperties properties)
    {
        return properties instanceof UserProperties userProperties ? userProperties :
                userPropertiesFactoryRegistry.getFactoryForClass(UserPropertiesImpl.class).createInstance(properties);
    }

    private PDFProperties getPDFProperties(HierarchicalProperties properties)
    {
        return properties instanceof PDFProperties pdfProperties? pdfProperties :
                userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class).createInstance(properties);
    }

    private String getDefaultOTPURL(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        // The default URL will be based on the Portal Base URL
        String defaultURL = StringUtils.trimToNull(getPortalProperties(properties).getBaseURL());

        if (defaultURL != null) {
            defaultURL = MiscStringUtils.ensureEndsWith(defaultURL, "/") + DEFAULT_RELATIVE_OTP_URL;
        }

        return defaultURL;
    }

    private String getDefaultPdfReplyURL(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        // The default URL will be based on the Portal Base URL
        String defaultReplyURL = StringUtils.trimToNull(getPortalProperties(properties).getBaseURL());

        if (defaultReplyURL != null) {
            defaultReplyURL = MiscStringUtils.ensureEndsWith(defaultReplyURL, "/") + PDF_DEFAULT_RELATIVE_REPLY_URL;
        }

        return defaultReplyURL;
    }

    private String getDefaultOTPSecretKey(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        String clientSecret = StringUtils.trimToNull(getUserProperties(properties).getClientSecret());

        try {
            return clientSecret != null ? secretKeyDeriver.deriveKey(clientSecret, properties.getCategory(),
                    getPDFProperties(properties).getOTPSubKey()) : null;
        }
        catch (GeneralSecurityException e) {
            throw new HierarchicalPropertiesException(e);
        }
    }

    private String getDefaultOTPSubKey()  {
        return DEFAULT_OTP_SUB_KEY;
    }

    private String getDefaultPdfReplySecretKey(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        String serverSecret = StringUtils.trimToNull(getUserProperties(properties).getServerSecret());

        try {
            return serverSecret != null ? secretKeyDeriver.deriveKey(serverSecret, properties.getCategory(),
                    getPDFProperties(properties).getPdfReplySubKey()) : null;
        }
        catch (GeneralSecurityException e) {
            throw new HierarchicalPropertiesException(e);
        }
    }

    private String getDefaultPdfReplySubKey()  {
        return DEFAULT_PDF_REPLY_SUB_KEY;
    }

    @Override
    public String getDefaultValue(@Nonnull HierarchicalProperties properties, String property)
    throws HierarchicalPropertiesException
    {
        // Note: This is not optimal if we need to support multiple default values. If more than
        // a couple properties need calculated default values, we will probably need to use a set
        // of registered properties or something. For now this will suffice.
        if (PDFPropertiesImpl.OTP_URL.equals(property)) {
            return getDefaultOTPURL(properties);
        }
        else if (PDFPropertiesImpl.PDF_REPLY_URL.equals(property)) {
            return getDefaultPdfReplyURL(properties);
        }
        else if (PDFPropertiesImpl.OTP_SECRET_KEY.equals(property)) {
            return getDefaultOTPSecretKey(properties);
        }
        else if (PDFPropertiesImpl.OTP_SUB_KEY.equals(property)) {
            return getDefaultOTPSubKey();
        }
        else if (PDFPropertiesImpl.PDF_REPLY_SECRET_KEY.equals(property)) {
            return getDefaultPdfReplySecretKey(properties);
        }
        else if (PDFPropertiesImpl.PDF_REPLY_SUB_KEY.equals(property)) {
            return getDefaultPdfReplySubKey();
        }

        return null;
    }
}
