/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAddressUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Collection;
import java.util.Collections;

/**
 * Abstract matcher that matches when the sending user has skip calendar and the message is a calendar message.
 * <p>
 * The matcher condition determines which user property is checked:
 * <p>
 * - skipSMIME: checks the isSMIMESkipCalendar user property
 * - skipSMIMESigning: checks the isSMIMESkipSigningCalendar user property
 *
 * @author Martijn Brinkers
 *
 */
public class SenderIsSMIMESkipCalendar extends AbstractIsSMIMESkipCalendar
{
    private static final Logger logger = LoggerFactory.getLogger(SenderIsSMIMESkipCalendar.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> result = null;

        InternetAddress originator = getMessageOriginatorIdentifier().getOriginator(mail);

        MailAddress mailAddress = new MailAddress(originator);

        Collection<MailAddress> matchingUsers = getMatchingMailAddresses(Collections.singleton(mailAddress));

        if (matchingUsers != null && !matchingUsers.isEmpty()) {
            result = MailAddressUtils.getRecipients(mail);
        }

        return result != null ? result : Collections.emptyList();
    }
}
