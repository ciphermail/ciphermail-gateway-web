/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

/**
 * Matcher that matches all recipients with valid passwords. The passwords will be added to the mail attributes
 * as a map that maps the recipients email address to password and password ID.
 * (@See {@link CoreApplicationMailAttributes#setPasswords(Mail, Passwords)}
 *
 * Usage:
 *
 * HasValidPassword=matchOnError=false | Matches when the recipient has a valid password
 *
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *
 * @author Martijn Brinkers
 *
 */
@SuppressWarnings("java:S6813")
public class HasValidPassword extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(HasValidPassword.class);

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    protected static final String ACTIVATION_CONTEXT_KEY = "HasValidPassword" ;

    /*
     * Keeps track of Passwords of matching users
     */
    protected static class HasValidPasswordActivationContext
    {
        private final Passwords passwords = new Passwords();

        public Passwords getPasswords() {
            return passwords;
        }

        public void clear() {
            passwords.clear();
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);
    }

    protected Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses,
            HasValidPasswordActivationContext context)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = HasValidPassword.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses, new AbstractRetryListener()
        {
            @Override
            public <T, E extends Throwable> void onError(RetryContext retryContext,
                    RetryCallback<T, E> retryCallback,
                    Throwable throwable)
            {
                context.clear();
            }
        });
    }

    /*
     * Called for each recipient to check whether the recipient has a valid password.
     */
    @VisibleForTesting
    protected boolean hasMatch(@Nonnull User user)
    {
        PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                .createInstance(user.getUserPreferences().getProperties());

        boolean match = false;

        try {
            String password = pdfProperties.getPassword();

            if (password != null)
            {
                PasswordContainer passwordContainer = new PasswordContainer();

                passwordContainer.setPassword(password);

                HasValidPasswordActivationContext hasValidPasswordContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                        HasValidPasswordActivationContext.class);

                hasValidPasswordContext.getPasswords().put(user.getEmail(), passwordContainer);

                match = true;
            }
        }
        catch (HierarchicalPropertiesException e) {
            getLogger().error("Unable to retrieve the password.", e);
        }

        return match;
    }

    private void addPasswordsAttribute(Mail mail, Passwords newPasswords)
    {
        // Check if there are already passwords set. If so, use them in favor of the passwords read from the user
        // properties
        Passwords existingPasswords = CoreApplicationMailAttributes.getPasswords(mail);

        if (existingPasswords != null)
        {
            getLogger().debug("Existing passwords found. Merge with new passwords.");

            newPasswords.putAll(existingPasswords);
        }

        CoreApplicationMailAttributes.setPasswords(mail, newPasswords);
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        HasValidPasswordActivationContext context = new HasValidPasswordActivationContext();

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);

        Collection<MailAddress> matchingUsers = getMatchingMailAddresses(MailAddressUtils.getRecipients(mail), context);

        addPasswordsAttribute(mail, context.getPasswords());

        return matchingUsers;
    }
}
