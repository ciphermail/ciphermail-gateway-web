/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesCategory;
import com.ciphermail.core.app.UserPreferencesCategoryManager;
import com.ciphermail.core.app.UserPreferencesManager;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

/**
 * Manages the global preferences. Global preferences inherit the default values from the factory preferences. The
 * factory preferences properties are initialized at startup.
 *
 * You should normally just create one instance of DefaultGlobalPreferencesManager.
 *
 * @author Martijn Brinkers
 *
 */
public class DefaultGlobalPreferencesManager implements GlobalPreferencesManager
{
    private static final Logger logger = LoggerFactory.getLogger(DefaultGlobalPreferencesManager.class);

    private static final String GLOBAL_PREFERENCES_NAME  = "preferences";
    private static final String FACTORY_PREFERENCES_NAME = "factory";

    private final UserPreferencesManager globalPreferencesManager;
    private final HierarchicalProperties factoryProperties;

    private boolean factoryPropertiesSynced = false;

    /*
     * properties started with "protected." are considered protected properties that are not placed in
     * the database. The consequence of this is that with some HierarchicalProperties implementations
     * these protected properties are not inherited. The system password is one example of a property
     * you do not want to be placed in the database.
     */
    private final Pattern protectedPropertiesPattern =  Pattern.compile("(?i)^protected\\..*");

    /**
     * Creates the global preferences and factory preferences if they not not yet exist. The properties
     * of the factory preferences are replaced by the provided factoryProperties the first time this
     * class is instantiated.
     */
    public DefaultGlobalPreferencesManager(@Nonnull UserPreferencesCategoryManager userPreferencesCategoryManager,
            HierarchicalProperties factoryProperties)
    {
        this.globalPreferencesManager = userPreferencesCategoryManager.getUserPreferencesManager(
                UserPreferencesCategory.GLOBAL.name());

        this.factoryProperties = factoryProperties;
    }

    @Override
    public void loadFactoryProperties(HierarchicalProperties factoryProperties)
    throws HierarchicalPropertiesException
    {
        syncFactoryProperties(factoryProperties);
    }

    @Override
    public UserPreferences getGlobalUserPreferences()
    throws HierarchicalPropertiesException
    {
        UserPreferences globalPreferences = globalPreferencesManager.getUserPreferences(GLOBAL_PREFERENCES_NAME);

        if (globalPreferences == null)
        {
            globalPreferences = globalPreferencesManager.addUserPreferences(GLOBAL_PREFERENCES_NAME);

            syncFactoryProperties();

            // the global preferences will inherit the factory preferences
            globalPreferences.getInheritedUserPreferences().add(getFactoryPreferences());
        }

        return globalPreferences;
    }

    private UserPreferences getFactoryPreferences() {
        return globalPreferencesManager.getUserPreferences(FACTORY_PREFERENCES_NAME);
    }

    /*
     * Copies the properties of factoryProperties to the properties of factory UserPreferences. The factoryProperties
     * is normally read from a property file at startup.
     */
    @Override
    public synchronized void syncFactoryProperties()
    throws HierarchicalPropertiesException
    {
        // At can happen that getFactoryPreferences() returns null. This happens for example during unit testing
        // where tables are deleted but factoryPropertiesSynced remains true. We will check for this explicitly.
        if (!factoryPropertiesSynced || getFactoryPreferences() == null)
        {
            syncFactoryProperties(factoryProperties);

            factoryPropertiesSynced = true;
        }
    }

    /*
     * Copies the properties of factoryProperties to the properties of factory UserPreferences. The factoryProperties
     * is normally read from a property file at startup.
     */
    private void syncFactoryProperties(HierarchicalProperties factoryProperties)
    throws HierarchicalPropertiesException
    {
        logger.info("Synchronizing factory preferences.");

        UserPreferences factoryPreferences = getFactoryPreferences();

        if (factoryPreferences == null)
        {
            logger.info("Factory preferences not found. Creating factory preferences.");

            factoryPreferences = globalPreferencesManager.addUserPreferences(FACTORY_PREFERENCES_NAME);
        }

        factoryPreferences.getProperties().deleteAll();

        HierarchicalPropertiesUtils.copyProperties(factoryProperties, factoryPreferences.getProperties(),
                protectedPropertiesPattern);
    }
}
