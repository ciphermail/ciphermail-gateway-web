/*
 * Copyright (c) 2012-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.properties.SecurityInfoProperties;
import com.ciphermail.core.app.properties.SecurityInfoPropertiesImpl;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Objects;

/**
 * Mailet that adds a SecurityInfoTags instance to the Mail attributes with values from the sender's properties.
 */
@SuppressWarnings("java:S6813")
public class AddSecurityInfoTags extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(AddSecurityInfoTags.class);

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailetTransacted()
    throws MessagingException
    {
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
    }

    private SecurityInfoProperties createSecurityInfoProperties(@Nonnull HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SecurityInfoPropertiesImpl.class)
                .createInstance(properties);
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        try {
            SecurityInfoProperties properties = createSecurityInfoProperties(userWorkflow.getUser(
                    originator.getAddress(), UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST)
                    .getUserPreferences().getProperties());

            CoreApplicationMailAttributes.setSecurityInfoTags(mail, new SecurityInfoTags(
                    properties.getSecurityInfoDecryptedTag(),
                    properties.getSecurityInfoSignedValidTag(),
                    properties.getSecurityInfoSignedByValidTag(),
                    properties.getSecurityInfoSignedInvalidTag(),
                    properties.getSecurityInfoMixedContentTag()));
        }
        catch (HierarchicalPropertiesException e) {
            throw new MessagingException("Error adding S/MIME info to attributes", e);
        }
    }
}
