/*
 * Copyright (c) 2013-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.SecurityInfoTags;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.notification.NotificationService;
import com.ciphermail.core.common.notification.NotificationSeverity;
import com.ciphermail.core.common.notification.StandardNotificationFacilities;
import com.ciphermail.core.common.security.openpgp.PGPContentSignatureValidator;
import com.ciphermail.core.common.security.openpgp.PGPKeyPairProvider;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingImporter;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyRingExtractor;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyRingListener;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyRingNotificationMessage;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyRingWrapper;
import com.ciphermail.core.common.security.openpgp.PGPRecursiveValidatingMIMEHandler;
import com.ciphermail.core.common.util.SizeUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Mailet which handles PGP encrypted and/or signed email
 */
@SuppressWarnings({"java:S6813"})
public class PGPHandler extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPHandler.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The key under which the extracted PGP keyrings will be stored
     */
    private static final String EXTRACTED_KEYS_CONTEXT_KEY = "extractedKeys";

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        INLINE_PGP_ENABLED                ("inlinePGPEnabled"),
        INLINE_PGP_ENABLED_ATTRIBUTE      ("inlinePGPEnabledAttribute"),
        SCAN_HTML_FOR_PGP                 ("scanHTMLForPGP"),
        SCAN_HTML_FOR_PGP_ATTRIBUTE       ("scanHTMLForPGPAttribute"),
        SKIP_NON_PGP_EXTENSIONS           ("skipNonPGPExtensions"),
        SKIP_NON_PGP_EXTENSIONS_ATTRIBUTE ("skipNonPGPExtensionsAttribute"),
        IMPORT_KEYS                       ("importKeys"),
        IMPORT_KEYS_ATTRIBUTE             ("importKeysAttribute"),
        REMOVE_KEYS                       ("removeKeys"),
        REMOVE_KEYS_ATTRIBUTE             ("removeKeysAttribute"),
        KEY_REPLACEMENT_TEXT              ("keyReplacementText"),
        DECRYPT                           ("decrypt"),
        DECRYPT_ATTRIBUTE                 ("decryptAttribute"),
        REMOVE_SIGNATURE                  ("removeSignature"),
        REMOVE_SIGNATURE_ATTRIBUTE        ("removeSignatureAttribute"),
        SEND_IMPORT_NOTIFICATION          ("sendImportNotification"),
        MAX_KEY_SIZE                      ("maxKeySize"),
        RETAIN_MESSAGE_ID                 ("retainMessageID"),
        ENABLE_PGP_UNIVERSAL_WORKAROUND   ("enablePGPUniversalWorkaround"),
        SUBJECT_TEMPLATE                  ("subjectTemplate"),
        MAX_MIME_DEPTH                    ("maxMimeDepth"),
        DECOMPRESSION_UPPERLIMIT          ("decompressionUpperlimit"),
        MAX_OBJECTS                       ("maxObjects"),
        HANDLED_PROCESSOR                 ("handledProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    static class PGPPublicKeyRingListenerImpl implements PGPPublicKeyRingListener
    {
        /*
         * The keys found in the email
         */
        final Set<PGPPublicKeyRingWrapper> keys = new HashSet<>();

        @Override
        public void onPGPPublicKeyRing(PGPPublicKeyRing keyRing)
        {
            if (keyRing != null)
            {
                try {
                    keys.add(new PGPPublicKeyRingWrapper(keyRing));
                }
                catch (Exception e) {
                    logger.error("Error adding PGPPublicKeyRing", e);
                }
            }
        }
    }

    /*
     * If true, PGP inline is supported (unless it is overruled by the inlinePGPEnabledAttribute)
     */
    private boolean defaultInlinePGPEnabled;

    /*
     * If set, inlinePGPEnabled will be read from the Mail attribute
     */
    private String inlinePGPEnabledAttribute;

    /*
     * If true, HTML parts are scanned for PGP by converting the HTML to text before scanning for PGP
     * (unless it is overruled by the scanHTMLForPGPAttribute)
     */
    private boolean defaultScanHTMLForPGP = true;

    /*
     * If set, scanHTMLForPGP will be read from the Mail attribute
     */
    private String scanHTMLForPGPAttribute;

    /*
     * If true, binary attachments, i.e., other than text/plain or text/html, that do not have a PGP type of extension
     * (.pgp, .asc etc.) will be skipped.
     */
    private boolean defaultSkipNonPGPExtensions = true;

    /*
     * If set, skipNonPGPExtensions will be read from the Mail attribute
     */
    private String skipNonPGPExtensionsAttribute;

    /*
     * If true, attached PGP keys will be extracted and imported (unless it is overruled by the extractKeysAttribute)
     */
    private boolean defaultImportKeys;

    /*
     * If set, extractKeys will be read from the Mail attribute
     */
    private String importKeysAttribute;

    /*
     * If true, attached PGP keys will be removed (unless it is overruled by the removeKeysAttribute)
     */
    private boolean defaultRemoveKeys;

    /*
     * If set, removeKeys will be read from the Mail attribute
     */
    private String removeKeysAttribute;

    /*
     * If true (the default), the PGP blob will be decrypted if it is encrypted and a suitable key is available
     */
    private boolean defaultDecrypt = true;

    /*
     * If set, decrypt will be read from the Mail attribute
     */
    private String decryptAttribute;

    /*
     * If true the clear signed signature will be removed from a PGP/MIME clear signed message
     */
    private boolean defaultRemoveSignature = true;

    /*
     * If set, removeSignature will be read from the Mail attribute
     */
    private String removeSignatureAttribute;

    /*
     * If keys are removed, the key is replaced with this text
     */
    private String keyReplacementText;

    /*
     * If true the original Message-ID will be used for the handled message
     */
    private boolean retainMessageID = true;

    /*
     * If true, x-pgp-encoding-format header will be used (if available) to detect whether a message is PGP/MIME
     */
    private boolean enablePGPUniversalWorkaround = true;

    /*
     * If true and a new key is imported, a sendImportNotification will be sent
     */
    private boolean sendImportNotification = true;

    /*
     * Maximum size of the key that will be extracted.
     */
    private int maxKeySize = SizeUtils.KB * 64;

    /*
     * The subject template. The first parameter is replaced with the current subject and the second
     * parameter is replaced with the encryption or signing tag.
     *
     *  Example subject:
     *
     *  %1$s %2$s
     */
    private String subjectTemplate = "%1$s %2$s";

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * The maximum number of bytes read for decompression. This is done to protect against "ZIP bombs".
     * The default value will be set to 10% of Runtime.getRuntime().maxMemory()
     */
    private int decompressionUpperlimit = (int) (Runtime.getRuntime().maxMemory() * 0.1);

    /*
     * The maximum number of PGP objects. If the number of PGP objects read exceeds the maximum, a PGP
     * exception will be thrown.
     */
    private int maxObjects = 64;

    /*
     * The next processor for messages that were handled. If null, the current processor is used
     */
    private String handledProcessor;

    /*
     * Lookup for KeyPair's based on a PGP keyID
     */
    @Inject
    private PGPKeyPairProvider keyPairProvider;

    /*
     * For validating the PGP signature
     */
    @Inject
    private PGPContentSignatureValidator signatureValidator;

    /*
     * For importing extracted keys
     */
    @Inject
    private PGPKeyRingImporter keyRingImporter;

    /*
     * For sending PGP key import notifications
     */
    @Inject
    private NotificationService notificationService;

    @Override
    protected void initMailetTransacted()
    {
        Objects.requireNonNull(keyPairProvider);
        Objects.requireNonNull(signatureValidator);
        Objects.requireNonNull(keyRingImporter);
        Objects.requireNonNull(notificationService);

        defaultInlinePGPEnabled = getBooleanInitParameter(Parameter.INLINE_PGP_ENABLED.name, defaultInlinePGPEnabled);
        inlinePGPEnabledAttribute = getInitParameter(Parameter.INLINE_PGP_ENABLED_ATTRIBUTE.name,
                inlinePGPEnabledAttribute);

        defaultScanHTMLForPGP = getBooleanInitParameter(Parameter.SCAN_HTML_FOR_PGP.name, defaultScanHTMLForPGP);
        scanHTMLForPGPAttribute = getInitParameter(Parameter.SCAN_HTML_FOR_PGP_ATTRIBUTE.name,
                scanHTMLForPGPAttribute);

        defaultSkipNonPGPExtensions = getBooleanInitParameter(Parameter.SKIP_NON_PGP_EXTENSIONS.name,
                defaultSkipNonPGPExtensions);
        skipNonPGPExtensionsAttribute = getInitParameter(Parameter.SKIP_NON_PGP_EXTENSIONS_ATTRIBUTE.name,
                skipNonPGPExtensionsAttribute);

        defaultImportKeys = getBooleanInitParameter(Parameter.IMPORT_KEYS.name, defaultImportKeys);
        importKeysAttribute = getInitParameter(Parameter.IMPORT_KEYS_ATTRIBUTE.name, importKeysAttribute);

        defaultRemoveKeys = getBooleanInitParameter(Parameter.REMOVE_KEYS.name, defaultRemoveKeys);
        removeKeysAttribute = getInitParameter(Parameter.REMOVE_KEYS_ATTRIBUTE.name, removeKeysAttribute);

        keyReplacementText = getInitParameter(Parameter.KEY_REPLACEMENT_TEXT.name);

        defaultDecrypt = getBooleanInitParameter(Parameter.DECRYPT.name, defaultDecrypt);
        decryptAttribute = getInitParameter(Parameter.DECRYPT_ATTRIBUTE.name);

        defaultRemoveSignature = getBooleanInitParameter(Parameter.REMOVE_SIGNATURE.name, defaultRemoveSignature);
        removeSignatureAttribute = getInitParameter(Parameter.REMOVE_SIGNATURE_ATTRIBUTE.name);

        sendImportNotification = getBooleanInitParameter(Parameter.SEND_IMPORT_NOTIFICATION.name,
                sendImportNotification);

        maxKeySize = getIntegerInitParameter(Parameter.MAX_KEY_SIZE.name, maxKeySize);

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);

        enablePGPUniversalWorkaround = getBooleanInitParameter(Parameter.ENABLE_PGP_UNIVERSAL_WORKAROUND.name,
                enablePGPUniversalWorkaround);

        subjectTemplate = getInitParameter(Parameter.SUBJECT_TEMPLATE.name, subjectTemplate);

        maxMimeDepth = getIntegerInitParameter(Parameter.MAX_MIME_DEPTH.name, maxMimeDepth);

        decompressionUpperlimit = getIntegerInitParameter(Parameter.DECOMPRESSION_UPPERLIMIT.name,
                decompressionUpperlimit);

        maxObjects = getIntegerInitParameter(Parameter.MAX_OBJECTS.name, maxObjects);

        handledProcessor = getInitParameter(Parameter.HANDLED_PROCESSOR.name, handledProcessor);

        StrBuilder sb = new StrBuilder();

        sb.append("inlinePGPEnabled: ").append(defaultInlinePGPEnabled);
        sb.appendSeparator("; ");
        sb.append("inlinePGPEnabledAttribute: ").append(inlinePGPEnabledAttribute);
        sb.appendSeparator("; ");
        sb.append("scanHTMLForPGP: ").append(defaultScanHTMLForPGP);
        sb.appendSeparator("; ");
        sb.append("scanHTMLForPGPAttribute: ").append(scanHTMLForPGPAttribute);
        sb.appendSeparator("; ");
        sb.append("skipNonPGPExtensions: ").append(defaultSkipNonPGPExtensions);
        sb.appendSeparator("; ");
        sb.append("skipNonPGPExtensionsAttribute: ").append(skipNonPGPExtensionsAttribute);
        sb.appendSeparator("; ");
        sb.append("importKeys: ").append(defaultImportKeys);
        sb.appendSeparator("; ");
        sb.append("importKeysAttribute: ").append(importKeysAttribute);
        sb.appendSeparator("; ");
        sb.append("removeKeys: ").append(defaultRemoveKeys);
        sb.appendSeparator("; ");
        sb.append("removeKeysAttribute: ").append(removeKeysAttribute);
        sb.appendSeparator("; ");
        sb.append("keyReplacementText: ").append(keyReplacementText);
        sb.appendSeparator("; ");
        sb.append("decrypt: ").append(defaultDecrypt);
        sb.appendSeparator("; ");
        sb.append("decryptAttribute: ").append(decryptAttribute);
        sb.appendSeparator("; ");
        sb.append("removeSignature: ").append(defaultRemoveSignature);
        sb.appendSeparator("; ");
        sb.append("removeSignatureAttribute: ").append(removeSignatureAttribute);
        sb.appendSeparator("; ");
        sb.append("sendImportNotification: ").append(sendImportNotification);
        sb.appendSeparator("; ");
        sb.append("maxKeySize: ").append(maxKeySize);
        sb.appendSeparator("; ");
        sb.append("retainMessageID: ").append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("enablePGPUniversalWorkaround: ").append(enablePGPUniversalWorkaround);
        sb.appendSeparator("; ");
        sb.append("subjectTemplate: ").append(subjectTemplate);
        sb.appendSeparator("; ");
        sb.append("maxMimeDepth: ").append(maxMimeDepth);
        sb.appendSeparator("; ");
        sb.append("decompressionUpperlimit: ").append(decompressionUpperlimit);
        sb.appendSeparator("; ");
        sb.append("maxObjects: ").append(maxObjects);
        sb.appendSeparator("; ");
        sb.append("handledProcessor: ").append(handledProcessor);

        getLogger().info("{}", sb);
    }

    private boolean extractKeys(MimeMessage message, boolean removeKeys, PGPPublicKeyRingListenerImpl keyRingListener)
    throws MessagingException
    {
        PGPPublicKeyRingExtractor extractor = new PGPPublicKeyRingExtractor(keyRingListener);

        extractor.setMaxKeySize(maxKeySize);
        extractor.setRemoveKeys(removeKeys);

        if (StringUtils.isNotEmpty(keyReplacementText)) {
            extractor.setReplacementText(keyReplacementText);
        }

        return extractor.extract(message);
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException
    {
        final MimeMessage originalMessage = mail.getMessage();

        PGPRecursiveValidatingMIMEHandler handler = new PGPRecursiveValidatingMIMEHandler(keyPairProvider,
                signatureValidator);

        handler.setMaxMimeDepth(maxMimeDepth);
        handler.setDecompressionUpperlimit(decompressionUpperlimit);
        handler.setMaxObjects(maxObjects);

        // read certain properties from the Mail object. If not set, use the default value
        handler.setInlinePGPEnabled(MailAttributesUtils.attributeAsBoolean(mail, inlinePGPEnabledAttribute,
                defaultInlinePGPEnabled));

        handler.setScanHTMLForPGP(MailAttributesUtils.attributeAsBoolean(mail, scanHTMLForPGPAttribute,
                defaultScanHTMLForPGP));

        handler.setSkipNonPGPExtensions(MailAttributesUtils.attributeAsBoolean(mail, skipNonPGPExtensionsAttribute,
                defaultSkipNonPGPExtensions));

        handler.setDecrypt(MailAttributesUtils.attributeAsBoolean(mail, decryptAttribute,
                defaultDecrypt));

        handler.setRemoveSignature(MailAttributesUtils.attributeAsBoolean(mail, removeSignatureAttribute,
                defaultRemoveSignature));

        handler.setRetainMessageID(retainMessageID);
        handler.setEnablePGPUniversalWorkaround(enablePGPUniversalWorkaround);
        handler.setSubjectTemplate(subjectTemplate);

        // Set the MailID for logging purposes
        handler.setMailID(CoreApplicationMailAttributes.getMailID(mail));

        PGPPublicKeyRingListenerImpl keyRingListener = new PGPPublicKeyRingListenerImpl();

        handler.setPublicKeyRingListener(keyRingListener);

        // Check whether there are security subject settings added to the Mail attributes
        SecurityInfoTags securityInfoTags = CoreApplicationMailAttributes.getSecurityInfoTags(mail);

        if (securityInfoTags != null)
        {
            // If there is a SecurityInfoTags added to the Mail, it means we need to add the info
            handler.setAddSecurityInfoToSubject(true);

            handler.setDecryptedTag(securityInfoTags.getDecryptedTag());
            handler.setSignedValidTag(securityInfoTags.getSignedValidTag());
            handler.setSignedByValidTag(securityInfoTags.getSignedByValidTag());
            handler.setSignedInvalidTag(securityInfoTags.getSignedInvalidTag());
            handler.setMixedContentTag(securityInfoTags.getMixedContentTag());
        }

        MimeMessage newMessage;

        try {
            newMessage = handler.handleMessage(mail.getMessage());
        }
        catch (IOException e) {
            throw new MessagingException("Error handling message", e);
        }

        // read certain properties from the Mail object. If not set, use the default value
        boolean importKeys = MailAttributesUtils.attributeAsBoolean(mail, importKeysAttribute, defaultImportKeys);
        boolean removeKeys = MailAttributesUtils.attributeAsBoolean(mail, removeKeysAttribute, defaultRemoveKeys);

        if (newMessage != null)
        {
            getLogger().debug("PGP message was handled");

            if (handler.isDecrypted())
            {
                getLogger().info("Message has been PGP decrypted; MailID: {}; Recipients: {}",
                        CoreApplicationMailAttributes.getMailID(mail), mail.getRecipients());
            }

            if (importKeys || removeKeys) {
                extractKeys(newMessage, removeKeys, keyRingListener);
            }

            mail.setMessage(newMessage);

            if (StringUtils.isNotEmpty(handledProcessor)) {
                mail.setState(handledProcessor);
            }
        }
        else if (importKeys || removeKeys)
        {
            // Message was not a PGP message. However, there might be attached PGP keys
            //
            // Note: could also be that the message could not be or was not decrypted
            MimeMessage message = mail.getMessage();

            if (removeKeys && originalMessage == message) {
                // Since we will extract keys and remove them (if found), we need to create a copy of the original
                // message first since this has not been done yet. The reason for this is that extract keys will
                // change the message if removeKeys is true. If there is an error during handling of the email,
                // we want to make sure that the original message stays intact.
                message = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                        MailUtils.cloneMessage(message);
            }

            boolean extracted = extractKeys(message, removeKeys, keyRingListener);

            if (extracted && removeKeys)
            {
                getLogger().info("PGP keys were removed from the message");

                // The message was changed because a key was removed. We therefore need to set the new message
                mail.setMessage(message);

                if (StringUtils.isNotEmpty(handledProcessor)) {
                    mail.setState(handledProcessor);
                }
            }
        }

        if (importKeys) {
            // Store the keys in the context so we can use them in serviceMailPostTransaction
            getActivationContext().set(EXTRACTED_KEYS_CONTEXT_KEY, keyRingListener.keys);
        }

        // Check if some part of the message could not be decrypted because a decryption key was not found
        if (handler.isDecryptionKeyNotFound())
        {
            MimeMessage message = mail.getMessage();

            if (originalMessage == message)
            {
                // We need to clone before the original message before changing it to make sure that the
                // message is a valid message
                message = retainMessageID ? MailUtils.cloneMessageWithFixedMessageID(message) :
                    MailUtils.cloneMessage(message);
            }

            message.setHeader(PGPRecursiveValidatingMIMEHandler.PGP_DECRYPTION_KEY_NOT_FOUND, "True");

            mail.setMessage(message);
        }
    }

    @Override
    protected void serviceMailPostTransaction(Mail mail)
    {
        // Check if there were keys extracted
        @SuppressWarnings("unchecked")
        final Set<PGPPublicKeyRingWrapper> keys = getActivationContext().get(EXTRACTED_KEYS_CONTEXT_KEY, Set.class);

        final Collection<PGPPublicKeyRing> importedKeyRings = new LinkedList<>();

        if (CollectionUtils.isNotEmpty(keys))
        {
            try {
                // Importing keys requires a database transaction
                getTransactionOperations().executeWithoutResult(status ->
                {
                    try {
                        for (PGPPublicKeyRingWrapper key : keys)
                        {
                            PGPPublicKeyRing publicKeyRing = key.getPublicKeyRing();

                            List<PGPKeyRingEntry> importedKeyEntries = keyRingImporter.importKeyRing(
                                    new ByteArrayInputStream(publicKeyRing.getEncoded()), null);

                            if (CollectionUtils.isNotEmpty(importedKeyEntries)) {
                                importedKeyRings.add(publicKeyRing);
                            }
                        }
                    }
                    catch (IOException | PGPException e) {
                        throw new UnhandledException(e);
                    }
                });
            }
            catch(UnhandledException e) {
                getLogger().error("Error importing keys", e);
            }

            if (!importedKeyRings.isEmpty())
            {
                getLogger().info("New PGP keys have been imported");

                if (sendImportNotification)
                {
                    try {
                        notificationService.sendNotification(StandardNotificationFacilities.PGP_KEY_IMPORT,
                                NotificationSeverity.INFORMATIONAL,
                                new PGPPublicKeyRingNotificationMessage(importedKeyRings));
                    }
                    catch (Exception e) {
                        getLogger().error("Error sending notification", e);
                    }
                }
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (handledProcessor != null) {
            requiredProcessors.add(new ProcessingState(handledProcessor));
        }

        return requiredProcessors;
    }
}
