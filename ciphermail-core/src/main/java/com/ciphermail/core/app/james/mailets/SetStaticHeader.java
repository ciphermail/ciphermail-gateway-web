/*
 * Copyright (c) 2014-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.mail.MailUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Mailet that adds a header to the message
 */
public class SetStaticHeader extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SetStaticHeader.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * Name of the header to set
     */
    private String name;

    /*
     * value of the header to set
     */
    private String value;

    /*
     * The mail attribute to read the header/name value from. The attribute value should be name: value
     */
    private String attribute;

    /*
     * If set and an error occurs changing the subject, the name of next processor
     */
    private String errorProcessor;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        NAME            ("name"),
        VALUE           ("value"),
        ATTRIBUTE       ("attribute"),
        ERROR_PROCESSOR ("errorProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    public final void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        name = StringUtils.trimToNull(getInitParameter(Parameter.NAME.name));

        value = StringUtils.defaultString(getInitParameter(Parameter.VALUE.name));

        attribute = StringUtils.defaultString(getInitParameter(Parameter.ATTRIBUTE.name));

        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
    }

    private void setHeader(Mail mail, MimeMessage message)
    throws MessagingException
    {
        String headerName = null;
        String headerValue = null;

        if (attribute != null)
        {
            String attributeValue = StringUtils.trimToNull(MailAttributesUtils.attributeAsString(mail, attribute));

            if (attributeValue != null)
            {
                String[] headerNameValue = StringUtils.split(attributeValue, ":", 2);

                if (headerNameValue.length > 0) {
                    headerName = headerNameValue[0];
                }

                if (headerNameValue.length > 1) {
                    headerValue = headerNameValue[1];
                }
            }
        }

        if (headerName == null)
        {
            headerName = name;
            headerValue = value;
        }

        headerName = StringUtils.trimToNull(headerName);
        headerValue = StringUtils.trimToEmpty(headerValue);

        if (headerName != null)
        {
            try {
                headerName = MimeUtility.encodeText(headerName);
            }
            catch (UnsupportedEncodingException e) {
                logger.error("Error encoding header name", e);
            }
        }

        try {
            headerValue = MimeUtility.encodeText(headerValue);
        }
        catch (UnsupportedEncodingException e) {
            logger.error("Error encoding header value", e);
        }

        if (headerName != null) {
            message.setHeader(headerName, headerValue);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final MimeMessage sourceMessage = mail.getMessage();

            try {
                MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(sourceMessage);

                setHeader(mail, clone);

                clone.saveChanges();

                MailUtils.validateMessage(clone);

                mail.setMessage(clone);
            }
            catch (Exception e)
            {
                logger.warn("Message with message-id {} and MailID {} is not a valid MIME message. " +
                        "The header can therefore not be set.",
                        MailUtils.getMessageIDQuietly(sourceMessage),
                        CoreApplicationMailAttributes.getMailID(mail));

                if (StringUtils.isNotEmpty(errorProcessor)) {
                    mail.setState(errorProcessor);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("Error setting header", e);

            if (StringUtils.isNotEmpty(errorProcessor)) {
                mail.setState(errorProcessor);
            }
        }
    }
    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (errorProcessor != null) {
            requiredProcessors.add(new ProcessingState(errorProcessor));
        }

        return requiredProcessors;
    }
}
