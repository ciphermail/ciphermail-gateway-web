/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;

import javax.annotation.Nonnull;

/**
 * TriggerProperties implementation
 */
@UserPropertiesType(name = "trigger", displayName = "Trigger", order = 70)
public class TriggerPropertiesImpl extends DelegatedHierarchicalProperties implements TriggerProperties
{
    private static final String SIGN_HEADER_TRIGGER                         = "trigger-sign-header";
    private static final String SIGN_HEADER_TRIGGER_ENABLED                 = "trigger-sign-header-enabled";
    private static final String ENCRYPT_HEADER_TRIGGER                      = "trigger-encrypt-header";
    private static final String ENCRYPT_HEADER_TRIGGER_ENABLED              = "trigger-encrypt-header-enabled";
    private static final String SUBJECT_TRIGGER_REGEX                       = "trigger-encrypt-subject-regex";
    private static final String SUBJECT_TRIGGER_REGEX_ENABLED               = "trigger-encrypt-subject-regex-enabled";
    private static final String SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN        = "trigger-encrypt-subject-regex-remove-pattern";
    private static final String SIGN_SUBJECT_TRIGGER_REGEX                  = "trigger-sign-subject-regex";
    private static final String SIGN_SUBJECT_TRIGGER_REGEX_ENABLED          = "trigger-sign-subject-regex-enabled";
    private static final String SIGN_SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN   = "trigger-sign-subject-regex-remove-pattern";

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public TriggerPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(name = SIGN_HEADER_TRIGGER)
    public void setSignHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException
    {
        setProperty(SIGN_HEADER_TRIGGER, UserPropertiesValidators.validateHeaderTrigger(
                SIGN_HEADER_TRIGGER, trigger));
    }

    @Override
    @UserProperty(name = SIGN_HEADER_TRIGGER,
            order = 10,
            allowNull = true
    )
    public String getSignHeaderTrigger()
    throws HierarchicalPropertiesException
    {
        return getProperty(SIGN_HEADER_TRIGGER);
    }

    @Override
    @UserProperty(name = SIGN_HEADER_TRIGGER_ENABLED)
    public void setSignHeaderTriggerEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SIGN_HEADER_TRIGGER_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SIGN_HEADER_TRIGGER_ENABLED,
            order = 20
    )
    public @Nonnull Boolean getSignHeaderTriggerEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SIGN_HEADER_TRIGGER_ENABLED);
    }

    @Override
    @UserProperty(name = ENCRYPT_HEADER_TRIGGER)
    public void setEncryptHeaderTrigger(String trigger)
    throws HierarchicalPropertiesException
    {
        setProperty(ENCRYPT_HEADER_TRIGGER, UserPropertiesValidators.validateHeaderTrigger(
                ENCRYPT_HEADER_TRIGGER, trigger));
    }

    @Override
    @UserProperty(name = ENCRYPT_HEADER_TRIGGER,
            order = 30,
            allowNull = true
    )
    public String getEncryptHeaderTrigger()
    throws HierarchicalPropertiesException
    {
        return getProperty(ENCRYPT_HEADER_TRIGGER);
    }

    /**
     * If true a header can trigger encryption of a message
     */
    @Override
    @UserProperty(name = ENCRYPT_HEADER_TRIGGER_ENABLED)
    public void setEncryptHeaderTriggerEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, ENCRYPT_HEADER_TRIGGER_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = ENCRYPT_HEADER_TRIGGER_ENABLED,
            order = 40
    )
    public @Nonnull Boolean getEncryptHeaderTriggerEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, ENCRYPT_HEADER_TRIGGER_ENABLED);
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX)
    public void setSubjectTriggerRegEx(String subjectTriggerRegEx)
    throws HierarchicalPropertiesException
    {
        setProperty(SUBJECT_TRIGGER_REGEX, UserPropertiesValidators.validateRegEx(SUBJECT_TRIGGER_REGEX, subjectTriggerRegEx));
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX,
            order = 50,
            allowNull = true
    )
    public String getSubjectTriggerRegEx()
    throws HierarchicalPropertiesException
    {
        return getProperty(SUBJECT_TRIGGER_REGEX);
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX_ENABLED)
    public void setSubjectTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SUBJECT_TRIGGER_REGEX_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX_ENABLED,
            order = 60
    )
    public @Nonnull Boolean getSubjectTriggerRegExEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SUBJECT_TRIGGER_REGEX_ENABLED);
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN)
    public void setSubjectTriggerRegExRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN, removePattern);
    }

    @Override
    @UserProperty(name = SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN,
            order = 80
    )
    public @Nonnull Boolean getSubjectTriggerRegExRemovePattern()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN);
    }

    @Override
    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX)
    public void setSignSubjectTriggerRegEx(String signSubjectTriggerRegEx)
    throws HierarchicalPropertiesException
    {
        setProperty(SIGN_SUBJECT_TRIGGER_REGEX, UserPropertiesValidators.validateRegEx(SIGN_SUBJECT_TRIGGER_REGEX,
                signSubjectTriggerRegEx));
    }

    @Override
    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX,
            order = 90,
            allowNull = true
    )
    public String getSignSubjectTriggerRegEx()
    throws HierarchicalPropertiesException
    {
        return getProperty(SIGN_SUBJECT_TRIGGER_REGEX);
    }

    @Override
    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX_ENABLED)
    public void setSignSubjectTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SIGN_SUBJECT_TRIGGER_REGEX_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX_ENABLED,
            order = 100
    )
    public @Nonnull Boolean getSignSubjectTriggerRegExEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SIGN_SUBJECT_TRIGGER_REGEX_ENABLED);
    }

    @Override
    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN)
    public void setSignSubjectTriggerRegExRemovePattern(Boolean removePattern)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SIGN_SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN, removePattern);
    }

    @UserProperty(name = SIGN_SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN,
            order = 120
    )
    @Override
    public @Nonnull Boolean getSignSubjectTriggerRegExRemovePattern()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SIGN_SUBJECT_TRIGGER_REGEX_REMOVE_PATTERN);
    }
}
