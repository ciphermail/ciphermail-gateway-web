/*
 * Copyright (c) 2016-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.common.dkim.DKIMMessageHeaders;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.mail.SkipHeadersOutputStream;
import com.ciphermail.core.common.util.CRLFOutputStream;
import com.ciphermail.core.common.util.Context;
import com.ciphermail.core.common.util.ContextImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.james.jdkim.DKIMSigner;
import org.apache.james.jdkim.api.BodyHasher;
import org.apache.james.jdkim.api.Headers;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.exceptions.FailException;
import org.apache.mailet.Mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.IOException;
import java.io.OutputStream;
import java.security.PrivateKey;

/**
 * Base class for digitally signing a message using the DKIM protocol
 */
public abstract class AbstractDKIMSign extends AbstractCipherMailMailet
{
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        CONVERT_TO_7BIT    ("convertTo7Bit"),
        DKIM_HEADER        ("dkimHeader"),
        FOLD_SIGNATURE     ("foldSignature");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * If true and the message contains 8bit parts, the message will be converted
     * to 7bit prior to signing.
     */
    private boolean convertTo7Bit;

    /*
     * If true the signature will be folded. Only enable foldSignature if header signing
     * is relaxed otherwise folding will break the DKIM signature.
     */
    private boolean foldSignature;

    /*
     * The DKIM header to use (defaults to DKIM-Signature)
     */
    private String dkimHeader = DKIMUtils.DEFAULT_DKIM_HEADER;

    @Override
    protected void initMailet()
    throws MessagingException
    {
        convertTo7Bit = getBooleanInitParameter(Parameter.CONVERT_TO_7BIT.name, true);

        foldSignature = getBooleanInitParameter(Parameter.FOLD_SIGNATURE.name, false);

        dkimHeader = getInitParameter(Parameter.DKIM_HEADER.name, dkimHeader);
    }

    protected void initialize(Mail mail, Context context)
    throws IOException, MessagingException
    {
        // Empty on purpose. Can be overridden by classes that extend this class.
    }

    protected abstract PrivateKey getPrivateKey(Mail mail, Context context)
    throws IOException, MessagingException;

    protected abstract String getSignatureTemplate(Mail mail, Context context)
    throws IOException, MessagingException;

    /*
     * Returns the Crypto provider to use for DKIM signing
     */
    protected String getProvider()
    {
        // Use the default provider
        return null;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            Context context = new ContextImpl();

            initialize(mail, context);

            PrivateKey privateKey = getPrivateKey(mail, context);

            if (privateKey != null)
            {
                // If we change the message, it can happen that if the message is an invalid MIME message
                // that the message end up in the error spool because after changing a header, the
                // complete message gets re-encoded to MIME at some point.
                MimeMessage message = MailUtils.cloneMessageWithFixedMessageID(mail.getMessage());

                if (convertTo7Bit) {
                    // Before signing, the message should be converted to 7bit to make sure
                    // the signature 'survives' the Internet.
                    boolean converted = MailUtils.convertTo7Bit(message);

                    if (converted) {
                        message.saveChanges();
                    }
                }

                String signatureTemplate = getSignatureTemplate(mail, context);

                if (StringUtils.isNotEmpty(signatureTemplate))
                {
                    DKIMSigner signer = new DKIMSigner(signatureTemplate, privateKey);

                    SignatureRecord signRecord = signer.newSignatureRecordTemplate(signatureTemplate);

                    BodyHasher bodyHasher = signer.newBodyHasher(signRecord);

                    OutputStream os = null;

                    try {
                        os = new CRLFOutputStream(new SkipHeadersOutputStream(
                                bodyHasher.getOutputStream()));

                        message.writeTo(os);
                    }
                    catch (IOException e) {
                        throw new MessagingException("Exception calculating body hash: " + e.getMessage(), e);
                    }
                    finally
                    {
                        try {
                            if (os != null) {
                                os.close();
                            }
                        }
                        catch (IOException e) {
                            throw new MessagingException("Exception calculating body hash: " + e.getMessage(), e);
                        }
                    }

                    Headers headers = new DKIMMessageHeaders(message);

                    String signature = signer.sign(headers, bodyHasher);

                    // Replace default DKIM header by custom header
                    signature = dkimHeader + StringUtils.substringAfter(signature, DKIMUtils.DEFAULT_DKIM_HEADER);

                    // the signature field cannot be folded after signing in the simple mode
                    // because that would break the signature
                    if (foldSignature) {
                        signature = MimeUtility.fold(dkimHeader.length(), signature);
                    }

                    HeaderUtils.prependHeaderLine(message, signature);

                    // Validate the message to make sure that James can write the message.
                    //
                    // Note: Because normally only a header is added, the message-id is not changed. The
                    // exception to this is when the message is converted from 8bit to 7bit (see above).
                    // When the message is converted from 7bit to 8bit the message-id is changed.
                    MailUtils.validateMessage(message);

                    // Message is okay, set it as the new message
                    mail.setMessage(message);

                    getLogger().atInfo().log(createLogLine("Message was DKIM signed", mail, true));
                }
                else {
                    // Should never happen in normal circumstances because the Signature template cannot be set
                    // to an empty value with the GUI
                    getLogger().atWarn().log(createLogLine("The message could not be DKIM signed. " +
                            "Signature template is not set", mail, true));
                }
            }
            else {
                getLogger().atWarn().log(createLogLine("The message could not be DKIM signed. There is no private key",
                        mail, true));
            }
        }
        catch(MessagingException | IOException | FailException e) {
            getLogger().error("DKIM signing failed.", e);
        }
    }
}
