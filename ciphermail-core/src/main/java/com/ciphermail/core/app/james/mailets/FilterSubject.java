/*
 * Copyright (c) 2012-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.cache.PatternCache;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.util.RegExprUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Mailet that filters the subject using a regular expression. The RegEx can either be static text or read
 * from a Mail attribute.
 */
@SuppressWarnings({"java:S6813"})
public class FilterSubject extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(FilterSubject.class);

    /*
     * The subject filter
     */
    private String filter;

    /*
     * The name of the Mail attribute from which the filter will be read.
     */
    private String attribute;

    /*
     * If set and an error occurs changing the subject, the name of next processor
     */
    private String errorProcessor;

    /*
     * Used for caching compiled Pattern's from a reg expr
     */
    @Inject
    private PatternCache patternCache;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        FILTER          ("filter"),
        ATTRIBUTE       ("attribute"),
        ERROR_PROCESSOR ("errorProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException
    {
        Objects.requireNonNull(patternCache);

        filter = getInitParameter(Parameter.FILTER.name);

        attribute = getInitParameter(Parameter.ATTRIBUTE.name);

        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
    }

    private String[] getFilter(Mail mail)
    {
        String regExpValue = attribute != null ? MailAttributesUtils.attributeAsString(mail, attribute) : filter;

        String[] result = RegExprUtils.splitRegExp(regExpValue);

        if (regExpValue != null && result == null) {
            getLogger().warn("Filter {} is not a valid filter", regExpValue);
        }

        return result;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final MimeMessage message = mail.getMessage();

            String currentSubject = MailUtils.getSafeSubject(message);

            if (currentSubject != null)
            {
                String[] localFilter = getFilter(mail);

                if (localFilter != null)
                {
                    Pattern pattern = patternCache.getPattern(StringUtils.defaultString(localFilter[0]));

                    if (pattern != null)
                    {
                        String newSubject = pattern.matcher(currentSubject).replaceAll(StringUtils.defaultString(
                                localFilter[1]));

                        if (!StringUtils.equals(currentSubject, newSubject))
                        {
                            getLogger().info("Subject filtered. Old subject: {}, new subject: {}", currentSubject,
                                    newSubject);

                            // If we change the message, it can happen that if the message is an invalid MIME message
                            // that the message end up in the error spool because after changing a header, the
                            // complete message gets re-encoded to MIME at some point. If the message is invalid MIME
                            // this might result in the message being stored in the error spool. To solve this we can
                            // clone the message and validate the message before changing the subject and decide not
                            // to change the subject if the message cannot be validated. The problem with this however
                            // is that if the subject filter is used to filter out the security keywords from the
                            // subject line which indicate the security properties of the email (like signed,
                            // encrypted etc.), and this cannot be done because the message is an invalid MIME message,
                            // then this might be misused to "spoof" the security keywords because the subject filter
                            // cannot remove them. However, storing the mail in the error spool is also not a good
                            // option because an attacker can misuse this to inject a lot of mails in the error spool.
                            // If keyword filtering is an absolute must, it's advised to use a spam/antivirus filter
                            // in front of the gateway to check whether message is strictly conforming to the MIME
                            // standards
                            try {
                                MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(message);

                                clone.setSubject(newSubject);

                                clone.saveChanges();

                                MailUtils.validateMessage(clone);

                                mail.setMessage(clone);
                            }
                            catch (Exception e)
                            {
                                getLogger().warn("Message with message-id {} and MailID {} is not a valid MIME message. " +
                                        "The subject can therefore not be filtered.",
                                        MailUtils.getMessageIDQuietly(message),
                                        CoreApplicationMailAttributes.getMailID(mail));

                                if (StringUtils.isNotEmpty(errorProcessor)) {
                                    mail.setState(errorProcessor);
                                }
                            }
                        }
                    }
                }
            }
            else {
                getLogger().debug("Subject is null");
            }
        }
        catch (MessagingException e)
        {
            getLogger().error("Error handling message while filtering subject", e);

            if (StringUtils.isNotEmpty(errorProcessor)) {
                mail.setState(errorProcessor);
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (errorProcessor != null) {
            requiredProcessors.add(new ProcessingState(errorProcessor));
        }

        return requiredProcessors;
    }
}
