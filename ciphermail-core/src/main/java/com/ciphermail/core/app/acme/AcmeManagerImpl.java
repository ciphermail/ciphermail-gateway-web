/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.core.app.acme;

import com.ciphermail.core.app.GlobalPreferencesManager;
import com.ciphermail.core.common.jackson.JacksonUtil;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.AltNamesInspector;
import com.ciphermail.core.common.security.certificate.CertificateUtils;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilder;
import com.ciphermail.core.common.security.certificate.TLSKeyPairBuilderUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemWriter;
import org.shredzone.acme4j.Account;
import org.shredzone.acme4j.AccountBuilder;
import org.shredzone.acme4j.Authorization;
import org.shredzone.acme4j.Certificate;
import org.shredzone.acme4j.Order;
import org.shredzone.acme4j.Problem;
import org.shredzone.acme4j.Session;
import org.shredzone.acme4j.Status;
import org.shredzone.acme4j.challenge.Http01Challenge;
import org.shredzone.acme4j.exception.AcmeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.ReentrantLock;

/**
 * AcmeManagerImpl is an implementation of the AcmeManager interface that provides methods for managing ACME operations,
 * such as setting account key pair, creating account key pair, ordering certificates, etc.
 */
public class AcmeManagerImpl implements AcmeManager
{
    private static final Logger logger = LoggerFactory.getLogger(AcmeManagerImpl.class);

    /*
     * Thread pool for running back-ground tasks
     */
    private final ExecutorService executorService;

    /*
     * The ACME settings are stored in the global settings
     */
    private final GlobalPreferencesManager globalPreferencesManager;

    /*
     * Starts transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * File containing the issued domain certificate and key
     */
    private final File acmeCertPath;

    /*
     * File containing the final issued certificate
     */
    private final File tlsCertPath;

    /*
     * Maximum attempts of status polling until VALID/INVALID is expected
     */
    private int maxChallengeAttempts = 50;

    /*
     * Maximum attempts of status polling until VALID/INVALID is expected
     */
    private int maxOrderAttempts = 50;

    /*
     * Default wait time (in msec) between consecutive attempts
     */
    private long retryInterval = Duration.ofSeconds(3).toMillis();

    /*
     * Max age (in msec) for challenge tokens
     */
    private long challengeMaxStaleTime = Duration.ofHours(24).toMillis();

    /*
     * Renew the certificate if the certificate expires in less than the given days
     */
    private int validityRenewDays = 30;

    /*
     * Lock to make sure that only one order background process can run concurrently
     */
    private static final ReentrantLock orderLock = new ReentrantLock();

    public AcmeManagerImpl(
            @Nonnull GlobalPreferencesManager globalPreferencesManager,
            @Nonnull ExecutorService executorService,
            @Nonnull TransactionOperations transactionOperations,
            @Nonnull File acmeCertPath,
            @Nonnull File tlsCertPath)
    {
        this.globalPreferencesManager = Objects.requireNonNull(globalPreferencesManager);
        this.executorService = Objects.requireNonNull(executorService);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
        this.acmeCertPath = Objects.requireNonNull(acmeCertPath);
        this.tlsCertPath = Objects.requireNonNull(tlsCertPath);
    }

    private AcmeSettings getAcmeSettings()
    throws HierarchicalPropertiesException
    {
        return AcmeSettingsImpl.getInstance(globalPreferencesManager.getGlobalUserPreferences().getProperties());
    }

    @Override
    public void setAccountKeyPair(@Nonnull KeyPair accountKeyPair)
    throws HierarchicalPropertiesException, IOException
    {
        StringWriter pem = new StringWriter();

        try (PemWriter pemWriter = new PemWriter(pem)) {
            pemWriter.writeObject(new JcaMiscPEMGenerator(accountKeyPair));
        }

        getAcmeSettings().setAccountKeyPair(pem.toString());
    }

    @Override
    public KeyPair getAccountKeyPair()
    throws HierarchicalPropertiesException, IOException
    {
        String pemKeyPair = getAcmeSettings().getAccountKeyPair();

        if (pemKeyPair == null) {
            return null;
        }

        try (PEMParser pemParser = new PEMParser(new StringReader(pemKeyPair)))
        {
            SecurityFactory securityFactory = SecurityFactoryFactory.getSecurityFactory();

            JcaPEMKeyConverter keyConverter = new JcaPEMKeyConverter();

            // Since we are importing PEM, we will use the non-sensitive provider because any private material
            // will not be stored on HSM
            keyConverter.setProvider(securityFactory.getNonSensitiveProvider());

            return keyConverter.getKeyPair((PEMKeyPair) pemParser.readObject());
        }
    }

    @Override
    public void createAccountKeyPair(@Nonnull TLSKeyPairBuilder.Algorithm algorithm)
    throws GeneralSecurityException, HierarchicalPropertiesException, IOException
    {
        setAccountKeyPair(TLSKeyPairBuilder.getInstance().setKeyAlgorithm(Objects.requireNonNull(algorithm))
                .buildKeyPair());
    }

    @Override
    public Optional<URI> getTermsOfService()
    throws HierarchicalPropertiesException, AcmeException
    {
        return getAcmeSession().getMetadata().getTermsOfService();
    }

    @Override
    public void setTosAccepted(boolean accepted)
    throws HierarchicalPropertiesException
    {
        getAcmeSettings().setTosAccepted(accepted);
    }

    @Override
    public boolean isTosAccepted()
    throws HierarchicalPropertiesException
    {
        return getAcmeSettings().getTosAccepted();
    }

    @Override
    public @Nonnull Account findOrRegisterAccount()
    throws HierarchicalPropertiesException, IOException, AcmeException
    {
        KeyPair accountKeyPair = getAccountKeyPair();

        if (accountKeyPair == null) {
            throw new AcmeException("Account keypair does not exist");
        }

        Session session = getAcmeSession();

        AcmeSettings acmeSettings = getAcmeSettings();

        AccountBuilder accountBuilder = new AccountBuilder()
                .agreeToTermsOfService()
                .useKeyPair(accountKeyPair);

        if (acmeSettings.getAccountEmail() != null) {
            accountBuilder.addEmail(acmeSettings.getAccountEmail());
        }

        if (acmeSettings.getKeyIdentifier() != null && acmeSettings.getEncodedMacKey() != null) {
            accountBuilder.withKeyIdentifier(acmeSettings.getKeyIdentifier(), acmeSettings.getEncodedMacKey());
        }

        return accountBuilder.create(session);
    }

    @Override
    public void deactivateAccount()
    throws HierarchicalPropertiesException, IOException, AcmeException
    {
        findOrRegisterAccount().deactivate();
    }

    @Override
    public void orderCertificate(@Nonnull TLSKeyPairBuilder.Algorithm algorithm, @Nonnull List<String> domains)
    throws HierarchicalPropertiesException, AcmeException, IOException
    {
        if (orderLock.isLocked()) {
            throw new AcmeException("An order is already pending");
        }

        if (!isTosAccepted()) {
            throw new AcmeException("Terms of service is not yet accepted");
        }

        if (getAccountKeyPair() == null) {
            throw new AcmeException("Account KeyPair is not available");
        }

        executorService.submit(() ->
        {
            if (!orderLock.tryLock()) {
                // can only happen if some thread got the lock in between the initial check and starting the thread
                // this is not likely
                throw new IllegalStateException("An order is already pending");
            }

            try {
                orderCertificateBackground(algorithm, domains);
            }
            catch (IOException | GeneralSecurityException | AcmeException e) {
                logger.error("Error ordering certificate", e);
            }
            finally {
                orderLock.unlock();
            }
        });
    }

    @Scheduled(
            fixedDelayString = "${ciphermail.acme.check.fixed-delay:3600000}",
            initialDelayString = "${ciphermail.acme.check.initial-delay:30000}"
    )
    public void autoRenewCertificate()
    throws AcmeException, IOException
    {
        renewCertificate(false);
    }

    @Override
    public void renewCertificate(boolean forceRenewal)
    throws AcmeException, IOException
    {
        if (!acmeCertPath.exists())
        {
            logger.debug("There is no ACME certificate to renew");

            return;
        }

        X509Certificate acmeCertificate;

        // check if there is a certificate that should be renewed
        try {
            List<X509Certificate> chain = CertificateUtils.readX509Certificates(acmeCertPath);

            if (chain.isEmpty()) {
                // should not happen
                throw new AcmeException(String.format("The file %s does not contain any certificates", acmeCertPath));
            }

            acmeCertificate = chain.get(0);
        }
        catch (CertificateException | NoSuchProviderException | FileNotFoundException e) {
            throw new AcmeException("Error reading ACME certificate", e);
        }

        // check if the certificate should be renewed
        Date expirationDate = acmeCertificate.getNotAfter();

        if (DateUtils.addDays(new Date(), validityRenewDays).before(expirationDate) && !forceRenewal)
        {
            logger.info("No need to renew certificate. Certificate expiration date {}", expirationDate);

            return;
        }

        logger.warn("Certificate expires on {}", expirationDate);

        if (orderLock.isLocked()) {
            throw new AcmeException("An order is already pending");
        }

        if  (Boolean.FALSE.equals(transactionOperations.execute(tx ->
        {
            try {
                return getAcmeSettings().getAutoRenewalEnabled();
            }
            catch (HierarchicalPropertiesException e) {
                throw new UnhandledException(e);
            }
        })))
        {
            // this can only happen if an ACME certificate was requested but an ACME cert is no longer used
            logger.warn("Certificate renewal is not enabled");

            return;
        }

        // get the domains from the certificate
        AltNamesInspector altNamesInspector = new AltNamesInspector(acmeCertificate);

        List<String> domains = altNamesInspector.getDNSNames();

        if (domains.isEmpty()) {
            throw new AcmeException("Domains are empty");
        }

        TLSKeyPairBuilder.Algorithm algorithm = TLSKeyPairBuilderUtils.getPublicKeyAlgorithm(
                acmeCertificate.getPublicKey());

        if (algorithm == null) {
            // should not happen
            algorithm = TLSKeyPairBuilder.Algorithm.SECP256R1;
        }

        TLSKeyPairBuilder.Algorithm finalAlgorithm = algorithm;

        transactionOperations.executeWithoutResult(tx ->
        {
            try {
                orderCertificate(finalAlgorithm, domains);
            }
            catch (HierarchicalPropertiesException | AcmeException | IOException e) {
                logger.error("Error renewing certificate with algorithm {} for domain(s) {}",
                        finalAlgorithm, domains, e);
            }
        });
    }

    @Override
    public AcmeTokenCache.Challenge getChallenge(@Nonnull String token)
    throws HierarchicalPropertiesException, JsonProcessingException
    {
        return loadTokenCache().getChallenge(token);
    }

    // will be executed in a back-ground thread
    private void orderCertificateBackground(@Nonnull TLSKeyPairBuilder.Algorithm algorithm, List<String> domains)
    throws IOException, GeneralSecurityException, AcmeException
    {
        // transactions should not take too long
        // waiting for the challenge takes a long time
        Account account = transactionOperations.execute(tx ->
        {
            try {
                return findOrRegisterAccount();
            }
            catch (HierarchicalPropertiesException | IOException | AcmeException e) {
                throw new UnhandledException(e);
            }
        });

        KeyPair domainKeyPair = TLSKeyPairBuilder.getInstance().setKeyAlgorithm(Objects.requireNonNull(algorithm))
                .buildKeyPair();

        Order order = Objects.requireNonNull(account).newOrder().domains(domains).create();

        // Perform all required authorizations
        for (Authorization auth : order.getAuthorizations()) {
            authorize(auth);
        }

        // Order the certificate
        order.execute(domainKeyPair);

        Status status = waitForCertificateIssuance(order);

        if (status != Status.VALID) {
            throw new AcmeException(String.format("Challenge failed. Problem: %s", order.getError()
                    .map(Problem::toString)
                    .orElse("unknown")));
        }

        // Get the certificate
        Certificate certificate = order.getCertificate();

        logger.info("ACME certificate was generated. Domains: {} Location: {}", domains, certificate.getLocation());

        // Write a combined file containing the private key, the certificate and chain.
        try (FileWriter fw = new FileWriter(acmeCertPath))
        {
            try (PemWriter pemWriter = new PemWriter(fw))
            {
                pemWriter.writeObject(new JcaMiscPEMGenerator(domainKeyPair));

                for (X509Certificate x509Certificate : certificate.getCertificateChain()) {
                    pemWriter.writeObject(new JcaMiscPEMGenerator(x509Certificate));
                }
            }
        }

        // copy the acme cert to the monitores TLS cert file
        FileUtils.copyFile(acmeCertPath, tlsCertPath);
    }

    private AcmeTokenCache loadTokenCache()
    throws HierarchicalPropertiesException, JsonProcessingException
    {
        String tokenCacheJSON = getAcmeSettings().getTokenCacheJSON();

        return tokenCacheJSON != null ? JacksonUtil.getObjectMapper().readValue(
                tokenCacheJSON, AcmeTokenCache.class) : new AcmeTokenCache();
    }

    private void storeTokenCache(AcmeTokenCache tokenCache)
    throws HierarchicalPropertiesException, JsonProcessingException
    {
        AcmeSettings acmeSettings = getAcmeSettings();

        // store the updated token cache
        acmeSettings.setTokenCacheJSON(JacksonUtil.getObjectMapper().writeValueAsString(tokenCache));
    }

    private void authorize(@Nonnull Authorization auth)
    throws AcmeException
    {
        logger.info("Authorization for domain {}", auth.getIdentifier().getDomain());

        // The authorization is already valid. No need to process a challenge.
        if (auth.getStatus() == Status.VALID) {
            return;
        }

        // Find the desired challenge and prepare it.
        Http01Challenge challenge = auth.findChallenge(Http01Challenge.class)
                .orElseThrow(() -> new AcmeException(String.format("Found no %s challenge", Http01Challenge.TYPE)));

        // transaction should be short so create local transaction and not wait for the challenge to finish
        transactionOperations.executeWithoutResult(tx ->
        {
            try {
                AcmeTokenCache tokenCache = loadTokenCache();

                tokenCache.addChallenge(challenge.getToken(), challenge.getAuthorization());

                // store the updated token cache
                storeTokenCache(tokenCache);
            }
            catch (HierarchicalPropertiesException | JsonProcessingException e) {
                throw new UnhandledException(e);
            }
        });

        try {
            // Now trigger the challenge.
            challenge.trigger();

            Status status = waitForChallengeResult(challenge);

            if (status != Status.VALID) {
                throw new AcmeException(String.format("Challenge failed. Problem: %s", challenge.getError()
                        .map(Problem::toString)
                        .orElse("unknown")));
            }
        }
        finally {
            transactionOperations.executeWithoutResult(tx ->
            {
                try {
                    // remove ACME challenge from the cache
                    AcmeTokenCache tokenCache = loadTokenCache();

                    logger.info("Removing token {}", challenge.getToken());

                    tokenCache.removeChallenge(challenge.getToken());

                    // remove stale challenges
                    // this can, for example, happen if there was an exception after the transaction
                    for (String staleToken : tokenCache.getStaleTokens(challengeMaxStaleTime))
                    {
                        logger.warn("Removing stale token {}", staleToken);

                        tokenCache.removeChallenge(staleToken);
                    }

                    // store the updated token cache
                    storeTokenCache(tokenCache);
                }
                catch (Exception e) {
                    logger.error("Error updating token cache", e);
                }
            });
        }
    }

    private @Nonnull Status waitForChallengeResult(Http01Challenge challenge)
    throws AcmeException
    {
        for (int attempt = 0; attempt < maxChallengeAttempts; attempt++)
        {
            logger.info("Checking current status, attempt {} of {}", attempt, maxChallengeAttempts);

            Instant now = Instant.now();

            // Update the status property
            Instant retryAfter = challenge.fetch().orElse(now.plusMillis(retryInterval));

            // Check the status
            Status currentStatus = challenge.getStatus();

            if (currentStatus == Status.VALID || currentStatus == Status.INVALID)  {
                // Reached VALID or INVALID, we're done here
                return currentStatus;
            }

            logger.info("Retry after {}", retryAfter);

            // Wait before next try
            try {
                Thread.sleep(now.until(retryAfter, ChronoUnit.MILLIS));
            }
            catch (InterruptedException ex)
            {
                Thread.currentThread().interrupt();

                throw new AcmeException("Interrupted while waiting for a challenge");
            }
        }

        throw new AcmeException("ACME challenge failed");
    }

    private @Nonnull Status waitForCertificateIssuance(Order order)
    throws AcmeException
    {
        for (int attempt = 0; attempt < maxChallengeAttempts; attempt++)
        {
            logger.info("Checking current status, attempt {} of {}", attempt, maxChallengeAttempts);

            Instant now = Instant.now();

            // Update the status property
            Instant retryAfter = order.fetch().orElse(now.plusMillis(retryInterval));

            // Check the status
            Status currentStatus = order.getStatus();

            if (currentStatus == Status.VALID || currentStatus == Status.INVALID)  {
                // Reached VALID or INVALID, we're done here
                return currentStatus;
            }

            logger.info("Retry after {}", retryAfter);

            // Wait before next try
            try {
                Thread.sleep(now.until(retryAfter, ChronoUnit.MILLIS));
            }
            catch (InterruptedException ex)
            {
                Thread.currentThread().interrupt();

                throw new AcmeException("Interrupted while waiting for a challenge");
            }
        }

        throw new AcmeException("ACME certificate issuance failed");
    }

    private Session getAcmeSession()
    throws HierarchicalPropertiesException
    {
        return new Session(getAcmeSettings().getServerUri());
    }

    public int getMaxChallengeAttempts() {
        return maxChallengeAttempts;
    }

    public void setMaxChallengeAttempts(int maxChallengeAttempts) {
        this.maxChallengeAttempts = maxChallengeAttempts;
    }

    public int getMaxOrderAttempts() {
        return maxOrderAttempts;
    }

    public void setMaxOrderAttempts(int maxOrderAttempts) {
        this.maxOrderAttempts = maxOrderAttempts;
    }

    public long getRetryInterval() {
        return retryInterval;
    }

    public void setRetryInterval(long retryInterval) {
        this.retryInterval = retryInterval;
    }

    public long getChallengeMaxStaleTime() {
        return challengeMaxStaleTime;
    }

    public void setChallengeMaxStaleTime(long challengeMaxStaleTime) {
        this.challengeMaxStaleTime = challengeMaxStaleTime;
    }

    public int getValidityRenewDays() {
        return validityRenewDays;
    }

    public void setValidityRenewDays(int validityRenewDays) {
        this.validityRenewDays = validityRenewDays;
    }
}
