/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionScheme;
import com.ciphermail.core.common.security.smime.SMIMESigningAlgorithm;

import javax.annotation.Nonnull;

/**
 * UserProperties contains higher level functionality (as opposed to the basic HierarchicalProperties methods)
 * for accessing CipherMail specific settings.
 *
 */
public interface SMIMEProperties
{
    /**
     * If S/MIME is enabled for the user
     */
    void setSMIMEEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMEEnabled()
    throws HierarchicalPropertiesException;

    /**
     * True if S/MIME strict is enabled for the user
     */
    void setSMIMEStrict(Boolean strict)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMEStrict()
    throws HierarchicalPropertiesException;

    /**
     * If true messages will only be signed when encrypted
     */
    void setSkipSignOnly(Boolean value)
    throws HierarchicalPropertiesException;

    Boolean getSkipSignOnly()
    throws HierarchicalPropertiesException;

    /**
     * If true calendar emails are not S/MIME'd
     * (Outlook cannot handle S/MIME calendar items)
     */
    void setSMIMESkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMESkipCalendar()
    throws HierarchicalPropertiesException;

    /**
     * If true calendar emails are not digitally signed
     * (Outlook cannot handle S/MIME calendar items)
     */
    void setSMIMESkipSigningCalendar(Boolean skip)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMESkipSigningCalendar()
    throws HierarchicalPropertiesException;

    /**
     * If true certain headers will be added to the encrypted CMS blob
     */
    void setSMIMEProtectHeaders(Boolean protect)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMEProtectHeaders()
    throws HierarchicalPropertiesException;

    /**
     * The S/MIME encryption algorithm to use (see SMIMEEncryptionAlgorithm for supported algorithms)
     */
    void setSMIMEEncryptionAlgorithm(SMIMEEncryptionAlgorithm algorithm)
    throws HierarchicalPropertiesException;

    SMIMEEncryptionAlgorithm getSMIMEEncryptionAlgorithm()
    throws HierarchicalPropertiesException;

    /**
     * The S/MIME encryption scheme to use (see SMIMEEncryptionScheme for supported scheme's)
     */
    void setSMIMEEncryptionScheme(SMIMEEncryptionScheme scheme)
    throws HierarchicalPropertiesException;

    SMIMEEncryptionScheme getSMIMEEncryptionScheme()
    throws HierarchicalPropertiesException;

    /**
     * The S/MIME signing algorithm to use (see SMIMESigningAlgorithm for supported algorithms)
     */
    void setSMIMESigningAlgorithm(SMIMESigningAlgorithm algorithm)
    throws HierarchicalPropertiesException;

    SMIMESigningAlgorithm getSMIMESigningAlgorithm()
    throws HierarchicalPropertiesException;

    /**
     * If true the message will be checked for illegal characters after decryption (to prevent the EFAIL attack)
     */
    void setSMIMECheckInvalid7BitChars(Boolean checkInvalid7BitChars)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMECheckInvalid7BitChars()
    throws HierarchicalPropertiesException;

    /**
     * If true and illegal characters are detected in the decrypted message (see checkInvalid7BitChars), decryption
     * will be aborted
     */
    void setSMIMEAbortDecryptionOnInvalid7BitChars(Boolean abortDecryptionOnInvalid7BitChars)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getSMIMEAbortDecryptionOnInvalid7BitChars()
    throws HierarchicalPropertiesException;

    /**
     * If true, auto selection of encryption certificates is enabled
     */
    void setAutoSelectEncryptionCerts(Boolean enabled)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAutoSelectEncryptionCerts()
    throws HierarchicalPropertiesException;

    /**
     * If true, the most recent (i.e., with the latest valid from) signing certificate will always be used
     */
    void setAlwaysUseFreshestSigningCert(Boolean useFreshestSigningCert)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAlwaysUseFreshestSigningCert()
    throws HierarchicalPropertiesException;

    /**
     * If true and a message is S/MIME encrypted, the additional certificates will be added
     */
    void setAddAdditionalCertificates(Boolean add)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAddAdditionalCertificates()
    throws HierarchicalPropertiesException;

    /**
     * The maximum size of a message that will be S/MIME'd
     */
    void setMaxSizeSMIME(Long size)
    throws HierarchicalPropertiesException;

    @Nonnull Long getMaxSizeSMIME()
    throws HierarchicalPropertiesException;

    /**
     * If true the signature from S/MIME message will be removed
     */
    void setRemoveSMIMESignature(Boolean value)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getRemoveSMIMESignature()
    throws HierarchicalPropertiesException;

    /**
     * If true, a certificate will be requested if the sender does not yet have a certificate
     */
    void setAutoRequestCertificate(Boolean value)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAutoRequestCertificate()
    throws HierarchicalPropertiesException;

    /**
     * If true, a certificate will be automatically imported from digitally signed email
     */
    void setAutoImportCertificatesFromMail(Boolean value)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAutoImportCertificatesFromMail()
    throws HierarchicalPropertiesException;

    /**
     * If true, a certificate will only be automatically imported from digitally signed email if the certificate
     * is trusted
     */
    void setAutoImportCertificatesSkipUntrusted(Boolean value)
    throws HierarchicalPropertiesException;

    @Nonnull Boolean getAutoImportCertificatesSkipUntrusted()
    throws HierarchicalPropertiesException;
}
