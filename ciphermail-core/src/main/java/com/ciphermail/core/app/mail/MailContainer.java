/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.mail;

import org.apache.commons.lang.SerializationException;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * Is used to serialize the Mail into MailRepositoryItem. Serialization is done this way to make sure
 * we can support upward and backward compatibility.
 *
 * Note: the MimeMessage contained in the Mail object is not serialized!
 *
 * @author Martijn Brinkers
 *
 */
public class MailContainer implements Serializable
{
    @Serial
    private static final long serialVersionUID = 1L;

    /*
     * The Mail to serialize
     */
    private Mail mail;

    public MailContainer(@Nonnull Mail mail) {
        this.mail = Objects.requireNonNull(mail);
    }

    @Serial
    private void writeObject(ObjectOutputStream out)
    throws IOException
    {
        out.writeLong(serialVersionUID);

        out.writeObject(mail);
    }

    @Serial
    private void readObject(ObjectInputStream in)
    throws IOException, ClassNotFoundException
    {
        long version = in.readLong();

        if (version != serialVersionUID) {
            throw new SerializationException("Version expected '" + serialVersionUID + "' but got '" + version);
        }

        mail = (Mail) in.readObject();
    }

    public Mail getMail() {
        return mail;
    }
}
