/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.dkim.DKIMUtils;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.HeaderUtils;
import com.ciphermail.core.common.security.ca.CertificateRequestHandler;
import com.ciphermail.core.common.security.ca.CertificateRequestHandlerRegistry;
import com.ciphermail.core.common.security.openpgp.keyserver.HKPKeyServersImpl;
import com.ciphermail.core.common.security.password.validator.JSONNOutOfMRulePasswordData;
import com.ciphermail.core.common.security.password.validator.NOutOfMRuleJSONParser;
import com.ciphermail.core.common.security.password.validator.PasswordStrengthValidator;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateBuilderException;
import com.ciphermail.core.common.template.TemplateObjectsFactory;
import com.ciphermail.core.common.trigger.HeaderTriggerUtils;
import com.ciphermail.core.common.util.ByteArray;
import com.ciphermail.core.common.util.DomainUtils;
import com.ciphermail.core.common.util.JSONUtils;
import com.ciphermail.core.common.util.LimitReachedException;
import com.ciphermail.core.common.util.PhoneNumberUtils;
import com.ciphermail.core.common.util.RegExprUtils;
import com.ciphermail.core.common.util.SizeLimitedOutputStream;
import com.ciphermail.core.common.util.URIUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.james.jdkim.api.SignatureRecord;
import org.apache.james.jdkim.tagvalue.SignatureRecordImpl;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.io.StringReader;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.regex.Pattern;

public class UserPropertiesValidators
{
    // keys for the validation message to allow error messages to be localized
    private static final String EXCEED_MAX_LENGTH_KEY = "backend.validation.exceed-max-length";
    private static final String INVALID_EMAIL_KEY = "backend.validation.invalid-email-address";
    private static final String INVALID_EMAIL_LIST_KEY = "backend.validation.invalid-email-list";
    private static final String INVALID_DOMAIN_KEY = "backend.validation.invalid-domain";
    private static final String INVALID_REGEX_KEY = "backend.validation.invalid-regex";
    private static final String INVALID_JSON_KEY = "backend.validation.invalid-json";
    private static final String INVALID_FORMAT_STRING_KEY = "backend.validation.invalid-format-string";
    private static final String INVALID_HEADER_NAME_VALUE_KEY = "backend.validation.invalid-header-name-value";
    private static final String INVALID_HEADER_TRIGGER_KEY = "backend.validation.invalid-header-trigger";
    private static final String INVALID_PASSWORD_POLICY_KEY = "backend.validation.invalid-password-policy";
    private static final String INVALID_URI_KEY = "backend.validation.invalid-uri";
    private static final String INVALID_PHONENUMBER_KEY = "backend.validation.invalid-phone-number";
    private static final String INVALID_TEMPLATE_KEY = "backend.validation.invalid-template";
    private static final String INVALID_CERTIFICATE_REQUEST_HANDLER = "backend.validation.invalid-certificate-request-handler";
    private static final String INVALID_DKIM_TEMPLATE = "backend.validation.invalid-dkim-template";
    private static final String INVALID_SMS_TRANSPORT = "backend.validation.invalid-sms-transport";
    private static final String INVALID_KEYPAIR = "backend.validation.invalid-key-pair";
    private static final String WEAK_PASSWORD = "backend.validation.weak-password";
    private static final String INVALID_PDF = "backend.validation.invalid-pdf";
    private static final String INVALID_PGP_KEYSERVERS = "backend.validation.invalid-pgp-key-servers";
    private static final String INVALID_SUBJECT_FILTER_KEY = "backend.validation.invalid-subject-filter";
    private static final String INVALID_SHA512_THUMBPRINT = "backend.validation.invalid-sha512-thumbprint";

    /*
     * Regex for a SHA512 thumbprint
     */
    private static final Pattern sha512RegEx = Pattern.compile("(?i)[a-f0-9]{128}");

    private UserPropertiesValidators() {
        // empty on purpose
    }

    public static String validateMaxLength(@Nonnull String propertyName, String value, int maxLength)
    throws PropertyValidationFailedException
    {
        if (value != null && value.length() > maxLength) {
            throw PropertyValidationFailedException.createInstance(String.format(
                    "%s exceeds max length %s", value, maxLength),
                    EXCEED_MAX_LENGTH_KEY, propertyName);
        }

        return value;
    }

    public static String validateEmail(@Nonnull String propertyName, String email)
    throws PropertyValidationFailedException
    {
        if (email != null && !EmailAddressUtils.isValid(email)) {
            throw PropertyValidationFailedException.createInstance(String.format(
                    "%s is not a valid email address", email),
                    INVALID_EMAIL_KEY, propertyName);
        }

        return email;
    }

    public static String validateEmailList(@Nonnull String propertyName, String addressList)
    throws PropertyValidationFailedException
    {
        if (addressList == null) {
            return null;
        }

        try {
            List<String> addresses = EmailAddressUtils.parseAddressList(addressList);

            if (addresses.isEmpty()) {
                throw new AddressException("There are no email addresses");
            }

            addressList = StringUtils.join(addresses, ",");
        }
        catch (AddressException e) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s contains an invalid email address", addressList),
                    INVALID_EMAIL_LIST_KEY, propertyName);
        }

        return addressList;
    }

    public static String validateDomain(@Nonnull String propertyName, String domain, DomainUtils.DomainType domainType)
    throws PropertyValidationFailedException
    {
        if (domain != null && !DomainUtils.isValid(domain, domainType)) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid domain", domain),
                    INVALID_DOMAIN_KEY, propertyName);
        }

        return domain;
    }

    public static String validateRegEx(@Nonnull String propertyName, String regEx)
    throws PropertyValidationFailedException
    {
        if (regEx != null && !RegExprUtils.isValidRegExp(regEx)) {
            throw PropertyValidationFailedException.createInstance(String.format(
                    "%s is not a valid regular expression", regEx),
                    INVALID_REGEX_KEY, propertyName);
        }

        return regEx;
    }

    public static String validateJSONObject(@Nonnull String propertyName, String json)
    throws PropertyValidationFailedException
    {
        if (json != null && !JSONUtils.isValidJSONObject(json)) {
            throw PropertyValidationFailedException.createInstance(
                            "value is not a valid JSON object",
                    INVALID_JSON_KEY, propertyName);
        }

        return json;
    }

    public static String validateFormatString(@Nonnull String propertyName, String formatString, Object... parameters)
    throws PropertyValidationFailedException
    {
        if (formatString != null)
        {
            try {
                formatString.formatted(parameters);
            }
            catch (IllegalFormatException e) {
                throw PropertyValidationFailedException.createInstance(
                        "value is not a valid format string",
                        INVALID_FORMAT_STRING_KEY, propertyName);
            }
        }

        return formatString;
    }

    /**
     * Validates whether the input equals to HEADER-NAME[:HEADER-VALUE] where HEADER-NAME and HEADER-VALUE should only
     * contain ASCII characters. HEADER-VALUE is optional
     */
    public static String validateHeader(@Nonnull String propertyName, String headerNameValue)
    throws PropertyValidationFailedException
    {
        if (headerNameValue == null) {
            return null;
        }

        String[] pair = StringUtils.split(headerNameValue, ":", 2);

        String headerName = null;
        String headerValue = "";

        if (pair.length > 0) {
            headerName = pair[0];
        }

        if (pair.length > 1) {
            headerValue = pair[1];
        }

        if (headerName == null || !HeaderUtils.isValidHeaderName(headerName) || !StringUtils.isAsciiPrintable(headerValue)) {
            throw PropertyValidationFailedException.createInstance(String.format(
                    "%s is not a valid header:name value", headerNameValue),
                    INVALID_HEADER_NAME_VALUE_KEY, propertyName);
        }

        return headerNameValue;
    }

    /**
     * Validates whether the input equals to
     * <p>
     * HEADER-NAME[:REGEX]
     * <p>
     * Where HEADER-NAME is the name of the header and REGEX is the optional header value specified as a regular
     * expression.
     */
    public static String validateHeaderTrigger(@Nonnull String propertyName, String headerTrigger)
    throws PropertyValidationFailedException
    {
        if (headerTrigger == null) {
            return null;
        }

        try {
            HeaderTriggerUtils.parseHeaderTrigger(headerTrigger);
        }
        catch (IllegalArgumentException e)
        {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid header trigger", headerTrigger),
                    INVALID_HEADER_TRIGGER_KEY, propertyName);
        }

        return headerTrigger;
    }

    /**
     * Validates the given password policy string against the defined rules.
     *
     * @param propertyName the name of the property associated with the password policy
     * @param passwordPolicy the JSON string representing the password policy to be validated
     * @return the validated password policy string if it is valid
     * @throws PropertyValidationFailedException if the password policy is invalid or cannot be parsed
     */
    public static String validatePasswordPolicy(@Nonnull String propertyName, String passwordPolicy)
    throws PropertyValidationFailedException
    {
        if (passwordPolicy != null) {
            try {
                NOutOfMRuleJSONParser.parseJSON(passwordPolicy);
            }
            catch (Exception e) {
                throw PropertyValidationFailedException.createInstance(String.format(
                        "%s is not a valid password policy", passwordPolicy),
                        INVALID_PASSWORD_POLICY_KEY, propertyName);
            }
        }

        return passwordPolicy;
    }

    /**
     * Validates a given password against a specified password policy using a password strength validator.
     *
     * @param propertyName the name of the property being validated
     * @param password the password to validate; may be null
     * @param passwordStrengthValidator an instance of {@link PasswordStrengthValidator} used to validate the strength of the password
     * @param passwordPolicyJSON the JSON representation of the password policy against which the password will be validated
     * @return the validated password if it passes the strength validation, or throws an exception if validation fails
     * @throws PropertyValidationFailedException if the password does not meet the strength requirements defined by the policy
     */
    public static String validatePassword(
            @Nonnull String propertyName,
            String password,
            @Nonnull PasswordStrengthValidator passwordStrengthValidator,
            @Nonnull String passwordPolicyJSON)
    throws PropertyValidationFailedException
    {
        // we do not have a username here, set it to the empty value
        if (password != null && !passwordStrengthValidator.isStrongPassword(new JSONNOutOfMRulePasswordData(
                "", password, passwordPolicyJSON)))
        {
            throw PropertyValidationFailedException.createInstance("Selected password is too weak",
                    WEAK_PASSWORD, propertyName);
        }

        return password;
    }

    /**
     * Validates if the given URI is valid according to the specified URI type.
     * If the URI is invalid, a {@code PropertyValidationFailedException} is thrown.
     *
     * @param propertyName the name of the property associated with the URI, must not be null.
     * @param uri the URI string to be validated.
     * @param uriType the type of URI to validate against, must not be null.
     * @return the validated URI string if it is valid, or null if the provided URI is null.
     * @throws PropertyValidationFailedException if the URI is not valid according to the specified URI type.
     */
    public static String validateURI(@Nonnull String propertyName, String uri, @Nonnull URIUtils.URIType uriType)
    throws PropertyValidationFailedException
    {
        if (uri != null && !URIUtils.isValidURI(uri, uriType)) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid URI", uri),
                    INVALID_URI_KEY, propertyName);
        }

        return uri;
    }

    /**
     * Validates the given telephone number to ensure it is in a valid format.
     * If the input number is null, the method returns null. Otherwise, it attempts to
     * normalize and validate the phone number. If the validation fails, an exception is thrown.
     *
     * @param propertyName the name of the property being validated; must not be null
     * @param phoneNumber the telephone number to validate; can be null
     * @return the normalized and validated phone number if validation is successful,
     *         or null if the input phone number is null
     * @throws PropertyValidationFailedException if the phone number is invalid or
     *         cannot be normalized
     */
    public static String validateTelephoneNumber(@Nonnull String propertyName, String phoneNumber)
    throws PropertyValidationFailedException
    {
        if (phoneNumber == null) {
            return null;
        }

        String validated = PhoneNumberUtils.normalizeAndValidatePhoneNumber(phoneNumber);

        if (validated == null) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid phone number", phoneNumber),
                    INVALID_PHONENUMBER_KEY, propertyName);
        }

        return validated;
    }

    /**
     * Validates the given template using the provided {@link TemplateBuilder}.
     * If the template is null, the method returns null. Otherwise, it attempts to build
     * the template with no properties to ensure its validity.
     *
     * @param propertyName the name of the property being validated; must not be null
     * @param template the template string to validate; can be null
     * @param templateBuilder a {@link TemplateBuilder} instance used for template validation; must not be null
     * @return the original template string if validation is successful, or null if the template input is null
     * @throws PropertyValidationFailedException if the template is invalid and cannot be built
     */
    public static String validateTemplate(@Nonnull String propertyName, String template,
            @Nonnull TemplateBuilder templateBuilder)
    throws PropertyValidationFailedException
    {
        if (template == null) {
            return null;
        }

        try {
            // test whether the template can be built without any properties
            templateBuilder.buildTemplate(template, TemplateObjectsFactory.createSimpleHash());
        }
        catch (TemplateBuilderException e) {
            throw PropertyValidationFailedException.createInstance("Template is not valid",
                    INVALID_TEMPLATE_KEY, propertyName,
                    new PropertyValidationFailedException.Param("message", e.getMessage()));
        }

        return template;
    }

    /**
     * Validates whether the provided certificate request handler name corresponds to an existing handler
     * in the given certificate request handler registry. If the handler does not exist, a
     * {@code PropertyValidationFailedException} is thrown with an appropriate error message.
     *
     * @param propertyName the name of the property being validated; must not be null
     * @param certificateRequestHandlerName the name of the certificate request handler to validate; can be null
     * @param certificateRequestHandlerRegistry a registry containing registered certificate request handlers; must not be null
     * @return the name of the validated certificate handler, or {@code null} if the input certificate request handler name is null
     * @throws PropertyValidationFailedException if the certificate request handler name is invalid or does not exist in the registry
     */
    public static String validateCertificateRequestHandler(
            @Nonnull String propertyName,
            String certificateRequestHandlerName,
            @Nonnull CertificateRequestHandlerRegistry certificateRequestHandlerRegistry)
    throws PropertyValidationFailedException
    {
        if (certificateRequestHandlerName == null) {
            return null;
        }

        CertificateRequestHandler certificateRequestHandler = certificateRequestHandlerRegistry.getHandler(
                certificateRequestHandlerName);

        if (certificateRequestHandler == null) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "Certificate Request Handler with name %s does not exist", certificateRequestHandlerName),
                    INVALID_CERTIFICATE_REQUEST_HANDLER, propertyName);
        }

        return certificateRequestHandler.getCertificateHandlerName();
    }

    /**
     * Validates a DKIM (DomainKeys Identified Mail) signature template by replacing tokens using
     * a test email address, constructing a {@link SignatureRecord}, and invoking its validation logic.
     * If the validation fails, a {@code PropertyValidationFailedException} is thrown with a descriptive
     * error message.
     *
     * @param propertyName the name of the property being validated; must not be null
     * @param signatureTemplate the DKIM signature template to validate; can be null
     * @return the original signature template if validation is successful, or null if the input is null
     * @throws PropertyValidationFailedException if the signature template is invalid
     */
    public static String validateDKIMSignatureTemplate(@Nonnull String propertyName, String signatureTemplate)
    throws PropertyValidationFailedException
    {
        if (signatureTemplate == null) {
            return null;
        }

        try {
            SignatureRecord signatureRecord = new SignatureRecordImpl(
                    DKIMUtils.replaceSignatureTemplateTokens(signatureTemplate, "test@example.com"));

            signatureRecord.validate();
        }
        catch (IllegalStateException e) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "DKIM signature template %s is invalid", signatureTemplate),
                    INVALID_DKIM_TEMPLATE, propertyName, e);
        }

        return signatureTemplate;
    }

    /**
     * Validates the provided SMS transport name against the available entries in the SMSTransportFactoryRegistry.
     * If the SMS transport name is not registered in the factory registry, an exception will be thrown.
     *
     * @param propertyName the name of the property being validated; must not be null
     * @param smsTransportName the name of the SMS transport to validate; can be null
     * @param transportFactoryRegistry the registry containing the SMS transport factory definitions; must not be null
     * @return the provided SMS transport name if validation is successful, or null if the input SMS transport name is null
     * @throws PropertyValidationFailedException if the SMS transport name is not registered in the factory registry
     */
    public static String validateSMSTransport(
            @Nonnull String propertyName,
            String smsTransportName,
            @Nonnull SMSTransportFactoryRegistry transportFactoryRegistry)
    throws PropertyValidationFailedException
    {
        if (smsTransportName == null) {
            return null;
        }

        if (transportFactoryRegistry.getFactory(smsTransportName) == null) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "SMS transport with name %s does not exist", smsTransportName),
                    INVALID_SMS_TRANSPORT, propertyName);
        }

        return smsTransportName;
    }

    /**
     * Validates whether the provided PEM-encoded content represents a valid key pair.
     * If the content is invalid or cannot be parsed into a key pair, a {@link PropertyValidationFailedException}
     * is thrown with an appropriate error message.
     *
     * @param propertyName the name of the property being validated; used for error reporting
     * @param pem a string containing PEM-encoded content to be validated; can be null
     * @return the original PEM string if validation is successful, or null if the input is null
     * @throws PropertyValidationFailedException if the PEM content is invalid or does not represent a key pair
     */
    public static String validateKeyPair(@Nonnull String propertyName, String pem)
    throws PropertyValidationFailedException
    {
        if (pem == null) {
            return null;
        }

        PEMParser pemParser = new PEMParser(new StringReader(pem));

        Object o;

        try {
            o = pemParser.readObject();
        }
        catch (IOException e) {
            throw PropertyValidationFailedException.createInstance(
                            "Unable to read PEM encoded private key",
                    INVALID_KEYPAIR, propertyName);
        }
        finally {
            IOUtils.closeQuietly(pemParser);
        }

        if (! (o instanceof PEMKeyPair)) {
            throw PropertyValidationFailedException.createInstance(
                    "PEM object is not a key pair",
                    INVALID_KEYPAIR, propertyName);
        }

        return pem;
    }

    /**
     * Checks if the byte array is a valid PDF
     */
    public static ByteArray validatePdf(@Nonnull String propertyName, ByteArray pdf, long maxSize)
    throws PropertyValidationFailedException
    {
        if (pdf == null) {
            return null;
        }

        try {
            // Loading and writing the PDF should not fail
            // A PDF can be compressed so limit the number of bytes
            try (PDDocument pdfDocument = PDDocument.load(pdf.getBytes())) {
                pdfDocument.save(new SizeLimitedOutputStream(NullOutputStream.INSTANCE, maxSize));
            }
        }
        catch (LimitReachedException e) {
            throw PropertyValidationFailedException.createInstance(
                    String.format("The PDF exceeds the maximum size %s", FileUtils.byteCountToDisplaySize(maxSize)),
                    INVALID_PDF, propertyName);
        }
        catch (InvalidPasswordException e) {
            throw PropertyValidationFailedException.createInstance(
                    "The PDF is encrypted",
                    INVALID_PDF, propertyName);
        }
        catch (Exception e) {
            throw PropertyValidationFailedException.createInstance(
                    String.format("Error reading PDF (%s)", e.getMessage()),
                    INVALID_PDF, propertyName);
        }

        return pdf;
    }

    /**
     * Validates and re-encodes a JSON-encoded string representing PGP key servers.
     * If the input is invalid or cannot be re-encoded, a {@link PropertyValidationFailedException} is thrown.
     *
     * @param propertyName the name of the property being validated; used for error reporting
     * @param jsonEncodedKeyServers the JSON-encoded string representing the key servers; can be null
     * @return the re-encoded JSON string if validation is successful, or null if the input is null
     * @throws PropertyValidationFailedException if the input cannot be validated or re-encoded
     */
    public static String validateJSONEncodePGPKeyServers(@Nonnull String propertyName, String jsonEncodedKeyServers)
    throws PropertyValidationFailedException
    {
        if (jsonEncodedKeyServers == null) {
            return null;
        }

        try {
            // test whether json value can be decoded and re-encoded
            return HKPKeyServersImpl.toJSON(HKPKeyServersImpl.fromJSON(jsonEncodedKeyServers));
        }
        catch (IOException e) {
            throw PropertyValidationFailedException.createInstance("PGP key servers is not valid",
                    INVALID_PGP_KEYSERVERS, propertyName,
                    new PropertyValidationFailedException.Param("message", e.getMessage()));
        }
    }

    /**
     * Validates whether the input matches /REGEX/ VALUE where REGEX should be a valid regular expression
     */
    public static String validateSubjectFilter(@Nonnull String propertyName, String subjectFilter)
    throws PropertyValidationFailedException
    {
        if (subjectFilter == null) {
            return null;
        }

        if (RegExprUtils.splitRegExp(subjectFilter) == null) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid subject filter", subjectFilter),
                    INVALID_SUBJECT_FILTER_KEY, propertyName);
        }

        return subjectFilter;
    }

    /**
     * Validates whether the provided thumbprint string matches the defined SHA512 format.
     * If the thumbprint does not match the SHA512 regex pattern, a {@code PropertyValidationFailedException}
     * is thrown with a descriptive message.
     *
     * @param propertyName the name of the property being validated; used for error reporting
     * @param thumbprint the SHA512 thumbprint string to be validated; can be null
     * @return the provided thumbprint if it is valid
     * @throws PropertyValidationFailedException if the thumbprint is not a valid SHA512 hash
     */
    public static String validateSHA512Thumbprint(@Nonnull String propertyName, String thumbprint)
    throws PropertyValidationFailedException
    {
        if (thumbprint != null && !sha512RegEx.matcher(thumbprint).matches()) {
            throw PropertyValidationFailedException.createInstance(String.format(
                            "%s is not a valid SHA512 thumbprint", thumbprint),
                    INVALID_SHA512_THUMBPRINT, propertyName);
        }

        return thumbprint;
    }
}
