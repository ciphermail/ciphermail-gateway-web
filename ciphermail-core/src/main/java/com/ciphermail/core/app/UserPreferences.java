/*
 * Copyright (c) 2008-2017, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app;

import com.ciphermail.core.app.properties.UserProperties;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.util.LinkedSet;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.Set;

/**
 * UserPreferences contains preferences for a specific user. UserPreferences is not just used by the User object,
 * although the name seems to imply this, but also for a domain.
 *
 * Note: a UserPreferences is uniquely identified by the combination of name and category. The implementor should therefore
 * override equals is such a way that an object is equal if and only if name and category is equal.
 *
 * @author Martijn Brinkers
 *
 */
public interface UserPreferences
{
    /**
     * Name of this preference. This name should be unique.
     */
    @Nonnull String getName();

    /**
     * The category of this UserPreferences.
     */
    @Nonnull String getCategory();

    /**
     * A mutable Set of X509Certificates provided by this UserPreferences.
     * <p>
     * Note: There is no guarantee that returned set will always be the same instance
     * (depends on implementation). The returned Set should be a 'live' set ie.
     * changes made to the set are persisted.
     */
    @Nonnull Set<X509Certificate> getCertificates();

    /**
     * Returns an immutable Set of certificates that are inherited from the inherited UserPreferences. The returned
     * set is unordered.
     */
    @Nonnull Set<X509Certificate> getInheritedCertificates();

    /**
     * Returns a mutable set of named certificates. The named certificates set is a general purpose
     * collection of certificates. The rationale for the named certificates collection is that it
     * allows you to associate certificates with the user preference without having to change the
     * database. This cannot be solved by using the UserProperties because it does not contain
     * a foreign key to a certificate.
     * <p>
     * Note: There is no guarantee that returned set will always be the same instance
     * (depends on implementation). The returned Set should be a 'live' set ie. changes
     * made to the set are persisted.
     */
    @Nonnull Set<NamedCertificate> getNamedCertificates();

    /**
     * Returns an immutable Set of nameCertificates that are inherited from the inherited UserPreferences. The returned
     * set is unordered.
     */
    @Nonnull Set<NamedCertificate> getInheritedNamedCertificates();

    /**
     * Returns the KeyCertificatePair for this UserPreferences.
     */
    KeyAndCertificate getKeyAndCertificate()
    throws CertStoreException, KeyStoreException;

    /**
     * Sets the KeyAndCertificate for this UserPreferences.
     */
    void setKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException;

    /**
     * Returns an immutable Set of KeyAndCertificates that are inherited from the inherited UserPreferences. The
     * returned set is unordered.
     */
    Set<KeyAndCertificate> getInheritedKeyAndCertificates()
    throws CertStoreException, KeyStoreException;

    /**
     * The properties. The returned HierarchicalProperties object is the combination of the properties of this
     * UserPreferences and the properties of the inherited UserPreferences.
     */
    @Nonnull UserProperties getProperties();

    /**
     * Returns the UserProperties which are explicitly set, i.e., does not contain any inherited properties. This is
     * for example used when exporting the user properties.
     */
    @Nonnull UserProperties getExplicitlySetProperties();

    /**
     * Returns the named blobs referenced by this UserPreferences object. The Set of NamesBlob's that's returned are
     * only the named blobs referenced by this UserPreferences. To get the inherited named blobs use
     * getInheritedUserPreferences#getNamedBlobs.
     * <p>
     * Note: There is no guarantee that returned set will always be the same instance (depends on implementation).
     * The returned Set should be a 'live' set ie. changes made to the set are persisted.
     */
    @Nonnull Set<NamedBlob> getNamedBlobs();

    /**
     * Returns a mutable ordered set of inherited UserPreferences. The last element in the ordered set is the most
     * significant UserPreferences and the first element the least significant.
     * <p>
     * Note: There is no guarantee that returned set will always be the same instance
     * (depends on implementation). The returned Set should be a 'live' set ie. changes
     * made to the set are persisted.
     */
    @Nonnull LinkedSet<UserPreferences> getInheritedUserPreferences();
}
