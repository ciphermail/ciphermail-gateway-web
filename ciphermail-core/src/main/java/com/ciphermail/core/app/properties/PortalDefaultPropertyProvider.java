/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DefaultPropertyProvider;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

/**
 *  DefaultPropertyProvider implementation for PortalProperties
 */
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class PortalDefaultPropertyProvider implements DefaultPropertyProvider
{
    /*
     * The default relative URLs
     */
    private static final String DEFAULT_RELATIVE_SIGNUP_URL = "signup";
    private static final String DEFAULT_RELATIVE_PASSWORD_RESET_URL = "password-reset";

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Autowired
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Override
    public String getName() {
        return PortalDefaultPropertyProvider.class.getName();
    }

    private PortalProperties createPortalProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(PortalPropertiesImpl.class)
                .createInstance(properties);
    }

    private String getDefaultSignupURL(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        // The default URL will be based on the Portal Base URL
        String defaultURL = StringUtils.trimToNull(createPortalProperties(properties).getBaseURL());

        if (defaultURL != null) {
            defaultURL = MiscStringUtils.ensureEndsWith(defaultURL, "/") + DEFAULT_RELATIVE_SIGNUP_URL;
        }

        return defaultURL;
    }

    private String getDefaultPasswordResetURL(@Nonnull HierarchicalProperties properties)
    throws HierarchicalPropertiesException
    {
        // The default URL will be based on the Portal Base URL
        String defaultURL = StringUtils.trimToNull(createPortalProperties(properties).getBaseURL());

        if (defaultURL != null) {
            defaultURL = MiscStringUtils.ensureEndsWith(defaultURL, "/") + DEFAULT_RELATIVE_PASSWORD_RESET_URL;
        }

        return defaultURL;
    }

    @Override
    public String getDefaultValue(@Nonnull HierarchicalProperties properties, String property)
    throws HierarchicalPropertiesException
    {
        // Note: This is not optimal if we need to support multiple default values. If more than
        // a couple properties need calculated default values, we will probably need to use a set
        // of registered properties or something. For now this will suffice.
        if (PortalPropertiesImpl.SIGNUP_URL.equals(property)) {
            return getDefaultSignupURL(properties);
        }
        else if (PortalPropertiesImpl.PASSWORD_RESET_URL.equals(property)) {
            return getDefaultPasswordResetURL(properties);
        }

        return null;
    }
}
