/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * PDF user properties
 */
public interface PDFProperties
{
    /**
     * The password used for pdf encryption.
     */
    void setPassword(String password)
    throws HierarchicalPropertiesException;

    String getPassword()
    throws HierarchicalPropertiesException;

    /**
     * The subject password trigger
     */
    void setSubjectPasswordTriggerRegEx(String trigger)
    throws HierarchicalPropertiesException;

    String getSubjectPasswordTriggerRegEx()
    throws HierarchicalPropertiesException;

    /**
     * If true, the subject password trigger is enabled
     */
    void setSubjectPasswordTriggerRegExEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getSubjectPasswordTriggerRegExEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The length of the generated password in bytes
     */
    void setPasswordLength(Integer length)
    throws HierarchicalPropertiesException;

    Integer getPasswordLength()
    throws HierarchicalPropertiesException;

    /**
     * If true, generated passwords will be sent to the message originator
     */
    void setPasswordsSendToOriginator(Boolean value)
    throws HierarchicalPropertiesException;

    Boolean getPasswordsSendToOriginator()
    throws HierarchicalPropertiesException;

    /**
     * If true, One Time Passwords will be enabled
     */
    void setOTPEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getOTPEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The secret key for generating One Time Passwords (OTPs).
     */
    void setOTPSecretKey(String secretKey)
    throws HierarchicalPropertiesException;

    String getOTPSecretKey()
    throws HierarchicalPropertiesException;

    /**
     * The sub-key for generating One Time Passwords (OTPs).
     */
    void setOTPSubKey(String subKey)
    throws HierarchicalPropertiesException;

    String getOTPSubKey()
    throws HierarchicalPropertiesException;

    /**
     * The OTP generator URL
     */
    void setOTPURL(String url)
    throws HierarchicalPropertiesException;

    String getOTPURL()
    throws HierarchicalPropertiesException;

    /**
     * If true, PDF encryption is allowed for this user
     */
    void setPdfEncryptionAllowed(Boolean allowed)
    throws HierarchicalPropertiesException;

    Boolean getPdfEncryptionAllowed()
    throws HierarchicalPropertiesException;

    /**
     * The maximum size of a message that will be PDF'd
     */
    void setPdfMaxSize(Long size)
    throws HierarchicalPropertiesException;

    Long getPdfMaxSize()
    throws HierarchicalPropertiesException;

    /**
     * Determines whether a PDF reply is allowed. For sender of a PDF encrypted email this means that
     * a reply URL is added to the encrypted PDF. For the receiver, if false the receiver cannot reply
     * to a PDF.
     */
    void setPdfReplyEnabled(Boolean allowed)
    throws HierarchicalPropertiesException;

    Boolean getPdfReplyEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The secret key for generating HMAC signed PDF reply URLs
     */
    void setPdfReplySecretKey(String secretKey)
    throws HierarchicalPropertiesException;

    String getPdfReplySecretKey()
    throws HierarchicalPropertiesException;

    /**
     * The Pdf Reply sub-key
     */
    void setPdfReplySubKey(String subKey)
    throws HierarchicalPropertiesException;

    String getPdfReplySubKey()
    throws HierarchicalPropertiesException;

    /**
     * The formatted string template for the reply subject
     */
    void setPdfReplySubjectFormatString(String formatString)
    throws HierarchicalPropertiesException;

    String getPdfReplySubjectFormatString()
    throws HierarchicalPropertiesException;

    /**
     * The formatted string template for the From friendly name
     */
    void setPdfReplyFromFormatString(String formatString)
    throws HierarchicalPropertiesException;

    String getPdfReplyFromFormatString()
    throws HierarchicalPropertiesException;

    /**
     * If true, the replier to the PDF will receive a CC of the message
     */
    void setPdfReplyCC(Boolean sendCC)
    throws HierarchicalPropertiesException;

    Boolean getPdfReplyCC()
    throws HierarchicalPropertiesException;

    /**
     * The time in seconds a reply is valid from the time the PDF was created.
     */
    void setPdfReplyValidityInterval(Long interval)
    throws HierarchicalPropertiesException;

    Long getPdfReplyValidityInterval()
    throws HierarchicalPropertiesException;

    /**
     * The base URL for PDF reply
     */
    void setPdfReplyURL(String url)
    throws HierarchicalPropertiesException;

    String getPdfReplyURL()
    throws HierarchicalPropertiesException;

    /**
     * The email sender to use for a PDF reply
     */
    void setPdfReplySender(String email)
    throws HierarchicalPropertiesException;

    String getPdfReplySender()
    throws HierarchicalPropertiesException;

    /**
     * If true, the PDF reply sender will be used for the PDF reply instead of the "real" reply'er
     */
    void setPdfReplySenderEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getPdfReplySenderEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The max allowed total size of all attachments for a reply
     */
    void setPdfReplyAttachmentsMaxSize(Long maxSize)
    throws HierarchicalPropertiesException;

    Long getPdfReplyAttachmentsMaxSize()
    throws HierarchicalPropertiesException;

    /**
     * The max allowed reply body size
     */
    void setPdfReplyBodyMaxSize(Long maxSize)
    throws HierarchicalPropertiesException;

    Long getPdfReplyBodyMaxSize()
    throws HierarchicalPropertiesException;

    /**
     * Only PDF encrypt the message if encryption is required
     */
    void setPdfOnlyEncryptIfMandatory(Boolean encryptIfMandatory)
    throws HierarchicalPropertiesException;

    Boolean getPdfOnlyEncryptIfMandatory()
    throws HierarchicalPropertiesException;

    /**
     * S/MIME sign the PDF encrypted email
     */
    void setPdfSignEmail(Boolean sign)
    throws HierarchicalPropertiesException;

    Boolean getPdfSignEmail()
    throws HierarchicalPropertiesException;

    /**
     * If true, the complete MIME message is deep scanned for attachments etc.
     */
    void setPdfDeepScan(Boolean deepscan)
    throws HierarchicalPropertiesException;

    Boolean getPdfDeepScan()
    throws HierarchicalPropertiesException;

    /**
     * If true, QR codes for PDF OTP is enabled
     */
    void setPdfQrCodeEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getPdfQrCodeEnabled()
    throws HierarchicalPropertiesException;
}
