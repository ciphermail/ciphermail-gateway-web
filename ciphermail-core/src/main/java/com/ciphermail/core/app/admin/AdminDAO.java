/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.admin;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import com.ciphermail.core.common.hibernate.GenericHibernateDAO;
import com.ciphermail.core.common.hibernate.SessionAdapter;
import com.ciphermail.core.common.hibernate.SortDirection;
import org.hibernate.query.Query;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * DAO for Admin entity {@link Admin}
 */
public class AdminDAO extends GenericHibernateDAO
{
    private AdminDAO(@Nonnull SessionAdapter session) {
        super(session);
    }

    /**
     * Create a new AdminDAO instance
     * @param session a Hibernate database session
     * @return new DAO instance
     */
    public static AdminDAO getInstance(@Nonnull SessionAdapter session) {
        return new AdminDAO(session);
    }

    /**
     * Returns an admin with the given name
     * @param name the name of the admin
     * @return The AdminDAO or null if there is no admin with the given name
     */
    public Admin getAdmin(@Nonnull String name)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Admin> criteriaQuery = criteriaBuilder.createQuery(Admin.class);

        Root<Admin> rootEntity = criteriaQuery.from(Admin.class);

        criteriaQuery.where(criteriaBuilder.equal(rootEntity.get(Admin.NAME_COLUMN), name));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(null);
    }

    /**
     * Deletes the admin with the given name.
     * @param name the admin to delete
     * @return true if the admin was deleted, false is the admin with the given name does not exist.
     */
    public boolean deleteAdmin(@Nonnull String name)
    {
        boolean deleted = false;

        Admin admin = getAdmin(name);

        if (admin != null)
        {
            delete(admin);

            deleted = true;
        }

        return deleted;
    }

    /**
     * Deletes all admins
     */
    public void deleteAll()
    {
        List<Admin> admins = getAdmins(null, null, null);

        for (Admin admin : admins) {
            delete(admin);
        }
    }

    /**
     * Returns a list of all admins
     * @param firstResult the index of the first result. If null, the index start at 0.
     * @param maxResults the maximum number of admins to return. If null, all Admin's will be returned.
     * @param sortDirection the sorting. If null, sorting will be ASC.
     * @return a list of Admin's
     */
    public List<Admin> getAdmins(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Admin> criteriaQuery = criteriaBuilder.createQuery(Admin.class);

        Root<Admin> rootEntity = criteriaQuery.from(Admin.class);

        criteriaQuery.orderBy(sortDirection == SortDirection.DESC ?
                criteriaBuilder.desc(rootEntity.get(Admin.NAME_COLUMN)) :
                criteriaBuilder.asc(rootEntity.get(Admin.NAME_COLUMN)));

        Query<Admin> query = createQuery(criteriaQuery);

        if (firstResult != null) {
            query.setFirstResult(firstResult);
        }

        if (maxResults != null) {
            query.setMaxResults(maxResults);
        }

        return query.getResultList();
    }

    /**
     * @return The number of Admin's
     */
    public long getAdminCount()
    {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();

        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<Admin> rootEntity = criteriaQuery.from(Admin.class);

        criteriaQuery.select(criteriaBuilder.count(rootEntity));

        return createQuery(criteriaQuery).uniqueResultOptional().orElse(0L);
    }
}
