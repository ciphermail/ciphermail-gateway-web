/*
 * Copyright (c) 2017-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.common.mail.BodyPartUtils;
import com.ciphermail.core.common.mail.PartException;
import com.ciphermail.core.common.mail.PartScanner;
import com.ciphermail.core.common.mail.PartScanner.PartListener;
import com.ciphermail.core.common.security.smime.SMIMEType;
import com.ciphermail.core.common.security.smime.SMIMEUtils;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang3.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.Part;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Matcher that matches if a message (or part of the message) is S/MIME. The difference between IsMIME matcher and
 * this matcher is that this matcher scans the message for attached S/MIME messages whereas IsMIME only checks the
 * upper message content-type.
 * <p>
 * Usage:
 * <p>
 * IsSMIME=matchOnError=false            | Matches on all S/MIME types
 * IsSMIME=matchOnError=false,none       | Matches when the message is not S/MIME
 * IsSMIME=matchOnError=false,signed     | Matches on S/MIME signed types
 * IsSMIME=matchOnError=false,encrypted  | Matches on S/MIME encrypted types
 * IsSMIME=matchOnError=false,compressed | Matches on S/MIME compressed types
 * <p>
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 *
 * @author Martijn Brinkers
 *
 */
public class IsSMIMEDeepScan extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(IsSMIMEDeepScan.class);

    /*
     * Max mime depth to search for parts
     */
    private int maxMimeDepth = 8;

    /*
     * The S/MIME types (signed, encrypted etc.) the mail should be to match
     */
    private final Set<SMIMEType> smimeTypes = new HashSet<>();

    final PartListener partListener = (parent, part, context) -> IsSMIMEDeepScan.this.onPart(part, context);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        String condition = getCondition();

        if (StringUtils.isNotEmpty(condition))
        {
            for (String item : StringUtils.split(condition, ','))
            {
                SMIMEType type = SMIMEType.fromName(item);

                if (type == null) {
                    throw new IllegalArgumentException(item + " is not a valid S/MIME type.");
                }

                smimeTypes.add(type);
            }
        }
    }

    private boolean onPart(Part part, Object context)
    {
        boolean continueScanning = true;

        try {
            MutableBoolean match = (MutableBoolean) context;

            // if the part is a attached message (rfc822) we should inspect the attached message
            if (part.isMimeType("message/rfc822")) {
                part = BodyPartUtils.extractFromRFC822(part);
            }

            SMIMEType messageType = SMIMEUtils.getSMIMEType(part);

            // check if conditionType matches the type of the message or of conditionType is null
            // that the message is an S/MIME message (does not matter what type of S/MIME message)
            if ((smimeTypes.contains(messageType) || (smimeTypes.isEmpty() && messageType != SMIMEType.NONE))) {
                match.setValue(true);
            }

            if (match.booleanValue()) {
                // Stop scanning
                continueScanning = false;
            }
        }
        catch (Exception e) {
            getLogger().error("Error handling MIME part", e);
        }

        return continueScanning;
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        Collection<MailAddress> matching = Collections.emptyList();

        PartScanner partScanner = new PartScanner(partListener, maxMimeDepth);

        partScanner.setExceptionOnMaxDepthReached(true);

        MutableBoolean match = new MutableBoolean();

        try {
            partScanner.scanPart(mail.getMessage(), match);
        }
        catch (PartException | IOException e) {
            throw new MessagingException("Error while scanning message for S/MIME parts", e);
        }

        if (match.booleanValue()) {
            matching = mail.getRecipients();
        }

        return matching;
    }

    public int getMaxMimeDepth() {
        return maxMimeDepth;
    }

    public void setMaxMimeDepth(int maxMimeDepth) {
        this.maxMimeDepth = maxMimeDepth;
    }
}
