/*
 * Copyright (c) 2008-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.properties.SMIMEProperties;
import com.ciphermail.core.app.properties.SMIMEPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.InheritedKeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.FreshestKeyAndCertificateComparator;
import com.ciphermail.core.common.security.certificate.validator.CertificateValidatorChain;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMESigning;
import com.ciphermail.core.common.security.smime.selector.CertificateSelector;
import com.ciphermail.core.common.security.smime.selector.KeyAndCertificateSelector;
import com.ciphermail.core.common.security.smime.selector.SigningKeyAndCertificateSelector;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.security.KeyStoreException;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * UserHibernate is not thread safe because the session variable is not thread safe. Typical lifetime should
 * be similar to the session.
 *
 * @author Martijn Brinkers
 *
 */
public class UserHibernate implements User
{
    private static final Logger logger = LoggerFactory.getLogger(UserHibernate.class);

    private final UserEntity userEntity;
    private final PKISecurityServices pKISecurityServices;
    private final CertificateSelector encryptionCertificateSelector;
    private final Session session;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    protected UserHibernate(
            @Nonnull UserEntity userEntity,
            @Nonnull PKISecurityServices pKISecurityServices,
            @Nonnull CertificateSelector encryptionCertificateSelector,
            @Nonnull Session session,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.userEntity = Objects.requireNonNull(userEntity);
        this.pKISecurityServices = Objects.requireNonNull(pKISecurityServices);
        this.encryptionCertificateSelector = Objects.requireNonNull(encryptionCertificateSelector);
        this.session = Objects.requireNonNull(session);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public String getEmail() {
        return userEntity.getEmail();
    }

    @Override
    public UserPreferences getUserPreferences()
    {
        return new UserPreferencesHibernate(userEntity.getUserPreferencesEntity(),
                pKISecurityServices.getKeyAndCertStore(), session, userPropertiesFactoryRegistry);
    }

    private SMIMEProperties createSMIMEProperties(HierarchicalProperties properties)
    {
        return userPropertiesFactoryRegistry.getFactoryForClass(SMIMEPropertiesImpl.class)
                .createInstance(properties);
    }

    @Override
    public @Nonnull Set<X509Certificate> getAutoSelectEncryptionCertificates()
    throws HierarchicalPropertiesException
    {
        // Only auto select certificates if enabled
        if (createSMIMEProperties(getUserPreferences().getProperties()).getAutoSelectEncryptionCerts()) {
            return encryptionCertificateSelector.getMatchingCertificates(getEmail());
        }

        return Collections.emptySet();
    }

    @Override
    public void setSigningKeyAndCertificate(KeyAndCertificate keyAndCertificate)
    throws CertStoreException, KeyStoreException
    {
        getUserPreferences().setKeyAndCertificate(keyAndCertificate);
    }

    private boolean isSigningKeyAndCertificateValid(KeyAndCertificate keyAndCertificate)
    {
        if (keyAndCertificate == null) {
            return false;
        }

        CertificateValidatorChain chain = new CertificateValidatorChain();

        // Add the following CertificateValidators to the chain so we can check if the certificate is
        // trusted, not revoked and can be used for S/MIME signing.
        chain.addValidators(new IsValidForSMIMESigning());
        chain.addValidators(pKISecurityServices.getPKITrustCheckCertificateValidatorFactory().
                    createValidator(null));

        boolean valid = false;

        try {
            valid = chain.isValid(keyAndCertificate.getCertificate());
        }
        catch (CertificateException e) {
            logger.error("Error validating certificate.", e);
        }
        return valid;
    }

    /*
     * Returns the KeyAndCertificate which is valid for signing and which has the most recent
     * noBefore date.
     */
    private KeyAndCertificate getFreshestSigningKeyCertificate()
    {
        KeyAndCertificate signingKeyAndCertificate = null;

        KeyAndCertificateSelector selector = new SigningKeyAndCertificateSelector(pKISecurityServices);

        // Select all KeyAndCertificates that are valid and have a matching email address. In most cases
        // this should just return 0 or 1 KeyAndCertificate unless there is more than one signing certificate
        // for a user.
        Set<KeyAndCertificate> keyAndCertificates = selector.getMatchingKeyAndCertificates(getEmail());

        if (CollectionUtils.isNotEmpty(keyAndCertificates))
        {
            // If there is only one signing certificate, use it
            if (keyAndCertificates.size() == 1) {
                signingKeyAndCertificate = keyAndCertificates.iterator().next();
            }
            else  {
                // If there are multiple signing certificates, sort on notBefore and use the
                // newest certificate
                List<KeyAndCertificate> sorted = new ArrayList<>(keyAndCertificates);

                sorted.sort(new FreshestKeyAndCertificateComparator());

                // The last one is the KeyAndCertificate with the newest notBefore i.e., the certificate
                // that was created the latest.
                signingKeyAndCertificate = sorted.get(sorted.size() - 1);
            }
        }

        return signingKeyAndCertificate;
    }

    @Override
    public KeyAndCertificate getSigningKeyAndCertificate()
    throws CertStoreException, KeyStoreException, HierarchicalPropertiesException
    {
        UserPreferences userPreferences = getUserPreferences();

        KeyAndCertificate signingKeyAndCertificate = null;

        // check if always select the freshest valid signing certificate is enabled
        if (createSMIMEProperties(userPreferences.getProperties()).getAlwaysUseFreshestSigningCert())
        {
            // Always select the freshest valid signing certificate
            signingKeyAndCertificate = getFreshestSigningKeyCertificate();

            if (signingKeyAndCertificate != null) {
                // Since the signing certificate should always be auto selected, we can set the current
                // selected keyAndCertificate to null to make sure that any previous selected keyAndCertificate
                // will not be "in-use".
                userPreferences.setKeyAndCertificate(null);
            }
        }

        if (signingKeyAndCertificate == null) {
            // Get the current selected signing key and certificate and check if it is still valid.
            // If not try to find one that is valid.
            signingKeyAndCertificate = userPreferences.getKeyAndCertificate();

            if (!isSigningKeyAndCertificateValid(signingKeyAndCertificate))
            {
                // Currently selected signingKeyAndCertificate is not valid (or there is none selected)
                signingKeyAndCertificate = getFreshestSigningKeyCertificate();

                if (signingKeyAndCertificate != null)
                {
                    // Store the newly selected signing keyAndCertificate.
                    userPreferences.setKeyAndCertificate(signingKeyAndCertificate);
                }
                else {
                    // Set the current keyAndCertificate to null to make sure that any previous selected
                    // keyAndCertificate which is no longer valid is removed otherwise the certificate
                    // will remain "in-use".
                    userPreferences.setKeyAndCertificate(null);
                }
            }
        }

        // If we do not have a valid signing certificate check if we inherit a valid one
        if (signingKeyAndCertificate == null)
        {
            Set<KeyAndCertificate> inheritedKeyAndCertificates = userPreferences.getInheritedKeyAndCertificates();

            // select the first valid one
            for (KeyAndCertificate keyAndCertificate : inheritedKeyAndCertificates)
            {
                if (isSigningKeyAndCertificateValid(keyAndCertificate))
                {
                    // Wrap the KeyAndCertificate to allow knowing the certificate is inherited
                    signingKeyAndCertificate = new InheritedKeyAndCertificate(keyAndCertificate);

                    break;
                }
            }
        }

        return signingKeyAndCertificate;
    }

    protected UserEntity getUserEntity() {
        return userEntity;
    }

    @Override
    public String toString() {
        return getEmail();
    }

    /**
     * User is equals iff email address is equal.
     */
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof User rhs)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return new EqualsBuilder()
            .append(getEmail(), rhs.getEmail())
            .isEquals();
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
            .append(getEmail())
            .toHashCode();
    }
}
