/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.eval.GetContentTypeFunction;
import com.ciphermail.core.app.james.eval.GetMailAttributeFunction;
import com.ciphermail.core.app.james.eval.GetMailSizeFunction;
import com.ciphermail.core.app.james.eval.GetUserPropertyFunction;
import com.ciphermail.core.app.james.eval.IsMIMETypeFunction;
import com.ciphermail.core.app.james.eval.ToFloatFunction;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.google.common.annotations.VisibleForTesting;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

@SuppressWarnings({"java:S6813"})
public abstract class AbstractEvaluateUser extends AbstractContextAwareCipherMailMatcher
{
    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * The expression to evaluate
     */
    private String expression;

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        expression = StringUtils.trimToNull(getCondition());

        if (expression == null) {
            throw new IllegalArgumentException("Expression must be specified.");
        }

        StrBuilder sb = new StrBuilder();

        sb.append("Expression: ");
        sb.append(expression);

        getLogger().info("{}", sb);
    }

    /*
     * Note: Evaluator is not thread safe.
     */
    private Evaluator getEvaluator() {
        return new Evaluator();
    }

    protected Collection<MailAddress> getMatchingMailAddresses(Mail mail, Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = user ->
                AbstractEvaluateUser.this.hasMatch(mail, user);

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses);
    }

    protected void addDefaultFunctions(Evaluator evaluator)
    {
        evaluator.putFunction(new ToFloatFunction());
        evaluator.putFunction(new IsMIMETypeFunction());
    }

    @VisibleForTesting
    protected boolean hasMatch(Mail mail, User user)
    throws MessagingException
    {
        Evaluator evaluator = getEvaluator();

        addDefaultFunctions(evaluator);

        evaluator.putFunction(new GetContentTypeFunction(mail));
        evaluator.putFunction(new GetMailAttributeFunction(mail));
        evaluator.putFunction(new GetMailSizeFunction(mail));
        evaluator.putFunction(new GetUserPropertyFunction(user));

        try {
            boolean match = evaluator.getBooleanResult(expression);

            getLogger().debug("Expression: {}, Match: {}", expression, match);

            return match;
        }
        catch (EvaluationException e) {
            throw new MessagingException("expression cannot be evaluated.", e);
        }
    }

    protected MessageOriginatorIdentifier getMessageOriginatorIdentifier() {
        return messageOriginatorIdentifier;
    }
}
