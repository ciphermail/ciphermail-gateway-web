/*
 * Copyright (c) 2012-2021, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.tools;

import com.ciphermail.core.common.hibernate.HibernateInfo;
import com.ciphermail.core.common.hibernate.HibernateSessionFactoryBuilder;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.hibernate.SessionFactory;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaExport.Action;
import org.hibernate.tool.schema.TargetType;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.EnumSet;
import java.util.Properties;

@SuppressWarnings({"java:S106", "java:S112"})
public class GenerateSchema
{
    private static final String COMMAND_NAME = GenerateSchema.class.getName();

    /*
     * The command line instance
     */
    private static CommandLine commandLine;

    /*
     * The CLI option for showing help
     */
    private static final String HELP_OPTION_SHORT = "h";
    private static final String HELP_OPTION_LONG = "help";

    /*
     * The CLI option for the file to which the generated schema file will be written to
     */
    private static final String OUTPUT_FILE_OPTION = "output-file";

    /*
     * If set, the drop schema will be generated instead of create schema
     */
    private static final String DROP_OPTION = "drop";

    /*
     * Hibernate options
     */
    private static final String DRIVER_CLASS_OPTION = "driver-class";
    private static final String URL_OPTION = "url";
    private static final String USERNAME_OPTION = "username";
    private static final String PASSWORD_OPTION = "password";
    private static final String DIALECT_OPTION = "dialect";
    private static final String PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION = "physical-naming-strategy-class-name";

    public static class LocalServices
    {
        @Bean
        public HibernateInfo hibernateInfoService()  {
            return new HibernateInfo();
        }

        @Bean
        public SessionFactory sessionFactoryService(HibernateInfo hibernateInfo)
        {
            Properties props = new Properties();

            props.setProperty("hibernate.connection.driver_class", commandLine.getOptionValue(DRIVER_CLASS_OPTION));
            props.setProperty("hibernate.connection.url", commandLine.getOptionValue(URL_OPTION));
            props.setProperty("hibernate.connection.username", commandLine.getOptionValue(USERNAME_OPTION));
            props.setProperty("hibernate.connection.password", commandLine.getOptionValue(PASSWORD_OPTION));

            if (commandLine.hasOption(DIALECT_OPTION)) {
                props.setProperty("hibernate.dialect", commandLine.getOptionValue(DIALECT_OPTION));
            }

            HibernateSessionFactoryBuilder builder = HibernateSessionFactoryBuilder.createInstance()
                    .setHibernateInfo(hibernateInfo)
                    .setProperties(props).setPackagesToScan("com.ciphermail");

            if (commandLine.hasOption(PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION))
            {
                builder.setPhysicalNamingStrategyClassName(commandLine.getOptionValue(
                        PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION));
            }

            return builder.build();
        }
    }

    private static Options createCommandLineOptions()
    {
        Options options = new Options();

        options.addOption(Option.builder(HELP_OPTION_SHORT).longOpt(HELP_OPTION_LONG).desc("Show help").build());

        options.addOption(Option.builder().longOpt(OUTPUT_FILE_OPTION).argName("file").hasArg().required().
                desc("The file to which the generated schema file will be written to").build());

        options.addOption(Option.builder().longOpt(DRIVER_CLASS_OPTION).argName("class").hasArg().required().
                desc("The database driver class").build());

        options.addOption(Option.builder().longOpt(URL_OPTION).argName("url").hasArg().required().
                desc("The database JDBC url").build());

        options.addOption(Option.builder().longOpt(USERNAME_OPTION).argName("username").hasArg().required().
                desc("The database JDBC authentication username").build());

        options.addOption(Option.builder().longOpt(PASSWORD_OPTION).argName("password").hasArg().required().
                desc("The database JDBC authentication password").build());

        options.addOption(Option.builder().longOpt(DIALECT_OPTION).argName("dialect").hasArg().
                desc("The database dialect (default: auto detection)").build());

        options.addOption(Option.builder().longOpt(PHYSICAL_NAMING_STRATEGY_CLASS_NAME_OPTION).argName("strategy").hasArg().
                desc("The naming strategy").build());

        options.addOption(Option.builder().longOpt(DROP_OPTION).desc(
                "If set, the drop schema will be generated instead of the creation schema").build());

        return options;
    }

    private static void handleCommandline(String[] args)
    throws Exception
    {
        CommandLineParser parser = new DefaultParser();

        Options options = createCommandLineOptions();

        HelpFormatter formatter = new HelpFormatter();

        try {
            commandLine = parser.parse(options, args);
        }
        catch (ParseException e) {
            formatter.printHelp(COMMAND_NAME, options, false);

            throw e;
        }

        if (commandLine.getOptions().length == 0 || commandLine.hasOption(HELP_OPTION_SHORT) ||
            commandLine.hasOption(HELP_OPTION_LONG))
        {
            formatter.printHelp(COMMAND_NAME, options, false);

            System.exit(1);

            return;
        }

        try (AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(
                LocalServices.class))
        {
            EnumSet<TargetType> targets = EnumSet.of(TargetType.SCRIPT);

            SchemaExport schemaExport = new SchemaExport();
            schemaExport.setFormat(true);

            schemaExport.setOutputFile(commandLine.getOptionValue(OUTPUT_FILE_OPTION));
            schemaExport.setOverrideOutputFileContent();

            schemaExport.execute(targets, commandLine.hasOption(DROP_OPTION) ? Action.DROP : Action.CREATE,
                    applicationContext.getBean(HibernateInfo.class).getMetadata());
        }
    }

    public static void main(String[] args)
    throws Exception
    {
        try {
            GenerateSchema.handleCommandline(args);
        }
        catch (MissingArgumentException | MissingOptionException e)
        {
            System.err.println("Some required parameters are missing: " + e.getMessage());

            System.exit(2);
        }
        catch (UnrecognizedOptionException e)
        {
            System.err.println(e.getMessage());

            System.exit(3);
        }
    }
}
