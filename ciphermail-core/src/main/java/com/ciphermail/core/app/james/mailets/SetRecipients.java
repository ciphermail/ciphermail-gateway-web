/*
 * Copyright (c) 2017-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.MailAddressUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet which sets the recipients of the Mail to a new list of recipients, i.e., redirects the mail to different
 * recipients
 */
public class SetRecipients extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SetRecipients.class);

    /*
     * The new recipients
     */
    private final List<MailAddress> recipients = new LinkedList<>();

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        RECIPIENT ("recipients");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException
    {
        String rawRecipients = Objects.requireNonNull(StringUtils.trimToNull(
                getInitParameter(Parameter.RECIPIENT.name)),
                Parameter.RECIPIENT.name + " parameter must be set");

        String[] recipientArray = StringUtils.split(rawRecipients, ',');

        for (String recipient : recipientArray)
        {
            String trimmedRecipient = StringUtils.trimToNull(recipient);

            if (trimmedRecipient != null) {
                try {
                    recipients.add(new MailAddress(trimmedRecipient));
                }
                catch (ParseException e) {
                    throw new ParseException("Email address " + trimmedRecipient + " is not a valid email address");
                }
            }
        }

        if (recipients.isEmpty()) {
            throw new MessagingException("There are no valid recipients");
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        logger.debug("Replacing recipients '{}' by '{}'", MailAddressUtils.getRecipients(mail), recipients);

        mail.setRecipients(recipients);
    }
}
