/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.app.NamedBlobCategories;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.workflow.UserPreferencesWorkflow;
import com.ciphermail.core.common.dlp.PolicyPatternMarshaller;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.util.CollectionUtils;

import javax.annotation.Nonnull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of {@link PolicyPatternManager}.
 *
 * @author Martijn Brinkers
 *
 */
public class PolicyPatternManagerImpl implements PolicyPatternManager
{
    /*
     * Sane maximum which should with normal usage never be reached
     */
    private static final int MAX_IN_USE_INFO_ITEMS = 100;

    /*
     * The named blob category to use.
     */
    private static final String CATEGORY = NamedBlobCategories.DLP_POLICY_PATTERN_CATEGORY;
    /*
     * Is used to persistently store the PolicyPatternNode's.
     */
    private final NamedBlobManager namedBlobManager;

    /*
     * Used for marshalling/unmarshalling PolicyPattern's to/from xml.
     */
    private final PolicyPatternMarshaller policyPatternMarshaller;

    /*
     * Used for checking whether a pattern is used by a UserPreferences.
     */
    private final UserPreferencesWorkflow userPreferencesWorkflow;

    public PolicyPatternManagerImpl(
            @Nonnull NamedBlobManager namedBlobManager,
            @Nonnull PolicyPatternMarshaller policyPatternMarshaller,
            @Nonnull UserPreferencesWorkflow userPreferencesWorkflow)
    {
        this.namedBlobManager = Objects.requireNonNull(namedBlobManager);
        this.policyPatternMarshaller = Objects.requireNonNull(policyPatternMarshaller);
        this.userPreferencesWorkflow = Objects.requireNonNull(userPreferencesWorkflow);
    }

    @Override
    public @Nonnull PolicyPatternNode createPattern(@Nonnull String name) {
        return new PolicyPatternNodeImpl(namedBlobManager.createNamedBlob(CATEGORY, name), policyPatternMarshaller);
    }

    @Override
    public void deletePattern(@Nonnull String name) {
        namedBlobManager.deleteNamedBlob(CATEGORY, name);
    }

    @Override
    public PolicyPatternNode getPattern(@Nonnull String name)
    {
        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);

        return namedBlob != null ? new PolicyPatternNodeImpl(namedBlob, policyPatternMarshaller) : null;
    }

    @Override
    public List<PolicyPatternNode> getPatterns(Integer firstResult, Integer maxResults)
    {
        List<PolicyPatternNode> patterns = new LinkedList<>();

        List<? extends NamedBlob> namedBlobs = namedBlobManager.getByCategory(CATEGORY, firstResult, maxResults);

        if (CollectionUtils.getSize(namedBlobs) > 0)
        {
            for (NamedBlob namedBlob : namedBlobs) {
                patterns.add(new PolicyPatternNodeImpl(namedBlob, policyPatternMarshaller));
            }
        }

        return patterns;
    }

    @Override
    public long getNumberOfPatterns() {
        return namedBlobManager.getByCategoryCount(CATEGORY);
    }

    @Override
    public boolean isInUse(@Nonnull String name)
    {
        long refs = 0;

        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);

        if (namedBlob != null)
        {
            // Check if used by a UserPreference
            refs = userPreferencesWorkflow.getReferencingFromNamedBlobsCount(namedBlob);

            if (refs == 0) {
                // Check if the pattern is used by a group
                refs = namedBlobManager.getReferencedByCount(namedBlob);
            }
        }

        return refs > 0;
    }

    @Override
    public List<String> getInUseInfo(@Nonnull String name)
    {
        List<String> result = new LinkedList<>();

        NamedBlob namedBlob = namedBlobManager.getNamedBlob(CATEGORY, name);

        if (namedBlob != null)
        {
            // Check if used by a UserPreference
            //
            // Note: use a sane maximum which should with normal usage never be reached
            List<UserPreferences> referencing = userPreferencesWorkflow.getReferencingFromNamedBlobs(
                    namedBlob, null, MAX_IN_USE_INFO_ITEMS);

            if (CollectionUtils.isNotEmpty(referencing))
            {
                for (UserPreferences userPreferences :referencing)
                {
                    result.add("user," + userPreferences.getCategory() +
                            "," + userPreferences.getName());
                }
            }

            // Check if the pattern is used by a group
            //
            // Note: use a sane maximum which should with normal usage never be reached
            List<? extends NamedBlob> blobs = namedBlobManager.getReferencedBy(namedBlob,
                    null, MAX_IN_USE_INFO_ITEMS);

            if (CollectionUtils.isNotEmpty(blobs))
            {
                for (NamedBlob blob : blobs) {
                    result.add("pattern,group," + blob.getName());
                }
            }
        }

        return result;
    }
}
