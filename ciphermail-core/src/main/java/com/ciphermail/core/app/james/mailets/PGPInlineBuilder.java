/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.PGPPublicKeys;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mime.FileExtensionResolver;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPCompressionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPEncryptionAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPHashAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPInlineMIMEBuilder;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPSecurityFactoryFactory;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Mailet for PGP signing and encryption.
 */
@SuppressWarnings({"java:S6813"})
public class PGPInlineBuilder extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPInlineBuilder.class);

    private static final String ACTIVATION_CONTEXT_KEY = PGPInlineBuilder.class.getName();

    @Override
    protected Logger getLogger() {
        return logger;
    }
    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SIGNING_ALGORITHM               ("signingAlgorithm"),
        SIGNING_ALGORITHM_ATTRIBUTE     ("signingAlgorithmAttribute"),
        ENCRYPTION_ALGORITHM            ("encryptionAlgorithm"),
        ENCRYPTION_ALGORITHM_ATTRIBUTE  ("encryptionAlgorithmAttribute"),
        COMPRESSION_ALGORITHM           ("compressionAlgorithm"),
        COMPRESSION_ALGORITHM_ATTRIBUTE ("compressionAlgorithmAttribute"),
        CONVERT_HTML_TO_TEXT            ("convertHTMLToText"),
        CONVERT_HTML_TO_TEXT_ATTRIBUTE  ("convertHTMLToTextAttribute"),
        ADD_INTEGRITY_PACKET            ("addIntegrityPacket"),
        ADD_INTEGRITY_PACKET_ATTRIBUTE  ("addIntegrityPacketAttribute"),
        RETAIN_MESSAGE_ID               ("retainMessageID"),
        SIGNED_PROCESSOR                ("signedProcessor"),
        ENCRYPTED_PROCESSOR             ("encryptedProcessor"),
        MAX_MIME_DEPTH                  ("maxMimeDepth");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The hash algorithm used for the signature.
     */
    private PGPHashAlgorithm signingAlgorithm = PGPHashAlgorithm.SHA256;

    /*
     * The Mail attribute from which the signing algorithm is read. If null or if there is no
     * attribute with the given name, the signingAlgorithm is used.
     */
    private String signingAlgorithmAttribute;

    /*
     * The encryption algorithm to use
     */
    private PGPEncryptionAlgorithm encryptionAlgorithm = PGPEncryptionAlgorithm.AES_128;

    /*
     * The Mail attribute from which the encryption algorithm is read. If null or if there is no
     * attribute with the given name, the encryptionAlgorithm is used.
     */
    private String encryptionAlgorithmAttribute;

    /*
     * The compression algorithm to use
     */
    private PGPCompressionAlgorithm compressionAlgorithm = PGPCompressionAlgorithm.ZLIB;

    /*
     * The Mail attribute from which the compression algorithm is read. If null or if there is no
     * attribute with the given name, the compressionAlgorithm is used.
     */
    private String compressionAlgorithmAttribute;

    /*
     * If true, a HTML part is converted to text and added as an additional part
     */
    private boolean convertHTMLToText = true;

    /*
     * The Mail attribute from which the covertHTMLToText value is read. If null or if there is no
     * attribute with the given name, the covertHTMLToText value is used.
     */
    private String convertHTMLToTextAttribute;

    /*
     * If true an integrity packet will be added to the PGP encrypted message
     */
    private boolean addIntegrityPacket = true;

    /*
     * The Mail attribute from which the addIntegrityPacket value is read. If null or if there is no
     * attribute with the given name, the addIntegrityPacket value is used.
     */
    private String addIntegrityPacketAttribute;

    /*
     * If true the original Message-ID will be used for the signed message
     */
    private boolean retainMessageID = true;

    /*
     * The next processor if signing succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String signedProcessor;

    /*
     * The next processor if enryption succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String encryptedProcessor;

    /*
     * maximum recursive depth for MIME parts.
     */
    private int maxMimeDepth = 8;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Identifies the 'sender' of the message (default version uses from as the identifier)
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Returns a file extension associated with a content-type
     */
    @Inject
    private FileExtensionResolver fileExtensionResolver;

    /*
     * Retrieves the PGP signing key for a user preferences instance
     */
    @Inject
    private UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelector;

    /*
     * For converting a PGPPrivate key to a PrivateKey
     */
    private JcaPGPKeyConverter keyConverter;

    /*
     * Context class for storing data in the activation context
     */
    private static class ActivationContext
    {
        /*
         * The PGP message. Can be null
         */
        private final MimeMessage pgpMessage;

        /*
         * PGP builder. Not null if pgpMessage is not null
         */
        private final PGPInlineMIMEBuilder pgpBuilder;

        /*
         * True if the message was signed
         */
        private final boolean signed;

        /*
         * True if the message was encrypted
         */
        private final boolean encrypted;

        ActivationContext(MimeMessage pgpMessage, PGPInlineMIMEBuilder pgpBuilder, boolean signed, boolean encrypted)
        {
            if (pgpMessage != null && pgpBuilder == null) {
                 throw new IllegalArgumentException("pgpBuilder is null");
            }

            this.pgpMessage = pgpMessage;
            this.pgpBuilder = pgpBuilder;
            this.signed = signed;
            this.encrypted = encrypted;
        }

        public MimeMessage getPgpMessage() {
            return pgpMessage;
        }

        public PGPInlineMIMEBuilder getPgpBuilder() {
            return pgpBuilder;
        }

        public boolean isSigned() {
            return signed;
        }

        public boolean isEncrypted() {
            return encrypted;
        }
    }

    @Override
    protected void initMailetTransacted()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(userPreferencesPGPSigningKeySelector);
        Objects.requireNonNull(fileExtensionResolver);

        String param = getInitParameter(Parameter.SIGNING_ALGORITHM.name);

        if (param != null)
        {
            signingAlgorithm = Objects.requireNonNull(PGPHashAlgorithm.fromName(param),
                    param + " is not a valid signing algorithm");
        }

        signingAlgorithmAttribute = getInitParameter(Parameter.SIGNING_ALGORITHM_ATTRIBUTE.name);

        param = getInitParameter(Parameter.ENCRYPTION_ALGORITHM.name);

        if (param != null)
        {
            encryptionAlgorithm = Objects.requireNonNull(PGPEncryptionAlgorithm.fromName(param),
                    param + " is not a valid encryption algorithm");
        }

        encryptionAlgorithmAttribute = getInitParameter(Parameter.ENCRYPTION_ALGORITHM_ATTRIBUTE.name);

        param = getInitParameter(Parameter.COMPRESSION_ALGORITHM.name);

        if (param != null)
        {
            compressionAlgorithm = Objects.requireNonNull(PGPCompressionAlgorithm.fromName(param),
                    param + " is not a valid compression algorithm");
        }

        compressionAlgorithmAttribute = getInitParameter(Parameter.COMPRESSION_ALGORITHM_ATTRIBUTE.name);

        convertHTMLToText = getBooleanInitParameter(Parameter.CONVERT_HTML_TO_TEXT.name, convertHTMLToText);
        convertHTMLToTextAttribute = getInitParameter(Parameter.CONVERT_HTML_TO_TEXT_ATTRIBUTE.name);

        addIntegrityPacket = getBooleanInitParameter(Parameter.ADD_INTEGRITY_PACKET.name, addIntegrityPacket);
        addIntegrityPacketAttribute = getInitParameter(Parameter.ADD_INTEGRITY_PACKET_ATTRIBUTE.name);

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);

        signedProcessor = getInitParameter(Parameter.SIGNED_PROCESSOR.name);

        encryptedProcessor = getInitParameter(Parameter.ENCRYPTED_PROCESSOR.name);

        maxMimeDepth = getIntegerInitParameter(Parameter.MAX_MIME_DEPTH.name, maxMimeDepth);

        StrBuilder sb = new StrBuilder();

        sb.append("signingAlgorithm: ");
        sb.append(signingAlgorithm);
        sb.append("; ");
        sb.append("signingAlgorithmAttribute: ");
        sb.append(signingAlgorithmAttribute);
        sb.append("; ");
        sb.append("encryptionAlgorithm: ");
        sb.append(encryptionAlgorithm);
        sb.append("; ");
        sb.append("encryptionAlgorithmAttribute: ");
        sb.append(encryptionAlgorithmAttribute);
        sb.append("; ");
        sb.append("compressionAlgorithm: ");
        sb.append(compressionAlgorithm);
        sb.append("; ");
        sb.append("compressionAlgorithmAttribute: ");
        sb.append(compressionAlgorithmAttribute);
        sb.append("; ");
        sb.append("convertHTMLToText: ");
        sb.append(convertHTMLToText);
        sb.append("; ");
        sb.append("convertHTMLToTextAttribute: ");
        sb.append(convertHTMLToTextAttribute);
        sb.append("; ");
        sb.append("addIntegrityPacket: ");
        sb.append(addIntegrityPacket);
        sb.append("; ");
        sb.append("addIntegrityPacketAttribute: ");
        sb.append(addIntegrityPacketAttribute);
        sb.append("; ");
        sb.append("retainMessageID: ");
        sb.append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("Signed processor: ");
        sb.append(signedProcessor);
        sb.appendSeparator("; ");
        sb.append("Encrypted processor: ");
        sb.append(encryptedProcessor);
        sb.appendSeparator("; ");
        sb.append("maxMimeDepth: ");
        sb.append(maxMimeDepth);

        getLogger().info("{}", sb);

        keyConverter = new JcaPGPKeyConverter();

        // Private keys stored on an HSM are directly returned by JcaPGPKeyConverter. JcaPGPKeyConverter is only
        // used for converting soft keys
        keyConverter.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());
    }

    private PGPHashAlgorithm getSigningAlgorithm(Mail mail)
    {
        PGPHashAlgorithm algorithm = null;

        // Check if the Mail attribute contains the algorithm
        if (StringUtils.isNotEmpty(signingAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    signingAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPHashAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", signingAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default signing algorithm
            algorithm = this.signingAlgorithm;
        }

        getLogger().debug("Signing algorithm: {}", algorithm);

        return algorithm;
    }

    private PGPEncryptionAlgorithm getEncryptionAlgorithm(Mail mail)
    {
        PGPEncryptionAlgorithm algorithm = null;

        // Check if the Mail attribute contains the algorithm
        if (StringUtils.isNotEmpty(encryptionAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    encryptionAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPEncryptionAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid encryption algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", encryptionAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default algorithm
            algorithm = this.encryptionAlgorithm;
        }

        getLogger().debug("Encryption algorithm: {}", algorithm);

        return algorithm;
    }

    private PGPCompressionAlgorithm getCompressionAlgorithm(Mail mail)
    {
        PGPCompressionAlgorithm algorithm = null;

        // Check if the Mail attribute contains the algorithm
        if (StringUtils.isNotEmpty(compressionAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    compressionAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPCompressionAlgorithm.fromName(attributeValue.get());

                if (algorithm == null)
                {
                    getLogger().warn("The attribute value {} is not a valid compression algorithm.",
                            attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", compressionAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default algorithm
            algorithm = this.compressionAlgorithm;
        }

        getLogger().debug("Compression algorithm: {}", algorithm);

        return algorithm;
    }

    private boolean getConvertHTMLToText(Mail mail)
    {
        // Get value from the mail attribute
        boolean convert = MailAttributesUtils.attributeAsBoolean(mail, convertHTMLToTextAttribute,
                    this.convertHTMLToText);

        getLogger().debug("convertHTMLToText: {}", convert);

        return convert;
    }

    private boolean getAddIntegrityPacket(Mail mail)
    {
        // Get value from the mail attribute
        boolean addPacket = MailAttributesUtils.attributeAsBoolean(mail, addIntegrityPacketAttribute,
                    this.addIntegrityPacket);

        getLogger().debug("addIntegrityPacket: {}", addPacket);

        return addPacket;
    }

    private PGPKeyRingEntry getSigningKeyEntry(Mail mail)
    throws MessagingException, IOException
    {
        try {
            PGPKeyRingEntry signingKey = null;

            InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

            UserPreferences userPreferences = userWorkflow.getUser(originator.getAddress(),
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getUserPreferences();

            signingKey = userPreferencesPGPSigningKeySelector.select(userPreferences);

            if (signingKey == null) {
                getLogger().debug("No valid PGP signing key found for {}", originator);
            }

            return signingKey;
        }
        catch (HierarchicalPropertiesException | PGPException e) {
            throw new IOException(e);
        }
    }

    private PrivateKey getPrivateKey(PGPPrivateKey pgpPrivateKey)
    throws PGPException
    {
        PrivateKey result = null;

        if (pgpPrivateKey != null) {
            result = keyConverter.getPrivateKey(pgpPrivateKey);
        }

        return result;
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException, IOException
    {
        try {
            boolean sign = false;
            boolean encrypt = false;

            PrivateKey signingKey = null;

            PGPKeyRingEntry signingKeyEntry = getSigningKeyEntry(mail);

            if (signingKeyEntry != null)
            {
                signingKey = getPrivateKey(signingKeyEntry.getPrivateKey());

                if (signingKey != null) {
                    sign = true;
                }
                else {
                    getLogger().warn("PGP signing key is no longer available for key with KeyID {}",
                            PGPUtils.getKeyIDHex(signingKeyEntry.getKeyID()));
                }
            }

            // The PGP public encryption keys should be stored in the Mail attributes
            PGPPublicKeys publicKeysAttribute = CoreApplicationMailAttributes.getPGPPublicKeys(mail);

            List<PGPPublicKey> encryptionKeys = null;

            if (publicKeysAttribute != null)
            {
                encryptionKeys = publicKeysAttribute.getPublicKeys();

                 if (CollectionUtils.isNotEmpty(encryptionKeys)) {
                     encrypt = true;
                 }
            }

            if (sign || encrypt)
            {
                PGPInlineMIMEBuilder builder = new PGPInlineMIMEBuilder(fileExtensionResolver);

                builder.setSign(sign);

                if (sign) {
                    builder.setSigner(signingKeyEntry.getPublicKey(), signingKey);
                }

                builder.setEncrypt(encrypt);

                if (encrypt) {
                    builder.setEncryptionKeys(encryptionKeys);
                }

                builder.setHashAlgorithm(getSigningAlgorithm(mail));
                builder.setEncryptionAlgorithm(getEncryptionAlgorithm(mail));
                builder.setCompressionAlgorithm(getCompressionAlgorithm(mail));
                builder.setConvertHTMLToText(getConvertHTMLToText(mail));
                builder.setAddIntegrityPacket(getAddIntegrityPacket(mail));
                builder.setRetainMessageID(retainMessageID);
                builder.setMaxMimeDepth(maxMimeDepth);

                MimeMessage pgpMessage = builder.build(mail.getMessage());

                // We should postpone replacing the email in the Mail object until after the transaction
                // because the transaction might fail in which case the transaction will be retried. If we
                // replace the email now, the email will be signed/encrypted twice
                getActivationContext().set(ACTIVATION_CONTEXT_KEY, new ActivationContext(pgpMessage, builder,
                        sign, encrypt));
            }
        }
        catch (PGPException e) {
            // Need to throw Exception to make sure that any database transaction is rolled back
            throw new IOException(e);
        }
    }

    @Override
    protected void retryTransaction()
    {
        // Clear activation context
        getActivationContext().set(ACTIVATION_CONTEXT_KEY, null);
    }

    @Override
    protected void serviceMailPostTransaction(Mail mail)
    {
        ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                ActivationContext.class);

        if (activationContext != null)
        {
            MimeMessage pgpMessage = activationContext.getPgpMessage();

            if (pgpMessage != null)
            {
                try {
                    mail.setMessage(pgpMessage);
                }
                catch (MessagingException e) {
                    // Should not happen
                    throw new UnhandledException("Error setting message", e);
                }

                PGPInlineMIMEBuilder pgpBuilder = activationContext.getPgpBuilder();

                if (activationContext.isEncrypted())
                {
                    if (activationContext.isSigned())
                    {
                        getLogger().info("Message was PGP/INLINE signed and encrypted. Hash algorithm: {}; " +
                                "Encryption algorithm: {}; Compression algorithm: {}; Add integrity packet: {}; " +
                                "MailID: {}; Recipients: {}",
                            pgpBuilder.getHashAlgorithm().getFriendlyName(),
                            pgpBuilder.getEncryptionAlgorithm().getFriendlyName(),
                            pgpBuilder.getCompressionAlgorithm().getFriendlyName(),
                            pgpBuilder.isAddIntegrityPacket(),
                            CoreApplicationMailAttributes.getMailID(mail),
                            mail.getRecipients());
                    }
                    else {
                        getLogger().info("Message was PGP/INLINE encrypted. Encryption algorithm: {}; " +
                                "Compression algorithm: {}; Add integrity packet: {}; MailID: {}; Recipients: {}",
                            pgpBuilder.getEncryptionAlgorithm().getFriendlyName(),
                            pgpBuilder.getCompressionAlgorithm().getFriendlyName(),
                            pgpBuilder.isAddIntegrityPacket(),
                            CoreApplicationMailAttributes.getMailID(mail),
                            mail.getRecipients());
                    }

                    if (StringUtils.isNotEmpty(encryptedProcessor)) {
                        mail.setState(encryptedProcessor);
                    }
                }
                else {
                    getLogger().info("Message was PGP/INLINE signed. Hash algorithm: {}, MailID: {}",
                            pgpBuilder.getHashAlgorithm().getFriendlyName(),
                            CoreApplicationMailAttributes.getMailID(mail));

                    if (StringUtils.isNotEmpty(signedProcessor)) {
                        mail.setState(signedProcessor);
                    }
                }
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (signedProcessor != null) {
            requiredProcessors.add(new ProcessingState(signedProcessor));
        }

        if (encryptedProcessor != null) {
            requiredProcessors.add(new ProcessingState(encryptedProcessor));
        }

        return requiredProcessors;
    }
}
