/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import org.apache.commons.lang.UnhandledException;
import org.apache.james.core.MailAddress;
import org.slf4j.Logger;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.retry.support.RetryTemplateBuilder;
import org.springframework.transaction.support.TransactionOperations;

import javax.mail.MessagingException;
import java.util.Collection;
import java.util.Objects;

/**
 * Helper class used to get User instances from email addresses within a database transaction.
 */
public class MailAddressHandler
{
    /*
     * Is used for starting database transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * Helper for getting users
     */
    private final UserWorkflow userWorkflow;

    /*
     * The event handler instance to call when a User is found
     */
    private final HandleUserEventHandler eventHandler;

    /*
     * The logger of the class that "owns" this MailAddressHandler
     */
    private final Logger logger;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener;

    /*
     * The event handler interface
     */
    public interface HandleUserEventHandler
    {
        void handleUser(User user)
        throws MessagingException;
    }

    public MailAddressHandler(TransactionOperations transactionOperations, UserWorkflow userWorkflow,
            HandleUserEventHandler eventHandler, Logger logger)
    {
        this.transactionOperations = Objects.requireNonNull(transactionOperations);
        this.userWorkflow = Objects.requireNonNull(userWorkflow);
        this.eventHandler = Objects.requireNonNull(eventHandler);
        this.logger = Objects.requireNonNull(logger);
        this.loggingRetryListener = new LoggingRetryListener(logger);
    }

    public void handleMailAddressesWithRetry(Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        handleMailAddressesWithRetry(mailAddresses, null, null);
    }

    public void handleMailAddressesWithRetry(Collection<MailAddress> mailAddresses, RetryListener retryListener)
    throws MessagingException
    {
        handleMailAddressesWithRetry(mailAddresses, retryListener, null);
    }

    public void handleMailAddressesWithRetry(Collection<MailAddress> mailAddresses, RetryListener retryListener,
            Integer maxRetryAttempts)
    throws MessagingException
    {
        RetryTemplateBuilder retryTemplateBuilder = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder(
                    maxRetryAttempts)
                .withListener(loggingRetryListener);

        if (retryListener != null) {
            retryTemplateBuilder.withListener(retryListener);
        }

        RetryTemplate retryTemplate = retryTemplateBuilder.build();

        retryTemplate.execute(ctx ->
        {
            handleMailAddresses(mailAddresses);

            return null;
        });
    }

    public void handleMailAddresses(Collection<MailAddress> mailAddresses)
    throws MessagingException
    {
        if (mailAddresses == null) {
            return;
        }

        try {
            transactionOperations.executeWithoutResult(status ->
            {
                try {
                    for (MailAddress mailAddress : mailAddresses)
                    {
                        if (mailAddress != null)
                        {
                            // We will only accept valid email addresses. If an email address is invalid it will be
                            // converted to EmailAddressUtils.INVALID_EMAIL
                            String validatedEmail = EmailAddressUtils.canonicalizeAndValidate(mailAddress.toString(),
                                    false);

                            if (validatedEmail != null)
                            {
                                // Call the event handler with the found user
                                eventHandler.handleUser(userWorkflow.getUser(validatedEmail,
                                        UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST));
                            }
                            else {
                                logger.debug("{} is not a valid email address.", mailAddress);
                            }
                        }
                    }
                }
                catch (HierarchicalPropertiesException | MessagingException e) {
                    throw new UnhandledException(e);
                }
            });
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof MessagingException messagingException) {
                throw messagingException;
            }
            else if (cause instanceof HierarchicalPropertiesException hierarchicalPropertiesException) {
                throw new MessagingException("Error handling mail addresses", hierarchicalPropertiesException);
            }
            else {
                throw e;
            }
        }
    }
}
