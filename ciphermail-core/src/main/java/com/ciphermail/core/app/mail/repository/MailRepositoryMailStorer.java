/*
 * Copyright (c) 2010-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.mail.repository;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.mail.MailSerializer;
import com.ciphermail.core.common.mail.repository.MailRepository;
import com.ciphermail.core.common.mail.repository.MailRepositoryItem;
import org.apache.mailet.Mail;

import javax.annotation.Nonnull;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Objects;

/**
 * MailStorer implementation that stores the mail in a MailRepository.
 *
 * @author Martijn Brinkers
 *
 */
public class MailRepositoryMailStorer implements MailStorer
{
    /*
     * The mail repository to store the mail into
     */
    private final MailRepository repository;

    /*
     * Used to identify the 'sender'. The default MessageOriginatorIdentifier uses the from as
     * the identifier.
     */
    private final MessageOriginatorIdentifier originatorIdentifier;

    public MailRepositoryMailStorer(@Nonnull MailRepository repository,
            @Nonnull MessageOriginatorIdentifier originatorIdentifier)
    {
        this.repository = Objects.requireNonNull(repository);
        this.originatorIdentifier = Objects.requireNonNull(originatorIdentifier);
    }

    @Override
    public void store(@Nonnull Mail mail)
    throws MessagingException, IOException
    {
        MailRepositoryItem item = repository.createItem(mail.getMessage());

        item.setRecipients(MailAddressUtils.toInternetAddressList(MailAddressUtils.getRecipients(mail)));
        item.setRemoteAddress(mail.getRemoteAddr());
        item.setSender(MailAddressUtils.toInternetAddress(mail.getMaybeSender().asOptional().orElse(null)));
        item.setOriginator(originatorIdentifier.getOriginator(mail));

        // Store the Mail object in the additional data blob so the Mail item can be 'resurrected'
        // when it is released from the store (for example the message is release from quarantine)
        item.setAdditionalData(new MailSerializer().serialize(mail));

        repository.addItem(item);

        // Store the ID of the MailRepositoryItem in the Mail object (this is required for example when
        // a link to the stored email should be generated).
        CoreApplicationMailAttributes.setMailRepositoryID(mail, item.getID());
    }
}
