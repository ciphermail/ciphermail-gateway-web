/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.OTPPasswordContainer;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.properties.PDFProperties;
import com.ciphermail.core.app.properties.PDFPropertiesImpl;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.otp.ClientSecretIdGenerator;
import com.ciphermail.core.common.security.otp.OTPGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Objects;

/**
 * Mailet that generates OTP passwords for all recipients.
 */
@SuppressWarnings({"java:S6813"})
public class OTPPasswordGenerator extends AbstractGeneratePassword
{
    private static final Logger logger = LoggerFactory.getLogger(OTPPasswordGenerator.class);

    /*
     * Generator used for generating the passwords.
     */
    @Inject
    private OTPGenerator otpGenerator;

    /*
     * Generator used for generating the alient ID which identifies which client secret was used
     */
    @Inject
    private ClientSecretIdGenerator clientSecretIdGenerator;

    /*
     * Provides factory classes which will be used to create instances of user property objects
     */
    @Inject
    private UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(otpGenerator);
        Objects.requireNonNull(clientSecretIdGenerator);
    }

    @Override
    protected PasswordContainer createPasswordContainer() {
        return new OTPPasswordContainer();
    }

    @Override
    protected void generatePassword(@Nonnull User user, @Nonnull PasswordContainer passwordContainer)
    throws MessagingException
    {
        OTPPasswordContainer otpPasswordContainer = (OTPPasswordContainer) passwordContainer;

        try {
            PDFProperties pdfProperties = userPropertiesFactoryRegistry.getFactoryForClass(PDFPropertiesImpl.class)
                    .createInstance(user.getUserPreferences().getProperties());

            String otpSecretKey = pdfProperties.getOTPSecretKey();

            if (otpSecretKey == null) {
                // should never happen
                throw new MessagingException("No OTP secret key found for user " + user.getEmail());
            }

            byte[] otpSecretKeyBytes = otpSecretKey.getBytes(StandardCharsets.UTF_8);

            otpPasswordContainer.setPassword(otpGenerator.generate(otpSecretKeyBytes,
                    otpPasswordContainer.getPasswordID().getBytes(StandardCharsets.UTF_8),
                    otpPasswordContainer.getPasswordLength()));

            // Generate the Client Secret Identifier using a known value. This can be used to detect which
            // client secret should be used to re-generate the password (for example, using the mobile app)
            otpPasswordContainer.setClientSecretID(clientSecretIdGenerator.generateClientSecretID(otpSecretKeyBytes));
        }
        catch (HierarchicalPropertiesException | GeneralSecurityException | IOException e) {
            throw new MessagingException("Error generating OTP password", e);
        }
    }
}
