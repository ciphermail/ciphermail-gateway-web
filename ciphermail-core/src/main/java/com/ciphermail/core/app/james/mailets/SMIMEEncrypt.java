/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.security.smime.SMIMEAuthBuilderImpl;
import com.ciphermail.core.common.security.smime.SMIMEBuilder;
import com.ciphermail.core.common.security.smime.SMIMEBuilderException;
import com.ciphermail.core.common.security.smime.SMIMEBuilderImpl;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionAlgorithm;
import com.ciphermail.core.common.security.smime.SMIMEEncryptionScheme;
import com.ciphermail.core.common.security.smime.SMIMERecipientMode;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * S/MIME encrypts the message using the certificates from the mail attributes (See
 * {@link CoreApplicationMailAttributes#getCertificates(Mail)})
 * If there are no certificates the mail will not be encrypted.
 */
public class SMIMEEncrypt extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMEEncrypt.class);

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ALGORITHM                    ("algorithm"),
        ALGORITHM_ATTRIBUTE          ("algorithmAttribute"),
        ENCRYPTION_SCHEME            ("encryptionScheme"),
        ENCRYPTION_SCHEME_ATTRIBUTE  ("encryptionSchemeAttribute"),
        KEY_SIZE                     ("keySize"),
        RECIPIENT_MODE               ("recipientMode"),
        USE_DEPRECATED_CONTENT_TYPES ("useDeprecatedContentTypes"),
        PROTECTED_HEADER             ("protectedHeader"),
        RETAIN_MESSAGE_ID            ("retainMessageID"),
        ENCRYPTED_PROCESSOR          ("encryptedProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * How should the certificate be identified in the encrypted blob
     */
    private SMIMERecipientMode recipientMode = SMIMERecipientMode.ISSUER_SERIAL;

    /*
     * The encryption algorithm
     */
    private SMIMEEncryptionAlgorithm encryptionAlgorithm = SMIMEEncryptionAlgorithm.AES128_CBC;

    /*
     * The Mail attribute from which the encryption algorithm is read. If null or if there is no
     * attribute with the given name, the encryptionAlgorithm is used.
     */
    private String encryptionAlgorithmAttribute;

    /*
     * The encryption scheme
     */
    private SMIMEEncryptionScheme encryptionScheme = SMIMEEncryptionScheme.RSAES_PKCS1_V1_5;

    /*
     * The Mail attribute from which the encryption scheme is read. If null or if there is no
     * attribute with the given name, the encryptionScheme is used.
     */
    private String encryptionSchemeAttribute;

    /*
     * Key size to use. Only applicable when the algorithm supports varaiable key size
     */
    private Integer keySize;

    /*
     * If true the old x-pkcs7-* content types will be used
     */
    private boolean useDeprecatedContentTypes;

    /*
     * If true the original Message-ID will be used for the encrypted message
     */
    private boolean retainMessageID = true;

    /*
     * The next processor if encryption succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String encryptedProcessor;

    /*
     * Determines which headers will be encrypted.
     */
    private List<String> protectedHeaders;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private SMIMERecipientMode getRecipientMode() {
        return recipientMode;
    }

    private SMIMEEncryptionAlgorithm getEncryptionAlgorithm(Mail mail)
    {
        SMIMEEncryptionAlgorithm algorithm = null;

        // Check if the Mail attribute contains the encryption algorithm
        if (StringUtils.isNotEmpty(encryptionAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    encryptionAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = SMIMEEncryptionAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", encryptionAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default encryption algorithm
            algorithm = this.encryptionAlgorithm;
        }

        getLogger().debug("Encryption algorithm: {}", algorithm);

        return algorithm;
    }

    private SMIMEEncryptionScheme getEncryptionScheme(Mail mail)
    {
        SMIMEEncryptionScheme scheme = null;

        // Check if the Mail attribute contains the encryption scheme
        if (StringUtils.isNotEmpty(encryptionSchemeAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    encryptionSchemeAttribute, String.class);

            if (attributeValue.isPresent())
            {
                scheme = SMIMEEncryptionScheme.fromName(attributeValue.get());

                if (scheme == null) {
                    getLogger().warn("The attribute value {} is not a valid encryption scheme.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", encryptionSchemeAttribute);
            }
        }

        if (scheme == null) {
            // Use the default encryption scheme
            scheme = this.encryptionScheme;
        }

        getLogger().debug("Encryption scheme: {}", scheme);

        return scheme;
    }

    private void initProtectedHeaders()
    {
        String param = getInitParameter(Parameter.PROTECTED_HEADER.name);

        protectedHeaders = param != null ? List.of(param.split("\\s*,\\s*")) : Collections.emptyList();
    }

    @Override
    public final void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        // Initialize encryption algorithm
        String param = getInitParameter(Parameter.ALGORITHM.name);

        if (param != null)
        {
            encryptionAlgorithm = Objects.requireNonNull(SMIMEEncryptionAlgorithm.fromName(param),
                    param + " is not a valid encryption algorithm");
        }

        encryptionAlgorithmAttribute = getInitParameter(Parameter.ALGORITHM_ATTRIBUTE.name);

        // Initialize encryption scheme
        param = getInitParameter(Parameter.ENCRYPTION_SCHEME.name);

        if (param != null)
        {
            encryptionScheme = Objects.requireNonNull(SMIMEEncryptionScheme.fromName(param),
                    param + " is not a valid encryption scheme");
        }

        encryptionSchemeAttribute = getInitParameter(Parameter.ENCRYPTION_SCHEME_ATTRIBUTE.name);

        // Initialize key size
        param = getInitParameter(Parameter.KEY_SIZE.name);

        if (param != null)
        {
            keySize = Integer.valueOf(param);

            if (keySize <= 0) {
                throw new IllegalArgumentException(Parameter.KEY_SIZE.name + " must be > 0");
            }
        }

        // Initialize recipient mode
        param = getInitParameter(Parameter.RECIPIENT_MODE.name);

        if (param != null)
        {
            recipientMode = Objects.requireNonNull(SMIMERecipientMode.fromName(param),
                param + " is not a valid SMIMERecipientMode");
        }

        // Initialize use deprecated content-type's
        param = getInitParameter(Parameter.USE_DEPRECATED_CONTENT_TYPES.name);

        if (param != null) {
            useDeprecatedContentTypes = BooleanUtils.toBoolean(param);
        }

        // Initialize retain message ID
        param = getInitParameter(Parameter.RETAIN_MESSAGE_ID.name);

        if (param != null) {
            retainMessageID = BooleanUtils.toBoolean(param);
        }

        encryptedProcessor = getInitParameter(Parameter.ENCRYPTED_PROCESSOR.name);

        initProtectedHeaders();

        StrBuilder sb = new StrBuilder();

        sb.append("Algorithm: ");
        sb.append(encryptionAlgorithm);
        sb.appendSeparator("; ");
        sb.append("Algorithm attribute: ");
        sb.append(encryptionAlgorithmAttribute);
        sb.appendSeparator("; ");
        sb.append("Scheme: ");
        sb.append(encryptionScheme);
        sb.appendSeparator("; ");
        sb.append("Scheme attribute: ");
        sb.append(encryptionSchemeAttribute);
        sb.appendSeparator("; ");
        sb.append("KeySize: ");
        sb.append(keySize);
        sb.appendSeparator("; ");
        sb.append("Recipient Mode: ");
        sb.append(recipientMode);
        sb.appendSeparator("; ");
        sb.append("Use deprecated content-type's: ");
        sb.append(useDeprecatedContentTypes);
        sb.appendSeparator("; ");
        sb.append("Retain Message-ID: ");
        sb.append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("Encrypted processor: ");
        sb.append(encryptedProcessor);
        sb.appendSeparator("; ");
        sb.append("Protected headers: ");
        sb.append(StringUtils.join(protectedHeaders, ","));

        getLogger().info("{}", sb);
    }

    private List<String> getProtectedHeaders(Mail mail)
    {
        // Get the protected headers from mail if available, if not available use the protected headers
        List<String> headers = CoreApplicationMailAttributes.getProtectedHeaders(mail);

        if (headers.isEmpty()) {
            headers = this.protectedHeaders;
        }

        if (getLogger().isDebugEnabled()) {
            getLogger().debug("Protected headers: {}", StringUtils.join(headers, ", "));
        }

        return headers;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            Certificates certificateStore = CoreApplicationMailAttributes.getCertificates(mail);

            if (certificateStore != null)
            {
                Collection<X509Certificate> certificates = certificateStore.getCertificates();

                if (!certificates.isEmpty())
                {
                    MimeMessage message = mail.getMessage();

                    if (message != null)
                    {
                        SMIMEEncryptionAlgorithm algorithm = getEncryptionAlgorithm(mail);

                        SMIMEBuilder sMIMEBuilder = algorithm.isAuth() ?
                                new SMIMEAuthBuilderImpl(message, getProtectedHeaders(mail).toArray(new String[]{})) :
                                new SMIMEBuilderImpl(message, getProtectedHeaders(mail).toArray(new String[]{}));

                        sMIMEBuilder.setUseDeprecatedContentTypes(useDeprecatedContentTypes);

                        SMIMEEncryptionScheme scheme = getEncryptionScheme(mail);

                        for (X509Certificate certificate : certificates) {
                            sMIMEBuilder.addRecipient(certificate, getRecipientMode(), scheme);
                        }

                        int localKeySize = (keySize != null ? keySize : algorithm.defaultKeySize());

                        sMIMEBuilder.encrypt(algorithm, localKeySize);

                        MimeMessage encrypted = sMIMEBuilder.buildMessage();

                        if (encrypted != null)
                        {
                            encrypted.saveChanges();

                            getLogger().atInfo().log("Message was S/MIME encrypted. Encryption algorithm: {}; Key size: {}; " +
                                    "Encryption Scheme: {}; MailID: {}; Recipients: {}",
                                    algorithm,
                                    localKeySize,
                                    scheme,
                                    CoreApplicationMailAttributes.getMailID(mail),
                                    mail.getRecipients());

                            // A new MimeMessage instance will be created. This makes ure that the
                            // MimeMessage can be written by James (using writeTo()). The message-ID
                            // of the source message will be retained if told to do so
                            encrypted = retainMessageID ? new MimeMessageWithID(encrypted, message.getMessageID()) :
                                new MimeMessage(encrypted);

                            mail.setMessage(encrypted);

                            if (StringUtils.isNotEmpty(encryptedProcessor)) {
                                mail.setState(encryptedProcessor);
                            }
                        }
                    }
                    else {
                        getLogger().warn("Message is null.");
                    }
                }
                else {
                    getLogger().warn("Certificate collection is empty.");
                }
            }
            else {
                getLogger().warn("Certificates attribute not found.");
            }
        }
        catch(SMIMEBuilderException e) {
            getLogger().error("Error encrypting the message.", e);
        }
        catch(MessagingException e) {
            getLogger().error("Error reading the message.", e);
        }
        catch (IOException e) {
            getLogger().error("IOException.", e);
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (encryptedProcessor != null) {
            requiredProcessors.add(new ProcessingState(encryptedProcessor));
        }

        return requiredProcessors;
    }
}
