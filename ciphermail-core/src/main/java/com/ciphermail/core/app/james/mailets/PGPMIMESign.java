/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPDocumentType;
import com.ciphermail.core.common.security.openpgp.PGPHashAlgorithm;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPMIMESignatureBuilder;
import com.ciphermail.core.common.security.openpgp.PGPSecurityFactoryFactory;
import com.ciphermail.core.common.security.openpgp.PGPUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPKeyConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Mailet which signs the email with PGP/MIME
 */
@SuppressWarnings({"java:S6813"})
public class PGPMIMESign extends AbstractTransactedMailet
{
    private static final Logger logger = LoggerFactory.getLogger(PGPMIMESign.class);

    private static final String ACTIVATION_CONTEXT_KEY = PGPMIMESign.class.getName();

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SIGNING_ALGORITHM           ("signingAlgorithm"),
        SIGNING_ALGORITHM_ATTRIBUTE ("signingAlgorithmAttribute"),
        SIGNATURE_TYPE              ("signatureType"),
        RETAIN_MESSAGE_ID           ("retainMessageID"),
        SIGNED_PROCESSOR            ("signedProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The hash algorithm used for the signature.
     */
    private PGPHashAlgorithm signingAlgorithm = PGPHashAlgorithm.SHA256;

    /*
     * The Mail attribute from which the signing algorithm is read. If null or if there is no
     * attribute with the given name, the signingAlgorithm is used.
     */
    private String signingAlgorithmAttribute;

    /*
     * The signature type to use (Binary or text)
     */
    private PGPDocumentType signatureType = PGPDocumentType.TEXT;

    /*
     * If true the original Message-ID will be used for the signed message
     */
    private boolean retainMessageID = true;

    /*
     * The next processor if signing succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String signedProcessor;

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Identifies the 'sender' of the message (default version uses from as the identifier)
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Retrieves the PGP signing key for a user preferences instance
     */
    @Inject
    private UserPreferencesPGPSigningKeySelector userPreferencesPGPSigningKeySelector;

    /*
     * For converting a PGPPrivate key to a PrivateKey
     */
    private JcaPGPKeyConverter keyConverter;

    /*
     * Context class for storing data in the activation context
     */
    private static class ActivationContext
    {
        /*
         * The PGP/MIME signed message. Can be null
         */
        private final MimeMessage signedMessage;

        /*
         * Must be set if signedMessage is not null
         */
        private final PGPMIMESignatureBuilder signatureBuilder;

        ActivationContext(MimeMessage signedMessage, PGPMIMESignatureBuilder signatureBuilder)
        {
            if (signedMessage != null) {
                Objects.requireNonNull(signatureBuilder);
            }

            this.signedMessage = signedMessage;
            this.signatureBuilder = signatureBuilder;
        }

        public MimeMessage getSignedMessage() {
            return signedMessage;
        }

        public PGPMIMESignatureBuilder getSignatureBuilder() {
            return signatureBuilder;
        }
    }

    @Override
    protected void initMailetTransacted()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(userPreferencesPGPSigningKeySelector);

        String param = getInitParameter(Parameter.SIGNING_ALGORITHM.name);

        if (param != null)
        {
            signingAlgorithm = Objects.requireNonNull(PGPHashAlgorithm.fromName(param),
                    param + " is not a valid signing algorithm");
        }

        signingAlgorithmAttribute = getInitParameter(Parameter.SIGNING_ALGORITHM_ATTRIBUTE.name);

        param = getInitParameter(Parameter.SIGNATURE_TYPE.name);

        if (param != null)
        {
            signatureType = Objects.requireNonNull(PGPDocumentType.fromName(param),
                    param + " is not a valid signature type");
        }

        retainMessageID = getBooleanInitParameter(Parameter.RETAIN_MESSAGE_ID.name, retainMessageID);

        signedProcessor = getInitParameter(Parameter.SIGNED_PROCESSOR.name);

        StrBuilder sb = new StrBuilder();

        sb.append("signingAlgorithm: ");
        sb.append(signingAlgorithm);
        sb.append("; ");
        sb.append("signingAlgorithmAttribute: ");
        sb.append(signingAlgorithmAttribute);
        sb.append("; ");
        sb.append("retainMessageID: ");
        sb.append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("Signed processor: ");
        sb.append(signedProcessor);

        getLogger().info("{}", sb);

        keyConverter = new JcaPGPKeyConverter();

        // Private keys stored on an HSM are directly returned by JcaPGPKeyConverter. JcaPGPKeyConverter is only
        // used for converting soft keys
        keyConverter.setProvider(PGPSecurityFactoryFactory.getSecurityFactory().getNonSensitiveProvider());
    }

    private PGPHashAlgorithm getSigningAlgorithm(Mail mail)
    {
        PGPHashAlgorithm algorithm = null;

        // Check if the Mail attribute contains the signing algorithm
        if (StringUtils.isNotEmpty(signingAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    signingAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = PGPHashAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", signingAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default signing algorithm
            algorithm = this.signingAlgorithm;
        }

        getLogger().debug("Signing algorithm: {}", algorithm);

        return algorithm;
    }

    private PrivateKey getPrivateKey(PGPPrivateKey pgpPrivateKey)
    throws PGPException
    {
        PrivateKey result = null;

        if (pgpPrivateKey != null) {
            result = keyConverter.getPrivateKey(pgpPrivateKey);
        }

        return result;
    }

    @Override
    protected void serviceMailTransacted(Mail mail)
    throws MessagingException, IOException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        try {
            UserPreferences userPreferences = userWorkflow.getUser(originator.getAddress(),
                    UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST).getUserPreferences();

            PGPKeyRingEntry entry = userPreferencesPGPSigningKeySelector.select(userPreferences);

            if (entry != null)
            {
                PrivateKey privateKey = getPrivateKey(entry.getPrivateKey());

                if (privateKey != null)
                {
                    PGPMIMESignatureBuilder signatureBuilder = new PGPMIMESignatureBuilder();

                    signatureBuilder.setHashAlgorithm(getSigningAlgorithm(mail));
                    signatureBuilder.setSignatureType(signatureType);
                    signatureBuilder.setRetainMessageId(retainMessageID);

                    MimeMessage signedMessage = signatureBuilder.sign(mail.getMessage(), entry.getPublicKey(),
                            privateKey);

                    if (signedMessage != null)
                    {
                        // We should postpone replacing the email in the Mail object until after the transaction
                        // because the transaction might fail in which case the transaction will be retried. If we
                        // replace the email now, the email will be signed twice
                        getActivationContext().set(ACTIVATION_CONTEXT_KEY, new ActivationContext(signedMessage,
                                signatureBuilder));
                    }
                }
                else {
                    getLogger().atWarn().log("PGP signing key is no longer available for key with KeyID {}",
                            PGPUtils.getKeyIDHex(entry.getKeyID()));
                }
            }
            else {
                getLogger().debug("No valid PGP signing key entry found for {}", originator);
            }
        }
        catch (HierarchicalPropertiesException | PGPException e) {
            // Need to throw Exception to make sure that any database transaction is rolled back
            throw new IOException(e);
        }
    }

    @Override
    protected void retryTransaction()
    {
        // Clear activation context
        getActivationContext().set(ACTIVATION_CONTEXT_KEY, null);
    }

    @Override
    protected void serviceMailPostTransaction(Mail mail)
    {
        ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                ActivationContext.class);

        if (activationContext != null)
        {
            MimeMessage signedMessage = activationContext.getSignedMessage();

            if (signedMessage != null)
            {
                if (MailUtils.isValidMessage(signedMessage))
                {
                    getLogger().atInfo().log("Message was PGP/MIME signed. Hash algorithm: {}; MailID: {}; " +
                            "Recipients: {}",
                            activationContext.getSignatureBuilder().getHashAlgorithm().getFriendlyName(),
                            CoreApplicationMailAttributes.getMailID(mail),
                            mail.getRecipients());

                    try {
                        mail.setMessage(signedMessage);
                    }
                    catch (MessagingException e) {
                        // Should not happen
                        throw new UnhandledException("Error setting message", e);
                    }

                    if (StringUtils.isNotEmpty(signedProcessor)) {
                        mail.setState(signedProcessor);
                    }
                }
                else {
                    getLogger().atWarn().log("The message is not a valid MIME message. The message can " +
                        "therefore not be PGP/MIME signed. MailID: {}; Recipients: {}",
                        CoreApplicationMailAttributes.getMailID(mail),
                        mail.getRecipients());
                }
            }
        }
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (signedProcessor != null) {
            requiredProcessors.add(new ProcessingState(signedProcessor));
        }

        return requiredProcessors;
    }
}
