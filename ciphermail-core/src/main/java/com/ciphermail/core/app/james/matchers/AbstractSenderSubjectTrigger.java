/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * A Matcher that matches the subject against an expression. The matching parameters (the reg ex pattern,
 * if the matching pattern should be removed from the subject) are taken from the senders
 * user properties. This abstract method getTriggerDetails() should be implemented to return the
 * trigger details.
 * <p>
 * Usage:
 * <p>
 * SenderSubjectTrigger=matchOnError=false
 * <p>
 * If matchOnError is true, this matcher matches when an exception has been thrown by the matcher and
 * vice versa.
 */
@SuppressWarnings({"java:S6813"})
public abstract class AbstractSenderSubjectTrigger extends SubjectTrigger
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractSenderSubjectTrigger.class);

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * The key under which we store the activation context
     */
    private static final String ACTIVATION_CONTEXT_KEY = "triggerDetails";

    @Override
    protected Logger getLogger() {
        return logger;
    }

    protected static class TriggerDetails
    {
        private final String subjectTriggerRegEx;
        private final boolean removePattern;
        private final boolean enabled;

        protected TriggerDetails(String subjectTriggerRegEx, boolean removePattern, boolean enabled)
        {
            this.subjectTriggerRegEx = subjectTriggerRegEx;
            this.removePattern = removePattern;
            this.enabled = enabled;
        }

        String getSubjectTriggerRegEx() {
            return subjectTriggerRegEx;
        }

        boolean isRemovePattern() {
            return removePattern;
        }

        public boolean isEnabled() {
            return enabled;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }

    /**
     * Override to provide the TriggerDetails for the user
     */
    protected abstract TriggerDetails getTriggerDetails(@Nonnull User user)
    throws HierarchicalPropertiesException;

    private TriggerDetails loadTriggerDetailsTransacted(Mail mail)
    throws MessagingException, HierarchicalPropertiesException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        TriggerDetails triggerDetails = null;

        String userEmail = originator.getAddress();

        userEmail = EmailAddressUtils.canonicalizeAndValidate(userEmail, false);

        User user = userWorkflow.getUser(userEmail, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

        triggerDetails = getTriggerDetails(user);

        getLogger().debug("Trigger details: {}", triggerDetails);

        return triggerDetails;
    }

    @Override
    public void init()
    {
        // Note: super.init() is not called on purpose. super.init() reads the pattern from config.xml while this
        // matcher gets the pattern from the class that extends this class
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);

        // We must call getCondition even if the condition is not used otherwise matchOnError will not be initialized
        getCondition();
    }

    @Override
    protected Collection<MailAddress> onMatch(Mail mail, Matcher matcher)
    throws MessagingException, IOException
    {
        TriggerDetails triggerDetails = getActivationContext().get(ACTIVATION_CONTEXT_KEY, TriggerDetails.class);

        if (triggerDetails.isRemovePattern())
        {
            // Remove the pattern from the subject.
            removePattern(mail, matcher);
        }

        return MailAddressUtils.getRecipients(mail);
    }

    /*
     * Override to add functionality
     */
    protected void logOnDisabled(Mail mail) {
        getLogger().debug("#logOnDisabled");
    }

    @Override
    protected Pattern getHeaderValuePattern(Mail mail)
    throws MessagingException
    {
        try {
            Pattern pattern = null;

            TriggerDetails triggerDetails = transactionOperations.execute(status ->
            {
                try {
                    return loadTriggerDetailsTransacted(mail);
                }
                catch (MessagingException | HierarchicalPropertiesException e) {
                    throw new UnhandledException(e);
                }
            });

            String trigger = null;

            if (triggerDetails != null) {
                trigger = triggerDetails.getSubjectTriggerRegEx();
            }

            // We only want to log the disabled log line if a trigger was set and enabled is false. The reason for this
            // is that by default a trigger is not set and we do not want to clutter the logs.
            if (StringUtils.isNotBlank(trigger))
            {
                if (triggerDetails.isEnabled())
                {
                    try {
                        pattern = Pattern.compile(trigger);
                    }
                    catch(PatternSyntaxException e) {
                        getLogger().warn("The trigger '{}' is not a valid pattern. Matcher will not match.", trigger );
                    }
                }
                else {
                    logOnDisabled(mail);
                }
            }

            // Place the triggerDetails in the context so we can use it in onMatch
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, triggerDetails);

            return pattern;
        }
        catch (UnhandledException e) {
            throw new MessagingException("Error reading user properties.", e);
        }
    }
}
