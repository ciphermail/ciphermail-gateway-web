/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.dlp.PolicyPatternNodeUtils;
import com.ciphermail.core.app.dlp.UserPreferencesPolicyPatternManager;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.dlp.PolicyPattern;
import com.google.common.annotations.VisibleForTesting;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.Collection;
import java.util.Objects;

/**
 * Mailet that checks whether the message violates some regular expression policies. The policies are
 * retrieved from the sending users preferences.
 */
@SuppressWarnings({"java:S6813"})
public class SenderRegExpPolicyChecker extends AbstractRegExpPolicyChecker
{
    private static final Logger logger = LoggerFactory.getLogger(SenderRegExpPolicyChecker.class);

    /*
     * Activation context key
     */
    private static final String ACTIVATION_CONTEXT_KEY = "context";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Is used to identify the 'sender' (from or enveloped sender or...) of the message
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Used for getting the policy patterns from a user
     */
    @Inject
    private UserPreferencesPolicyPatternManager policyManager;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(policyManager);
    }

    /*
     * Gets called by MailAddressHandler#handleMailAddresses to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(User user)
    {
        getActivationContext().set(ACTIVATION_CONTEXT_KEY,
                PolicyPatternNodeUtils.getPolicyPatterns(policyManager.getPatterns(user.getUserPreferences())));
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Collection<PolicyPattern> getPolicyPatterns(Mail mail)
    throws MessagingException
    {
        InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

        MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                this::onHandleUserEvent, getLogger());

        mailAddressHandler.handleMailAddressesWithRetry(MailAddressUtils.fromAddressArrayToMailAddressList(originator));

        return getActivationContext().get(ACTIVATION_CONTEXT_KEY, Collection.class);
    }
}
