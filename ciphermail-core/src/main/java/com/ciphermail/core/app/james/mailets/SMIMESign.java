/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.mail.MimeMessageWithID;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.smime.MicAlgStyle;
import com.ciphermail.core.common.security.smime.SMIMEBuilder;
import com.ciphermail.core.common.security.smime.SMIMEBuilderException;
import com.ciphermail.core.common.security.smime.SMIMEBuilderImpl;
import com.ciphermail.core.common.security.smime.SMIMESignMode;
import com.ciphermail.core.common.security.smime.SMIMESigningAlgorithm;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Mailet that digitally signs the email using S/MIME (if the sender has a valid signing key).
 */
@SuppressWarnings({"java:S6813"})
public class SMIMESign extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SMIMESign.class);

    /*
     * The digest (hash) algorithm used for the signature.
     */
    private SMIMESigningAlgorithm signingAlgorithm = SMIMESigningAlgorithm.SHA256_WITH_RSA;

    /*
     * The Mail attribute from which the signing algorithm is read. If null or if there is no
     * attribute with the given name, the signingAlgorithm is used.
     */
    private String signingAlgorithmAttribute;

    /*
     * A message can be clear or opaque signed.
     */
    private SMIMESignMode signMode = SMIMESignMode.CLEAR;

    /*
     * Helper for database transactions
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Identifies the 'sender' of the message (default version uses from as the identifier)
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Used to add/get users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used for building a certificate path for the signing certificate
     */
    @Inject
    private CertificatePathBuilderFactory certificatePathBuilderFactory;

    /*
     * If true the old x-pkcs7-* content types will be used
     */
    private boolean useDeprecatedContentTypes;

    /*
     * The micalg version to use for S/MIME signed messages (RFC 3851, RFC 5751)
     */
    private MicAlgStyle micAlgStyle = MicAlgStyle.RFC5751;

    /*
     * Determines which headers will be signed.
     */
    private String[] protectedHeaders = new String[]{};

    /*
     * If true the root certificate will be added to the signed blob
     */
    private boolean addRoot = true;

    /*
     * If true the original Message-ID will be used for the signed message
     */
    private boolean retainMessageID = true;

    /*
     * The next processor if signing succeeded. If null, the default next processor will be used (i.e,
     * the current configured processor will be used)
     */
    private String signedProcessor;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener = new LoggingRetryListener(getLogger());

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        ALGORITHM                    ("algorithm"),
        ALGORITHM_ATTRIBUTE          ("algorithmAttribute"),
        SIGN_MODE                    ("signMode"),
        MIC_ALG_STYLE                ("micAlgStyle"),
        USE_DEPRECATED_CONTENT_TYPES ("useDeprecatedContentTypes"),
        PROTECTED_HEADER             ("protectedHeader"),
        ADD_ROOT                     ("addRoot"),
        RETAIN_MESSAGE_ID            ("retainMessageID"),
        SIGNED_PROCESSOR             ("signedProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private void initProtectedHeaders()
    {
        String param = getInitParameter(Parameter.PROTECTED_HEADER.name);

        if (param != null) {
            protectedHeaders = param.split("\\s*,\\s*");
        }
    }

    @Override
    public final void initMailet()
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(certificatePathBuilderFactory);

        String param = getInitParameter(Parameter.ALGORITHM.name);

        if (param != null)
        {
            signingAlgorithm = Objects.requireNonNull(SMIMESigningAlgorithm.fromName(param),
                    param + " is not a valid signing algorithm.");
        }

        signingAlgorithmAttribute = getInitParameter(Parameter.ALGORITHM_ATTRIBUTE.name);

        param = getInitParameter(Parameter.SIGN_MODE.name);

        if (param != null)
        {
            signMode = Objects.requireNonNull(SMIMESignMode.fromName(param),
                    param + " is not a valid SMIME sign mode.");
        }

        param = getInitParameter(Parameter.MIC_ALG_STYLE.name);

        if (param != null)
        {
            micAlgStyle = Objects.requireNonNull(MicAlgStyle.fromName(param),
                    param + " is not a valid MicAlgStyle.");
        }

        param = getInitParameter(Parameter.ADD_ROOT.name);

        if (param != null) {
            addRoot = BooleanUtils.toBoolean(param);
        }

        param = getInitParameter(Parameter.RETAIN_MESSAGE_ID.name);

        if (param != null) {
            retainMessageID = BooleanUtils.toBoolean(param);
        }

        signedProcessor = getInitParameter(Parameter.SIGNED_PROCESSOR.name);

        param = getInitParameter(Parameter.USE_DEPRECATED_CONTENT_TYPES.name);

        if (param != null) {
            useDeprecatedContentTypes = BooleanUtils.toBoolean(param);
        }

        initProtectedHeaders();

        StrBuilder sb = new StrBuilder();

        sb.append("Signing algorithm: ");
        sb.append(signingAlgorithm);
        sb.appendSeparator("; ");
        sb.append("Algorithm attribute: ");
        sb.append(signingAlgorithmAttribute);
        sb.appendSeparator("; ");
        sb.append("Sign mode: ");
        sb.append(signMode);
        sb.appendSeparator("; ");
        sb.append("Add root: ");
        sb.append(addRoot);
        sb.appendSeparator("; ");
        sb.append("Use deprecated content-type's: ");
        sb.append(useDeprecatedContentTypes);
        sb.appendSeparator("; ");
        sb.append("Retain Message-ID: ");
        sb.append(retainMessageID);
        sb.appendSeparator("; ");
        sb.append("Signed processor: ");
        sb.append(signedProcessor);
        sb.appendSeparator("; ");
        sb.append("Protected headers: ");
        sb.append(StringUtils.join(protectedHeaders, ","));

        getLogger().info("{}", sb);
    }

    private SMIMESigningAlgorithm getSigningAlgorithm(Mail mail)
    {
        SMIMESigningAlgorithm algorithm = null;

        // Check if the Mail attribute contains the signing algorithm
        if (StringUtils.isNotEmpty(signingAlgorithmAttribute))
        {
            Optional<String> attributeValue = MailAttributesUtils.getManagedAttributeValue(mail,
                    signingAlgorithmAttribute, String.class);

            if (attributeValue.isPresent())
            {
                algorithm = SMIMESigningAlgorithm.fromName(attributeValue.get());

                if (algorithm == null) {
                    getLogger().warn("The attribute value {} is not a valid algorithm.", attributeValue);
                }
            }
            else {
                getLogger().debug("Attribute with name {} was not found", signingAlgorithmAttribute);
            }
        }

        if (algorithm == null) {
            // Use the default signing algorithm
            algorithm = this.signingAlgorithm;
        }

        getLogger().debug("Signing algorithm: {}", algorithm);

        return algorithm;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

            RetryTemplate retryTemplate = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                    .withListener(loggingRetryListener)
                    .build();

            KeyAndCertificate signingKeyAndCertificate = retryTemplate.execute(ctx ->
                    transactionOperations.execute(status ->
                    {
                        try {
                            return getSigningKeyAndCertificateTransacted(originator);
                        }
                        catch (AddressException | CertStoreException | KeyStoreException | HierarchicalPropertiesException e) {
                            throw new UnhandledException(e);
                        }
                    })
            );

            MimeMessage message = mail.getMessage();

            if (signingKeyAndCertificate != null && message != null)
            {
                SMIMEBuilder sMIMEBuilder = new SMIMEBuilderImpl(message, protectedHeaders);

                sMIMEBuilder.setUseDeprecatedContentTypes(useDeprecatedContentTypes);
                sMIMEBuilder.setMicAlgStyle(micAlgStyle);

                X509Certificate signingCertificate = signingKeyAndCertificate.getCertificate();

                PrivateKey privateKey = signingKeyAndCertificate.getPrivateKey();

                if (privateKey != null && signingCertificate != null)
                {
                    X509Certificate[] chain = retryTemplate.execute(ctx -> transactionOperations.execute(status ->
                            getCertificateChainTransacted(signingCertificate)));

                    sMIMEBuilder.addCertificates(chain);

                    SMIMESigningAlgorithm localAlgorithm = getSigningAlgorithm(mail);

                    sMIMEBuilder.addSigner(privateKey, signingCertificate, localAlgorithm);

                    sMIMEBuilder.sign(signMode);

                    MimeMessage signed = sMIMEBuilder.buildMessage();

                    if (signed != null)
                    {
                        signed.saveChanges();

                        getLogger().atInfo().log("Message was S/MIME signed. Signing algorithm: {}; Sign mode: {}; " +
                        		"MailID: {}; Recipients: {}",
                                localAlgorithm, signMode, CoreApplicationMailAttributes.getMailID(mail),
                                mail.getRecipients());

                        // A new MimeMessage instance will be created. This makes ure that the
                        // MimeMessage can be written by James (using writeTo()). The message-ID
                        // of the source message will be retained if told to do so
                        signed = retainMessageID ? new MimeMessageWithID(signed, message.getMessageID()) :
                            new MimeMessage(signed);

                        mail.setMessage(signed);

                        if (StringUtils.isNotEmpty(signedProcessor)) {
                            mail.setState(signedProcessor);
                        }
                    }
                }
                else {
                    if (privateKey == null) {
                        getLogger().warn("PrivateKey is missing. Message cannot be signed.");
                    }

                    if (signingCertificate == null) {
                        getLogger().warn("signingCertificate is missing. Message cannot be signed.");
                    }
                }
            }
            else {
                getLogger().debug("No signing certificate found, or message is null.");
            }
        }
        catch (MessagingException e) {
            getLogger().error("Error reading the message.", e);
        }
        catch (IOException | SMIMEBuilderException | UnhandledException e) {
            getLogger().error("Error signing the message.", e);
        }
    }

    /*
     * Returns the signing certificate for the user or null if the user does not have a valid signing certificate
     */
    @VisibleForTesting
    protected KeyAndCertificate getSigningKeyAndCertificateTransacted(InternetAddress originator)
    throws AddressException, HierarchicalPropertiesException, CertStoreException, KeyStoreException
    {
        KeyAndCertificate keyAndCertificate = null;

        if (originator != null)
        {
            String userEmail = originator.getAddress();

            userEmail = EmailAddressUtils.canonicalizeAndValidate(userEmail, false);

            User user = userWorkflow.getUser(userEmail, UserWorkflow.UserNotExistResult.DUMMY_IF_NOT_EXIST);

            keyAndCertificate = user.getSigningKeyAndCertificate();
        }

        return keyAndCertificate;
    }

    /*
     * Returns the certificate chain of the signingCertificate.
     */
    @VisibleForTesting
    protected X509Certificate[] getCertificateChainTransacted(X509Certificate signingCertificate)
    {
        X509Certificate[] chain = null;

        try {
            // Use CertificatePathBuilderFactory instead of PKITrustCheckCertificateValidator because we
            // assume that the signing certificate was already checked for revocation etc.
            // CertificatePathBuilderFactory is faster than PKITrustCheckCertificateValidator
            CertificatePathBuilder pathBuilder = certificatePathBuilderFactory.createCertificatePathBuilder();

            CertPathBuilderResult pathBuilderResult = pathBuilder.buildPath(signingCertificate);

            CertPath certPath = pathBuilderResult.getCertPath();

            if (certPath != null && CollectionUtils.isNotEmpty(certPath.getCertificates()))
            {
                X509Certificate root = null;

                if (addRoot && pathBuilderResult instanceof PKIXCertPathBuilderResult pkixCertPathBuilderResult)
                {
                    TrustAnchor trustAnchor = pkixCertPathBuilderResult.getTrustAnchor();

                    if (trustAnchor != null) {
                        root = trustAnchor.getTrustedCert();
                    }
                }

                List<X509Certificate> completePath = new LinkedList<>();

                for (Certificate fromPath : certPath.getCertificates())
                {
                    if (!(fromPath instanceof X509Certificate)) {
                        // only X509Certificates are supported
                        continue;
                    }

                    completePath.add((X509Certificate) fromPath);
                }

                if (root != null && addRoot) {
                    completePath.add(root);
                }

                chain = new X509Certificate[completePath.size()];

                chain = completePath.toArray(chain);
            }
        }
        catch(CertPathBuilderException e)
        {
            if (getLogger().isDebugEnabled()) {
                getLogger().info("Error building path for signing certificate.", e);
            }
            else {
                getLogger().info("Error building path for signing certificate. {}",
                        ExceptionUtils.getRootCauseMessage(e));
            }
        }

        if (chain == null) {
            chain = new X509Certificate[]{signingCertificate};
        }

        return chain;
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (signedProcessor != null) {
            requiredProcessors.add(new ProcessingState(signedProcessor));
        }

        return requiredProcessors;
    }
}
