/*
 * Copyright (c) 2015-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.mailet.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;

import javax.inject.Inject;
import javax.mail.MessagingException;
import java.util.Objects;

/**
 * Mailet implementation that gets an instance of MailMailet from Spring
 */
public class SpringMailet extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SpringMailet.class);

    @Inject
    private AbstractApplicationContext applicationContext;

    private static final String SERVICE_ID_PARAMETER = "springServiceID";

    /*
     * The MailMailet instance retrieved from spring
     */
    private MailMailet mailMailet;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected void initMailet()
    throws MessagingException
    {
        String serviceId = Objects.requireNonNull(StringUtils.trimToNull(getInitParameter(SERVICE_ID_PARAMETER)),
                SERVICE_ID_PARAMETER + " parameter is missing");

        mailMailet = applicationContext.getBean(serviceId, MailMailet.class);

        StrBuilder sb = new StrBuilder();

        sb.append("Initializing mailet " + getMailetName());
        sb.append("; Spring service id: ").append(serviceId);
        sb.append("; MailMailet Class: ").append(mailMailet.getClass());

        getLogger().info("{}", sb);
    }

    @Override
    public void serviceMail(Mail mail) {
        mailMailet.serviceMail(mail);
    }
}
