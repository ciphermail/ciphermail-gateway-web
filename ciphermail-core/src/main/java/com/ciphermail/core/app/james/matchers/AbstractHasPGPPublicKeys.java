/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.PGPPublicKeys;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeySelector;
import com.ciphermail.core.common.security.openpgp.PGPPublicKeyWrapper;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@SuppressWarnings({"java:S6813"})
public abstract class AbstractHasPGPPublicKeys extends AbstractContextAwareCipherMailMatcher
{
    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * Used for the selection of PGP public keys for particular user(s).
     */
    @Inject
    private PGPPublicKeySelector encryptionKeySelector;

    /*
     * The context key under which some data will be stored
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "PGPPublicKeys";

    /*
     * Keeps track of keys and users that have matched.
     */
    protected static class HasPublicKeysActivationContext
    {
        /*
         * Collection of all the PGP public keys for all users.
         *
         * Note: we use a PGPPublicKeyWrapper since a PGPPublicKey does not implement equals and hashCode
         */
        private final Set<PGPPublicKeyWrapper> publicKeys = new HashSet<>();

        /*
         * All the users with valid PGP public keys
         */
        private final Set<User> users = new HashSet<>();

        public Set<PGPPublicKeyWrapper> getPublicKeys() {
            return publicKeys;
        }

        public Set<User> getUsers() {
            return users;
        }

        public void clear()
        {
            publicKeys.clear();
            users.clear();
        }
    }

    @Override
    public void init()
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(transactionOperations);
        Objects.requireNonNull(encryptionKeySelector);
    }

    private Collection<MailAddress> getMatchingMailAddresses(final Collection<MailAddress> mailAddresses,
            HasPublicKeysActivationContext context)
    throws MessagingException
    {
        MailAddressMatcher.HasMatchEventHandler hasMatchEventHandler = AbstractHasPGPPublicKeys.this::hasMatch;

        MailAddressMatcher matcher = new MailAddressMatcher(transactionOperations, userWorkflow,
                hasMatchEventHandler, getLogger());

        return matcher.getMatchingMailAddressesWithRetry(mailAddresses, new AbstractRetryListener()
        {
            @Override
            public <T, E extends Throwable> void onError(RetryContext retryContext, RetryCallback<T, E> retryCallback,
                    Throwable throwable)
            {
                context.clear();
            }
        });
    }

    @VisibleForTesting
    protected boolean hasMatch(@Nonnull User user)
    throws MessagingException
    {
        HasPublicKeysActivationContext context = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                HasPublicKeysActivationContext.class);

        boolean match = false;

        try {
            Set<PGPKeyRingEntry> keyRingEntries = encryptionKeySelector.select(user.getEmail());

            if (CollectionUtils.isNotEmpty(keyRingEntries))
            {
                // There are valid PGP public keys for the user
                context.getUsers().add(user);

                Set<PGPPublicKeyWrapper> publicKeys = context.getPublicKeys();

                for (PGPKeyRingEntry keyRingEntry : keyRingEntries) {
                    publicKeys.add(new PGPPublicKeyWrapper(keyRingEntry.getPublicKey()));
                }

                match = true;
            }
        }
        catch (PGPException | IOException e) {
            throw new MessagingException("Error selecting PGP keys for user " + user.getEmail(), e);
        }

        return match;
    }

    @Override
    public final Collection<MailAddress> matchMail(Mail mail)
    throws MessagingException
    {
        HasPublicKeysActivationContext context = new HasPublicKeysActivationContext();

        getActivationContext().set(ACTIVATION_CONTEXT_KEY, context);

        Collection<MailAddress> matching = getMatchingMailAddresses(getMailAddresses(mail), context);

        addMailProperty(mail, context);

        return matching;
    }

    /*
     * Adds PGP public keys of all matching users to the mail attributes.
     */
    private void addMailProperty(Mail mail, HasPublicKeysActivationContext context)
    {
        Set<PGPPublicKeyWrapper> keyWrappers = context.getPublicKeys();

        if (CollectionUtils.isNotEmpty(keyWrappers))
        {
            List<PGPPublicKey> publicKeys = new LinkedList<>();

            for (PGPPublicKeyWrapper keyWrapper : keyWrappers) {
                publicKeys.add(keyWrapper.getPublicKey());
            }

            CoreApplicationMailAttributes.setPGPPublicKeys(mail, new PGPPublicKeys(publicKeys));
        }
    }

    protected abstract Collection<MailAddress> getMailAddresses(Mail mail)
    throws MessagingException;
}
