/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.PasswordContainer;
import com.ciphermail.core.app.james.Passwords;
import com.ciphermail.core.app.james.PhoneNumber;
import com.ciphermail.core.app.james.PhoneNumbers;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.sms.SMSGateway;
import com.ciphermail.core.common.template.TemplateBuilder;
import com.ciphermail.core.common.template.TemplateHashModelBuilder;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Mailet that will send passwords that are attached to the Mail (see {@link CoreApplicationMailAttributes#getPasswords(Mail)}) to the SMS
 * gateway. The phone numbers to use will be retrieved from the Mail attributes (see {@link CoreApplicationMailAttributes#getPhoneNumbers(Mail)})
 */
@SuppressWarnings("java:S6813")
public class SMSPassword extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SMSPassword.class);

    /*
     * The Freemarker template for the message.
     */
    private String templateFile;

    /*
     * The SMSGateway will send the SMS using the registered transport.
     */
    @Inject
    private SMSGateway smsGateway;

    /*
     * For building Freemarker templates
     */
    @Inject
    private TemplateBuilder templateBuilder;

    @Inject
    private TemplateHashModelBuilder templateHashModelBuilder;

    /*
     * The processor to use when a failure occurs (like missing phone number).
     */
    private String failureProcessor;

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        MAX_EMAIL_LENGTH  ("maxEmailLength"),
        TEMPLATE          ("template"),
        FAILURE_PROCESSOR ("failureProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    private void loadTemplate(StrBuilder sb)
    {
        templateFile = Objects.requireNonNull(getInitParameter(Parameter.TEMPLATE.name),
                Parameter.TEMPLATE.name + " parameter is required");

        // Validate the template (if available)
        try {
            templateBuilder.getTemplateByName(templateFile);
        }
        catch (IOException e) {
            throw new IllegalArgumentException("Error loading the template.", e);
        }

        sb.append("Template: ").append(templateFile);
    }

    private void initFailureProcessor(StrBuilder sb)
    {
        failureProcessor = getInitParameter(Parameter.FAILURE_PROCESSOR.name);

        sb.append("; failureProcessor: ").append(failureProcessor);
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        getLogger().info("Initializing mailet: {}", getMailetName());

        Objects.requireNonNull(smsGateway);
        Objects.requireNonNull(templateBuilder);

        StrBuilder sb = new StrBuilder();

        loadTemplate(sb);

        initFailureProcessor(sb);

        getLogger().info("{}", sb);
    }

    /*
     * Returns the recipients that have valid phone numbers and passwords.
     */
    private Set<String> getValidRecipients(Set<String> uniqueRecipients, Passwords passwords, PhoneNumbers phoneNumbers)
    {
        Set<String> validRecipients = new HashSet<>();

        if (passwords == null)
        {
            getLogger().warn("Passwords not found.");

            return validRecipients;
        }

        if (phoneNumbers == null)
        {
            getLogger().warn("Phone numbers not found.");

            return validRecipients;
        }

        // Check that for every password there is a phone number and vice versa
        if (!passwords.keySet().equals(phoneNumbers.keySet()))
        {
            getLogger().warn("Passwords and PhoneNumbers do not match up.");

            return validRecipients;
        }

        // Check that for every recipient there is a password.
        for (String recipient : uniqueRecipients)
        {
            if (passwords.containsKey(recipient))
            {
                validRecipients.add(recipient);
            }
            else {
                // Just report and continue processing.
                getLogger().warn("Password for recipient {} is missing.", recipient );
            }
        }

        return validRecipients;
    }

    private Address getFrom(Mail mail)
    {
        Address from = null;

        try {
            List<InternetAddress> addresses = EmailAddressUtils.getFromNonStrict(mail.getMessage());

            from = (addresses != null && !addresses.isEmpty()) ? addresses.get(0) : null;
        }
        catch (MessagingException e) {
            getLogger().warn("From address is not a valid email address.");
        }

        return from;
    }

    protected Template getTemplate(Mail mail)
    throws IOException, MessagingException
    {
        return templateBuilder.getTemplateByName(templateFile);
    }

    private String getSMSBody(Mail mail, Address from, PasswordContainer passwordContainer, String recipient)
    throws TemplateException, IOException, MessagingException
    {
        SimpleHash root = templateHashModelBuilder.buildSimpleHash();

        root.put("from",       from);
        root.put("password",   passwordContainer.getPassword());
        root.put("passwordID", passwordContainer.getPasswordID());
        root.put("recipient",  recipient);

        StringWriter writer = new StringWriter();

        getTemplate(mail).process(root, writer);

        return writer.toString();
    }

    private void sendPasswords(Mail mail, Set<String> uniqueRecipients, Passwords passwords,
            PhoneNumbers phoneNumbers)
    throws TemplateException, IOException, MessagingException
    {
        Address from = getFrom(mail);

        for (String recipient : uniqueRecipients)
        {
            PasswordContainer passwordContainer = passwords.get(recipient);
            PhoneNumber phoneNumber = phoneNumbers.get(recipient);

            smsGateway.sendSMS(phoneNumber.getNumber(), getSMSBody(mail, from,
                    Objects.requireNonNull(passwordContainer), recipient));
        }
    }

    private Set<String> getUniqueRecipients(Mail mail)
    {
        Set<String> uniqueRecipients = new HashSet<>();

        Collection<MailAddress> recipients = MailAddressUtils.getRecipients(mail);

        for (MailAddress recipient : recipients)
        {
            String email = EmailAddressUtils.canonicalizeAndValidate(recipient.toString(), true);

            if (email != null) {
                uniqueRecipients.add(email);
            }
        }

        return uniqueRecipients;
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            Passwords passwords = CoreApplicationMailAttributes.getPasswords(mail);
            PhoneNumbers phoneNumbers = CoreApplicationMailAttributes.getPhoneNumbers(mail);

            Set<String> uniqueRecipients = getUniqueRecipients(mail);

            Set<String> validRecipients = getValidRecipients(uniqueRecipients, passwords, phoneNumbers);

            if (!validRecipients.isEmpty()) {
                sendPasswords(mail, validRecipients, passwords, phoneNumbers);
            }
            else {
                if (failureProcessor != null) {
                    mail.setState(failureProcessor);
                }
            }
        }
        catch (TemplateException e) {
            getLogger().error("Error reading template.", e);
        }
        catch (IOException e) {
            getLogger().error("IOException.", e);
        }
        catch (MessagingException e) {
            getLogger().error("MessagingException.", e);
        }
    }

    protected TemplateBuilder getTemplateBuilder() {
        return templateBuilder;
    }

    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (failureProcessor != null) {
            requiredProcessors.add(new ProcessingState(failureProcessor));
        }

        return requiredProcessors;
    }
}
