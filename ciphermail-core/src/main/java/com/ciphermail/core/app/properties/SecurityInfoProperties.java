/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * UserProperties contains higher level functionality (as opposed to the basic HierarchicalProperties methods)
 * for accessing CipherMail specific settings.
 *
 */
public interface SecurityInfoProperties
{
    /**
     * If true, security info will be added to the subject for received messages (see the securityInfoDecryptedTag,
     * securityInfoSignatureValidTag and securityInfoSignatureInvalidTag properties)
     */
    void setAddSecurityInfo(Boolean add)
    throws HierarchicalPropertiesException;

    Boolean getAddSecurityInfo()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was decrypted (only if addSecurityInfo is true)
     */
    void setSecurityInfoDecryptedTag(String tag)
    throws HierarchicalPropertiesException;

    String getSecurityInfoDecryptedTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true)
     */
    void setSecurityInfoSignedValidTag(String tag)
    throws HierarchicalPropertiesException;

    String getSecurityInfoSignedValidTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed and valid (only if addSecurityInfo is true).
     * Additionally, the email address of the signer will be added because the from is different from the signer.
     */
    void setSecurityInfoSignedByValidTag(String tag)
    throws HierarchicalPropertiesException;

    String getSecurityInfoSignedByValidTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages was signed but invalid (only if addSecurityInfo is true)
     */
    void setSecurityInfoSignedInvalidTag(String tag)
    throws HierarchicalPropertiesException;

    String getSecurityInfoSignedInvalidTag()
    throws HierarchicalPropertiesException;

    /**
     * Tag that will be added to the subject if the messages contains mixed content (i.e., some but not all parts are
     * signed and/or encrypted)
     */
    void setSecurityInfoMixedContentTag(String tag)
    throws HierarchicalPropertiesException;

    String getSecurityInfoMixedContentTag()
    throws HierarchicalPropertiesException;
}
