/*
 * Copyright (c) 2015-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MailAttributesUtils;
import com.ciphermail.core.common.util.RegExprUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * Matcher that matches on the subject. The matcher parameters are read from the Mail attributes
 *
 * @author Martijn Brinkers
 *
 */
public class MailAttributesSenderSubjectTrigger extends SubjectTrigger
{
    private static final Logger logger = LoggerFactory.getLogger(MailAttributesSenderSubjectTrigger.class);

    /*
     * JSON parameter names
     */
    private static final String SUBJECT_TRIGGER_ATTRIBUTE_PARAM = "subjectTriggerAttribute";
    private static final String SUBJECT_TRIGGER_DEFAULT_PARAM = "subjectTriggerDefault";
    private static final String REG_EXPR_ATTRIBUTE_PARAM = "regExprAttribute";
    private static final String REG_EXPR_DEFAULT_PARAM = "regExprDefault";
    private static final String REMOVE_PATTERN_ATTRIBUTE_PARAM = "removePatternAttribute";
    private static final String REMOVE_PATTERN_DEFAULT_PARAM = "removePatternDefault";

    /*
     * The Mail attribute to read the subject trigger from
     */
    private String subjectTriggerAttribute;

    /*
     * The default subject trigger to use if not found in the Mail attributes
     */
    private String subjectTriggerDefault;

    /*
     * The Mail attribute to read the reg exp value from
     */
    private String regExprAttribute;

    /*
     * The default value for reg exp if not found in the Mail attributes
     */
    private boolean regExprDefault;

    /*
     * The Mail attribute to read the remove pattern value from
     */
    private String removePatternAttribute;

    /*
     * The default value for remove pattern if not found in the Mail attributes
     */
    private boolean removePatternDefault;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    {
        // Note: super.init() is not called on purpose. super.init() reads the pattern from config.xml while this
        // matcher gets the pattern from the class that extends this class
        getLogger().info("Initializing matcher: {}", getMatcherName());

        String condition = getCondition();

        try {
            getLogger().info("Condition: {}", condition);

            JSONObject parameters = new JSONObject(condition);

            if (parameters.has(SUBJECT_TRIGGER_ATTRIBUTE_PARAM)) {
                subjectTriggerAttribute = parameters.getString(SUBJECT_TRIGGER_ATTRIBUTE_PARAM);
            }

            if (parameters.has(SUBJECT_TRIGGER_DEFAULT_PARAM)) {
                subjectTriggerDefault = parameters.getString(SUBJECT_TRIGGER_DEFAULT_PARAM);
            }

            if (parameters.has(REG_EXPR_ATTRIBUTE_PARAM)) {
                regExprAttribute = parameters.getString(REG_EXPR_ATTRIBUTE_PARAM);
            }

            if (parameters.has(REG_EXPR_DEFAULT_PARAM)) {
                regExprDefault = parameters.getBoolean(REG_EXPR_DEFAULT_PARAM);
            }

            if (parameters.has(REMOVE_PATTERN_ATTRIBUTE_PARAM)) {
                removePatternAttribute = parameters.getString(REMOVE_PATTERN_ATTRIBUTE_PARAM);
            }

            if (parameters.has(REMOVE_PATTERN_DEFAULT_PARAM)) {
                removePatternDefault = parameters.getBoolean(REMOVE_PATTERN_DEFAULT_PARAM);
            }
        }
        catch (JSONException e) {
            throw new IllegalArgumentException("The JSON string is not correct", e);
        }
    }

    @Override
    protected Collection<MailAddress> onMatch(Mail mail, Matcher matcher)
    throws MessagingException, IOException
    {
        boolean removePattern = removePatternDefault;

        if (StringUtils.isNotEmpty(removePatternAttribute)) {
            removePattern = MailAttributesUtils.attributeAsBoolean(mail, removePatternAttribute, removePatternDefault);
        }

        if (removePattern)
        {
            // Remove the pattern from the subject.
            removePattern(mail, matcher);
        }

        return MailAddressUtils.getRecipients(mail);
    }

    @Override
    protected Pattern getHeaderValuePattern(Mail mail)
    {
        Pattern pattern = null;

        String subjectTrigger = subjectTriggerDefault;
        boolean regExpr = regExprDefault;

        if (StringUtils.isNotEmpty(subjectTriggerAttribute)) {
            subjectTrigger= MailAttributesUtils.attributeAsString(mail, subjectTriggerAttribute, subjectTriggerDefault);
        }

        if (StringUtils.isNotEmpty(regExprAttribute)) {
            regExpr = MailAttributesUtils.attributeAsBoolean(mail, regExprAttribute, regExprDefault);
        }

        if (StringUtils.isNotBlank(subjectTrigger))
        {
            if (!regExpr)
            {
                // If the trigger is not a regular expression we need to escape all characters that are
                // special for a regular expression
                subjectTrigger = "(?i)" + RegExprUtils.escape(subjectTrigger);
            }

            try {
                pattern = Pattern.compile(subjectTrigger);
            }
            catch(PatternSyntaxException e) {
                logger.warn("The trigger '{}' is not a valid pattern. Matcher will not match.", subjectTrigger );
            }
        }

        return pattern;
    }
}
