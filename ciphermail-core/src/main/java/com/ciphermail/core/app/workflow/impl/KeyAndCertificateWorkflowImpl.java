/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow.impl;

import com.ciphermail.core.app.workflow.KeyAndCertificateWorkflow;
import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.security.KeyAndCertificate;
import com.ciphermail.core.common.security.KeyAndCertificateImpl;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.security.certificate.X509CertificateInspector;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilder;
import com.ciphermail.core.common.security.certpath.CertificatePathBuilderFactory;
import com.ciphermail.core.common.security.certstore.X509CertStoreEntry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.UnhandledException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.bouncycastle.openssl.PEMEncryptor;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcePEMEncryptorBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathBuilderResult;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Some methods of this KeyAndCertificateWorkflow implementation handle their own transactions.
 *
 * @author Martijn Brinkers
 *
 */
public class KeyAndCertificateWorkflowImpl extends CertificateWorkflowImpl implements KeyAndCertificateWorkflow
{
    private static final Logger logger = LoggerFactory.getLogger(KeyAndCertificateWorkflowImpl.class);

    /*
     * The store where the certificate and keys are stored
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * This is used for building the certificate path (from end-user cert to root)
     */
    private final CertificatePathBuilderFactory pathBuilderFactory;

    /*
     * ALgorithm used for encrypting PEM objects
     */
    private String pemEncryptionAlgorithm = "AES-256-CBC";

    /*
     * At what interval should the session be cleared when importing a keystore
     * Clearing the session is done to save memory when importing a large number of keys
     */
    private int sessionClearInterval = 10;

    public KeyAndCertificateWorkflowImpl(
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull CertificatePathBuilderFactory pathBuilderFactory,
            @Nonnull SessionManager sessionManager,
            @Nonnull TransactionOperations transactionOperations)
    {
        super(keyAndCertStore, sessionManager, transactionOperations);

        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.pathBuilderFactory = Objects.requireNonNull(pathBuilderFactory);
    }

    @Override
    public List<X509CertStoreEntry> importKeyStoreTransacted(@Nonnull KeyStore keyStore, @Nonnull MissingKeyAction missingKeyAction)
    throws KeyStoreException
    {
        try {
            RetryTemplate retryTemplate = createRetryTemplate();

            return retryTemplate.execute(ctx -> getTransactionOperations().execute(status ->
            {
                try {
                    return internalImportKeyStoreTransacted(keyStore, missingKeyAction);
                }
                catch (KeyStoreException e) {
                    throw new UnhandledException(e);
                }
            }));
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof KeyStoreException keyStoreException) {
                throw keyStoreException;
            }
            else {
                throw e;
            }
        }
    }

    private List<X509CertStoreEntry> importChain(Certificate[] chain)
    {
        List<X509CertStoreEntry> imported = new LinkedList<>();

        if (chain != null)
        {
            for (Certificate certificate : chain)
            {
                if (!(certificate instanceof X509Certificate x509Certificate)) {
                    continue;
                }

                try {
                    if (!keyAndCertStore.contains(x509Certificate)) {
                        imported.add(keyAndCertStore.addCertificate((X509Certificate) certificate));
                    }
                }
                catch (CertStoreException e) {
                    logger.error("Error importing certificate.", e);
                }
            }
        }

        return imported;
    }

    private List<X509CertStoreEntry> internalImportKeyStoreTransacted(@Nonnull KeyStore keyStore,
            @Nonnull MissingKeyAction missingKeyAction)
    throws KeyStoreException
    {
        Set<X509CertStoreEntry> importedEntries = new LinkedHashSet<>();

        Enumeration<String> aliases = keyStore.aliases();

        // counts the number of aliases that have been read
        // this is used to periodically clean the session to
        // save memory when importing a very large keystore
        int aliasCounter = 0;

        while (aliases.hasMoreElements())
        {
            aliasCounter++;

            // periodically clear the session to save memory. There is no need to keep all imported keys and
            // certificates in memory because the session and transaction is locally started and finished
            if (aliasCounter % sessionClearInterval == 0) {
                getSessionManager().getSession().clear();
            }

            String alias = aliases.nextElement();

            logger.debug("Alias: {}", alias);

            Certificate certificate = keyStore.getCertificate(alias);

            if (!(certificate instanceof X509Certificate)) {
                // only X509Certificates are supported
                continue;
            }

            try {
                Key key = keyStore.getKey(alias, null);

                if (!(key instanceof PrivateKey)) {
                    key = null;
                }

                if (key == null && missingKeyAction == MissingKeyAction.SKIP_CERTIFICATE_ONLY_ENTRY)
                {
                    logger.debug("Certificate found but missing Private key. Skipping certificate");

                    continue;
                }

                KeyAndCertificate keyAndCertificate = new KeyAndCertificateImpl(
                        (PrivateKey) key, (X509Certificate) certificate);

                importedEntries.add(keyAndCertStore.addKeyAndCertificate(keyAndCertificate));

                Certificate[] chain = keyStore.getCertificateChain(alias);

                importedEntries.addAll(importChain(chain));
            }
            catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException | CertStoreException e) {
                logger.error("Unable to retrieve the key.",e);
            }
        }

        return importedEntries.stream().toList();
    }

    @Override
    public void copyKeysToPKCS12OutputstreamTransacted(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException
    {
        try {
            RetryTemplate retryTemplate = createRetryTemplate();

            retryTemplate.execute(ctx ->
            {
                getTransactionOperations().executeWithoutResult(status ->
                {
                    try {
                      copyKeysToPKCS12OutputstreamTransactedInternal(certificates, password, includeRoot, output);
                    }
                    catch (KeyStoreException e) {
                        throw new UnhandledException(e);
                    }
                });

                return null;
            });
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof KeyStoreException keyStoreException) {
                throw keyStoreException;
            }
            else {
                throw e;
            }
        }
    }

    private void copyKeysToPKCS12OutputstreamTransactedInternal(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException
    {
        try {
            KeyStore keyStore = SecurityFactoryFactory.getSecurityFactory().createKeyStore("PKCS12");

            keyStore.load(null);

            for (X509Certificate certificate : certificates)
            {
                if (certificate == null) {
                    continue;
                }

                X509CertStoreEntry entry = keyAndCertStore.getByCertificate(certificate);

                if (entry != null && entry.getCertificate() != null)
                {
                    KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(entry);

                    if (keyAndCertificate != null)
                    {
                        if (!certificate.equals(keyAndCertificate.getCertificate())) {
                            throw new IllegalStateException("Certificate mismatch.");
                        }

                        X509Certificate[] chain = null;

                        // Build a certificate chain so we add the chain (if valid)
                        try {
                            CertificatePathBuilder pathBuilder = pathBuilderFactory.createCertificatePathBuilder();

                            CertPathBuilderResult pathBuilderResult = pathBuilder.buildPath(certificate);

                            X509Certificate root = null;

                            if (includeRoot && pathBuilderResult instanceof PKIXCertPathBuilderResult pkixCertPathBuilderResult)
                            {
                                TrustAnchor trustAnchor = pkixCertPathBuilderResult.getTrustAnchor();

                                if (trustAnchor != null) {
                                    root = trustAnchor.getTrustedCert();
                                }
                            }

                            CertPath certPath = pathBuilderResult.getCertPath();

                            if (certPath != null && CollectionUtils.isNotEmpty(certPath.getCertificates()))
                            {
                                List<X509Certificate> completePath = new LinkedList<>();

                                for (Certificate fromPath : certPath.getCertificates())
                                {
                                    if (!(fromPath instanceof X509Certificate)) {
                                        // only X509Certificates are supported
                                        continue;
                                    }

                                    completePath.add((X509Certificate) fromPath);
                                }

                                if (root != null && includeRoot) {
                                    completePath.add(root);
                                }

                                chain = new X509Certificate[completePath.size()];

                                chain = completePath.toArray(chain);
                            }
                        }
                        catch (CertPathBuilderException e)
                        {
                            logger.warn("Could not build a path. Message: {}",
                                    ExceptionUtils.getRootCauseMessage(e));
                        }

                        if (ArrayUtils.getLength(chain) == 0) {
                            chain = new X509Certificate[] {certificate};
                        }

                        String alias = X509CertificateInspector.getThumbprint(certificate);

                        if (keyAndCertificate.getPrivateKey() != null)
                        {
                            keyStore.setKeyEntry(alias, keyAndCertificate.getPrivateKey(),
                                    password, chain);
                        }
                        else {
                            keyStore.setCertificateEntry(alias, certificate);
                        }
                    }
                }
            }

            keyStore.store(output, password);
        }
        catch (NoSuchAlgorithmException | CertificateException | IOException | CertStoreException |
                NoSuchProviderException e)
        {
            throw new KeyStoreException(e);
        }
    }

    @Override
    public void copyKeysToPEMOutputstreamTransacted(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull  char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException
    {
        try {
            RetryTemplate retryTemplate = createRetryTemplate();

            retryTemplate.execute(ctx ->
            {
                getTransactionOperations().executeWithoutResult(status ->
                {
                    try {
                        copyKeysToPEMOutputstreamTransactedInternal(certificates, password, includeRoot,
                                output);
                    }
                    catch (KeyStoreException e) {
                        throw new UnhandledException(e);
                    }
                });

                return null;
            });
        }
        catch (UnhandledException e)
        {
            Throwable cause = e.getCause();

            if (cause instanceof KeyStoreException keyStoreException) {
                throw keyStoreException;
            }
            else {
                throw e;
            }
        }
    }

    private void copyKeysToPEMOutputstreamTransactedInternal(@Nonnull Collection<X509Certificate> certificates,
            @Nonnull char[] password, boolean includeRoot, @Nonnull OutputStream output)
    throws KeyStoreException
    {
        try {
            StringWriter pem = new StringWriter();
            JcaPEMWriter pemWriter = new JcaPEMWriter(pem);

            try {
                PEMEncryptor encryptor = new JcePEMEncryptorBuilder(pemEncryptionAlgorithm).build(password);

                // Because we want to export a certificate only once, we need to keep track of the certificates which
                // are already exported. Because we build a chain for every certificate, an intermediate or root
                // can be found multiple times
                Set<X509Certificate> exportedCertificates = new HashSet<>();

                for (X509Certificate certificate : certificates)
                {
                    if (certificate == null) {
                        continue;
                    }

                    X509CertStoreEntry entry = keyAndCertStore.getByCertificate(certificate);

                    if (entry != null && entry.getCertificate() != null)
                    {
                        KeyAndCertificate keyAndCertificate = keyAndCertStore.getKeyAndCertificate(entry);

                        if (keyAndCertificate != null)
                        {
                            if (!certificate.equals(keyAndCertificate.getCertificate())) {
                                throw new IllegalStateException("Certificate mismatch.");
                            }

                            X509Certificate[] chain = null;

                            // Build a certificate chain so we add the chain (if valid)
                            try {
                                CertificatePathBuilder pathBuilder = pathBuilderFactory.createCertificatePathBuilder();

                                CertPathBuilderResult pathBuilderResult = pathBuilder.buildPath(certificate);

                                X509Certificate root = null;

                                if (includeRoot && pathBuilderResult instanceof PKIXCertPathBuilderResult pkixCertPathBuilderResult)
                                {
                                    TrustAnchor trustAnchor = pkixCertPathBuilderResult.getTrustAnchor();

                                    if (trustAnchor != null) {
                                        root = trustAnchor.getTrustedCert();
                                    }
                                }

                                CertPath certPath = pathBuilderResult.getCertPath();

                                if (certPath != null && CollectionUtils.isNotEmpty(certPath.getCertificates()))
                                {
                                    List<X509Certificate> completePath = new LinkedList<>();

                                    for (Certificate fromPath : certPath.getCertificates())
                                    {
                                        if (!(fromPath instanceof X509Certificate)) {
                                            // only X509Certificates are supported
                                            continue;
                                        }

                                        completePath.add((X509Certificate) fromPath);
                                    }

                                    if (root != null && includeRoot) {
                                        completePath.add(root);
                                    }

                                    chain = new X509Certificate[completePath.size()];

                                    chain = completePath.toArray(chain);
                                }
                            }
                            catch (CertPathBuilderException e)
                            {
                                logger.warn("Could not build a path. Message: {}",
                                        ExceptionUtils.getRootCauseMessage(e));
                            }

                            if (ArrayUtils.getLength(chain) == 0) {
                                chain = new X509Certificate[] {certificate};
                            }


                            if (keyAndCertificate.getPrivateKey() != null)
                            {
                                pemWriter.writeObject(new JcaMiscPEMGenerator(keyAndCertificate.getPrivateKey(),
                                        encryptor));
                            }

                            if (chain != null)
                            {
                                for (X509Certificate exportedCertificate : chain) {
                                    if (!exportedCertificates.contains(exportedCertificate)) {
                                        pemWriter.writeObject(new JcaMiscPEMGenerator(exportedCertificate));
                                    }

                                    exportedCertificates.add(exportedCertificate);
                                }
                            }
                        }
                    }
                }
            }
            finally {
                pemWriter.close();
            }

            IOUtils.write(pem.toString(), output, StandardCharsets.UTF_8);
        }
        catch (IOException | CertStoreException e) {
            throw new KeyStoreException(e);
        }
    }

    public String getPemEncryptionAlgorithm() {
        return pemEncryptionAlgorithm;
    }

    public void setPemEncryptionAlgorithm(String pemEncryptionAlgorithm) {
        this.pemEncryptionAlgorithm = pemEncryptionAlgorithm;
    }

    public int getSessionClearInterval() {
        return sessionClearInterval;
    }

    public void setSessionClearInterval(int sessionClearInterval) {
        this.sessionClearInterval = sessionClearInterval;
    }
}
