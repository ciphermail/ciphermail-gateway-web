/*
 * Copyright (c) 2013-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.matchers;

import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.security.smime.SMIMESecurityInfoHeader;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.Collections;

/**
 * Matcher that checks whether the message contained a valid S/MIME signature. The original signature might have
 * been removed and therefore the message might not be signed any more.
 * <p>
 * Currently, This matcher checks for CipherMail specific headers (these headers are removed when the email first enters the
 * system to prevent spoofing.
 * <p>
 * Note: currently only one signature per layer is checked
 *
 * @author Martijn Brinkers
 *
 */
public class HadValidSMIMESignature extends AbstractContextAwareCipherMailMatcher
{
    private static final Logger logger = LoggerFactory.getLogger(HadValidSMIMESignature.class);

    private static final String LAYERS_PARAMETER = "layers";
    private static final String CHECK_SENDER_MISMATCH_PARAMETER = "checkSenderMismatch";

    /*
     * The S/MIME layers to check.
     */
    private int[] layersToCheck;

    /*
     * If true, the sender (from) and email address from the certificate are checked for a mismatch
     */
    private boolean checkSenderMismatch;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void init()
    throws MessagingException
    {
        getLogger().info("Initializing matcher: {}", getMatcherName());

        String condition = getCondition();

        try {
            getLogger().info("Condition: {}", condition);

            JSONObject parameters = new JSONObject(condition);

            JSONArray layersParameter = parameters.getJSONArray(LAYERS_PARAMETER);

            if (layersParameter == null) {
                throw new MessagingException(LAYERS_PARAMETER + " missing");
            }

            layersToCheck = new int[layersParameter.length()];

            for (int i = 0; i < layersToCheck.length; i++) {
                layersToCheck[i] = layersParameter.getInt(i);
            }

            if (parameters.has(CHECK_SENDER_MISMATCH_PARAMETER)) {
                checkSenderMismatch = parameters.getBoolean(CHECK_SENDER_MISMATCH_PARAMETER);
            }
        }
        catch (JSONException e) {
            throw new MessagingException("The JSON string is not correct", e);
        }
    }

    private Boolean isHeaderValueTrue(String[] values)
    throws MessagingException
    {
        if (ArrayUtils.getLength(values) == 0) {
            // No header found
            return null;
        }

        if (ArrayUtils.getLength(values) > 1) {
            throw new MessagingException("Multiple headers found.");
        }

        return "true".equalsIgnoreCase(values[0]);
    }

    private Boolean isLayerValid(MimeMessage message, String from, int layer)
    throws MessagingException
    {
        // Only return valid if the first signer of the layer is trusted and verified
        Boolean trusted = isHeaderValueTrue(message.getHeader(SMIMESecurityInfoHeader.SIGNER_TRUSTED + "0-" + layer));

        Boolean verified = isHeaderValueTrue(message.getHeader(SMIMESecurityInfoHeader.SIGNER_VERIFIED + "0-" + layer));

        if (trusted == null)
        {
            // If one header does not exist, the other should not exist
            if (verified != null) {
                throw new MessagingException("trusted/verified header mismatch");
            }

            // No decision since there were no headers for this layer
            return null;
        }
        else {
            // If one header exists, the other must exist
            if (verified == null) {
                throw new MessagingException("trusted/verified header mismatch");
            }

            boolean valid = trusted && verified;

            if (valid && checkSenderMismatch)
            {
                // The message is only valid if there is no signer mismatch
                valid = false;

                String[] signers = message.getHeader(SMIMESecurityInfoHeader.SIGNER_EMAIL_ADDRESSES + "0-" + layer);

                if (!ArrayUtils.isEmpty(signers))
                {
                    for (String signer : signers)
                    {
                        if (StringUtils.equals(from, EmailAddressUtils.canonicalize(signer))) {
                            // One of the signers is equals to the from so no mismatch
                            valid = true;

                            break;
                        }
                    }
                }
                else {
                    getLogger().warn("No signers found in the message headers");
                }

                if (!valid) {
                    getLogger().warn("There is a mismatch between sender and signer(s). Signers: [{}]",
                            StringUtils.join(signers, ','));
                }
            }

            return valid;
        }
    }

    @Override
    public Collection<MailAddress> matchMail(Mail mail)
    {
        Boolean valid = null;

        try {
            MimeMessage message = mail.getMessage();

            String from = null;

            if (checkSenderMismatch)
            {
                Address[] froms = EmailAddressUtils.getFromQuietly(message);

                if (!ArrayUtils.isEmpty(froms)) {
                    from = EmailAddressUtils.canonicalizeAndValidate(EmailAddressUtils.getEmailAddress(froms[0]), true);
                }
            }

            if (checkSenderMismatch && from == null) {
                getLogger().warn("from address is not set or is not a valid email address");
            }
            else {
                for (int i = 0; i < layersToCheck.length; i++)
                {
                    Boolean thisLayerValid = isLayerValid(message, from, layersToCheck[i]);

                    if (thisLayerValid == null) {
                        // Header was not present for this layer
                        continue;
                    }

                    valid = thisLayerValid;

                    if (Boolean.FALSE.equals(valid))
                    {
                        // If one layer is invalid, stop since the message has no valid S/MIME signature
                        break;
                    }
                }
            }
        }
        catch (MessagingException e)
        {
            getLogger().error("Error validating signature. Assuming the signature is not valid", e);

            valid = false;
        }

        if (valid == null) {
            // There was no S/MIME signature header so no valid signature
            valid = false;
        }

        return Boolean.TRUE.equals(valid) ? MailAddressUtils.getRecipients(mail) : Collections.emptyList();
    }
}
