/*
 * Copyright (c) 2011-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.NamedCertificateCategory;
import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.impl.NamedCertificateUtils;
import com.ciphermail.core.app.james.Certificates;
import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.security.PKISecurityServices;
import com.ciphermail.core.common.security.certificate.validator.CertificateValidatorChain;
import com.ciphermail.core.common.security.certificate.validator.IsValidForSMIMEEncryption;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.ciphermail.core.common.util.LoggingRetryListener;
import com.ciphermail.core.common.util.RetryTemplateBuilderBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.james.core.MailAddress;
import org.apache.mailet.Mail;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.mail.MessagingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Mailet that adds additional certificates for a recipient to the {@link CoreApplicationMailAttributes} certificates
 * collection.
 */
@SuppressWarnings({"java:S6813"})
public abstract class AbstractAddAdditionalCertificates extends AbstractCipherMailMailet
{
    /*
     * The activation context key
     */
    protected static final String ACTIVATION_CONTEXT_KEY = "context";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * For validating the certificates
     */
    @Inject
    private PKISecurityServices pKISecurityServices;

    /*
     * If true, only valid additional certificates are used
     */
    private boolean validateCertificates = true;

    /*
     * Retry listener which just logs
     */
    private final RetryListener loggingRetryListener = new LoggingRetryListener(getLogger());

    /*
     * Callback used to track user objects.
     */
    protected static class ActivationContext
    {
        // The additional certificates
        private final Set<X509Certificate> additionalCertificates = new HashSet<>();

        public Set<X509Certificate> getAdditionalCertificates() {
            return additionalCertificates;
        }

        public void clear() {
            additionalCertificates.clear();
        }
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        VALIDATE_CERTS ("validateCertificates");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(pKISecurityServices);
        Objects.requireNonNull(transactionOperations);

        validateCertificates = getBooleanInitParameter(Parameter.VALIDATE_CERTS.name, validateCertificates);
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    protected void onHandleUserEvent(@Nonnull User user)
    {
        UserPreferences userPreferences = user.getUserPreferences();

        Set<X509Certificate> additionalCertificates = NamedCertificateUtils.getByName(
                NamedCertificateCategory.ADDITIONAL.name(),
                userPreferences.getNamedCertificates());

        // Add the inherited certificates
        additionalCertificates.addAll(NamedCertificateUtils.getByName(
                NamedCertificateCategory.ADDITIONAL.name(),
                userPreferences.getInheritedNamedCertificates()));

        ActivationContext activationContext = getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                ActivationContext.class);

        activationContext.getAdditionalCertificates().addAll(additionalCertificates);
    }

    protected abstract Collection<MailAddress> getMailAddresses(@Nonnull Mail mail)
    throws MessagingException;

    private Set<X509Certificate> validateCertificatesTransacted(@Nonnull Set<X509Certificate> certificates)
    {
        Set<X509Certificate> validated = new HashSet<>();

        for (X509Certificate certificate : certificates)
        {
            CertificateValidatorChain chain = new CertificateValidatorChain();

            // Add the following CertificateValidators to the chain so we can check if the certificate is
            // trusted, not revoked and can be used for S/MIME encryption.
            chain.addValidators(new IsValidForSMIMEEncryption());
            chain.addValidators(pKISecurityServices.getPKITrustCheckCertificateValidatorFactory().
                    createValidator(null));

            try {
                if (chain.isValid(certificate)) {
                    validated.add(certificate);
                }
            }
            catch (Exception e) {
                getLogger().debug("Error validating certificate.", e);
            }
        }

        return validated;
    }

    private Set<X509Certificate> validateCertificates(@Nonnull Set<X509Certificate> certificates)
    {
        RetryTemplate retryTemplate = RetryTemplateBuilderBuilder.createDatabaseRetryTemplateBuilder()
                .withListener(loggingRetryListener).build();

        return Objects.requireNonNullElse(retryTemplate.execute(ctx ->
                transactionOperations.execute(status -> validateCertificatesTransacted(certificates))),
                Collections.emptySet());
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            ActivationContext activationContext = new ActivationContext();

            getActivationContext().set(ACTIVATION_CONTEXT_KEY, activationContext);

            MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                    this::onHandleUserEvent, getLogger());

            RetryListener retryListener = new AbstractRetryListener()
            {
                @Override
                public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                        Throwable throwable)
                {
                    activationContext.clear();
                }
            };

            mailAddressHandler.handleMailAddressesWithRetry(getMailAddresses(mail), retryListener);

            Certificates certificateContainer = CoreApplicationMailAttributes.getCertificates(mail);

            if (certificateContainer != null)
            {
                Set<X509Certificate> certificates = certificateContainer.getCertificates();

                if (CollectionUtils.isNotEmpty(certificates))
                {
                    Set<X509Certificate> additionalCertificates = activationContext.getAdditionalCertificates();

                    certificates.addAll(validateCertificates ? validateCertificates(additionalCertificates) :
                            additionalCertificates);

                    // Need to set the collection again to make it persistent
                    CoreApplicationMailAttributes.setCertificates(mail, certificateContainer);
                }
            }
        }
        catch(MessagingException e) {
            getLogger().error("Error adding additional certificates.", e);
        }
    }
}
