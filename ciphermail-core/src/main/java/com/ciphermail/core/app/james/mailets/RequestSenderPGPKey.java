/*
 * Copyright (c) 2014-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.User;
import com.ciphermail.core.app.UserPreferencesPGPSigningKeySelector;
import com.ciphermail.core.app.james.MailAddressUtils;
import com.ciphermail.core.app.james.MessageOriginatorIdentifier;
import com.ciphermail.core.app.james.UserPersister;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestor;
import com.ciphermail.core.app.openpgp.PGPSecretKeyRequestorResult;
import com.ciphermail.core.app.openpgp.keyserver.KeyServerClientSubmitResult;
import com.ciphermail.core.app.workflow.UserWorkflow;
import com.ciphermail.core.common.mail.EmailAddressUtils;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.security.openpgp.PGPKeyRingEntry;
import com.ciphermail.core.common.util.AbstractRetryListener;
import com.ciphermail.core.common.util.KeyedBarrier;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ObjectUtils.Null;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.mailet.Mail;
import org.bouncycastle.openpgp.PGPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.transaction.support.TransactionOperations;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * Requests a PGP key for the sender.
 */
@SuppressWarnings({"java:S6813"})
public class RequestSenderPGPKey extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(RequestSenderPGPKey.class);

    /*
     * The activation context key
     */
    private static final String ACTIVATION_CONTEXT_KEY = "activationContext";

    /*
     * Used for adding and retrieving users
     */
    @Inject
    private UserWorkflow userWorkflow;

    /*
     * Identifies the 'sender' of the message (default version uses from as the identifier)
     */
    @Inject
    private MessageOriginatorIdentifier messageOriginatorIdentifier;

    /*
     * Requests the keys
     */
    @Inject
    private PGPSecretKeyRequestor keyRequestor;

    /*
     * Retrieves the PGP signing key for a user preferences instance
     */
    @Inject
    private UserPreferencesPGPSigningKeySelector signingKeySelector;

    /*
     * Used to execute database actions in a transaction
     */
    @Inject
    private TransactionOperations transactionOperations;

    /*
     * If true a key won't be requested if the sender already has a key
     */
    private boolean skipIfAvailable = true;

    /*
     * If true and a PGP key is created, the user will be added
     */
    private boolean addUser;

    /*
     * If true, requests are synchronized using a KeyedBarrier
     */
    private boolean synchronizeRequests = true;

    /*
     * Used for synchronizing key requests to make sure that no key is requested multiple times when a message is
     * sent by the same user in a short timeframe.
     */
    private final KeyedBarrier<InternetAddress, Null> keyedBarrier = new KeyedBarrier<>();

    /*
     * The maximum time (in msec) a request can be blocked by the KeyedBarrier
     */
    private static final long KEYED_BARRIER_TIMEOUT = DateUtils.MILLIS_PER_SECOND * 60;

    /*
     * used to track user objects.
     */
    private static class ActivationContext
    {
        private User user;

        public void setUser(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }

        public void clear() {
            user = null;
        }
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        SKIP_IF_AVAILABLE    ("skipIfAvailable"),
        ADD_USER             ("addUser"),
        SYNCHRONIZE_REQUESTS ("synchronizeRequests");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        Objects.requireNonNull(userWorkflow);
        Objects.requireNonNull(messageOriginatorIdentifier);
        Objects.requireNonNull(keyRequestor);
        Objects.requireNonNull(signingKeySelector);
        Objects.requireNonNull(transactionOperations);

        skipIfAvailable = getBooleanInitParameter(Parameter.SKIP_IF_AVAILABLE.name, skipIfAvailable);

        addUser = getBooleanInitParameter(Parameter.ADD_USER.name, addUser);

        synchronizeRequests = getBooleanInitParameter(Parameter.SYNCHRONIZE_REQUESTS.name, synchronizeRequests);

        StrBuilder sb = new StrBuilder();

        sb.append("skipIfAvailable: ");
        sb.append(skipIfAvailable);
        sb.append("; ");
        sb.append("addUser: ");
        sb.append(addUser);
        sb.append("; ");
        sb.append("synchronizeRequests: ");
        sb.append(synchronizeRequests);

        getLogger().info("{}", sb);
    }

    private void makeNonPersistentUserPersistent(User user)
    {
        if (user != null && addUser)
        {
            UserPersister.getInstance(userWorkflow, transactionOperations)
                    .tryToMakeNonPersistentUsersPersistent(Collections.singleton(user));
        }
    }

    /*
     * Gets called by MailAddressHandler#handleRecipients to handle the user
     */
    @VisibleForTesting
    protected void onHandleUserEvent(User user)
    throws MessagingException
    {
        try {
            ActivationContext activationContext = Objects.requireNonNull(getActivationContext().get(ACTIVATION_CONTEXT_KEY,
                    ActivationContext.class));

            boolean requestKey = true;

            if (skipIfAvailable)
            {
                PGPKeyRingEntry entry = signingKeySelector.select(user.getUserPreferences());

                if (entry != null)
                {
                    logger.debug("There is already a signing PGP key for user {}", user.getEmail());

                    requestKey = false;
                }
            }

            if (requestKey)
            {
                // Request a new PGP secret key
                PGPSecretKeyRequestorResult requestResult = keyRequestor.requestSecretKey(user.getEmail());

                // FIXME submitting the key should be done outside this transaction because if the transaction is
                // retried because of a ConstraintViolation, a key which is not stored in the database is submitted
                // anyway
                if (requestResult != null)
                {
                    KeyServerClientSubmitResult submitResult = requestResult.getKeyServerClientSubmitResult();

                    if (submitResult != null)
                    {
                        if (CollectionUtils.isNotEmpty(submitResult.getURLs()))
                        {
                            for (String submitted : submitResult.getURLs()) {
                                logger.info("The new PGP key was submitted to the key server @{}", submitted);
                            }
                        }
                        if (CollectionUtils.isNotEmpty(submitResult.getErrors()))
                        {
                            for (String error : submitResult.getErrors()) {
                                logger.error("Error submitting the new PGP key. Message: {}", error);
                            }
                        }
                    }
                }

                activationContext.setUser(user);
            }
        }
        catch(PGPException | HierarchicalPropertiesException | IOException e) {
            throw new MessagingException("Error reading PGP signing key for user: " + user.getEmail(), e);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            ActivationContext activationContext = new ActivationContext();

            // Place the activationContext in the context so we can use it in onHandleUserEvent
            getActivationContext().set(ACTIVATION_CONTEXT_KEY, activationContext);

            MailAddressHandler mailAddressHandler = new MailAddressHandler(transactionOperations, userWorkflow,
                    this::onHandleUserEvent, getLogger());

            InternetAddress originator = messageOriginatorIdentifier.getOriginator(mail);

            RetryListener retryListener = new AbstractRetryListener()
            {
                @Override
                public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                        Throwable throwable)
                {
                    activationContext.clear();
                }
            };

            // If the From is an invalid email address invalid@invalid.tld will be returned. We don't want to request
            // a PGP key for an invalid email address
            if (!EmailAddressUtils.isInvalidDummyAddress(originator))
            {
                Callable<Null> callable = () ->
                {
                    mailAddressHandler.handleMailAddressesWithRetry(
                            MailAddressUtils.fromAddressArrayToMailAddressList(originator),
                            retryListener);

                    // Note: activationContext.getUser() will return null if there was no PGP key requested for the user
                    makeNonPersistentUserPersistent(activationContext.getUser());

                    return null;
                };

                if (synchronizeRequests) {
                    // Synchronize requests to make sure only one key is requested for a user even if multiple
                    // messages are sent by the same user at the same time (generating a key might take some time)
                    keyedBarrier.execute(originator, callable, KEYED_BARRIER_TIMEOUT);
                }
                else {
                    callable.call();
                }
            }
            else {
                logger.debug("Message originator is an invalid email address. PGP Key request will be skipped.");
            }
        }
        catch(Exception e) {
            getLogger().error("Error requesting a PGP key.", e);
        }
    }
}
