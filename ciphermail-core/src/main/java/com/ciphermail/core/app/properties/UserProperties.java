/*
 * Copyright (c) 2008-2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.app.EncryptMode;
import com.ciphermail.core.app.UserLocality;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;

/**
 * UserProperties contains higher level functionality (as opposed to the basic HierarchicalProperties methods)
 * for accessing CipherMail specific settings.
 */
public interface UserProperties extends HierarchicalProperties
{
    /**
     * A general purpose free form comment field
     */
    void setComment(String comment)
    throws HierarchicalPropertiesException;

    String getComment()
    throws HierarchicalPropertiesException;

    /**
     * The date the user properties was created (can be null if unknown)
     */
    void setDateCreated(Long time)
    throws HierarchicalPropertiesException;

    Long getDateCreated()
    throws HierarchicalPropertiesException;

    /**
     * The locality of the user determines whether an incoming message should be encrypted
     * or decrypted.
     */
    void setUserLocality(UserLocality locality)
    throws HierarchicalPropertiesException;

    UserLocality getUserLocality()
    throws HierarchicalPropertiesException;

    /**
     * If the user supports encryption
     */
    void setEncryptMode(EncryptMode encryptMode)
    throws HierarchicalPropertiesException;

    EncryptMode getEncryptMode()
    throws HierarchicalPropertiesException;

    /**
     * If true, calendar emails are not encrypted and/or signed
     * (Outlook cannot handle encrypted or signed calendar items)
     */
    void setSkipCalendar(Boolean skip)
    throws HierarchicalPropertiesException;

    Boolean getSkipCalendar()
    throws HierarchicalPropertiesException;

    /**
     * If true, a notification will be sent to the sender when the message is encrypted
     */
    void setSendEncryptionNotification(Boolean sendNotification)
    throws HierarchicalPropertiesException;

    Boolean getSendEncryptionNotification()
    throws HierarchicalPropertiesException;

    /**
     * The password policy for passwords set by end-users (for example via the subject line of the email)
     */
    void setPasswordPolicy(String passwordPolicy)
    throws HierarchicalPropertiesException;

    String getPasswordPolicy()
    throws HierarchicalPropertiesException;

    /**
     * If true, the subject filter will be enabled
     */
    void setSubjectFilterEnabled(Boolean enabled)
    throws HierarchicalPropertiesException;

    Boolean getSubjectFilterEnabled()
    throws HierarchicalPropertiesException;

    /**
     * The subject filter reg ex.
     */
    void setSubjectFilterRegEx(String regex)
    throws HierarchicalPropertiesException;

    String getSubjectFilterRegEx()
    throws HierarchicalPropertiesException;

    /**
     * The post-processing header to add for internal email
     */
    void setPostProcessingHeaderInternal(String headerNameValue)
    throws HierarchicalPropertiesException;

    String getPostProcessingHeaderInternal()
    throws HierarchicalPropertiesException;

    /**
     * The post-processing header to add for external email
     */
    void setPostProcessingHeaderExternal(String headerNameValue)
    throws HierarchicalPropertiesException;

    String getPostProcessingHeaderExternal()
    throws HierarchicalPropertiesException;

    /**
     * The organization ID
     */
    void setOrganizationID(String organizationID)
    throws HierarchicalPropertiesException;

    String getOrganizationID()
    throws HierarchicalPropertiesException;

    /**
     * The system sender email address
     */
    void setSystemSender(String email)
    throws HierarchicalPropertiesException;

    String getSystemSender()
    throws HierarchicalPropertiesException;

    /**
     * The system from email address
     */
    void setSystemFrom(String email)
    throws HierarchicalPropertiesException;

    String getSystemFrom()
    throws HierarchicalPropertiesException;

    /**
     * The footer added to emails generated by the gateway
     */
    void setFooter(String footer)
    throws HierarchicalPropertiesException;

    String getFooter()
    throws HierarchicalPropertiesException;

    /**
     * The footer link added to emails generated by the gateway
     */
    void setFooterLink(String footerLink)
    throws HierarchicalPropertiesException;

    String getFooterLink()
    throws HierarchicalPropertiesException;

    /**
     * The footer link title added to emails generated by the gateway
     */
    void setFooterLinkTitle(String footerLinkTitle)
    throws HierarchicalPropertiesException;

    String getFooterLinkTitle()
    throws HierarchicalPropertiesException;

    /**
     * Sets the server secret which is used for generating URLs etc. (using HMAC)
     */
    void setServerSecret(String secret)
    throws HierarchicalPropertiesException;

    String getServerSecret()
    throws HierarchicalPropertiesException;

    /**
     * The secret shared between gateway and client used for one time passwords (OTP) etc
     */
    void setClientSecret(String secret)
    throws HierarchicalPropertiesException;

    String getClientSecret()
    throws HierarchicalPropertiesException;

    /**
     * Secret which is used for the detection of auto generated system mail (using a header signed with hmac)
     */
    void setSystemMailSecret(String secret)
    throws HierarchicalPropertiesException;

    String getSystemMailSecret()
    throws HierarchicalPropertiesException;

    /**
     * Custom property 1
     */
    void setCustom1(String value)
    throws HierarchicalPropertiesException;

    String getCustom1()
    throws HierarchicalPropertiesException;

    /**
     * Custom property 2
     */
    void setCustom2(String value)
    throws HierarchicalPropertiesException;

    String getCustom2()
    throws HierarchicalPropertiesException;

    /**
     * Custom property 3
     */
    void setCustom3(String value)
    throws HierarchicalPropertiesException;

    String getCustom3()
    throws HierarchicalPropertiesException;
}
