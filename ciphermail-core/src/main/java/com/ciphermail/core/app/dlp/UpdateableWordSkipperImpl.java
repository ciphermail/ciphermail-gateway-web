/*
 * Copyright (c) 2010-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.dlp;

import com.ciphermail.core.app.NamedBlobCategories;
import com.ciphermail.core.common.properties.NamedBlob;
import com.ciphermail.core.common.properties.NamedBlobManager;
import com.ciphermail.core.common.util.MiscStringUtils;
import com.ciphermail.core.common.util.UnicodeReader;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.UnhandledException;
import org.springframework.core.io.Resource;
import org.springframework.transaction.support.TransactionOperations;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

/**
 * UpdateableWordSkipper implementation that reads and writes the list of words to skip to/from a {@link NamedBlob}.
 *
 * @author Martijn Brinkers
 *
 */
public class UpdateableWordSkipperImpl implements UpdateableWordSkipper
{
    /*
     * The named blob category to use.
     */
    private static final String NAMED_BLOB_CATEGORY = NamedBlobCategories.DLP_WORD_SKIPPER_CATEGORY;

    /*
     * The named blob name under which the skip list is stored.
     */
    private static final String NAMED_BLOB_KEY = "skipList";

    /*
     * Is used to persistently store the word list.
     */
    private final NamedBlobManager namedBlobManager;

    /*
     * File containing the factory list of skip words
     */
    private final Resource factorySkipListResource;

    /*
     * For starting transactions
     */
    private final TransactionOperations transactionOperations;

    /*
     * The set of words to skip. The words will be stored with lowercase.
     */
    private final Set<String> skipWords = new LinkedHashSet<>();

    public UpdateableWordSkipperImpl(
            @Nonnull NamedBlobManager namedBlobManager,
            @Nonnull Resource factorySkipListResource,
            @Nonnull TransactionOperations transactionOperations)
    {
        this.namedBlobManager = Objects.requireNonNull(namedBlobManager);
        this.factorySkipListResource = Objects.requireNonNull(factorySkipListResource);
        this.transactionOperations = Objects.requireNonNull(transactionOperations);

        loadSkipList();
    }

    private void loadSkipList()
    {
        skipWords.clear();

        transactionOperations.executeWithoutResult(tx ->
        {
            try {
                // First see whether the list can be loaded from the Named Blobs.
                NamedBlob namedBlob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);

                if (namedBlob != null) {
                    loadSkipListFromNamedBlob(namedBlob);
                }
                else {
                    // load factory list
                    try (BufferedInputStream bufferdInput = new BufferedInputStream(
                            factorySkipListResource.getInputStream()))
                    {
                        // Read the file using a Unicode aware reader
                        readSkipList(new UnicodeReader(bufferdInput, CharEncoding.UTF_8));
                    }
                }
            }
            catch (IOException e) {
                throw new UnhandledException(e);
            }
        });
    }

    private void loadSkipListFromNamedBlob(@Nonnull NamedBlob namedBlob)
    throws IOException
    {
        byte[] blob = namedBlob.getBlob();

        if (blob != null)
        {
            // The list is stored as a UTF-8 encoded string
            try {
                readSkipList(new StringReader(new String(blob, StandardCharsets.UTF_8)));
            }
            catch (UnsupportedEncodingException e) {
                throw new IOException(e);
            }
        }
    }

    private void readSkipList(Reader reader)
    throws IOException
    {
        try (LineIterator lineIterator = new LineIterator(reader))
        {
            while (lineIterator.hasNext())
            {
                String line = StringUtils.trimToNull(lineIterator.next());

                if (line == null || line.startsWith("#")) {
                    continue;
                }

                String[] words = StringUtils.split(line);

                for (String word : words) {
                    skipWords.add(word.toLowerCase());
                }
            }
        }
    }

    @Override
    public synchronized boolean isSkip(String word) {
        if (word == null) {
            return true;
        }

        return skipWords.contains(word.toLowerCase());
    }

    @Override
    public synchronized String getSkipList() {
        return StringUtils.join(skipWords, ' ');
    }

    @Override
    public synchronized void setSkipList(String skipList)
    {
        NamedBlob namedBlob = namedBlobManager.getNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);

        if (namedBlob == null) {
            namedBlob = namedBlobManager.createNamedBlob(NAMED_BLOB_CATEGORY, NAMED_BLOB_KEY);
        }

        namedBlob.setBlob(MiscStringUtils.getBytesUTF8(skipList));

        loadSkipList();
    }
}
