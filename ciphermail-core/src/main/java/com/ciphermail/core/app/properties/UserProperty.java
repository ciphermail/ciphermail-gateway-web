/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a method is a user property. A property should be placed on a getter. If there is
 * a setter for the getter, the property will also be writeable.
 */
@Documented
@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface UserProperty
{
    String BASE_KEY = "userproperty.";
    String DISPLAY_NAME_KEY = ".displayname.key";
    String DESCRIPTION_KEY = ".description.key";
    String HELP_KEY = ".help.key";

    int DEFAULT_ORDER = 1000;

    /**
     * Name of the property
     */
    String name();

    /**
     * Whether the property should be visible from the GUI. Set to false for properties which should not be edited
     * with the GUI for example because they are advanced properties which are not used in standard setups.
     * <p>
     * Note: setting visible for a getter, implies that the setter is also not visible
     */
    boolean visible() default true;

    /**
     * The editor to use for editing the property
     */
    String editor() default "";

    /**
     * Key which is used to look up the display name
     */
    String displayNameKey() default "";

    /**
     * Key which is used to look up the description
     */
    String descriptionKey() default "";

    /**
     * Key which is used to look up the help URL
     */
    String helpKey() default "";

    /**
     * Set to true if the property is a user level property
     */
    boolean user() default true;

    /**
     * Set to true if the property is a domain level property
     */
    boolean domain() default true;

    /**
     * Set to true if the property is a global level property
     */
    boolean global() default true;

    /**
     * Set to true if the factory property value can be null
     */
    boolean allowNull() default false;

    /**
     * Options for this UserProperty
     */
    UserPropertyOption[] options() default {};

    /**
     * The order in which the UserProperty should be displayed (form low to high)
     */
    int order() default DEFAULT_ORDER;
}
