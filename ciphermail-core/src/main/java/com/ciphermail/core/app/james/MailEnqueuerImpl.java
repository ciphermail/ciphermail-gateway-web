/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james;

import org.apache.james.queue.api.MailQueue;
import org.apache.james.queue.api.MailQueueName;
import org.apache.james.queue.api.ManageableMailQueue;
import org.apache.mailet.Mail;

import java.util.Objects;

public class MailEnqueuerImpl implements MailEnqueuer
{
    /*
     * Provides instances of the MailQueueFactory which is used to get access to the MailQueue's
     */
    private final MailQueueFactoryProvider mailQueueFactoryProvider;

    /*
     * The name of the Mail queue on which the mail should be enqueued.
     */
    private final MailQueueName mailQueueName;

    public MailEnqueuerImpl(MailQueueFactoryProvider mailQueueFactoryProvider, String queueName)
    {
        this.mailQueueFactoryProvider = Objects.requireNonNull(mailQueueFactoryProvider);
        this.mailQueueName = MailQueueName.of(Objects.requireNonNull(queueName));
    }

    private ManageableMailQueue getManageableMailQueue()
    throws MailQueue.MailQueueException
    {
        // Assume that the underlying mail queue is a ManageableMailQueue
        return mailQueueFactoryProvider.getMailQueueFactory().getQueue(mailQueueName).
                filter(ManageableMailQueue.class::isInstance).
                map(ManageableMailQueue.class::cast).
                orElseThrow(() -> new MailQueue.MailQueueException(String.format(
                        "Queue with name %s not found", mailQueueName)));
    }

    @Override
    public void enqueue(Mail mail)
    throws MailQueue.MailQueueException
    {
        getManageableMailQueue().enQueue(Objects.requireNonNull(mail));
    }

    @Override
    public long getMailQueueSize()
    throws MailQueue.MailQueueException
    {
        return getManageableMailQueue().getSize();
    }
}
