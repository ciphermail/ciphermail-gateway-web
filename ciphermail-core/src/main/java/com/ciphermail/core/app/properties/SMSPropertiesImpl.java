/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.properties;

import com.ciphermail.core.common.properties.DelegatedHierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalProperties;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.properties.HierarchicalPropertiesUtils;
import com.ciphermail.core.common.sms.SMSTransportFactoryRegistry;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

/**
 * Implementation of SMSProperties
 */
@UserPropertiesType(name = "sms-settings", displayName = "Settings", parent = SMSCategory.NAME, order = 10)
@SuppressWarnings({"SpringJavaInjectionPointsAutowiringInspection", "java:S6813"})
public class SMSPropertiesImpl extends DelegatedHierarchicalProperties implements SMSProperties
{
    private static final String SMS_PHONE_NUMBER             = "sms-phone-number";
    private static final String SMS_SEND_ENABLED             = "sms-send-enabled";
    private static final String SMS_RECEIVE_ENABLED          = "sms-receive-enabled";
    private static final String SMS_PHONE_NUMBER_SET_ENABLED = "sms-phone-number-set-enabled";
    private static final String PHONE_DEFAULT_COUNTRY_CODE   = "sms-default-country-code";
    private static final String ACTIVE_TRANSPORT             = "sms-active-transport";

    @Autowired
    private SMSTransportFactoryRegistry transportFactoryRegistry;

    /*
     * Note: do not delete or make private because this class is instantiated using reflection
     */
    public SMSPropertiesImpl(@Nonnull HierarchicalProperties properties) {
        super(properties);
    }

    @Override
    @UserProperty(
            name = SMS_PHONE_NUMBER
    )
    public void setSMSPhoneNumber(String phoneNumber)
    throws HierarchicalPropertiesException
    {
        this.setProperty(SMS_PHONE_NUMBER, UserPropertiesValidators.validateTelephoneNumber(SMS_PHONE_NUMBER,
                phoneNumber));
    }

    @UserProperty(name = SMS_PHONE_NUMBER,
            order = 10,
            allowNull = true
    )
    @Override
    public String getSMSPhoneNumber()
    throws HierarchicalPropertiesException
    {
        return this.getProperty(SMS_PHONE_NUMBER);
    }

    @Override
    @UserProperty(name = SMS_SEND_ENABLED)
    public void setSMSSendEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMS_SEND_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SMS_SEND_ENABLED,
            order = 20
    )
    public @Nonnull Boolean getSMSSendEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMS_SEND_ENABLED);
    }

    @Override
    @UserProperty(name = SMS_RECEIVE_ENABLED)
    public void setSMSReceiveEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMS_RECEIVE_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SMS_RECEIVE_ENABLED,
            order = 30
    )
    public @Nonnull Boolean getSMSReceiveEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMS_RECEIVE_ENABLED);
    }

    @Override
    @UserProperty(name = SMS_PHONE_NUMBER_SET_ENABLED)
    public void setSMSPhoneNumberSetEnabled(Boolean enabled)
    throws HierarchicalPropertiesException
    {
        HierarchicalPropertiesUtils.setBoolean(this, SMS_PHONE_NUMBER_SET_ENABLED, enabled);
    }

    @Override
    @UserProperty(name = SMS_PHONE_NUMBER_SET_ENABLED,
            order = 40
    )
    public @Nonnull Boolean getSMSPhoneNumberSetEnabled()
    throws HierarchicalPropertiesException
    {
        return HierarchicalPropertiesUtils.getBoolean(this, SMS_PHONE_NUMBER_SET_ENABLED);
    }

    @Override
    @UserProperty(name = PHONE_DEFAULT_COUNTRY_CODE)
    public void setPhoneDefaultCountryCode(String defaultCountryCode)
    throws HierarchicalPropertiesException
    {
        // TODO validate country code or remove country code and require tel number to be fully qualified
        setProperty(PHONE_DEFAULT_COUNTRY_CODE, defaultCountryCode);
    }

    @Override
    @UserProperty(name = PHONE_DEFAULT_COUNTRY_CODE,
            order = 50,
            allowNull = true
    )
    public String getPhoneDefaultCountryCode()
    throws HierarchicalPropertiesException
    {
        return getProperty(PHONE_DEFAULT_COUNTRY_CODE);
    }

    @Override
    @UserProperty(name = ACTIVE_TRANSPORT,
            user = false, domain = false
    )
    public void setActiveSMSTransportName(String defaultSMSTransportName)
    throws HierarchicalPropertiesException
    {
        setProperty(ACTIVE_TRANSPORT, UserPropertiesValidators.validateSMSTransport(ACTIVE_TRANSPORT,
                defaultSMSTransportName, transportFactoryRegistry));
    }

    @Override
    @UserProperty(name = ACTIVE_TRANSPORT,
            user = false, domain = false,
            order = 60,
            allowNull = true
    )
    public String getActiveSMSTransportName()
    throws HierarchicalPropertiesException
    {
        return getProperty(ACTIVE_TRANSPORT);
    }
}
