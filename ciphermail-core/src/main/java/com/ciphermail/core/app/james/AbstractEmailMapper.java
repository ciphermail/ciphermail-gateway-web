package com.ciphermail.core.app.james;

import com.ciphermail.core.common.mail.EmailAddressUtils;

import java.util.HashMap;

/**
 * Hashmap with put and set which checks whether the input is a valid email address. The email address is
 * canonicalized.
 *
 * Note: only put will be canonicalized. It is possible to add items with invalid email addresses, for example using
 * putAll.
 *
 * @param <T>
 */
public class AbstractEmailMapper<T> extends HashMap<String, T>
{
    @Override
    public T put(String email, T value)
    {
        email = EmailAddressUtils.canonicalizeAndValidate(email, true);

        if (email == null) {
            // Only accept valid email addresses
            return null;
        }

        return super.put(email, value);
    }

    @Override
    public T get(Object object)
    {
        if (!(object instanceof String)) {
            return null;
        }

        String email = EmailAddressUtils.canonicalizeAndValidate((String) object, true);

        if (email == null) {
            // Only accept valid email addresses
            return null;
        }

        return super.get(email);
    }
}
