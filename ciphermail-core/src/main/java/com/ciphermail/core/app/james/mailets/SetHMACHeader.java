/*
 * Copyright (c) 2011-2019, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.james.mailets;

import com.ciphermail.core.app.james.CoreApplicationMailAttributes;
import com.ciphermail.core.common.mail.MailUtils;
import com.ciphermail.core.common.security.SecurityFactory;
import com.ciphermail.core.common.security.SecurityFactoryFactory;
import com.ciphermail.core.common.util.HexUtils;
import com.ciphermail.core.common.util.MiscStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.mailet.Mail;
import org.apache.mailet.ProcessingState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Mailet that sets a header with a HMAC calculated from a 'secret'. This can be used to identify
 * that a certain mail was created by a server. Because the header value cannot be kept secret,
 * the header value should only be used as a quick check and signing, for example DKIM, should
 * be used to check whether the email was really created by the server.
 */
public class SetHMACHeader extends AbstractCipherMailMailet
{
    private static final Logger logger = LoggerFactory.getLogger(SetHMACHeader.class);

    @Override
    protected Logger getLogger() {
        return logger;
    }

    /*
     * Thrown when the secret is not set
     */
    private static class MissingSecretException extends Exception {
    }

    /*
     * The mailet initialization parameters used by this mailet.
     */
    private enum Parameter
    {
        HEADER          ("header"),
        VALUE           ("value"),
        SECRET          ("secret"),
        ERROR_PROCESSOR ("errorProcessor");

        private final String name;

        Parameter(String name) {
            this.name = name;
        }
    }

    /*
     * The hmac algorithm.
     */
    private static final String ALGORITHM = "HmacSHA256";

    /*
     * For getting the HMAC instance
     */
    private SecurityFactory securityFactory;

    /*
     * The name of the header to add
     */
    private String header;

    /*
     * The value which will be HMAC'd. It is assumed that the value only contains
     * ASCII characters.
     */
    private String value;

    /*
     * The secret key for the HMAC. It is assumed that the secret only contains
     * ASCII characters. It should therefore be long enough.
     */
    private String secret;

    /*
     * If set and an error occurs removing the headers, the name of next processor
     */
    private String errorProcessor;

    @Override
    public void initMailet()
    throws MessagingException
    {
        super.initMailet();

        header = Objects.requireNonNull(getInitParameter(Parameter.HEADER.name),
                Parameter.HEADER.name + " is required");

        value = Objects.requireNonNull(getInitParameter(Parameter.VALUE.name),
                Parameter.VALUE.name  + " is required");

        secret = getInitParameter(Parameter.SECRET.name);
        securityFactory = SecurityFactoryFactory.getSecurityFactory();
        errorProcessor = getInitParameter(Parameter.ERROR_PROCESSOR.name);
    }

    protected byte[] getSecret(Mail mail)
    throws MessagingException
    {
        return MiscStringUtils.getBytesASCII(secret);
    }

    private String calculateHMAC(String value, Mail mail)
    throws MessagingException, MissingSecretException
    {

        try {
            Mac mac = securityFactory.createMAC(ALGORITHM);

            byte[] localSecret = getSecret(mail);

            if (localSecret == null) {
                throw new MissingSecretException();
            }

            SecretKeySpec keySpec = new SecretKeySpec(localSecret, "raw");

            mac.init(keySpec);

            mac.update(MiscStringUtils.getBytesASCII(value));

            return HexUtils.hexEncode(mac.doFinal());
        }
        catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException e) {
            throw new MessagingException("Error creating HMAC.", e);
        }
    }

    @Override
    public void serviceMail(Mail mail)
    {
        try {
            final MimeMessage sourceMessage = mail.getMessage();

            try {
                MimeMessage clone = MailUtils.cloneMessageWithFixedMessageID(sourceMessage);

                clone.setHeader(header, calculateHMAC(value, mail));

                clone.saveChanges();

                MailUtils.validateMessage(clone);

                mail.setMessage(clone);
            }
            catch (MissingSecretException e)
            {
                getLogger().warn("Secret is not set.");

                if (StringUtils.isNotEmpty(errorProcessor)) {
                    mail.setState(errorProcessor);
                }
            }
            catch (Exception e)
            {
                getLogger().warn("Message with message-id {} and MailID {} is not a valid MIME message. " +
                        "The HMAC header can therefore not be set.",
                        MailUtils.getMessageIDQuietly(sourceMessage),
                        CoreApplicationMailAttributes.getMailID(mail));

                if (StringUtils.isNotEmpty(errorProcessor)) {
                    mail.setState(errorProcessor);
                }
            }
        }
        catch (Exception e)
        {
            getLogger().error("Error setting HMAC header.", e);

            if (StringUtils.isNotEmpty(errorProcessor)) {
                mail.setState(errorProcessor);
            }
        }
    }
    @Override
    public Collection<ProcessingState> requiredProcessingState()
    {
        List<ProcessingState> requiredProcessors = new LinkedList<>(super.requiredProcessingState());

        if (errorProcessor != null) {
            requiredProcessors.add(new ProcessingState(errorProcessor));
        }

        return requiredProcessors;
    }
}
