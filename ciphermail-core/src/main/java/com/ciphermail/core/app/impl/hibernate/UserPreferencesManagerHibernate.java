/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.impl.hibernate;

import com.ciphermail.core.app.UserPreferences;
import com.ciphermail.core.app.UserPreferencesManager;
import com.ciphermail.core.app.properties.UserPropertiesFactoryRegistry;
import com.ciphermail.core.common.hibernate.CloseSessionOnCloseIterator;
import com.ciphermail.core.common.hibernate.HibernateUtils;
import com.ciphermail.core.common.hibernate.SessionAdapterFactory;
import com.ciphermail.core.common.hibernate.SessionManager;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.security.KeyAndCertStore;
import com.ciphermail.core.common.util.CloseableIterator;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.jdbc.ReturningWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.sql.SQLException;
import java.util.Objects;

/**
 * A UserPreferencesManagerHibernate manages UserPreferences for a specific category (for example user category,
 * global category etc.).
 *
 * @author Martijn Brinkers
 *
 */
public class UserPreferencesManagerHibernate implements UserPreferencesManager
{
    private static final Logger logger = LoggerFactory.getLogger(UserPreferencesManagerHibernate.class);

    private static final String INCORRECT_IMPLEMENTATION =
        "UserPreferences cannot be deleted because user is not a UserPreferencesHibernate.";

    /*
     * The category of UserPreferences managed by this instance.
     */
    private final String category;

    /*
     * The store containing private keys and X509Certificates.
     */
    private final KeyAndCertStore keyAndCertStore;

    /*
     * Handles the session state.
     */
    private final SessionManager sessionManager;

    /*
     * Provides a factory for creating UserPropertiesImpl instances
     */
    private final UserPropertiesFactoryRegistry userPropertiesFactoryRegistry;

    protected UserPreferencesManagerHibernate(
            @Nonnull String category,
            @Nonnull KeyAndCertStore keyAndCertStore,
            @Nonnull SessionManager sessionManager,
            @Nonnull UserPropertiesFactoryRegistry userPropertiesFactoryRegistry)
    {
        this.category = Objects.requireNonNull(category);
        this.keyAndCertStore = Objects.requireNonNull(keyAndCertStore);
        this.sessionManager = Objects.requireNonNull(sessionManager);
        this.userPropertiesFactoryRegistry = Objects.requireNonNull(userPropertiesFactoryRegistry);
    }

    @Override
    public @Nonnull UserPreferences addUserPreferences(@Nonnull String name) {
        return getUserPreferences(getDAO().addPreferences(category, name));
    }

    @Override
    public boolean deleteUserPreferences(@Nonnull UserPreferences userPreferences)
    {
        boolean deleted = false;

        if (userPreferences instanceof UserPreferencesHibernate userPreferencesHibernate) {
            getDAO().deletePreferences(userPreferencesHibernate.getUserPreferencesEntity());

            deleted = true;
        }
        else {
            logger.warn(INCORRECT_IMPLEMENTATION);
        }

        return deleted;
    }

    @Override
    public boolean isInUse(@Nonnull UserPreferences userPreferences)
    {
        boolean inUse = false;

        if (userPreferences instanceof UserPreferencesHibernate userPreferencesHibernate) {
            inUse = getDAO().isInherited(userPreferencesHibernate.getUserPreferencesEntity());
        }
        else {
            logger.warn(INCORRECT_IMPLEMENTATION);
        }

        return inUse;
    }

    @Override
    public UserPreferences getUserPreferences(@Nonnull String name)
    {
        UserPreferences userPreferences = null;

        UserPreferencesEntity entity = getDAO().getPreferences(category, name);

        if (entity != null) {
            userPreferences = getUserPreferences(entity);
        }

        return userPreferences;
    }

    @Override
    public long getUserPreferencesCount() {
        return getDAO().getUserPreferencesCount(category);
    }

    @Override
    public CloseableIterator<String> getNameIterator()
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        return session.doReturningWork((ReturningWork<CloseableIterator<String>>) connection ->
        {
            StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

            try {
                return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).
                            getPreferencesIterator(category),
                        SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
            }
            catch (Exception e)
            {
                HibernateUtils.closeSessionQuietly(statelessSession);

                throw new SQLException(e);
            }
        });
    }

    @Override
    public CloseableIterator<String> getStatefulNameIterator() {
        return getDAO().getPreferencesIterator(category);
    }

    @Override
    public CloseableIterator<String> getNameIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection)
    {
        // Share the SQL connection from the session for the stateless session to make it uses the same transaction.
        // To make sure that the stateless session has access to the data which is not yet committed but cached,
        // we need to flush the session before opening a stateless session
        Session session = sessionManager.getSession();

        session.flush();

        return session.doReturningWork((ReturningWork<CloseableIterator<String>>) connection ->
        {
            StatelessSession statelessSession = sessionManager.openStatelessSession(connection);

            try {
                return new CloseSessionOnCloseIterator<>(getStatelessDAO(statelessSession).getPreferencesIterator(
                            category, firstResult, maxResults, sortDirection),
                        SessionAdapterFactory.create(statelessSession, sessionManager.getSessionFactory()));
            }
            catch (Exception e)
            {
                HibernateUtils.closeSessionQuietly(statelessSession);

                throw new SQLException(e);
            }
        });
    }

    private UserPreferences getUserPreferences(UserPreferencesEntity entity)
    {
        return new UserPreferencesHibernate(entity, keyAndCertStore, sessionManager.getSession(),
                userPropertiesFactoryRegistry);
    }

    private UserPreferencesDAO getDAO() {
        return UserPreferencesDAO.getInstance(SessionAdapterFactory.create(sessionManager.getSession()));
    }

    private UserPreferencesDAO getStatelessDAO(@Nonnull StatelessSession statelessSession)
    {
        return UserPreferencesDAO.getInstance(SessionAdapterFactory.create(
                statelessSession, sessionManager.getSessionFactory()));
    }
}
