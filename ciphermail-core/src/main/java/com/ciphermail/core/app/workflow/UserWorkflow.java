/*
 * Copyright (c) 2008-2022, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.core.app.workflow;

import com.ciphermail.core.app.User;
import com.ciphermail.core.common.hibernate.SortDirection;
import com.ciphermail.core.common.properties.HierarchicalPropertiesException;
import com.ciphermail.core.common.util.RequiredByJames;
import com.ciphermail.core.common.util.CloseableIterator;
import com.ciphermail.core.common.util.CloseableIteratorException;

import javax.annotation.Nonnull;
import javax.mail.internet.AddressException;
import java.util.Set;

/**
 * UserWorkflow is a higher level (compared to {@link com.ciphermail.core.app.UserManager} service to manage user.
 * For example, it makes sure that a user inherits the correct preferences (from domain(s), global, factory) and
 * has methods to get additional information (like for example isInUse).
 */
@RequiredByJames
public interface UserWorkflow
{
    enum UserNotExistResult {NULL_IF_NOT_EXIST, DUMMY_IF_NOT_EXIST}

    /**
     * Returns the user. Return value if a non-existing user, depends on the userNotExistResult
     * parameter. If CREATE_IF_NOT_EXIST addUser is called.
     */
    User getUser(@Nonnull String email, UserNotExistResult userNotExistResult)
    throws AddressException, HierarchicalPropertiesException;

    /**
     * Returns a bounded set of users.
     */
    Set<User> getUsers(Integer firstResult, Integer maxResults, SortDirection sortDirection)
    throws AddressException, CloseableIteratorException, HierarchicalPropertiesException;

    /**
     * Returns the number of users.
     */
    long getUserCount();

    /**
     * Adds a user with the given email. The user is not yet persisted. Use makePersistent
     * to persist the user.
     */
    User addUser(@Nonnull String email)
    throws AddressException, HierarchicalPropertiesException;

    /**
     * True if the user is in use and cannot be deleted
     */
    boolean isInUse(@Nonnull String email)
    throws AddressException;

    /**
     * Deletes the user. Returns true if deleted.
     */
    boolean deleteUser(@Nonnull User user);

    /**
     * Makes the user persistent, i.e., store the user in the database.
     */
    User makePersistent(@Nonnull User user);

    /**
     * Returns true if the user is persistent.
     */
    boolean isPersistent(@Nonnull User user);

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    CloseableIterator<String> getEmailIterator();

    /**
     * Returns an iterator that returns all the email addresses of all the users. This iterator must be manually closed.
     */
    CloseableIterator<String> getEmailIterator(Integer firstResult, Integer maxResults,
            SortDirection sortDirection);

    /**
     * Returns an iterator that returns all the email addresses of all the users that match the
     * search string (using LIKE). This iterator must be manually closed.
     */
    CloseableIterator<String> searchEmail(@Nonnull String search, Integer firstResult, Integer maxResults,
            SortDirection sortDirection);

    /**
     * Returns the number of email address that will be returned by a call to searchEmail if maxResults is MAXINT.
     */
    long getSearchEmailCount(@Nonnull String search);
}
