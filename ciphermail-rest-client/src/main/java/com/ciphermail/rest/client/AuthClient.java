/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.app.admin.AuthenticationType;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@SuppressWarnings({"java:S6813"})
public class AuthClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<AuthDTO.OAuth2ClientRegistrationDetails> getOIDCClients()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(RestPaths.AUTH_GET_OAUTH2_CLIENTS_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Set<String> getAvailablePermissions()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(RestPaths.AUTH_GET_AVAILABLE_PERMISSIONS_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<AuthDTO.RoleDetails> getRoles()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(RestPaths.AUTH_GET_ROLES_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public AuthDTO.RoleDetails getRole(@Nonnull String role)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_ROLE_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(AuthDTO.RoleDetails.class);
    }

    public List<AuthDTO.AdminDetails> getAdmins()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(RestPaths.AUTH_GET_ADMINS_PATH)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public AuthDTO.AdminDetails getAdmin(@Nonnull String admin)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_ADMIN_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(AuthDTO.AdminDetails.class);
    }

    public void addAdmin(@Nonnull String admin, @Nonnull AuthenticationType authenticationType)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);
        params.put("authenticationType", authenticationType);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_ADD_ADMIN_PATH)
                        .queryParam("admin", "{admin}")
                        .queryParam("authenticationType", "{authenticationType}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deleteAdmin(@Nonnull String admin)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_DELETE_ADMIN_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void createRole(@Nonnull String role)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_CREATE_ROLE_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deleteRole(@Nonnull String role)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_DELETE_ROLE_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void addRolePermissions(
            @Nonnull String role,
            @Nonnull List<String> permissions)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_ADD_ROLE_PERMISSIONS_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(permissions)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setRolePermissions(
            @Nonnull String role,
            @Nonnull List<String> permissions)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_ROLE_PERMISSIONS_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(permissions)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void removeRolePermissions(
            @Nonnull String role,
            @Nonnull List<String> permissions)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_REMOVE_ROLE_PERMISSIONS_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(permissions)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setPassword(@Nonnull String admin, @Nonnull String password)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_PASSWORD_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(password)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setIpAddresses(@Nonnull String admin, @Nonnull List<String> ipAddresses)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_IP_ADDRESSES_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(ipAddresses)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setRoles(@Nonnull String admin, @Nonnull List<String> roles)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_ROLES_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(roles)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setInheritedRoles(@Nonnull String role, @Nonnull List<String> inheritedRoles)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("role", role);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_INHERITED_ROLES_PATH)
                        .queryParam("role", "{role}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(inheritedRoles)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Set<String> getEffectivePermissions(@Nonnull String admin)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_EFFECTIVE_PERMISSIONS_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void set2FAEnabled(@Nonnull String admin, boolean enable)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);
        params.put("enable", enable);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_2FA_ENABLED_PATH)
                        .queryParam("admin", "{admin}")
                        .queryParam("enable", "{enable}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void set2FASecret(@Nonnull String admin, @Nonnull String secret)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_2FA_SECRET_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(secret)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String get2FASecret(@Nonnull String admin)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("admin", admin);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_2FA_SECRET_PATH)
                        .queryParam("admin", "{admin}")
                        .build(params))
                .accept(MediaType.TEXT_PLAIN)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getCaller2FASecret()
    {
        Map<String, Object> params = new HashMap<>();

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_CALLER_2FA_SECRET_PATH)
                        .build(params))
                .accept(MediaType.TEXT_PLAIN)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getCaller2FASecretQRCode()
    {
        Map<String, Object> params = new HashMap<>();

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_CALLER_2FA_SECRET_QR_CODE_PATH)
                        .build(params))
                .accept(MediaType.TEXT_PLAIN)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setCaller2FAEnabled(boolean enable)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("enable", enable);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_SET_CALLER_2FA_ENABLED_PATH)
                        .queryParam("enable", "{enable}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public boolean getCaller2FAEnabled()
    {
        return Boolean.TRUE.equals(restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.AUTH_GET_CALLER_2FA_ENABLED_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {}));
    }
}
