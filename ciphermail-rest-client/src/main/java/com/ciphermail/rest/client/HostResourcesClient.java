/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.client.util.RestClientUtil;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class HostResourcesClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public ResponseEntity<ByteArrayResource> getPublicResource(@Nonnull String id, String hostname)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);
        params.put("hostname", hostname);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE)
                        .queryParam("id", "{id}")
                        .queryParamIfPresent("hostname", RestClientUtil.getOptionalQueryValue(hostname, "{hostname}"))
                        .build(params))
                .accept(MediaType.TEXT_PLAIN)
                .retrieve()
                .toEntity(new ParameterizedTypeReference<>() {});
    }

    public String getEncodedPublicResource(@Nonnull String id, String hostname)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);
        params.put("hostname", hostname);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.HOST_RESOURCES_GET_ENCODED_PUBLIC_RESOURCE)
                        .queryParam("id", "{id}")
                        .queryParamIfPresent("hostname", RestClientUtil.getOptionalQueryValue(hostname, "{hostname}"))
                        .build(params))
                .accept(MediaType.TEXT_PLAIN)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
