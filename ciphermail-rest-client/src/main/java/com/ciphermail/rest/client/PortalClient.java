/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.client.util.RestClientUtil;
import com.ciphermail.rest.core.AuthDTO;
import com.ciphermail.rest.core.PortalDTO;
import com.ciphermail.rest.core.RestPaths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class PortalClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public PortalDTO.PortalProperties getPortalProperties(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_PORTAL_PROPERTIES_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void set2FAEnabled(
            @Nonnull String email,
            @Nonnull boolean enable)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("enable", enable);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_SET_2FA_ENABLED_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("enable", "{enable}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void set2FASecret(
            @Nonnull String email,
            @Nonnull String secret)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_SET_2FA_SECRET_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(secret)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getPdfReplyURL(
            @Nonnull String sender,
            @Nonnull String replyRecipient,
            @Nonnull String replySender,
            @Nonnull String subject,
            String messageID,
            Integer maxSubjectLength,
            String baseURL)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("sender", sender);
        params.put("replyRecipient", replyRecipient);
        params.put("replySender", replySender);
        params.put("subject", subject);

        if (messageID != null) {
            params.put("messageID", messageID);
        }

        if (maxSubjectLength != null) {
            params.put("maxSubjectLength", maxSubjectLength);
        }

        if (baseURL != null) {
            params.put("baseURL", baseURL);
        }

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_PDF_REPLY_URL_PATH)
                        .queryParam("sender", "{sender}")
                        .queryParam("replyRecipient", "{replyRecipient}")
                        .queryParam("replySender", "{replySender}")
                        .queryParam("subject", "{subject}")
                        .queryParamIfPresent("messageID", RestClientUtil.getOptionalQueryValue(messageID, "{messageID}"))
                        .queryParamIfPresent("maxSubjectLength", RestClientUtil.getOptionalQueryValue(maxSubjectLength, "{maxSubjectLength}"))
                        .queryParamIfPresent("baseURL", RestClientUtil.getOptionalQueryValue(baseURL, "{baseURL}"))
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PortalDTO.PdfReplyParameters getPdfReplyParameters(
            @Nonnull String env,
            @Nonnull String hmac)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("env", env);
        params.put("hmac", hmac);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_PDF_REPLY_PARAMETERS_PATH)
                        .queryParam("env", "{env}")
                        .queryParam("hmac", "{hmac}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getPdfPassword(
            @Nonnull String email,
            @Nonnull String code)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("code", code);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_PDF_PASSWORD_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("code", "{code}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getOTPSecretKeyQRCode(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_OTP_SECRET_KEY_QR_CODE_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getOTPJSONCode(@Nonnull String email, @Nonnull String passwordID, int passwordLength)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("passwordID", passwordID);
        params.put("passwordLength", passwordLength);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_OTP_JSON_CODE_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("passwordID", "{passwordID}")
                        .queryParam("passwordLength", "{passwordLength}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getOTPQRCode(@Nonnull String email, @Nonnull String passwordID, int passwordLength)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("passwordID", passwordID);
        params.put("passwordLength", passwordLength);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_GET_OTP_QR_CODE_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("passwordID", "{passwordID}")
                        .queryParam("passwordLength", "{passwordLength}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PortalDTO.PortalSignupValidationResult validatePortalSignup(@Nonnull String signupCode)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("signupCode", signupCode);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_VALIDATE_SIGNUP_PATH)
                        .queryParam("signupCode", "{signupCode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PortalDTO.PortalSignupValidationResult setPortalSignupPassword(
            @Nonnull String signupCode,
            @Nonnull String password)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("signupCode", signupCode);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH)
                        .queryParam("signupCode", "{signupCode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(password)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void forgotPassword(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_FORGOT_PASSWORD_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public PortalDTO.PortalPasswordResetValidationResult validatePortalPasswordReset(@Nonnull String resetCode)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("resetCode", resetCode);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH)
                        .queryParam("resetCode", "{resetCode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PortalDTO.PortalPasswordResetValidationResult resetPortalPassword(
            @Nonnull String resetCode,
            @Nonnull String password)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("resetCode", resetCode);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH)
                        .queryParam("resetCode", "{resetCode}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(password)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void changePortalPassword(@RequestParam String email, @RequestBody AuthDTO.ChangePassword changePassword)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PORTAL_CHANGE_PORTAL_PASSWORD_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(changePassword)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
