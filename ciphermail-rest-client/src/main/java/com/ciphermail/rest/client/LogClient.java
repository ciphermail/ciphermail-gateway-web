/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.client.util.RestClientUtil;
import com.ciphermail.rest.core.LogDTO;
import com.ciphermail.rest.core.RestPaths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class LogClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public String getLogLines(
            @Nonnull LogDTO.LogTarget logTarget,
            boolean reverse,
            Integer firstResult,
            Integer maxResults,
            String filter,
            String since,
            String until)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("logTarget", logTarget);
        params.put("reverse", reverse);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);
        params.put("filter", filter);
        params.put("since", since);
        params.put("until", until);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.LOG_GET_LOG_LINES_PATH)
                        .queryParam("logTarget", "{logTarget}")
                        .queryParam("reverse", "{reverse}")
                        .queryParamIfPresent("firstResult", RestClientUtil.getOptionalQueryValue(firstResult, "{firstResult}"))
                        .queryParamIfPresent("maxResults", RestClientUtil.getOptionalQueryValue(maxResults, "{maxResults}"))
                        .queryParamIfPresent("filter", RestClientUtil.getOptionalQueryValue(filter, "{filter}"))
                        .queryParamIfPresent("since", RestClientUtil.getOptionalQueryValue(since, "{since}"))
                        .queryParamIfPresent("until", RestClientUtil.getOptionalQueryValue(until, "{until}"))
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
