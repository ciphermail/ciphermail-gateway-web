/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.openpgp.PGPKeyGenerationType;
import com.ciphermail.rest.core.PGPDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@SuppressWarnings({"java:S6813"})
public class PGPClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public Integer importKeyring(
            String password,
            boolean ignoreParsingErrors,
            @Nonnull Resource resource)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("ignoreParsingErrors", ignoreParsingErrors);

        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("password", password);
        requestBody.add("keyringFile", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_IMPORT_KEYRING_PATH)
                        .queryParam("ignoreParsingErrors", "{ignoreParsingErrors}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(Integer.class);
    }

    public byte[] exportPublicKeys(@Nonnull List<UUID> ids)
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_EXPORT_PUBLIC_KEYS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(ids)
                .retrieve()
                .body(byte[].class);
    }

    public byte[] exportSecretKeys(
            @Nonnull List<UUID> ids,
            @Nonnull String password)
    {
        PGPDTO.ExportSecretKeysRequestBody requestBody = new PGPDTO.ExportSecretKeysRequestBody(ids, password);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_EXPORT_SECRET_KEYS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(byte[].class);
    }

    public void setPGPKeyEmailAndDomains(
            @Nonnull UUID id,
            @Nonnull List<String> emailAndDomains)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_SET_PGP_KEY_EMAIL_AND_DOMAINS_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(emailAndDomains)
                .retrieve()
                .body(Void.class);
    }

    public List<PGPDTO.PGPKeyDetails> getEncryptionKeysForEmail(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_EMAIL_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<PGPDTO.PGPKeyDetails> getEncryptionKeysForDomain(@Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_ENCRYPTION_KEYS_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<PGPDTO.PGPKeyDetails> getAvailableSigningKeysForEmail(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_EMAIL_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<PGPDTO.PGPKeyDetails> getAvailableSigningKeysForDomain(@Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_AVAILABLE_SIGNING_KEYS_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PGPDTO.PGPKeyDetails getSigningKeyForUser(@Nonnull String email)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_SIGNING_KEY_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PGPDTO.PGPKeyDetails.class);
    }

    public PGPDTO.PGPKeyDetails getSigningKeyForDomain(@Nonnull String domain)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PGPDTO.PGPKeyDetails.class);
    }

    public void setSigningKeyForUser(@Nonnull String email, @Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_SET_SIGNING_KEY_FOR_USER_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public void setSigningKeyForDomain(@Nonnull String domain, @Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_SET_SIGNING_KEY_FOR_DOMAIN_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public PGPDTO.GenerateSecretKeyRingDetails generateSecretKeyRing(
            @Nonnull String email,
            @Nonnull String name,
            @Nonnull PGPKeyGenerationType keyType,
            boolean publishKeys)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("name", name);
        params.put("keyType", keyType);
        params.put("publishKeys", publishKeys);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_GENERATE_SECRET_KEY_RING_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("name", "{name}")
                        .queryParam("keyType", "{keyType}")
                        .queryParam("publishKeys", "{publishKeys}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(PGPDTO.GenerateSecretKeyRingDetails.class);
    }

    public void revokeKey(@Nonnull UUID id)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_REVOKE_KEY_PATH)
                        .queryParam("id", "{id}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public Boolean isUserIDValid(
            @Nonnull UUID id,
            @Nonnull String userID)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);
        params.put("userID", userID);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_IS_USER_ID_VALID_PATH)
                        .queryParam("id", "{id}")
                        .queryParam("userID", "{userID}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public Boolean isUserIDRevoked(
            @Nonnull UUID id,
            @Nonnull String userID)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);
        params.put("userID", userID);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_IS_USER_ID_REVOKED_PATH)
                        .queryParam("id", "{id}")
                        .queryParam("userID", "{userID}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }
}
