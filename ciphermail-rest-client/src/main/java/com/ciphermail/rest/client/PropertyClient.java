/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.PropertyDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class PropertyClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public PropertyDTO.PropertyValue getUserProperty(
            @Nonnull String email,
            @Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_USER_PROPERTY_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PropertyDTO.PropertyValue.class);
    }

    public List<PropertyDTO.PropertyValue> getUserProperties(
            @Nonnull String email,
            @Nonnull List<String> names)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_USER_PROPERTIES_PATH)
                        .queryParam("email", "{email}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(names)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setUserProperty(
            @Nonnull String email,
            @Nonnull String name,
            @Nonnull String value)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(value)
                .retrieve()
                .body(Void.class);
    }

    public void resetUserProperty(
            @Nonnull String email,
            @Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("email", email);
        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_USER_PROPERTY_PATH)
                        .queryParam("email", "{email}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public PropertyDTO.PropertyValue getDomainProperty(
            @Nonnull String domain,
            @Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_DOMAIN_PROPERTY_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PropertyDTO.PropertyValue.class);
    }

    public List<PropertyDTO.PropertyValue> getDomainProperties(
            @Nonnull String domain,
            @Nonnull List<String> names)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_DOMAIN_PROPERTIES_PATH)
                        .queryParam("domain", "{domain}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(names)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setDomainProperty(
            @Nonnull String domain,
            @Nonnull String name,
            @Nonnull String value)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(value)
                .retrieve()
                .body(Void.class);
    }

    public void resetDomainProperty(
            @Nonnull String domain,
            @Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("domain", domain);
        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_DOMAIN_PROPERTY_PATH)
                        .queryParam("domain", "{domain}")
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public PropertyDTO.PropertyValue getGlobalProperty(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_GLOBAL_PROPERTY_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(PropertyDTO.PropertyValue.class);
    }

    public List<PropertyDTO.PropertyValue> getGlobalProperties(@Nonnull List<String> names)
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_GLOBAL_PROPERTIES_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(names)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setGlobalProperty(
            @Nonnull String name,
            @Nonnull String value)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(value)
                .retrieve()
                .body(Void.class);
    }

    public void resetGlobalProperty(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_SET_GLOBAL_PROPERTY_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public List<PropertyDTO.UserPropertiesDetails> getAvailableProperties(
            boolean skipInvisible,
            boolean skipUnauthorized)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("skipInvisible", skipInvisible);
        params.put("skipUnauthorized", skipUnauthorized);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_AVAILABLE_PROPERTIES_PATH)
                        .queryParam("skipInvisible", "{skipInvisible}")
                        .queryParam("skipUnauthorized", "{skipUnauthorized}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public PropertyDTO.UserPropertyDescriptorDetails getUserPropertyDescriptor(@Nonnull String name)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("name", name);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PROPERTY_GET_USER_PROPERTY_DESCRIPTOR_PATH)
                        .queryParam("name", "{name}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
