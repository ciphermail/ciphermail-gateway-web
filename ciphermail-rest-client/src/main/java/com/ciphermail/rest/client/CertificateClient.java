/*
 * Copyright (c) 2023, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.app.workflow.MissingKeyAction;
import com.ciphermail.core.common.security.asn1.ObjectEncoding;
import com.ciphermail.core.common.util.Expired;
import com.ciphermail.core.common.util.Match;
import com.ciphermail.core.common.util.MissingKeyAlias;
import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateImportAction;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.KeyExportFormat;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class CertificateClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<CertificateDTO.X509CertificateDetails> getCertificateDetails(@Nonnull Resource resource)
    {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("chain", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_GET_CERTIFICATE_DETAILS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> getCertificates(
            @Nonnull CertificateStore store,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_GET_CERTIFICATES_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getCertificatesCount(
            @Nonnull CertificateStore store,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_GET_CERTIFICATES_COUNT_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesByEmail(
            @Nonnull CertificateStore store,
            @Nonnull String email,
            @Nonnull Match match,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("email", email);
        params.put("match", match);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("email", "{email}")
                        .queryParam("match", "{match}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long searchForCertificatesByEmailCount(
            @Nonnull CertificateStore store,
            @Nonnull String email,
            @Nonnull Match match,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("email", email);
        params.put("match", match);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_EMAIL_COUNT_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("email", "{email}")
                        .queryParam("match", "{match}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesBySubject(
            @Nonnull CertificateStore store,
            @Nonnull String subject,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("subject", subject);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("subject", "{subject}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long searchForCertificatesBySubjectCount(
            @Nonnull CertificateStore store,
            @Nonnull String subject,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("subject", subject);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_SUBJECT_COUNT_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("subject", "{subject}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public List<CertificateDTO.X509CertificateDetails> searchForCertificatesByIssuer(
            @Nonnull CertificateStore store,
            @Nonnull String issuer,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias,
            int firstResult,
            int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("issuer", issuer);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);
        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("issuer", "{issuer}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long searchForCertificatesByIssuerCount(
            @Nonnull CertificateStore store,
            @Nonnull String issuer,
            @Nonnull Expired expired,
            @Nonnull MissingKeyAlias missingKeyAlias)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("issuer", issuer);
        params.put("expired", expired);
        params.put("missingKeyAlias", missingKeyAlias);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_SEARCH_FOR_CERTIFICATES_BY_ISSUER_COUNT_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("issuer", "{issuer}")
                        .queryParam("expired", "{expired}")
                        .queryParam("missingKeyAlias", "{missingKeyAlias}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public CertificateDTO.X509CertificateDetails getCertificate(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_GET_CERTIFICATE_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public List<CertificateDTO.X509CertificateDetails> importCertificates(
            @Nonnull CertificateStore store,
            @Nonnull CertificateImportAction importAction,
            @Nonnull Resource resource)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("certificates", resource);

        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("importAction", importAction);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_IMPORT_CERTIFICATES_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("importAction", "{importAction}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deleteCertificate(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_DELETE_CERTIFICATE_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body("")
                .retrieve()
                .body(Void.class);
    }

    public byte[] exportCertificates(
            @Nonnull CertificateStore store,
            @Nonnull ObjectEncoding encoding,
            @Nonnull List<String> thumbprints)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("encoding", encoding);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_EXPORT_CERTIFICATES_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("encoding", "{encoding}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(thumbprints)
                .retrieve()
                .body(byte[].class);
    }

    public byte[] exportCertificateChain(
            @Nonnull ObjectEncoding encoding,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("encoding", encoding);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_EXPORT_CERTIFICATE_CHAIN_PATH)
                        .queryParam("encoding", "{encoding}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(byte[].class);
    }

    public List<CertificateDTO.X509CertificateDetails> importPrivateKeys(
            @Nonnull CertificateStore store,
            @Nonnull String keyStorePassword,
            @Nonnull MissingKeyAction missingKeyAction,
            @Nonnull Resource resource)
    {
        MultiValueMap<String, Object> requestBody = new LinkedMultiValueMap<>();

        requestBody.add("file", resource);
        requestBody.add("keyStorePassword", keyStorePassword);

        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("missingKeyAction", missingKeyAction);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_IMPORT_PRIVATE_KEYS_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("missingKeyAction", "{missingKeyAction}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public byte[] exportPrivateKeys(
            @Nonnull CertificateStore store,
            @Nonnull String keyStorePassword,
            @Nonnull KeyExportFormat keyExportFormat,
            boolean includeRoot,
            @Nonnull List<String> thumbprints)
    {
        CertificateDTO.ExportPrivateKeysRequestBody requestBody = new CertificateDTO.ExportPrivateKeysRequestBody(
                keyStorePassword, thumbprints);

        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("keyExportFormat", keyExportFormat);
        params.put("includeRoot", includeRoot);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_EXPORT_PRIVATE_KEYS_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("keyExportFormat", "{keyExportFormat}")
                        .queryParam("includeRoot", "{includeRoot}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(requestBody)
                .retrieve()
                .body(byte[].class);
    }

    public List<CertificateDTO.CertificateReference> getCertificateReferences(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_GET_CERTIFICATE_REFERENCED_DETAILS_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Boolean isCertificateReferenced(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_IS_CERTIFICATE_REFERENCED_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Boolean.class);
    }

    public List<CertificateDTO.X509CertificateDetails> importSystemRoots()
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_IMPORT_SYSTEM_ROOTS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(Void.class)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }
}
