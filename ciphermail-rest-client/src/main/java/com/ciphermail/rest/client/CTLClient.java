/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.core.common.security.ctl.CTLEntryStatus;
import com.ciphermail.rest.core.CTLDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class CTLClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public List<CTLDTO.CTLResult> getCTLs(int firstResult, int maxResults)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("firstResult", firstResult);
        params.put("maxResults", maxResults);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CTL_GET_CTLS_PATH)
                        .queryParam("firstResult", "{firstResult}")
                        .queryParam("maxResults", "{maxResults}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public Long getCTLsCount()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CTL_GET_CTLS_COUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(Long.class);
    }

    public CTLDTO.CTLResult getCTL(@Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CTL_GET_CTL_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CTLDTO.CTLResult.class);
    }

    public CTLDTO.CTLResult setCTL(
            @Nonnull String thumbprint,
            @Nonnull CTLEntryStatus status,
            boolean allowExpired)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);
        params.put("status", status);
        params.put("allowExpired", allowExpired);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CTL_SET_CTL_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .queryParam("status", "{status}")
                        .queryParam("allowExpired", "{allowExpired}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(CTLDTO.CTLResult.class);
    }

    public CTLDTO.CTLResult deleteCTL(@Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CTL_DELETE_CTL_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(CTLDTO.CTLResult.class);
    }
}
