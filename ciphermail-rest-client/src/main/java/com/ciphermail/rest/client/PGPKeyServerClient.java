/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.PGPKeyServerDTO;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@SuppressWarnings({"java:S6813"})
public class PGPKeyServerClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public PGPKeyServerDTO.PGPKeyServerSearchResult searchKeys(
            @Nonnull String query,
            boolean exactMatch,
            int maxKeys)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("query", query);
        params.put("exactMatch", exactMatch);
        params.put("maxKeys", maxKeys);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_SEARCH_KEYS_PATH)
                        .queryParam("query", "{query}")
                        .queryParam("exactMatch", "{exactMatch}")
                        .queryParam("maxKeys", "{maxKeys}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(PGPKeyServerDTO.PGPKeyServerSearchResult.class);
    }

    public PGPKeyServerDTO.PGPKeyServerDownloadResult downloadKeys(
            @Nonnull List<String> keyIDs,
            boolean autoTrust)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("autoTrust", autoTrust);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_DOWNLOAD_KEYS_PATH)
                        .queryParam("autoTrust", "{autoTrust}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(keyIDs)
                .retrieve()
                .body(PGPKeyServerDTO.PGPKeyServerDownloadResult.class);
    }

    public PGPKeyServerDTO.PGPKeyServerDownloadResult refreshKeys(@Nonnull List<UUID> ids)
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_REFRESH_KEYS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(ids)
                .retrieve()
                .body(PGPKeyServerDTO.PGPKeyServerDownloadResult.class);
    }

    public PGPKeyServerDTO.KeyServerSubmitDetails submitKeys(@Nonnull List<UUID> ids)
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_SUBMIT_KEYS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(ids)
                .retrieve()
                .body(PGPKeyServerDTO.KeyServerSubmitDetails.class);
    }

    public List<PGPKeyServerDTO.PGPKeyServerClientSettings> getPGPKeyServers()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_GET_PGP_KEY_SERVERS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setPGPKeyServers(@Nonnull List<String> serverURLs)
    {
        List<PGPKeyServerDTO.PGPKeyServerClientSettings> keyServerClientSettings = new LinkedList<>();

        for (String serverURL : serverURLs)
        {
            keyServerClientSettings.add(new PGPKeyServerDTO.PGPKeyServerClientSettings(serverURL,
                    "/pks/lookup","/pks/add"));
        }

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.PGP_KEY_SERVER_SET_PGP_KEY_SERVERS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(keyServerClientSettings)
                .retrieve()
                .body(Void.class);
    }
}
