/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.CertificateDTO;
import com.ciphermail.rest.core.CertificateStore;
import com.ciphermail.rest.core.RestPaths;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class CertificateValidationClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public CertificateDTO.CertificateValidationResult validateCertificateForSigning(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_SIGNING_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.CertificateValidationResult validateExternalCertificateForSigning(
            String thumbprint,
            @Nonnull Resource resource)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("chain", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_SIGNING_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.CertificateValidationResult validateCertificateForEncryption(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.CertificateValidationResult validateExternalCertificateForEncryption(
            String thumbprint,
            @Nonnull Resource resource)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("chain", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_FOR_ENCRYPTION_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.CertificateValidationResult validateCertificate(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_CERTIFICATE_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.CertificateValidationResult validateExternalCertificate(
            String thumbprint,
            @Nonnull Resource resource)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("thumbprint", thumbprint);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        parts.add("chain", resource);

        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_VALIDATE_EXTERNAL_CERTIFICATE_PATH)
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(parts)
                .retrieve()
                .body(CertificateDTO.CertificateValidationResult.class);
    }

    public CertificateDTO.IssuerWithStore getIssuerCertificate(
            @Nonnull CertificateStore store,
            @Nonnull String thumbprint)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("store", store);
        params.put("thumbprint", thumbprint);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.CERTIFICATE_VALIDATION_GET_ISSUER_CERTIFICATE_PATH)
                        .queryParam("store", "{store}")
                        .queryParam("thumbprint", "{thumbprint}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(CertificateDTO.IssuerWithStore.class);
    }
}
