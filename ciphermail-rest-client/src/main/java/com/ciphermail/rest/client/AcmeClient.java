/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.rest.client;

import com.ciphermail.rest.core.AcmeDTO;
import com.ciphermail.rest.core.RestPaths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@SuppressWarnings({"java:S6813"})
public class AcmeClient
{
    @Autowired
    private RestClientProvider restClientProvider;

    public void createAccountKeyPair(
            @Nonnull AcmeDTO.KeyAlgorithm keyAlgorithm,
            boolean replaceExistingKey)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyAlgorithm", keyAlgorithm);
        params.put("replaceExistingKey", replaceExistingKey);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_CREATE_ACCOUNT_KEYPAIR_PATH)
                        .queryParam("keyAlgorithm", "{keyAlgorithm}")
                        .queryParam("replaceExistingKey", "{replaceExistingKey}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public Boolean accountKeyPairExists()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_ACCOUNT_KEYPAIR_EXISTS_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public URI getTermsOfService()
    {
        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_GET_TERMS_OF_SERVICE_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void setTosAccepted(boolean accepted)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("accepted", accepted);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_SET_TOS_ACCEPTED_PATH)
                        .queryParam("accepted", "{accepted}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(Void.class);
    }

    public boolean isTosAccepted()
    {
        return Boolean.TRUE.equals(restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_GET_TOS_ACCEPTED_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {}));
    }

    public AcmeDTO.Account findOrRegisterAccount()
    {
        return restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_FIND_OR_REGISTER_ACCOUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void deactivateAccount()
    {
        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_DEACTIVATE_ACCOUNT_PATH)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void renewCertificate(boolean forceRenewal)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("forceRenewal", forceRenewal);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_RENEW_CERTIFICATE_PATH)
                        .queryParam("forceRenewal", "{forceRenewal}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public void orderCertificate(
            @Nonnull AcmeDTO.KeyAlgorithm keyAlgorithm,
            @Nonnull List<String> domains)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("keyAlgorithm", keyAlgorithm);

        restClientProvider.getRestClient()
                .post()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_ORDER_CERTIFICATE_PATH)
                        .queryParam("keyAlgorithm", "{keyAlgorithm}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .headers(restClientProvider.getCsrfTokenHeadersConsumer())
                .body(domains)
                .retrieve()
                .body(new ParameterizedTypeReference<>() {});
    }

    public String getAuthorization(@Nonnull String token)
    {
        Map<String, Object> params = new HashMap<>();

        params.put("token", token);

        return restClientProvider.getRestClient()
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(RestPaths.ACME_GET_AUTHORIZATION_PATH)
                        .queryParam("token", "{token}")
                        .build(params))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .body(String.class);
    }
}
