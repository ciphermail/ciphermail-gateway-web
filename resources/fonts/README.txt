Place .ttf fonts here that should be included with the PDF for additional font support.

For example extract DejaVuSans.ttf from dejavu-fonts-ttf-2.33.tar.bz2.

If a new .ttf file is placed in the fonts directory, gateway back-end should be restarted to use the new font files.
