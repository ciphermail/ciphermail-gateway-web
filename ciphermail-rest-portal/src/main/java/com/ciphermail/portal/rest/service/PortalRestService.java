/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest.service;

import com.ciphermail.core.common.mail.MailTransport;
import com.ciphermail.core.common.mail.MailTransportImpl;
import com.ciphermail.rest.client.RestClientProvider;
import com.ciphermail.rest.client.RestClientSettingsProvider;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@Configuration
@SecurityScheme(
        name = "basicAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
@OpenAPIDefinition(
        info = @Info(title = "CipherMail portal back-end API", version = "v1"),
        security = @SecurityRequirement(name = "basicAuth")
)
public class PortalRestService
{
    @Value("${ciphermail.portal.rest-client.base-url}")
    private String baseUrl;

    @Value("${ciphermail.portal.rest-client.username}")
    private String username;

    @Value("${ciphermail.portal.rest-client.password}")
    private String password;

    @Bean
    public RestClientSettingsProvider createRestClientSettingsProvider() {
        return new RestClientSettingsProvider()
        {
            @Override
            public String getBaseUrl() {
                return baseUrl;
            }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public String getPassword() {
                return password;
            }

            @Override
            public String getTOTP() {
                return null;
            }
        };
    }

    @Bean
    public RestClientProvider createRestClientProvider(RestClientSettingsProvider restClientSettingsProvider) {
        return new RestClientProvider(restClientSettingsProvider);
    }

    @Bean
    public MailTransport createMailTransport() {
        return new MailTransportImpl();
    }
}
