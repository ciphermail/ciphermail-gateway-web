/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest.service;

import com.ciphermail.core.common.security.crypto.RandomGenerator;
import com.ciphermail.core.common.security.crypto.impl.RandomGeneratorImpl;
import com.ciphermail.core.common.security.otp.TOTP;
import com.ciphermail.core.common.security.otp.TOTPImpl;
import com.ciphermail.portal.rest.PortalRestPaths;
import com.ciphermail.rest.core.RestPaths;
import com.ciphermail.rest.server.service.PreEmptiveOnlyHttpBasicConfigurer;
import com.ciphermail.rest.server.service.TFAFailureCache;
import com.ciphermail.rest.server.service.TFAPasswordAuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.RequestCacheConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;
import org.springframework.web.filter.RequestContextFilter;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@SuppressWarnings("java:S6813")
public class WebSecurityConfiguration
{
    @Value("${ciphermail.rest.authentication.swagger-require-login:true}")
    private boolean swaggerRequireLogin;

    @Value("${ciphermail.rest.authentication.login-processing-url}")
    private String loginProcessingUrl;

    @Value("${ciphermail.rest.authentication.login-success-url}")
    private String loginSuccessUrl;

    @Value("${ciphermail.rest.authentication.login-failure-url}")
    private String loginFailureUrl;

    @Value("${ciphermail.rest.authentication.logout-url}")
    private String logoutUrl;

    @Value("${ciphermail.rest.authentication.return-401-if-unauthorized}")
    private boolean return401IfUnauthorized;

    @Value("${ciphermail.rest.authentication.oauth2.post-logout-redirect-uri}")
    private String oauth2PostLogoutRedirectUri;

    @Value("${ciphermail.rest.authentication.oauth2.login-processing-url}")
    private String oauth2LoginProcessingUrl;

    @Value("${ciphermail.rest.authentication.oauth2.authorization-request-base-uri}")
    private String authorizationRequestBaseUri;

    /*
     * OAuth2 client registrations
     */
    @Autowired(required = false)
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return Argon2PasswordEncoder.defaultsForSpringSecurity_v5_8();
    }

    @Bean
    public TOTP createTOTPService() {
        return new TOTPImpl();
    }

    @Bean
    public RandomGenerator createRandomGenerator()
    throws NoSuchAlgorithmException, NoSuchProviderException
    {
        return new RandomGeneratorImpl();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(
            HttpSecurity http,
            UserDetailsService userDetailsService,
            TOTP totp,
            TFAFailureCache tfaFailureCache)
    throws Exception
    {
        if (!swaggerRequireLogin) {
            http.authorizeHttpRequests(auth -> auth.requestMatchers(
                "/v3/api-docs/**",
                "/swagger-ui/**").permitAll());
        }

        http
            // some paths should be accessible without authentication
            .authorizeHttpRequests(auth -> auth.requestMatchers(RestPaths.HOST_RESOURCES_GET_PUBLIC_RESOURCE).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(RestPaths.AUTH_GET_OAUTH2_CLIENTS_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PDF_PARSE_PDF_REPLY_PARAMETERS_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PDF_SEND_PDF_REPLY_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PORTAL_VALIDATE_SIGNUP_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PORTAL_FORGOT_PASSWORD_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(PortalRestPaths.PORTAL_RESET_PORTAL_PASSWORD_PATH).permitAll())
            .authorizeHttpRequests(auth -> auth.requestMatchers(RestPaths.ACME_GET_AUTHORIZATION_PATH + "/**").permitAll())
            // /error should always be allowed, if not, any exception (500 error etc.) will result in a 401 error
            // see https://docs.spring.io/spring-boot/reference/web/servlet.html#web.servlet.spring-mvc.error-handling
            .authorizeHttpRequests(auth -> auth.requestMatchers("/error").permitAll())
            // all other paths should be authenticated
            .authorizeHttpRequests(auth -> auth.anyRequest().authenticated())
            .csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()))
            .csrf(csrf -> csrf.csrfTokenRequestHandler(new CsrfTokenRequestAttributeHandler()))
            .formLogin(f -> f.loginProcessingUrl(loginProcessingUrl))
            .formLogin(f -> f.defaultSuccessUrl(loginSuccessUrl, true))
            .formLogin(f -> f.failureUrl(loginFailureUrl))
            .logout(logout -> logout.logoutUrl(logoutUrl))
            .requestCache(RequestCacheConfigurer::disable);

        if (return401IfUnauthorized) {
            // always return a 401 instead of 302 (redirect to log in) if not authenticated
            http.exceptionHandling(e -> e.authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)));
        }

        // only enable oauth2 if an oauth2 client is registered
        if (clientRegistrationRepository != null)
        {
            http
                .oauth2Login(withDefaults())
                .oauth2Login(o -> o.loginProcessingUrl(oauth2LoginProcessingUrl))
                .oauth2Login(o -> o.authorizationEndpoint(a -> a.baseUri(authorizationRequestBaseUri)))
                .oauth2Login(o -> o.defaultSuccessUrl(loginSuccessUrl, true))
                .oauth2Login(o -> o.failureUrl(loginFailureUrl))
                // add OIDC RP-Initiated logout functionality
                .logout(logout -> logout.logoutSuccessHandler(oidcLogoutSuccessHandler()));
        }

        // add pre-emptive basic auth filter
        http.with(new PreEmptiveOnlyHttpBasicConfigurer(), withDefaults());

        // Add Servlet Filter that exposes the request to the current thread
        http.addFilterBefore(new RequestContextFilter(), BasicAuthenticationFilter.class);

        // add filter that checks TOTP if required
        http.addFilterAfter(new TFAPasswordAuthFilter(userDetailsService, totp, tfaFailureCache),
                BasicAuthenticationFilter.class);

        return http.build();
    }

    // logout handler for OpenID connect
    OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler()
    {
        OidcClientInitiatedLogoutSuccessHandler successHandler = new OidcClientInitiatedLogoutSuccessHandler(
                clientRegistrationRepository);

        successHandler.setPostLogoutRedirectUri(oauth2PostLogoutRedirectUri);

        return successHandler;
    }
}
