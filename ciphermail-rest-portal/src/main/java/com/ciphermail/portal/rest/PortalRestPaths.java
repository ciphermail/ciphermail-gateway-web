/*
 * Copyright (c) 2024, CipherMail.
 *
 * This file is part of CipherMail email encryption.
 *
 * CipherMail is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License
 * version 3, 19 November 2007 as published by the Free Software
 * Foundation.
 *
 * CipherMail is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with CipherMail. If not, see <http://www.gnu.org/licenses/>
 *
 * Additional permission under GNU AGPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or
 * combining it with aspectjrt.jar, aspectjweaver.jar, tyrex-1.0.3.jar,
 * freemarker.jar, dom4j.jar, mx4j-jmx.jar, mx4j-tools.jar,
 * spice-classman-1.0.jar, spice-loggerstore-0.5.jar, spice-salt-0.8.jar,
 * spice-xmlpolicy-1.0.jar, wsdl4j-1.6.1.jar (or modified versions of
 * these libraries), containing parts covered by the terms of Eclipse
 * Public License, tyrex license, freemarker license, dom4j license,
 * mx4j license, Spice Software License, Common Development and
 * Distribution License (CDDL), Common Public License (CPL) the
 * licensors of this Program grant you additional permission to convey
 * the resulting work.
 */
package com.ciphermail.portal.rest;

/**
 * All the CipherMail Portal REST paths
 */
@SuppressWarnings("java:S1075")
public class PortalRestPaths
{
    private PortalRestPaths() {
        // empty on purpose
    }

    public static final String PDF_PARSE_PDF_REPLY_PARAMETERS_PATH = "/api/pdf/parsePdfReplyParameters";
    public static final String PDF_SEND_PDF_REPLY_PATH = "/api/pdf/sendPdfReply";
    public static final String PDF_GET_PDF_PASSWORD_PATH = "/api/pdf/getPdfPassword";
    public static final String GET_OTP_SECRET_KEY_QR_CODE_PATH = "/api/pdf/getOTPSecretKeyQRCode";
    public static final String GET_OTP_JSON_CODE_PATH = "/api/pdf/getOTPJSONCode";
    public static final String GET_OTP_QR_CODE_PATH = "/api/pdf/getOTPQRCode";
    public static final String PORTAL_GET_PORTAL_PROPERTIES_PATH = "/api/portal/getPortalProperties";
    public static final String PORTAL_VALIDATE_SIGNUP_PATH = "/api/portal/validatePortalSignup";
    public static final String PORTAL_SET_PORTAL_SIGNUP_PASSWORD_PATH = "/api/portal/setPortalSignupPassword";
    public static final String PORTAL_FORGOT_PASSWORD_PATH = "/api/portal/forgotPassword";
    public static final String PORTAL_VALIDATE_PORTAL_PASSWORD_RESET_PATH = "/api/portal/validatePortalPasswordReset";
    public static final String PORTAL_RESET_PORTAL_PASSWORD_PATH = "/api/portal/resetPortalPassword";
    public static final String PORTAL_CHANGE_PORTAL_PASSWORD_PATH = "/api/portal/changePortalPassword";
    public static final String PORTAL_VALIDATE_TOTP_PATH = "/api/portal/validateTOTP";
    public static final String PORTAL_GET_2FA_SECRET_QR_CODE_PATH = "/api/portal/get2FASecretQRCode";
    public static final String PORTAL_SET_2FA_ENABLED_PATH = "/api/portal/set2FAEnabled";
}
