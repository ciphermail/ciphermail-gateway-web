#!/bin/bash

set -e
set -u
set -o pipefail

# script which can be used to checksum maven dependencies

SCRIPT_NAME=$(basename "$0")

file_extension='sha256'

usage()
{
    echo "Usage: $SCRIPT_NAME [option...]" >&2
    echo "" >&2
    echo "    -h                          show usage" >&2
    echo "    --help                      show usage" >&2
    echo "    --generate-checksums        generate sha256 hashes of the jar dependencies" >&2
    echo "    --validate-checksums        validates sha256 hashes of the jar dependencies" >&2

    exit 1
}

if [ "$#" -eq 0 ]; then usage; fi

generate_checksums()
{
    mvn clean test-compile dependency:copy-dependencies -DexcludeGroupIds="com.ciphermail"
    find . -type d -path '*/target/dependency' -print0 | xargs -0 -I{} bash -c 'sha256sum {}/*.jar > {}/../../maven-dependency-checksum.'"${file_extension}"
}

validate_checksums()
{
    # generate new checksum and diff the files
    file_extension='sha256.new'
    generate_checksums
    find . -name maven-dependency-checksum.sha256 -print0 | xargs -0 -I{} diff {} {}.new
}

GETOPT_TEMP=$(getopt -o h --long "help,generate-checksums,validate-checksums" \
    -n "$SCRIPT_NAME" -- "$@")

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

# Note the quotes around $GETOPT_TEMP: they are essential!
eval set -- "$GETOPT_TEMP"

while true ; do
    case "$1" in
        -h|--help) usage ;;
        --generate-checksums) generate_checksums ; shift 1 ;;
        --validate-checksums) validate_checksums ; shift 1 ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done
