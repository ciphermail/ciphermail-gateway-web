Name: ciphermail-gateway-portal-backend
Version: @RPM_VERSION@
Release: 1
Summary: CipherMail Email Encryption Gateway portal back-end
License: AGPLv3
Group: CipherMail
URL: https://www.ciphermail.com
Vendor: CipherMail
Conflicts: djigzo

%global homedir /opt/ciphermail

# dir where the built rpm will be placed
%define _rpmdir ./build/packages
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

# allow arch dependent binaries in noarch package
%define _binaries_in_noarch_packages_terminate_build 0

%post
# postinst script for CipherMail

systemctl daemon-reload

# All scriptlets MUST exit with the zero exit status (https://fedoraproject.org/wiki/Packaging:Scriptlets)
exit 0

%description

CipherMail is an email server that encrypts and decrypts emails. It supports three encryption standards: S/MIME,
OpenPGP, and PDF encryption. S/MIME and OpenPGP offer authentication, message integrity, non-repudiation, and
protection against message interception.

%install

# create lib dir
mkdir -p %{buildroot}%{homedir}/portal/lib
find ciphermail-gateway-portal-backend/target/ -name \*.jar -exec cp {} %{buildroot}%{homedir}/portal/lib \;

mkdir -p %{buildroot}/etc/ciphermail/portal.spring.config.d

%files

"/etc/*"
"/opt/*"
