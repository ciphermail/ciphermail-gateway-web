Name: ciphermail-gateway-backend
Version: @RPM_VERSION@
Release: 1
Summary: CipherMail Email Encryption Gateway back-end
License: AGPLv3
Group: CipherMail
URL: https://www.ciphermail.com
Vendor: CipherMail
Conflicts: djigzo

Requires: cyrus-sasl
Requires: cyrus-sasl-gs2
Requires: cyrus-sasl-gssapi
Requires: cyrus-sasl-ldap
Requires: cyrus-sasl-md5
Requires: cyrus-sasl-ntlm
Requires: cyrus-sasl-plain
Requires: cyrus-sasl-scram
Requires: cyrus-sasl-sql
Requires: java-17-openjdk-headless
Requires: postfix
Requires: postfix-pcre
Requires: sudo
Requires: tar

%global homedir /opt/ciphermail

# dir where the built rpm will be placed
%define _rpmdir ./build/packages
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

# allow arch dependent binaries in noarch package
%define _binaries_in_noarch_packages_terminate_build 0

%post
# postinst script for CipherMail

systemctl daemon-reload

# All scriptlets MUST exit with the zero exit status (https://fedoraproject.org/wiki/Packaging:Scriptlets)
exit 0

%description

CipherMail is an email server that encrypts and decrypts emails. It supports three encryption standards: S/MIME,
OpenPGP, and PDF encryption. S/MIME and OpenPGP offer authentication, message integrity, non-repudiation, and
protection against message interception.

%install

# create lib dir
mkdir -p %{buildroot}%{homedir}/backend/lib
find ciphermail-gateway-backend/target/ -name \*.jar -exec cp {} %{buildroot}%{homedir}/backend/lib \;

# create /etc/ciphermail dir
mkdir -p %{buildroot}/etc/ciphermail
cp -r ciphermail-gateway-backend/conf/etc/* %{buildroot}/etc/ciphermail

# create scripts dir and copy the required scripts
mkdir -p %{buildroot}%{homedir}/scripts
cp ciphermail-postfix/scripts/*.sh %{buildroot}%{homedir}/scripts/
cp scripts/cm-generate-db-schema.sh %{buildroot}%{homedir}/scripts/
cp scripts/cm-admin-db-tool.sh %{buildroot}%{homedir}/scripts/
chmod -R 755 %{buildroot}%{homedir}/scripts/

# create symlinks to scripts
mkdir -p %{buildroot}/usr/bin
mkdir -p %{buildroot}/usr/sbin

ln -s %{homedir}/scripts/cm-admin-db-tool.sh %{buildroot}/usr/bin/cm-admin-db-tool
ln -s %{homedir}/scripts/cm-postfix-configure-main.sh %{buildroot}/usr/sbin
ln -s %{homedir}/scripts/cm-postfix-ctl.sh %{buildroot}/usr/sbin
ln -s %{homedir}/scripts/cm-postfix-map.sh %{buildroot}/usr/sbin

# copy postfix config files
mkdir -p %{buildroot}%{homedir}/conf/postfix
cp conf/postfix/* %{buildroot}%{homedir}/conf/postfix

# copy sudoers files
mkdir -p %{buildroot}%{homedir}/conf/sudo
cp conf/sudo/* %{buildroot}%{homedir}/conf/sudo

# copy systemd config files
mkdir -p %{buildroot}/lib/systemd/system
cp conf/systemd/ciphermail-backend.service %{buildroot}/lib/systemd/system
mkdir -p %{buildroot}/usr/lib/sysusers.d
cp conf/systemd/ciphermail-backend.conf.sysusers %{buildroot}/usr/lib/sysusers.d/ciphermail-backend.conf

%files

"/etc/*"
"/lib/*"
"/opt/*"
"/usr/bin/*"
"/usr/sbin/*"
"/usr/lib/sysusers.d/*"
