Name: ciphermail-cli
Version: @RPM_VERSION@
Release: 1
Summary: CipherMail CLI
License: AGPLv3
Group: CipherMail
URL: https://www.ciphermail.com
Vendor: CipherMail

Requires: bash-completion
Recommends: jq

%global homedir /opt/ciphermail/cli

# dir where the built rpm will be placed
%define _rpmdir ./build/packages
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

# allow arch dependent binaries in noarch package
%define _binaries_in_noarch_packages_terminate_build 0

%description

CLI tool for interfacing with the CipherMail back-end.

%install

mkdir -p %{buildroot}%{homedir}
mkdir -p %{buildroot}/usr/bin

# copy executable jar
cp ciphermail-shell/target/ciphermail-shell-*-exec.jar %{buildroot}%{homedir}

# copy native executable and script
cp ciphermail-shell/target/ciphermail-shell %{buildroot}/usr/bin
cp ciphermail-shell/scripts/ciphermail-cli %{buildroot}/usr/bin

chmod 755 %{buildroot}/usr/bin/*

# copy bash completion
mkdir -p %{buildroot}/etc/bash_completion.d
cp %{buildroot}/../../ciphermail-shell-completion %{buildroot}/etc/bash_completion.d

%files

"/etc/*"
"/opt/*"
"/usr/bin/*"
